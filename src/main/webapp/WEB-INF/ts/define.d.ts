interface JQueryStatic {

    /**
     * Perform an asynchronous HTTP (Ajax) request.
     *
     * @param settings A set of key/value pairs that configure the Ajax request. All settings are optional. A default can be set for any option with $.ajaxSetup().
     */
    yrcmv(method, args?): any;
}

interface JQuery {
    yrcmv(method, args?): JQuery;
}