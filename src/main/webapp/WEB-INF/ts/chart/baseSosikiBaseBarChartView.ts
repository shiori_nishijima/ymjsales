/// <reference path="../define.d.ts"/>
/// <reference path="../jquery.d.ts"/>
/// <reference path="baseBarChartView.ts"/>

class BaseSosikiBaseBarChartView extends BaseBarChartView {

    protected _data = null;
    protected condition = { year: null, month: null, div1: null, div2: null, div3: null };
    protected layer = 0;
    protected layerConditions = [];
    protected layerNames = [];

    initializeView() {
        $.yrcmv('linkSelect', {
            elements: [$('#largeClass2'), $('#middleClass')],
        });

        $('#largeClass2').on('change', () => {
            this.draw(true);
        });
        $('#middleClass').on('change', () => {
            this.draw(true);
        });
        $('#year').on('change', () => {
            this.getData(this.condition);
        });
        $('#month').on('change', () => {
            this.getData(this.condition);
        });

        $('input[name=resultType]').on('change', (e) => {
            // ネットは粗利だけ
            if ($(e.target).val() == '03') {
                $('#net-radio-items').show();
            } else {
                $('#net-radio-items').hide();
            }
            this.draw(true);
        });
        // 初期表示は隠す
        $('#net-radio-items').hide();
        $('input[name=net]').on('change', () => {
            this.draw(true);
        });

        $('#showPrevious').css('visibility', 'hidden');
        $('#showPrevious').on('click', () => {
            var layerCondition = this.layerConditions[--this.layer];
            this.getData(layerCondition);
        });

        $(window).on('resize', () => {
            this.draw(false);
        });

        // 初期表示で自動でグラフを表示
        this.getData({});
    }

    getUrl(): string { return ''; }

    getData(condition) {
        $('input').prop('readonly', true);
        $('select option:not(:selected)').prop('disabled', true);
        $.yrcmv('showLoadingView');

        this.condition = {
            year: $('#year').val(),
            month: $('#month').val(),
            div1: condition.div1,
            div2: condition.div2,
            div3: condition.div3,
        };

        $.ajax({
            type: 'POST',
            url: this.getUrl(),
            contentType: 'application/json',
            data: JSON.stringify(this.condition),
        }).done((json, statusText, jqXHR) => {

            $.yrcmv('responseCallback', {
                jqXHR: jqXHR
            });
            this._data = json;
            this.draw(true);
            // 使用できなくしたものを解除
            $('input').prop('readonly', false);
            $('select option:not(:selected)').prop('disabled', false);
        }).fail((jqXHR, statusText, errorThrown) => {
            $.yrcmv('responseCallback', {
                jqXHR: jqXHR
            });
        });
    }

    /**
     * 計算、分類、品種、本数金額粗利の設定に応じて計算
     */
    calc(data): number {
        // 分類
        var largeClass2 = $('#largeClass2').val();
        var middleClass = $('#middleClass').val();
        var resultType = $('input[name=resultType]:checked').val();;
        var arr = [];
        // 大分類
        /**
        * ①夏
        * →上記中分類のナツ系 + OR/ID
        * ②スノー
        * →上記中分類のスノー系
        * ③チフ
        * →上記中分類のチフ
        * ④関連用品
        * →上記中分類のその他
        * ⑤整備作業料 + 廃棄品処理料
        * →上記中分類の作業
        */
        if (largeClass2 == 'all') {
            arr.push(data['PCR_SUMMER']);
            arr.push(data['VAN_SUMMER']);
            arr.push(data['LT_SUMMER']);
            arr.push(data['TB_SUMMER']);
            arr.push(data['PCB']);
            arr.push(data['ORID']);
            arr.push(data['PCR_PURE_SNOW']);
            arr.push(data['VAN_PURE_SNOW']);
            arr.push(data['LT_SNOW']);
            arr.push(data['TB_SNOW']);
            // チフ、その他、作業、リトレッドに本数はあるが、本数の合計には加算しないので除外
            if (resultType != '01') {
                arr.push(data['TIFU']);
                arr.push(data['OTHER']);
                arr.push(data['WORK']);
                arr.push(data['LT_RETREAD_SUMMER']);
                arr.push(data['TB_RETREAD_SUMMER']);
                arr.push(data['TIRE_RETREAD_SUMMER']);
                arr.push(data['LT_RETREAD_SNOW']);
                arr.push(data['TB_RETREAD_SNOW']);
            }
        }
        // 夏
        else if (middleClass == 'all') {

            if (largeClass2 == '01') {
                arr.push(data['PCR_SUMMER']);
                arr.push(data['VAN_SUMMER']);
                arr.push(data['LT_SUMMER']);
                arr.push(data['TB_SUMMER']);
                arr.push(data['PCB']);
                arr.push(data['ORID']);
            }
            // スノー
            else if (largeClass2 == '02') {
                arr.push(data['PCR_PURE_SNOW']);
                arr.push(data['VAN_PURE_SNOW']);
                arr.push(data['LT_SNOW']);
                arr.push(data['TB_SNOW']);
            }
            // チフ
            else if (largeClass2 == '03') {
                arr.push(data['TIFU']);
            }
            // 関連用品
            else if (largeClass2 == '04') {
                arr.push(data['OTHER']);
            }
            // 整備作業料
            else if (largeClass2 == '05') {
                arr.push(data['WORK']);
            }
            // リトレッドナツ
            else if (largeClass2 == '06') {
                arr.push(data['LT_RETREAD_SUMMER']);
                arr.push(data['TB_RETREAD_SUMMER']);
                arr.push(data['TIRE_RETREAD_SUMMER']);
            }
            // リトレッドスノー
            else if (largeClass2 == '07') {
                arr.push(data['LT_RETREAD_SNOW']);
                arr.push(data['TB_RETREAD_SNOW']);
            }
        }
        // 中分類
        else {
            arr.push(data[middleClass]);
        }

        var result = 0;
        for (var i = 0; i < arr.length; i++) {
            var o = arr[i];
            var value = 0;
            // 本数
            if (resultType == '01') {
                result = result + o.number;
            }
            // 金額
            else if (resultType == '02') {
                result = result + o.sales;
            }
            // 粗利
            else {
                var net = $('input[name=net]:checked').val();
                // 本社ネット
                if (net == '01') {
                    result = result + o.hqMargin;
                } else {
                    result = result + o.margin;
                }
            }
        }
        return result;
    }

}
