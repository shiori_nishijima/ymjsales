/// <reference path="../jquery.d.ts"/>
/// <reference path="../define.d.ts"/>
/// <reference path="baseSosikiBaseBarChartView.ts"/>

class ChartBudgetPerformanceComparisonView extends BaseSosikiBaseBarChartView {

    getUrl() {
        return 'chartBudgetPerformanceComparisonData';
    }

    draw(animation) {
        $.yrcmv('showLoadingView');
        if (this._data == null) {
            return;
        }

        $('#d3').empty();

        var data = [];
        var scale = {
            x: {
                min: 0,
                max: this._data.data.length
            },
            y: {
                min: 0,
                max: 0
            },
            rate: {
                min: 0,
                max: 0,
            }
        };
        for (var i = 0; i < this._data.data.length; i++) {
            var d = this._data.data[i];
            var resultVal = this.calc(d.results);
            var planVal = this.calc(d.plans);

            // 実績と計画がないものは表示しない
            if (resultVal == 0 && planVal == 0) {
                continue;
            }

            var lastYearResultVal = this.calc(d.lastYeraResults);
            var rate = $.yrcmv('toPercentage', {
                val1: resultVal,
                val2: planVal
            });
            var elongation = $.yrcmv('toPercentage', {
                val1: resultVal - lastYearResultVal,
                val2: lastYearResultVal
            });

            var resultType = $('input[name=resultType]:checked').val();
            // 本数以外は1000単位
            resultVal = resultType == '01' ? resultVal : $.yrcmv('toK', {
                val: resultVal
            });
            planVal = resultType == '01' ? planVal : $.yrcmv('toK', {
                val: planVal
            });
            scale.y.max = Math.max(Math.max(scale.y.max, resultVal), planVal);
            scale.y.min = Math.min(Math.min(scale.y.min, resultVal), planVal);
            scale.rate.max = Math.max(scale.rate.max, rate);
            scale.rate.min = Math.min(scale.rate.min, rate);
            data.push({
                x: i,
                y: {
                    result: resultVal,
                    plan: planVal,
                    rate: rate,
                    elongation: elongation,
                },
                div1: d.div1Cd,
                div2: d.div2Cd,
                div3: d.div3Cd,
                tanto: d.tanto,
                xLabel: d.name,
            });
        }

        // y軸に少し余裕を持たせるために少しMAX値を大きくする
        scale.y.max = scale.y.max * 1.1;
        scale.rate.max = scale.rate.max * 1.05;
        if (scale.y.min < 0) {
            scale.y.min = scale.y.min * 1.1;
        }
        if (scale.rate.min < 0) {
            scale.rate.min = scale.rate.min * 1.05;
        }

        // グラフのmarginや高さの設定
        var margin = {
            top: 50,
            right: 50,
            bottom: 70,
            left: 100
        };

        var legendSize = {
            width: 200,
            height: 50,
        }
        var svgWidth = $('#d3').width() - $.yrcmv('calcScrollbarWidth');
        var width = svgWidth - margin.left - margin.right;
        var height = 500 - margin.top - margin.bottom;

        // svgの定義
        var svg = this.createSvg(width, height, margin);
        var chartSvg = this.createChartSvg(svg, margin)

        this.drawLegend([{
            label: '計画',
            class: 'plan'
        }, {
                label: '実績',
                class: 'result'
            }, {
                label: '達成率',
                class: 'rate'
            }], legendSize, svgWidth, svg);

        // スケールと出力レンジの定義
        // http://blog.livedoor.jp/kamikaze_cyclone/archives/34197135.html
        //https://github.com/mbostock/d3/wiki/Ordinal-Scales#ordinal_rangeRoundBands
        var x = this.createScaleX(data, width);
        var y = this.createScaleY([scale.y.min, scale.y.max], height);
        var yRate = this.createScaleY([scale.rate.min, scale.rate.max], height);

        // 軸の定義
        var xAxis = this.drawAxisX(x, chartSvg, height);
        var resultType = $('input[name=resultType]:checked').val();

        var yAxis = this.drawAxisY(y, true, chartSvg, width, $('label[for=resultType_' + resultType + ']').html());
        // 小数点は出さないように
        if (scale.y.max < 9) {
            yAxis.ticks(scale.y.max);
        }
        var yRateAxis = this.drawAxisY(yRate, false, chartSvg, width, '達成率');

        // －値の時の0値のライン
        var zeroLine = this.drawZeroLine(chartSvg, x, y, scale.y, width);

        // 予算グラフ
        // アニメーションさせる
        // http://wonderpla.net/blog/engineer/D3js_Animation/
        var planChart = this.drawChart(animation, chartSvg, data, x, y, scale.y, height, 'plan', this.getPlan, 2, 0);

        // 実績グラフを表示
        var resultChart = this.drawChart(animation, chartSvg, data, x, y, scale.y, height, 'result', this.getResult, 2, 1);

        //達成率
        var rateChart = this.draUpperLine(animation, chartSvg, data, x, yRate, 5, 'rate', this.getRate);

        // 達成率の数値 
        var rateChartString = this.drawChartString(animation, chartSvg, data, x, yRate, 'rateString',
            this.getRate, (num) => { return num == 0 ? '' : this.formatRate(num) });

        // ドリルダウン用の透明 
        var drillDown = this.drawDrillDown(chartSvg, data, 'drillDown', x, height, (d, i) => {
            if (!d.tanto) {
                this.layerConditions[this.layer] = this.condition;
                this.layer++;
                this.layerNames[this.layer] = d.xLabel;
                this.getData(d);
            }
        });

        var chartTitle = this.drawChartTitle(svg, margin, this.layerNames[this.layer]);

        // テーブルの作成
        var $tableContainer = this.drawTable(data);
        $tableContainer.find('#labelTitle').html(this._data.div.name);

        // 描画後に表示を切り替えたいのでここで処理をする
        if (this.layer == 0) {
            $('#showPrevious').css('visibility', 'hidden');
        } else {
            $('#showPrevious').css('visibility', 'visible');
        }

        $.yrcmv('hideLoadingView');
    }

}

$(() => {
    new ChartBudgetPerformanceComparisonView().initializeView();
});