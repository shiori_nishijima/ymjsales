/// <reference path="../jquery.d.ts"/>
/// <reference path="../d3.d.ts"/>
/// <reference path="baseChartView.ts"/>

class BaseBarChartView extends BaseChartView {

    draw(animation) { }
    
    /** X軸用のスケールの作成 数字はグラフ感の幅とかの調整 */
    createScaleX(data, width): any {
        return d3.scale.ordinal().domain(data.map((d) => {
            return d.xLabel;
        })).rangeRoundBands([0, width], 0.4, 0.2);
    }

    /** Y軸用のスケールの作成  */
    createScaleY(domain, height): any {
        return d3.scale.linear().domain(domain).range([height, 0]);
    }

    /** X軸の描画  */
    drawAxisX(x, chartSvg, height): any {
        var xAxis = d3.svg.axis().scale(x).orient('bottom');
        // x軸をsvgに表示
        chartSvg.append('g').attr('class', 'x axis').attr('transform', 'translate(0,' + height + ')').call(xAxis);
        // 軸ラベルを斜めにする
        chartSvg.selectAll('.x.axis text').attr('transform', function(d) {
            var translateY = Math.sin(30 * (Math.PI / 180)) * this.getBBox().width / 2;
            var translateX = Math.cos(30 * (Math.PI / 180)) * this.getBBox().width / 2;
            return 'translate(' + (-translateX) + ',' + translateY + ')rotate(-30)';
        });
        return xAxis;
    }

    /** Y軸の描画  */
    drawAxisY(y, left, chartSvg, width, title): any {
        var yAxis = d3.svg.axis().scale(y).orient(left ? 'left' : 'right');
        // Y軸 2軸グラフで、右側にY軸表示する場合はleftにfalse
        if (left) {
            chartSvg.append('g').attr('class', 'y axis').call(yAxis).append('text').attr('dy', '-0.5em').attr('dx',
                '0em').style('text-anchor', 'end').text(title);
        } else {
            chartSvg.append('g').attr('class', 'y axis').attr('transform', 'translate(' + width + ' ,0)').call(
                yAxis).append('text').attr('dy', '-0.5em').attr('dx', '0em').style('text-anchor', 'start')
                .text(title);
        }
        return yAxis;
    }

    /** グラフ用のSVG枠の作成 */
    createSvg(width, height, margin): any {
        return d3.select('#d3').append('svg').attr('width', width + margin.left + margin.right).attr('height',
            height + margin.top + margin.bottom);
    }

    /** グラフ用のgroupの追加 MARGINは縦軸、横軸の文字表示したときに隠れないための値 */
    createChartSvg(svg, margin): any {
        return svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
    }

    /** グラフのタイトル描画  */
    drawChartTitle(svg, margin, title): any {
        var text = svg.append('g').attr('class', 'title').attr('transform', 'translate(' + 20 + ',' + 20 + ')').append('text');
        return title ? text.text(title) : text;
    }

    /** chartLengthグラフの数（実績・計画のようにくっつけて表示する場合2をいれる） chartSeq くっつけて表示するグラフの順番  */
    drawChart(animation, chartSvg, data, x, y, scale, height, css, getData, chartLength, chartSeq): any {

        var chart = chartSvg.append('g').selectAll('.' + css).data(data).enter().append('rect').attr('class', css).attr(
            'x', (d) => {
                // グラフの数に応じて位置を調整
                var barWidth = x.rangeBand() / chartLength;
                return data.length == 1 ? x(d.xLabel) + x.rangeBand() / 4 + (barWidth / 2 * (chartSeq)) : x(d.xLabel) + (barWidth * chartSeq)
            })
        // グラフが1つしかないと幅が広すぎるので狭くする
            .attr('width', x.rangeBand() / (data.length == 1 ? 2 * chartLength : chartLength));

        // アニメーションさせる
        // ｰ値があるときにｰ値は下に行くように調整
        // http://wonderpla.net/blog/engineer/D3js_Animation/
        if (animation) {
            // －値がある場合は0を基準に上下にグラフを描画
            if (scale.min < 0) {
                chart.attr('y', (d) => {
                    // 0基準で考える
                    return y(0);
                }).attr('height', (d, i) => {
                    return 0;
                }).transition().delay((d, i) => {
                    return i * 100;
                }).duration(500).attr('y', (d) => {
                    return getData(d) < 0 ? y(0) : y(getData(d));
                }).attr('height', (d, i) => {
                    var h = 0;
                    if (getData(d) < 0) {
                        h = height - y(0) - (height - y(getData(d)));
                    } else {
                        h = (height - y(getData(d))) - (height - y(0));
                    }
                    return h < 0 ? 0 : h;
                });
            } else {
                chart.attr('y', (d) => {
                    return height;
                }).attr('height', (d, i) => {
                    return 0;
                }).transition().delay((d, i) => {
                    return i * 100;
                }).duration(500).attr('y', (d) => {
                    return y(getData(d));
                }).attr('height', (d, i) => {
                    return height - y(getData(d));
                });
            }

        } else {

            if (scale.min < 0) {
                chart.attr('y', (d) => {
                    return getData(d) < 0 ? y(0) : y(getData(d));
                }).attr('height', (d, i) => {
                    var h = 0;
                    if (getData(d) < 0) {
                        h = height - y(0) - (height - y(getData(d)));
                    } else {
                        h = (height - y(getData(d))) - (height - y(0));
                    }
                    return h < 0 ? 0 : h;
                });
            } else {
                chart.attr('y', (d) => {
                    return y(getData(d));
                }).attr('height', (d, i) => {
                    return height - y(getData(d));
                });
            }
        }

        return chart;
    }

    /** ゼロ値の線を引く */
    drawZeroLine(chartSvg, x, y, scale, width) {
        var svg = chartSvg.append('g');

        // マイナスがないときは対象外
        if (scale.min < 0) {

            var linePoint = [[0, y(0)], [width, y(0)]];

            // lineの設定。
            var line = d3.svg.line()
                .x(function(d) { return d[0]; })
                .y(function(d) { return d[1]; })
                .interpolate('cardinal'); // 線の形を決める。
 
            svg.append('path')
                .datum(linePoint)
                .attr('class', 'zeroLine')
                .attr('d', line); // 上で作ったlineを入れて、ラインpathを作る。
        }
        return svg;
    }

    /** グラフの頂点に文字を書く  */
    drawChartString(animation, chartSvg, data, x, y, css, getData, format): any {

        var chartString = chartSvg.append('g').selectAll('.' + css).data(data).enter().append('text').attr('x',
            (d) => {
                return x(d.xLabel) + x.rangeBand() / 2;
            }).attr('width', x.rangeBand() / (data.length == 1 ? 2 : 1));

        chartString.text((d) => {
            return format(getData(d));
        });
        // センタリング
        chartString.style('text-anchor', 'middle');

        if (animation) {

            chartString.attr('y', (d) => {
                return 0;
            }).transition().delay((d, i) => {
                return i * 100;
            }).duration(500).attr('y', (d) => {
                return y(getData(d)) - 10;
            });
        } else {
            chartString.attr('y', (d) => {
                return y(getData(d)) - 10;
            });
        }
        return chartString;
    }

    /** drillDown用にクリック対象の透明なSVGの作成  */
    drawDrillDown(chartSvg, data, css, x, height, fn): any {
        var drill = chartSvg.append('g').selectAll('.' + css).data(data).enter().append('rect').attr('class', css)
            .attr('x', (d) => {
                return x(d.xLabel) + (data.length == 1 ? (x.rangeBand() / 4) : 0);
            }).attr('width', x.rangeBand() / (data.length == 1 ? 2 : 1)).attr('height', height).attr('y', (d) => {
                return 0;
            });
        drill.on('click', fn);
        return drill;
    }

    formatRate(num): string {
        return (num == 0 ? '0' : d3.format(',.1f')(num)) + '%';
    }

    formatRateNoSymbol(num): string {
        return (num == 0 ? '0' : d3.format(',.1f')(num)) + '';
    }

    formatNum(num): string {
        return d3.format(',')(num);
    }

    /** データのテーブルを描画 これはTableを追加するだけ  */
    drawTable(data): any {
        var $tableContainer = $('#table');
        $tableContainer.empty();
        var $table = $('<table id="dataTable"><thead><tr><th style="width: 300px;" id="labelTitle"></th><th style="width: 150px;">計画</th><th style="width: 150px;">実績</th><th style="width: 80px;">達成率</th><th style="width: 80px;">伸長率</th></tr></thead><tbody></tbody></table>');

        var $tbody = $table.find('tbody');

        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            var $tr = $('<tr><td>' + d.xLabel + '</td><td class="number">' + this.formatNum(d.y.plan)
                + '</td><td class="number">' + this.formatNum(d.y.result) + '</td><td class="number">'
                + this.formatRateNoSymbol(d.y.rate) + '</td><td class="number">' + this.formatRateNoSymbol(d.y.elongation)
                + '</td></tr>');
            $tbody.append($tr);
        }

        $tableContainer.append($table);
        //$tableContainer.fixTable({
        //    autoWidth : false,
        //    fixColumn : 0,
        //});

        return $tableContainer;
    }

    getRate(d): number {
        return d.y.rate;
    }

    getResult(d): number {
        return d.y.result;
    }

    getPlan(d): number {
        return d.y.plan;
    }

    
    /**     
     * 凡例の描画     
     */
    drawLegend(legend, size, svgWidth, chartSvg) {
        var width = size.width;
        var height = size.heigth;
        var legendSize = 10;

        // svgの定義
        var svg = chartSvg.append('g');

        var x = d3.scale.ordinal().domain(legend.map((d) => {
            return d.label;
        })).rangeRoundBands([0, width]);
        var axis = d3.svg.axis().scale(x).orient('bottom').tickSize(0, 0);
        // 凡例用ラベル
        svg.append('g').attr('class', 'legendLabel axis').attr('transform', (label) => {

            return 'translate(' + (legendSize / 2) + ' ,' + legendSize + ')'
        }).call(axis);
        svg.selectAll('.legend').data(legend).enter().append('rect').attr('y', 0).attr('x', (d, i) => {
            return x(d.label) + x.rangeBand() / 2;
        }).attr('width', legendSize).attr('height', legendSize).attr('class', (d) => {
            return d.class;
        });

        svg.attr('transform', function() {
            return 'translate(' + (svgWidth / 2 - this.getBBox().width / 2) + ',' + 0 + ')'
        });

    }

    /* 値の上限にラインを引く */
    draUpperLine(animation, chartSvg, data, x, y, height, css, getData): any {

        var chart = chartSvg.selectAll('.' + css).data(data).enter().append('rect').attr('class', css).attr(
            'x', (d) => {
                // グラフが一つの場合幅を狭く
                return x(d.xLabel) + (data.length == 1 ? (x.rangeBand() / 4) : 0);
            }).attr('width', x.rangeBand() / (data.length == 1 ? 2 : 1)).attr('height', (d) => {
                if (getData(d) != 0) {
                    return height;
                } else {
                    return 0;
                }
            });

        if (animation) {

            chart.attr('y', (d) => {
                return 0;
            }).transition().delay((d, i) => {
                return i * 100;
            }).duration(500).attr('y', (d) => {
                return y(getData(d));
            });
        } else {
            chart.attr('y', (d) => {
                return y(getData(d));
            });
        }

        return chart;
    }


}
