/// <reference path="../jquery.d.ts"/>
/// <reference path="../define.d.ts"/>
/// <reference path="baseBarChartView.ts"/>

interface Window {
    largeClass2Map: any;
    middleClassMap: any;
}

class ChartKindAchievementRateView extends BaseBarChartView {

    private _data = null;
    private largeClass2 = true;
    private middleClass = null;
    private forceEmpty = false;

    initializeView() {


        $.yrcmv('linkSelect', {
            elements: [$('#largeClass2'), $('#middleClass')],
        });
        $('#sosoki').yrcmv('div', {
            reset: false,
            year: $('#year').val(),
            month: $('#month').val()
        });


        $('#year').on('change', () => {

            $('#sosoki').yrcmv('div', {
                reset: false,
                year: $('#year').val(),
                month: $('#month').val(),
                onComplete: () => {
                    this.getData();
                },
                onEmpty: () => {
                    this.forceEmpty = true;
                },
                onNotEmpty: () => {
                    this.forceEmpty = false;
                },
            });
        });
        $('#month').on('change', () => {
            $('#sosoki').yrcmv('div', {
                reset: false,
                year: $('#year').val(),
                month: $('#month').val(),
                onComplete: () => {
                    this.getData();
                },
                onEmpty: () => {
                    this.forceEmpty = true;
                },
                onNotEmpty: () => {
                    this.forceEmpty = false;
                },
            });
        });
        $('#sosoki').on('change', '#div1', () => {
            this.getData();
        });
        $('#sosoki').on('change', '#div2', () => {
            this.getData();
        });
        $('#sosoki').on('change', '#div3', () => {
            this.getData();
        });
        $('#sosoki').on('change', '#tanto', () => {
            this.getData();
        });
        $('input[name=resultType]').on('change', (e) => {
            // ネットは粗利だけ
            if ($(e.target).val() == '03') {
                $('#net-radio-items').show();
            } else {
                $('#net-radio-items').hide();
            }
            this.draw(true);
        });
        // 初期表示は隠す
        $('#net-radio-items').hide();
        $('input[name=net]').on('change', () => {
            this.draw(true);
        });
        $('#showPrevious').css('visibility', 'hidden');
        $('#showPrevious').on('click', () => {
            this.largeClass2 = true;
            this.middleClass = null;
            this.getData();
        });

        $(window).on('resize', () => {
            this.draw(false);
        });

        // 初期表示で自動でグラフを表示
        this.getData();
    }


    getData() {
        $('input').prop('readonly', true);
        $('select option:not(:selected)').prop('disabled', true);
        $.yrcmv('showLoadingView');

        var condition = {
            year: $('#year').val(),
            month: $('#month').val(),
            div1: $('#div1').val(),
            div2: $('#div2').val(),
            div3: $('#div3').val(),
            tanto: $('#tanto').val(),
            forceEmpty: this.forceEmpty,
        };

        $.ajax({
            type: 'POST',
            url: 'chartKindAchievementRateData',
            contentType: 'application/json',
            data: JSON.stringify(condition),
        }).done((json, statusText, jqXHR) => {

            $.yrcmv('responseCallback', {
                jqXHR: jqXHR
            });
            this._data = json;
            this.draw(true);
            $('input').prop('readonly', false);
            $('select option:not(:selected)').prop('disabled', false);
        }).fail((jqXHR, statusText, errorThrown) => {
            $.yrcmv('responseCallback', {
                jqXHR: jqXHR
            });
        });
    }

    draw(animation) {
        $.yrcmv('showLoadingView');

        if (this._data == null) {
            return;
        }

        $('#d3').empty();

        var data = [];
        var scale = {
            x: {
                min: 0,
                max: this._data.length
            },
            y: {
                min: 0,
                max: 0
            },
            rate: {
                min: 0,
                max: 0,
            }
        };

        $.each(this.largeClass2 ? window.largeClass2Map : window.middleClassMap[this.middleClass], (key, val) => {
            var resultVal = this.calc(this._data.results, key);
            var planVal = this.calc(this._data.plans, key);
            var lastYearResultVal = this.calc(this._data.lastYearResults, key);
            var rate = $.yrcmv('toPercentage', {
                val1: resultVal,
                val2: planVal
            });

            var elongation = $.yrcmv('toPercentage', {
                val1: resultVal - lastYearResultVal,
                val2: lastYearResultVal
            });

            var resultType = $('input[name=resultType]:checked').val();
            // 本数以外は1000単位
            resultVal = resultType == '01' ? resultVal : $.yrcmv('toK', {
                val: resultVal
            });
            planVal = resultType == '01' ? planVal : $.yrcmv('toK', {
                val: planVal
            });
            scale.y.max = Math.max(Math.max(scale.y.max, resultVal), planVal);
            scale.y.min = Math.min(Math.min(scale.y.min, resultVal), planVal);
            scale.rate.max = Math.max(scale.rate.max, rate);
            scale.rate.min = Math.min(scale.rate.min, rate);
            data.push({
                x: key,
                y: {
                    result: resultVal,
                    plan: planVal,
                    rate: rate,
                    elongation: elongation,
                },
                kind: key,
                xLabel: val,
            });

            // y軸に少し余裕を持たせるために少しMAX値を大きくする
            scale.y.max = scale.y.max * 1.1;
            scale.rate.max = scale.rate.max * 1.05;
            if (scale.y.min < 0) {
                scale.y.min = scale.y.min * 1.1;
            }
            if (scale.rate.min < 0) {
                scale.rate.min = scale.rate.min * 1.05;
            }
        });
        // グラフのmarginや高さの設定
        var margin = {
            top: 50,
            right: 50,
            bottom: 70,
            left: 100
        };

        var legendSize = {
            width: 200,
            height: 50,
        }
        var svgWidth = $('#d3').width() - $.yrcmv('calcScrollbarWidth');
        var width = svgWidth - margin.left - margin.right;
        var height = 500 - margin.top - margin.bottom;

        // svgの定義
        var svg = this.createSvg(width, height, margin);
        var chartSvg = this.createChartSvg(svg, margin)

        this.drawLegend([
            {
                label: '達成率',
                class: 'rate'
            }]
            , legendSize, svgWidth, svg);
 
        // スケールと出力レンジの定義
        // http://blog.livedoor.jp/kamikaze_cyclone/archives/34197135.html
        //https://github.com/mbostock/d3/wiki/Ordinal-Scales#ordinal_rangeRoundBands
        var x = this.createScaleX(data, width);
        var y = this.createScaleY([scale.rate.min, scale.rate.max], height);

        // 軸の定義
        var xAxis = this.drawAxisX(x, chartSvg, height);
        var yAxis = this.drawAxisY(y, true, chartSvg, width, '達成率');

        // －値の時の0値のライン
        var zeroLine = this.drawZeroLine(chartSvg, x, y, scale.rate, width);

        // グラフを表示
        var chart = this.drawChart(animation, chartSvg, data, x, y, scale.y, height, 'rate', this.getRate, 1, 0)

        // 達成率の数値 
        var rateChartString = this.drawChartString(animation, chartSvg, data, x, y, 'rateString', this.getRate, this.formatRate);

        // ドリルダウン用の透明 
        var drillDown = this.drawDrillDown(chartSvg, data, 'drillDown', x, height, (d, i) => {
            if (this.largeClass2) {
                this.middleClass = d.kind;
                this.largeClass2 = false;
                this.draw(true);
            }
        });

        // テーブルの作成
        var $tableContainer = this.drawTable(data);


        // 描画後に表示を切り替えたいのでここで処理をする
        if (this.largeClass2) {
            $('#showPrevious').css('visibility', 'hidden');
        } else {
            $('#showPrevious').css('visibility', 'visible');
        }

        $.yrcmv('hideLoadingView');
    }

    /**
    計算、分類、品種、本数金額粗利の設定に応じて計算
     */
    calc(data, kind): number {
        var arr = [];
        if (this.largeClass2) {
            if (kind == '01') {
                arr.push(data['PCR_SUMMER']);
                arr.push(data['VAN_SUMMER']);
                arr.push(data['LT_SUMMER']);
                arr.push(data['TB_SUMMER']);
                arr.push(data['PCB']);
                arr.push(data['ORID']);
            }
            // スノー
            else if (kind == '02') {
                arr.push(data['PCR_PURE_SNOW']);
                arr.push(data['VAN_PURE_SNOW']);
                arr.push(data['LT_SNOW']);
                arr.push(data['TB_SNOW']);
            }
            // チフ
            else if (kind == '03') {
                arr.push(data['TIFU']);
            }
            // 関連用品
            else if (kind == '04') {
                arr.push(data['OTHER']);
            }
            // 整備作業料
            else if (kind == '05') {
                arr.push(data['WORK']);
            }
            // リトレッドナツ
            else if (kind == '06') {
                arr.push(data['LT_RETREAD_SUMMER']);
                arr.push(data['TB_RETREAD_SUMMER']);
                arr.push(data['TIRE_RETREAD_SUMMER']);
            }
            // リトレッドスノー
            else if (kind == '07') {
                arr.push(data['LT_RETREAD_SNOW']);
                arr.push(data['TB_RETREAD_SNOW']);
            }
        }
        // 中分類
        else {
            arr.push(data[kind]);
        }

        var result = 0;
        var resultType = $('input[name=resultType]:checked').val();
        for (var i = 0; i < arr.length; i++) {
            var o = arr[i];
            var value = 0;
            // 本数
            if (resultType == '01') {
                result = result + (o ? o.number : 0);
            }
            // 金額
            else if (resultType == '02') {
                result = result + (o ? o.sales : 0);
            }
            // 粗利
            else {
                var net = $('input[name=net]:checked').val();
                // 本社ネット
                if (net == '01') {
                    result = result + (o ? o.hqMargin : 0);
                } else {
                    result = result + (o ? o.margin: 0);
                }
            }
        }
        return result;
    }


}

$(() => {
    new ChartKindAchievementRateView().initializeView();
});