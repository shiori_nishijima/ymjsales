(function($) {

    var inputExistenceRegex = '.+';
    var nonInputRegex = '^$';
    var datatablesLang_ja = {
            'sProcessing' : '処理中...',
            'sLengthMenu' : '_MENU_ 件表示',
            'sZeroRecords' : 'データはありません。',
            'sInfo' : ' _TOTAL_ 件中 _START_ から _END_ まで表示',
            'sInfoEmpty' : ' 0 件中 0 から 0 まで表示',
            'sInfoFiltered' : '（全 _MAX_ 件より抽出）',
            'sInfoPostFix' : '',
            'sSearch' : '検索:',
            'sUrl' : '',
            'oPaginate' : {
                'sFirst' : '先頭',
                'sPrevious' : '前',
                'sNext' : '次',
                'sLast' : '最終'
            },
            'NotEntered' : '未入力',
            'Entered' : '入力あり'
        };
    var datatablesLang_en = {
            'sProcessing' : 'Processing...',
            'sLengthMenu' : 'Display _MENU_ records per page',
            'sZeroRecords' : 'Nothing found - sorry',
            'sInfo' : ' Showing page _PAGE_ of _PAGES_',
            'sInfoEmpty' : ' No records available',
            'sInfoFiltered' : '(filtered from _MAX_ total records)',
            'sInfoPostFix' : '',
            'sSearch' : 'search:',
            'sUrl' : '',
            'oPaginate' : {
                'sFirst' : 'First',
                'sPrevious' : 'Prev',
                'sNext' : 'Next',
                'sLast' : 'Last'
            },
            'NotEntered' : 'Not entered',
            'Entered' : 'Entered'
        };
    var datatablesLang_th = {
            'sProcessing' : 'กำลังดำเนินการ...',
            'sLengthMenu' : 'แสดง _MENU_ แถว',
            'sZeroRecords' : 'ไม่พบข้อมูล',
            'sInfo' : ' แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว',
            'sInfoEmpty' : ' แสดง 0 ถึง 0 จาก 0 แถว',
            'sInfoFiltered' : '(กรองข้อมูล _MAX_ ทุกแถว)',
            'sInfoPostFix' : '',
            'sSearch' : 'ค้นหา: ',
            'sUrl' : '',
            'oPaginate' : {
                'sFirst' : 'หน้าแรก',
                'sPrevious' : 'ก่อนหน้า',
                'sNext' : 'ถัดไป',
                'sLast' : 'หน้าสุดท้าย'
            },
            'NotEntered' : 'ไม่มีข้อมูลใด ๆ',
            'Entered' : 'คุณได้ป้อน'
        };
    var datePickerOption_ja = {
            closeText : '確定',
            prevText : '前へ',
            nextText : '次へ',
            currentText : '今日',
            monthNames : [ '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月' ],
            monthNamesShort : [ '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月' ],
            dayNames : [ '日', '月', '火', '水', '木', '金', '土' ],
            dayNamesShort : [ '日', '月', '火', '水', '木', '金', '土' ],
            dayNamesMin : [ '日', '月', '火', '水', '木', '金', '土' ],
            weekHeader : 'Wk',
            yearSuffix : '年',
            clear : 'クリア'
        };
    var datePickerOption_en = {
            closeText : 'Done',
            prevText : 'Prev',
            nextText : 'Next',
            currentText : 'Today',
            monthNames : [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
            monthNamesShort : [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
            dayNames : [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
            dayNamesShort : [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ],
            dayNamesMin : [ 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa' ],
            weekHeader : 'Wk',
            yearSuffix : '',
            clear : 'Clear'
        };
    var datePickerOption_th = {
            closeText : 'ปิด',
            prevText : 'ย้อน',
            nextText : 'ถัดไป',
            currentText : 'วันนี้',
            monthNames : [ 'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม' ],
            monthNamesShort : [ 'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.' ],
            dayNames : [ 'อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์' ],
            dayNamesShort : [ 'อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.' ],
            dayNamesMin : [ 'อ', 'จ', 'อ', 'พ', 'พ', 'ศ', 'ส' ],
            weekHeader : 'Wk',
            yearSuffix : '',
            clear : 'ลบ'
        };

    var MSG_LOADING = "処理中です";

	// 多言語化対応しないぺージは日本語で表示
    if (typeof locale === 'undefined') {
        locale = 'ja'
    }

    var methods = {
        'datepicker' : function($e, options) {
            var maxDate = options.maxDate;
            var minDate = options.minDate;
            var defaultOptions = {
                dateFormat : 'yy/mm/dd',
                firstDay : 0,
                isRTL : false,
                showMonthAfterYear : true,
                changeMonth : true,
                changeYear : true,
                showButtonPanel : true,
                showClearButton : true,
                maxDate : maxDate,
                minDate : minDate,
                beforeShow : function(input) {
                }
            };
            var datePickerLang = locale == 'ja' ? datePickerOption_ja : (locale == 'en' ? datePickerOption_en : datePickerOption_th);
            options = $.extend(defaultOptions, options, datePickerLang);
            if (options.showClearButton) {
                options.beforeShow = function(input) {
                    setTimeout(
                            function() {
                                var buttonPane = $(input).datepicker("widget").find(".ui-datepicker-buttonpane");
                                var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button"></button>');
                                btn.text(datePickerLang.clear);
                                btn.unbind("click").bind("click", function() {
                                    $.datepicker._clearDate(input);
                                });
                                btn.appendTo(buttonPane);
                            }, 1);
                }
            }
            $e.attr('readonly', 'readonly');
            return $e.datepicker(options);
        },

        'linkSelect' : function(obj, options) {
            var defaultOptions = {
                elements : [],
                all : []
            };
            options = $.extend(defaultOptions, options);

            var tantoSelected = "notBelong";
            for (var i = 0; i < options.elements.length; i++) {
                var $parent = options.elements[i];
                var selected = $parent.find('[selected=selected]').parent().attr('label');
                if (i + 1 < options.elements.length) {
                    var $child = options.elements[i + 1];
                    $parent.data('child', $child);
                }
                _linkSelect($parent, options);
                if ($parent.data(selected)) {
                    $parent.append($parent.data(selected))
                }
                if ($parent.val() != "all") {
                    tantoSelected = $parent.val();
                }
            }

            for (var i = 0; i < options.all.length; i++) {
                _linkSelectSetup(options.all[i]);
                if (options.all[i].data(tantoSelected)) {
                    options.all[i].append(options.all[i].data(tantoSelected));
                } else {
                    options.all[i].append(options.all[i].data("all"));
                }
            }
        },

        'reflect' : function(obj, targets) {
            for (var i = 0; i < targets.length; i++) {
                targets[i].data('yrcmvCommon.reflect.index', i);
                targets[i].bind('change.yrcmvreflect', function() {
                    for (var j = 0; j < targets.length; j++) {
                        if ($(this).data('yrcmvCommon.reflect.index') != j) {
                            if (targets[j].attr('type') == 'checkbox') {
                                if ($(this).attr('checked')) {
                                    targets[j].attr('checked', 'checked');
                                } else {
                                    targets[j].removeAttr('checked');
                                }
                            } else {
                                targets[j].val($(this).val());
                            }
                        }
                    }
                });
            }
            return obj;
        },

        'responseCallback' : function(obj, target) {
            var statusCode = target.jqXHR.status;
            // spring securityでセッション管理をしているため、ajaxの結果としては最終的に200を返すようになる。
            // responseHeaderの値をみて判断する
            if (statusCode == 200) {
            	var header = target.jqXHR.getResponseHeader('session-status');
            	if (target.jqXHR.getResponseHeader('session-status') == 'timeout') {
            		location.href = 'timeout.html';
            	}
            } else if (statusCode == 401) {
        		location.href = 'timeout.html';
            } else if (statusCode == 404) {
        		location.href = '/ymjmv/404.jsp';
            } else if (statusCode == 500) {
        		location.href = '/ymjmv/500.jsp';
            }
            return obj;
        },

        'modeChange' : function(obj, $table) {
            var $button = $(obj);
            $button.find('input').on('change', function(e) {
                $button.find('input').each(function(index, e) {
                    var $e = $(e);
                    if ($e.prop('checked')) {
                        $table.columns().header().flatten().to$().each(function(index, elem) {
                            if ($e.val() === 'sort') {
                                _sortMode(elem);
                            } else {
                                _colReorderMode(elem);
                            }
                        });
                    }
                });
            });
        },

        'tabChange' : function(obj) {
            var $button = $(obj);
            _tabChange($button);
            $button.on('change', function(e) {
                _tabChange($button);
            });
        },

        'radioChange' : function(obj, options) {
            var defaultOptions = {
                callback : function($button) {
                },
            };
            options = $.extend(defaultOptions, options);
            var $button = $(obj);
            options.callback($button);
            $button.on('change', function(e) {
                options.callback($button);
            });
        },

        'footerFiter' : function(obj, $table) {
            // Apply the search
            $table
                .columns()
                .eq(0)
                .each(function(colIdx) {
                    $('input[type=text]', $table.column(colIdx).footer()).on('keyup change', function() {
                        // https://datatables.net/release-datatables/extensions/ColReorder/examples/col_filter.html
                        $table.column($(this).parent().index() + ':visible').search(this.value).draw();
                    });

                    $('input[type=button]', $table.column(colIdx).footer())
                        .each(function(index, e) {
                            $(e).click(function(e) {
                                // ボタン押下時のリストをテーブルに存在するデータから生成する
                                var $elem = $(e.currentTarget);
                                var data = new Array();
                                
                                var columnIndex = $elem.parent().index(); // 列順序は変えられるのでクリック時に取り直す
                                $table.columns(columnIndex + ':visible')
                                    .nodes()
                                    .flatten()
                                    .to$()
                                    .each(function(index, e) {
                                        var $e = $(e);
                                        var str = $.trim($e.data('sort'));
                                        if (str == "") {
                                            str = $.trim($e.text());
                                        }
                                        if (str) {
                                            _push(str, data);
                                        } else {
                                             var $div = $e.find('div');
                                             if ($div.length > 0) {
                                                 $div.each(function(index, e) {
                                                     var str = $.trim($(e).html());
                                                     _push(str, data);
                                                 });
                                             } else {
                                                 var str = $.trim($e.html());
                                                 _push(str, data);
                                             }
                                        }
                                    });

                                // 表示する要素作成時に既にチェック済み項目にはチェックを入れる
                                var filters = getFilterRegex($table, $table.column(columnIndex + ':visible').index());
                                var datatablesLang = locale == 'ja' ? datatablesLang_ja : (locale == 'en' ? datatablesLang_en : datatablesLang_th);
                                var $ul = $('<ul class="ColVis_collection"></ul>');
                                if ($elem.attr('data-insert-non-input-item')) {
                                    $ul.append('<li><label><input type="checkbox" value="' + nonInputRegex + '" '
                                        + ($.inArray('^$', filters) > -1 ? 'checked="checked"' : '') + '><span>' + datatablesLang["NotEntered"] + '</span></label></li>');
                                } else if ($elem.attr('data-insert-input-existence-item')) {
                                    $ul.append('<li><label><input type="checkbox" value="' + inputExistenceRegex + '" '
                                        + ($.inArray('.+', filters) > -1 ? 'checked="checked"' : '') + '><span>' + datatablesLang["Entered"] + '</span></label></li>');
                                }
                                $elem.parent().append($ul);
                                $ul.hide();
                                if (!$elem.attr('data-insert-input-existence-item')) {
                                    // 入力あり列は基本フリー項目なので通常の項目は生成しない
                                    for (var i = 0; i < data.length; i++) {
                                        $ul.append('<li><label><input type="checkbox" value="' + data[i] + '" ' + ($.inArray(data[i], filters) > -1 ? 'checked="checked"' : '') + '><span>' + data[i] + '</span></label></li>');
                                    }
                                }

                                $ul.find('input[type=checkbox]').on('change', function() {
                                    var regex = null;
                                    $ul.find('input[type=checkbox]').each(function(index, e) {
                                        var $e = $(e);
                                        if ($e.prop('checked')) {
                                            if (regex != null) {
                                                regex = regex.replace("\^(", "").replace(")\$", "");
                                                regex = regex + '|';
                                            } else {
                                                regex = '';
                                            }
                                            regex = '\^(' + regex + $e.val().replace("(", "\\(").replace(")", "\\)") + ')\$';
                                        }
                                    });
                                    if (regex == null) {
                                        regex = '';
                                    }
                                    // https://datatables.net/release-datatables/extensions/ColReorder/examples/col_filter.html
                                    $table.column(columnIndex + ':visible').search(regex, true, false).draw();
                                });
                                $ul.css({
                                    position : 'absolute',
                                    top : $elem.position().top - $ul.outerHeight(),
                                    left : $elem.position().left,
                                    opacity : 0
                                });
                                var $background = $('<div></div>')
                                    .addClass('ColVis_collectionBackground')
                                    .css('opacity', 0)
                                    .click(function() {
                                        $background.animate({ "opacity" : 0 }, 500, function() {
                                            $background.remove();
                                            $ul.remove()
                                        });
                                        $ul.animate({ "opacity" : 0 }, 500, function() { $ul.hide(); });
                                    });
                                document.body.appendChild($background[0]);
                                $background.animate({ "opacity" : 0.1 }, 500);
                                $ul.show();
                                $ul.animate({ "opacity" : 1 }, 500);
                            });
                        });
                });
        },


        /**
         * データテーブルの共通処理実装 オプションは上書き可能
         */
        'datatables' : function(table, options) {

            var defaultOptions = {
                    'language' : locale == 'ja' ? datatablesLang_ja : (locale == 'en' ? datatablesLang_en : datatablesLang_th),
                    'scrollY': 400,
                    'scrollX': true,
                    'stateSave': true,
                    'stateLoadParams': function (settings, data) {
                        // datatable表示時にフィルタ状態をリセットする
                        data.search.search = "";
                        var columns = data.columns;
                        if (columns) {
                            for (var i = 0; i < columns.length; i++) {
                                var column = columns[i];
                                column.search.search = "";
                            }
                        }
                    },
                    'initComplete' : function( settings, json ) {}
                };
            options = $.extend(defaultOptions, options);

            return $(table).DataTable(options);;
        },

        'isMobile' : function() {
            return _isMobile();
        },

        'isSmartPhone' : function() {
            return _isSmartPhone();
        },

        'isIpad' : function() {
            return _isIpad();
        },

        'isAndroid' : function() {
            return _isAndroid();
        },

        'isIphone' : function() {
            return _isIphone();
        },

        'sortNumber' : function($e, arr) {
            arr.sort(function(a, b) {
                return (parseInt(a) > parseInt(b)) ? 1 : -1;
            });
        },

        'displayMessage' : function($e, message) {
            _displayMessage(message)
        },

        'round' : function($e, options) {
            var defaultOptions = {
                scale : 0,
                val : 0,
            };
            options = $.extend(defaultOptions, options);
            return _round(options.val, options.scale);
        },

        'toK' : function($e, options) {
            var defaultOptions = {
                scale : 0,
                val : 0,
            };
            options = $.extend(defaultOptions, options);
            return _round(options.val / 1000, options.scale);
        },

        'toPercentage' : function($e, options) {
            var defaultOptions = {
                scale : 0,
                val1 : 0,
                val2 : 0,
            };

            options = $.extend(defaultOptions, options);

            if (options.val2 == 0) {
                return 0;
            }

            // IEEE 754により精度がおかしくなるため 少数無くして割る
            var result = Math.round(_round(options.val1 / options.val2, 3) * 1000)  / 10
            return result;
        },

        'calcScrollbarWidth' : function() {
            var $scrollDiv = $('<div style="width: 100px; height: 100px; overflow: scroll; position: absolute; top: -9999px;"></div>');
            $('body').append($scrollDiv);
            // Get the scrollbar width
            var scrollbarWidth = $scrollDiv.get(0).offsetWidth - $scrollDiv.get(0).clientWidth;
            // Delete the DIV
            $scrollDiv.remove();
            return scrollbarWidth;
        },
        
        'showLoadingView' : function(e, message) {
            _showLoadingView(message);
        },
        
        'hideLoadingView' : function() {
            _hideLoadingView();
        },

        'div' : function(e, arg) {
            _createDiv(e, 'divSelectItem/create', true, arg);
        },

        'divNoAll' : function(e, arg) {
            _createDiv(e, 'divSelectItem/createHideAllLabel', true, arg);
        },

        'divNoTanto' : function(e, arg) {
            _createDiv(e, 'divSelectItem/create', false, arg);
        },
        
        'divVisitList' : function(e, arg) {
            _createDiv(e, 'divSelectItem/createVisitList', true, arg);
        },

        'divOnlySalesOfficeTantoAll' : function(e, arg) {
            _createDiv(e, 'divSelectItem/createOnlySalesOfficeTantoAllLabel', true, arg);
        },

        'divSameLevelSalesOfficeTantoAll' : function(e, arg) {
            _createDiv(e, 'divSelectItem/createSameLevelSalesOfficeTantoAll', true, arg);
        },

    };

    /**
     * スケールにしたがって四捨五入。
     */
    function _round(val, scale) {
        scale = scale > 0 ? scale : 0;
        var newnumber = new Number(val+'').toFixed(parseInt(scale));
        return parseFloat(newnumber);
    }

    /**
     * モバイル端末かどうか iPhone、iPadまたはandroidに該当するかどうか。
     */
    function _isMobile() {
        return _isIphone() || _isIpad() || _isAndroid();
    }

    /**
     * スマートフォン端末かどうか iPhoneまたはandroidに該当するかどうか
     * 
     * @returns {Boolean}
     */
    function _isSmartPhone() {
        return _isIphone() || _isAndroid();
    }

    /**
     * iPadかどうか
     * 
     * @returns {Boolean}
     */
    function _isIpad() {
        return navigator.userAgent.indexOf("iPad") > -1;
    }

    /**
     * iPhoneかどうか
     * 
     * @returns {Boolean}
     */
    function _isIphone() {
        return navigator.userAgent.indexOf("iPhone") > -1;
    }

    /**
     * androidかどうか
     * 
     * @returns {Boolean}
     */
    function _isAndroid() {
        return navigator.userAgent.indexOf("Android") > -1;
    }

    /**
     * 列に対応するフィルタ正規表現または文字列を取得。
     * パイプで複数存在する場合は分割して配列で返却する。
     */
    function getFilterRegex($table, colIdx) {
        // 保持しているフィルタ正規表現を取得
        var filters = [];
        var savedState = $table.state();
        // 表示インデックスと実際のデータ保持インデックスを変換する
        var colReorderState = savedState['ColReorder'];
        if (colReorderState) {
        	var originalIndex = colReorderState[colIdx];
        	if (originalIndex) {
            	var columns = savedState['columns']
            	var data = columns[originalIndex];
            	var filterString = data.search.search;
            	filters = filterString.replace("\\(", "(").replace("\\)", ")").replace("\^(", "").replace(")\$", "").split('|');
        	}
        } else {
            // colReorderを使用していない場合はオリジナルのindexを使う
            var columns = savedState['columns']
            var data = columns[colIdx];
            var filterString = data.search.search;
            filters = filterString.replace("\\(", "(").replace("\\)", ")").replace("\^(", "").replace(")\$", "").split('|');
        }
        return filters;
    }

    function _push(str, array) {
        if (str) {
            if ($.inArray(str, array) < 0) {
                array.push(str);
            }
        }
    }

    /**
     * メッセージを表示する。
     * @param message
     * @param during 表示時間
     * @param sleep スリープ時間
     */
    function _displayMessage(message, during, sleep) {
        var sleepTime = sleep ? sleep : 0;
        setTimeout(_displayMessageInternal(message, during), sleepTime);
    }

    /**
     * メッセージを表示する。
     * @param message
     * @param during 表示時間
     */
    function _displayMessageInternal(message, during) {
        return function() {
            $('#dispMessage').remove();
            $('#loadingMsgView').remove();
            $('<div id="dispMessage" class="popup-message" style="width:320px;height:100px;text-align:center;z-index:999999999"><p style="font-weight:bold;">' + message + '</p></div>')
            .css({
                display:'block',
                opacity: 0.9,
                top: $(window).height() / 2 - 50,
                left: $(window).width() / 2 - 160
             })
            .appendTo('body')
            .delay(during ? during : 2000)
            .fadeOut(400, function() {
                $(this).remove();
            });
        };
    }

    function _sortMode(tableHead) {
        tableHead.removeEventListener('touchstart', _modeChangeHandler, true);
        tableHead.removeEventListener('touchmove', _modeChangeHandler, true);
        tableHead.removeEventListener('touchend', _modeChangeHandler, true);
        tableHead.removeEventListener('touchcancel', _modeChangeHandler, true);
    }

    function _colReorderMode(tableHead) {
        _sortMode(tableHead);
        // iPad対応
        tableHead.addEventListener('touchstart', _modeChangeHandler, true);
        tableHead.addEventListener('touchmove', _modeChangeHandler, true);
        tableHead.addEventListener('touchend', _modeChangeHandler, true);
        tableHead.addEventListener('touchcancel', _modeChangeHandler, true);
    }

    function _modeChangeHandler(event) {
        var touches = event.changedTouches;
        var first = touches[0];
        var type = '';

        switch (event.type) {
        case 'touchstart':
            type = 'mousedown';
            break;
        case 'touchmove':
            type = 'mousemove';
            break;
        case 'touchend':
            type = 'mouseup';
            break;
        default:
            return;
        }
        var simulatedEvent = document.createEvent('MouseEvent');
        simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX,
                first.clientY, false, false, false, false, 0/* left */, null);

        first.target.dispatchEvent(simulatedEvent);
        event.preventDefault();
    }

    function _linkSelectSetup($e) {
        // 子の内容をGroupごとに保存
        $e.children().each(function() {
            $e.data($(this).attr('label'), $(this).html());
        });
        // 中身を削除
        $e.empty();
    }

    function _linkSelect($parent, options) {
        var $child = $parent.data('child');
        if ($child) {
            // 一端退避
            var childVal = $child.val();
            _linkSelectSetup($child);
            $child.append($child.data($parent.val()));
            $child.val(childVal);
        }

        // 親をclickしたときのイベント
        $parent.change(function(e) {
            _changeSelect($(e.currentTarget), $parent.data('child'), options, true);
        });

//        for (var i = 0; i < all.length; i++) {
//            var $elem = all[i];
//            $parent.change(function(e) {
//                _changeSelect($(e.currentTarget), $elem, all, false);
//                
//                
//            });
//        }
    }

    function _changeSelect($parent, $target, options, isSpreadChild) {
        if ($target) {
            $target.empty();
            var parent = $parent.val() != 'all' ? $parent.val() : null;
            var data = $target.data(parent == null ? 'notBelong' : parent);
            if (data) {
                $target.append(data);
                $target.trigger('change');
            } else {
                $target.append($target.data('all'));
            }
            if (isSpreadChild) {
                _changeSelect($target, $target.data('child'), options, true);
            }

        } else {
            for (var i = 0; i < options.all.length; i++) {
                for (var j = options.elements.length - 1; j > -1; j--) {
                    var $elem = options.all[i];
                    if (options.elements[j].val() != 'all') {
                        _changeSelect(options.elements[j], $elem, options, false);
                        break;
                    }
                    if (j == 0) {
                        _changeSelect(options.elements[j], $elem, options, false);
                    }
                }
            }
        }
    }

    function _tabChange($button) {
        $button.find('input').each(function(index, e) {
            var $e = $(e);
            if ($e.prop('checked')) {
                $('div[data-tab]').each(function(index, tab) {
                    var $tab = $(tab);
                    if ($e.val() === 'all' || $e.val() === $(tab).attr('data-tab')) {
                        $tab.show();
                    } else {
                        $tab.hide();
                    }
                })
            }
        });
    }
    
    /**
     * ローディングを表示。
     */
    function _showLoadingView(message) {
        if ($("#loadingView").size() > 0) {
            return;
        }
        var displayMessage = message ? message : MSG_LOADING;
        var html = '<div id="loadingView" class="div-loader-area"><div class="div-loader"></div><div class="div-loader-message">' + displayMessage + '</div></div>';
        $('body').append($(html));
        $('#loadingView').click(function() { return false; });
    }
    

    /**
     * ローディングを削除。
     */
    function _hideLoadingView() {
        $("#loadingView").remove();
    }
    
    function _createDiv(e, url, createTanto, arg) {
        var reset = arg.reset;
        $.ajax({
            type : 'POST',
            url : url,
            contentType : 'application/json',
            data : JSON.stringify({date : arg.date, year : arg.year, month : arg.month, deleteDiv1All : arg.deleteDiv1All}),
        }).done(function(json, statusText, jqXHR) {
            $.yrcmv('responseCallback', {
                jqXHR : jqXHR
            });

            if (json.div1.length ==0 || json.div2.length ==0 || json.div3.length ==0 || json.tanto.length ==0) {
                if (arg.onEmpty) {
                    arg.onEmpty();
                }
            } else {
                if (arg.onNotEmpty) {
                    arg.onNotEmpty();
                } 
            }
            
            var $div1 = $('<select class="visitList-company-section" id="div1" name="div1" ></select>');
            $div1.on('change', function() {
                $('#tempDiv1').val($(this).val());
            });
            var $div2 = $('<select class="visitList-company-section" id="div2" name="div2" ></select>');
            $div2.on('change', function() {
                $('#tempDiv2').val($(this).val());
            });
            var $div3 = $('<select class="visitList-company-section" id="div3" name="div3" ></select>');
            $div3.on('change', function() {
                $('#tempDiv3').val($(this).val());
            });
            var $tanto = $('<select class="visitList-company-section" id="tanto" name="tanto" ></select>');
            $tanto.on('change', function() {
                $('#tempTanto').val($(this).val());
            });

            $('#div1').remove();
            $('#div2').remove();
            $('#div3').remove();

            $(e).append($div1);
            $(e).append($div2);
            $(e).append($div3);
            if (createTanto) {
                $('#tanto').remove();
                $(e).append($tanto);
            }


            for ( var i = 0; i < json.div1.length; i++) {
                var div1= json.div1[i];
                var $option = $('<option></option>');
                $option.val(div1.value);
                $option.html(div1.label);
                if (!reset && div1.value == $('#tempDiv1').val()) {
                    $option.attr('selected', 'selected');
                }
                $div1.append($option);
            }

            for (var i = 0; i < json.div2.length; i++) {
                var group = json.div2[i];
                var $optgroup = $('<optgroup></optgroup>');
                $optgroup.attr('label', group.label);
                $div2.append($optgroup);

                for (var j = 0; j < group.items.length; j++) {
                    var item= group.items[j];
                    var $option = $('<option></option>');
                    $option.val(item.value);
                    $option.html(item.label);
                    if (!reset && group.label == $('#tempDiv1').val() && item.value == $('#tempDiv2').val()) {
                        $option.attr('selected', 'selected');
                    }
                    $optgroup.append($option);
                }
            }

            for (var i = 0; i < json.div3.length; i++) {
                var group = json.div3[i];
                var $optgroup = $('<optgroup></optgroup>');
                $optgroup.attr('label', group.label);
                $div3.append($optgroup);
                
                for (var j = 0; j < group.items.length; j++) {
                    var item= group.items[j];
                    var $option = $('<option></option>');
                    $option.val(item.value);
                    $option.html(item.label);
                    if (!reset && group.label == $('#tempDiv2').val() && item.value == $('#tempDiv3').val()) {
                        $option.attr('selected', 'selected');
                    }
                    $optgroup.append($option);
                }
            }
            if (createTanto) { 
                for (var i = 0; i < json.tanto.length; i++) {
                    var group = json.tanto[i];
                    var $optgroup = $('<optgroup></optgroup>');
                    $optgroup.attr('label', group.label);
                    $tanto.append($optgroup);
                    
                    for (var j = 0; j < group.items.length; j++) {
                        var item= group.items[j];
                        var $option = $('<option></option>');
                        $option.val(item.value);
                        $option.html(item.label);
                        if (!reset && (group.label == $('#tempDiv3').val() || group.label == $('#tempDiv2').val() || group.label == $('#tempDiv1').val() || 'notBelong' == group.label) && item.value == $('#tempTanto').val()) {
                            $option.attr('selected', 'selected');
                        }
                        $optgroup.append($option);
                    }
                }
            }
            if (createTanto) { 
                $.yrcmv('linkSelect', {elements: [ $('#div1'), $('#div2'), $('#div3') ], all: [$('#tanto')]});
            } else {
                $.yrcmv('linkSelect', {elements : [ $('#div1'), $('#div2'), $('#div3') ]});
            }

            
            if (arg.onComplete) {
                arg.onComplete();
            }

        }).fail(function(jqXHR, statusText, errorThrown) {
            $.yrcmv('responseCallback', {
                jqXHR : jqXHR
            });
        });

    }

    $.yrcmv = $.fn.yrcmv = function(method, args) {
        var fn = methods[method];
        return fn(this, args);
    };

})(jQuery);
