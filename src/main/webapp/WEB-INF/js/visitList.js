/**
 * 訪問一覧用JS
 */
var visitComment = 'visitComment/';
var visitListRead = 'visitListRead/';
var visitListViewList = 'visitListViewList/';
var visitListMarketInfo = 'visitListMarketInfo/'
var divVisitList = 'divVisitList';
(function($) {
    $(document).ready(function() {
        
        /* ★★datatables適用前に、テーブル内のイベントを付与 ここから */
        // コメント表示押下時
        var currentCallId;
        var loginUserId;
        $('input[data-roll=displayComment]').click(function(e) {
            $('tr[data-roll=comments]').remove();
            var callId = $(e.currentTarget).data('display-comment');
            currentCallId = callId;
            $('[data-comment-call-id]').data('comment-call-id', callId);
            var techCallId = $(e.currentTarget).data('tech-call-id');
            techCallId = techCallId ? techCallId : null
            $('[data-comment-tech-call-id]').data('comment-tech-call-id', techCallId);
            var param = {
                'techCallId' : techCallId,
            };

            $.ajax({
                type : 'GET',
                data : param,
                url : visitComment + callId,
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                loginUserId = json.userId; // 画面表示時にログインユーザIDを保持
                var $div = $('#inline-content');
                $.colorbox({
                    inline : true,
                    href : '#inline-content'
                });

                $div.find('#visitUserFirstName').html(json.firstName);
                $div.find('#visitUserLastName').html(json.lastName);
                $div.find('#eigyoSosikiNm').html(json.eigyoSosikiNm);
                $div.find('#likeCount').html(json.likeCount);
                $div.find('#likeCount').data('like-count', json.likeCount); // ここで必ず数値を設定する
                if (json.isLike) {
                    $div.find('#unlike').show();
                    $div.find('#like').hide()
                } else {
                    $div.find('#unlike').hide();
                    $div.find('#like').show()
                }

                if (json.comments.length < 0) {
                    $('#commentSeparator').hide();
                } else {
                    $('#commentSeparator').show();
                    for (var i = 0; i < json.comments.length; i++) {
                        var o = json.comments[i];
                        $('#commentSeparator').after(createComment(false, o.id, o.tech, o.userId, o.date, o.lastName, o.firstName, o.eigyoSosikiNm, o.description));
                    }
                }
                $.colorbox.resize();
                $('#gmailLink').attr('href', 'https://mail.google.com/mail/?view=cm&fs=1&tf=1&body=' + json.gmailLink + '%26fp%3Dlink');

            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
            });
        });

        _addDisplayViewHistoryEvent($('button[data-roll=displayViewHistory]'));
        
        $('.download1').click(function() {
            var id = $(this).data('download-call-id');
            var techCallId = $(this).data('download-tech-call-id');
            $('#downloadTechCallId').val(techCallId);
            $('#doDownload1').val(id);
            $('#doDownload1').click();
        });
        $('.download2').click(function() {
            var id = $(this).data('download-call-id');
            var techCallId = $(this).data('download-tech-call-id');
            $('#downloadTechCallId').val(techCallId);
            $('#doDownload2').val(id);
            $('#doDownload2').click();
        });
        $('.download3').click(function() {
            var id = $(this).data('download-call-id');
            var techCallId = $(this).data('download-tech-call-id');
            $('#downloadTechCallId').val(techCallId);
            $('#doDownload3').val(id);
            $('#doDownload3').click();
        });
        
        
        $('input[data-roll=market-info]').on('click', function(e) {

            var callId = $(e.currentTarget).data('market-info-id');
            var techCallId = $(e.currentTarget).data('tech-call-id');

            $.ajax({
                type : 'GET',
                url : visitListMarketInfo + callId,
                data : {
                    techCallId : techCallId
                }
            }).done(function(json, statusText, jqXHR) {

                $.yrcmv('responseCallback', {
                    jqXHR : jqXHR
                });

                $.colorbox({
                    inline : true,
                    href : '#inline-content-market-info'
                });

                var container = $('#inline-content-market-info');

                container.find('#infoDiv').html(json.infoDivName);
                container.find('#performance').html(json.performanceName);
                container.find('#artNo').html(json.artNo);
                container.find('#usages').html(json.usagesName);
                container.find('#size').html(json.size);
                container.find('#roadSurface').html(json.roadSurfaceName);
                container.find('#pattern').html(json.pattern);
                container.find('#carType').html(json.carTypeName);
                container.find('#serial').html(json.serial);
                container.find('#carBodyShape').html(json.carBodyShapeName);
                container.find('#damagedPart').html(json.damagedPartName);
                container.find('#carMaker').html(json.carMakerName);
                container.find('#damagedType').html(json.damagedTypeName);
                container.find('#mountingTerm').html(json.mountingTerm != null ? json.mountingTerm : '');
                container.find('#damagedName').html(json.damagedNameName);
                container.find('#mileage').html(json.mileage != null ? json.mileage : '');
                container.find('#mileageUnit').html(json.mileageUnitName);

                $.colorbox.resize();
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', {
                    jqXHR : jqXHR
                });
            });
        });
        
        /* ★★datatables適用前に、テーブル内のイベントを付与 ここまで */

        // PC以外ではダウンロードを表示しない
        if ($.yrcmv('isMobile')) {
            $('.file-download').remove();
        }
        // 技サ訪問実績以外では作成者を表示しない
        if (!tech) {
        	$('.created-user').remove();
        }
        
        var columns =  [
            { 'searchable' : false, 'orderable' : false, 'width' : '20px' }, // 既読チェック
            { 'width' : '25px' }, // 訪問
            { 'width' : '300px' }, // 件名
            { 'width' : '200px' }, // 得意先名
            { 'width' : '100px' }, // 販路
            { 'width' : '180px' }, // 訪問日時
            { 'width' : '300px' }, // 内容
            { 'width' : '50px' }, // 関連先
            { 'width' : '60px' }, // ステータス
            { 'width' : '120px' }, // 訪問理由
            { 'width' : '90px' }, // 部門
            { 'width' : '90px' }, // 営業所
            { 'width' : '90px' }, // 担当者
            { 'width' : '130px' }, // 社内同行者
            { 'width' : '25px' }, // 予定数
            { 'width' : '25px' }, // 実績数
            { 'width' : '200px' }, // 情報区分
            { 'width' : '100px' }, // メーカー
            { 'width' : '30px' }, // 品種
            { 'width' : '50px' }, // カテゴリ
            { 'width' : '100px' }, // 感度
            { 'width' : '45px' }, // コメント未読
            { 'width' : '60px' }, // 注目マーク数
            { 'searchable' : false, 'orderable' : false, 'width' : '100px' }, // コメント表示
            { 'width' : '60px' }, // コメント入力
            { 'width' : '60px' }, // 閲覧履歴
        ];
        
        // PCの場合ダウンロード追加
        if (!($.yrcmv('isMobile'))) {
            columns.splice(23, 0, { 'width' : '60px' }); // 添付ファイル
        }
        // 技サ訪問実績の場合作成者を追加
        if (tech) {
        	columns.splice(13, 0, { 'width' : '90px' }); // 作成者
        }
        
        // テーブル内のDOMにイベントを付ける場合は、DataTableを作る前のDOMに対して作ること
        // DataTableを作ってからだと、Pagingをしたときに見えているものしかイベントがつかない
        var $table = $('table[data-roll=fixtable]').yrcmv('datatables', {
            'colReorder' : {
                'reorderCallback' : function() {
                    $('.ColVis_ShowAll').click();
                   $.visitList.colvis($table);
                } ,
            },
            'dom': '<"ColVis_wrapper"C>R<l<"modeChange_wrapper">f>rtip',
            'columns': columns
        });

        // 表示非表示
        $.visitList.colvis($table, true);
    
        $.yrcmv('footerFiter', $table);
        $('.modeChange').yrcmv('modeChange', $table);
    
        $('#maxDate').ready(minMaxDateflow);
        $('#minDate').ready(minMaxDateflow);
    
        function minMaxDateflow() {
            var maxDate = $('#maxDate').val();
            var minDate = $('#minDate').val();
            if (maxDate != null && minDate != null) {
                $('#dateFrom').yrcmv('datepicker', {showClearButton : false, maxDate : maxDate, minDate : minDate});
                $('#dateTo').yrcmv('datepicker', {maxDate : maxDate, minDate : minDate});
            }
        }
    
        $(document).bind('cbox_closed', function() {
            var $div = $('#inline-content');
            $div.find('#visitDateFrom').html('');
            $div.find('#visitDateTo').html('');
            $div.find('#name').html('');
            $div.find('#visitUserFirstName').html('');
            $div.find('#visitUserLastName').html('');
            $div.find('#detail').html('');
            $div.find('tr[data-roll=comments]').remove();
            $div.find('.error_message').remove();
            $.colorbox.resize();
        });

        $('#unlike').click(function(e) {
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            var $like = $('span[data-roll-like=' + callId + ']');
            var tech = techCallId ? true :false;
            $.ajax({
                type : 'DELETE',
                url : visitComment + 'like/' + callId +'/' + tech ,
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                var count = $('#likeCount').data('like-count');
                count--;
                $('#likeCount').data('like-count', count);
                $('#likeCount').html(count);
                $('#unlike').hide();
                $('#like').show();
                $like.html(count <= 0 ? '' : count);
                // datatablesに保持した値も書き換える
                $table.cell($like[0].parentNode).data($like[0].outerHTML);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
            });
        });
    
        $('#like').click(function(e) {
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            var $like = $('span[data-roll-like=' + callId + ']');
            $.ajax({
                type : 'POST',
                url : visitComment + 'like/' + callId,
                data : {
                    techCallId : techCallId,
                },
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                var count = $('#likeCount').data('like-count');
                count++;
                $('#likeCount').data('like-count', count);
                $('#likeCount').html(count);
                $('#unlike').show();
                $('#like').hide();
                $like.html(count);
                // datatablesに保持した値も書き換える
                $table.cell($like[0].parentNode).data($like[0].outerHTML);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
            });
        });
    
        $('#registerComment').click(function(e) {
            $('.error_message').remove();
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            $.ajax({
                type : 'POST',
                url : visitComment,
                contentType : 'application/json',
                dataType : 'json',
                data : JSON.stringify({
                    callId : callId,
                    comment : $('#commentEditor').val(),
                    techCallId : techCallId,
                }),
            }).done(
                function(json, statusText, jqXHR) {
                    commentResistdone(loginUserId, json, statusText, jqXHR, callId, $table, true);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                for (var i = 0; i < jqXHR.responseJSON.length; i++) {
                    $('#commentErrorMessage').append( $('<span class="error_message">' + jqXHR.responseJSON[i] + '</span>'));
                }
            });
        });
    
        $('#readComment').click(function(e) {
            // コメント確認済みにマーク押下時
            $('.error_message').remove();
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            $.ajax({
                type : 'POST',
                url : visitComment + 'read/' + callId,
                data : {
                    techCallId : techCallId,
                },
            }).done(function(json, statusText, jqXHR) {
                commentResistdone(loginUserId, json, statusText, jqXHR, callId, $table, false);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                for (var i = 0; i < jqXHR.responseJSON.length; i++) {
                    $('#commentErrorMessage').append( $('<span class="error_message">' + jqXHR.responseJSON[i] + '</span>'));
                }
            });
        });
        
        $('#toRead').click(function(e) {
            $('.error_message').remove();

            var calls = [];
            $('.dataTables_scrollBody input.readCallId[type=checkbox]:checked').each(function (index, e) {
                var o = {
                        id: $(e).val(),
                        callId:  $(e).data('tech-call-id'),
                }
                calls.push(o);
            });
            $.ajax({
                type : 'POST',
                url : visitListRead,
                contentType : 'application/json',
                data : JSON.stringify({
                    calls : calls
                }),
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                for (var i = 0; i < calls.length; i++) {
                    
                    var $span = $('span[data-roll-read=' + calls[i].id + ']');
                    // 非表示の場合は無視
                    if ($span.length > 0) {
                        $span.html(readAlready);
                        // datatablesに保持した値も書き換える
                        $table.cell($span.get(0).parentNode).data($span.get(0).outerHTML);
                    }
                   // 閲覧履歴ボタン
                   var $button = $('button[data-roll-read=' + calls[i].id + ']');
                   if ($button.length > 0) {
                       if (!$button.data('calls-readed')) {
                           var count = 0 + $button.html();
                           $button.html(++count);
                           // datatablesに保持した値も書き換える
                           $table.cell($button[0].parentNode).data($button[0].outerHTML);
                           // イベントが消えので再度イベントを設定
                           _addDisplayViewHistoryEvent($('button[data-roll-read=' + calls[i].id + ']'));
                       }
                   }
                }
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
            });
        });
    
        $('#selectAll').click(function() {
            if ($('#selectAll').data('all-selected')) {
                // scrollテーブルの見えないもの除外
                $('.dataTables_scrollBody .readCallId').prop('checked', false);
                $('#selectAll').data('all-selected', false);
            } else {
                // scrollテーブルの見えないもの除外
                $('.dataTables_scrollBody .readCallId').prop('checked', true);
                $('#selectAll').data('all-selected', true);
            }
        });
        
        $('#commentTable').on('click', '.comment-delete', function(e) {
            // 動的にクライアント側で差し替えるのでonで処理
            var $td = $(e.currentTarget).parents('td');
            var id = $td.data('id');
            var tech = $td.data('tech');
            $.ajax({
                type : 'DELETE',
                url : visitComment + id + '/' + tech,
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                $td.remove();
                $.colorbox.resize();
                controlCommentExistenceLabel(json.commentCount, currentCallId, $table);
            });
        });

        
        $('#dateFrom').on('change', function() {
            var dateString = $(this).val();
            var date = new Date(dateString);
            var yearMonth = date.getFullYear() + '-'+ date.getMonth(); 
            
            var old = new Date($('#tempDateFrom').val());
            var oldYearMonth = old.getFullYear() + '-' +  old.getMonth(); 
            
            if (yearMonth !=  oldYearMonth) {
                    $('#visitListSosiki').yrcmv(divVisitList, {
                        reset : true,
                        date : $('#dateFrom').val(),
                        onEmpty : function() {
                            $('.search').on('click.div', function () {
                                return false;
                            });
                        },
                        onNotEmpty : function() {
                            $('.search').off('click.div');
                        }
                    });
            }
            $('#tempDateFrom').val(dateString);
        });
        
        // ドロップダウン表示時のみ
        if ($('#visitListSosiki').length > 0) {
            $('#visitListSosiki').yrcmv(divVisitList, {
                reset : false,
                date : $('#dateFrom').val(),
                onEmpty : function() {
                    $('.search').on('click.div', function () {
                        return false;
                    });
                },
                onNotEmpty : function() {
                    $('.search').off('click.div');
                }
            });
        }
    });

    function _addDisplayViewHistoryEvent($e) {
           $e.click(function(e) {
                $('#historyContainer').empty();
                var callId = $(e.currentTarget).data('roll-read');
                var techCallId = $(e.currentTarget).data('read-tech-call-id');

                $.ajax({
                    type : 'GET',
                    url : visitListViewList + callId,
                    data: {
                        techCallId: techCallId
                    }
                }).done(function(json, statusText, jqXHR) {
                    $.yrcmv('responseCallback', { jqXHR : jqXHR });

                    $.colorbox({
                        inline : true,
                        href : '#viewHistory'
                    });
                    
                    for (i = 0; i < json.length; i++) {
                        var line  = '<tr><td >' +  json[i].lastName + ' ' + json[i].firstName + '　' + json[i].eigyoSosikiNm + '　' + json[i].date + '</td></tr>';
                        $('#historyContainer').append(line);
                    }
                    
                    $.colorbox.resize();
                }).fail(function(jqXHR, statusText, errorThrown) {
                    $.yrcmv('responseCallback', { jqXHR : jqXHR });
                });
            });
        
    }

    /**
     * コメント保存、コメント確認済みにマーク完了後の処理。
     */
    function commentResistdone(loginUserId, json, statusText, jqXHR, callId, $table, isSaveComment) {
        // inline-content
        $.yrcmv('responseCallback', { jqXHR : jqXHR });
        $('#commentSeparator').show();
        $('#commentEnd').before(createComment(true, json.id, json.tech, json.userId, json.date, json.lastName, json.firstName, json.eigyoSosikiNm, json.description));
        $('#commentEditor').val(null);
        $.colorbox.resize();
        controlCommentExistenceLabel(json.commentCount, callId, $table);
        if (isSaveComment) {
            controlCommentEnteredLabel(json.commentCount, callId, $table);
        }
    }
    
    /**
     * コメント有無ラベルの制御。
     * コメント保存、コメント確認済みマーク後の制御に利用するため既読前提となり、「なし」か「コメント無し」のみ。
     */
    function controlCommentExistenceLabel(commentCount, callId, $table) {
        var $span = $('span[data-roll-comment-read=' + callId + ']');
        if ($span.length > 0) {
            if (commentCount < 1) {
                $span.html(noComment);
            } else {
                $span.html(none);
            }
            // datatablesに保持した値も書き換える
            $table.cell($span[0].parentNode).data($span[0].outerHTML);
        }
    }
    
    /**
     * コメント入力・未入力ラベルの制御。
     */
    function controlCommentEnteredLabel(commentCount, callId, $table) {
        var $span = $('span[data-roll-comment-entered=' + callId + ']');
        
        if ($span.length > 0) {
            
            if (!$span.data('comment-entered')) {
                $span.html(confirmAlready);
                $span.attr('data-comment-entered', true);
                $table.cell($span[0].parentNode).data($span[0].outerHTML);
            }
        }
    }

    function createComment(isNew, id, isTech, userId, date, lastName, firstName, eigyoSosikiNm, description) {
        var result  = '<tr data-roll="comments">'
            + '<td colspan="2" style="position:relative;" data-id="' + id + '" data-tech="' + isTech + '">'
            + '<div><span style="word-wrap: break-word; word-break: break-all;">'
            + (description == null ? '' : description.replace(/\r?\n/g, '<br>')) + '</span></div>'
            +'<div style="width:95%;"><span class="date">'
            + (date == null ? '' : date) + '</span>'
            + '　<span>'
            + (lastName == null ? '' : lastName)
            + ' '
            + (firstName == null ? '' : firstName)
            + '</span>'
            + '　<span>' + (eigyoSosikiNm == null ? '' : eigyoSosikiNm) + '</span>'
            + '</div>'
            + (isNew !== false ? '<span class="comment-delete"/>' : '') // 新しく追加したコメントの場合のみ削除可能
            + '</td></tr>';
        return result;
    }

    $(document).ready(function() {

        if ($.yrcmv('isMobile')) {
            $('#csvDownload').remove();
        }
        
        $('#csvDownload').on('click', function() {
            $('#csv-download-form [name=div1]').val($('#div1').val());
            $('#csv-download-form [name=div2]').val($('#div2').val());
            $('#csv-download-form [name=div3]').val($('#div3').val());
            $('#csv-download-form [name=tanto]').val($('#tanto').val());
            $('#csv-download-form [name=market]').val($('#market').val());
            $('#csv-download-form [name=dateFrom]').val($('#dateFrom').val());
            $('#csv-download-form [name=dateTo]').val($('#dateTo').val());
            $('#csv-download-form [name=heldOnly]').prop('checked', $('#heldOnly').prop('checked'));
            $('#csv-download-form [name=hasInfoOnly]').prop('checked', $('#hasInfoOnly').prop('checked'));
            $('#csv-download-form [name=unreadOnly]').prop('checked', $('#unreadOnly').prop('checked'));
            $('#csv-download-form [name=callsInfoDiv]').val($('#callsInfoDiv').val());
            $('#csv-download-form').submit();
        })

    });
})(jQuery)

