function link_blank(url){
    event.returnValue = false;
    event.cancelBubble = true;
    window.open(url,'_blank');
    return false;
}

/*
 * 
 */
function logout() {
    if (!confirm("ログアウトします。\n\nよろしいですか？")) {
        return;
    }

/*
    //メニューサイトのセッション削除ＪＳＰ実行
    $.ajax({
        url: "/SBM_MV/dsbd/mv_logout.jsp",
        async: false,
        type: 'GET',
        dataType: 'text',
    }).done(function(data){
    }).fail(function(XMLHttpRequest, textStatus, errorThrown){
        alert(errorThrown.message);
    });
*/

//20140311 kou comment start
/*
    //pentahoのセッション削除ＪＳＰ実行
    $.ajax({
        url: "/pentaho/pentaho_logout.jsp",
        async: false,
        type: 'GET',
        dataType: 'text',
    }).done(function(data){
    }).fail(function(XMLHttpRequest, textStatus, errorThrown){
        alert(errorThrown.message);
    });
*/
//20140311 kou comment end

    location.href = "logout";
}
