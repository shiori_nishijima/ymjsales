/**
 * 週報用JS
 */
var visitComment = 'visitComment/';
var visitListRead = 'visitListRead/';
var visitListViewList = 'visitListViewList/';
var visitListMarketInfo = 'visitListMarketInfo/'
var divVisitList = 'divVisitList';
(function($) {
    $(document).ready(function() {

        $.yrcmv('linkSelect', {
            elements : [ $('#year'), $('#month'), $('#week') ]
        });

        $('#visitListSosiki').yrcmv('divNoAll', {
            reset : false,
            year : $('#year').val(),
            month : $('#month').val().substring(4, 6),
            onEmpty : function() {
                $('.search').on('click.div', function () {
                    return false;
                });
            },
            onNotEmpty : function() {
                $('.search').off('click.div');
            }
        });
        $('#year').on('change', function() {
            $('#visitListSosiki').yrcmv('div', {
                reset : false,
                year : $('#year').val(),
                month : $('#month').val().substring(4, 6),
                onEmpty : function() {
                    $('.search').on('click.div', function () {
                        return false;
                    });
                },
                onNotEmpty : function() {
                    $('.search').off('click.div');
                }
            });
        });
        $('#month').on('change', function() {
            $('#visitListSosiki').yrcmv('div', {
                reset : false,
                year : $('#year').val(),
                month : $('#month').val().substring(4, 6),
                onEmpty : function() {
                    $('.search').on('click.div', function () {
                        return false;
                    });
                },
                onNotEmpty : function() {
                    $('.search').off('click.div');
                }
            });
        });
        $('#searchButton').click(function(e) {
            // 担当者を選択しないと検索させない
            $('#real').prop('checked', true);
            if ($('[name=tanto] option:selected').val() == 'all') {
                return false;
            }
        });

        /* ★★datatables適用前に、テーブル内のイベントを付与 ここから */
        // コメント表示押下時
        var currentCallId;
        var loginUserId;
        $('input[data-roll=displayComment]').click(function(e) {
            $('tr[data-roll=comments]').remove();
            var callId = $(e.currentTarget).data('display-comment');
            currentCallId = callId;
            $('[data-comment-call-id]').data('comment-call-id', callId);
            var techCallId = $(e.currentTarget).data('tech-call-id');
            techCallId = techCallId ? techCallId : null
            $('[data-comment-tech-call-id]').data('comment-tech-call-id', techCallId);
            var param = {
                'techCallId' : techCallId,
            };

            $.ajax({
                type : 'GET',
                data : param,
                url : visitComment + callId,
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                loginUserId = json.userId; // 画面表示時にログインユーザIDを保持
                var $div = $('#inline-content');
                $.colorbox({
                    inline : true,
                    href : '#inline-content'
                });

                $div.find('#visitUserFirstName').html(json.firstName);
                $div.find('#visitUserLastName').html(json.lastName);
                $div.find('#eigyoSosikiNm').html(json.eigyoSosikiNm);

                if (json.comments.length < 0) {
                    $('#commentSeparator').hide();
                } else {
                    $('#commentSeparator').show();
                    for (var i = 0; i < json.comments.length; i++) {
                        var o = json.comments[i];
                        $('#commentSeparator').after(createComment(false, o.id, o.tech, o.userId, o.date, o.lastName, o.firstName, o.eigyoSosikiNm, o.description));
                    }
                }
                $.colorbox.resize();
                $('#gmailLink').attr('href', 'https://mail.google.com/mail/?view=cm&fs=1&tf=1&body=' + json.gmailLink + '%26fp%3Dlink');

            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
            });
        });

        $('#registerComment').click(function(e) {
            $('.error_message').remove();
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            $.ajax({
                type : 'POST',
                url : visitComment,
                contentType : 'application/json',
                dataType : 'json',
                data : JSON.stringify({
                    callId : callId,
                    comment : $('#commentEditor').val(),
                    techCallId : techCallId,
                }),
            }).done(
                function(json, statusText, jqXHR) {
                    commentResistdone(loginUserId, json, statusText, jqXHR, callId, true);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                for (var i = 0; i < jqXHR.responseJSON.length; i++) {
                    $('#commentErrorMessage').append( $('<span class="error_message">' + jqXHR.responseJSON[i] + '</span>'));
                }
            });
        });

        $('#readComment').click(function(e) {
            // コメント確認済みにマーク押下時
            $('.error_message').remove();
            var callId = $(e.currentTarget).data('comment-call-id');
            var techCallId = $(e.currentTarget).data('comment-tech-call-id');
            $.ajax({
                type : 'POST',
                url : visitComment + 'read/' + callId,
                data : {
                    techCallId : techCallId,
                },
            }).done(function(json, statusText, jqXHR) {
                commentResistdone(loginUserId, json, statusText, jqXHR, callId, false);
            }).fail(function(jqXHR, statusText, errorThrown) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                for (var i = 0; i < jqXHR.responseJSON.length; i++) {
                    $('#commentErrorMessage').append( $('<span class="error_message">' + jqXHR.responseJSON[i] + '</span>'));
                }
            });
        });

        $('#selectAll').click(function() {
            if ($('#selectAll').data('all-selected')) {
                // scrollテーブルの見えないもの除外
                $('.dataTables_scrollBody .readCallId').prop('checked', false);
                $('#selectAll').data('all-selected', false);
            } else {
                // scrollテーブルの見えないもの除外
                $('.dataTables_scrollBody .readCallId').prop('checked', true);
                $('#selectAll').data('all-selected', true);
            }
        });

        $('#commentTable').on('click', '.comment-delete', function(e) {
            // 動的にクライアント側で差し替えるのでonで処理
            var $td = $(e.currentTarget).parents('td');
            var id = $td.data('id');
            var tech = $td.data('tech');
            $.ajax({
                type : 'DELETE',
                url : visitComment + id + '/' + tech,
            }).done(function(json, statusText, jqXHR) {
                $.yrcmv('responseCallback', { jqXHR : jqXHR });
                $td.remove();
                $.colorbox.resize();
            });
        });
    });

    /**
     * コメント保存、コメント確認済みにマーク完了後の処理。
     */
    function commentResistdone(loginUserId, json, statusText, jqXHR, callId, isSaveComment) {
        // inline-content
        $.yrcmv('responseCallback', { jqXHR : jqXHR });
        $('#commentSeparator').show();
        $('#commentEnd').before(createComment(true, json.id, json.tech, json.userId, json.date, json.lastName, json.firstName, json.eigyoSosikiNm, json.description));
        $('#commentEditor').val(null);
        $.colorbox.resize();
        controlCommentExistenceLabel(json.commentCount, callId);
        if (isSaveComment) {
            controlCommentEnteredLabel(json.commentCount, callId);
        }
    }

    function createComment(isNew, id, isTech, userId, date, lastName, firstName, eigyoSosikiNm, description) {
        var result  = '<tr data-roll="comments">'
            + '<td colspan="2" style="position:relative;" data-id="' + id + '" data-tech="' + isTech + '">'
            + '<div><span style="word-wrap: break-word; word-break: break-all;">'
            + (description == null ? '' : description.replace(/\r?\n/g, '<br>')) + '</span></div>'
            +'<div style="width:95%;"><span class="date">'
            + (date == null ? '' : date) + '</span>'
            + '　<span>'
            + (lastName == null ? '' : lastName)
            + ' '
            + (firstName == null ? '' : firstName)
            + '</span>'
            + '　<span>' + (eigyoSosikiNm == null ? '' : eigyoSosikiNm) + '</span>'
            + '</div>'
            + (isNew !== false ? '<span class="comment-delete"/>' : '') // 新しく追加したコメントの場合のみ削除可能
            + '</td></tr>';
        return result;
    }

    /**
     * コメント有無ラベルの制御。
     * コメント保存、コメント確認済みマーク後の制御に利用するため既読前提となり、「なし」か「コメント無し」のみ。
     */
    function controlCommentExistenceLabel(commentCount, callId) {
        var $span = $('span[data-roll-comment-read=' + callId + ']');
        if ($span.length > 0) {
            if (commentCount < 1) {
                $span.html(noComment);
            } else {
                $span.html(none);
            }
        }
    }

    /**
     * コメント入力・未入力ラベルの制御。
     */
    function controlCommentEnteredLabel(commentCount, callId) {
        var $span = $('span[data-roll-comment-entered=' + callId + ']');
        
        if ($span.length > 0) {
            
            if (!$span.data('comment-entered')) {
                $span.html(confirmAlready);
                $span.attr('data-comment-entered', true);
            }
        }
    }
})(jQuery)
