(function($) {

	var defaults = {
		autoWidth : true,
		fixColumn : 1,
		eventClone : true,
	};
	
	var $fixTable;
	
	$.fixTable = $.fn.fixTable = function(options) {

	    // リサイズイベントをクリア
	    //TODO 同一画面で複数のfixTableを使うときは改良が必要
        $(window).off('resize.fixTable');

		options = $.extend(defaults, options);
		// 1.元のテーブルから、スクロールさせる原点となる座標を求める。
		// 2.テーブルを 3 つコピーして、全部で 4 つの全く同じテーブルを用意。
		// 3.全体を 1 つの div で囲い、さらに 4 つのテーブルの周りをそれぞれまた div で囲う。
		// 4.全体の divに対しては、”relative”、4 つの div は “absolute” に position を指定。
		// 5.4 つの divがそれぞれ、左上固定部 / 上固定行 / 左固定列 / スクロール部 になるよう、
		// スクロール原点に合わせて、leftとtopを動かす。
		// 6.4 つの div それぞれに、画面上に見せたい範囲の width と height を設定する。
		// 7.上固定行 /左固定列 / スクロール部 については、見えてはいけない部分を隠すため、ネガティブマージンを設定する。
		// 8.上固定行 /左固定列は、overflow を hidden にして、スクロールをスクロール部と同期させる。
		var $tableContainer = this;
        $fixTable = $tableContainer;
		var $table = $tableContainer.children('table');

		var scrollbarWidth = calcScrollbarWidth();
		$table.width($table.width() - scrollbarWidth);
		$table.css('table-layout', options.autoWidth ? 'auto' : 'fixed');

		var name = $tableContainer.attr('id');
		if (!name) {
			name = '';
		} else {
			name = '_' + name;
		}

		var containerWidth = $tableContainer.outerWidth(true);
		var containerHeight = $tableContainer.outerHeight(true);
		var headerHeight = calcHeaderHeight($table);
		// スクロールバー表示されると幅headerがズレるので補正有り
		var fixColumnWidth = calcFixColumnWidth($table, options.fixColumn);
		var $leftHeader;
		var $rightHeader;
		var $rightBody;
		var $leftBody;
		if (options.fixColumn < 1) {
			$leftHeader = $('<table></table>');
			$leftBody = $('<table></table>');
		}
		if (options.eventClone) {
			if (options.fixColumn > 0) {
				$leftHeader = $($table.clone());
				$leftBody = $($table.clone());
			}
			$rightHeader = $($table.clone());
			$rightBody = $($table.clone());
		} else {
			var html = $table.get(0).outerHTML;
			if (options.fixColumn > 0) {
				$leftHeader = $(html);
				$leftBody = $(html);
			}
			$rightHeader = $(html);
			$rightBody = $(html);
		}

		$leftHeader.attr('id', 'leftHeader' + name);
		$rightHeader.attr('id', 'rightHeader' + name);
		$rightBody.attr('id', 'rightBody' + name);
		$leftBody.attr('id', 'leftBody' + name);

		var $leftHeaderWrapper = $('<div class="leftHeaderWrapper"></div>');
		var $rightHeaderWrapper = $('<div class="rightHeaderWrapper"></div>');
		var $rightBodyWrapper = $('<div class="rightBodyWrapper"></div>');
		var $leftBodyWrapper = $('<div class="leftBodyWrapper"></div>');

		this.data('fixTable', {
			leftHeader : $leftHeader,
			leftHeaderWrapper : $leftHeaderWrapper,
			rightHeader : $rightHeader,
			rightHeaderWrapper : $rightHeaderWrapper,
			rightBodyWrapper : $rightBodyWrapper,
			rightBody : $rightBody,
			leftBody : $leftBody,
			leftBodyWrapper : $leftBodyWrapper,
			headerHeight : headerHeight,
			scrollbarWidth : scrollbarWidth,
		});

		$tableContainer.append($rightBodyWrapper);
		$tableContainer.append($rightHeaderWrapper);
		$tableContainer.append($leftBodyWrapper);
		$tableContainer.append($leftHeaderWrapper);

		$leftHeaderWrapper.append($leftHeader);
		$rightBodyWrapper.append($rightBody);
		$leftBodyWrapper.append($leftBody);
		$rightHeaderWrapper.append($rightHeader);

		$leftHeaderWrapper.width(fixColumnWidth);
		$leftHeaderWrapper.height(headerHeight);
		//$leftHeader.css('paddingRight', scrollbarWidth);

		$rightHeaderWrapper.height(headerHeight);

		$rightBody.css('marginTop', -headerHeight);
		$rightBodyWrapper.css({
			top : headerHeight
		});

		$leftBodyWrapper.width(fixColumnWidth);
		$leftBodyWrapper.css({
			top : headerHeight
		});
		$leftBody.css('marginTop', -headerHeight);

		resize($tableContainer);

		$rightBodyWrapper.scroll(function(e) {
			$rightHeaderWrapper.scrollLeft($rightBodyWrapper.scrollLeft());
			$leftBodyWrapper.scrollTop($rightBodyWrapper.scrollTop());
		})
		$table.hide();

		var timer = false;
		$(window).on('resize.fixTable', function() {
            if (timer !== false) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                resize($tableContainer);
            }, 200);
        });

		this.destroy = function() {
		    $(window).off('resize.fixTable');
	        $fixTable = null;
		}

        this.bodyScrollLeft = function(scrollLeft) {
            if (typeof scrollLeft === "undefined") {
                return $rightBodyWrapper.scrollLeft();
            } else {
                $rightBodyWrapper.scrollLeft(scrollLeft)
                $rightHeaderWrapper.scrollLeft(scrollLeft);
            }
        }

        this.bodyScrollTop = function(scrollTop) {
            if (typeof scrollTop === "undefined") {
                return $rightBodyWrapper.scrollTop();
            } else {
                $leftBodyWrapper.scrollTop(scrollTop);
                $rightBodyWrapper.scrollTop(scrollTop);
            }
        }

		return this;
	};

	function resize($fixTable) {

		var data = $fixTable.data('fixTable');
		var $leftHeaderWrapper = data.leftHeaderWrapper;
		var $rightHeader = data.rightHeader;
		var $rightHeaderWrapper = data.rightHeaderWrapper;
		var $rightBody = data.rightBody;
		var $rightBodyWrapper = data.rightBodyWrapper;
		var $leftBodyWrapper = data.leftBodyWrapper;
		var scrollbarWidth = data.scrollbarWidth;
		var headerHeight = data.headerHeight;

		// スクロールバーの幅headerがズレるので補正
		var rightHeaderWrapperWidth = $fixTable.width()
				- ($fixTable.outerHeight(true) < $rightBody.outerHeight(true) ? scrollbarWidth
						: 0);
		$rightHeaderWrapper.width(rightHeaderWrapperWidth);

		$rightBodyWrapper.height($fixTable.height() - headerHeight);
		$rightBodyWrapper.width($fixTable.width());

		// スクロールバーの幅headerがズレるので補正
		$leftBodyWrapper
				.height($fixTable.height()
						- headerHeight
						- ($fixTable.outerWidth(true) < $rightBody
								.outerWidth(true) ? scrollbarWidth : 0));

	}

	$.fixTable.destroy = $.fn.fixTable.destroy = function(options) {
        $(window).off('resize.fixTable');
        $fixTable = null;
	}
	
	    $.fixTable.bodyScrollTop = $.fn.fixTable.bodyScrollTop = function(scrollTop) {
	        if ($fixTable) {
	            var data = $fixTable.data('fixTable');
	            var $leftBodyWrapper = data.leftBodyWrapper;
	            var $rightBodyWrapper = data.rightBodyWrapper;
                if (typeof scrollTop === 'undefined') {
                    return $rightBodyWrapper.scrollTop();
                } else {
                    $leftBodyWrapper.scrollTop(scrollTop);
                    $rightBodyWrapper.scrollTop(scrollTop);
                }
	        }
	    }

    $.fixTable.bodyScrollLeft = $.fn.fixTable.bodyScrollLeft = function(scrollLeft) {
        if ($fixTable) {
            var data = $fixTable.data('fixTable');
            var $rightBodyWrapper = data.rightBodyWrapper;
            var $rightHeaderWrapper = data.rightHeaderWrapper;
             if (typeof scrollLeft === 'undefined') {
                 return $rightBodyWrapper.scrollLeft();
             } else {
                 $rightBodyWrapper.scrollLeft(scrollLeft)
                 $rightHeaderWrapper.scrollLeft(scrollLeft);
             }
        }
    }

	function calcHeaderHeight($table) {
		var tableBorder = parseInt($table.find('tr>th').first().css(
				'borderBottomWidth'), 10);
		tableBorder = tableBorder ? +tableBorder : 0;
		return $table.children('thead').outerHeight(true) + tableBorder;
	}

	function calcFixColumnWidth($table, leftFix) {
		if (leftFix < 1) {
			return 0;
		}

		var tableBorder = parseInt($table.find('tr>td').first().css(
				'borderLeftWidth'), 10);
		tableBorder = tableBorder ? +tableBorder : 0;
		var $tr = $table.children('tbody').children('tr').first();
		var result = 0;
		$tr.find('td').each(function(index, e) {
			if (index > leftFix - 1) {
				return false;
			} else {
				result = result + $(e).outerWidth(true);
			}
		});
		result = result + tableBorder;
		return result;
	}

	function calcScrollbarWidth() {
		var $scrollDiv = $('<div style="width: 100px; height: 100px; overflow: scroll; position: absolute; top: -9999px;"></div>');
		$('body').append($scrollDiv);
		// Get the scrollbar width
		var scrollbarWidth = $scrollDiv.get(0).offsetWidth
				- $scrollDiv.get(0).clientWidth;
		// Delete the DIV
		$scrollDiv.remove();
		return scrollbarWidth;
	}

})(jQuery);
