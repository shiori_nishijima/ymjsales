package jp.co.yrc.mv.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import jp.co.yrc.mv.common.Constants.COMPANY;


public class DateUtils {

	/**
	 * 現在年月のCalendarを生成します。
	 * 日付には1日を設定します。
	 * 
	 * @return　現在年月のカレンダー
	 */
	public static Calendar getYearMonthCalendar() {
		Calendar c = getCalendar();
		c.set(Calendar.DATE, 1); // 日付には1日を設定
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}
	
	/**
	 * 指定した年月のCalendarを生成します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return　指定した年月のカレンダー
	 */
	public static Calendar getYearMonthCalendar(int year, int month) {
		Calendar c = getCalendar();
		c.set(year, month - 1, 1, 0, 0, 0); // 日付には1日を設定
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}
	
	/**
	 * 指定したカレンダーに追加月数を加えたCalendarを生成します。
	 * 
	 * @param calendar カレンダー
	 * @param additionMonth 追加する月数
	 * @return　指定した年月のカレンダー
	 */
	public static Calendar getYearMonthCalendar(Calendar calendar, int additionMonth) {
		Calendar c = getYearMonthCalendar();
		c.setTime(calendar.getTime());
		c.add(Calendar.MONTH, additionMonth);
		return c;
	}
	
	/**
	 * 指定した年月日のCalendarを生成します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return　指定した年月のカレンダー
	 */
	public static Calendar getYearMonthDateCalendar(int year, int month, int date) {
		Calendar c = getCalendar();
		c.set(year, month - 1, date, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c;
	}

	/**
	 * 指定したカレンダーに追加日数を加えたCalendarを生成します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return　指定した年月のカレンダー
	 */
	public static Calendar getYearMonthDateCalendar(Calendar calendar, int additionDate) {
		Calendar c = getCalendar();
		c.setTime(calendar.getTime());
		c.add(Calendar.DATE, additionDate);
		return c;
	}

	/**
	 * 指定した年月が現在年月より過去かどうかを判定します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return 現在年月より過去の場合はtrue
	 */
	public static boolean isPastYearMonth(int year, int month) {
		Calendar currentYearMonth = getYearMonthCalendar();
		Calendar paramYearMonth = getYearMonthCalendar(year, month);
		return currentYearMonth.compareTo(paramYearMonth) > 0;
	}
	
	/**
	 * 指定したカレンダーが現在年月より過去かどうかを判定します。
	 * 
	 * @param calendar カレンダー
	 * @return 現在年月より過去の場合はtrue
	 */
	public static boolean isPastYearMonth(Calendar c) {
		return isPastYearMonth(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
	}
	
	/**
	 * 指定した年月が現在年月と同じかどうかを判定します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return 現在年月と同じ場合はtrue
	 */
	public static boolean isCurrentYearMonth(int year, int month) {
		Calendar currentYearMonth = getYearMonthCalendar();
		Calendar paramYearMonth = getYearMonthCalendar(year, month);
		return currentYearMonth.compareTo(paramYearMonth) == 0;
	}
	
	/**
	 * 現在のカレンダーを取得します。
	 * 
	 * @return カレンダー
	 */
	public static Calendar getCalendar() {
		String usrCompany = LocaleContext.instance().getCompany();
		TimeZone tz = TimeZone.getTimeZone(COMPANY.convertCompanyToTimeZone(usrCompany));

		DateFormat df = DateFormat.getDateTimeInstance();
		df.setTimeZone(tz);

		String dateStr = df.format(Calendar.getInstance(tz).getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Date formatDate = null;
		try {
			formatDate = (Date) sdf.parse(dateStr);
		} catch (ParseException e) {
			formatDate = new Date();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(formatDate);

		return cal;
	}
	
	/**
	 * DB登録用のDateを取得します。
	 * 
	 * @return　Date 現在日時のDate
	 */
	public static Date getDBDate() {
		return getCalendar().getTime();
	}

	/**
	 * DB登録用のTimestampを取得します。
	 * 
	 * @return　Date 現在日時のDate
	 */
	public static Timestamp getDBTimestamp() {
		return new Timestamp(getDBDate().getTime());
	}

	/**
	 * 曜日ラベル用フォーマットを取得します。
	 * 
	 * @return SimpleDateFormat
	 */
	public static SimpleDateFormat getDayOfWeekFormat() {
		return new SimpleDateFormat("E", LocaleContext.instance().getLocale());
	}

	/**
	 * 引数に指定したDateの日を取得します。
	 * 
	 * @param date　Date
	 * @return 日
	 */
	public static int getDay(Date date) {
		Calendar c = getCalendar();
		c.setTime(date);
		return c.get(Calendar.DATE);
	}

	/**
	 * 現在の年月を取得します。
	 * 
	 * @return 年月
	 */
	public static int getNowYearMonth() {
		Calendar now = getCalendar();
		return toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
	}

	/**
	 * 指定した年月を6桁の数値に変換します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return 年月（6桁）
	 */
	public static int toYearMonth(Integer year, Integer month) {
		return Integer.valueOf(String.format("%d%02d", year, month));
	}

	/**
	 * 指定したカレンダーを6桁の数値に変換します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @return 年月（6桁）
	 */
	public static int toYearMonth(Calendar c) {
		return toYearMonth(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
	}

	/**
	 * 指定した6桁の数値を月のみの値に変換します。
	 * 
	 * @param yearMonth 年月
	 * @return 月（2桁）
	 */
	public static int toMonth(Integer yearMonth) {
		String s = yearMonth.toString();
		return Integer.valueOf(s.substring(4, 6));
	}
}
