package jp.co.yrc.mv.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {

	public static String getMD5HexString(String password) {

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5インスタンス生成エラー");
		}
		byte[] bytePassword = digest.digest(password.getBytes());
		String hexPassword = MD5Util.toHexString(bytePassword);

		return hexPassword;
	}

	/**
	 * Byte[]を16進文字列に変換する
	 * 
	 * @param arrInput
	 * @return
	 */
	public static String toHexString(byte[] arr) {
		StringBuffer buff = new StringBuffer(arr.length * 2);
		for (int i = 0; i < arr.length; i++) {
			// 0xffでbyteのマイナス値をプラスに変換する
			String b = Integer.toHexString(arr[i] & 0xff);
			if (b.length() == 1) {
				buff.append("0");
			}
			buff.append(b);
		}
		return buff.toString();
	}
}
