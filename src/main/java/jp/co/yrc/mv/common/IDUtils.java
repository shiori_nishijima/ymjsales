package jp.co.yrc.mv.common;

import java.util.Random;

public class IDUtils {
	/**
	 * GUIDを生成します。
	 *
	 * @return
	 */
	public static String generateGuid() {

		// 取得結果フォーマット:1-14桁 sssssfffffffff
		String str = String.format("%1$014d", System.nanoTime());

		// 秒取得
		long dec = Long.parseLong(str.substring(0, 5));
		// 秒未満取得
		long sec = Long.parseLong(str.substring(5));

		String hexDec = ensureLength(Long.toHexString(dec), 5);
		String hexSec = ensureLength(Long.toHexString(sec), 6);

		String guid = "";
		guid += hexDec;
		guid += createGuidSection(3);
		guid += '-';
		guid += createGuidSection(4);
		guid += '-';
		guid += createGuidSection(4);
		guid += '-';
		guid += createGuidSection(4);
		guid += '-';
		guid += hexSec;
		guid += createGuidSection(6);

		return guid;
	}

	private static String createGuidSection(int characters) {
		String ret = "";
		for (int i = 0; i < characters; i++) {
			int random = new Random().nextInt(15);
			ret += Integer.toHexString(random);
		}
		return ret;
	}

	private static String ensureLength(String str, int length) {
		int strlen = str.length();
		if (strlen < length) {
			StringBuilder sb = new StringBuilder(str);
			for (int i = strlen; i < length; i++) {
				sb.append("0");
			}
			str = sb.toString();
		} else if (strlen > length) {
			str = str.substring(0, length);
		}
		return str;
	}
}
