package jp.co.yrc.mv.common;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jp.co.yrc.mv.html.SelectItem;

public final class Constants {

	public static final class Separator {
		/**
		 * カンマ
		 */
		public static final String COMMA = ",";

		/**
		 * キャレット
		 */
		public static final String CARET = "^";

		/**
		 * キャレットカンマ
		 */
		public static final String CARET_COMMA = "^,^";
	}

	public static final class SelectItrem {
		public static final String GROUP_LABEL_ALL = "全て";
		public static final String GROUP_LABEL_ALL_BLANK = "";
		public static final String GROUP_VAL_ALL = "all";
		public static final String GROUP_VAL_NOT_BELONG = "notBelong";
		public static final SelectItem OPTION_ALL = new SelectItem(GROUP_LABEL_ALL, GROUP_VAL_ALL);
		public static final SelectItem OPTION_ALL_BLANK = new SelectItem(GROUP_LABEL_ALL_BLANK, GROUP_VAL_ALL);
	}

	public enum CorpCd {
		YMJ("ymj", "ymj"); 

		private String code;
		private String[] corpCodes;

		public static String[] convertCompanyToCodes(String company) {
			for (CorpCd o : values()) {
				if (o.getCode().equals(company)) {
					return o.getCorpCodes();
				}
			}
			return new String[] {""};
		}

		private CorpCd(String code, String... corpCodes) {
			this.code = code;
			this.corpCodes = corpCodes;
		}

		public String getCode() {
			return code;
		}
		
		public String[] getCorpCodes() {
			return corpCodes;
		}
	}

	/**
	 * 07 訪問実績の所属販社内のすべての情報が閲覧可能。その他は部門権限04と同じ。<br>
	 * 08 訪問実績の所属販社内のすべての情報が閲覧可能。その他は営業所権限05と同じ。<br>
	 * 09 訪問実績の所属販社内のすべての情報が閲覧可能。その他はセールス権限06と同じ。<br>
	 */
	public enum Role {
		All("01", "ROLE_ALL"), 
		Multiple("02", "ROLE_MULTIPLE"), 
		Company("03", "ROLE_COMPANY"), 
		Department("04", "ROLE_DEPARTMENT"), 
		SalesOffice("05", "ROLE_OFFICE"), 
		Sales("06", "ROLE_SALES"),

		// 以降拡張ロール
		DepartmentVisitListSalesCompany("07", 
				"ROLE_DEPARTMENT_VISIT_LIST_SALES_COMPANY", Department), 
		SalesOfficeVisitListSalesCompany("08", 
				"ROLE_SALES_OFFICE_VISIT_LIST_SALES_COMPANY", SalesOffice), 
		SalesVisitListSalesCompany("09", 
				"ROLE_SALES_VISIT_LIST_SALES_COMPANY", Sales), 
		SalesOfficeVisitListDepartment("10", 
				"ROLE_SALES_OFFICE_VISIT_LIST_DEPARTMENT", SalesOffice), 
		SalesVisitListDepartment("11", 
				"ROLE_SALES_VISIT_LIST_DEPARTMENT", Sales),

		TechService("12", "ROLE_TECH_SERVICE", All);

		private static final Map<Role, Role> visitListRole;
		static {
			Map<Role, Role> map = new HashMap<>();
			map.put(All, All);
			map.put(Multiple, Multiple);
			map.put(Company, Company);
			map.put(Department, Department);
			map.put(SalesOffice, SalesOffice);
			map.put(Sales, Sales);
			map.put(DepartmentVisitListSalesCompany, Company);
			map.put(SalesOfficeVisitListSalesCompany, Company);
			map.put(SalesVisitListSalesCompany, Company);
			map.put(SalesOfficeVisitListDepartment, Department);
			map.put(SalesVisitListDepartment, Department);
			map.put(TechService, All);
			visitListRole = Collections.unmodifiableMap(map);
		}

		/**
		 * 権限コードからRoleを取得します。
		 * @param code
		 * @return ロール
		 */
		public static Role getByCode(String code) {
			for (Role r : values()) {
				if (r.getCode().equals(code)) {
					return r;
				}
			}
			return null;
		}

		public static Role getByRole(String role) {

			for (Role r : values()) {
				if (r.getRole().equals(role)) {
					return r;
				}
			}
			return null;
		}

		/**
		 * 訪問一覧用のプルダウン作成で使う権限。
		 * 
		 * @param role
		 * @return
		 */
		public static Role getVisitListRole(Role role) {
			return visitListRole.get(role);
		}

		private String code;
		private String role;
		private Role parent;

		private Role(String code, String role) {
			this.role = role;
			this.code = code;
			this.parent = this;
		}

		private Role(String code, String role, Role parent) {
			this.role = role;
			this.code = code;
			this.parent = parent;
		}

		public String getCode() {
			return code;
		}

		public Role getParent() {
			return parent;
		}

		public String getRole() {
			return role;
		}
		
		/**
		 * 営業所長またはセールスかどうかを判定します。
		 * @return
		 */
		public boolean isSalesOfficeOrSales() {
			return this.parent.equals(SalesOffice) || this.parent.equals(Sales);
		}
	}
	
	/**
	 * 企業・タイムゾーングループ
	 */
	public enum COMPANY {
		YMJ("ymj", "Asia/Tokyo");

		private String company;
		private String timeZone;

		private COMPANY(String company, String timeZone) {
			this.company = company;
			this.timeZone = timeZone;
		}

		/**
		 * companyに紐づくタイムゾーンを返却します。
		 * @return タイムゾーン
		 */
		public static String convertCompanyToTimeZone(String company) {
			for (COMPANY o : values()) {
				if (o.getCompany().equals(company)) {
					return o.getTimeZone();
				}
			}
			return YMJ.timeZone;
		}

		public String getCompany() {
			return company;
		}
		public String getTimeZone() {
			return timeZone;
		};
	}

	/**
	 * 日付フォーマット
	 */
	public enum DateFormat {
		yyyyMMddHHmm("yyyy/MM/dd HH:mm"), 
		yyyyMMdd("yyyy/MM/dd"), 
		Time("HH:mm"), 
		yyyy("yyyy"), 
		yyyyMM("yyyy/MM");
		
		private String format;

		private DateFormat(String format) {
			this.format = format;
		}

		public SimpleDateFormat getFormat() {
			return new SimpleDateFormat(format);
		}

		public String getFormatString() {
			return format;
		}
	}

	/**
	 * 親タイプ
	 */
	public enum ParentType {
		Accounts("Accounts", "企業"), 
		Opportunities("Opportunities", "案件"), 
		Calls("Calls", "活動"), 
		TechServiceCalls("TechServiceCalls", "技サ活動");
		private String type;
		private String typeName;

		public static ParentType get(String parentType) {
			if (Accounts.type.equals(parentType)) {
				return Accounts;
			} else if (Opportunities.type.equals(parentType)) {
				return Opportunities;
			} else if (Calls.type.equals(parentType)) {
				return Calls;
			}
			return null;
		}

		private ParentType(String type, String typeName) {
			this.type = type;
			this.typeName = typeName;
		}

		public String toString() {
			return type;
		}

		public String getTypeName() {
			return typeName;
		};
	}

	/**
	 * ステータス
	 */
	public enum Status {
		Planned("Planned", "予定"), 
		Held("Held", "完了");
		private String type;
		private String typeName;

		public static Status get(String parentType) {
			if (Planned.type.equals(parentType)) {
				return Planned;
			} else if (Held.type.equals(parentType)) {
				return Held;
			}
			return null;
		}

		private Status(String type, String typeName) {
			this.type = type;
			this.typeName = typeName;
		}

		public String toString() {
			return type;
		}

		public String getTypeName() {
			return typeName;
		};
	}

	/**
	 * 削除
	 *
	 */
	public enum Delete {
		False(false), True(true);

		private boolean delete;

		private Delete(boolean delete) {
			this.delete = delete;
		}

		public Boolean toBoolean() {
			return delete;
		}

		public char toChar() {
			return String.valueOf(ordinal()).charAt(0);
		}

		public String toString() {
			return String.valueOf(this.ordinal());
		}
	}

	/**
	 * 中分類
	 *
	 */
	public static final class MiddleClassification {
		public static final String PCR_SUMMER = "PCR_SUMMER"; /* PCRナツ */
		public static final String VAN_SUMMER = "VAN_SUMMER"; /* VANナツ */
		public static final String LT_SUMMER = "LT_SUMMER"; /* LTナツ */
		public static final String TB_SUMMER = "TB_SUMMER"; /* TBナツ */
		public static final String PCR_PURE_SNOW = "PCR_PURE_SNOW"; /* PCR純S */
		public static final String VAN_PURE_SNOW = "VAN_PURE_SNOW"; /* VAN純S */
		public static final String LT_SNOW = "LT_SNOW"; /* LTスノー */
		public static final String LT_AS = "LT_AS"; /* LTAS */
		public static final String LT_PURE_SNOW = "LT_PURE_SNOW"; /* LT純S */
		public static final String TB_SNOW = "TB_SNOW"; /* TBスノー */
		public static final String TB_AS = "TB_AS"; /* TBAS */
		public static final String TB_PURE_SNOW = "TB_PURE_SNOW"; /* TB純S */
		public static final String ORID = "ORID"; /* OR/ID */
		public static final String OR = "OR"; /* OR */
		public static final String ID = "ID"; /* ID */
		public static final String TIFU = "TIFU"; /* チフ */
		public static final String OTHER = "OTHER"; /* その他 */
		public static final String WORK = "WORK"; /* 作業 */
		public static final String PCB_SUMMER = "PCB_SUMMER"; /* PCB */
		public static final String PCB_PURE_SNOW = "PCB_PURE_SNOW"; /* PCB */
		public static final String PCB = "PCB"; /* PCB */
		public static final String RETREAD = "RETREAD"; /* リトレッド */
		public static final String LT_RETREAD_SUMMER = "LT_RETREAD_SUMMER"; /* LTナツリトレッド */
		public static final String TB_RETREAD_SUMMER = "TB_RETREAD_SUMMER"; /* TBナツリトレッド */
		public static final String TIRE_RETREAD_SUMMER = "TIRE_RETREAD_SUMMER"; /* 特タナツリトレッド */
		public static final String LT_RETREAD_SNOW = "LT_RETREAD_SNOW"; /* LTスノーリトレッド */
		public static final String TB_RETREAD_SNOW = "TB_RETREAD_SNOW"; /* LBスノーリトレッド */

	}

	public enum ResultsType {
		NUMBER("本数", "01"), 
		SALES("金額", "02"), 
		MARGIN("粗利", "03");

		String value;
		String label;

		private ResultsType(String label, String value) {
			this.value = value;
			this.label = label;
		}

		public String getValue() {

			return value;
		}

		public String getLabel() {
			return label;
		}
	}

	public enum ResultsNet {

		HQ("本社NET", "01", Role.SalesOffice, Role.Sales), 
		SALESOFFICE("営業所NET", "02"),;

		String value;
		String label;
		Role[] ignoreRoles = {};

		private ResultsNet(String label, String value) {
			this.value = value;
			this.label = label;
		}

		private ResultsNet(String label, String value, Role... ignoreRoles) {
			this.value = value;
			this.label = label;
			this.ignoreRoles = ignoreRoles;
		}

		public String getValue() {
			return value;
		}

		public String getLabel() {
			return label;
		}
		
		/**
		 * 引数のロールがNETの持つ表示不可リストに含まれているかを判定します。
		 * @param parentRole ユーザの持つ親ロール
		 * @return 含まれていればtrue
		 */
		public boolean containsIgnoreRoles(Role parentRole) {
			for (Role ignoreRole : ignoreRoles) {
				if (parentRole.equals(ignoreRole)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * 販路
	 */
	public static final class Market {
		public static final String SS = "Z2511H";
		public static final String RS = "Z2511L";
		public static final String CD = "Z2511N";
		public static final String PS = "Z2511J";
		public static final String CS = "Z2511P";
		public static final String HC = "Z2511S";
		public static final String LEASE = "Z2511X";
		public static final String INDIRECT_OTHER = "Z2511R";
		public static final String TRUCK = "Z2511C";
		public static final String BUS = "Z2511E";
		public static final String HIRE_TAXI = "Z2511G";
		public static final String DIRECT_OTHER = "Z2511F";
		public static final String RETAIL = "RETAIL";
	}

	public enum Group {
		GROUP_IMPORTANT("GROUP_IMPORTANT", "重点"), 
		GROUP_NEW("GROUP_NEW", "新規"), 
		GROUP_DEEP_PLOWING("GROUP_DEEP_PLOWING", "深耕"), 
		GROUP_Y_CP("GROUP_Y_CP", "Y-CP"), 
		GROUP_OTHER_CP("GROUP_OTHER_CP", "他社CP"), 
		GROUP_ONE("GROUP_ONE", "G1"), 
		GROUP_TWO("GROUP_TWO", "G2"), 
		GROUP_THREE("GROUP_THREE", "G3"), 
		GROUP_FOUR("GROUP_FOUR", "G4"), 
		GROUP_FIVE("GROUP_FIVE", "G5"), 
		RANK_A("A", "A"), 
		RANK_B("B", "B"), 
		RANK_C("C", "C"), 
		GROUP_OUTSIDE("GROUP_OUTSIDE", "担当外");

		public static Group getByCode(String code) {
			for (Group g : values()) {
				if (g.getCode().equals(code)) {
					return g;
				}
			}
			return null;
		}

		private String code;
		private String name;

		private Group(String code, String name) {
			this.code = code;
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

	}

	public static final class SelectionItems {
		public static final class Calls {
			public static final String CATEGORY = "category";
			public static final String DIVISION_C = "divisionC";
			public static final String INFO_DIV = "infoDiv";
			public static final String KIND = "kind";
			public static final String MAKER = "maker";
			public static final String PARENT_TYPE = "parentType";
			public static final String SENSITIVITY = "sensitivity";
			public static final String STATUS = "status";

			public static final String getName() {
				return "Calls";
			}
		}

		public static final class CompassKind {
			public static final String LARGE_CLASS_2 = "largeClass2";
			public static final String MIDDLE_CLASS = "middleClass";

			public static final String getName() {
				return "CompassKind";
			}
		}

	}

	public static final class LogInfo {
		public static final class Level {
			public static final String ACCESS_MV = "ACCESS_MV";
		}
	}

	public static final class Div {

		public static final Div DIV1 = new Div(0, "販社");
		public static final Div DIV2 = new Div(1, "部門");
		public static final Div DIV3 = new Div(2, "営業所");
		public static final Div SALES = new Div(3, "セールス");

		String name;
		int layer;

		private Div(int layer, String name) {
			super();
			this.layer = layer;
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getLayer() {
			return layer;
		}

		public void setLayer(int layer) {
			this.layer = layer;
		}
	}

	public static class LogInfos {
		public enum TimeUnit {
			Month("01", "月別"), 
			Day("02", "日別"), 
			Hour("03", "時間別");

			private String value;
			private String label;

			private TimeUnit(String value, String label) {
				this.value = value;
				this.label = label;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			public String getLabel() {
				return label;
			}

			public void setLabel(String label) {
				this.label = label;
			}

			public static TimeUnit getByValue(String value) {
				for (TimeUnit t : values()) {
					if (t.getValue().equals(value)) {
						return t;
					}
				}
				return null;
			}

		}

		public enum UserAgent {
			PC("01", "PC"), 
			IPad("02", "iPad");

			private String value;
			private String label;

			private UserAgent(String value, String label) {
				this.value = value;
				this.label = label;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			public String getLabel() {
				return label;
			}

			public void setLabel(String label) {
				this.label = label;
			}
		}
	}
}
