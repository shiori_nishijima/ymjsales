package jp.co.yrc.mv.common;

import java.util.Locale;

/**
 * ロケール情報を保持するクラス。
 * 各スレッドごとに設定、取得する。
 * 
 */
public abstract class LocaleContext {
	@SuppressWarnings("rawtypes")
	private static ThreadLocal instance = new ThreadLocal() {
		protected Object initialValue() {
			return null;
		};
	};

	/**
	 * スレッドに紐づいたロケールを取得します。
	 * 
	 * @return　ロケール
	 */
	public abstract Locale getLocale();

	/**
	 * スレッドにロケールを設定します。
	 * 
	 * @param locale ロケール
	 */
	public abstract LocaleContext setLocale(Locale locale);
	
	/**
	 * スレッドに紐づいた会社を取得します。
	 * 
	 * @return　会社
	 */
	public abstract String getCompany();

	/**
	 * スレッドに会社を設定します。
	 * 
	 * @param company 会社
	 */
	public abstract void setCompany(String company);

	/**
	 * スレッドに紐づいたLocaleContextインスタンスを取得します。
	 * 紐づいたインスタンスが存在しない場合は、新規インスタンスをスレッドに紐づけてから返却します。
	 * 
	 * @return ロケールコンテキスト
	 */
	@SuppressWarnings("unchecked")
	public static LocaleContext instance() {
		LocaleContext ctx = (LocaleContext) instance.get();
		if (ctx != null) {
			return ctx;
		}
		instance.set(new LocaleContextImpl());
		return (LocaleContext) instance.get();
	}

	static class LocaleContextImpl extends LocaleContext {
		private Locale locale;
		private String company;

		public Locale getLocale() {
			if (locale == null) {
				locale = Locale.getDefault();
			}
			return locale;
		}

		public LocaleContext setLocale(Locale locale) {
			this.locale = locale;
			return this;
		}

		public String getCompany() {
			if (company == null) {
				company = Constants.COMPANY.YMJ.getCompany();
			}
			return company;
		}

		public void setCompany(String company) {
			this.company = company;
		}
	}
}
