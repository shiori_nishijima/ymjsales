package jp.co.yrc.mv.rest;

import java.security.Principal;

import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class ChartSosikiAchievementRateRestController extends ChartBudgetPerformanceComparisonRestController {

	/**
	 * グラフ用情報を取得します。
	 * 同じデータを使うので継承でそのまま使う。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @return 結果オブジェクト
	 */
	@Override
	@RequestMapping(value = "/chartSosikiAchievementRateData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result getData(@RequestBody Param param, Principal principal) {
		return super.getData(param, principal);
	}
}
