package jp.co.yrc.mv.rest;

import java.security.Principal;

import jp.co.yrc.mv.dto.MarketInfoDto;
import jp.co.yrc.mv.service.MarketInfosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping({ "/visitListMarketInfo", "/tech/visitListMarketInfo" })
public class VisitListMarketInfoController {

	@Autowired
	MarketInfosService marketInfosService;

	@RequestMapping(value = "/{callId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result find(@PathVariable String callId, String techCallId, Principal principal) {

		Result result = new Result();

		MarketInfoDto o = marketInfosService.selectById(callId, techCallId);

		result.artNo = o.getArtNo();
		result.carBodyShapeName = o.getCarBodyShapeName();
		result.carMakerName = o.getCarMakerName();
		result.carTypeName = o.getCarTypeName();
		result.damagedNameName = o.getDamagedNameName();
		result.damagedPartName = o.getDamagedPartName();
		result.damagedTypeName = o.getDamagedTypeName();
		result.id = o.getId();
		result.infoDivName = o.getInfoDivName();
		result.mileage = o.getMileage();
		result.mileageUnitName = o.getMileageUnitName();
		result.mountingTerm = o.getMountingTerm();
		result.performanceName = o.getPerformanceName();
		result.roadSurfaceName = o.getRoadSurfaceName();
		result.usagesName = o.getUsagesName();
		result.size = o.getSize();

		result.pattern = o.getPattern();
		result.serial = o.getSerial();

		return result;
	}

	public static class Result {
		public String infoDivName;
		public String damagedPartName;
		public String damagedTypeName;
		public String performanceName;
		public String roadSurfaceName;
		public String carTypeName;
		public String carBodyShapeName;
		public String mileageUnitName;
		public String id;
		public String artNo;
		public String serial;
		public String damagedNameName;
		public String carMakerName;
		public String usagesName;
		public Integer mountingTerm;
		public Integer mileage;
		public String size;
		public String pattern;
	}
}
