package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MonthlyComments;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.MonthlyCommentsService;
import jp.co.yrc.mv.validator.annotation.ByteLength;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class MonthlyCommentController {

	@Autowired
	MonthlyCommentsService monthlyCommentsService;

	@Autowired
	UserDetailsHelper userDetailHelper;

	@RequestMapping(value = "/monthlyComment", method = RequestMethod.GET)
	public MonthlyCommentsDto showMonthlyComments(MonthlyCommentsDto monthlyCommentDto, Principal principal) {
		UserInfoDto user = userDetailHelper.getUserInfo(principal);
		MonthlyCommentsDto result = monthlyCommentsService.getMonthlyComments(monthlyCommentDto.getYear(),
				monthlyCommentDto.getMonth(), monthlyCommentDto.getUserId(), user.getCorpCodes());

		return result == null ? new MonthlyCommentsDto() : result;
	}

	@RequestMapping(value = "/monthlyComment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, String> saveMonthlyComment(@Valid @RequestBody MonthlyCommentParam requestMonthlyComment,
			Principal principal) {
		String userId = userDetailHelper.getUserId(principal);

		requestMonthlyComment.setModifiedUserId(userId);
		monthlyCommentsService.save(requestMonthlyComment);

		// JSONを返さないとクライアントでエラーになるので空のものを返す
		return Collections.singletonMap("", "");
	}

	public static class MonthlyCommentParam extends MonthlyComments {

		@ByteLength(max = 65535)
		@Override
		public String getBossComment1() {
			return super.getBossComment1();
		}

		@ByteLength(max = 65535)
		@Override
		public String getBossComment2() {
			return super.getBossComment2();
		}

		@ByteLength(max = 65535)
		@Override
		public String getBossComment3() {
			return super.getBossComment3();
		}

		@ByteLength(max = 65535)
		@Override
		public String getBossComment4() {
			return super.getBossComment4();
		}

		@ByteLength(max = 65535)
		@Override
		public String getBossComment5() {
			return super.getBossComment5();
		}
	}
}
