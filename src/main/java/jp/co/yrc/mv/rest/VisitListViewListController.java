package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.UsersService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping({ "/visitListViewList", "/tech/visitListViewList" })
public class VisitListViewListController {

	@Autowired
	CallsService callsService;

	@Autowired
	UsersService usersService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@RequestMapping(value = "/{callId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Result> list(@PathVariable String callId, String techCallId, Principal principal) {
		List<UserSosikiDto> list = StringUtils.isEmpty(techCallId) ? callsService.listCallReadUser(callId)
				: callsService.listCallReadUser4Tech(callId);

		List<Result> result = new ArrayList<>();
		for (UserSosikiDto o : list) {

			Calendar editDate = Calendar.getInstance();
			editDate.setTime(o.getDateModified());

			@SuppressWarnings("unchecked")
			UserSosikiDto userSosiki = usersService.selectUserById(o.getId(), Collections.EMPTY_LIST, null, null,
					editDate.get(Calendar.YEAR), editDate.get(Calendar.MONTH) + 1);

			if (userSosiki != null) {

				Result r = new Result();
				r.firstName = userSosiki.getFirstName();
				r.lastName = userSosiki.getLastName();
				r.eigyoSosikiNm = userSosiki.getEigyoSosikiNm();
				r.date = Constants.DateFormat.yyyyMMddHHmm.getFormat().format(o.getDateModified());
				result.add(r);
			}

		}
		return result;
	}

	public static class Result {
		public String date;
		public String firstName;
		public String lastName;
		public String eigyoSosikiNm;
	}
}
