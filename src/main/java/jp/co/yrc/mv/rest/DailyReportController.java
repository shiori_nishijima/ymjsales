package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.DailyComments;
import jp.co.yrc.mv.entity.DailyCommentsReadUsers;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.DailyCommentsService;
import jp.co.yrc.mv.validator.annotation.ByteLength;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class DailyReportController {

	@Autowired
	DailyCommentsService dailyCommentsService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@RequestMapping(value = "/dailyBossComment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DailyComments saveMonthlyComment(@Valid @RequestBody DailyCommentParam comments, Principal principal) {
		String userId = userDetailsHelper.getUserId(principal);
		comments.setModifiedUserId(userId);
		comments.setCreatedBy(userId);
		DailyComments result = dailyCommentsService.saveBossComment(comments);
		return result;
	}

	@RequestMapping(value = "/dailyCommentConfirm", method = RequestMethod.POST)
	public List<ConfirmResult> confirm(@RequestBody ConfirmParam param, Principal principal) {
		List<DailyCommentsReadUsers> list = dailyCommentsService.confirm(param.id,
				userDetailsHelper.getUserId(principal));
		List<ConfirmResult> result = new ArrayList<>();

		UserInfoDto user = userDetailsHelper.getUserInfo(principal);

		SimpleDateFormat format = Constants.DateFormat.yyyyMMdd.getFormat();
		for (DailyCommentsReadUsers o : list) {
			ConfirmResult r = new ConfirmResult();

			r.firstName = user.getFirstName();
			r.lastName = user.getLastName();
			r.id = o.getDailyCommentId();
			r.date = format.format(o.getDateModified());
			result.add(r);

		}
		return result;
	}

	public static class ConfirmResult {
		String id;
		String firstName;
		String lastName;
		String date;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

	}

	public static class ConfirmParam {
		public Set<String> id;
	}

	public static class DailyCommentParam extends DailyComments {

		@ByteLength(max = 65535)
		@Override
		public String getBossComment() {
			return super.getBossComment();
		}
	}
}
