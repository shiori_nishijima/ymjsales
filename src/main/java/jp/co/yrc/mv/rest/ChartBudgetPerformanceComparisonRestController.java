package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.Div;
import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.dto.EqualsEigyoSosikiDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.SimpleResult;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.MonthlyKindPlans;
import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.helper.KindHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.MstEigyoSosikiService;
import jp.co.yrc.mv.service.PlansService;
import jp.co.yrc.mv.service.ResultsService;
import jp.co.yrc.mv.service.UsersService;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class ChartBudgetPerformanceComparisonRestController {

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	KindHelper kindHelper;

	@Autowired
	ResultsService resultsService;

	@Autowired
	PlansService plansService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	UsersService usersService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	/**
	 * グラフ用情報を取得します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @return 結果オブジェクト
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/chartBudgetPerformanceComparisonData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result getData(@RequestBody Param param, Principal principal) {

		Result result = new Result();

		param.setDiv1(soshikiHelper.checkSoshikiParamValue(param.getDiv1()));
		param.setDiv2(soshikiHelper.checkSoshikiParamValue(param.getDiv2()));
		param.setDiv3(soshikiHelper.checkSoshikiParamValue(param.getDiv3()));

		UserInfoDto loginUser = userDetailsHelper.getUserInfo(principal);

		Role auth = Role.getByCode(loginUser.getAuthorization());
		auth = auth.getParent();

		int lastYearMonth = DateUtils.toYearMonth(param.getYear(), param.getMonth());

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		boolean isHistory = lastYearMonth < nowYearMonth;

		Set<EqualsEigyoSosikiDto> mySet = new LinkedHashSet<>();

		List<EigyoSosikiDto> sosokiList = Role.All.equals(auth) ? new ArrayList<EigyoSosikiDto>()
				: mstEigyoSosikiService.listMySosiki(loginUser.getCorpCodes(), loginUser.getId(), param.getYear(),
						param.getMonth());

		for (EigyoSosikiDto o : sosokiList) {
			EqualsEigyoSosikiDto copy = dozerBeanMapper.map(o, EqualsEigyoSosikiDto.class);
			mySet.add(copy);
		}

		List<EigyoSosikiDto> currentSosikiList = isHistory ? (Role.All.equals(auth) ? Collections.EMPTY_LIST
				: mstEigyoSosikiService.listMySosikiTreeFromHistory(loginUser.getCorpCodes(), loginUser.getId(),
						param.getYear(), param.getMonth())) : Collections.EMPTY_LIST;

		for (EigyoSosikiDto o : currentSosikiList) {
			EqualsEigyoSosikiDto copy = dozerBeanMapper.map(o, EqualsEigyoSosikiDto.class);
			mySet.add(copy);
		}

		List<EqualsEigyoSosikiDto> mySosikiList = new ArrayList<>(mySet);
		Collections.sort(mySosikiList);

		if (StringUtils.isEmpty(param.getDiv3())) {
			List<MstEigyoSosiki> list;
			// DVI1が無い場合は初期表示と見なす。
			// 権限に応じて作る
			if (StringUtils.isEmpty(param.getDiv1())) {
				result.div = roleDefault(auth);
				list = createTargetSosikiList(param.getYear(), param.getMonth(), loginUser, auth, mySosikiList);
			} else {

				result.div = param.getDiv2() != null ? Div.DIV3 : param.getDiv1() != null ? Div.DIV2 : Div.DIV1;

				list = mstEigyoSosikiService.listCompany(Collections.singletonList(param.getDiv1()), param.getDiv2(),
						null, null, userDetailsHelper.getUserInfo(principal).getCorpCodes(), param.getYear(),
						param.getMonth());

			}

			for (MstEigyoSosiki sosiki : list) {

				sosiki.setDiv1Cd(soshikiHelper.checkSoshikiParamValue(sosiki.getDiv1Cd()));
				sosiki.setDiv2Cd(soshikiHelper.checkSoshikiParamValue(sosiki.getDiv2Cd()));
				sosiki.setDiv3Cd(soshikiHelper.checkSoshikiParamValue(sosiki.getDiv3Cd()));

				if (isTargetUser(mySosikiList, sosiki.getDiv1Cd(), sosiki.getDiv2Cd(), sosiki.getDiv3Cd(), auth)) {

					Data data = new Data();

					data.setDiv1Cd(sosiki.getDiv1Cd());
					data.setDiv2Cd(sosiki.getDiv2Cd());
					data.setDiv3Cd(sosiki.getDiv3Cd());

					String divName;
					if (StringUtils.isEmpty(sosiki.getDiv2Cd())) {
						divName = sosiki.getDiv1Nm();
					} else if (StringUtils.isEmpty(sosiki.getDiv3Cd())) {
						divName = sosiki.getDiv2Nm();
					} else {
						divName = sosiki.getDiv3Nm();
					}
					divName = StringUtils.defaultString(divName);
					String shrotName = StringUtils.defaultString(sosiki.getEigyoSosikiShortNm());
					String longName = StringUtils.defaultString(sosiki.getEigyoSosikiNm());

					String[] strs = { divName, shrotName, longName };

					String min = strs[0];
					for (int i = 0; i < strs.length; i++) {
						if (min.length() < 1 || (strs[i].length() > 0 && min.length() > strs[i].length())) {
							min = strs[i];
						}
					}
					data.setName(min);

					// 実績
					MonthlyKindResults monthlyKindResults = resultsService.sumMonthlyKindResult(param.getYear(),
							param.getMonth(), null, Collections.singletonList(sosiki.getDiv1Cd()), sosiki.getDiv2Cd(),
							sosiki.getDiv3Cd());

					kindHelper.setupResult(data.results, monthlyKindResults);

					// 前年実績
					MonthlyKindResults lastYearMonthlyKindResults = resultsService.sumMonthlyKindResult(
							param.getYear() - 1, param.getMonth(), null, Collections.singletonList(sosiki.getDiv1Cd()),
							sosiki.getDiv2Cd(), sosiki.getDiv3Cd());
					kindHelper.setupResult(data.lastYeraResults, lastYearMonthlyKindResults);

					// 計画
					MonthlyKindPlans monthlyKindPlans = plansService.sumMonthlyKindPlan(param.getYear(),
							param.getMonth(), null, Collections.singletonList(sosiki.getDiv1Cd()), sosiki.getDiv2Cd(),
							sosiki.getDiv3Cd());

					kindHelper.setupPlan(data.plans, monthlyKindPlans);
					result.data.add(data);
				}
			}

		} else {
			result.div = Div.SALES;
			List<UserSosikiDto> list = usersService.listUser(null, Collections.singletonList(param.getDiv1()),
					param.getDiv2(), param.getDiv3(), param.getYear(), param.getMonth());

			for (UserSosikiDto user : list) {

				user.setDiv1(soshikiHelper.checkSoshikiParamValue(user.getDiv1()));
				user.setDiv2(soshikiHelper.checkSoshikiParamValue(user.getDiv2()));
				user.setDiv3(soshikiHelper.checkSoshikiParamValue(user.getDiv3()));

				// 過去、現在所属している組織のセールスのみを表示する
				if (isTargetUser(mySosikiList, user.getDiv1(), user.getDiv2(), user.getDiv3(), auth)) {
					Data data = new Data();

					data.setDiv1Cd(user.getDiv1());
					data.setDiv2Cd(user.getDiv2());
					data.setDiv3Cd(user.getDiv3());
					data.setTanto(user.getId());
					data.setName(user.getLastName() + user.getFirstName());

					// 実績
					MonthlyKindResults monthlyKindResults = resultsService.sumMonthlyKindResult(param.getYear(),
							param.getMonth(), user.getId(), Collections.singletonList(user.getDiv1()), user.getDiv2(),
							user.getDiv3());
					kindHelper.setupResult(data.results, monthlyKindResults);

					// 前年実績
					MonthlyKindResults lastYearMonthlyKindResults = resultsService.sumMonthlyKindResult(
							param.getYear() - 1, param.getMonth(), user.getId(),
							Collections.singletonList(user.getDiv1()), user.getDiv2(), user.getDiv3());
					kindHelper.setupResult(data.lastYeraResults, lastYearMonthlyKindResults);

					// 計画
					MonthlyKindPlans monthlyKindPlans = plansService.sumMonthlyKindPlan(param.getYear(),
							param.getMonth(), user.getId(), Collections.singletonList(user.getDiv1()), user.getDiv2(),
							user.getDiv3());

					kindHelper.setupPlan(data.plans, monthlyKindPlans);
					result.data.add(data);
				}
			}

		}
		return result;
	}

	protected boolean isTargetUser(List<? extends EigyoSosikiDto> sosikiList, String div1, String div2, String div3,
			Role auth) {

		if (Constants.Role.All.equals(auth)) {
			return true;
		}
		for (MstEigyoSosiki mySosiki : sosikiList) {

			// 自分の所属する会社のみ
			if (Constants.Role.Multiple.equals(auth) || Constants.Role.Company.equals(auth)) {

				if (div1.equals(mySosiki.getDiv1Cd())) {
					return true;
				}

			}
			// 上記以外は権限に応じて自分の所属する組織
			else if (Constants.Role.Department.equals(auth)) {

				if (div1.equals(mySosiki.getDiv1Cd()) && div2.equals(mySosiki.getDiv2Cd())) {
					return true;
				}

			}
			// sales
			else if (Constants.Role.SalesOffice.equals(auth) || Constants.Role.Sales.equals(auth)) {

				if (div1.equals(mySosiki.getDiv1Cd()) && div2.equals(mySosiki.getDiv2Cd())
						&& div3.equals(mySosiki.getDiv3Cd())) {
					return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	protected List<MstEigyoSosiki> createTargetSosikiList(Integer year, Integer month, UserInfoDto loginUser,
			Role auth, List<? extends EigyoSosikiDto> mySosikiList) {
		List<MstEigyoSosiki> list;
		// 全社
		if (Constants.Role.All.equals(auth)) {
			list = mstEigyoSosikiService.listCompany(Collections.EMPTY_LIST, null, null, null,
					loginUser.getCorpCodes(), year, month);
		}
		// 自分の所属する会社のみ
		else if (Constants.Role.Multiple.equals(auth) || Constants.Role.Company.equals(auth)) {

			List<String> div1 = new ArrayList<>();

			for (EigyoSosikiDto my : mySosikiList) {
				div1.add(my.getDiv1Cd());
			}

			list = mstEigyoSosikiService.listBelongCompany(div1, null, loginUser.getCorpCodes(), year, month);

		}
		// 上記以外は権限に応じて自分の所属する組織
		else {

			list = new ArrayList<>();

			// 所属する部門
			if (Constants.Role.Department.equals(auth)) {
				Set<EqualsEigyoSosikiDto> set = new LinkedHashSet<>();
				for (EigyoSosikiDto sosiki : mySosikiList) {
					if (StringUtils.isNotEmpty(sosiki.getDiv1Cd()) && StringUtils.isNotEmpty(sosiki.getDiv2Cd())) {

						EqualsEigyoSosikiDto mstEigyoSosiki = dozerBeanMapper.map(mstEigyoSosikiService.findIdByDivCd(
								sosiki.getDiv1Cd(), sosiki.getDiv2Cd(), null, year, month), EqualsEigyoSosikiDto.class);

						set.add(mstEigyoSosiki);
					}
				}
				list.addAll(set);
			}
			// sales
			else if (Constants.Role.SalesOffice.equals(auth) || Constants.Role.Sales.equals(auth)) {
				for (EigyoSosikiDto sosiki : mySosikiList) {
					if (StringUtils.isNotEmpty(sosiki.getDiv1Cd()) && StringUtils.isNotEmpty(sosiki.getDiv2Cd())
							&& StringUtils.isNotEmpty(sosiki.getDiv3Cd())) {
						EqualsEigyoSosikiDto mstEigyoSosiki = dozerBeanMapper.map(sosiki, EqualsEigyoSosikiDto.class);
						list.add(mstEigyoSosiki);
					}
				}
			}
		}
		return list;
	}

	public Div roleDefault(Role role) {

		switch (role) {
		case All:
		case Multiple:
		case Company:
			return Div.DIV1;
		case Department:
			return Div.DIV2;
		case SalesOffice:
		case Sales:
			return Div.DIV3;
		default:
			return Div.SALES;
		}
	}

	public static class Result {

		Div div;
		List<Data> data = new ArrayList<>();

		public Div getDiv() {
			return div;
		}

		public void setDiv(Div div) {
			this.div = div;
		}

		public List<Data> getData() {
			return data;
		}

		public void setData(List<Data> data) {
			this.data = data;
		}

	}

	public static class Data {

		String div1Cd;
		String div2Cd;
		String div3Cd;
		String tanto;
		String name;

		Map<String, SimpleResult> results = new LinkedHashMap<>();
		Map<String, SimpleResult> lastYeraResults = new LinkedHashMap<>();
		Map<String, SimpleResult> plans = new LinkedHashMap<>();

		public String getDiv1Cd() {
			return div1Cd;
		}

		public void setDiv1Cd(String div1Cd) {
			this.div1Cd = div1Cd;
		}

		public String getDiv2Cd() {
			return div2Cd;
		}

		public void setDiv2Cd(String div2Cd) {
			this.div2Cd = div2Cd;
		}

		public String getDiv3Cd() {
			return div3Cd;
		}

		public void setDiv3Cd(String div3Cd) {
			this.div3Cd = div3Cd;
		}

		public Map<String, SimpleResult> getResults() {
			return results;
		}

		public void setResults(Map<String, SimpleResult> results) {
			this.results = results;
		}

		public Map<String, SimpleResult> getPlans() {
			return plans;
		}

		public void setPlans(Map<String, SimpleResult> plans) {
			this.plans = plans;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTanto() {
			return tanto;
		}

		public void setTanto(String tanto) {
			this.tanto = tanto;
		}

		public Map<String, SimpleResult> getLastYeraResults() {
			return lastYeraResults;
		}

		public void setLastYeraResults(Map<String, SimpleResult> lastYeraResults) {
			this.lastYeraResults = lastYeraResults;
		}

	}

	static class Param extends SearchConditionBaseDto {

	}

}
