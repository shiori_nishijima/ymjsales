package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.SimpleResult;
import jp.co.yrc.mv.entity.MonthlyKindPlans;
import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.helper.KindHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.PlansService;
import jp.co.yrc.mv.service.ResultsService;

@RestController
@Transactional
public class ChartKindAchievementRateRestController {

	@Autowired
	ResultsService resultsService;

	@Autowired
	PlansService plansService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	KindHelper kindHelper;

	@RequestMapping(value = "/chartKindAchievementRateData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Result getData(@RequestBody Param param, Principal principal) {

		Result result = new Result();

		// 組織のhistoryで存在しないデータを選択したとき
		if (!param.forceEmpty) {
			String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
			String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
			String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
			String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

			List<String> div1List = soshikiHelper.createSearchSalesCompanyCodeList(div1,
					userDetailsHelper.getUserInfo(principal), param.getYear(), param.getMonth());

			// 実績
			MonthlyKindResults monthlyKindResults = resultsService.sumMonthlyKindResult(param.getYear(),
					param.getMonth(), tanto, div1List, div2, div3);
			kindHelper.setupResult(result.results, monthlyKindResults);

			// 前年実績
			MonthlyKindResults lastYearMonthlyKindResults = resultsService.sumMonthlyKindResult(param.getYear() - 1,
					param.getMonth(), tanto, div1List, div2, div3);
			kindHelper.setupResult(result.lastYearResults, lastYearMonthlyKindResults);

			// 計画
			MonthlyKindPlans monthlyKindPlans = plansService.sumMonthlyKindPlan(param.getYear(), param.getMonth(),
					tanto, div1List, div2, div3);

			kindHelper.setupPlan(result.plans, monthlyKindPlans);
		}
		return result;
	}

	public static class Result {

		String div1Cd;
		String div2Cd;
		String div3Cd;
		String tanto;
		String name;

		Map<String, SimpleResult> results = new LinkedHashMap<>();
		Map<String, SimpleResult> lastYearResults = new LinkedHashMap<>();
		Map<String, SimpleResult> plans = new LinkedHashMap<>();

		public String getDiv1Cd() {
			return div1Cd;
		}

		public void setDiv1Cd(String div1Cd) {
			this.div1Cd = div1Cd;
		}

		public String getDiv2Cd() {
			return div2Cd;
		}

		public void setDiv2Cd(String div2Cd) {
			this.div2Cd = div2Cd;
		}

		public String getDiv3Cd() {
			return div3Cd;
		}

		public void setDiv3Cd(String div3Cd) {
			this.div3Cd = div3Cd;
		}

		public Map<String, SimpleResult> getResults() {
			return results;
		}

		public void setResults(Map<String, SimpleResult> results) {
			this.results = results;
		}

		public Map<String, SimpleResult> getPlans() {
			return plans;
		}

		public void setPlans(Map<String, SimpleResult> plans) {
			this.plans = plans;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTanto() {
			return tanto;
		}

		public void setTanto(String tanto) {
			this.tanto = tanto;
		}

		public Map<String, SimpleResult> getLastYearResults() {
			return lastYearResults;
		}

		public void setLastYearResults(Map<String, SimpleResult> lastYearResults) {
			this.lastYearResults = lastYearResults;
		}

	}

	public static class Param extends SearchConditionBaseDto {

		boolean forceEmpty;

		public boolean isForceEmpty() {
			return forceEmpty;
		}

		public void setForceEmpty(boolean forceEmpty) {
			this.forceEmpty = forceEmpty;
		}

	}
}
