package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.Set;

import jp.co.yrc.mv.entity.TechServiceCalls;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
@Transactional
@RequestMapping(value = { "/visitListRead", "/tech/visitListRead" })
public class VisitListReadController {

	@Autowired
	CallsService callsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void toRead(@RequestBody RegisterParam param, Principal principal) {
		
		callsService.toRead(param.calls, userDetailsHelper.getUserId(principal));
	}

	public static class RegisterParam {
		public Set<TechServiceCalls> calls;
	}
	
}
