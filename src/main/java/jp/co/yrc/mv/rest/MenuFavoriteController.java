package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jp.co.yrc.mv.entity.MenuFavorites;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.MenuFavoritesService;

@RestController
@Transactional
@RequestMapping({ "/menuFavorite" })
public class MenuFavoriteController {

	@Autowired
	MenuFavoritesService menuFavoritesService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> list(Principal principal) {

		String userId = userDetailsHelper.getUserId(principal);

		List<String> result = new ArrayList<>();

		for (MenuFavorites m : menuFavoritesService.listByUserId(userId)) {

			result.add(m.getUrl());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, String> update(@RequestBody List<String> urls, Principal principal) {

		String userId = userDetailsHelper.getUserId(principal);

		menuFavoritesService.updateFavorites(urls, userId);

		return Collections.EMPTY_MAP;
	}

}
