package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.CommentsDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.Comments;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.TechServiceComments;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.CommentsService;
import jp.co.yrc.mv.service.MstEigyoSosikiService;
import jp.co.yrc.mv.service.UsersService;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
@RequestMapping({ "/visitComment", "/tech/visitComment" })
public class VisitCommentController {

	@Autowired
	CommentsService commentsService;

	@Autowired
	CallsService callsService;

	@Autowired
	UsersService usersService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@Value("${sfa.call.url}")
	String sfaCallUrl;

	/**
	 * コメント情報を取得します。
	 * 
	 * @param callId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{callId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CallsResult getComments(@PathVariable String callId, String techCallId, Principal principal) {
		// コメント表示しただけでは既読にしない
		String userId = userDetailsHelper.getUserId(principal);
		SimpleDateFormat format = Constants.DateFormat.yyyyMMddHHmm.getFormat();
		SimpleDateFormat timeFormat = Constants.DateFormat.Time.getFormat();
		CallsResult result = new CallsResult();
		boolean isNotTech = StringUtils.isEmpty(techCallId);

		CallsDto c = isNotTech ? callsService.findCommentsInfo(callId, userId) : callsService.findCommentsInfo4Tech(
				callId, userId);
		result.id = c.getId();
		result.userId = userId; // ログインユーザIDを返す
		result.name = c.getName();
		result.dateFrom = format.format(c.getDateStart());
		result.dateTo = timeFormat.format(c.getTimeEndC());
		result.detail = c.getComFreeC();
		result.likeCount = c.getCallsLikeCount();
		result.isLike = StringUtils.isNotEmpty(c.getCallsLikeId());
		result.gmailLink = sfaCallUrl + callId;

		result.callId = techCallId;

		Calendar dataStartCal = DateUtils.getCalendar();
		dataStartCal.setTime(c.getDateStart());

		UserSosikiDto callsUserSosiki = usersService.selectUserById(c.getAssignedUserId(), Collections.EMPTY_LIST,
				null, null, dataStartCal.get(Calendar.YEAR), dataStartCal.get(Calendar.MONTH) + 1);

		result.firstName = callsUserSosiki.getFirstName();
		result.lastName = callsUserSosiki.getLastName();
		result.eigyoSosikiNm = callsUserSosiki.getEigyoSosikiNm();

		List<CommentsDto> comments = isNotTech ? commentsService.listCallsComment(callId) : commentsService
				.listCallsComment4Tech(callId);

		for (CommentsDto com : comments) {
			CommentsResult r = new CommentsResult();
			r.description = com.getDescription();
			r.id = com.getId();
			r.userId = com.getAssignedUserId();

			r.name = com.getName();
			r.date = format.format(com.getDateModified());

			Calendar comEditCal = Calendar.getInstance();
			comEditCal.setTime(com.getDateModified());

			UserSosikiDto commentUserSosiki = usersService.selectUserById(com.getAssignedUserId(),
					Collections.EMPTY_LIST, null, null, comEditCal.get(Calendar.YEAR),
					comEditCal.get(Calendar.MONTH) + 1);

			r.firstName = commentUserSosiki.getFirstName();
			r.lastName = commentUserSosiki.getLastName();
			r.eigyoSosikiNm = commentUserSosiki.getEigyoSosikiNm();
			r.userName = commentUserSosiki.getIdForCustomer();

			r.tech = !isNotTech;
			result.comments.add(r);
		}
		return result;
	}

	/**
	 * コメントを登録します。
	 * 
	 * @param param
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommentsResult register(@Valid @RequestBody RegisterParam param, Principal principal) {

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);

		Comments c = new Comments();
		c.setDescription(param.comment);
		c.setParentId(param.callId);
		c.setConfirmation(false);

		if (StringUtils.isEmpty(param.techCallId)) {
			commentsService.save(c, userInfo.getId());
			// コメントリスト取得時に既読としないので登録時に操作者を既読とする
			commentsService.toRead(param.callId, userInfo.getId());
			commentsService.toUnread(param.callId, userInfo.getId());
		} else {
			commentsService.save4Tech(c, userInfo.getId());
			// コメントリスト取得時に既読としないので登録時に操作者を既読とする
			commentsService.toRead4Tech(param.callId, userInfo.getId());
			commentsService.toUnread4Tech(param.callId, userInfo.getId());
		}

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		List<MstEigyoSosiki> eigyoSosikis = mstEigyoSosikiService.findByUserId(userInfo.getCorpCodes(),
				userInfo.getId(), now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		CommentsResult result = new CommentsResult();
		result.description = c.getDescription();
		result.id = c.getId();
		result.userName = userInfo.getIdForCustomer();
		result.lastName = userInfo.getLastName();
		result.firstName = userInfo.getFirstName();
		if (!eigyoSosikis.isEmpty()) {
			result.eigyoSosikiNm = eigyoSosikis.get(0).getEigyoSosikiNm();
		}
		result.name = c.getName();
		result.commentCount = StringUtils.isEmpty(param.techCallId) ? commentsService
				.countNonConfirmationCallsComment(param.callId) : commentsService
				.countNonConfirmationCallsComment4Tech(param.callId);
		result.date = Constants.DateFormat.yyyyMMddHHmm.getFormat().format(c.getDateModified());
		result.userId = userInfo.getId();
		result.tech = StringUtils.isNotEmpty(param.techCallId);
		return result;
	}

	@RequestMapping(value = "/like/{callId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void like(@PathVariable String callId, String techCallId, Principal principal) {
		String userId = userDetailsHelper.getUserId(principal);
		if (StringUtils.isEmpty(techCallId)) {
			callsService.like(callId, userId);
		} else {
			callsService.like4Tech(callId, userId);
		}

	}

	@RequestMapping(value = "/like/{callId}/{tech}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void unlike(@PathVariable String callId, @PathVariable boolean tech, Principal principal) {
		String userId = userDetailsHelper.getUserId(principal);

		if (!tech) {
			callsService.unlike(callId, userId);
		} else {
			callsService.unlike4Tech(callId, userId);
		}
	}

	/**
	 * コメント確認済としてデータを登録します。
	 * 
	 * @param callId
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/read/{callId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommentsResult read(@PathVariable String callId, String techCallId, Principal principal) {

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
		Comments c = new Comments();
		c.setDescription(message.getMessage("comment.confirmed"));
		c.setParentId(callId);
		c.setConfirmation(true);

		if (StringUtils.isEmpty(techCallId)) {

			commentsService.save(c, userInfo.getId());
			// コメントリスト取得時に既読としないので確認済みマーク時に操作者を既読とする
			commentsService.toRead(callId, userInfo.getId());
		} else {
			commentsService.save4Tech(c, userInfo.getId());
			// コメントリスト取得時に既読としないので確認済みマーク時に操作者を既読とする
			commentsService.toRead4Tech(callId, userInfo.getId());

		}

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());

		List<MstEigyoSosiki> eigyoSosikis = mstEigyoSosikiService.findByUserId(userInfo.getCorpCodes(),
				userInfo.getId(), now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		CommentsResult result = new CommentsResult();
		result.description = c.getDescription();
		result.id = c.getId();
		result.userName = userInfo.getIdForCustomer();
		result.lastName = userInfo.getLastName();
		result.firstName = userInfo.getFirstName();
		result.name = c.getName();
		if (!eigyoSosikis.isEmpty()) {
			result.eigyoSosikiNm = eigyoSosikis.get(0).getEigyoSosikiNm();
		}

		result.commentCount = StringUtils.isEmpty(techCallId) ? commentsService
				.countNonConfirmationCallsComment(callId) : commentsService
				.countNonConfirmationCallsComment4Tech(callId);

		result.date = Constants.DateFormat.yyyyMMddHHmm.getFormat().format(c.getDateModified());
		result.userId = userInfo.getId();
		result.tech = StringUtils.isNotEmpty(techCallId);
		return result;
	}

	@RequestMapping(value = "/{id}/{tech}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public CommentsResult delete(@PathVariable String id, @PathVariable Boolean tech) {
		CommentsResult result = new CommentsResult();

		if (!tech) {
			Comments comment = commentsService.findOne(id);
			commentsService.delete(comment);
			// コメント有無の表示変更用に返す
			result.commentCount = commentsService.countNonConfirmationCallsComment(comment.getParentId());
		} else {
			TechServiceComments comment = commentsService.findOne4Tech(id);
			commentsService.delete4Tech(comment);
			// コメント有無の表示変更用に返す
			result.commentCount = commentsService.countNonConfirmationCallsComment4Tech(comment.getParentId());
		}

		return result;
	}

	public static class RegisterParam {
		public String callId;
		public String techCallId;
		@NotBlank
		@Length(max = 200)
		public String comment;
	}

	public static class CallsResult {
		public String id;
		public String callId;
		public String userId;
		public String dateFrom;
		public String dateTo;
		public String name;
		public String firstName;
		public String lastName;
		public String eigyoSosikiNm;
		public String detail;
		public int likeCount;
		public boolean isLike;
		public List<CommentsResult> comments = new ArrayList<>();
		public String gmailLink;
	}

	public static class CommentsResult {
		public String id;
		public String callId;
		public String userId;
		public String name;
		public String firstName;
		public String lastName;
		public String userName;
		public String description;
		public String date;
		public String eigyoSosikiNm;
		public int commentCount;
		public boolean tech;
	}
}
