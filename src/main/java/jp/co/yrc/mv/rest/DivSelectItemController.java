package jp.co.yrc.mv.rest;

import java.security.Principal;
import java.text.ParseException;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.helper.DivSelectItemHelper;
import jp.co.yrc.mv.helper.HideAllLabelDivSelectItemHelper;
import jp.co.yrc.mv.helper.OnlySalesOfficeTantoAllLabelDivSelectItemHelper;
import jp.co.yrc.mv.helper.SameLevelSalesOfficeTantoAllDivSelectItemHelper;
import jp.co.yrc.mv.helper.SearchDivHelper;
import jp.co.yrc.mv.helper.VisitListDivSelectItemHelper;
import jp.co.yrc.mv.html.DivSelectItem;

@RestController
@Transactional
@RequestMapping(value = { "/tech/divSelectItem", "/divSelectItem" })
public class DivSelectItemController {

	@Autowired
	SearchDivHelper searchDivHelper;
	
	@Autowired
	@Qualifier("divSelectItemHelper")
	DivSelectItemHelper divHelper;
	
	@Autowired
	VisitListDivSelectItemHelper visitListDivHelper;
	
	@Autowired
	HideAllLabelDivSelectItemHelper hideAllLabelDivHelper;
	
	@Autowired
	SameLevelSalesOfficeTantoAllDivSelectItemHelper sameLevelSalesOfficeTantoAllDivHelper;
	
	@Autowired
	OnlySalesOfficeTantoAllLabelDivSelectItemHelper onlySalesOfficeTantoAllLabelDivHelper;
	
	/**
	 * 組織ドロップダウン項目を作成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param model モデル
	 * @return ドロップダウン項目
	 * @throws ParseException
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DivSelectItem create(@RequestBody Param param, Principal principal, Model model) throws ParseException {
		YearMonth yearMonth = createYearMonth(param);
		DivSelectItem result = divHelper.create(model, principal, yearMonth.year, yearMonth.month);
		if (Boolean.valueOf(param.deleteDiv1All)) {
			divHelper.removeAllDiv1(result);
		}
		return result;
	}

	/**
	 * 訪問実績用組織ドロップダウン項目を作成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param model モデル
	 * @return ドロップダウン項目
	 * @throws ParseException
	 */
	@RequestMapping(value = "/createVisitList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DivSelectItem createVisitList(@RequestBody Param param, Principal principal, Model model)
			throws ParseException {
		YearMonth yearMonth = createYearMonth(param);
		DivSelectItem result = visitListDivHelper.create(model, principal, yearMonth.year, yearMonth.month);
		if (Boolean.valueOf(param.deleteDiv1All)) {
			visitListDivHelper.removeAllDiv1(result);
		}
		return result;
	}

	/**
	 * 「全て」をブランク表示する組織ドロップダウン項目を作成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param model モデル
	 * @return ドロップダウン項目
	 * @throws ParseException
	 */
	@RequestMapping(value = "/createHideAllLabel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DivSelectItem createHideAllLabel(@RequestBody Param param, Principal principal, Model model)
			throws ParseException {
		YearMonth yearMonth = createYearMonth(param);
		DivSelectItem result = hideAllLabelDivHelper.create(model, principal, yearMonth.year, yearMonth.month);
		if (Boolean.valueOf(param.deleteDiv1All)) {
			hideAllLabelDivHelper.removeAllDiv1(result);
		}
		return result;
	}
	
	/**
	 * 同一レベルの他部署は営業所の担当者「全て」まで閲覧可能とする組織ドロップダウン項目を作成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param model モデル
	 * @return ドロップダウン項目
	 * @throws ParseException
	 */
	@RequestMapping(value = "/createSameLevelSalesOfficeTantoAll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DivSelectItem createSameLevelSalesOfficeTantoAll(@RequestBody Param param, Principal principal, Model model)
			throws ParseException {
		YearMonth yearMonth = createYearMonth(param);
		DivSelectItem result = sameLevelSalesOfficeTantoAllDivHelper.create(model, principal, yearMonth.year, yearMonth.month);
		if (Boolean.valueOf(param.deleteDiv1All)) {
			sameLevelSalesOfficeTantoAllDivHelper.removeAllDiv1(result);
		}
		return result;
	}
	
	/**
     * 営業所の担当者のみ「全て」を表示する組織ドロップダウン項目を作成するヘルパー。 
	 * それ以外のレベルは「全て」をブランク表示する。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param model モデル
	 * @return ドロップダウン項目
	 * @throws ParseException
	 */
	@RequestMapping(value = "/createOnlySalesOfficeTantoAllLabel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DivSelectItem createOnlySalesOfficeTantoAllLabel(@RequestBody Param param, Principal principal, Model model)
			throws ParseException {
		YearMonth yearMonth = createYearMonth(param);
		DivSelectItem result = onlySalesOfficeTantoAllLabelDivHelper.create(model, principal, yearMonth.year, yearMonth.month);
		if (Boolean.valueOf(param.deleteDiv1All)) {
			onlySalesOfficeTantoAllLabelDivHelper.removeAllDiv1(result);
		}
		return result;
	}
	
	/**
	 * 年月を生成します。
	 * 
	 * @param param パラメータ
	 * @return 年月オブジェクト
	 * @throws ParseException 
	 */
	private YearMonth createYearMonth(Param param) throws ParseException {
		YearMonth yearMonth = new YearMonth();
		if (param.date != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(Constants.DateFormat.yyyyMMdd.getFormat().parse(param.date));
			yearMonth.year = c.get(Calendar.YEAR);
			yearMonth.month = c.get(Calendar.MONTH) + 1;
		} else if (param.year != null && param.month != null) {
			yearMonth.year = Integer.parseInt(param.year);
			yearMonth.month = Integer.parseInt(param.month);
		}
		return yearMonth;
	}

	/**
	 * 年月。
	 */
	public static class YearMonth {
		int year;
		int month;
	}
	
	/**
	 * リクエストパラメータ。
	 */
	public static class Param {
		String year;
		String month;
		String date;
		String deleteDiv1All;

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}

		public String getMonth() {
			return month;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getDeleteDiv1All() {
			return deleteDiv1All;
		}

		public void setDeleteDiv1All(String deleteDiv1All) {
			this.deleteDiv1All = deleteDiv1All;
		}

	}

}
