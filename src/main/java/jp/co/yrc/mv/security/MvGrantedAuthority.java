package jp.co.yrc.mv.security;

import jp.co.yrc.mv.common.Constants.Role;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.util.Assert;

/**
 * MVのRollを設定した権限。
 *
 */
public final class MvGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	private final Role role;

	public MvGrantedAuthority(Role role) {
		Assert.hasText(role.getRole(), "A granted authority textual representation is required");
		this.role = role;
	}

	public String getAuthority() {
		return role.getRole();
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj instanceof MvGrantedAuthority) {
			return role == (((MvGrantedAuthority) obj).role);
		}

		return false;
	}

	public String getCode() {
		return this.role.getCode();
	}

	public int hashCode() {
		return this.role.hashCode();
	}

	public String toString() {
		return this.role.getRole();
	}
}
