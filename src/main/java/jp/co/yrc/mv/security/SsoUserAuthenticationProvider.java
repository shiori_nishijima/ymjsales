package jp.co.yrc.mv.security;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import jp.co.yrc.mv.dto.UserInfoDto;

/**
 * SSOの認証処理をする。
 */
@Component
public class SsoUserAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserDetailsService mvUserDetailsService;

	/** デフォルトの日付フォーマット。 */
	private static final String DATE_FORMAT_DEFAULT = "yyyyMMddHHmmss";

	/** デフォルトのタイムスタンプ誤差。 */
	private static final long DIFF_TIME = 5 * 60 * 1000;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		SsoUserAuthenticationToken authenticationToken = (SsoUserAuthenticationToken) authentication;

		String username = authenticationToken.getName();
		String linkId = authenticationToken.getLinkId();
		String kaishaCd = authenticationToken.getKaishaCd();
		String ts = authenticationToken.getTs();
		String sg = authenticationToken.getSg();
		String locale = authenticationToken.getLocale();

		List<String> list = new ArrayList<String>();
		list.add(username);
		list.add(linkId);
		list.add(kaishaCd);
		list.add(ts);

		if (validateSignature(sg, list.toArray(new String[list.size()]))) {
			UserInfoDto result = (UserInfoDto) mvUserDetailsService.loadUserByUsername(username);

			if (result != null) {
				LocaleContextHolder.setLocale(new Locale(locale)); // dashboard.htmlに行く前にエラーになった場合用。
				return new SsoUserAuthenticationToken(result, authentication.getCredentials(),
						linkId, kaishaCd, ts, sg, locale, result.getAuthorities());
			}
		} else {
			throw new UsernameNotFoundException("jp.co.yrc.mv.error.login");
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return SsoUserAuthenticationToken.class.equals(authentication);
	}

	/**
	 * タイムスタンプを生成します。
	 * 
	 * @return タイムスタンプ
	 */
	public static String makeTimeStamp() {

		DateFormat f = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
		return f.format(new Date());
	}

	/**
	 * シグネチャを作成します。
	 * 
	 * @param datas
	 *            シグネチャの作成に必要なデータ群
	 * @return シグネチャ
	 */
	public static String makeCertSignature(String[] datas) {

		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < datas.length; i++) {
			buf.append(datas[i]).append("&");
		}

		if (buf.length() > 0) {
			buf.deleteCharAt(buf.length() - 1);
		}

		// SHA-1メッセージダイジェスト値を計算
		byte[] raw = makeSHA1Digest(buf.toString());

		// BASE64形式でエンコード
		return base64Encode(raw);
	}

	/**
	 * シグネチャが正しいかチェックします。
	 * 
	 * @param signature
	 *            シグネチャ
	 * @param datas
	 *            シグネチャの作成に必要なデータ群
	 * @return シグネチャが正しい場合、<code>true</code>
	 */
	public static boolean validateSignature(String signature, String[] datas) {
		return makeCertSignature(datas).equals(signature);
	}

	/**
	 * タイムスタンプが正しいかチェックします。
	 * 
	 * @param timeStamp
	 *            タイムスタンプ(フォーマット:yyyyMMddHHmmss)
	 * @return タイムスタンプが正しい場合、<code>true</code>
	 */
	public static boolean validateTimeStamp(String timeStamp) {
		return validateTimeStamp(timeStamp, DIFF_TIME);
	}

	/**
	 * タイムスタンプが正しいかチェックします。
	 * 
	 * @param timeStamp
	 *            タイムスタンプ(フォーマット:yyyyMMddHHmmss)
	 * @param diffTime
	 *            タイムスタンプ誤差
	 * @return タイムスタンプが正しい場合、<code>true</code>
	 */
	public static boolean validateTimeStamp(String timeStamp, long diffTime) {
		return validateTimeStamp(timeStamp, DATE_FORMAT_DEFAULT, diffTime);
	}

	/**
	 * タイムスタンプが正しいかチェックします。
	 * 
	 * @param timeStamp
	 *            タイムスタンプ
	 * @param format
	 *            タイムスタンプのフォーマット
	 * @param diffTime
	 *            タイムスタンプ誤差
	 * @return タイムスタンプが正しい場合、<code>true</code>
	 */
	public static boolean validateTimeStamp(String timeStamp, String format, long diffTime) {

		Date timeStampDate = null;
		SimpleDateFormat f = new SimpleDateFormat(format);
		try {
			timeStampDate = f.parse(timeStamp);

		} catch (ParseException e) {
			StringBuffer buf = new StringBuffer();
			buf.append("タイムスタンプのフォーマットが不正です。");
			buf.append("timeStamp=").append(timeStamp);
			buf.append(",format=").append(format);

			throw new RuntimeException(buf.toString(), e);
		}

		// タイムスタンプ誤差チェック
		Date nowDate = new Date();
		long time = nowDate.getTime() - timeStampDate.getTime();
		if (time <= diffTime && time >= -diffTime) {
			return true;
		}

		return false;
	}

	/**
	 * SHA-1メッセージダイジェスト値を計算する。
	 * 
	 * @param keyTemp
	 *            暗号文
	 * @return SHA-1メッセージダイジェスト値
	 */
	private static byte[] makeSHA1Digest(String keyTemp) {

		byte[] hash = null;

		try {
			// SHA-1メッセージダイジェスト値を計算する
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			sha.update(keyTemp.getBytes("UTF-8"));
			hash = sha.digest();

		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		return hash;
	}

	/**
	 * 指定されたバイト列を、BASE64形式でエンコードし、その文字列を返す。
	 * 
	 * @param raw
	 *            エンコード対象のバイト列
	 * @return BASE64形式の文字列
	 */
	private static String base64Encode(byte[] raw) {
		StringBuffer encoded = new StringBuffer();

		for (int i = 0; i < raw.length; i += 3) {
			encoded.append(base64EncodeBlock(raw, i));
		}

		return encoded.toString();
	}

	/**
	 * 指定されたバイト列を、BASE64形式でエンコードし、その文字列を返す。ブロックごとに行う。
	 * 
	 * @param raw
	 *            エンコード対象のバイト列
	 * @param offset
	 *            オフセット
	 * @return BASE64 形式の文字列
	 */
	private static char[] base64EncodeBlock(byte[] raw, int offset) {
		int block = 0;
		int slack = raw.length - offset - 1;
		int end = (slack >= 2) ? 2 : slack;

		for (int i = 0; i <= end; i++) {
			byte b = raw[offset + i];
			int neuter = (b < 0) ? b + 256 : b;
			block += neuter << (8 * (2 - i));
		}

		char[] base64 = new char[4];
		for (int i = 0; i < 4; i++) {
			int sixBit = (block >>> (6 * (3 - i))) & 0x3f;
			base64[i] = base64EncodeGetChar(sixBit);
		}

		if (slack < 1) {
			base64[2] = '=';
		}
		if (slack < 2) {
			base64[3] = '=';
		}

		return base64;
	}

	/**
	 * 指定された6ビットを、BASE64形式でエンコードし、その文字を返す。
	 * 
	 * @param sixBit
	 *            エンコード対象の6ビット
	 * @return BASE64 形式の文字
	 */
	private static char base64EncodeGetChar(int sixBit) {
		if (sixBit >= 0 && sixBit <= 25) {
			return (char) ('A' + sixBit);
		}
		if (sixBit >= 26 && sixBit <= 51) {
			return (char) ('a' + (sixBit - 26));
		}
		if (sixBit >= 52 && sixBit <= 61) {
			return (char) ('0' + (sixBit - 52));
		}
		if (sixBit == 62) {
			return '+';
		}
		if (sixBit == 63) {
			return '/';
		}

		return '?';
	}

	public static class SsoUserForm {
		@NotBlank
		private String userId;
		@NotBlank
		private String linkId;
		@NotBlank
		private String kaishaCd;
		@NotBlank
		private String ts;
		@NotBlank
		private String signature;
		private String userLang;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getLinkId() {
			return linkId;
		}

		public void setLinkId(String linkId) {
			this.linkId = linkId;
		}

		public String getKaishaCd() {
			return kaishaCd;
		}

		public void setKaishaCd(String kaishaCd) {
			this.kaishaCd = kaishaCd;
		}

		public String getTs() {
			return ts;
		}

		public void setTs(String ts) {
			this.ts = ts;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public String getUserLang() {
			return userLang;
		}

		public void setUserLang(String userLang) {
			this.userLang = userLang;
		}

	}

}
