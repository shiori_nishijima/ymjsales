package jp.co.yrc.mv.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

/**
 * Springへアクセスした際の認証エラーをハンドリングするクラス。
 * 
 * セッション新規作成時、セッションあり未認証時、セッション無効時のすべての未認証状態で呼ばれる。
 * 
 * AJAXのアクセスで認証エラーとなる場合はタイムアウト後のアクセスであると判断できるため
 * 本クラスで{@link HttpServletResponse#SC_UNAUTHORIZED}を返却。
 * 
 * 通常のアクセスでの認証エラーは本クラスでは処理せず、親クラスの処理を呼び出す。
 * 結果、セッション無効時はinvalid-session-urlが呼ばれる。
 */
public class AjaxAuthenticationProcessingEntryPoint extends LoginUrlAuthenticationEntryPoint {

	public AjaxAuthenticationProcessingEntryPoint(String loginUrl) {
		super(loginUrl);
	}
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		String requestType = request.getHeader("x-requested-with");
		if (requestType != null && requestType.equals("XMLHttpRequest")) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		super.commence(request, response, authException);
	}
}
