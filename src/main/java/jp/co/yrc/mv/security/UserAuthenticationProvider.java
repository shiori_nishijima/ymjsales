package jp.co.yrc.mv.security;

import jp.co.yrc.mv.common.MD5Util;
import jp.co.yrc.mv.dto.UserInfoDto;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * ユーザー認証をする。 DBからユーザーを取得してパスワードの整合性を確認
 */
@Component
public class UserAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	private static final Logger logger = Logger.getLogger(UserAuthenticationProvider.class);

	@Autowired
	UserDetailsService mvUserDetailsService;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		String password = (String) authentication.getCredentials();

		String hashPassword = MD5Util.getMD5HexString(password);
		if (logger.isDebugEnabled()) {
			logger.debug(String.format("id[%s] : pass[%s] : hash[%s]", authentication.getPrincipal(), password,
					hashPassword));
		}
		if (!hashPassword.equals(((UserInfoDto) userDetails).getPassword())) {
			throw new BadCredentialsException("jp.co.yrc.mv.error.login");
		}
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		String password = (String) authentication.getCredentials();

		if (StringUtils.isBlank(username) && StringUtils.isBlank(password)) {
			throw new BothNotEnterException("jp.co.yrc.mv.error.login");
		} else if (StringUtils.isBlank(username)) {
			throw new UserNameNotEnterException("jp.co.yrc.mv.error.user");
		} else if (StringUtils.isBlank(password)) {
			throw new PasswordNotEnterException("jp.co.yrc.mv.error.password");
		}

		UserInfoDto result = (UserInfoDto) mvUserDetailsService.loadUserByUsername(username);
		if (result == null) {
			throw new UsernameNotFoundException("jp.co.yrc.mv.error.login");
		}
		return result;
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		// SSO用のTokenクラスを省くように処理を書き換え。
		return UsernamePasswordAuthenticationToken.class.equals(authentication);
	}
}
