package jp.co.yrc.mv.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * SSO認証処理情報を保持するトークン
 */
public class SsoUserAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1752501322007997223L;
	
	private final String linkId;
	private final String kaishaCd;
	private final String ts;
	private final String sg;
	private final String locale;
	
	public SsoUserAuthenticationToken(Object principal, Object credentials, String linkId, String kaishaCd, String ts, String sg, String locale) {
		super(principal, credentials);
		
		this.linkId = linkId;
		this.kaishaCd = kaishaCd;
		this.ts = ts;
		this.sg = sg;
		this.locale = locale;
	
	}

	public SsoUserAuthenticationToken(Object principal, Object credentials, String linkId, String kaishaCd, String ts, String sg, String locale, Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
		
		this.linkId = linkId;
		this.kaishaCd = kaishaCd;
		this.ts = ts;
		this.sg = sg;
		this.locale = locale;	
	}

	public String getLinkId() {
		return linkId;
	}

	public String getKaishaCd() {
		return kaishaCd;
	}

	public String getTs() {
		return ts;
	}

	public String getSg() {
		return sg;
	}

	public String getLocale() {
		return locale;
	}

}
