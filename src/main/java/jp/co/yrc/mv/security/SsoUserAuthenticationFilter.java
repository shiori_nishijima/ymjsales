package jp.co.yrc.mv.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * SSO認証情報を付加するフィルター
 */
public class SsoUserAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		String userId = obtainUserId(request);
		String password = obtainPassword(request);
		String linkId = obtainLinkId(request);
		String kaishaCd = obtainKaishaCd(request);
		String ts = obtainTs(request);
		String signature = obtainSignature(request);
		String locale = obtainLocale(request);

		SsoUserAuthenticationToken authRequest = new SsoUserAuthenticationToken(userId, password, linkId, kaishaCd, ts,
				signature, locale);
		setDetails(request, authRequest);

		return getAuthenticationManager().authenticate(authRequest);
	}

	@Override
	protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
		super.setDetails(request, authRequest);
	}

	protected String obtainUserId(HttpServletRequest request) {
		return request.getParameter("userId");
	}

	protected String obtainLinkId(HttpServletRequest request) {
		return request.getParameter("linkId");
	}

	protected String obtainKaishaCd(HttpServletRequest request) {
		return request.getParameter("kaishaCd");
	}

	protected String obtainTs(HttpServletRequest request) {
		return request.getParameter("ts");
	}

	protected String obtainSignature(HttpServletRequest request) {
		return request.getParameter("signature");
	}

	protected String obtainLocale(HttpServletRequest request) {
		return request.getParameter("locale");
	}
}
