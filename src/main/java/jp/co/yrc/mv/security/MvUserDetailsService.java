package jp.co.yrc.mv.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.yrc.mv.common.Constants.COMPANY;
import jp.co.yrc.mv.common.Constants.CorpCd;
import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.MstEigyoSosikiDao;
import jp.co.yrc.mv.dao.UsersDao;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.Users;

@Service
public class MvUserDetailsService implements UserDetailsService {

	@Autowired
	UsersDao usersDao;

	@Autowired
	MstEigyoSosikiDao mstEigyoSosikiDao;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Override
	@Transactional(readOnly = true)
	public UserInfoDto loadUserByUsername(String username) throws UsernameNotFoundException {
		// CorpCodesは利用しない
		Users user = usersDao.selectLoginUser(username);
		if (user == null) {
			return null;
		}

		// デフォルトを設定しておく
		List<String> corpCdList = Arrays.asList(CorpCd.convertCompanyToCodes(COMPANY.YMJ.getCompany()));
		UserInfoDto userInfo = dozerBeanMapper.map(user, UserInfoDto.class);
		userInfo.setLogin(true);
		userInfo.setEnabled(true);
		userInfo.setCorpCodes(corpCdList);
		userInfo.setCompany(COMPANY.YMJ.getCompany()); // 固定で設定
				
		// ユーザに紐づくdiv1を保持しておく
		Role role = Role.getByCode(userInfo.getAuthorization());
		if (role == Role.All || role == Role.TechService) {
			userInfo.setCompanies(mstEigyoSosikiDao.listDiv1());
		} else {
			userInfo.setCompanies(mstEigyoSosikiDao.listDiv1ByUserId(username));
		}
		List<GrantedAuthority> collection = new ArrayList<>();

		collection.add(new MvGrantedAuthority(role));
		// application-config.xmlに定義してある ログインする用の権限 ADMINとか有れば追加すること
		// ROLE_で始まる文字列でないとSpringに怒られる
		collection.add(new SimpleGrantedAuthority("ROLE_USER"));
		userInfo.setAuthorities(collection);

		userInfo.setLoginTime(DateUtils.getDBDate());

		return userInfo;
	}
}
