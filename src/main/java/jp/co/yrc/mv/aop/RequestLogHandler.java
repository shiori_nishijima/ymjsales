package jp.co.yrc.mv.aop;

import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.yrc.mv.common.Constants.COMPANY;
import jp.co.yrc.mv.common.LocaleContext;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.LogInfoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * リクエストのログを出力します。
 * 
 * ログ仕様はSFAに合わせている。 インターセプタ指定はapplication-config.xmlにあり。
 */
public class RequestLogHandler extends HandlerInterceptorAdapter {

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	LogInfoService logInfoService;

	public static final Logger logger = LoggerFactory.getLogger(RequestLogHandler.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		StringBuilder log = new StringBuilder();

		// ヘッダ情報の取得
		Map<String, String> headerMap = new LinkedHashMap<String, String>();

		Enumeration<String> headernames = request.getHeaderNames();
		while (headernames.hasMoreElements()) {
			String name = headernames.nextElement();
			Enumeration<String> headervals = request.getHeaders(name);

			while (headervals.hasMoreElements()) {
				String val = headervals.nextElement();
				headerMap.put(name, val);
			}
		}

		// ユーザ情報の取得
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String userId = null;
		Date loginDate = null;
		if (authentication != null) {
			Object obj = authentication.getPrincipal();
			if (obj instanceof UserInfoDto) {
				UserInfoDto userInfoDto = (UserInfoDto) obj;
				userId = userInfoDto.getId();
				loginDate = userInfoDto.getLoginTime();
				LocaleContext.instance().setCompany(COMPANY.YMJ.getCompany());
			}
		}

		String uri = request.getRequestURI();
		log.append("RequestURL [").append(uri);

		log.append("] UserId [").append(userId).append("]");

		if (logger.isDebugEnabled()) {
			log.append(" Content-Type [").append(headerMap.get("content-type")).append("]");
		}

		Map<String, String[]> parameterMap = request.getParameterMap();
		Map<String, String> requestParams;
		if (parameterMap != null && parameterMap.size() > 0) {
			requestParams = new LinkedHashMap<String, String>();
			for (String key : parameterMap.keySet()) {
				requestParams.put(key, Arrays.toString(parameterMap.get(key)));
			}
			log.append(" RequestParams [").append(requestParams).append("]");
		}

		if (request.getQueryString() != null) {
			log.append(" QueryParams [").append(request.getQueryString()).append("]");
		}

		logger.info(log.toString());
		logInfoService.access(uri, request.getContextPath(),  parameterMap, headerMap.get("user-agent"), userId, loginDate);
		return true;
	}
}
