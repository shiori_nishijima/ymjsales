package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCommentsReadUsersListener implements EntityListener<TechServiceCommentsReadUsers> {

    @Override
    public void preInsert(TechServiceCommentsReadUsers entity, PreInsertContext<TechServiceCommentsReadUsers> context) {
    }

    @Override
    public void preUpdate(TechServiceCommentsReadUsers entity, PreUpdateContext<TechServiceCommentsReadUsers> context) {
    }

    @Override
    public void preDelete(TechServiceCommentsReadUsers entity, PreDeleteContext<TechServiceCommentsReadUsers> context) {
    }

    @Override
    public void postInsert(TechServiceCommentsReadUsers entity, PostInsertContext<TechServiceCommentsReadUsers> context) {
    }

    @Override
    public void postUpdate(TechServiceCommentsReadUsers entity, PostUpdateContext<TechServiceCommentsReadUsers> context) {
    }

    @Override
    public void postDelete(TechServiceCommentsReadUsers entity, PostDeleteContext<TechServiceCommentsReadUsers> context) {
    }
}