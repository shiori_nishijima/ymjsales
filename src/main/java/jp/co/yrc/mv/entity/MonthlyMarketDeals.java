package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyMarketDealsListener.class)
@Table(name = "monthly_market_deals")
public class MonthlyMarketDeals {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** SS持軒数 */
    @Column(name = "ss_own_count")
    BigDecimal ssOwnCount;

    /** SS取引軒数 */
    @Column(name = "ss_deal_count")
    BigDecimal ssDealCount;

    /** SS取引得M数 */
    @Column(name = "ss_account_master_count")
    BigDecimal ssAccountMasterCount;

    /** RS持軒数 */
    @Column(name = "rs_own_count")
    BigDecimal rsOwnCount;

    /** RS取引軒数 */
    @Column(name = "rs_deal_count")
    BigDecimal rsDealCount;

    /** RS取引得M数 */
    @Column(name = "rs_account_master_count")
    BigDecimal rsAccountMasterCount;

    /** CD持軒数 */
    @Column(name = "cd_own_count")
    BigDecimal cdOwnCount;

    /** CD取引軒数 */
    @Column(name = "cd_deal_count")
    BigDecimal cdDealCount;

    /** CD取引得M数 */
    @Column(name = "cd_account_master_count")
    BigDecimal cdAccountMasterCount;

    /** PS持軒数 */
    @Column(name = "ps_own_count")
    BigDecimal psOwnCount;

    /** PS取引軒数 */
    @Column(name = "ps_deal_count")
    BigDecimal psDealCount;

    /** PS取引得M数 */
    @Column(name = "ps_account_master_count")
    BigDecimal psAccountMasterCount;

    /** CS持軒数 */
    @Column(name = "cs_own_count")
    BigDecimal csOwnCount;

    /** CS取引軒数 */
    @Column(name = "cs_deal_count")
    BigDecimal csDealCount;

    /** CS取引得M数 */
    @Column(name = "cs_account_master_count")
    BigDecimal csAccountMasterCount;

    /** HC持軒数 */
    @Column(name = "hc_own_count")
    BigDecimal hcOwnCount;

    /** HC取引軒数 */
    @Column(name = "hc_deal_count")
    BigDecimal hcDealCount;

    /** HC取引得M数 */
    @Column(name = "hc_account_master_count")
    BigDecimal hcAccountMasterCount;

    /** リース持軒数 */
    @Column(name = "lease_own_count")
    BigDecimal leaseOwnCount;

    /** リース取引軒数 */
    @Column(name = "lease_deal_count")
    BigDecimal leaseDealCount;

    /** リース取引得M数 */
    @Column(name = "lease_account_master_count")
    BigDecimal leaseAccountMasterCount;

    /** 間他持軒数 */
    @Column(name = "indirect_other_own_count")
    BigDecimal indirectOtherOwnCount;

    /** 間他取引軒数 */
    @Column(name = "indirect_other_deal_count")
    BigDecimal indirectOtherDealCount;

    /** 間他取引得M数 */
    @Column(name = "indirect_other_account_master_count")
    BigDecimal indirectOtherAccountMasterCount;

    /** トラック持軒数 */
    @Column(name = "truck_own_count")
    BigDecimal truckOwnCount;

    /** トラック取引軒数 */
    @Column(name = "truck_deal_count")
    BigDecimal truckDealCount;

    /** トラック取引得M数 */
    @Column(name = "truck_account_master_count")
    BigDecimal truckAccountMasterCount;

    /** バス持軒数 */
    @Column(name = "bus_own_count")
    BigDecimal busOwnCount;

    /** バス取引軒数 */
    @Column(name = "bus_deal_count")
    BigDecimal busDealCount;

    /** バス取引得M数 */
    @Column(name = "bus_account_master_count")
    BigDecimal busAccountMasterCount;

    /** ハイタク持軒数 */
    @Column(name = "hire_taxi_own_count")
    BigDecimal hireTaxiOwnCount;

    /** ハイタク取引軒数 */
    @Column(name = "hire_taxi_deal_count")
    BigDecimal hireTaxiDealCount;

    /** ハイタク取引得M数 */
    @Column(name = "hire_taxi_account_master_count")
    BigDecimal hireTaxiAccountMasterCount;

    /** 直他持軒数 */
    @Column(name = "direct_other_own_count")
    BigDecimal directOtherOwnCount;

    /** 直他取引軒数 */
    @Column(name = "direct_other_deal_count")
    BigDecimal directOtherDealCount;

    /** 直他取引得M数 */
    @Column(name = "direct_other_account_master_count")
    BigDecimal directOtherAccountMasterCount;

    /** 小売持軒数 */
    @Column(name = "retail_own_count")
    BigDecimal retailOwnCount;

    /** 小売取引軒数 */
    @Column(name = "retail_deal_count")
    BigDecimal retailDealCount;

    /** 小売取引得M数 */
    @Column(name = "retail_account_master_count")
    BigDecimal retailAccountMasterCount;

    /** 取引計画数 */
    @Column(name = "plan_count")
    BigDecimal planCount;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the ssOwnCount.
     * 
     * @return the ssOwnCount
     */
    public BigDecimal getSsOwnCount() {
        return ssOwnCount;
    }

    /** 
     * Sets the ssOwnCount.
     * 
     * @param ssOwnCount the ssOwnCount
     */
    public void setSsOwnCount(BigDecimal ssOwnCount) {
        this.ssOwnCount = ssOwnCount;
    }

    /** 
     * Returns the ssDealCount.
     * 
     * @return the ssDealCount
     */
    public BigDecimal getSsDealCount() {
        return ssDealCount;
    }

    /** 
     * Sets the ssDealCount.
     * 
     * @param ssDealCount the ssDealCount
     */
    public void setSsDealCount(BigDecimal ssDealCount) {
        this.ssDealCount = ssDealCount;
    }

    /** 
     * Returns the ssAccountMasterCount.
     * 
     * @return the ssAccountMasterCount
     */
    public BigDecimal getSsAccountMasterCount() {
        return ssAccountMasterCount;
    }

    /** 
     * Sets the ssAccountMasterCount.
     * 
     * @param ssAccountMasterCount the ssAccountMasterCount
     */
    public void setSsAccountMasterCount(BigDecimal ssAccountMasterCount) {
        this.ssAccountMasterCount = ssAccountMasterCount;
    }

    /** 
     * Returns the rsOwnCount.
     * 
     * @return the rsOwnCount
     */
    public BigDecimal getRsOwnCount() {
        return rsOwnCount;
    }

    /** 
     * Sets the rsOwnCount.
     * 
     * @param rsOwnCount the rsOwnCount
     */
    public void setRsOwnCount(BigDecimal rsOwnCount) {
        this.rsOwnCount = rsOwnCount;
    }

    /** 
     * Returns the rsDealCount.
     * 
     * @return the rsDealCount
     */
    public BigDecimal getRsDealCount() {
        return rsDealCount;
    }

    /** 
     * Sets the rsDealCount.
     * 
     * @param rsDealCount the rsDealCount
     */
    public void setRsDealCount(BigDecimal rsDealCount) {
        this.rsDealCount = rsDealCount;
    }

    /** 
     * Returns the rsAccountMasterCount.
     * 
     * @return the rsAccountMasterCount
     */
    public BigDecimal getRsAccountMasterCount() {
        return rsAccountMasterCount;
    }

    /** 
     * Sets the rsAccountMasterCount.
     * 
     * @param rsAccountMasterCount the rsAccountMasterCount
     */
    public void setRsAccountMasterCount(BigDecimal rsAccountMasterCount) {
        this.rsAccountMasterCount = rsAccountMasterCount;
    }

    /** 
     * Returns the cdOwnCount.
     * 
     * @return the cdOwnCount
     */
    public BigDecimal getCdOwnCount() {
        return cdOwnCount;
    }

    /** 
     * Sets the cdOwnCount.
     * 
     * @param cdOwnCount the cdOwnCount
     */
    public void setCdOwnCount(BigDecimal cdOwnCount) {
        this.cdOwnCount = cdOwnCount;
    }

    /** 
     * Returns the cdDealCount.
     * 
     * @return the cdDealCount
     */
    public BigDecimal getCdDealCount() {
        return cdDealCount;
    }

    /** 
     * Sets the cdDealCount.
     * 
     * @param cdDealCount the cdDealCount
     */
    public void setCdDealCount(BigDecimal cdDealCount) {
        this.cdDealCount = cdDealCount;
    }

    /** 
     * Returns the cdAccountMasterCount.
     * 
     * @return the cdAccountMasterCount
     */
    public BigDecimal getCdAccountMasterCount() {
        return cdAccountMasterCount;
    }

    /** 
     * Sets the cdAccountMasterCount.
     * 
     * @param cdAccountMasterCount the cdAccountMasterCount
     */
    public void setCdAccountMasterCount(BigDecimal cdAccountMasterCount) {
        this.cdAccountMasterCount = cdAccountMasterCount;
    }

    /** 
     * Returns the psOwnCount.
     * 
     * @return the psOwnCount
     */
    public BigDecimal getPsOwnCount() {
        return psOwnCount;
    }

    /** 
     * Sets the psOwnCount.
     * 
     * @param psOwnCount the psOwnCount
     */
    public void setPsOwnCount(BigDecimal psOwnCount) {
        this.psOwnCount = psOwnCount;
    }

    /** 
     * Returns the psDealCount.
     * 
     * @return the psDealCount
     */
    public BigDecimal getPsDealCount() {
        return psDealCount;
    }

    /** 
     * Sets the psDealCount.
     * 
     * @param psDealCount the psDealCount
     */
    public void setPsDealCount(BigDecimal psDealCount) {
        this.psDealCount = psDealCount;
    }

    /** 
     * Returns the psAccountMasterCount.
     * 
     * @return the psAccountMasterCount
     */
    public BigDecimal getPsAccountMasterCount() {
        return psAccountMasterCount;
    }

    /** 
     * Sets the psAccountMasterCount.
     * 
     * @param psAccountMasterCount the psAccountMasterCount
     */
    public void setPsAccountMasterCount(BigDecimal psAccountMasterCount) {
        this.psAccountMasterCount = psAccountMasterCount;
    }

    /** 
     * Returns the csOwnCount.
     * 
     * @return the csOwnCount
     */
    public BigDecimal getCsOwnCount() {
        return csOwnCount;
    }

    /** 
     * Sets the csOwnCount.
     * 
     * @param csOwnCount the csOwnCount
     */
    public void setCsOwnCount(BigDecimal csOwnCount) {
        this.csOwnCount = csOwnCount;
    }

    /** 
     * Returns the csDealCount.
     * 
     * @return the csDealCount
     */
    public BigDecimal getCsDealCount() {
        return csDealCount;
    }

    /** 
     * Sets the csDealCount.
     * 
     * @param csDealCount the csDealCount
     */
    public void setCsDealCount(BigDecimal csDealCount) {
        this.csDealCount = csDealCount;
    }

    /** 
     * Returns the csAccountMasterCount.
     * 
     * @return the csAccountMasterCount
     */
    public BigDecimal getCsAccountMasterCount() {
        return csAccountMasterCount;
    }

    /** 
     * Sets the csAccountMasterCount.
     * 
     * @param csAccountMasterCount the csAccountMasterCount
     */
    public void setCsAccountMasterCount(BigDecimal csAccountMasterCount) {
        this.csAccountMasterCount = csAccountMasterCount;
    }

    /** 
     * Returns the hcOwnCount.
     * 
     * @return the hcOwnCount
     */
    public BigDecimal getHcOwnCount() {
        return hcOwnCount;
    }

    /** 
     * Sets the hcOwnCount.
     * 
     * @param hcOwnCount the hcOwnCount
     */
    public void setHcOwnCount(BigDecimal hcOwnCount) {
        this.hcOwnCount = hcOwnCount;
    }

    /** 
     * Returns the hcDealCount.
     * 
     * @return the hcDealCount
     */
    public BigDecimal getHcDealCount() {
        return hcDealCount;
    }

    /** 
     * Sets the hcDealCount.
     * 
     * @param hcDealCount the hcDealCount
     */
    public void setHcDealCount(BigDecimal hcDealCount) {
        this.hcDealCount = hcDealCount;
    }

    /** 
     * Returns the hcAccountMasterCount.
     * 
     * @return the hcAccountMasterCount
     */
    public BigDecimal getHcAccountMasterCount() {
        return hcAccountMasterCount;
    }

    /** 
     * Sets the hcAccountMasterCount.
     * 
     * @param hcAccountMasterCount the hcAccountMasterCount
     */
    public void setHcAccountMasterCount(BigDecimal hcAccountMasterCount) {
        this.hcAccountMasterCount = hcAccountMasterCount;
    }

    /** 
     * Returns the leaseOwnCount.
     * 
     * @return the leaseOwnCount
     */
    public BigDecimal getLeaseOwnCount() {
        return leaseOwnCount;
    }

    /** 
     * Sets the leaseOwnCount.
     * 
     * @param leaseOwnCount the leaseOwnCount
     */
    public void setLeaseOwnCount(BigDecimal leaseOwnCount) {
        this.leaseOwnCount = leaseOwnCount;
    }

    /** 
     * Returns the leaseDealCount.
     * 
     * @return the leaseDealCount
     */
    public BigDecimal getLeaseDealCount() {
        return leaseDealCount;
    }

    /** 
     * Sets the leaseDealCount.
     * 
     * @param leaseDealCount the leaseDealCount
     */
    public void setLeaseDealCount(BigDecimal leaseDealCount) {
        this.leaseDealCount = leaseDealCount;
    }

    /** 
     * Returns the leaseAccountMasterCount.
     * 
     * @return the leaseAccountMasterCount
     */
    public BigDecimal getLeaseAccountMasterCount() {
        return leaseAccountMasterCount;
    }

    /** 
     * Sets the leaseAccountMasterCount.
     * 
     * @param leaseAccountMasterCount the leaseAccountMasterCount
     */
    public void setLeaseAccountMasterCount(BigDecimal leaseAccountMasterCount) {
        this.leaseAccountMasterCount = leaseAccountMasterCount;
    }

    /** 
     * Returns the indirectOtherOwnCount.
     * 
     * @return the indirectOtherOwnCount
     */
    public BigDecimal getIndirectOtherOwnCount() {
        return indirectOtherOwnCount;
    }

    /** 
     * Sets the indirectOtherOwnCount.
     * 
     * @param indirectOtherOwnCount the indirectOtherOwnCount
     */
    public void setIndirectOtherOwnCount(BigDecimal indirectOtherOwnCount) {
        this.indirectOtherOwnCount = indirectOtherOwnCount;
    }

    /** 
     * Returns the indirectOtherDealCount.
     * 
     * @return the indirectOtherDealCount
     */
    public BigDecimal getIndirectOtherDealCount() {
        return indirectOtherDealCount;
    }

    /** 
     * Sets the indirectOtherDealCount.
     * 
     * @param indirectOtherDealCount the indirectOtherDealCount
     */
    public void setIndirectOtherDealCount(BigDecimal indirectOtherDealCount) {
        this.indirectOtherDealCount = indirectOtherDealCount;
    }

    /** 
     * Returns the indirectOtherAccountMasterCount.
     * 
     * @return the indirectOtherAccountMasterCount
     */
    public BigDecimal getIndirectOtherAccountMasterCount() {
        return indirectOtherAccountMasterCount;
    }

    /** 
     * Sets the indirectOtherAccountMasterCount.
     * 
     * @param indirectOtherAccountMasterCount the indirectOtherAccountMasterCount
     */
    public void setIndirectOtherAccountMasterCount(BigDecimal indirectOtherAccountMasterCount) {
        this.indirectOtherAccountMasterCount = indirectOtherAccountMasterCount;
    }

    /** 
     * Returns the truckOwnCount.
     * 
     * @return the truckOwnCount
     */
    public BigDecimal getTruckOwnCount() {
        return truckOwnCount;
    }

    /** 
     * Sets the truckOwnCount.
     * 
     * @param truckOwnCount the truckOwnCount
     */
    public void setTruckOwnCount(BigDecimal truckOwnCount) {
        this.truckOwnCount = truckOwnCount;
    }

    /** 
     * Returns the truckDealCount.
     * 
     * @return the truckDealCount
     */
    public BigDecimal getTruckDealCount() {
        return truckDealCount;
    }

    /** 
     * Sets the truckDealCount.
     * 
     * @param truckDealCount the truckDealCount
     */
    public void setTruckDealCount(BigDecimal truckDealCount) {
        this.truckDealCount = truckDealCount;
    }

    /** 
     * Returns the truckAccountMasterCount.
     * 
     * @return the truckAccountMasterCount
     */
    public BigDecimal getTruckAccountMasterCount() {
        return truckAccountMasterCount;
    }

    /** 
     * Sets the truckAccountMasterCount.
     * 
     * @param truckAccountMasterCount the truckAccountMasterCount
     */
    public void setTruckAccountMasterCount(BigDecimal truckAccountMasterCount) {
        this.truckAccountMasterCount = truckAccountMasterCount;
    }

    /** 
     * Returns the busOwnCount.
     * 
     * @return the busOwnCount
     */
    public BigDecimal getBusOwnCount() {
        return busOwnCount;
    }

    /** 
     * Sets the busOwnCount.
     * 
     * @param busOwnCount the busOwnCount
     */
    public void setBusOwnCount(BigDecimal busOwnCount) {
        this.busOwnCount = busOwnCount;
    }

    /** 
     * Returns the busDealCount.
     * 
     * @return the busDealCount
     */
    public BigDecimal getBusDealCount() {
        return busDealCount;
    }

    /** 
     * Sets the busDealCount.
     * 
     * @param busDealCount the busDealCount
     */
    public void setBusDealCount(BigDecimal busDealCount) {
        this.busDealCount = busDealCount;
    }

    /** 
     * Returns the busAccountMasterCount.
     * 
     * @return the busAccountMasterCount
     */
    public BigDecimal getBusAccountMasterCount() {
        return busAccountMasterCount;
    }

    /** 
     * Sets the busAccountMasterCount.
     * 
     * @param busAccountMasterCount the busAccountMasterCount
     */
    public void setBusAccountMasterCount(BigDecimal busAccountMasterCount) {
        this.busAccountMasterCount = busAccountMasterCount;
    }

    /** 
     * Returns the hireTaxiOwnCount.
     * 
     * @return the hireTaxiOwnCount
     */
    public BigDecimal getHireTaxiOwnCount() {
        return hireTaxiOwnCount;
    }

    /** 
     * Sets the hireTaxiOwnCount.
     * 
     * @param hireTaxiOwnCount the hireTaxiOwnCount
     */
    public void setHireTaxiOwnCount(BigDecimal hireTaxiOwnCount) {
        this.hireTaxiOwnCount = hireTaxiOwnCount;
    }

    /** 
     * Returns the hireTaxiDealCount.
     * 
     * @return the hireTaxiDealCount
     */
    public BigDecimal getHireTaxiDealCount() {
        return hireTaxiDealCount;
    }

    /** 
     * Sets the hireTaxiDealCount.
     * 
     * @param hireTaxiDealCount the hireTaxiDealCount
     */
    public void setHireTaxiDealCount(BigDecimal hireTaxiDealCount) {
        this.hireTaxiDealCount = hireTaxiDealCount;
    }

    /** 
     * Returns the hireTaxiAccountMasterCount.
     * 
     * @return the hireTaxiAccountMasterCount
     */
    public BigDecimal getHireTaxiAccountMasterCount() {
        return hireTaxiAccountMasterCount;
    }

    /** 
     * Sets the hireTaxiAccountMasterCount.
     * 
     * @param hireTaxiAccountMasterCount the hireTaxiAccountMasterCount
     */
    public void setHireTaxiAccountMasterCount(BigDecimal hireTaxiAccountMasterCount) {
        this.hireTaxiAccountMasterCount = hireTaxiAccountMasterCount;
    }

    /** 
     * Returns the directOtherOwnCount.
     * 
     * @return the directOtherOwnCount
     */
    public BigDecimal getDirectOtherOwnCount() {
        return directOtherOwnCount;
    }

    /** 
     * Sets the directOtherOwnCount.
     * 
     * @param directOtherOwnCount the directOtherOwnCount
     */
    public void setDirectOtherOwnCount(BigDecimal directOtherOwnCount) {
        this.directOtherOwnCount = directOtherOwnCount;
    }

    /** 
     * Returns the directOtherDealCount.
     * 
     * @return the directOtherDealCount
     */
    public BigDecimal getDirectOtherDealCount() {
        return directOtherDealCount;
    }

    /** 
     * Sets the directOtherDealCount.
     * 
     * @param directOtherDealCount the directOtherDealCount
     */
    public void setDirectOtherDealCount(BigDecimal directOtherDealCount) {
        this.directOtherDealCount = directOtherDealCount;
    }

    /** 
     * Returns the directOtherAccountMasterCount.
     * 
     * @return the directOtherAccountMasterCount
     */
    public BigDecimal getDirectOtherAccountMasterCount() {
        return directOtherAccountMasterCount;
    }

    /** 
     * Sets the directOtherAccountMasterCount.
     * 
     * @param directOtherAccountMasterCount the directOtherAccountMasterCount
     */
    public void setDirectOtherAccountMasterCount(BigDecimal directOtherAccountMasterCount) {
        this.directOtherAccountMasterCount = directOtherAccountMasterCount;
    }

    /** 
     * Returns the retailOwnCount.
     * 
     * @return the retailOwnCount
     */
    public BigDecimal getRetailOwnCount() {
        return retailOwnCount;
    }

    /** 
     * Sets the retailOwnCount.
     * 
     * @param retailOwnCount the retailOwnCount
     */
    public void setRetailOwnCount(BigDecimal retailOwnCount) {
        this.retailOwnCount = retailOwnCount;
    }

    /** 
     * Returns the retailDealCount.
     * 
     * @return the retailDealCount
     */
    public BigDecimal getRetailDealCount() {
        return retailDealCount;
    }

    /** 
     * Sets the retailDealCount.
     * 
     * @param retailDealCount the retailDealCount
     */
    public void setRetailDealCount(BigDecimal retailDealCount) {
        this.retailDealCount = retailDealCount;
    }

    /** 
     * Returns the retailAccountMasterCount.
     * 
     * @return the retailAccountMasterCount
     */
    public BigDecimal getRetailAccountMasterCount() {
        return retailAccountMasterCount;
    }

    /** 
     * Sets the retailAccountMasterCount.
     * 
     * @param retailAccountMasterCount the retailAccountMasterCount
     */
    public void setRetailAccountMasterCount(BigDecimal retailAccountMasterCount) {
        this.retailAccountMasterCount = retailAccountMasterCount;
    }

    /** 
     * Returns the planCount.
     * 
     * @return the planCount
     */
    public BigDecimal getPlanCount() {
        return planCount;
    }

    /** 
     * Sets the planCount.
     * 
     * @param planCount the planCount
     */
    public void setPlanCount(BigDecimal planCount) {
        this.planCount = planCount;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}