package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MstEigyoSosikiHistoryListener implements EntityListener<MstEigyoSosikiHistory> {

    @Override
    public void preInsert(MstEigyoSosikiHistory entity, PreInsertContext<MstEigyoSosikiHistory> context) {
    }

    @Override
    public void preUpdate(MstEigyoSosikiHistory entity, PreUpdateContext<MstEigyoSosikiHistory> context) {
    }

    @Override
    public void preDelete(MstEigyoSosikiHistory entity, PreDeleteContext<MstEigyoSosikiHistory> context) {
    }

    @Override
    public void postInsert(MstEigyoSosikiHistory entity, PostInsertContext<MstEigyoSosikiHistory> context) {
    }

    @Override
    public void postUpdate(MstEigyoSosikiHistory entity, PostUpdateContext<MstEigyoSosikiHistory> context) {
    }

    @Override
    public void postDelete(MstEigyoSosikiHistory entity, PostDeleteContext<MstEigyoSosikiHistory> context) {
    }
}