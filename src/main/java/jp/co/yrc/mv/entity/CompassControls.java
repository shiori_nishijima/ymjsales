package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = CompassControlsListener.class)
@Table(name = "compass_controls")
public class CompassControls {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** 帳票表示順 */
    @Id
    @Column(name = "report_display_seq")
    Integer reportDisplaySeq;

    /** 帳票表示品種名称) */
    @Column(name = "report_display_name")
    String reportDisplayName;

    /** 販管計画品種コード */
    @Column(name = "plan_product_kind")
    String planProductKind;

    /** 販管計画品種名称 */
    @Column(name = "plan_product_kind_name")
    String planProductKindName;

    /** 共通品種コード(Compass品種コード) */
    @Id
    @Column(name = "compass_kind")
    String compassKind;

    /** 共通品種名称(Compass品種名) */
    @Column(name = "compass_kind_name")
    String compassKindName;

    /** 削除済み */
    @Column(name = "deleted")
    Boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the reportDisplaySeq.
     * 
     * @return the reportDisplaySeq
     */
    public Integer getReportDisplaySeq() {
        return reportDisplaySeq;
    }

    /** 
     * Sets the reportDisplaySeq.
     * 
     * @param reportDisplaySeq the reportDisplaySeq
     */
    public void setReportDisplaySeq(Integer reportDisplaySeq) {
        this.reportDisplaySeq = reportDisplaySeq;
    }

    /** 
     * Returns the reportDisplayName.
     * 
     * @return the reportDisplayName
     */
    public String getReportDisplayName() {
        return reportDisplayName;
    }

    /** 
     * Sets the reportDisplayName.
     * 
     * @param reportDisplayName the reportDisplayName
     */
    public void setReportDisplayName(String reportDisplayName) {
        this.reportDisplayName = reportDisplayName;
    }

    /** 
     * Returns the planProductKind.
     * 
     * @return the planProductKind
     */
    public String getPlanProductKind() {
        return planProductKind;
    }

    /** 
     * Sets the planProductKind.
     * 
     * @param planProductKind the planProductKind
     */
    public void setPlanProductKind(String planProductKind) {
        this.planProductKind = planProductKind;
    }

    /** 
     * Returns the planProductKindName.
     * 
     * @return the planProductKindName
     */
    public String getPlanProductKindName() {
        return planProductKindName;
    }

    /** 
     * Sets the planProductKindName.
     * 
     * @param planProductKindName the planProductKindName
     */
    public void setPlanProductKindName(String planProductKindName) {
        this.planProductKindName = planProductKindName;
    }

    /** 
     * Returns the compassKind.
     * 
     * @return the compassKind
     */
    public String getCompassKind() {
        return compassKind;
    }

    /** 
     * Sets the compassKind.
     * 
     * @param compassKind the compassKind
     */
    public void setCompassKind(String compassKind) {
        this.compassKind = compassKind;
    }

    /** 
     * Returns the compassKindName.
     * 
     * @return the compassKindName
     */
    public String getCompassKindName() {
        return compassKindName;
    }

    /** 
     * Sets the compassKindName.
     * 
     * @param compassKindName the compassKindName
     */
    public void setCompassKindName(String compassKindName) {
        this.compassKindName = compassKindName;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}