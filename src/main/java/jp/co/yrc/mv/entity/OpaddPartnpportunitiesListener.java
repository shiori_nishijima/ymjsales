package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OpaddPartnpportunitiesListener implements EntityListener<OpaddPartnpportunities> {

    @Override
    public void preInsert(OpaddPartnpportunities entity, PreInsertContext<OpaddPartnpportunities> context) {
    }

    @Override
    public void preUpdate(OpaddPartnpportunities entity, PreUpdateContext<OpaddPartnpportunities> context) {
    }

    @Override
    public void preDelete(OpaddPartnpportunities entity, PreDeleteContext<OpaddPartnpportunities> context) {
    }

    @Override
    public void postInsert(OpaddPartnpportunities entity, PostInsertContext<OpaddPartnpportunities> context) {
    }

    @Override
    public void postUpdate(OpaddPartnpportunities entity, PostUpdateContext<OpaddPartnpportunities> context) {
    }

    @Override
    public void postDelete(OpaddPartnpportunities entity, PostDeleteContext<OpaddPartnpportunities> context) {
    }
}