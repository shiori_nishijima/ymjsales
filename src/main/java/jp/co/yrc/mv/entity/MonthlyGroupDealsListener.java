package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyGroupDealsListener implements EntityListener<MonthlyGroupDeals> {

    @Override
    public void preInsert(MonthlyGroupDeals entity, PreInsertContext<MonthlyGroupDeals> context) {
    }

    @Override
    public void preUpdate(MonthlyGroupDeals entity, PreUpdateContext<MonthlyGroupDeals> context) {
    }

    @Override
    public void preDelete(MonthlyGroupDeals entity, PreDeleteContext<MonthlyGroupDeals> context) {
    }

    @Override
    public void postInsert(MonthlyGroupDeals entity, PostInsertContext<MonthlyGroupDeals> context) {
    }

    @Override
    public void postUpdate(MonthlyGroupDeals entity, PostUpdateContext<MonthlyGroupDeals> context) {
    }

    @Override
    public void postDelete(MonthlyGroupDeals entity, PostDeleteContext<MonthlyGroupDeals> context) {
    }
}