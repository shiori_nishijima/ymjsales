package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpTempForwardingStocksListener implements EntityListener<TmpTempForwardingStocks> {

    @Override
    public void preInsert(TmpTempForwardingStocks entity, PreInsertContext<TmpTempForwardingStocks> context) {
    }

    @Override
    public void preUpdate(TmpTempForwardingStocks entity, PreUpdateContext<TmpTempForwardingStocks> context) {
    }

    @Override
    public void preDelete(TmpTempForwardingStocks entity, PreDeleteContext<TmpTempForwardingStocks> context) {
    }

    @Override
    public void postInsert(TmpTempForwardingStocks entity, PostInsertContext<TmpTempForwardingStocks> context) {
    }

    @Override
    public void postUpdate(TmpTempForwardingStocks entity, PostUpdateContext<TmpTempForwardingStocks> context) {
    }

    @Override
    public void postDelete(TmpTempForwardingStocks entity, PostDeleteContext<TmpTempForwardingStocks> context) {
    }
}