package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyMarketDemandEstimationsListener.class)
@Table(name = "monthly_market_demand_estimations")
public class MonthlyMarketDemandEstimations {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** SS年間推定需要 */
    @Column(name = "ss_year_demand_estimation")
    BigDecimal ssYearDemandEstimation;

    /** RS年間推定需要 */
    @Column(name = "rs_year_demand_estimation")
    BigDecimal rsYearDemandEstimation;

    /** CD年間推定需要 */
    @Column(name = "cd_year_demand_estimation")
    BigDecimal cdYearDemandEstimation;

    /** PS年間推定需要 */
    @Column(name = "ps_year_demand_estimation")
    BigDecimal psYearDemandEstimation;

    /** CS年間推定需要 */
    @Column(name = "cs_year_demand_estimation")
    BigDecimal csYearDemandEstimation;

    /** HC年間推定需要 */
    @Column(name = "hc_year_demand_estimation")
    BigDecimal hcYearDemandEstimation;

    /** リース年間推定需要 */
    @Column(name = "lease_year_demand_estimation")
    BigDecimal leaseYearDemandEstimation;

    /** 間他年間推定需要 */
    @Column(name = "indirect_other_year_demand_estimation")
    BigDecimal indirectOtherYearDemandEstimation;

    /** トラック年間推定需要 */
    @Column(name = "truck_year_demand_estimation")
    BigDecimal truckYearDemandEstimation;

    /** バス年間推定需要 */
    @Column(name = "bus_year_demand_estimation")
    BigDecimal busYearDemandEstimation;

    /** ハイタク年間推定需要 */
    @Column(name = "hire_taxi_year_demand_estimation")
    BigDecimal hireTaxiYearDemandEstimation;

    /** 直他年間推定需要 */
    @Column(name = "direct_other_year_demand_estimation")
    BigDecimal directOtherYearDemandEstimation;

    /** 小売年間推定需要 */
    @Column(name = "retail_year_demand_estimation")
    BigDecimal retailYearDemandEstimation;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the ssYearDemandEstimation.
     * 
     * @return the ssYearDemandEstimation
     */
    public BigDecimal getSsYearDemandEstimation() {
        return ssYearDemandEstimation;
    }

    /** 
     * Sets the ssYearDemandEstimation.
     * 
     * @param ssYearDemandEstimation the ssYearDemandEstimation
     */
    public void setSsYearDemandEstimation(BigDecimal ssYearDemandEstimation) {
        this.ssYearDemandEstimation = ssYearDemandEstimation;
    }

    /** 
     * Returns the rsYearDemandEstimation.
     * 
     * @return the rsYearDemandEstimation
     */
    public BigDecimal getRsYearDemandEstimation() {
        return rsYearDemandEstimation;
    }

    /** 
     * Sets the rsYearDemandEstimation.
     * 
     * @param rsYearDemandEstimation the rsYearDemandEstimation
     */
    public void setRsYearDemandEstimation(BigDecimal rsYearDemandEstimation) {
        this.rsYearDemandEstimation = rsYearDemandEstimation;
    }

    /** 
     * Returns the cdYearDemandEstimation.
     * 
     * @return the cdYearDemandEstimation
     */
    public BigDecimal getCdYearDemandEstimation() {
        return cdYearDemandEstimation;
    }

    /** 
     * Sets the cdYearDemandEstimation.
     * 
     * @param cdYearDemandEstimation the cdYearDemandEstimation
     */
    public void setCdYearDemandEstimation(BigDecimal cdYearDemandEstimation) {
        this.cdYearDemandEstimation = cdYearDemandEstimation;
    }

    /** 
     * Returns the psYearDemandEstimation.
     * 
     * @return the psYearDemandEstimation
     */
    public BigDecimal getPsYearDemandEstimation() {
        return psYearDemandEstimation;
    }

    /** 
     * Sets the psYearDemandEstimation.
     * 
     * @param psYearDemandEstimation the psYearDemandEstimation
     */
    public void setPsYearDemandEstimation(BigDecimal psYearDemandEstimation) {
        this.psYearDemandEstimation = psYearDemandEstimation;
    }

    /** 
     * Returns the csYearDemandEstimation.
     * 
     * @return the csYearDemandEstimation
     */
    public BigDecimal getCsYearDemandEstimation() {
        return csYearDemandEstimation;
    }

    /** 
     * Sets the csYearDemandEstimation.
     * 
     * @param csYearDemandEstimation the csYearDemandEstimation
     */
    public void setCsYearDemandEstimation(BigDecimal csYearDemandEstimation) {
        this.csYearDemandEstimation = csYearDemandEstimation;
    }

    /** 
     * Returns the hcYearDemandEstimation.
     * 
     * @return the hcYearDemandEstimation
     */
    public BigDecimal getHcYearDemandEstimation() {
        return hcYearDemandEstimation;
    }

    /** 
     * Sets the hcYearDemandEstimation.
     * 
     * @param hcYearDemandEstimation the hcYearDemandEstimation
     */
    public void setHcYearDemandEstimation(BigDecimal hcYearDemandEstimation) {
        this.hcYearDemandEstimation = hcYearDemandEstimation;
    }

    /** 
     * Returns the leaseYearDemandEstimation.
     * 
     * @return the leaseYearDemandEstimation
     */
    public BigDecimal getLeaseYearDemandEstimation() {
        return leaseYearDemandEstimation;
    }

    /** 
     * Sets the leaseYearDemandEstimation.
     * 
     * @param leaseYearDemandEstimation the leaseYearDemandEstimation
     */
    public void setLeaseYearDemandEstimation(BigDecimal leaseYearDemandEstimation) {
        this.leaseYearDemandEstimation = leaseYearDemandEstimation;
    }

    /** 
     * Returns the indirectOtherYearDemandEstimation.
     * 
     * @return the indirectOtherYearDemandEstimation
     */
    public BigDecimal getIndirectOtherYearDemandEstimation() {
        return indirectOtherYearDemandEstimation;
    }

    /** 
     * Sets the indirectOtherYearDemandEstimation.
     * 
     * @param indirectOtherYearDemandEstimation the indirectOtherYearDemandEstimation
     */
    public void setIndirectOtherYearDemandEstimation(BigDecimal indirectOtherYearDemandEstimation) {
        this.indirectOtherYearDemandEstimation = indirectOtherYearDemandEstimation;
    }

    /** 
     * Returns the truckYearDemandEstimation.
     * 
     * @return the truckYearDemandEstimation
     */
    public BigDecimal getTruckYearDemandEstimation() {
        return truckYearDemandEstimation;
    }

    /** 
     * Sets the truckYearDemandEstimation.
     * 
     * @param truckYearDemandEstimation the truckYearDemandEstimation
     */
    public void setTruckYearDemandEstimation(BigDecimal truckYearDemandEstimation) {
        this.truckYearDemandEstimation = truckYearDemandEstimation;
    }

    /** 
     * Returns the busYearDemandEstimation.
     * 
     * @return the busYearDemandEstimation
     */
    public BigDecimal getBusYearDemandEstimation() {
        return busYearDemandEstimation;
    }

    /** 
     * Sets the busYearDemandEstimation.
     * 
     * @param busYearDemandEstimation the busYearDemandEstimation
     */
    public void setBusYearDemandEstimation(BigDecimal busYearDemandEstimation) {
        this.busYearDemandEstimation = busYearDemandEstimation;
    }

    /** 
     * Returns the hireTaxiYearDemandEstimation.
     * 
     * @return the hireTaxiYearDemandEstimation
     */
    public BigDecimal getHireTaxiYearDemandEstimation() {
        return hireTaxiYearDemandEstimation;
    }

    /** 
     * Sets the hireTaxiYearDemandEstimation.
     * 
     * @param hireTaxiYearDemandEstimation the hireTaxiYearDemandEstimation
     */
    public void setHireTaxiYearDemandEstimation(BigDecimal hireTaxiYearDemandEstimation) {
        this.hireTaxiYearDemandEstimation = hireTaxiYearDemandEstimation;
    }

    /** 
     * Returns the directOtherYearDemandEstimation.
     * 
     * @return the directOtherYearDemandEstimation
     */
    public BigDecimal getDirectOtherYearDemandEstimation() {
        return directOtherYearDemandEstimation;
    }

    /** 
     * Sets the directOtherYearDemandEstimation.
     * 
     * @param directOtherYearDemandEstimation the directOtherYearDemandEstimation
     */
    public void setDirectOtherYearDemandEstimation(BigDecimal directOtherYearDemandEstimation) {
        this.directOtherYearDemandEstimation = directOtherYearDemandEstimation;
    }

    /** 
     * Returns the retailYearDemandEstimation.
     * 
     * @return the retailYearDemandEstimation
     */
    public BigDecimal getRetailYearDemandEstimation() {
        return retailYearDemandEstimation;
    }

    /** 
     * Sets the retailYearDemandEstimation.
     * 
     * @param retailYearDemandEstimation the retailYearDemandEstimation
     */
    public void setRetailYearDemandEstimation(BigDecimal retailYearDemandEstimation) {
        this.retailYearDemandEstimation = retailYearDemandEstimation;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}