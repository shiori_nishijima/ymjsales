package jp.co.yrc.mv.entity;

import java.sql.Date;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OpportunitiesCstmListener.class)
@Table(name = "opportunities_cstm")
public class OpportunitiesCstm {

    /** 案件CD */
    @Id
    @Column(name = "id_c")
    String idC;

    /** ごみ（未使用） */
    @Column(name = "aaa_c")
    String aaaC;

    /** 競合・既存キャリア */
    @Column(name = "op_compe_c")
    String opCompeC;

    /** 企業名備考 */
    @Column(name = "op_compname_memo_c")
    String opCompnameMemoC;

    /** 契約予定日 */
    @Column(name = "op_cont_plandate_c")
    Date opContPlandateC;

    /** データ　組織（未使用） */
    @Column(name = "op_data_organ_c")
    String opDataOrganC;

    /** ディペンデンシ内容 */
    @Column(name = "op_dep_detail_c")
    String opDepDetailC;

    /** ディペンデンシ */
    @Column(name = "op_depend_c")
    String opDependC;

    /** 終了サービス：サービス分類（未使用） */
    @Column(name = "op_endsv_cate_c")
    String opEndsvCateC;

    /** 終了サービス：種別（未使用） */
    @Column(name = "op_endsv_class_c")
    String opEndsvClassC;

    /** 終了サービス：サービス分類（未使用） */
    @Column(name = "op_endsv_num_c")
    String opEndsvNumC;

    /** 提案プラン2 */
    @Column(name = "op_plan2_c")
    String opPlan2C;

    /** 備考2 */
    @Column(name = "op_remarks2_c")
    String opRemarks2C;

    /** 終了サービス：金額/数量（未使用） */
    @Column(name = "op_endsv_unit_c")
    String opEndsvUnitC;

    /** 終了サービス：提供終了月（未使用） */
    @Column(name = "op_endsvend_mm_c")
    String opEndsvendMmC;

    /** 終了サービス：サービス提供終了年（未使用） */
    @Column(name = "op_endsvend_yyyy_c")
    String opEndsvendYyyyC;

    /** 終了サービス：提供開始月（未使用） */
    @Column(name = "op_endsvstart_mm_c")
    String opEndsvstartMmC;

    /** 終了サービス：サービス提供開始年（未使用） */
    @Column(name = "op_endsvstart_yyyy_c")
    String opEndsvstartYyyyC;

    /** 新規サービス：提供開始月（未使用） */
    @Column(name = "op_newsv_cate_c")
    String opNewsvCateC;

    /** 新規サービス：種別（未使用） */
    @Column(name = "op_newsv_class_c")
    String opNewsvClassC;

    /** 新規サービス：金額/数量（未使用） */
    @Column(name = "op_newsv_num_c")
    String opNewsvNumC;

    /** 提案プラン1 */
    @Column(name = "op_plan1_c")
    String opPlan1C;

    /** 備考1 */
    @Column(name = "op_remarks1_c")
    String opRemarks1C;

    /** 新規サービス：単位（未使用） */
    @Column(name = "op_newsv_unit_c")
    String opNewsvUnitC;

    /** 新規サービス：サービス提供終了月（未使用） */
    @Column(name = "op_newsvend_mm_c")
    String opNewsvendMmC;

    /** 新規サービス：サービス提供終了年（未使用） */
    @Column(name = "op_newsvend_yyyy_c")
    String opNewsvendYyyyC;

    /** 新規サービス：サービス提供開始月（未使用） */
    @Column(name = "op_newsvstart_mm_c")
    String opNewsvstartMmC;

    /** 新規サービス：サービス提供開始年（未使用） */
    @Column(name = "op_newsvstart_yyyy_c")
    String opNewsvstartYyyyC;

    /** 音声/携帯　組織（未使用） */
    @Column(name = "op_onseikeitai_organ_c")
    String opOnseikeitaiOrganC;

    /** 受注確度 */
    @Column(name = "op_order_poss_c")
    String opOrderPossC;

    /** オーナ（ごみ）（未使用） */
    @Column(name = "op_owner_c")
    String opOwnerC;

    /** ビジネスパートナ（未使用） */
    @Column(name = "op_partner_c")
    String opPartnerC;

    /** 営業（主管）（未使用） */
    @Column(name = "op_sales_main_c")
    String opSalesMainC;

    /** 営業（サポート）（未使用） */
    @Column(name = "op_sales_support_c")
    String opSalesSupportC;

    /** ステップ */
    @Column(name = "op_step_c")
    String opStepC;

    /** 進捗状況（未使用） */
    @Column(name = "op_sts_c")
    String opStsC;

    /** 主要サービス分類（未使用） */
    @Column(name = "op_sv_cate_c")
    String opSvCateC;

    /** フォーキャスト（旧コミットメント） */
    @Column(name = "op_commit_c")
    String opCommitC;

    /** フォーキャスト（未使用） */
    @Column(name = "op_forecast_c")
    String opForecastC;

    /** 案件ID生成用オートインクリメント（自動採番） */
    @Column(name = "opportunity_number_c")
    Integer opportunityNumberC;

    /** 案件ID（自動採番） */
    @Column(name = "anken_id_c")
    String ankenIdC;

    /** 問合せ番号（未使用） */
    @Column(name = "doc_id_c")
    String docIdC;

    /** 提案内容1 */
    @Column(name = "proposal1_c")
    String proposal1C;

    /** 提案価格1 */
    @Column(name = "proposal_price1_c")
    Double proposalPrice1C;

    /** 割引率1 */
    @Column(name = "discount_rate1_c")
    Double discountRate1C;

    /** 提案内容2 */
    @Column(name = "proposal2_c")
    String proposal2C;

    /** 提案価格2 */
    @Column(name = "proposal_price2_c")
    Double proposalPrice2C;

    /** 割引率2 */
    @Column(name = "discount_rate2_c")
    Double discountRate2C;

    /** 決着状況 */
    @Column(name = "closed_situation_c")
    String closedSituationC;

    /** 勝因／敗因 */
    @Column(name = "victory_defeat_cause_c")
    String victoryDefeatCauseC;

    /** 勝因／敗因詳細 */
    @Column(name = "victory_defeat_cause_detail_c")
    String victoryDefeatCauseDetailC;

    /** 進捗チェック（社内、競合） */
    @Column(name = "emphasis_strategy1_c")
    String emphasisStrategy1C;

    /** 進捗チェック（企業） */
    @Column(name = "emphasis_strategy2_c")
    String emphasisStrategy2C;

    /** 本部施策 */
    @Column(name = "measure_dept_c")
    String measureDeptC;

    /** 全社施策 */
    @Column(name = "measure_company_c")
    String measureCompanyC;

    /** Y興味 */
    @Column(name = "y_interest_c")
    String yInterestC;

    /** Y関心領域 */
    @Column(name = "y_interest_area_c")
    String yInterestAreaC;

    /** Y無し理由 */
    @Column(name = "y_ng_reason_c")
    String yNgReasonC;

    /** Y利用代理店 */
    @Column(name = "y_agency_c")
    String yAgencyC;

    /** Y利用媒体 */
    @Column(name = "y_medium_c")
    String yMediumC;

    /** Y備考欄 */
    @Column(name = "y_remarks_c")
    String yRemarksC;

    /** Y提案先URL */
    @Column(name = "y_posturl_c")
    String yPosturlC;

    /** 予備1 */
    @Column(name = "reserve1_c")
    String reserve1C;

    /** 予備2 */
    @Column(name = "reserve2_c")
    String reserve2C;

    /** 予備3 */
    @Column(name = "reserve3_c")
    String reserve3C;

    /** 予備4 */
    @Column(name = "reserve4_c")
    String reserve4C;

    /** 予備5 */
    @Column(name = "reserve5_c")
    String reserve5C;

    /** 予備6 */
    @Column(name = "reserve6_c")
    String reserve6C;

    /** 予備7 */
    @Column(name = "reserve7_c")
    String reserve7C;

    /** 予備8 */
    @Column(name = "reserve8_c")
    String reserve8C;

    /** 予備9 */
    @Column(name = "reserve9_c")
    String reserve9C;

    /** 予備10 */
    @Column(name = "reserve10_c")
    String reserve10C;

    /**  */
    @Column(name = "calling_agent")
    String callingAgent;

    /**  */
    @Column(name = "marginal_profit_c")
    Double marginalProfitC;

    /** 
     * Returns the idC.
     * 
     * @return the idC
     */
    public String getIdC() {
        return idC;
    }

    /** 
     * Sets the idC.
     * 
     * @param idC the idC
     */
    public void setIdC(String idC) {
        this.idC = idC;
    }

    /** 
     * Returns the aaaC.
     * 
     * @return the aaaC
     */
    public String getAaaC() {
        return aaaC;
    }

    /** 
     * Sets the aaaC.
     * 
     * @param aaaC the aaaC
     */
    public void setAaaC(String aaaC) {
        this.aaaC = aaaC;
    }

    /** 
     * Returns the opCompeC.
     * 
     * @return the opCompeC
     */
    public String getOpCompeC() {
        return opCompeC;
    }

    /** 
     * Sets the opCompeC.
     * 
     * @param opCompeC the opCompeC
     */
    public void setOpCompeC(String opCompeC) {
        this.opCompeC = opCompeC;
    }

    /** 
     * Returns the opCompnameMemoC.
     * 
     * @return the opCompnameMemoC
     */
    public String getOpCompnameMemoC() {
        return opCompnameMemoC;
    }

    /** 
     * Sets the opCompnameMemoC.
     * 
     * @param opCompnameMemoC the opCompnameMemoC
     */
    public void setOpCompnameMemoC(String opCompnameMemoC) {
        this.opCompnameMemoC = opCompnameMemoC;
    }

    /** 
     * Returns the opContPlandateC.
     * 
     * @return the opContPlandateC
     */
    public Date getOpContPlandateC() {
        return opContPlandateC;
    }

    /** 
     * Sets the opContPlandateC.
     * 
     * @param opContPlandateC the opContPlandateC
     */
    public void setOpContPlandateC(Date opContPlandateC) {
        this.opContPlandateC = opContPlandateC;
    }

    /** 
     * Returns the opDataOrganC.
     * 
     * @return the opDataOrganC
     */
    public String getOpDataOrganC() {
        return opDataOrganC;
    }

    /** 
     * Sets the opDataOrganC.
     * 
     * @param opDataOrganC the opDataOrganC
     */
    public void setOpDataOrganC(String opDataOrganC) {
        this.opDataOrganC = opDataOrganC;
    }

    /** 
     * Returns the opDepDetailC.
     * 
     * @return the opDepDetailC
     */
    public String getOpDepDetailC() {
        return opDepDetailC;
    }

    /** 
     * Sets the opDepDetailC.
     * 
     * @param opDepDetailC the opDepDetailC
     */
    public void setOpDepDetailC(String opDepDetailC) {
        this.opDepDetailC = opDepDetailC;
    }

    /** 
     * Returns the opDependC.
     * 
     * @return the opDependC
     */
    public String getOpDependC() {
        return opDependC;
    }

    /** 
     * Sets the opDependC.
     * 
     * @param opDependC the opDependC
     */
    public void setOpDependC(String opDependC) {
        this.opDependC = opDependC;
    }

    /** 
     * Returns the opEndsvCateC.
     * 
     * @return the opEndsvCateC
     */
    public String getOpEndsvCateC() {
        return opEndsvCateC;
    }

    /** 
     * Sets the opEndsvCateC.
     * 
     * @param opEndsvCateC the opEndsvCateC
     */
    public void setOpEndsvCateC(String opEndsvCateC) {
        this.opEndsvCateC = opEndsvCateC;
    }

    /** 
     * Returns the opEndsvClassC.
     * 
     * @return the opEndsvClassC
     */
    public String getOpEndsvClassC() {
        return opEndsvClassC;
    }

    /** 
     * Sets the opEndsvClassC.
     * 
     * @param opEndsvClassC the opEndsvClassC
     */
    public void setOpEndsvClassC(String opEndsvClassC) {
        this.opEndsvClassC = opEndsvClassC;
    }

    /** 
     * Returns the opEndsvNumC.
     * 
     * @return the opEndsvNumC
     */
    public String getOpEndsvNumC() {
        return opEndsvNumC;
    }

    /** 
     * Sets the opEndsvNumC.
     * 
     * @param opEndsvNumC the opEndsvNumC
     */
    public void setOpEndsvNumC(String opEndsvNumC) {
        this.opEndsvNumC = opEndsvNumC;
    }

    /** 
     * Returns the opPlan2C.
     * 
     * @return the opPlan2C
     */
    public String getOpPlan2C() {
        return opPlan2C;
    }

    /** 
     * Sets the opPlan2C.
     * 
     * @param opPlan2C the opPlan2C
     */
    public void setOpPlan2C(String opPlan2C) {
        this.opPlan2C = opPlan2C;
    }

    /** 
     * Returns the opRemarks2C.
     * 
     * @return the opRemarks2C
     */
    public String getOpRemarks2C() {
        return opRemarks2C;
    }

    /** 
     * Sets the opRemarks2C.
     * 
     * @param opRemarks2C the opRemarks2C
     */
    public void setOpRemarks2C(String opRemarks2C) {
        this.opRemarks2C = opRemarks2C;
    }

    /** 
     * Returns the opEndsvUnitC.
     * 
     * @return the opEndsvUnitC
     */
    public String getOpEndsvUnitC() {
        return opEndsvUnitC;
    }

    /** 
     * Sets the opEndsvUnitC.
     * 
     * @param opEndsvUnitC the opEndsvUnitC
     */
    public void setOpEndsvUnitC(String opEndsvUnitC) {
        this.opEndsvUnitC = opEndsvUnitC;
    }

    /** 
     * Returns the opEndsvendMmC.
     * 
     * @return the opEndsvendMmC
     */
    public String getOpEndsvendMmC() {
        return opEndsvendMmC;
    }

    /** 
     * Sets the opEndsvendMmC.
     * 
     * @param opEndsvendMmC the opEndsvendMmC
     */
    public void setOpEndsvendMmC(String opEndsvendMmC) {
        this.opEndsvendMmC = opEndsvendMmC;
    }

    /** 
     * Returns the opEndsvendYyyyC.
     * 
     * @return the opEndsvendYyyyC
     */
    public String getOpEndsvendYyyyC() {
        return opEndsvendYyyyC;
    }

    /** 
     * Sets the opEndsvendYyyyC.
     * 
     * @param opEndsvendYyyyC the opEndsvendYyyyC
     */
    public void setOpEndsvendYyyyC(String opEndsvendYyyyC) {
        this.opEndsvendYyyyC = opEndsvendYyyyC;
    }

    /** 
     * Returns the opEndsvstartMmC.
     * 
     * @return the opEndsvstartMmC
     */
    public String getOpEndsvstartMmC() {
        return opEndsvstartMmC;
    }

    /** 
     * Sets the opEndsvstartMmC.
     * 
     * @param opEndsvstartMmC the opEndsvstartMmC
     */
    public void setOpEndsvstartMmC(String opEndsvstartMmC) {
        this.opEndsvstartMmC = opEndsvstartMmC;
    }

    /** 
     * Returns the opEndsvstartYyyyC.
     * 
     * @return the opEndsvstartYyyyC
     */
    public String getOpEndsvstartYyyyC() {
        return opEndsvstartYyyyC;
    }

    /** 
     * Sets the opEndsvstartYyyyC.
     * 
     * @param opEndsvstartYyyyC the opEndsvstartYyyyC
     */
    public void setOpEndsvstartYyyyC(String opEndsvstartYyyyC) {
        this.opEndsvstartYyyyC = opEndsvstartYyyyC;
    }

    /** 
     * Returns the opNewsvCateC.
     * 
     * @return the opNewsvCateC
     */
    public String getOpNewsvCateC() {
        return opNewsvCateC;
    }

    /** 
     * Sets the opNewsvCateC.
     * 
     * @param opNewsvCateC the opNewsvCateC
     */
    public void setOpNewsvCateC(String opNewsvCateC) {
        this.opNewsvCateC = opNewsvCateC;
    }

    /** 
     * Returns the opNewsvClassC.
     * 
     * @return the opNewsvClassC
     */
    public String getOpNewsvClassC() {
        return opNewsvClassC;
    }

    /** 
     * Sets the opNewsvClassC.
     * 
     * @param opNewsvClassC the opNewsvClassC
     */
    public void setOpNewsvClassC(String opNewsvClassC) {
        this.opNewsvClassC = opNewsvClassC;
    }

    /** 
     * Returns the opNewsvNumC.
     * 
     * @return the opNewsvNumC
     */
    public String getOpNewsvNumC() {
        return opNewsvNumC;
    }

    /** 
     * Sets the opNewsvNumC.
     * 
     * @param opNewsvNumC the opNewsvNumC
     */
    public void setOpNewsvNumC(String opNewsvNumC) {
        this.opNewsvNumC = opNewsvNumC;
    }

    /** 
     * Returns the opPlan1C.
     * 
     * @return the opPlan1C
     */
    public String getOpPlan1C() {
        return opPlan1C;
    }

    /** 
     * Sets the opPlan1C.
     * 
     * @param opPlan1C the opPlan1C
     */
    public void setOpPlan1C(String opPlan1C) {
        this.opPlan1C = opPlan1C;
    }

    /** 
     * Returns the opRemarks1C.
     * 
     * @return the opRemarks1C
     */
    public String getOpRemarks1C() {
        return opRemarks1C;
    }

    /** 
     * Sets the opRemarks1C.
     * 
     * @param opRemarks1C the opRemarks1C
     */
    public void setOpRemarks1C(String opRemarks1C) {
        this.opRemarks1C = opRemarks1C;
    }

    /** 
     * Returns the opNewsvUnitC.
     * 
     * @return the opNewsvUnitC
     */
    public String getOpNewsvUnitC() {
        return opNewsvUnitC;
    }

    /** 
     * Sets the opNewsvUnitC.
     * 
     * @param opNewsvUnitC the opNewsvUnitC
     */
    public void setOpNewsvUnitC(String opNewsvUnitC) {
        this.opNewsvUnitC = opNewsvUnitC;
    }

    /** 
     * Returns the opNewsvendMmC.
     * 
     * @return the opNewsvendMmC
     */
    public String getOpNewsvendMmC() {
        return opNewsvendMmC;
    }

    /** 
     * Sets the opNewsvendMmC.
     * 
     * @param opNewsvendMmC the opNewsvendMmC
     */
    public void setOpNewsvendMmC(String opNewsvendMmC) {
        this.opNewsvendMmC = opNewsvendMmC;
    }

    /** 
     * Returns the opNewsvendYyyyC.
     * 
     * @return the opNewsvendYyyyC
     */
    public String getOpNewsvendYyyyC() {
        return opNewsvendYyyyC;
    }

    /** 
     * Sets the opNewsvendYyyyC.
     * 
     * @param opNewsvendYyyyC the opNewsvendYyyyC
     */
    public void setOpNewsvendYyyyC(String opNewsvendYyyyC) {
        this.opNewsvendYyyyC = opNewsvendYyyyC;
    }

    /** 
     * Returns the opNewsvstartMmC.
     * 
     * @return the opNewsvstartMmC
     */
    public String getOpNewsvstartMmC() {
        return opNewsvstartMmC;
    }

    /** 
     * Sets the opNewsvstartMmC.
     * 
     * @param opNewsvstartMmC the opNewsvstartMmC
     */
    public void setOpNewsvstartMmC(String opNewsvstartMmC) {
        this.opNewsvstartMmC = opNewsvstartMmC;
    }

    /** 
     * Returns the opNewsvstartYyyyC.
     * 
     * @return the opNewsvstartYyyyC
     */
    public String getOpNewsvstartYyyyC() {
        return opNewsvstartYyyyC;
    }

    /** 
     * Sets the opNewsvstartYyyyC.
     * 
     * @param opNewsvstartYyyyC the opNewsvstartYyyyC
     */
    public void setOpNewsvstartYyyyC(String opNewsvstartYyyyC) {
        this.opNewsvstartYyyyC = opNewsvstartYyyyC;
    }

    /** 
     * Returns the opOnseikeitaiOrganC.
     * 
     * @return the opOnseikeitaiOrganC
     */
    public String getOpOnseikeitaiOrganC() {
        return opOnseikeitaiOrganC;
    }

    /** 
     * Sets the opOnseikeitaiOrganC.
     * 
     * @param opOnseikeitaiOrganC the opOnseikeitaiOrganC
     */
    public void setOpOnseikeitaiOrganC(String opOnseikeitaiOrganC) {
        this.opOnseikeitaiOrganC = opOnseikeitaiOrganC;
    }

    /** 
     * Returns the opOrderPossC.
     * 
     * @return the opOrderPossC
     */
    public String getOpOrderPossC() {
        return opOrderPossC;
    }

    /** 
     * Sets the opOrderPossC.
     * 
     * @param opOrderPossC the opOrderPossC
     */
    public void setOpOrderPossC(String opOrderPossC) {
        this.opOrderPossC = opOrderPossC;
    }

    /** 
     * Returns the opOwnerC.
     * 
     * @return the opOwnerC
     */
    public String getOpOwnerC() {
        return opOwnerC;
    }

    /** 
     * Sets the opOwnerC.
     * 
     * @param opOwnerC the opOwnerC
     */
    public void setOpOwnerC(String opOwnerC) {
        this.opOwnerC = opOwnerC;
    }

    /** 
     * Returns the opPartnerC.
     * 
     * @return the opPartnerC
     */
    public String getOpPartnerC() {
        return opPartnerC;
    }

    /** 
     * Sets the opPartnerC.
     * 
     * @param opPartnerC the opPartnerC
     */
    public void setOpPartnerC(String opPartnerC) {
        this.opPartnerC = opPartnerC;
    }

    /** 
     * Returns the opSalesMainC.
     * 
     * @return the opSalesMainC
     */
    public String getOpSalesMainC() {
        return opSalesMainC;
    }

    /** 
     * Sets the opSalesMainC.
     * 
     * @param opSalesMainC the opSalesMainC
     */
    public void setOpSalesMainC(String opSalesMainC) {
        this.opSalesMainC = opSalesMainC;
    }

    /** 
     * Returns the opSalesSupportC.
     * 
     * @return the opSalesSupportC
     */
    public String getOpSalesSupportC() {
        return opSalesSupportC;
    }

    /** 
     * Sets the opSalesSupportC.
     * 
     * @param opSalesSupportC the opSalesSupportC
     */
    public void setOpSalesSupportC(String opSalesSupportC) {
        this.opSalesSupportC = opSalesSupportC;
    }

    /** 
     * Returns the opStepC.
     * 
     * @return the opStepC
     */
    public String getOpStepC() {
        return opStepC;
    }

    /** 
     * Sets the opStepC.
     * 
     * @param opStepC the opStepC
     */
    public void setOpStepC(String opStepC) {
        this.opStepC = opStepC;
    }

    /** 
     * Returns the opStsC.
     * 
     * @return the opStsC
     */
    public String getOpStsC() {
        return opStsC;
    }

    /** 
     * Sets the opStsC.
     * 
     * @param opStsC the opStsC
     */
    public void setOpStsC(String opStsC) {
        this.opStsC = opStsC;
    }

    /** 
     * Returns the opSvCateC.
     * 
     * @return the opSvCateC
     */
    public String getOpSvCateC() {
        return opSvCateC;
    }

    /** 
     * Sets the opSvCateC.
     * 
     * @param opSvCateC the opSvCateC
     */
    public void setOpSvCateC(String opSvCateC) {
        this.opSvCateC = opSvCateC;
    }

    /** 
     * Returns the opCommitC.
     * 
     * @return the opCommitC
     */
    public String getOpCommitC() {
        return opCommitC;
    }

    /** 
     * Sets the opCommitC.
     * 
     * @param opCommitC the opCommitC
     */
    public void setOpCommitC(String opCommitC) {
        this.opCommitC = opCommitC;
    }

    /** 
     * Returns the opForecastC.
     * 
     * @return the opForecastC
     */
    public String getOpForecastC() {
        return opForecastC;
    }

    /** 
     * Sets the opForecastC.
     * 
     * @param opForecastC the opForecastC
     */
    public void setOpForecastC(String opForecastC) {
        this.opForecastC = opForecastC;
    }

    /** 
     * Returns the opportunityNumberC.
     * 
     * @return the opportunityNumberC
     */
    public Integer getOpportunityNumberC() {
        return opportunityNumberC;
    }

    /** 
     * Sets the opportunityNumberC.
     * 
     * @param opportunityNumberC the opportunityNumberC
     */
    public void setOpportunityNumberC(Integer opportunityNumberC) {
        this.opportunityNumberC = opportunityNumberC;
    }

    /** 
     * Returns the ankenIdC.
     * 
     * @return the ankenIdC
     */
    public String getAnkenIdC() {
        return ankenIdC;
    }

    /** 
     * Sets the ankenIdC.
     * 
     * @param ankenIdC the ankenIdC
     */
    public void setAnkenIdC(String ankenIdC) {
        this.ankenIdC = ankenIdC;
    }

    /** 
     * Returns the docIdC.
     * 
     * @return the docIdC
     */
    public String getDocIdC() {
        return docIdC;
    }

    /** 
     * Sets the docIdC.
     * 
     * @param docIdC the docIdC
     */
    public void setDocIdC(String docIdC) {
        this.docIdC = docIdC;
    }

    /** 
     * Returns the proposal1C.
     * 
     * @return the proposal1C
     */
    public String getProposal1C() {
        return proposal1C;
    }

    /** 
     * Sets the proposal1C.
     * 
     * @param proposal1C the proposal1C
     */
    public void setProposal1C(String proposal1C) {
        this.proposal1C = proposal1C;
    }

    /** 
     * Returns the proposalPrice1C.
     * 
     * @return the proposalPrice1C
     */
    public Double getProposalPrice1C() {
        return proposalPrice1C;
    }

    /** 
     * Sets the proposalPrice1C.
     * 
     * @param proposalPrice1C the proposalPrice1C
     */
    public void setProposalPrice1C(Double proposalPrice1C) {
        this.proposalPrice1C = proposalPrice1C;
    }

    /** 
     * Returns the discountRate1C.
     * 
     * @return the discountRate1C
     */
    public Double getDiscountRate1C() {
        return discountRate1C;
    }

    /** 
     * Sets the discountRate1C.
     * 
     * @param discountRate1C the discountRate1C
     */
    public void setDiscountRate1C(Double discountRate1C) {
        this.discountRate1C = discountRate1C;
    }

    /** 
     * Returns the proposal2C.
     * 
     * @return the proposal2C
     */
    public String getProposal2C() {
        return proposal2C;
    }

    /** 
     * Sets the proposal2C.
     * 
     * @param proposal2C the proposal2C
     */
    public void setProposal2C(String proposal2C) {
        this.proposal2C = proposal2C;
    }

    /** 
     * Returns the proposalPrice2C.
     * 
     * @return the proposalPrice2C
     */
    public Double getProposalPrice2C() {
        return proposalPrice2C;
    }

    /** 
     * Sets the proposalPrice2C.
     * 
     * @param proposalPrice2C the proposalPrice2C
     */
    public void setProposalPrice2C(Double proposalPrice2C) {
        this.proposalPrice2C = proposalPrice2C;
    }

    /** 
     * Returns the discountRate2C.
     * 
     * @return the discountRate2C
     */
    public Double getDiscountRate2C() {
        return discountRate2C;
    }

    /** 
     * Sets the discountRate2C.
     * 
     * @param discountRate2C the discountRate2C
     */
    public void setDiscountRate2C(Double discountRate2C) {
        this.discountRate2C = discountRate2C;
    }

    /** 
     * Returns the closedSituationC.
     * 
     * @return the closedSituationC
     */
    public String getClosedSituationC() {
        return closedSituationC;
    }

    /** 
     * Sets the closedSituationC.
     * 
     * @param closedSituationC the closedSituationC
     */
    public void setClosedSituationC(String closedSituationC) {
        this.closedSituationC = closedSituationC;
    }

    /** 
     * Returns the victoryDefeatCauseC.
     * 
     * @return the victoryDefeatCauseC
     */
    public String getVictoryDefeatCauseC() {
        return victoryDefeatCauseC;
    }

    /** 
     * Sets the victoryDefeatCauseC.
     * 
     * @param victoryDefeatCauseC the victoryDefeatCauseC
     */
    public void setVictoryDefeatCauseC(String victoryDefeatCauseC) {
        this.victoryDefeatCauseC = victoryDefeatCauseC;
    }

    /** 
     * Returns the victoryDefeatCauseDetailC.
     * 
     * @return the victoryDefeatCauseDetailC
     */
    public String getVictoryDefeatCauseDetailC() {
        return victoryDefeatCauseDetailC;
    }

    /** 
     * Sets the victoryDefeatCauseDetailC.
     * 
     * @param victoryDefeatCauseDetailC the victoryDefeatCauseDetailC
     */
    public void setVictoryDefeatCauseDetailC(String victoryDefeatCauseDetailC) {
        this.victoryDefeatCauseDetailC = victoryDefeatCauseDetailC;
    }

    /** 
     * Returns the emphasisStrategy1C.
     * 
     * @return the emphasisStrategy1C
     */
    public String getEmphasisStrategy1C() {
        return emphasisStrategy1C;
    }

    /** 
     * Sets the emphasisStrategy1C.
     * 
     * @param emphasisStrategy1C the emphasisStrategy1C
     */
    public void setEmphasisStrategy1C(String emphasisStrategy1C) {
        this.emphasisStrategy1C = emphasisStrategy1C;
    }

    /** 
     * Returns the emphasisStrategy2C.
     * 
     * @return the emphasisStrategy2C
     */
    public String getEmphasisStrategy2C() {
        return emphasisStrategy2C;
    }

    /** 
     * Sets the emphasisStrategy2C.
     * 
     * @param emphasisStrategy2C the emphasisStrategy2C
     */
    public void setEmphasisStrategy2C(String emphasisStrategy2C) {
        this.emphasisStrategy2C = emphasisStrategy2C;
    }

    /** 
     * Returns the measureDeptC.
     * 
     * @return the measureDeptC
     */
    public String getMeasureDeptC() {
        return measureDeptC;
    }

    /** 
     * Sets the measureDeptC.
     * 
     * @param measureDeptC the measureDeptC
     */
    public void setMeasureDeptC(String measureDeptC) {
        this.measureDeptC = measureDeptC;
    }

    /** 
     * Returns the measureCompanyC.
     * 
     * @return the measureCompanyC
     */
    public String getMeasureCompanyC() {
        return measureCompanyC;
    }

    /** 
     * Sets the measureCompanyC.
     * 
     * @param measureCompanyC the measureCompanyC
     */
    public void setMeasureCompanyC(String measureCompanyC) {
        this.measureCompanyC = measureCompanyC;
    }

    /** 
     * Returns the yInterestC.
     * 
     * @return the yInterestC
     */
    public String getYInterestC() {
        return yInterestC;
    }

    /** 
     * Sets the yInterestC.
     * 
     * @param yInterestC the yInterestC
     */
    public void setYInterestC(String yInterestC) {
        this.yInterestC = yInterestC;
    }

    /** 
     * Returns the yInterestAreaC.
     * 
     * @return the yInterestAreaC
     */
    public String getYInterestAreaC() {
        return yInterestAreaC;
    }

    /** 
     * Sets the yInterestAreaC.
     * 
     * @param yInterestAreaC the yInterestAreaC
     */
    public void setYInterestAreaC(String yInterestAreaC) {
        this.yInterestAreaC = yInterestAreaC;
    }

    /** 
     * Returns the yNgReasonC.
     * 
     * @return the yNgReasonC
     */
    public String getYNgReasonC() {
        return yNgReasonC;
    }

    /** 
     * Sets the yNgReasonC.
     * 
     * @param yNgReasonC the yNgReasonC
     */
    public void setYNgReasonC(String yNgReasonC) {
        this.yNgReasonC = yNgReasonC;
    }

    /** 
     * Returns the yAgencyC.
     * 
     * @return the yAgencyC
     */
    public String getYAgencyC() {
        return yAgencyC;
    }

    /** 
     * Sets the yAgencyC.
     * 
     * @param yAgencyC the yAgencyC
     */
    public void setYAgencyC(String yAgencyC) {
        this.yAgencyC = yAgencyC;
    }

    /** 
     * Returns the yMediumC.
     * 
     * @return the yMediumC
     */
    public String getYMediumC() {
        return yMediumC;
    }

    /** 
     * Sets the yMediumC.
     * 
     * @param yMediumC the yMediumC
     */
    public void setYMediumC(String yMediumC) {
        this.yMediumC = yMediumC;
    }

    /** 
     * Returns the yRemarksC.
     * 
     * @return the yRemarksC
     */
    public String getYRemarksC() {
        return yRemarksC;
    }

    /** 
     * Sets the yRemarksC.
     * 
     * @param yRemarksC the yRemarksC
     */
    public void setYRemarksC(String yRemarksC) {
        this.yRemarksC = yRemarksC;
    }

    /** 
     * Returns the yPosturlC.
     * 
     * @return the yPosturlC
     */
    public String getYPosturlC() {
        return yPosturlC;
    }

    /** 
     * Sets the yPosturlC.
     * 
     * @param yPosturlC the yPosturlC
     */
    public void setYPosturlC(String yPosturlC) {
        this.yPosturlC = yPosturlC;
    }

    /** 
     * Returns the reserve1C.
     * 
     * @return the reserve1C
     */
    public String getReserve1C() {
        return reserve1C;
    }

    /** 
     * Sets the reserve1C.
     * 
     * @param reserve1C the reserve1C
     */
    public void setReserve1C(String reserve1C) {
        this.reserve1C = reserve1C;
    }

    /** 
     * Returns the reserve2C.
     * 
     * @return the reserve2C
     */
    public String getReserve2C() {
        return reserve2C;
    }

    /** 
     * Sets the reserve2C.
     * 
     * @param reserve2C the reserve2C
     */
    public void setReserve2C(String reserve2C) {
        this.reserve2C = reserve2C;
    }

    /** 
     * Returns the reserve3C.
     * 
     * @return the reserve3C
     */
    public String getReserve3C() {
        return reserve3C;
    }

    /** 
     * Sets the reserve3C.
     * 
     * @param reserve3C the reserve3C
     */
    public void setReserve3C(String reserve3C) {
        this.reserve3C = reserve3C;
    }

    /** 
     * Returns the reserve4C.
     * 
     * @return the reserve4C
     */
    public String getReserve4C() {
        return reserve4C;
    }

    /** 
     * Sets the reserve4C.
     * 
     * @param reserve4C the reserve4C
     */
    public void setReserve4C(String reserve4C) {
        this.reserve4C = reserve4C;
    }

    /** 
     * Returns the reserve5C.
     * 
     * @return the reserve5C
     */
    public String getReserve5C() {
        return reserve5C;
    }

    /** 
     * Sets the reserve5C.
     * 
     * @param reserve5C the reserve5C
     */
    public void setReserve5C(String reserve5C) {
        this.reserve5C = reserve5C;
    }

    /** 
     * Returns the reserve6C.
     * 
     * @return the reserve6C
     */
    public String getReserve6C() {
        return reserve6C;
    }

    /** 
     * Sets the reserve6C.
     * 
     * @param reserve6C the reserve6C
     */
    public void setReserve6C(String reserve6C) {
        this.reserve6C = reserve6C;
    }

    /** 
     * Returns the reserve7C.
     * 
     * @return the reserve7C
     */
    public String getReserve7C() {
        return reserve7C;
    }

    /** 
     * Sets the reserve7C.
     * 
     * @param reserve7C the reserve7C
     */
    public void setReserve7C(String reserve7C) {
        this.reserve7C = reserve7C;
    }

    /** 
     * Returns the reserve8C.
     * 
     * @return the reserve8C
     */
    public String getReserve8C() {
        return reserve8C;
    }

    /** 
     * Sets the reserve8C.
     * 
     * @param reserve8C the reserve8C
     */
    public void setReserve8C(String reserve8C) {
        this.reserve8C = reserve8C;
    }

    /** 
     * Returns the reserve9C.
     * 
     * @return the reserve9C
     */
    public String getReserve9C() {
        return reserve9C;
    }

    /** 
     * Sets the reserve9C.
     * 
     * @param reserve9C the reserve9C
     */
    public void setReserve9C(String reserve9C) {
        this.reserve9C = reserve9C;
    }

    /** 
     * Returns the reserve10C.
     * 
     * @return the reserve10C
     */
    public String getReserve10C() {
        return reserve10C;
    }

    /** 
     * Sets the reserve10C.
     * 
     * @param reserve10C the reserve10C
     */
    public void setReserve10C(String reserve10C) {
        this.reserve10C = reserve10C;
    }

    /** 
     * Returns the callingAgent.
     * 
     * @return the callingAgent
     */
    public String getCallingAgent() {
        return callingAgent;
    }

    /** 
     * Sets the callingAgent.
     * 
     * @param callingAgent the callingAgent
     */
    public void setCallingAgent(String callingAgent) {
        this.callingAgent = callingAgent;
    }

    /** 
     * Returns the marginalProfitC.
     * 
     * @return the marginalProfitC
     */
    public Double getMarginalProfitC() {
        return marginalProfitC;
    }

    /** 
     * Sets the marginalProfitC.
     * 
     * @param marginalProfitC the marginalProfitC
     */
    public void setMarginalProfitC(Double marginalProfitC) {
        this.marginalProfitC = marginalProfitC;
    }
}