package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class AccountsHistoryListener implements EntityListener<AccountsHistory> {

    @Override
    public void preInsert(AccountsHistory entity, PreInsertContext<AccountsHistory> context) {
    }

    @Override
    public void preUpdate(AccountsHistory entity, PreUpdateContext<AccountsHistory> context) {
    }

    @Override
    public void preDelete(AccountsHistory entity, PreDeleteContext<AccountsHistory> context) {
    }

    @Override
    public void postInsert(AccountsHistory entity, PostInsertContext<AccountsHistory> context) {
    }

    @Override
    public void postUpdate(AccountsHistory entity, PostUpdateContext<AccountsHistory> context) {
    }

    @Override
    public void postDelete(AccountsHistory entity, PostDeleteContext<AccountsHistory> context) {
    }
}