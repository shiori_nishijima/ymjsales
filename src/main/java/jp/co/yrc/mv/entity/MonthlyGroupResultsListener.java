package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyGroupResultsListener implements EntityListener<MonthlyGroupResults> {

    @Override
    public void preInsert(MonthlyGroupResults entity, PreInsertContext<MonthlyGroupResults> context) {
    }

    @Override
    public void preUpdate(MonthlyGroupResults entity, PreUpdateContext<MonthlyGroupResults> context) {
    }

    @Override
    public void preDelete(MonthlyGroupResults entity, PreDeleteContext<MonthlyGroupResults> context) {
    }

    @Override
    public void postInsert(MonthlyGroupResults entity, PostInsertContext<MonthlyGroupResults> context) {
    }

    @Override
    public void postUpdate(MonthlyGroupResults entity, PostUpdateContext<MonthlyGroupResults> context) {
    }

    @Override
    public void postDelete(MonthlyGroupResults entity, PostDeleteContext<MonthlyGroupResults> context) {
    }
}