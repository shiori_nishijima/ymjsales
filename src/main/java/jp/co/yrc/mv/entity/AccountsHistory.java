package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = AccountsHistoryListener.class)
@Table(name = "accounts_history")
public class AccountsHistory {

    /** 得意先コード */
    @Id
    @Column(name = "id")
    String id;

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** 年月 */
    @Column(name = "yearmonth")
    Integer yearmonth;

    /** 名称 */
    @Column(name = "name")
    String name;

    /** 名称（母国語） */
    @Column(name = "name_native")
    String nameNative;

    /** 郵便番号 */
    @Column(name = "billing_address_postalcode")
    String billingAddressPostalcode;

    /** 住所 */
    @Column(name = "billing_address")
    String billingAddress;

    /** 住所（母国語） */
    @Column(name = "billing_address_native")
    String billingAddressNative;

    /** 親コード */
    @Column(name = "parent_id")
    String parentId;

    /** 担当者 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 販社コード */
    @Column(name = "sales_company_code")
    String salesCompanyCode;

    /** 部門コード */
    @Column(name = "department_code")
    String departmentCode;

    /** 営業所コード */
    @Column(name = "sales_office_code")
    String salesOfficeCode;

    /** 検索用名称 */
    @Column(name = "name_for_search")
    String nameForSearch;

    /** 検索用名称（母国語） */
    @Column(name = "name_for_search_native")
    String nameForSearchNative;

    /** 販路 */
    @Column(name = "market_id")
    String marketId;

    /** 銘柄 */
    @Column(name = "brand_id")
    String brandId;

    /** 持軒数カウント対象 */
    @Column(name = "own_count")
    byte[] ownCount;

    /** 取引軒数カウント対象 */
    @Column(name = "deal_count")
    byte[] dealCount;

    /** グルーピング重点 */
    @Column(name = "group_important")
    byte[] groupImportant;

    /** グルーピング新規 */
    @Column(name = "group_new")
    byte[] groupNew;

    /** グルーピング深耕 */
    @Column(name = "group_deep_plowing")
    byte[] groupDeepPlowing;

    /** グルーピングYコンセプトショップ */
    @Column(name = "group_y_cp")
    byte[] groupYCp;

    /** グルーピング他社コンセプトショップ */
    @Column(name = "group_other_cp")
    byte[] groupOtherCp;

    /** グルーピング1G */
    @Column(name = "group_one")
    byte[] groupOne;

    /** グルーピング2G */
    @Column(name = "group_two")
    byte[] groupTwo;

    /** グルーピング3G */
    @Column(name = "group_three")
    byte[] groupThree;

    /** グルーピング4G */
    @Column(name = "group_four")
    byte[] groupFour;

    /** グルーピング5G */
    @Column(name = "group_five")
    byte[] groupFive;

    /** 売上ランク */
    @Column(name = "sales_rank")
    String salesRank;

    /** 需要本数 */
    @Column(name = "demand_number")
    Integer demandNumber;

    /** 推定ISSYH */
    @Column(name = "iss_yh")
    Integer issYh;

    /** 推定ISSBS */
    @Column(name = "iss_bs")
    Integer issBs;

    /** 推定ISSDL */
    @Column(name = "iss_dl")
    Integer issDl;

    /** 推定ISSTY */
    @Column(name = "iss_ty")
    Integer issTy;

    /** 推定ISSGY */
    @Column(name = "iss_gy")
    Integer issGy;

    /** 推定ISSMI */
    @Column(name = "iss_mi")
    Integer issMi;

    /** 推定ISSPB */
    @Column(name = "iss_pb")
    Integer issPb;

    /** 推定ISSその他 */
    @Column(name = "iss_other")
    Integer issOther;

    /** 未得意先フラグ */
    @Column(name = "undeal")
    byte[] undeal;

    /** 削除済み */
    @Column(name = "deleted")
    byte[] deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the yearmonth.
     * 
     * @return the yearmonth
     */
    public Integer getYearmonth() {
        return yearmonth;
    }

    /** 
     * Sets the yearmonth.
     * 
     * @param yearmonth the yearmonth
     */
    public void setYearmonth(Integer yearmonth) {
        this.yearmonth = yearmonth;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the nameNative.
     * 
     * @return the nameNative
     */
    public String getNameNative() {
        return nameNative;
    }

    /** 
     * Sets the nameNative.
     * 
     * @param nameNative the nameNative
     */
    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }

    /** 
     * Returns the billingAddressPostalcode.
     * 
     * @return the billingAddressPostalcode
     */
    public String getBillingAddressPostalcode() {
        return billingAddressPostalcode;
    }

    /** 
     * Sets the billingAddressPostalcode.
     * 
     * @param billingAddressPostalcode the billingAddressPostalcode
     */
    public void setBillingAddressPostalcode(String billingAddressPostalcode) {
        this.billingAddressPostalcode = billingAddressPostalcode;
    }

    /** 
     * Returns the billingAddress.
     * 
     * @return the billingAddress
     */
    public String getBillingAddress() {
        return this.billingAddress;
    }

    /** 
     * Sets the billingAddressState.
     * 
     * @param billingAddressState the billingAddressState
     */
    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /** 
     * Returns the billingAddressStreet.
     * 
     * @return the billingAddressStreet
     */
    public String getBillingAddressNative() {
        return billingAddressNative;
    }

    /** 
     * Sets the billingAddressNative.
     * 
     * @param billingAddressNative the billingAddressNative
     */
    public void setBillingAddressNative(String billingAddressNative) {
        this.billingAddressNative = billingAddressNative;
    }

    /** 
     * Returns the parentId.
     * 
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /** 
     * Sets the parentId.
     * 
     * @param parentId the parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the salesCompanyCode.
     * 
     * @return the salesCompanyCode
     */
    public String getSalesCompanyCode() {
        return salesCompanyCode;
    }

    /** 
     * Sets the salesCompanyCode.
     * 
     * @param salesCompanyCode the salesCompanyCode
     */
    public void setSalesCompanyCode(String salesCompanyCode) {
        this.salesCompanyCode = salesCompanyCode;
    }

    /** 
     * Returns the departmentCode.
     * 
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /** 
     * Sets the departmentCode.
     * 
     * @param departmentCode the departmentCode
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /** 
     * Returns the salesOfficeCode.
     * 
     * @return the salesOfficeCode
     */
    public String getSalesOfficeCode() {
        return salesOfficeCode;
    }

    /** 
     * Sets the salesOfficeCode.
     * 
     * @param salesOfficeCode the salesOfficeCode
     */
    public void setSalesOfficeCode(String salesOfficeCode) {
        this.salesOfficeCode = salesOfficeCode;
    }

    /** 
     * Returns the nameForSearch.
     * 
     * @return the nameForSearch
     */
    public String getNameForSearch() {
        return nameForSearch;
    }

    /** 
     * Sets the nameForSearch.
     * 
     * @param nameForSearch the nameForSearch
     */
    public void setNameForSearch(String nameForSearch) {
        this.nameForSearch = nameForSearch;
    }
    
    /** 
     * Returns the nameForSearchNative.
     * 
     * @return the nameForSearchNative
     */
    public String getNameForSearchNative() {
        return nameForSearchNative;
    }

    /** 
     * Sets the nameForSearchNative.
     * 
     * @param nameForSearchNative the nameForSearchNative
     */
    public void setNameForSearchNative(String nameForSearchNative) {
        this.nameForSearchNative = nameForSearchNative;
    }

    /** 
     * Returns the marketId.
     * 
     * @return the marketId
     */
    public String getMarketId() {
        return marketId;
    }

    /** 
     * Sets the marketId.
     * 
     * @param marketId the marketId
     */
    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    /** 
     * Returns the brandId.
     * 
     * @return the brandId
     */
    public String getBrandId() {
        return brandId;
    }

    /** 
     * Sets the brandId.
     * 
     * @param brandId the brandId
     */
    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    /** 
     * Returns the ownCount.
     * 
     * @return the ownCount
     */
    public byte[] getOwnCount() {
        return ownCount;
    }

    /** 
     * Sets the ownCount.
     * 
     * @param ownCount the ownCount
     */
    public void setOwnCount(byte[] ownCount) {
        this.ownCount = ownCount;
    }

    /** 
     * Returns the dealCount.
     * 
     * @return the dealCount
     */
    public byte[] getDealCount() {
        return dealCount;
    }

    /** 
     * Sets the dealCount.
     * 
     * @param dealCount the dealCount
     */
    public void setDealCount(byte[] dealCount) {
        this.dealCount = dealCount;
    }

    /** 
     * Returns the groupImportant.
     * 
     * @return the groupImportant
     */
    public byte[] getGroupImportant() {
        return groupImportant;
    }

    /** 
     * Sets the groupImportant.
     * 
     * @param groupImportant the groupImportant
     */
    public void setGroupImportant(byte[] groupImportant) {
        this.groupImportant = groupImportant;
    }

    /** 
     * Returns the groupNew.
     * 
     * @return the groupNew
     */
    public byte[] getGroupNew() {
        return groupNew;
    }

    /** 
     * Sets the groupNew.
     * 
     * @param groupNew the groupNew
     */
    public void setGroupNew(byte[] groupNew) {
        this.groupNew = groupNew;
    }

    /** 
     * Returns the groupDeepPlowing.
     * 
     * @return the groupDeepPlowing
     */
    public byte[] getGroupDeepPlowing() {
        return groupDeepPlowing;
    }

    /** 
     * Sets the groupDeepPlowing.
     * 
     * @param groupDeepPlowing the groupDeepPlowing
     */
    public void setGroupDeepPlowing(byte[] groupDeepPlowing) {
        this.groupDeepPlowing = groupDeepPlowing;
    }

    /** 
     * Returns the groupYCp.
     * 
     * @return the groupYCp
     */
    public byte[] getGroupYCp() {
        return groupYCp;
    }

    /** 
     * Sets the groupYCp.
     * 
     * @param groupYCp the groupYCp
     */
    public void setGroupYCp(byte[] groupYCp) {
        this.groupYCp = groupYCp;
    }

    /** 
     * Returns the groupOtherCp.
     * 
     * @return the groupOtherCp
     */
    public byte[] getGroupOtherCp() {
        return groupOtherCp;
    }

    /** 
     * Sets the groupOtherCp.
     * 
     * @param groupOtherCp the groupOtherCp
     */
    public void setGroupOtherCp(byte[] groupOtherCp) {
        this.groupOtherCp = groupOtherCp;
    }

    /** 
     * Returns the groupOne.
     * 
     * @return the groupOne
     */
    public byte[] getGroupOne() {
        return groupOne;
    }

    /** 
     * Sets the groupOne.
     * 
     * @param groupOne the groupOne
     */
    public void setGroupOne(byte[] groupOne) {
        this.groupOne = groupOne;
    }

    /** 
     * Returns the groupTwo.
     * 
     * @return the groupTwo
     */
    public byte[] getGroupTwo() {
        return groupTwo;
    }

    /** 
     * Sets the groupTwo.
     * 
     * @param groupTwo the groupTwo
     */
    public void setGroupTwo(byte[] groupTwo) {
        this.groupTwo = groupTwo;
    }

    /** 
     * Returns the groupThree.
     * 
     * @return the groupThree
     */
    public byte[] getGroupThree() {
        return groupThree;
    }

    /** 
     * Sets the groupThree.
     * 
     * @param groupThree the groupThree
     */
    public void setGroupThree(byte[] groupThree) {
        this.groupThree = groupThree;
    }

    /** 
     * Returns the groupFour.
     * 
     * @return the groupFour
     */
    public byte[] getGroupFour() {
        return groupFour;
    }

    /** 
     * Sets the groupFour.
     * 
     * @param groupFour the groupFour
     */
    public void setGroupFour(byte[] groupFour) {
        this.groupFour = groupFour;
    }

    /** 
     * Returns the groupFive.
     * 
     * @return the groupFive
     */
    public byte[] getGroupFive() {
        return groupFive;
    }

    /** 
     * Sets the groupFive.
     * 
     * @param groupFive the groupFive
     */
    public void setGroupFive(byte[] groupFive) {
        this.groupFive = groupFive;
    }

    /** 
     * Returns the salesRank.
     * 
     * @return the salesRank
     */
    public String getSalesRank() {
        return salesRank;
    }

    /** 
     * Sets the salesRank.
     * 
     * @param salesRank the salesRank
     */
    public void setSalesRank(String salesRank) {
        this.salesRank = salesRank;
    }

    /** 
     * Returns the demandNumber.
     * 
     * @return the demandNumber
     */
    public Integer getDemandNumber() {
        return demandNumber;
    }

    /** 
     * Sets the demandNumber.
     * 
     * @param demandNumber the demandNumber
     */
    public void setDemandNumber(Integer demandNumber) {
        this.demandNumber = demandNumber;
    }

    /** 
     * Returns the issYh.
     * 
     * @return the issYh
     */
    public Integer getIssYh() {
        return issYh;
    }

    /** 
     * Sets the issYh.
     * 
     * @param issYh the issYh
     */
    public void setIssYh(Integer issYh) {
        this.issYh = issYh;
    }

    /** 
     * Returns the issBs.
     * 
     * @return the issBs
     */
    public Integer getIssBs() {
        return issBs;
    }

    /** 
     * Sets the issBs.
     * 
     * @param issBs the issBs
     */
    public void setIssBs(Integer issBs) {
        this.issBs = issBs;
    }

    /** 
     * Returns the issDl.
     * 
     * @return the issDl
     */
    public Integer getIssDl() {
        return issDl;
    }

    /** 
     * Sets the issDl.
     * 
     * @param issDl the issDl
     */
    public void setIssDl(Integer issDl) {
        this.issDl = issDl;
    }

    /** 
     * Returns the issTy.
     * 
     * @return the issTy
     */
    public Integer getIssTy() {
        return issTy;
    }

    /** 
     * Sets the issTy.
     * 
     * @param issTy the issTy
     */
    public void setIssTy(Integer issTy) {
        this.issTy = issTy;
    }

    /** 
     * Returns the issGy.
     * 
     * @return the issGy
     */
    public Integer getIssGy() {
        return issGy;
    }

    /** 
     * Sets the issGy.
     * 
     * @param issGy the issGy
     */
    public void setIssGy(Integer issGy) {
        this.issGy = issGy;
    }

    /** 
     * Returns the issMi.
     * 
     * @return the issMi
     */
    public Integer getIssMi() {
        return issMi;
    }

    /** 
     * Sets the issMi.
     * 
     * @param issMi the issMi
     */
    public void setIssMi(Integer issMi) {
        this.issMi = issMi;
    }

    /** 
     * Returns the issPb.
     * 
     * @return the issPb
     */
    public Integer getIssPb() {
        return issPb;
    }

    /** 
     * Sets the issPb.
     * 
     * @param issPb the issPb
     */
    public void setIssPb(Integer issPb) {
        this.issPb = issPb;
    }

    /** 
     * Returns the issOther.
     * 
     * @return the issOther
     */
    public Integer getIssOther() {
        return issOther;
    }

    /** 
     * Sets the issOther.
     * 
     * @param issOther the issOther
     */
    public void setIssOther(Integer issOther) {
        this.issOther = issOther;
    }

    /** 
     * Returns the undeal.
     * 
     * @return the undeal
     */
    public byte[] getUndeal() {
        return undeal;
    }

    /** 
     * Sets the undeal.
     * 
     * @param undeal the undeal
     */
    public void setUndeal(byte[] undeal) {
        this.undeal = undeal;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public byte[] getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(byte[] deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}