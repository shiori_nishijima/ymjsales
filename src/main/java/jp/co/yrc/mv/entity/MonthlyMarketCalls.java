package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyMarketCallsListener.class)
@Table(name = "monthly_market_calls")
public class MonthlyMarketCalls {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** SS訪問予定数 */
    @Column(name = "ss_plan_count")
    BigDecimal ssPlanCount;

    /** SS訪問数 */
    @Column(name = "ss_call_count")
    BigDecimal ssCallCount;

    /** SS情報数 */
    @Column(name = "ss_info_count")
    BigDecimal ssInfoCount;

    /** RS訪問予定数 */
    @Column(name = "rs_plan_count")
    BigDecimal rsPlanCount;

    /** RS訪問数 */
    @Column(name = "rs_call_count")
    BigDecimal rsCallCount;

    /** RS情報数 */
    @Column(name = "rs_info_count")
    BigDecimal rsInfoCount;

    /** CD訪問予定数 */
    @Column(name = "cd_plan_count")
    BigDecimal cdPlanCount;

    /** CD訪問数 */
    @Column(name = "cd_call_count")
    BigDecimal cdCallCount;

    /** CD情報数 */
    @Column(name = "cd_info_count")
    BigDecimal cdInfoCount;

    /** PS訪問予定数 */
    @Column(name = "ps_plan_count")
    BigDecimal psPlanCount;

    /** PS訪問数 */
    @Column(name = "ps_call_count")
    BigDecimal psCallCount;

    /** PS情報数 */
    @Column(name = "ps_info_count")
    BigDecimal psInfoCount;

    /** CS訪問予定数 */
    @Column(name = "cs_plan_count")
    BigDecimal csPlanCount;

    /** CS訪問数 */
    @Column(name = "cs_call_count")
    BigDecimal csCallCount;

    /** CS情報数 */
    @Column(name = "cs_info_count")
    BigDecimal csInfoCount;

    /** HC訪問予定数 */
    @Column(name = "hc_plan_count")
    BigDecimal hcPlanCount;

    /** HC訪問数 */
    @Column(name = "hc_call_count")
    BigDecimal hcCallCount;

    /** HC情報数 */
    @Column(name = "hc_info_count")
    BigDecimal hcInfoCount;

    /** リース訪問予定数 */
    @Column(name = "lease_plan_count")
    BigDecimal leasePlanCount;

    /** リース訪問数 */
    @Column(name = "lease_call_count")
    BigDecimal leaseCallCount;

    /** リース情報数 */
    @Column(name = "lease_info_count")
    BigDecimal leaseInfoCount;

    /** 間他訪問予定数 */
    @Column(name = "indirect_plan_count")
    BigDecimal indirectPlanCount;

    /** 間他訪問数 */
    @Column(name = "indirect_other_call_count")
    BigDecimal indirectOtherCallCount;

    /** 間他情報数 */
    @Column(name = "indirect_other_info_count")
    BigDecimal indirectOtherInfoCount;

    /** トラック訪問予定数 */
    @Column(name = "truck_plan_count")
    BigDecimal truckPlanCount;

    /** トラック訪問数 */
    @Column(name = "truck_call_count")
    BigDecimal truckCallCount;

    /** トラック情報数 */
    @Column(name = "truck_info_count")
    BigDecimal truckInfoCount;

    /** SS訪問予定数 */
    @Column(name = "bus_plan_count")
    BigDecimal busPlanCount;

    /** バス訪問数 */
    @Column(name = "bus_call_count")
    BigDecimal busCallCount;

    /** バス情報数 */
    @Column(name = "bus_info_count")
    BigDecimal busInfoCount;

    /** ハイタク訪問予定数 */
    @Column(name = "hire_plan_count")
    BigDecimal hirePlanCount;

    /** ハイタク訪問数 */
    @Column(name = "hire_taxi_call_count")
    BigDecimal hireTaxiCallCount;

    /** ハイタク情報数 */
    @Column(name = "hire_taxi_info_count")
    BigDecimal hireTaxiInfoCount;

    /** 直他訪問予定数 */
    @Column(name = "direct_plan_count")
    BigDecimal directPlanCount;

    /** 直他訪問数 */
    @Column(name = "direct_other_call_count")
    BigDecimal directOtherCallCount;

    /** 直他情報数 */
    @Column(name = "direct_other_info_count")
    BigDecimal directOtherInfoCount;

    /** 小売訪問予定数 */
    @Column(name = "retail_plan_count")
    BigDecimal retailPlanCount;

    /** 小売訪問数 */
    @Column(name = "retail_call_count")
    BigDecimal retailCallCount;

    /** 小売情報数 */
    @Column(name = "retail_info_count")
    BigDecimal retailInfoCount;

    /** 担当外SS訪問予定数 */
    @Column(name = "outside_ss_plan_count")
    BigDecimal outsideSsPlanCount;

    /** 担当外SS訪問数 */
    @Column(name = "outside_ss_call_count")
    BigDecimal outsideSsCallCount;

    /** 担当外SS情報数 */
    @Column(name = "outside_ss_info_count")
    BigDecimal outsideSsInfoCount;

    /** 担当外RS訪問予定数 */
    @Column(name = "outside_rs_plan_count")
    BigDecimal outsideRsPlanCount;

    /** 担当外RS訪問数 */
    @Column(name = "outside_rs_call_count")
    BigDecimal outsideRsCallCount;

    /** 担当外RS情報数 */
    @Column(name = "outside_rs_info_count")
    BigDecimal outsideRsInfoCount;

    /** 担当外CD訪問予定数 */
    @Column(name = "outside_cd_plan_count")
    BigDecimal outsideCdPlanCount;

    /** 担当外CD訪問数 */
    @Column(name = "outside_cd_call_count")
    BigDecimal outsideCdCallCount;

    /** 担当外CD情報数 */
    @Column(name = "outside_cd_info_count")
    BigDecimal outsideCdInfoCount;

    /** 担当外PS訪問予定数 */
    @Column(name = "outside_ps_plan_count")
    BigDecimal outsidePsPlanCount;

    /** 担当外PS訪問数 */
    @Column(name = "outside_ps_call_count")
    BigDecimal outsidePsCallCount;

    /** 担当外PS情報数 */
    @Column(name = "outside_ps_info_count")
    BigDecimal outsidePsInfoCount;

    /** 担当外CS訪問予定数 */
    @Column(name = "outside_cs_plan_count")
    BigDecimal outsideCsPlanCount;

    /** 担当外CS訪問数 */
    @Column(name = "outside_cs_call_count")
    BigDecimal outsideCsCallCount;

    /** 担当外CS情報数 */
    @Column(name = "outside_cs_info_count")
    BigDecimal outsideCsInfoCount;

    /** 担当外HC訪問予定数 */
    @Column(name = "outside_hc_plan_count")
    BigDecimal outsideHcPlanCount;

    /** 担当外HC訪問数 */
    @Column(name = "outside_hc_call_count")
    BigDecimal outsideHcCallCount;

    /** 担当外HC情報数 */
    @Column(name = "outside_hc_info_count")
    BigDecimal outsideHcInfoCount;

    /** 担当外リース訪問予定数 */
    @Column(name = "outside_lease_plan_count")
    BigDecimal outsideLeasePlanCount;

    /** 担当外リース訪問数 */
    @Column(name = "outside_lease_call_count")
    BigDecimal outsideLeaseCallCount;

    /** 担当外リース情報数 */
    @Column(name = "outside_lease_info_count")
    BigDecimal outsideLeaseInfoCount;

    /** 担当外間他訪問予定数 */
    @Column(name = "outside_indirect_plan_count")
    BigDecimal outsideIndirectPlanCount;

    /** 担当外間他訪問数 */
    @Column(name = "outside_indirect_other_call_count")
    BigDecimal outsideIndirectOtherCallCount;

    /** 担当外間他情報数 */
    @Column(name = "outside_indirect_other_info_count")
    BigDecimal outsideIndirectOtherInfoCount;

    /** 担当外トラック訪問予定数 */
    @Column(name = "outside_truck_plan_count")
    BigDecimal outsideTruckPlanCount;

    /** 担当外トラック訪問数 */
    @Column(name = "outside_truck_call_count")
    BigDecimal outsideTruckCallCount;

    /** 担当外トラック情報数 */
    @Column(name = "outside_truck_info_count")
    BigDecimal outsideTruckInfoCount;

    /** 担当外SS訪問予定数 */
    @Column(name = "outside_bus_plan_count")
    BigDecimal outsideBusPlanCount;

    /** 担当外バス訪問数 */
    @Column(name = "outside_bus_call_count")
    BigDecimal outsideBusCallCount;

    /** 担当外バス情報数 */
    @Column(name = "outside_bus_info_count")
    BigDecimal outsideBusInfoCount;

    /** 担当外ハイタク訪問予定数 */
    @Column(name = "outside_hire_plan_count")
    BigDecimal outsideHirePlanCount;

    /** 担当外ハイタク訪問数 */
    @Column(name = "outside_hire_taxi_call_count")
    BigDecimal outsideHireTaxiCallCount;

    /** 担当外ハイタク情報数 */
    @Column(name = "outside_hire_taxi_info_count")
    BigDecimal outsideHireTaxiInfoCount;

    /** 担当外直他訪問予定数 */
    @Column(name = "outside_direct_plan_count")
    BigDecimal outsideDirectPlanCount;

    /** 担当外直他訪問数 */
    @Column(name = "outside_direct_other_call_count")
    BigDecimal outsideDirectOtherCallCount;

    /** 担当外直他情報数 */
    @Column(name = "outside_direct_other_info_count")
    BigDecimal outsideDirectOtherInfoCount;

    /** 担当外小売訪問予定数 */
    @Column(name = "outside_retail_plan_count")
    BigDecimal outsideRetailPlanCount;

    /** 担当外小売訪問数 */
    @Column(name = "outside_retail_call_count")
    BigDecimal outsideRetailCallCount;

    /** 担当外小売情報数 */
    @Column(name = "outside_retail_info_count")
    BigDecimal outsideRetailInfoCount;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the ssPlanCount.
     * 
     * @return the ssPlanCount
     */
    public BigDecimal getSsPlanCount() {
        return ssPlanCount;
    }

    /** 
     * Sets the ssPlanCount.
     * 
     * @param ssPlanCount the ssPlanCount
     */
    public void setSsPlanCount(BigDecimal ssPlanCount) {
        this.ssPlanCount = ssPlanCount;
    }

    /** 
     * Returns the ssCallCount.
     * 
     * @return the ssCallCount
     */
    public BigDecimal getSsCallCount() {
        return ssCallCount;
    }

    /** 
     * Sets the ssCallCount.
     * 
     * @param ssCallCount the ssCallCount
     */
    public void setSsCallCount(BigDecimal ssCallCount) {
        this.ssCallCount = ssCallCount;
    }

    /** 
     * Returns the ssInfoCount.
     * 
     * @return the ssInfoCount
     */
    public BigDecimal getSsInfoCount() {
        return ssInfoCount;
    }

    /** 
     * Sets the ssInfoCount.
     * 
     * @param ssInfoCount the ssInfoCount
     */
    public void setSsInfoCount(BigDecimal ssInfoCount) {
        this.ssInfoCount = ssInfoCount;
    }

    /** 
     * Returns the rsPlanCount.
     * 
     * @return the rsPlanCount
     */
    public BigDecimal getRsPlanCount() {
        return rsPlanCount;
    }

    /** 
     * Sets the rsPlanCount.
     * 
     * @param rsPlanCount the rsPlanCount
     */
    public void setRsPlanCount(BigDecimal rsPlanCount) {
        this.rsPlanCount = rsPlanCount;
    }

    /** 
     * Returns the rsCallCount.
     * 
     * @return the rsCallCount
     */
    public BigDecimal getRsCallCount() {
        return rsCallCount;
    }

    /** 
     * Sets the rsCallCount.
     * 
     * @param rsCallCount the rsCallCount
     */
    public void setRsCallCount(BigDecimal rsCallCount) {
        this.rsCallCount = rsCallCount;
    }

    /** 
     * Returns the rsInfoCount.
     * 
     * @return the rsInfoCount
     */
    public BigDecimal getRsInfoCount() {
        return rsInfoCount;
    }

    /** 
     * Sets the rsInfoCount.
     * 
     * @param rsInfoCount the rsInfoCount
     */
    public void setRsInfoCount(BigDecimal rsInfoCount) {
        this.rsInfoCount = rsInfoCount;
    }

    /** 
     * Returns the cdPlanCount.
     * 
     * @return the cdPlanCount
     */
    public BigDecimal getCdPlanCount() {
        return cdPlanCount;
    }

    /** 
     * Sets the cdPlanCount.
     * 
     * @param cdPlanCount the cdPlanCount
     */
    public void setCdPlanCount(BigDecimal cdPlanCount) {
        this.cdPlanCount = cdPlanCount;
    }

    /** 
     * Returns the cdCallCount.
     * 
     * @return the cdCallCount
     */
    public BigDecimal getCdCallCount() {
        return cdCallCount;
    }

    /** 
     * Sets the cdCallCount.
     * 
     * @param cdCallCount the cdCallCount
     */
    public void setCdCallCount(BigDecimal cdCallCount) {
        this.cdCallCount = cdCallCount;
    }

    /** 
     * Returns the cdInfoCount.
     * 
     * @return the cdInfoCount
     */
    public BigDecimal getCdInfoCount() {
        return cdInfoCount;
    }

    /** 
     * Sets the cdInfoCount.
     * 
     * @param cdInfoCount the cdInfoCount
     */
    public void setCdInfoCount(BigDecimal cdInfoCount) {
        this.cdInfoCount = cdInfoCount;
    }

    /** 
     * Returns the psPlanCount.
     * 
     * @return the psPlanCount
     */
    public BigDecimal getPsPlanCount() {
        return psPlanCount;
    }

    /** 
     * Sets the psPlanCount.
     * 
     * @param psPlanCount the psPlanCount
     */
    public void setPsPlanCount(BigDecimal psPlanCount) {
        this.psPlanCount = psPlanCount;
    }

    /** 
     * Returns the psCallCount.
     * 
     * @return the psCallCount
     */
    public BigDecimal getPsCallCount() {
        return psCallCount;
    }

    /** 
     * Sets the psCallCount.
     * 
     * @param psCallCount the psCallCount
     */
    public void setPsCallCount(BigDecimal psCallCount) {
        this.psCallCount = psCallCount;
    }

    /** 
     * Returns the psInfoCount.
     * 
     * @return the psInfoCount
     */
    public BigDecimal getPsInfoCount() {
        return psInfoCount;
    }

    /** 
     * Sets the psInfoCount.
     * 
     * @param psInfoCount the psInfoCount
     */
    public void setPsInfoCount(BigDecimal psInfoCount) {
        this.psInfoCount = psInfoCount;
    }

    /** 
     * Returns the csPlanCount.
     * 
     * @return the csPlanCount
     */
    public BigDecimal getCsPlanCount() {
        return csPlanCount;
    }

    /** 
     * Sets the csPlanCount.
     * 
     * @param csPlanCount the csPlanCount
     */
    public void setCsPlanCount(BigDecimal csPlanCount) {
        this.csPlanCount = csPlanCount;
    }

    /** 
     * Returns the csCallCount.
     * 
     * @return the csCallCount
     */
    public BigDecimal getCsCallCount() {
        return csCallCount;
    }

    /** 
     * Sets the csCallCount.
     * 
     * @param csCallCount the csCallCount
     */
    public void setCsCallCount(BigDecimal csCallCount) {
        this.csCallCount = csCallCount;
    }

    /** 
     * Returns the csInfoCount.
     * 
     * @return the csInfoCount
     */
    public BigDecimal getCsInfoCount() {
        return csInfoCount;
    }

    /** 
     * Sets the csInfoCount.
     * 
     * @param csInfoCount the csInfoCount
     */
    public void setCsInfoCount(BigDecimal csInfoCount) {
        this.csInfoCount = csInfoCount;
    }

    /** 
     * Returns the hcPlanCount.
     * 
     * @return the hcPlanCount
     */
    public BigDecimal getHcPlanCount() {
        return hcPlanCount;
    }

    /** 
     * Sets the hcPlanCount.
     * 
     * @param hcPlanCount the hcPlanCount
     */
    public void setHcPlanCount(BigDecimal hcPlanCount) {
        this.hcPlanCount = hcPlanCount;
    }

    /** 
     * Returns the hcCallCount.
     * 
     * @return the hcCallCount
     */
    public BigDecimal getHcCallCount() {
        return hcCallCount;
    }

    /** 
     * Sets the hcCallCount.
     * 
     * @param hcCallCount the hcCallCount
     */
    public void setHcCallCount(BigDecimal hcCallCount) {
        this.hcCallCount = hcCallCount;
    }

    /** 
     * Returns the hcInfoCount.
     * 
     * @return the hcInfoCount
     */
    public BigDecimal getHcInfoCount() {
        return hcInfoCount;
    }

    /** 
     * Sets the hcInfoCount.
     * 
     * @param hcInfoCount the hcInfoCount
     */
    public void setHcInfoCount(BigDecimal hcInfoCount) {
        this.hcInfoCount = hcInfoCount;
    }

    /** 
     * Returns the leasePlanCount.
     * 
     * @return the leasePlanCount
     */
    public BigDecimal getLeasePlanCount() {
        return leasePlanCount;
    }

    /** 
     * Sets the leasePlanCount.
     * 
     * @param leasePlanCount the leasePlanCount
     */
    public void setLeasePlanCount(BigDecimal leasePlanCount) {
        this.leasePlanCount = leasePlanCount;
    }

    /** 
     * Returns the leaseCallCount.
     * 
     * @return the leaseCallCount
     */
    public BigDecimal getLeaseCallCount() {
        return leaseCallCount;
    }

    /** 
     * Sets the leaseCallCount.
     * 
     * @param leaseCallCount the leaseCallCount
     */
    public void setLeaseCallCount(BigDecimal leaseCallCount) {
        this.leaseCallCount = leaseCallCount;
    }

    /** 
     * Returns the leaseInfoCount.
     * 
     * @return the leaseInfoCount
     */
    public BigDecimal getLeaseInfoCount() {
        return leaseInfoCount;
    }

    /** 
     * Sets the leaseInfoCount.
     * 
     * @param leaseInfoCount the leaseInfoCount
     */
    public void setLeaseInfoCount(BigDecimal leaseInfoCount) {
        this.leaseInfoCount = leaseInfoCount;
    }

    /** 
     * Returns the indirectPlanCount.
     * 
     * @return the indirectPlanCount
     */
    public BigDecimal getIndirectPlanCount() {
        return indirectPlanCount;
    }

    /** 
     * Sets the indirectPlanCount.
     * 
     * @param indirectPlanCount the indirectPlanCount
     */
    public void setIndirectPlanCount(BigDecimal indirectPlanCount) {
        this.indirectPlanCount = indirectPlanCount;
    }

    /** 
     * Returns the indirectOtherCallCount.
     * 
     * @return the indirectOtherCallCount
     */
    public BigDecimal getIndirectOtherCallCount() {
        return indirectOtherCallCount;
    }

    /** 
     * Sets the indirectOtherCallCount.
     * 
     * @param indirectOtherCallCount the indirectOtherCallCount
     */
    public void setIndirectOtherCallCount(BigDecimal indirectOtherCallCount) {
        this.indirectOtherCallCount = indirectOtherCallCount;
    }

    /** 
     * Returns the indirectOtherInfoCount.
     * 
     * @return the indirectOtherInfoCount
     */
    public BigDecimal getIndirectOtherInfoCount() {
        return indirectOtherInfoCount;
    }

    /** 
     * Sets the indirectOtherInfoCount.
     * 
     * @param indirectOtherInfoCount the indirectOtherInfoCount
     */
    public void setIndirectOtherInfoCount(BigDecimal indirectOtherInfoCount) {
        this.indirectOtherInfoCount = indirectOtherInfoCount;
    }

    /** 
     * Returns the truckPlanCount.
     * 
     * @return the truckPlanCount
     */
    public BigDecimal getTruckPlanCount() {
        return truckPlanCount;
    }

    /** 
     * Sets the truckPlanCount.
     * 
     * @param truckPlanCount the truckPlanCount
     */
    public void setTruckPlanCount(BigDecimal truckPlanCount) {
        this.truckPlanCount = truckPlanCount;
    }

    /** 
     * Returns the truckCallCount.
     * 
     * @return the truckCallCount
     */
    public BigDecimal getTruckCallCount() {
        return truckCallCount;
    }

    /** 
     * Sets the truckCallCount.
     * 
     * @param truckCallCount the truckCallCount
     */
    public void setTruckCallCount(BigDecimal truckCallCount) {
        this.truckCallCount = truckCallCount;
    }

    /** 
     * Returns the truckInfoCount.
     * 
     * @return the truckInfoCount
     */
    public BigDecimal getTruckInfoCount() {
        return truckInfoCount;
    }

    /** 
     * Sets the truckInfoCount.
     * 
     * @param truckInfoCount the truckInfoCount
     */
    public void setTruckInfoCount(BigDecimal truckInfoCount) {
        this.truckInfoCount = truckInfoCount;
    }

    /** 
     * Returns the busPlanCount.
     * 
     * @return the busPlanCount
     */
    public BigDecimal getBusPlanCount() {
        return busPlanCount;
    }

    /** 
     * Sets the busPlanCount.
     * 
     * @param busPlanCount the busPlanCount
     */
    public void setBusPlanCount(BigDecimal busPlanCount) {
        this.busPlanCount = busPlanCount;
    }

    /** 
     * Returns the busCallCount.
     * 
     * @return the busCallCount
     */
    public BigDecimal getBusCallCount() {
        return busCallCount;
    }

    /** 
     * Sets the busCallCount.
     * 
     * @param busCallCount the busCallCount
     */
    public void setBusCallCount(BigDecimal busCallCount) {
        this.busCallCount = busCallCount;
    }

    /** 
     * Returns the busInfoCount.
     * 
     * @return the busInfoCount
     */
    public BigDecimal getBusInfoCount() {
        return busInfoCount;
    }

    /** 
     * Sets the busInfoCount.
     * 
     * @param busInfoCount the busInfoCount
     */
    public void setBusInfoCount(BigDecimal busInfoCount) {
        this.busInfoCount = busInfoCount;
    }

    /** 
     * Returns the hirePlanCount.
     * 
     * @return the hirePlanCount
     */
    public BigDecimal getHirePlanCount() {
        return hirePlanCount;
    }

    /** 
     * Sets the hirePlanCount.
     * 
     * @param hirePlanCount the hirePlanCount
     */
    public void setHirePlanCount(BigDecimal hirePlanCount) {
        this.hirePlanCount = hirePlanCount;
    }

    /** 
     * Returns the hireTaxiCallCount.
     * 
     * @return the hireTaxiCallCount
     */
    public BigDecimal getHireTaxiCallCount() {
        return hireTaxiCallCount;
    }

    /** 
     * Sets the hireTaxiCallCount.
     * 
     * @param hireTaxiCallCount the hireTaxiCallCount
     */
    public void setHireTaxiCallCount(BigDecimal hireTaxiCallCount) {
        this.hireTaxiCallCount = hireTaxiCallCount;
    }

    /** 
     * Returns the hireTaxiInfoCount.
     * 
     * @return the hireTaxiInfoCount
     */
    public BigDecimal getHireTaxiInfoCount() {
        return hireTaxiInfoCount;
    }

    /** 
     * Sets the hireTaxiInfoCount.
     * 
     * @param hireTaxiInfoCount the hireTaxiInfoCount
     */
    public void setHireTaxiInfoCount(BigDecimal hireTaxiInfoCount) {
        this.hireTaxiInfoCount = hireTaxiInfoCount;
    }

    /** 
     * Returns the directPlanCount.
     * 
     * @return the directPlanCount
     */
    public BigDecimal getDirectPlanCount() {
        return directPlanCount;
    }

    /** 
     * Sets the directPlanCount.
     * 
     * @param directPlanCount the directPlanCount
     */
    public void setDirectPlanCount(BigDecimal directPlanCount) {
        this.directPlanCount = directPlanCount;
    }

    /** 
     * Returns the directOtherCallCount.
     * 
     * @return the directOtherCallCount
     */
    public BigDecimal getDirectOtherCallCount() {
        return directOtherCallCount;
    }

    /** 
     * Sets the directOtherCallCount.
     * 
     * @param directOtherCallCount the directOtherCallCount
     */
    public void setDirectOtherCallCount(BigDecimal directOtherCallCount) {
        this.directOtherCallCount = directOtherCallCount;
    }

    /** 
     * Returns the directOtherInfoCount.
     * 
     * @return the directOtherInfoCount
     */
    public BigDecimal getDirectOtherInfoCount() {
        return directOtherInfoCount;
    }

    /** 
     * Sets the directOtherInfoCount.
     * 
     * @param directOtherInfoCount the directOtherInfoCount
     */
    public void setDirectOtherInfoCount(BigDecimal directOtherInfoCount) {
        this.directOtherInfoCount = directOtherInfoCount;
    }

    /** 
     * Returns the retailPlanCount.
     * 
     * @return the retailPlanCount
     */
    public BigDecimal getRetailPlanCount() {
        return retailPlanCount;
    }

    /** 
     * Sets the retailPlanCount.
     * 
     * @param retailPlanCount the retailPlanCount
     */
    public void setRetailPlanCount(BigDecimal retailPlanCount) {
        this.retailPlanCount = retailPlanCount;
    }

    /** 
     * Returns the retailCallCount.
     * 
     * @return the retailCallCount
     */
    public BigDecimal getRetailCallCount() {
        return retailCallCount;
    }

    /** 
     * Sets the retailCallCount.
     * 
     * @param retailCallCount the retailCallCount
     */
    public void setRetailCallCount(BigDecimal retailCallCount) {
        this.retailCallCount = retailCallCount;
    }

    /** 
     * Returns the retailInfoCount.
     * 
     * @return the retailInfoCount
     */
    public BigDecimal getRetailInfoCount() {
        return retailInfoCount;
    }

    /** 
     * Sets the retailInfoCount.
     * 
     * @param retailInfoCount the retailInfoCount
     */
    public void setRetailInfoCount(BigDecimal retailInfoCount) {
        this.retailInfoCount = retailInfoCount;
    }

    /** 
     * Returns the outsideSsPlanCount.
     * 
     * @return the outsideSsPlanCount
     */
    public BigDecimal getOutsideSsPlanCount() {
        return outsideSsPlanCount;
    }

    /** 
     * Sets the outsideSsPlanCount.
     * 
     * @param outsideSsPlanCount the outsideSsPlanCount
     */
    public void setOutsideSsPlanCount(BigDecimal outsideSsPlanCount) {
        this.outsideSsPlanCount = outsideSsPlanCount;
    }

    /** 
     * Returns the outsideSsCallCount.
     * 
     * @return the outsideSsCallCount
     */
    public BigDecimal getOutsideSsCallCount() {
        return outsideSsCallCount;
    }

    /** 
     * Sets the outsideSsCallCount.
     * 
     * @param outsideSsCallCount the outsideSsCallCount
     */
    public void setOutsideSsCallCount(BigDecimal outsideSsCallCount) {
        this.outsideSsCallCount = outsideSsCallCount;
    }

    /** 
     * Returns the outsideSsInfoCount.
     * 
     * @return the outsideSsInfoCount
     */
    public BigDecimal getOutsideSsInfoCount() {
        return outsideSsInfoCount;
    }

    /** 
     * Sets the outsideSsInfoCount.
     * 
     * @param outsideSsInfoCount the outsideSsInfoCount
     */
    public void setOutsideSsInfoCount(BigDecimal outsideSsInfoCount) {
        this.outsideSsInfoCount = outsideSsInfoCount;
    }

    /** 
     * Returns the outsideRsPlanCount.
     * 
     * @return the outsideRsPlanCount
     */
    public BigDecimal getOutsideRsPlanCount() {
        return outsideRsPlanCount;
    }

    /** 
     * Sets the outsideRsPlanCount.
     * 
     * @param outsideRsPlanCount the outsideRsPlanCount
     */
    public void setOutsideRsPlanCount(BigDecimal outsideRsPlanCount) {
        this.outsideRsPlanCount = outsideRsPlanCount;
    }

    /** 
     * Returns the outsideRsCallCount.
     * 
     * @return the outsideRsCallCount
     */
    public BigDecimal getOutsideRsCallCount() {
        return outsideRsCallCount;
    }

    /** 
     * Sets the outsideRsCallCount.
     * 
     * @param outsideRsCallCount the outsideRsCallCount
     */
    public void setOutsideRsCallCount(BigDecimal outsideRsCallCount) {
        this.outsideRsCallCount = outsideRsCallCount;
    }

    /** 
     * Returns the outsideRsInfoCount.
     * 
     * @return the outsideRsInfoCount
     */
    public BigDecimal getOutsideRsInfoCount() {
        return outsideRsInfoCount;
    }

    /** 
     * Sets the outsideRsInfoCount.
     * 
     * @param outsideRsInfoCount the outsideRsInfoCount
     */
    public void setOutsideRsInfoCount(BigDecimal outsideRsInfoCount) {
        this.outsideRsInfoCount = outsideRsInfoCount;
    }

    /** 
     * Returns the outsideCdPlanCount.
     * 
     * @return the outsideCdPlanCount
     */
    public BigDecimal getOutsideCdPlanCount() {
        return outsideCdPlanCount;
    }

    /** 
     * Sets the outsideCdPlanCount.
     * 
     * @param outsideCdPlanCount the outsideCdPlanCount
     */
    public void setOutsideCdPlanCount(BigDecimal outsideCdPlanCount) {
        this.outsideCdPlanCount = outsideCdPlanCount;
    }

    /** 
     * Returns the outsideCdCallCount.
     * 
     * @return the outsideCdCallCount
     */
    public BigDecimal getOutsideCdCallCount() {
        return outsideCdCallCount;
    }

    /** 
     * Sets the outsideCdCallCount.
     * 
     * @param outsideCdCallCount the outsideCdCallCount
     */
    public void setOutsideCdCallCount(BigDecimal outsideCdCallCount) {
        this.outsideCdCallCount = outsideCdCallCount;
    }

    /** 
     * Returns the outsideCdInfoCount.
     * 
     * @return the outsideCdInfoCount
     */
    public BigDecimal getOutsideCdInfoCount() {
        return outsideCdInfoCount;
    }

    /** 
     * Sets the outsideCdInfoCount.
     * 
     * @param outsideCdInfoCount the outsideCdInfoCount
     */
    public void setOutsideCdInfoCount(BigDecimal outsideCdInfoCount) {
        this.outsideCdInfoCount = outsideCdInfoCount;
    }

    /** 
     * Returns the outsidePsPlanCount.
     * 
     * @return the outsidePsPlanCount
     */
    public BigDecimal getOutsidePsPlanCount() {
        return outsidePsPlanCount;
    }

    /** 
     * Sets the outsidePsPlanCount.
     * 
     * @param outsidePsPlanCount the outsidePsPlanCount
     */
    public void setOutsidePsPlanCount(BigDecimal outsidePsPlanCount) {
        this.outsidePsPlanCount = outsidePsPlanCount;
    }

    /** 
     * Returns the outsidePsCallCount.
     * 
     * @return the outsidePsCallCount
     */
    public BigDecimal getOutsidePsCallCount() {
        return outsidePsCallCount;
    }

    /** 
     * Sets the outsidePsCallCount.
     * 
     * @param outsidePsCallCount the outsidePsCallCount
     */
    public void setOutsidePsCallCount(BigDecimal outsidePsCallCount) {
        this.outsidePsCallCount = outsidePsCallCount;
    }

    /** 
     * Returns the outsidePsInfoCount.
     * 
     * @return the outsidePsInfoCount
     */
    public BigDecimal getOutsidePsInfoCount() {
        return outsidePsInfoCount;
    }

    /** 
     * Sets the outsidePsInfoCount.
     * 
     * @param outsidePsInfoCount the outsidePsInfoCount
     */
    public void setOutsidePsInfoCount(BigDecimal outsidePsInfoCount) {
        this.outsidePsInfoCount = outsidePsInfoCount;
    }

    /** 
     * Returns the outsideCsPlanCount.
     * 
     * @return the outsideCsPlanCount
     */
    public BigDecimal getOutsideCsPlanCount() {
        return outsideCsPlanCount;
    }

    /** 
     * Sets the outsideCsPlanCount.
     * 
     * @param outsideCsPlanCount the outsideCsPlanCount
     */
    public void setOutsideCsPlanCount(BigDecimal outsideCsPlanCount) {
        this.outsideCsPlanCount = outsideCsPlanCount;
    }

    /** 
     * Returns the outsideCsCallCount.
     * 
     * @return the outsideCsCallCount
     */
    public BigDecimal getOutsideCsCallCount() {
        return outsideCsCallCount;
    }

    /** 
     * Sets the outsideCsCallCount.
     * 
     * @param outsideCsCallCount the outsideCsCallCount
     */
    public void setOutsideCsCallCount(BigDecimal outsideCsCallCount) {
        this.outsideCsCallCount = outsideCsCallCount;
    }

    /** 
     * Returns the outsideCsInfoCount.
     * 
     * @return the outsideCsInfoCount
     */
    public BigDecimal getOutsideCsInfoCount() {
        return outsideCsInfoCount;
    }

    /** 
     * Sets the outsideCsInfoCount.
     * 
     * @param outsideCsInfoCount the outsideCsInfoCount
     */
    public void setOutsideCsInfoCount(BigDecimal outsideCsInfoCount) {
        this.outsideCsInfoCount = outsideCsInfoCount;
    }

    /** 
     * Returns the outsideHcPlanCount.
     * 
     * @return the outsideHcPlanCount
     */
    public BigDecimal getOutsideHcPlanCount() {
        return outsideHcPlanCount;
    }

    /** 
     * Sets the outsideHcPlanCount.
     * 
     * @param outsideHcPlanCount the outsideHcPlanCount
     */
    public void setOutsideHcPlanCount(BigDecimal outsideHcPlanCount) {
        this.outsideHcPlanCount = outsideHcPlanCount;
    }

    /** 
     * Returns the outsideHcCallCount.
     * 
     * @return the outsideHcCallCount
     */
    public BigDecimal getOutsideHcCallCount() {
        return outsideHcCallCount;
    }

    /** 
     * Sets the outsideHcCallCount.
     * 
     * @param outsideHcCallCount the outsideHcCallCount
     */
    public void setOutsideHcCallCount(BigDecimal outsideHcCallCount) {
        this.outsideHcCallCount = outsideHcCallCount;
    }

    /** 
     * Returns the outsideHcInfoCount.
     * 
     * @return the outsideHcInfoCount
     */
    public BigDecimal getOutsideHcInfoCount() {
        return outsideHcInfoCount;
    }

    /** 
     * Sets the outsideHcInfoCount.
     * 
     * @param outsideHcInfoCount the outsideHcInfoCount
     */
    public void setOutsideHcInfoCount(BigDecimal outsideHcInfoCount) {
        this.outsideHcInfoCount = outsideHcInfoCount;
    }

    /** 
     * Returns the outsideLeasePlanCount.
     * 
     * @return the outsideLeasePlanCount
     */
    public BigDecimal getOutsideLeasePlanCount() {
        return outsideLeasePlanCount;
    }

    /** 
     * Sets the outsideLeasePlanCount.
     * 
     * @param outsideLeasePlanCount the outsideLeasePlanCount
     */
    public void setOutsideLeasePlanCount(BigDecimal outsideLeasePlanCount) {
        this.outsideLeasePlanCount = outsideLeasePlanCount;
    }

    /** 
     * Returns the outsideLeaseCallCount.
     * 
     * @return the outsideLeaseCallCount
     */
    public BigDecimal getOutsideLeaseCallCount() {
        return outsideLeaseCallCount;
    }

    /** 
     * Sets the outsideLeaseCallCount.
     * 
     * @param outsideLeaseCallCount the outsideLeaseCallCount
     */
    public void setOutsideLeaseCallCount(BigDecimal outsideLeaseCallCount) {
        this.outsideLeaseCallCount = outsideLeaseCallCount;
    }

    /** 
     * Returns the outsideLeaseInfoCount.
     * 
     * @return the outsideLeaseInfoCount
     */
    public BigDecimal getOutsideLeaseInfoCount() {
        return outsideLeaseInfoCount;
    }

    /** 
     * Sets the outsideLeaseInfoCount.
     * 
     * @param outsideLeaseInfoCount the outsideLeaseInfoCount
     */
    public void setOutsideLeaseInfoCount(BigDecimal outsideLeaseInfoCount) {
        this.outsideLeaseInfoCount = outsideLeaseInfoCount;
    }

    /** 
     * Returns the outsideIndirectPlanCount.
     * 
     * @return the outsideIndirectPlanCount
     */
    public BigDecimal getOutsideIndirectPlanCount() {
        return outsideIndirectPlanCount;
    }

    /** 
     * Sets the outsideIndirectPlanCount.
     * 
     * @param outsideIndirectPlanCount the outsideIndirectPlanCount
     */
    public void setOutsideIndirectPlanCount(BigDecimal outsideIndirectPlanCount) {
        this.outsideIndirectPlanCount = outsideIndirectPlanCount;
    }

    /** 
     * Returns the outsideIndirectOtherCallCount.
     * 
     * @return the outsideIndirectOtherCallCount
     */
    public BigDecimal getOutsideIndirectOtherCallCount() {
        return outsideIndirectOtherCallCount;
    }

    /** 
     * Sets the outsideIndirectOtherCallCount.
     * 
     * @param outsideIndirectOtherCallCount the outsideIndirectOtherCallCount
     */
    public void setOutsideIndirectOtherCallCount(BigDecimal outsideIndirectOtherCallCount) {
        this.outsideIndirectOtherCallCount = outsideIndirectOtherCallCount;
    }

    /** 
     * Returns the outsideIndirectOtherInfoCount.
     * 
     * @return the outsideIndirectOtherInfoCount
     */
    public BigDecimal getOutsideIndirectOtherInfoCount() {
        return outsideIndirectOtherInfoCount;
    }

    /** 
     * Sets the outsideIndirectOtherInfoCount.
     * 
     * @param outsideIndirectOtherInfoCount the outsideIndirectOtherInfoCount
     */
    public void setOutsideIndirectOtherInfoCount(BigDecimal outsideIndirectOtherInfoCount) {
        this.outsideIndirectOtherInfoCount = outsideIndirectOtherInfoCount;
    }

    /** 
     * Returns the outsideTruckPlanCount.
     * 
     * @return the outsideTruckPlanCount
     */
    public BigDecimal getOutsideTruckPlanCount() {
        return outsideTruckPlanCount;
    }

    /** 
     * Sets the outsideTruckPlanCount.
     * 
     * @param outsideTruckPlanCount the outsideTruckPlanCount
     */
    public void setOutsideTruckPlanCount(BigDecimal outsideTruckPlanCount) {
        this.outsideTruckPlanCount = outsideTruckPlanCount;
    }

    /** 
     * Returns the outsideTruckCallCount.
     * 
     * @return the outsideTruckCallCount
     */
    public BigDecimal getOutsideTruckCallCount() {
        return outsideTruckCallCount;
    }

    /** 
     * Sets the outsideTruckCallCount.
     * 
     * @param outsideTruckCallCount the outsideTruckCallCount
     */
    public void setOutsideTruckCallCount(BigDecimal outsideTruckCallCount) {
        this.outsideTruckCallCount = outsideTruckCallCount;
    }

    /** 
     * Returns the outsideTruckInfoCount.
     * 
     * @return the outsideTruckInfoCount
     */
    public BigDecimal getOutsideTruckInfoCount() {
        return outsideTruckInfoCount;
    }

    /** 
     * Sets the outsideTruckInfoCount.
     * 
     * @param outsideTruckInfoCount the outsideTruckInfoCount
     */
    public void setOutsideTruckInfoCount(BigDecimal outsideTruckInfoCount) {
        this.outsideTruckInfoCount = outsideTruckInfoCount;
    }

    /** 
     * Returns the outsideBusPlanCount.
     * 
     * @return the outsideBusPlanCount
     */
    public BigDecimal getOutsideBusPlanCount() {
        return outsideBusPlanCount;
    }

    /** 
     * Sets the outsideBusPlanCount.
     * 
     * @param outsideBusPlanCount the outsideBusPlanCount
     */
    public void setOutsideBusPlanCount(BigDecimal outsideBusPlanCount) {
        this.outsideBusPlanCount = outsideBusPlanCount;
    }

    /** 
     * Returns the outsideBusCallCount.
     * 
     * @return the outsideBusCallCount
     */
    public BigDecimal getOutsideBusCallCount() {
        return outsideBusCallCount;
    }

    /** 
     * Sets the outsideBusCallCount.
     * 
     * @param outsideBusCallCount the outsideBusCallCount
     */
    public void setOutsideBusCallCount(BigDecimal outsideBusCallCount) {
        this.outsideBusCallCount = outsideBusCallCount;
    }

    /** 
     * Returns the outsideBusInfoCount.
     * 
     * @return the outsideBusInfoCount
     */
    public BigDecimal getOutsideBusInfoCount() {
        return outsideBusInfoCount;
    }

    /** 
     * Sets the outsideBusInfoCount.
     * 
     * @param outsideBusInfoCount the outsideBusInfoCount
     */
    public void setOutsideBusInfoCount(BigDecimal outsideBusInfoCount) {
        this.outsideBusInfoCount = outsideBusInfoCount;
    }

    /** 
     * Returns the outsideHirePlanCount.
     * 
     * @return the outsideHirePlanCount
     */
    public BigDecimal getOutsideHirePlanCount() {
        return outsideHirePlanCount;
    }

    /** 
     * Sets the outsideHirePlanCount.
     * 
     * @param outsideHirePlanCount the outsideHirePlanCount
     */
    public void setOutsideHirePlanCount(BigDecimal outsideHirePlanCount) {
        this.outsideHirePlanCount = outsideHirePlanCount;
    }

    /** 
     * Returns the outsideHireTaxiCallCount.
     * 
     * @return the outsideHireTaxiCallCount
     */
    public BigDecimal getOutsideHireTaxiCallCount() {
        return outsideHireTaxiCallCount;
    }

    /** 
     * Sets the outsideHireTaxiCallCount.
     * 
     * @param outsideHireTaxiCallCount the outsideHireTaxiCallCount
     */
    public void setOutsideHireTaxiCallCount(BigDecimal outsideHireTaxiCallCount) {
        this.outsideHireTaxiCallCount = outsideHireTaxiCallCount;
    }

    /** 
     * Returns the outsideHireTaxiInfoCount.
     * 
     * @return the outsideHireTaxiInfoCount
     */
    public BigDecimal getOutsideHireTaxiInfoCount() {
        return outsideHireTaxiInfoCount;
    }

    /** 
     * Sets the outsideHireTaxiInfoCount.
     * 
     * @param outsideHireTaxiInfoCount the outsideHireTaxiInfoCount
     */
    public void setOutsideHireTaxiInfoCount(BigDecimal outsideHireTaxiInfoCount) {
        this.outsideHireTaxiInfoCount = outsideHireTaxiInfoCount;
    }

    /** 
     * Returns the outsideDirectPlanCount.
     * 
     * @return the outsideDirectPlanCount
     */
    public BigDecimal getOutsideDirectPlanCount() {
        return outsideDirectPlanCount;
    }

    /** 
     * Sets the outsideDirectPlanCount.
     * 
     * @param outsideDirectPlanCount the outsideDirectPlanCount
     */
    public void setOutsideDirectPlanCount(BigDecimal outsideDirectPlanCount) {
        this.outsideDirectPlanCount = outsideDirectPlanCount;
    }

    /** 
     * Returns the outsideDirectOtherCallCount.
     * 
     * @return the outsideDirectOtherCallCount
     */
    public BigDecimal getOutsideDirectOtherCallCount() {
        return outsideDirectOtherCallCount;
    }

    /** 
     * Sets the outsideDirectOtherCallCount.
     * 
     * @param outsideDirectOtherCallCount the outsideDirectOtherCallCount
     */
    public void setOutsideDirectOtherCallCount(BigDecimal outsideDirectOtherCallCount) {
        this.outsideDirectOtherCallCount = outsideDirectOtherCallCount;
    }

    /** 
     * Returns the outsideDirectOtherInfoCount.
     * 
     * @return the outsideDirectOtherInfoCount
     */
    public BigDecimal getOutsideDirectOtherInfoCount() {
        return outsideDirectOtherInfoCount;
    }

    /** 
     * Sets the outsideDirectOtherInfoCount.
     * 
     * @param outsideDirectOtherInfoCount the outsideDirectOtherInfoCount
     */
    public void setOutsideDirectOtherInfoCount(BigDecimal outsideDirectOtherInfoCount) {
        this.outsideDirectOtherInfoCount = outsideDirectOtherInfoCount;
    }

    /** 
     * Returns the outsideRetailPlanCount.
     * 
     * @return the outsideRetailPlanCount
     */
    public BigDecimal getOutsideRetailPlanCount() {
        return outsideRetailPlanCount;
    }

    /** 
     * Sets the outsideRetailPlanCount.
     * 
     * @param outsideRetailPlanCount the outsideRetailPlanCount
     */
    public void setOutsideRetailPlanCount(BigDecimal outsideRetailPlanCount) {
        this.outsideRetailPlanCount = outsideRetailPlanCount;
    }

    /** 
     * Returns the outsideRetailCallCount.
     * 
     * @return the outsideRetailCallCount
     */
    public BigDecimal getOutsideRetailCallCount() {
        return outsideRetailCallCount;
    }

    /** 
     * Sets the outsideRetailCallCount.
     * 
     * @param outsideRetailCallCount the outsideRetailCallCount
     */
    public void setOutsideRetailCallCount(BigDecimal outsideRetailCallCount) {
        this.outsideRetailCallCount = outsideRetailCallCount;
    }

    /** 
     * Returns the outsideRetailInfoCount.
     * 
     * @return the outsideRetailInfoCount
     */
    public BigDecimal getOutsideRetailInfoCount() {
        return outsideRetailInfoCount;
    }

    /** 
     * Sets the outsideRetailInfoCount.
     * 
     * @param outsideRetailInfoCount the outsideRetailInfoCount
     */
    public void setOutsideRetailInfoCount(BigDecimal outsideRetailInfoCount) {
        this.outsideRetailInfoCount = outsideRetailInfoCount;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}