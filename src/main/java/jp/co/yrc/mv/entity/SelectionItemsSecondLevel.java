package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = SelectionItemsSecondLevelListener.class)
@Table(name = "selection_items_second_level")
public class SelectionItemsSecondLevel {

    /**  */
    @Id
    @Column(name = "key_entity_name")
    String keyEntityName;

    /**  */
    @Id
    @Column(name = "key_property_name")
    String keyPropertyName;

    /**  */
    @Id
    @Column(name = "key_value")
    String keyValue;

    /**  */
    @Id
    @Column(name = "property_name")
    String propertyName;

    /**  */
    @Id
    @Column(name = "value")
    String value;

    /**  */
    @Column(name = "label")
    String label;

    /**  */
    @Column(name = "sort_order")
    Integer sortOrder;

    /**  */
    @Column(name = "deleted")
    Short deleted;

    /**  */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /**  */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /**  */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /**  */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the keyEntityName.
     * 
     * @return the keyEntityName
     */
    public String getKeyEntityName() {
        return keyEntityName;
    }

    /** 
     * Sets the keyEntityName.
     * 
     * @param keyEntityName the keyEntityName
     */
    public void setKeyEntityName(String keyEntityName) {
        this.keyEntityName = keyEntityName;
    }

    /** 
     * Returns the keyPropertyName.
     * 
     * @return the keyPropertyName
     */
    public String getKeyPropertyName() {
        return keyPropertyName;
    }

    /** 
     * Sets the keyPropertyName.
     * 
     * @param keyPropertyName the keyPropertyName
     */
    public void setKeyPropertyName(String keyPropertyName) {
        this.keyPropertyName = keyPropertyName;
    }

    /** 
     * Returns the keyValue.
     * 
     * @return the keyValue
     */
    public String getKeyValue() {
        return keyValue;
    }

    /** 
     * Sets the keyValue.
     * 
     * @param keyValue the keyValue
     */
    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    /** 
     * Returns the propertyName.
     * 
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /** 
     * Sets the propertyName.
     * 
     * @param propertyName the propertyName
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /** 
     * Returns the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /** 
     * Sets the value.
     * 
     * @param value the value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /** 
     * Returns the label.
     * 
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /** 
     * Sets the label.
     * 
     * @param label the label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /** 
     * Returns the sortOrder.
     * 
     * @return the sortOrder
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /** 
     * Sets the sortOrder.
     * 
     * @param sortOrder the sortOrder
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public Short getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}