package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OpaddPartnerListener implements EntityListener<OpaddPartner> {

    @Override
    public void preInsert(OpaddPartner entity, PreInsertContext<OpaddPartner> context) {
    }

    @Override
    public void preUpdate(OpaddPartner entity, PreUpdateContext<OpaddPartner> context) {
    }

    @Override
    public void preDelete(OpaddPartner entity, PreDeleteContext<OpaddPartner> context) {
    }

    @Override
    public void postInsert(OpaddPartner entity, PostInsertContext<OpaddPartner> context) {
    }

    @Override
    public void postUpdate(OpaddPartner entity, PostUpdateContext<OpaddPartner> context) {
    }

    @Override
    public void postDelete(OpaddPartner entity, PostDeleteContext<OpaddPartner> context) {
    }
}