package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OppSeOpportunitiesCListener.class)
@Table(name = "opp_se_opportunities_c")
public class OppSeOpportunitiesC {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 案件CD */
    @Column(name = "opp_se_opp7a17unities_ida")
    String oppSeOpp7a17unitiesIda;

    /** SE案件CD */
    @Column(name = "opp_se_opp1446sopp_se_idb")
    String oppSeOpp1446soppSeIdb;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the oppSeOpp7a17unitiesIda.
     * 
     * @return the oppSeOpp7a17unitiesIda
     */
    public String getOppSeOpp7a17unitiesIda() {
        return oppSeOpp7a17unitiesIda;
    }

    /** 
     * Sets the oppSeOpp7a17unitiesIda.
     * 
     * @param oppSeOpp7a17unitiesIda the oppSeOpp7a17unitiesIda
     */
    public void setOppSeOpp7a17unitiesIda(String oppSeOpp7a17unitiesIda) {
        this.oppSeOpp7a17unitiesIda = oppSeOpp7a17unitiesIda;
    }

    /** 
     * Returns the oppSeOpp1446soppSeIdb.
     * 
     * @return the oppSeOpp1446soppSeIdb
     */
    public String getOppSeOpp1446soppSeIdb() {
        return oppSeOpp1446soppSeIdb;
    }

    /** 
     * Sets the oppSeOpp1446soppSeIdb.
     * 
     * @param oppSeOpp1446soppSeIdb the oppSeOpp1446soppSeIdb
     */
    public void setOppSeOpp1446soppSeIdb(String oppSeOpp1446soppSeIdb) {
        this.oppSeOpp1446soppSeIdb = oppSeOpp1446soppSeIdb;
    }
}