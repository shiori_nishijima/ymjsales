package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyGroupCallsListener implements EntityListener<MonthlyGroupCalls> {

    @Override
    public void preInsert(MonthlyGroupCalls entity, PreInsertContext<MonthlyGroupCalls> context) {
    }

    @Override
    public void preUpdate(MonthlyGroupCalls entity, PreUpdateContext<MonthlyGroupCalls> context) {
    }

    @Override
    public void preDelete(MonthlyGroupCalls entity, PreDeleteContext<MonthlyGroupCalls> context) {
    }

    @Override
    public void postInsert(MonthlyGroupCalls entity, PostInsertContext<MonthlyGroupCalls> context) {
    }

    @Override
    public void postUpdate(MonthlyGroupCalls entity, PostUpdateContext<MonthlyGroupCalls> context) {
    }

    @Override
    public void postDelete(MonthlyGroupCalls entity, PostDeleteContext<MonthlyGroupCalls> context) {
    }
}