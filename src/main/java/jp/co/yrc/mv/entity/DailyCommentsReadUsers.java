package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = DailyCommentsReadUsersListener.class)
@Table(name = "daily_comments_read_users")
public class DailyCommentsReadUsers {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 日報コメントID */
    @Column(name = "daily_comment_id")
    String dailyCommentId;

    /** ユーザID */
    @Column(name = "user_id")
    String userId;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the dailyCommentId.
     * 
     * @return the dailyCommentId
     */
    public String getDailyCommentId() {
        return dailyCommentId;
    }

    /** 
     * Sets the dailyCommentId.
     * 
     * @param dailyCommentId the dailyCommentId
     */
    public void setDailyCommentId(String dailyCommentId) {
        this.dailyCommentId = dailyCommentId;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}