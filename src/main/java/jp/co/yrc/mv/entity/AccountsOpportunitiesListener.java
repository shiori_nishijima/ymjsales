package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class AccountsOpportunitiesListener implements EntityListener<AccountsOpportunities> {

    @Override
    public void preInsert(AccountsOpportunities entity, PreInsertContext<AccountsOpportunities> context) {
    }

    @Override
    public void preUpdate(AccountsOpportunities entity, PreUpdateContext<AccountsOpportunities> context) {
    }

    @Override
    public void preDelete(AccountsOpportunities entity, PreDeleteContext<AccountsOpportunities> context) {
    }

    @Override
    public void postInsert(AccountsOpportunities entity, PostInsertContext<AccountsOpportunities> context) {
    }

    @Override
    public void postUpdate(AccountsOpportunities entity, PostUpdateContext<AccountsOpportunities> context) {
    }

    @Override
    public void postDelete(AccountsOpportunities entity, PostDeleteContext<AccountsOpportunities> context) {
    }
}