package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OppSalessupportunitiesCListener.class)
@Table(name = "opp_salessupportunities_c")
public class OppSalessupportunitiesC {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 案件CD */
    @Column(name = "opp_saless3af5unities_ida")
    String oppSaless3af5unitiesIda;

    /** 営業サポートCD */
    @Column(name = "opp_saless78cealessup_idb")
    String oppSaless78cealessupIdb;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the oppSaless3af5unitiesIda.
     * 
     * @return the oppSaless3af5unitiesIda
     */
    public String getOppSaless3af5unitiesIda() {
        return oppSaless3af5unitiesIda;
    }

    /** 
     * Sets the oppSaless3af5unitiesIda.
     * 
     * @param oppSaless3af5unitiesIda the oppSaless3af5unitiesIda
     */
    public void setOppSaless3af5unitiesIda(String oppSaless3af5unitiesIda) {
        this.oppSaless3af5unitiesIda = oppSaless3af5unitiesIda;
    }

    /** 
     * Returns the oppSaless78cealessupIdb.
     * 
     * @return the oppSaless78cealessupIdb
     */
    public String getOppSaless78cealessupIdb() {
        return oppSaless78cealessupIdb;
    }

    /** 
     * Sets the oppSaless78cealessupIdb.
     * 
     * @param oppSaless78cealessupIdb the oppSaless78cealessupIdb
     */
    public void setOppSaless78cealessupIdb(String oppSaless78cealessupIdb) {
        this.oppSaless78cealessupIdb = oppSaless78cealessupIdb;
    }
}