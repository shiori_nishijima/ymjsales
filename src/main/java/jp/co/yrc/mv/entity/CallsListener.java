package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsListener implements EntityListener<Calls> {

    @Override
    public void preInsert(Calls entity, PreInsertContext<Calls> context) {
    }

    @Override
    public void preUpdate(Calls entity, PreUpdateContext<Calls> context) {
    }

    @Override
    public void preDelete(Calls entity, PreDeleteContext<Calls> context) {
    }

    @Override
    public void postInsert(Calls entity, PostInsertContext<Calls> context) {
    }

    @Override
    public void postUpdate(Calls entity, PostUpdateContext<Calls> context) {
    }

    @Override
    public void postDelete(Calls entity, PostDeleteContext<Calls> context) {
    }
}