package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = TechServiceCallsFilesListener.class)
@Table(name = "tech_service_calls_files")
public class TechServiceCallsFiles {

    /** 訪問ID */
    @Id
    @Column(name = "id")
    String id;

    /** ファイル1 */
    @Column(name = "file1")
    byte[] file1;

    /** ファイル名称1 */
    @Column(name = "file1_name")
    String file1Name;

    /** ファイル2 */
    @Column(name = "file2")
    byte[] file2;

    /** ファイル名称2 */
    @Column(name = "file2_name")
    String file2Name;

    /** ファイル3 */
    @Column(name = "file3")
    byte[] file3;

    /** ファイル名称3 */
    @Column(name = "file3_name")
    String file3Name;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the file1.
     * 
     * @return the file1
     */
    public byte[] getFile1() {
        return file1;
    }

    /** 
     * Sets the file1.
     * 
     * @param file1 the file1
     */
    public void setFile1(byte[] file1) {
        this.file1 = file1;
    }

    /** 
     * Returns the file1Name.
     * 
     * @return the file1Name
     */
    public String getFile1Name() {
        return file1Name;
    }

    /** 
     * Sets the file1Name.
     * 
     * @param file1Name the file1Name
     */
    public void setFile1Name(String file1Name) {
        this.file1Name = file1Name;
    }

    /** 
     * Returns the file2.
     * 
     * @return the file2
     */
    public byte[] getFile2() {
        return file2;
    }

    /** 
     * Sets the file2.
     * 
     * @param file2 the file2
     */
    public void setFile2(byte[] file2) {
        this.file2 = file2;
    }

    /** 
     * Returns the file2Name.
     * 
     * @return the file2Name
     */
    public String getFile2Name() {
        return file2Name;
    }

    /** 
     * Sets the file2Name.
     * 
     * @param file2Name the file2Name
     */
    public void setFile2Name(String file2Name) {
        this.file2Name = file2Name;
    }

    /** 
     * Returns the file3.
     * 
     * @return the file3
     */
    public byte[] getFile3() {
        return file3;
    }

    /** 
     * Sets the file3.
     * 
     * @param file3 the file3
     */
    public void setFile3(byte[] file3) {
        this.file3 = file3;
    }

    /** 
     * Returns the file3Name.
     * 
     * @return the file3Name
     */
    public String getFile3Name() {
        return file3Name;
    }

    /** 
     * Sets the file3Name.
     * 
     * @param file3Name the file3Name
     */
    public void setFile3Name(String file3Name) {
        this.file3Name = file3Name;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}