package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MarketInfosListener implements EntityListener<MarketInfos> {

    @Override
    public void preInsert(MarketInfos entity, PreInsertContext<MarketInfos> context) {
    }

    @Override
    public void preUpdate(MarketInfos entity, PreUpdateContext<MarketInfos> context) {
    }

    @Override
    public void preDelete(MarketInfos entity, PreDeleteContext<MarketInfos> context) {
    }

    @Override
    public void postInsert(MarketInfos entity, PostInsertContext<MarketInfos> context) {
    }

    @Override
    public void postUpdate(MarketInfos entity, PostUpdateContext<MarketInfos> context) {
    }

    @Override
    public void postDelete(MarketInfos entity, PostDeleteContext<MarketInfos> context) {
    }
}