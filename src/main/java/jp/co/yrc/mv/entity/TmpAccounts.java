package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = TmpAccountsListener.class)
@Table(name = "tmp_accounts")
public class TmpAccounts {

    /** 取引先コード */
    @Id
    @Column(name = "id")
    String id;

    /** 名称 */
    @Column(name = "name")
    String name;

    /** 名称カナ */
    @Column(name = "name_kana")
    String nameKana;

    /** 郵便番号 */
    @Column(name = "billing_address_postalcode")
    String billingAddressPostalcode;

    /** 都道府県（住所1） */
    @Column(name = "billing_address_state")
    String billingAddressState;

    /** 市区町村（住所2） */
    @Column(name = "billing_address_city")
    String billingAddressCity;

    /** 番地（住所3） */
    @Column(name = "billing_address_street")
    String billingAddressStreet;

    /** 親コード */
    @Column(name = "parent_id")
    String parentId;

    /** 担当者 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 販社コード */
    @Column(name = "sales_company_code")
    String salesCompanyCode;

    /** 部門コード */
    @Column(name = "department_code")
    String departmentCode;

    /** 営業所コード */
    @Column(name = "sales_office_code")
    String salesOfficeCode;

    /** 検索用名称 */
    @Column(name = "name_for_search")
    String nameForSearch;

    /** 販路 */
    @Column(name = "market_id")
    String marketId;

    /** 銘柄 */
    @Column(name = "brand_id")
    String brandId;

    /** 売上ランク */
    @Column(name = "sales_rank")
    String salesRank;

    /** 未取引先フラグ */
    @Column(name = "undeal")
    boolean undeal;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the nameKana.
     * 
     * @return the nameKana
     */
    public String getNameKana() {
        return nameKana;
    }

    /** 
     * Sets the nameKana.
     * 
     * @param nameKana the nameKana
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /** 
     * Returns the billingAddressPostalcode.
     * 
     * @return the billingAddressPostalcode
     */
    public String getBillingAddressPostalcode() {
        return billingAddressPostalcode;
    }

    /** 
     * Sets the billingAddressPostalcode.
     * 
     * @param billingAddressPostalcode the billingAddressPostalcode
     */
    public void setBillingAddressPostalcode(String billingAddressPostalcode) {
        this.billingAddressPostalcode = billingAddressPostalcode;
    }

    /** 
     * Returns the billingAddressState.
     * 
     * @return the billingAddressState
     */
    public String getBillingAddressState() {
        return billingAddressState;
    }

    /** 
     * Sets the billingAddressState.
     * 
     * @param billingAddressState the billingAddressState
     */
    public void setBillingAddressState(String billingAddressState) {
        this.billingAddressState = billingAddressState;
    }

    /** 
     * Returns the billingAddressCity.
     * 
     * @return the billingAddressCity
     */
    public String getBillingAddressCity() {
        return billingAddressCity;
    }

    /** 
     * Sets the billingAddressCity.
     * 
     * @param billingAddressCity the billingAddressCity
     */
    public void setBillingAddressCity(String billingAddressCity) {
        this.billingAddressCity = billingAddressCity;
    }

    /** 
     * Returns the billingAddressStreet.
     * 
     * @return the billingAddressStreet
     */
    public String getBillingAddressStreet() {
        return billingAddressStreet;
    }

    /** 
     * Sets the billingAddressStreet.
     * 
     * @param billingAddressStreet the billingAddressStreet
     */
    public void setBillingAddressStreet(String billingAddressStreet) {
        this.billingAddressStreet = billingAddressStreet;
    }

    /** 
     * Returns the parentId.
     * 
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /** 
     * Sets the parentId.
     * 
     * @param parentId the parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the salesCompanyCode.
     * 
     * @return the salesCompanyCode
     */
    public String getSalesCompanyCode() {
        return salesCompanyCode;
    }

    /** 
     * Sets the salesCompanyCode.
     * 
     * @param salesCompanyCode the salesCompanyCode
     */
    public void setSalesCompanyCode(String salesCompanyCode) {
        this.salesCompanyCode = salesCompanyCode;
    }

    /** 
     * Returns the departmentCode.
     * 
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /** 
     * Sets the departmentCode.
     * 
     * @param departmentCode the departmentCode
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /** 
     * Returns the salesOfficeCode.
     * 
     * @return the salesOfficeCode
     */
    public String getSalesOfficeCode() {
        return salesOfficeCode;
    }

    /** 
     * Sets the salesOfficeCode.
     * 
     * @param salesOfficeCode the salesOfficeCode
     */
    public void setSalesOfficeCode(String salesOfficeCode) {
        this.salesOfficeCode = salesOfficeCode;
    }

    /** 
     * Returns the nameForSearch.
     * 
     * @return the nameForSearch
     */
    public String getNameForSearch() {
        return nameForSearch;
    }

    /** 
     * Sets the nameForSearch.
     * 
     * @param nameForSearch the nameForSearch
     */
    public void setNameForSearch(String nameForSearch) {
        this.nameForSearch = nameForSearch;
    }

    /** 
     * Returns the marketId.
     * 
     * @return the marketId
     */
    public String getMarketId() {
        return marketId;
    }

    /** 
     * Sets the marketId.
     * 
     * @param marketId the marketId
     */
    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    /** 
     * Returns the brandId.
     * 
     * @return the brandId
     */
    public String getBrandId() {
        return brandId;
    }

    /** 
     * Sets the brandId.
     * 
     * @param brandId the brandId
     */
    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    /** 
     * Returns the salesRank.
     * 
     * @return the salesRank
     */
    public String getSalesRank() {
        return salesRank;
    }

    /** 
     * Sets the salesRank.
     * 
     * @param salesRank the salesRank
     */
    public void setSalesRank(String salesRank) {
        this.salesRank = salesRank;
    }

    /** 
     * Returns the undeal.
     * 
     * @return the undeal
     */
    public boolean isUndeal() {
        return undeal;
    }

    /** 
     * Sets the undeal.
     * 
     * @param undeal the undeal
     */
    public void setUndeal(boolean undeal) {
        this.undeal = undeal;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}