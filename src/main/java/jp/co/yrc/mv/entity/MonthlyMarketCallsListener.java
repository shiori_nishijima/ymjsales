package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyMarketCallsListener implements EntityListener<MonthlyMarketCalls> {

    @Override
    public void preInsert(MonthlyMarketCalls entity, PreInsertContext<MonthlyMarketCalls> context) {
    }

    @Override
    public void preUpdate(MonthlyMarketCalls entity, PreUpdateContext<MonthlyMarketCalls> context) {
    }

    @Override
    public void preDelete(MonthlyMarketCalls entity, PreDeleteContext<MonthlyMarketCalls> context) {
    }

    @Override
    public void postInsert(MonthlyMarketCalls entity, PostInsertContext<MonthlyMarketCalls> context) {
    }

    @Override
    public void postUpdate(MonthlyMarketCalls entity, PostUpdateContext<MonthlyMarketCalls> context) {
    }

    @Override
    public void postDelete(MonthlyMarketCalls entity, PostDeleteContext<MonthlyMarketCalls> context) {
    }
}