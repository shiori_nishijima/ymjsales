package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OppSeListener implements EntityListener<OppSe> {

    @Override
    public void preInsert(OppSe entity, PreInsertContext<OppSe> context) {
    }

    @Override
    public void preUpdate(OppSe entity, PreUpdateContext<OppSe> context) {
    }

    @Override
    public void preDelete(OppSe entity, PreDeleteContext<OppSe> context) {
    }

    @Override
    public void postInsert(OppSe entity, PostInsertContext<OppSe> context) {
    }

    @Override
    public void postUpdate(OppSe entity, PostUpdateContext<OppSe> context) {
    }

    @Override
    public void postDelete(OppSe entity, PostDeleteContext<OppSe> context) {
    }
}