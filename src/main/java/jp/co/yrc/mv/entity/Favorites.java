package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = FavoritesListener.class)
@Table(name = "favorites")
public class Favorites {

    /** お気に入りID */
    @Id
    @Column(name = "id")
    String id;

    /** 登録ユーザID */
    @Column(name = "user_id")
    String userId;

    /** 関連先ID */
    @Column(name = "parent_id")
    String parentId;

    /** 関連先Type */
    @Column(name = "parent_type")
    String parentType;

    /**  */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the parentId.
     * 
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /** 
     * Sets the parentId.
     * 
     * @param parentId the parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /** 
     * Returns the parentType.
     * 
     * @return the parentType
     */
    public String getParentType() {
        return parentType;
    }

    /** 
     * Sets the parentType.
     * 
     * @param parentType the parentType
     */
    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }
}