package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = PlansListener.class)
@Table(name = "plans")
public class Plans {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** 販社コード */
    @Id
    @Column(name = "sales_company_code")
    String salesCompanyCode;

    /** 部門コード */
    @Id
    @Column(name = "department_code")
    String departmentCode;

    /** 営業所コード */
    @Id
    @Column(name = "sales_office_code")
    String salesOfficeCode;

    /** 担当者 */
    @Id
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 販管計画年 */
    @Column(name = "sga_plan_year")
    Integer sgaPlanYear;

    /** 販管計画期区分 */
    @Id
    @Column(name = "sga_plan_term_div")
    String sgaPlanTermDiv;

    /** 販管計画年月 */
    @Column(name = "sga_plan_year_month")
    Integer sgaPlanYearMonth;

    /** 販社品種分類ＹＲＣ区分 */
    @Id
    @Column(name = "product_class_yrc_div")
    String productClassYrcDiv;

    /** 大分類1(動タ区分) */
    @Id
    @Column(name = "large_class1")
    String largeClass1;

    /** 大分類2(販社共通スノー区分) */
    @Id
    @Column(name = "large_class2")
    String largeClass2;

    /** 中分類(販社品種分類大分類コード) */
    @Id
    @Column(name = "middle_class")
    String middleClass;

    /** 小分類(販社品種分類中分類コード) */
    @Id
    @Column(name = "small_class")
    String smallClass;

    /** 販管計画品種コード */
    @Id
    @Column(name = "product_kind_cd")
    String productKindCd;

    /** 販管計画売上本数 */
    @Column(name = "number")
    BigDecimal number;

    /** 販管計画売上金額 */
    @Column(name = "sales")
    BigDecimal sales;

    /** 販管計画粗利金額 */
    @Column(name = "margin")
    BigDecimal margin;

    /** 販管計画本社粗利金額 */
    @Column(name = "hq_margin")
    BigDecimal hqMargin;

    /** 販社計画基準金額 */
    @Column(name = "base_amount")
    BigDecimal baseAmount;

    /** 更新ユーザID */
    @Column(name = "updated_user_id")
    String updatedUserId;

    /** 処理更新フラグ */
    @Column(name = "updated")
    boolean updated;

    /** 処理日時 */
    @Column(name = "date_updated")
    Timestamp dateUpdated;

    /** 販社品種分類他社区分 */
    @Column(name = "kind_other_cp_div")
    String kindOtherCpDiv;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the salesCompanyCode.
     * 
     * @return the salesCompanyCode
     */
    public String getSalesCompanyCode() {
        return salesCompanyCode;
    }

    /** 
     * Sets the salesCompanyCode.
     * 
     * @param salesCompanyCode the salesCompanyCode
     */
    public void setSalesCompanyCode(String salesCompanyCode) {
        this.salesCompanyCode = salesCompanyCode;
    }

    /** 
     * Returns the departmentCode.
     * 
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /** 
     * Sets the departmentCode.
     * 
     * @param departmentCode the departmentCode
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /** 
     * Returns the salesOfficeCode.
     * 
     * @return the salesOfficeCode
     */
    public String getSalesOfficeCode() {
        return salesOfficeCode;
    }

    /** 
     * Sets the salesOfficeCode.
     * 
     * @param salesOfficeCode the salesOfficeCode
     */
    public void setSalesOfficeCode(String salesOfficeCode) {
        this.salesOfficeCode = salesOfficeCode;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the sgaPlanYear.
     * 
     * @return the sgaPlanYear
     */
    public Integer getSgaPlanYear() {
        return sgaPlanYear;
    }

    /** 
     * Sets the sgaPlanYear.
     * 
     * @param sgaPlanYear the sgaPlanYear
     */
    public void setSgaPlanYear(Integer sgaPlanYear) {
        this.sgaPlanYear = sgaPlanYear;
    }

    /** 
     * Returns the sgaPlanTermDiv.
     * 
     * @return the sgaPlanTermDiv
     */
    public String getSgaPlanTermDiv() {
        return sgaPlanTermDiv;
    }

    /** 
     * Sets the sgaPlanTermDiv.
     * 
     * @param sgaPlanTermDiv the sgaPlanTermDiv
     */
    public void setSgaPlanTermDiv(String sgaPlanTermDiv) {
        this.sgaPlanTermDiv = sgaPlanTermDiv;
    }

    /** 
     * Returns the sgaPlanYearMonth.
     * 
     * @return the sgaPlanYearMonth
     */
    public Integer getSgaPlanYearMonth() {
        return sgaPlanYearMonth;
    }

    /** 
     * Sets the sgaPlanYearMonth.
     * 
     * @param sgaPlanYearMonth the sgaPlanYearMonth
     */
    public void setSgaPlanYearMonth(Integer sgaPlanYearMonth) {
        this.sgaPlanYearMonth = sgaPlanYearMonth;
    }

    /** 
     * Returns the productClassYrcDiv.
     * 
     * @return the productClassYrcDiv
     */
    public String getProductClassYrcDiv() {
        return productClassYrcDiv;
    }

    /** 
     * Sets the productClassYrcDiv.
     * 
     * @param productClassYrcDiv the productClassYrcDiv
     */
    public void setProductClassYrcDiv(String productClassYrcDiv) {
        this.productClassYrcDiv = productClassYrcDiv;
    }

    /** 
     * Returns the largeClass1.
     * 
     * @return the largeClass1
     */
    public String getLargeClass1() {
        return largeClass1;
    }

    /** 
     * Sets the largeClass1.
     * 
     * @param largeClass1 the largeClass1
     */
    public void setLargeClass1(String largeClass1) {
        this.largeClass1 = largeClass1;
    }

    /** 
     * Returns the largeClass2.
     * 
     * @return the largeClass2
     */
    public String getLargeClass2() {
        return largeClass2;
    }

    /** 
     * Sets the largeClass2.
     * 
     * @param largeClass2 the largeClass2
     */
    public void setLargeClass2(String largeClass2) {
        this.largeClass2 = largeClass2;
    }

    /** 
     * Returns the middleClass.
     * 
     * @return the middleClass
     */
    public String getMiddleClass() {
        return middleClass;
    }

    /** 
     * Sets the middleClass.
     * 
     * @param middleClass the middleClass
     */
    public void setMiddleClass(String middleClass) {
        this.middleClass = middleClass;
    }

    /** 
     * Returns the smallClass.
     * 
     * @return the smallClass
     */
    public String getSmallClass() {
        return smallClass;
    }

    /** 
     * Sets the smallClass.
     * 
     * @param smallClass the smallClass
     */
    public void setSmallClass(String smallClass) {
        this.smallClass = smallClass;
    }

    /** 
     * Returns the productKindCd.
     * 
     * @return the productKindCd
     */
    public String getProductKindCd() {
        return productKindCd;
    }

    /** 
     * Sets the productKindCd.
     * 
     * @param productKindCd the productKindCd
     */
    public void setProductKindCd(String productKindCd) {
        this.productKindCd = productKindCd;
    }

    /** 
     * Returns the number.
     * 
     * @return the number
     */
    public BigDecimal getNumber() {
        return number;
    }

    /** 
     * Sets the number.
     * 
     * @param number the number
     */
    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    /** 
     * Returns the sales.
     * 
     * @return the sales
     */
    public BigDecimal getSales() {
        return sales;
    }

    /** 
     * Sets the sales.
     * 
     * @param sales the sales
     */
    public void setSales(BigDecimal sales) {
        this.sales = sales;
    }

    /** 
     * Returns the margin.
     * 
     * @return the margin
     */
    public BigDecimal getMargin() {
        return margin;
    }

    /** 
     * Sets the margin.
     * 
     * @param margin the margin
     */
    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    /** 
     * Returns the hqMargin.
     * 
     * @return the hqMargin
     */
    public BigDecimal getHqMargin() {
        return hqMargin;
    }

    /** 
     * Sets the hqMargin.
     * 
     * @param hqMargin the hqMargin
     */
    public void setHqMargin(BigDecimal hqMargin) {
        this.hqMargin = hqMargin;
    }

    /** 
     * Returns the baseAmount.
     * 
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /** 
     * Sets the baseAmount.
     * 
     * @param baseAmount the baseAmount
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /** 
     * Returns the updatedUserId.
     * 
     * @return the updatedUserId
     */
    public String getUpdatedUserId() {
        return updatedUserId;
    }

    /** 
     * Sets the updatedUserId.
     * 
     * @param updatedUserId the updatedUserId
     */
    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    /** 
     * Returns the updated.
     * 
     * @return the updated
     */
    public boolean isUpdated() {
        return updated;
    }

    /** 
     * Sets the updated.
     * 
     * @param updated the updated
     */
    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    /** 
     * Returns the dateUpdated.
     * 
     * @return the dateUpdated
     */
    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    /** 
     * Sets the dateUpdated.
     * 
     * @param dateUpdated the dateUpdated
     */
    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    /** 
     * Returns the kindOtherCpDiv.
     * 
     * @return the kindOtherCpDiv
     */
    public String getKindOtherCpDiv() {
        return kindOtherCpDiv;
    }

    /** 
     * Sets the kindOtherCpDiv.
     * 
     * @param kindOtherCpDiv the kindOtherCpDiv
     */
    public void setKindOtherCpDiv(String kindOtherCpDiv) {
        this.kindOtherCpDiv = kindOtherCpDiv;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}