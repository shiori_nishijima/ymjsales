package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpAccountsListener implements EntityListener<TmpAccounts> {

    @Override
    public void preInsert(TmpAccounts entity, PreInsertContext<TmpAccounts> context) {
    }

    @Override
    public void preUpdate(TmpAccounts entity, PreUpdateContext<TmpAccounts> context) {
    }

    @Override
    public void preDelete(TmpAccounts entity, PreDeleteContext<TmpAccounts> context) {
    }

    @Override
    public void postInsert(TmpAccounts entity, PostInsertContext<TmpAccounts> context) {
    }

    @Override
    public void postUpdate(TmpAccounts entity, PostUpdateContext<TmpAccounts> context) {
    }

    @Override
    public void postDelete(TmpAccounts entity, PostDeleteContext<TmpAccounts> context) {
    }
}