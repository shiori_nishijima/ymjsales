package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpMarketsListener implements EntityListener<TmpMarkets> {

    @Override
    public void preInsert(TmpMarkets entity, PreInsertContext<TmpMarkets> context) {
    }

    @Override
    public void preUpdate(TmpMarkets entity, PreUpdateContext<TmpMarkets> context) {
    }

    @Override
    public void preDelete(TmpMarkets entity, PreDeleteContext<TmpMarkets> context) {
    }

    @Override
    public void postInsert(TmpMarkets entity, PostInsertContext<TmpMarkets> context) {
    }

    @Override
    public void postUpdate(TmpMarkets entity, PostUpdateContext<TmpMarkets> context) {
    }

    @Override
    public void postDelete(TmpMarkets entity, PostDeleteContext<TmpMarkets> context) {
    }
}