package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class LogInfosListener implements EntityListener<LogInfos> {

    @Override
    public void preInsert(LogInfos entity, PreInsertContext<LogInfos> context) {
    }

    @Override
    public void preUpdate(LogInfos entity, PreUpdateContext<LogInfos> context) {
    }

    @Override
    public void preDelete(LogInfos entity, PreDeleteContext<LogInfos> context) {
    }

    @Override
    public void postInsert(LogInfos entity, PostInsertContext<LogInfos> context) {
    }

    @Override
    public void postUpdate(LogInfos entity, PostUpdateContext<LogInfos> context) {
    }

    @Override
    public void postDelete(LogInfos entity, PostDeleteContext<LogInfos> context) {
    }
}