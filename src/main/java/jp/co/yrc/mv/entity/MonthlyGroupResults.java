package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyGroupResultsListener.class)
@Table(name = "monthly_group_results")
public class MonthlyGroupResults {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** Aランク売上 */
    @Column(name = "rank_a_sales")
    BigDecimal rankASales;

    /** Aランク粗利 */
    @Column(name = "rank_a_margin")
    BigDecimal rankAMargin;

    /** Aランク本数 */
    @Column(name = "rank_a_number")
    BigDecimal rankANumber;

    /** Bランク売上 */
    @Column(name = "rank_b_sales")
    BigDecimal rankBSales;

    /** Bランク粗利 */
    @Column(name = "rank_b_margin")
    BigDecimal rankBMargin;

    /** Bランク本数 */
    @Column(name = "rank_b_number")
    BigDecimal rankBNumber;

    /** Cランク売上 */
    @Column(name = "rank_c_sales")
    BigDecimal rankCSales;

    /** Cランク粗利 */
    @Column(name = "rank_c_margin")
    BigDecimal rankCMargin;

    /** Cランク本数 */
    @Column(name = "rank_c_number")
    BigDecimal rankCNumber;

    /** 重点売上 */
    @Column(name = "important_sales")
    BigDecimal importantSales;

    /** 重点粗利 */
    @Column(name = "important_margin")
    BigDecimal importantMargin;

    /** 重点本数 */
    @Column(name = "important_number")
    BigDecimal importantNumber;

    /** 新規売上 */
    @Column(name = "new_sales")
    BigDecimal newSales;

    /** 新規粗利 */
    @Column(name = "new_margin")
    BigDecimal newMargin;

    /** 新規本数 */
    @Column(name = "new_number")
    BigDecimal newNumber;

    /** 深耕売上 */
    @Column(name = "deep_plowing_sales")
    BigDecimal deepPlowingSales;

    /** 深耕利益 */
    @Column(name = "deep_plowing_margin")
    BigDecimal deepPlowingMargin;

    /** 深耕本数 */
    @Column(name = "deep_plowing_number")
    BigDecimal deepPlowingNumber;

    /** Y-CP売上 */
    @Column(name = "y_cp_sales")
    BigDecimal yCpSales;

    /** Y-CP粗利 */
    @Column(name = "y_cp_margin")
    BigDecimal yCpMargin;

    /** Y-CP本数 */
    @Column(name = "y_cp_number")
    BigDecimal yCpNumber;

    /** 他社CP売上 */
    @Column(name = "other_cp_sales")
    BigDecimal otherCpSales;

    /** 他社CP粗利 */
    @Column(name = "other_cp_margin")
    BigDecimal otherCpMargin;

    /** 他社CP本数 */
    @Column(name = "other_cp_number")
    BigDecimal otherCpNumber;

    /** グループ1売上 */
    @Column(name = "group_one_sales")
    BigDecimal groupOneSales;

    /** グループ1粗利 */
    @Column(name = "group_one_margin")
    BigDecimal groupOneMargin;

    /** グループ1本数 */
    @Column(name = "group_one_number")
    BigDecimal groupOneNumber;

    /** グループ2売上 */
    @Column(name = "group_two_sales")
    BigDecimal groupTwoSales;

    /** グループ2粗利 */
    @Column(name = "group_two_margin")
    BigDecimal groupTwoMargin;

    /** グループ2取本数 */
    @Column(name = "group_two_number")
    BigDecimal groupTwoNumber;

    /** グループ3売上 */
    @Column(name = "group_three_sales")
    BigDecimal groupThreeSales;

    /** グループ3粗利 */
    @Column(name = "group_three_margin")
    BigDecimal groupThreeMargin;

    /** グループ3本数 */
    @Column(name = "group_three_number")
    BigDecimal groupThreeNumber;

    /** グループ4売上 */
    @Column(name = "group_four_sales")
    BigDecimal groupFourSales;

    /** グループ4粗利 */
    @Column(name = "group_four_margin")
    BigDecimal groupFourMargin;

    /** グループ4本数 */
    @Column(name = "group_four_number")
    BigDecimal groupFourNumber;

    /** グループ5売上 */
    @Column(name = "group_five_sales")
    BigDecimal groupFiveSales;

    /** グループ5粗利 */
    @Column(name = "group_five_margin")
    BigDecimal groupFiveMargin;

    /** グループ5本数 */
    @Column(name = "group_five_number")
    BigDecimal groupFiveNumber;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the rankASales.
     * 
     * @return the rankASales
     */
    public BigDecimal getRankASales() {
        return rankASales;
    }

    /** 
     * Sets the rankASales.
     * 
     * @param rankASales the rankASales
     */
    public void setRankASales(BigDecimal rankASales) {
        this.rankASales = rankASales;
    }

    /** 
     * Returns the rankAMargin.
     * 
     * @return the rankAMargin
     */
    public BigDecimal getRankAMargin() {
        return rankAMargin;
    }

    /** 
     * Sets the rankAMargin.
     * 
     * @param rankAMargin the rankAMargin
     */
    public void setRankAMargin(BigDecimal rankAMargin) {
        this.rankAMargin = rankAMargin;
    }

    /** 
     * Returns the rankANumber.
     * 
     * @return the rankANumber
     */
    public BigDecimal getRankANumber() {
        return rankANumber;
    }

    /** 
     * Sets the rankANumber.
     * 
     * @param rankANumber the rankANumber
     */
    public void setRankANumber(BigDecimal rankANumber) {
        this.rankANumber = rankANumber;
    }

    /** 
     * Returns the rankBSales.
     * 
     * @return the rankBSales
     */
    public BigDecimal getRankBSales() {
        return rankBSales;
    }

    /** 
     * Sets the rankBSales.
     * 
     * @param rankBSales the rankBSales
     */
    public void setRankBSales(BigDecimal rankBSales) {
        this.rankBSales = rankBSales;
    }

    /** 
     * Returns the rankBMargin.
     * 
     * @return the rankBMargin
     */
    public BigDecimal getRankBMargin() {
        return rankBMargin;
    }

    /** 
     * Sets the rankBMargin.
     * 
     * @param rankBMargin the rankBMargin
     */
    public void setRankBMargin(BigDecimal rankBMargin) {
        this.rankBMargin = rankBMargin;
    }

    /** 
     * Returns the rankBNumber.
     * 
     * @return the rankBNumber
     */
    public BigDecimal getRankBNumber() {
        return rankBNumber;
    }

    /** 
     * Sets the rankBNumber.
     * 
     * @param rankBNumber the rankBNumber
     */
    public void setRankBNumber(BigDecimal rankBNumber) {
        this.rankBNumber = rankBNumber;
    }

    /** 
     * Returns the rankCSales.
     * 
     * @return the rankCSales
     */
    public BigDecimal getRankCSales() {
        return rankCSales;
    }

    /** 
     * Sets the rankCSales.
     * 
     * @param rankCSales the rankCSales
     */
    public void setRankCSales(BigDecimal rankCSales) {
        this.rankCSales = rankCSales;
    }

    /** 
     * Returns the rankCMargin.
     * 
     * @return the rankCMargin
     */
    public BigDecimal getRankCMargin() {
        return rankCMargin;
    }

    /** 
     * Sets the rankCMargin.
     * 
     * @param rankCMargin the rankCMargin
     */
    public void setRankCMargin(BigDecimal rankCMargin) {
        this.rankCMargin = rankCMargin;
    }

    /** 
     * Returns the rankCNumber.
     * 
     * @return the rankCNumber
     */
    public BigDecimal getRankCNumber() {
        return rankCNumber;
    }

    /** 
     * Sets the rankCNumber.
     * 
     * @param rankCNumber the rankCNumber
     */
    public void setRankCNumber(BigDecimal rankCNumber) {
        this.rankCNumber = rankCNumber;
    }

    /** 
     * Returns the importantSales.
     * 
     * @return the importantSales
     */
    public BigDecimal getImportantSales() {
        return importantSales;
    }

    /** 
     * Sets the importantSales.
     * 
     * @param importantSales the importantSales
     */
    public void setImportantSales(BigDecimal importantSales) {
        this.importantSales = importantSales;
    }

    /** 
     * Returns the importantMargin.
     * 
     * @return the importantMargin
     */
    public BigDecimal getImportantMargin() {
        return importantMargin;
    }

    /** 
     * Sets the importantMargin.
     * 
     * @param importantMargin the importantMargin
     */
    public void setImportantMargin(BigDecimal importantMargin) {
        this.importantMargin = importantMargin;
    }

    /** 
     * Returns the importantNumber.
     * 
     * @return the importantNumber
     */
    public BigDecimal getImportantNumber() {
        return importantNumber;
    }

    /** 
     * Sets the importantNumber.
     * 
     * @param importantNumber the importantNumber
     */
    public void setImportantNumber(BigDecimal importantNumber) {
        this.importantNumber = importantNumber;
    }

    /** 
     * Returns the newSales.
     * 
     * @return the newSales
     */
    public BigDecimal getNewSales() {
        return newSales;
    }

    /** 
     * Sets the newSales.
     * 
     * @param newSales the newSales
     */
    public void setNewSales(BigDecimal newSales) {
        this.newSales = newSales;
    }

    /** 
     * Returns the newMargin.
     * 
     * @return the newMargin
     */
    public BigDecimal getNewMargin() {
        return newMargin;
    }

    /** 
     * Sets the newMargin.
     * 
     * @param newMargin the newMargin
     */
    public void setNewMargin(BigDecimal newMargin) {
        this.newMargin = newMargin;
    }

    /** 
     * Returns the newNumber.
     * 
     * @return the newNumber
     */
    public BigDecimal getNewNumber() {
        return newNumber;
    }

    /** 
     * Sets the newNumber.
     * 
     * @param newNumber the newNumber
     */
    public void setNewNumber(BigDecimal newNumber) {
        this.newNumber = newNumber;
    }

    /** 
     * Returns the deepPlowingSales.
     * 
     * @return the deepPlowingSales
     */
    public BigDecimal getDeepPlowingSales() {
        return deepPlowingSales;
    }

    /** 
     * Sets the deepPlowingSales.
     * 
     * @param deepPlowingSales the deepPlowingSales
     */
    public void setDeepPlowingSales(BigDecimal deepPlowingSales) {
        this.deepPlowingSales = deepPlowingSales;
    }

    /** 
     * Returns the deepPlowingMargin.
     * 
     * @return the deepPlowingMargin
     */
    public BigDecimal getDeepPlowingMargin() {
        return deepPlowingMargin;
    }

    /** 
     * Sets the deepPlowingMargin.
     * 
     * @param deepPlowingMargin the deepPlowingMargin
     */
    public void setDeepPlowingMargin(BigDecimal deepPlowingMargin) {
        this.deepPlowingMargin = deepPlowingMargin;
    }

    /** 
     * Returns the deepPlowingNumber.
     * 
     * @return the deepPlowingNumber
     */
    public BigDecimal getDeepPlowingNumber() {
        return deepPlowingNumber;
    }

    /** 
     * Sets the deepPlowingNumber.
     * 
     * @param deepPlowingNumber the deepPlowingNumber
     */
    public void setDeepPlowingNumber(BigDecimal deepPlowingNumber) {
        this.deepPlowingNumber = deepPlowingNumber;
    }

    /** 
     * Returns the yCpSales.
     * 
     * @return the yCpSales
     */
    public BigDecimal getYCpSales() {
        return yCpSales;
    }

    /** 
     * Sets the yCpSales.
     * 
     * @param yCpSales the yCpSales
     */
    public void setYCpSales(BigDecimal yCpSales) {
        this.yCpSales = yCpSales;
    }

    /** 
     * Returns the yCpMargin.
     * 
     * @return the yCpMargin
     */
    public BigDecimal getYCpMargin() {
        return yCpMargin;
    }

    /** 
     * Sets the yCpMargin.
     * 
     * @param yCpMargin the yCpMargin
     */
    public void setYCpMargin(BigDecimal yCpMargin) {
        this.yCpMargin = yCpMargin;
    }

    /** 
     * Returns the yCpNumber.
     * 
     * @return the yCpNumber
     */
    public BigDecimal getYCpNumber() {
        return yCpNumber;
    }

    /** 
     * Sets the yCpNumber.
     * 
     * @param yCpNumber the yCpNumber
     */
    public void setYCpNumber(BigDecimal yCpNumber) {
        this.yCpNumber = yCpNumber;
    }

    /** 
     * Returns the otherCpSales.
     * 
     * @return the otherCpSales
     */
    public BigDecimal getOtherCpSales() {
        return otherCpSales;
    }

    /** 
     * Sets the otherCpSales.
     * 
     * @param otherCpSales the otherCpSales
     */
    public void setOtherCpSales(BigDecimal otherCpSales) {
        this.otherCpSales = otherCpSales;
    }

    /** 
     * Returns the otherCpMargin.
     * 
     * @return the otherCpMargin
     */
    public BigDecimal getOtherCpMargin() {
        return otherCpMargin;
    }

    /** 
     * Sets the otherCpMargin.
     * 
     * @param otherCpMargin the otherCpMargin
     */
    public void setOtherCpMargin(BigDecimal otherCpMargin) {
        this.otherCpMargin = otherCpMargin;
    }

    /** 
     * Returns the otherCpNumber.
     * 
     * @return the otherCpNumber
     */
    public BigDecimal getOtherCpNumber() {
        return otherCpNumber;
    }

    /** 
     * Sets the otherCpNumber.
     * 
     * @param otherCpNumber the otherCpNumber
     */
    public void setOtherCpNumber(BigDecimal otherCpNumber) {
        this.otherCpNumber = otherCpNumber;
    }

    /** 
     * Returns the groupOneSales.
     * 
     * @return the groupOneSales
     */
    public BigDecimal getGroupOneSales() {
        return groupOneSales;
    }

    /** 
     * Sets the groupOneSales.
     * 
     * @param groupOneSales the groupOneSales
     */
    public void setGroupOneSales(BigDecimal groupOneSales) {
        this.groupOneSales = groupOneSales;
    }

    /** 
     * Returns the groupOneMargin.
     * 
     * @return the groupOneMargin
     */
    public BigDecimal getGroupOneMargin() {
        return groupOneMargin;
    }

    /** 
     * Sets the groupOneMargin.
     * 
     * @param groupOneMargin the groupOneMargin
     */
    public void setGroupOneMargin(BigDecimal groupOneMargin) {
        this.groupOneMargin = groupOneMargin;
    }

    /** 
     * Returns the groupOneNumber.
     * 
     * @return the groupOneNumber
     */
    public BigDecimal getGroupOneNumber() {
        return groupOneNumber;
    }

    /** 
     * Sets the groupOneNumber.
     * 
     * @param groupOneNumber the groupOneNumber
     */
    public void setGroupOneNumber(BigDecimal groupOneNumber) {
        this.groupOneNumber = groupOneNumber;
    }

    /** 
     * Returns the groupTwoSales.
     * 
     * @return the groupTwoSales
     */
    public BigDecimal getGroupTwoSales() {
        return groupTwoSales;
    }

    /** 
     * Sets the groupTwoSales.
     * 
     * @param groupTwoSales the groupTwoSales
     */
    public void setGroupTwoSales(BigDecimal groupTwoSales) {
        this.groupTwoSales = groupTwoSales;
    }

    /** 
     * Returns the groupTwoMargin.
     * 
     * @return the groupTwoMargin
     */
    public BigDecimal getGroupTwoMargin() {
        return groupTwoMargin;
    }

    /** 
     * Sets the groupTwoMargin.
     * 
     * @param groupTwoMargin the groupTwoMargin
     */
    public void setGroupTwoMargin(BigDecimal groupTwoMargin) {
        this.groupTwoMargin = groupTwoMargin;
    }

    /** 
     * Returns the groupTwoNumber.
     * 
     * @return the groupTwoNumber
     */
    public BigDecimal getGroupTwoNumber() {
        return groupTwoNumber;
    }

    /** 
     * Sets the groupTwoNumber.
     * 
     * @param groupTwoNumber the groupTwoNumber
     */
    public void setGroupTwoNumber(BigDecimal groupTwoNumber) {
        this.groupTwoNumber = groupTwoNumber;
    }

    /** 
     * Returns the groupThreeSales.
     * 
     * @return the groupThreeSales
     */
    public BigDecimal getGroupThreeSales() {
        return groupThreeSales;
    }

    /** 
     * Sets the groupThreeSales.
     * 
     * @param groupThreeSales the groupThreeSales
     */
    public void setGroupThreeSales(BigDecimal groupThreeSales) {
        this.groupThreeSales = groupThreeSales;
    }

    /** 
     * Returns the groupThreeMargin.
     * 
     * @return the groupThreeMargin
     */
    public BigDecimal getGroupThreeMargin() {
        return groupThreeMargin;
    }

    /** 
     * Sets the groupThreeMargin.
     * 
     * @param groupThreeMargin the groupThreeMargin
     */
    public void setGroupThreeMargin(BigDecimal groupThreeMargin) {
        this.groupThreeMargin = groupThreeMargin;
    }

    /** 
     * Returns the groupThreeNumber.
     * 
     * @return the groupThreeNumber
     */
    public BigDecimal getGroupThreeNumber() {
        return groupThreeNumber;
    }

    /** 
     * Sets the groupThreeNumber.
     * 
     * @param groupThreeNumber the groupThreeNumber
     */
    public void setGroupThreeNumber(BigDecimal groupThreeNumber) {
        this.groupThreeNumber = groupThreeNumber;
    }

    /** 
     * Returns the groupFourSales.
     * 
     * @return the groupFourSales
     */
    public BigDecimal getGroupFourSales() {
        return groupFourSales;
    }

    /** 
     * Sets the groupFourSales.
     * 
     * @param groupFourSales the groupFourSales
     */
    public void setGroupFourSales(BigDecimal groupFourSales) {
        this.groupFourSales = groupFourSales;
    }

    /** 
     * Returns the groupFourMargin.
     * 
     * @return the groupFourMargin
     */
    public BigDecimal getGroupFourMargin() {
        return groupFourMargin;
    }

    /** 
     * Sets the groupFourMargin.
     * 
     * @param groupFourMargin the groupFourMargin
     */
    public void setGroupFourMargin(BigDecimal groupFourMargin) {
        this.groupFourMargin = groupFourMargin;
    }

    /** 
     * Returns the groupFourNumber.
     * 
     * @return the groupFourNumber
     */
    public BigDecimal getGroupFourNumber() {
        return groupFourNumber;
    }

    /** 
     * Sets the groupFourNumber.
     * 
     * @param groupFourNumber the groupFourNumber
     */
    public void setGroupFourNumber(BigDecimal groupFourNumber) {
        this.groupFourNumber = groupFourNumber;
    }

    /** 
     * Returns the groupFiveSales.
     * 
     * @return the groupFiveSales
     */
    public BigDecimal getGroupFiveSales() {
        return groupFiveSales;
    }

    /** 
     * Sets the groupFiveSales.
     * 
     * @param groupFiveSales the groupFiveSales
     */
    public void setGroupFiveSales(BigDecimal groupFiveSales) {
        this.groupFiveSales = groupFiveSales;
    }

    /** 
     * Returns the groupFiveMargin.
     * 
     * @return the groupFiveMargin
     */
    public BigDecimal getGroupFiveMargin() {
        return groupFiveMargin;
    }

    /** 
     * Sets the groupFiveMargin.
     * 
     * @param groupFiveMargin the groupFiveMargin
     */
    public void setGroupFiveMargin(BigDecimal groupFiveMargin) {
        this.groupFiveMargin = groupFiveMargin;
    }

    /** 
     * Returns the groupFiveNumber.
     * 
     * @return the groupFiveNumber
     */
    public BigDecimal getGroupFiveNumber() {
        return groupFiveNumber;
    }

    /** 
     * Sets the groupFiveNumber.
     * 
     * @param groupFiveNumber the groupFiveNumber
     */
    public void setGroupFiveNumber(BigDecimal groupFiveNumber) {
        this.groupFiveNumber = groupFiveNumber;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}