package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SelectionItemsSecondLevelListener implements EntityListener<SelectionItemsSecondLevel> {

    @Override
    public void preInsert(SelectionItemsSecondLevel entity, PreInsertContext<SelectionItemsSecondLevel> context) {
    }

    @Override
    public void preUpdate(SelectionItemsSecondLevel entity, PreUpdateContext<SelectionItemsSecondLevel> context) {
    }

    @Override
    public void preDelete(SelectionItemsSecondLevel entity, PreDeleteContext<SelectionItemsSecondLevel> context) {
    }

    @Override
    public void postInsert(SelectionItemsSecondLevel entity, PostInsertContext<SelectionItemsSecondLevel> context) {
    }

    @Override
    public void postUpdate(SelectionItemsSecondLevel entity, PostUpdateContext<SelectionItemsSecondLevel> context) {
    }

    @Override
    public void postDelete(SelectionItemsSecondLevel entity, PostDeleteContext<SelectionItemsSecondLevel> context) {
    }
}