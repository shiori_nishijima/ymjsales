package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ReserveStocksListener implements EntityListener<ReserveStocks> {

    @Override
    public void preInsert(ReserveStocks entity, PreInsertContext<ReserveStocks> context) {
    }

    @Override
    public void preUpdate(ReserveStocks entity, PreUpdateContext<ReserveStocks> context) {
    }

    @Override
    public void preDelete(ReserveStocks entity, PreDeleteContext<ReserveStocks> context) {
    }

    @Override
    public void postInsert(ReserveStocks entity, PostInsertContext<ReserveStocks> context) {
    }

    @Override
    public void postUpdate(ReserveStocks entity, PostUpdateContext<ReserveStocks> context) {
    }

    @Override
    public void postDelete(ReserveStocks entity, PostDeleteContext<ReserveStocks> context) {
    }
}