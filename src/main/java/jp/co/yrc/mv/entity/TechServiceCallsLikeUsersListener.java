package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCallsLikeUsersListener implements EntityListener<TechServiceCallsLikeUsers> {

    @Override
    public void preInsert(TechServiceCallsLikeUsers entity, PreInsertContext<TechServiceCallsLikeUsers> context) {
    }

    @Override
    public void preUpdate(TechServiceCallsLikeUsers entity, PreUpdateContext<TechServiceCallsLikeUsers> context) {
    }

    @Override
    public void preDelete(TechServiceCallsLikeUsers entity, PreDeleteContext<TechServiceCallsLikeUsers> context) {
    }

    @Override
    public void postInsert(TechServiceCallsLikeUsers entity, PostInsertContext<TechServiceCallsLikeUsers> context) {
    }

    @Override
    public void postUpdate(TechServiceCallsLikeUsers entity, PostUpdateContext<TechServiceCallsLikeUsers> context) {
    }

    @Override
    public void postDelete(TechServiceCallsLikeUsers entity, PostDeleteContext<TechServiceCallsLikeUsers> context) {
    }
}