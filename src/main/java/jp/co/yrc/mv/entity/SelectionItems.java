package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = SelectionItemsListener.class)
@Table(name = "selection_items")
public class SelectionItems {

    /** 変数名 */
    @Id
    @Column(name = "entity_name")
    String entityName;

    /** プロパティ名 */
    @Id
    @Column(name = "property_name")
    String propertyName;

    /** 値 */
    @Id
    @Column(name = "value")
    String value;

    /** ラベル */
    @Column(name = "label")
    String label;

    /** ソート順 */
    @Column(name = "sort_order")
    Integer sortOrder;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the entityName.
     * 
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /** 
     * Sets the entityName.
     * 
     * @param entityName the entityName
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /** 
     * Returns the propertyName.
     * 
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /** 
     * Sets the propertyName.
     * 
     * @param propertyName the propertyName
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /** 
     * Returns the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /** 
     * Sets the value.
     * 
     * @param value the value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /** 
     * Returns the label.
     * 
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /** 
     * Sets the label.
     * 
     * @param label the label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /** 
     * Returns the sortOrder.
     * 
     * @return the sortOrder
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /** 
     * Sets the sortOrder.
     * 
     * @param sortOrder the sortOrder
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}