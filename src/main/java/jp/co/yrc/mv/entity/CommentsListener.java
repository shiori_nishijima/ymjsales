package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CommentsListener implements EntityListener<Comments> {

    @Override
    public void preInsert(Comments entity, PreInsertContext<Comments> context) {
    }

    @Override
    public void preUpdate(Comments entity, PreUpdateContext<Comments> context) {
    }

    @Override
    public void preDelete(Comments entity, PreDeleteContext<Comments> context) {
    }

    @Override
    public void postInsert(Comments entity, PostInsertContext<Comments> context) {
    }

    @Override
    public void postUpdate(Comments entity, PostUpdateContext<Comments> context) {
    }

    @Override
    public void postDelete(Comments entity, PostDeleteContext<Comments> context) {
    }
}