package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MstEigyoSosikiListener.class)
@Table(name = "mst_eigyo_sosiki")
public class MstEigyoSosiki {

    /**  */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /**  */
    @Column(name = "eigyo_sosiki_nm")
    String eigyoSosikiNm;

    /**  */
    @Column(name = "eigyo_sosiki_short_nm")
    String eigyoSosikiShortNm;

    /**  */
    @Column(name = "corp_cd")
    String corpCd;

    /**  */
    @Column(name = "div1_cd")
    String div1Cd;

    /**  */
    @Column(name = "div2_cd")
    String div2Cd;

    /**  */
    @Column(name = "div3_cd")
    String div3Cd;

    /**  */
    @Column(name = "div4_cd")
    String div4Cd;

    /**  */
    @Column(name = "div5_cd")
    String div5Cd;

    /**  */
    @Column(name = "div6_cd")
    String div6Cd;

    /**  */
    @Column(name = "div1_nm")
    String div1Nm;

    /**  */
    @Column(name = "div2_nm")
    String div2Nm;

    /**  */
    @Column(name = "div3_nm")
    String div3Nm;

    /**  */
    @Column(name = "div4_nm")
    String div4Nm;

    /**  */
    @Column(name = "div5_nm")
    String div5Nm;

    /**  */
    @Column(name = "div6_nm")
    String div6Nm;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the eigyoSosikiNm.
     * 
     * @return the eigyoSosikiNm
     */
    public String getEigyoSosikiNm() {
        return eigyoSosikiNm;
    }

    /** 
     * Sets the eigyoSosikiNm.
     * 
     * @param eigyoSosikiNm the eigyoSosikiNm
     */
    public void setEigyoSosikiNm(String eigyoSosikiNm) {
        this.eigyoSosikiNm = eigyoSosikiNm;
    }

    /** 
     * Returns the eigyoSosikiShortNm.
     * 
     * @return the eigyoSosikiShortNm
     */
    public String getEigyoSosikiShortNm() {
        return eigyoSosikiShortNm;
    }

    /** 
     * Sets the eigyoSosikiShortNm.
     * 
     * @param eigyoSosikiShortNm the eigyoSosikiShortNm
     */
    public void setEigyoSosikiShortNm(String eigyoSosikiShortNm) {
        this.eigyoSosikiShortNm = eigyoSosikiShortNm;
    }

    /** 
     * Returns the corpCd.
     * 
     * @return the corpCd
     */
    public String getCorpCd() {
        return corpCd;
    }

    /** 
     * Sets the corpCd.
     * 
     * @param corpCd the corpCd
     */
    public void setCorpCd(String corpCd) {
        this.corpCd = corpCd;
    }

    /** 
     * Returns the div1Cd.
     * 
     * @return the div1Cd
     */
    public String getDiv1Cd() {
        return div1Cd;
    }

    /** 
     * Sets the div1Cd.
     * 
     * @param div1Cd the div1Cd
     */
    public void setDiv1Cd(String div1Cd) {
        this.div1Cd = div1Cd;
    }

    /** 
     * Returns the div2Cd.
     * 
     * @return the div2Cd
     */
    public String getDiv2Cd() {
        return div2Cd;
    }

    /** 
     * Sets the div2Cd.
     * 
     * @param div2Cd the div2Cd
     */
    public void setDiv2Cd(String div2Cd) {
        this.div2Cd = div2Cd;
    }

    /** 
     * Returns the div3Cd.
     * 
     * @return the div3Cd
     */
    public String getDiv3Cd() {
        return div3Cd;
    }

    /** 
     * Sets the div3Cd.
     * 
     * @param div3Cd the div3Cd
     */
    public void setDiv3Cd(String div3Cd) {
        this.div3Cd = div3Cd;
    }

    /** 
     * Returns the div4Cd.
     * 
     * @return the div4Cd
     */
    public String getDiv4Cd() {
        return div4Cd;
    }

    /** 
     * Sets the div4Cd.
     * 
     * @param div4Cd the div4Cd
     */
    public void setDiv4Cd(String div4Cd) {
        this.div4Cd = div4Cd;
    }

    /** 
     * Returns the div5Cd.
     * 
     * @return the div5Cd
     */
    public String getDiv5Cd() {
        return div5Cd;
    }

    /** 
     * Sets the div5Cd.
     * 
     * @param div5Cd the div5Cd
     */
    public void setDiv5Cd(String div5Cd) {
        this.div5Cd = div5Cd;
    }

    /** 
     * Returns the div6Cd.
     * 
     * @return the div6Cd
     */
    public String getDiv6Cd() {
        return div6Cd;
    }

    /** 
     * Sets the div6Cd.
     * 
     * @param div6Cd the div6Cd
     */
    public void setDiv6Cd(String div6Cd) {
        this.div6Cd = div6Cd;
    }

    /** 
     * Returns the div1Nm.
     * 
     * @return the div1Nm
     */
    public String getDiv1Nm() {
        return div1Nm;
    }

    /** 
     * Sets the div1Nm.
     * 
     * @param div1Nm the div1Nm
     */
    public void setDiv1Nm(String div1Nm) {
        this.div1Nm = div1Nm;
    }

    /** 
     * Returns the div2Nm.
     * 
     * @return the div2Nm
     */
    public String getDiv2Nm() {
        return div2Nm;
    }

    /** 
     * Sets the div2Nm.
     * 
     * @param div2Nm the div2Nm
     */
    public void setDiv2Nm(String div2Nm) {
        this.div2Nm = div2Nm;
    }

    /** 
     * Returns the div3Nm.
     * 
     * @return the div3Nm
     */
    public String getDiv3Nm() {
        return div3Nm;
    }

    /** 
     * Sets the div3Nm.
     * 
     * @param div3Nm the div3Nm
     */
    public void setDiv3Nm(String div3Nm) {
        this.div3Nm = div3Nm;
    }

    /** 
     * Returns the div4Nm.
     * 
     * @return the div4Nm
     */
    public String getDiv4Nm() {
        return div4Nm;
    }

    /** 
     * Sets the div4Nm.
     * 
     * @param div4Nm the div4Nm
     */
    public void setDiv4Nm(String div4Nm) {
        this.div4Nm = div4Nm;
    }

    /** 
     * Returns the div5Nm.
     * 
     * @return the div5Nm
     */
    public String getDiv5Nm() {
        return div5Nm;
    }

    /** 
     * Sets the div5Nm.
     * 
     * @param div5Nm the div5Nm
     */
    public void setDiv5Nm(String div5Nm) {
        this.div5Nm = div5Nm;
    }

    /** 
     * Returns the div6Nm.
     * 
     * @return the div6Nm
     */
    public String getDiv6Nm() {
        return div6Nm;
    }

    /** 
     * Sets the div6Nm.
     * 
     * @param div6Nm the div6Nm
     */
    public void setDiv6Nm(String div6Nm) {
        this.div6Nm = div6Nm;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}