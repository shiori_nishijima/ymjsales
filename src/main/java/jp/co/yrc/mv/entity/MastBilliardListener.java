package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MastBilliardListener implements EntityListener<MastBilliard> {

    @Override
    public void preInsert(MastBilliard entity, PreInsertContext<MastBilliard> context) {
    }

    @Override
    public void preUpdate(MastBilliard entity, PreUpdateContext<MastBilliard> context) {
    }

    @Override
    public void preDelete(MastBilliard entity, PreDeleteContext<MastBilliard> context) {
    }

    @Override
    public void postInsert(MastBilliard entity, PostInsertContext<MastBilliard> context) {
    }

    @Override
    public void postUpdate(MastBilliard entity, PostUpdateContext<MastBilliard> context) {
    }

    @Override
    public void postDelete(MastBilliard entity, PostDeleteContext<MastBilliard> context) {
    }
}