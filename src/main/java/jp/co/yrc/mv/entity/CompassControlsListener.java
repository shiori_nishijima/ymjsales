package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CompassControlsListener implements EntityListener<CompassControls> {

    @Override
    public void preInsert(CompassControls entity, PreInsertContext<CompassControls> context) {
    }

    @Override
    public void preUpdate(CompassControls entity, PreUpdateContext<CompassControls> context) {
    }

    @Override
    public void preDelete(CompassControls entity, PreDeleteContext<CompassControls> context) {
    }

    @Override
    public void postInsert(CompassControls entity, PostInsertContext<CompassControls> context) {
    }

    @Override
    public void postUpdate(CompassControls entity, PostUpdateContext<CompassControls> context) {
    }

    @Override
    public void postDelete(CompassControls entity, PostDeleteContext<CompassControls> context) {
    }
}