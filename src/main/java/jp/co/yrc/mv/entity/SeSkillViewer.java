package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = SeSkillViewerListener.class)
@Table(name = "se_skill_viewer")
public class SeSkillViewer {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 案件ID */
    @Column(name = "anken_id_c")
    String ankenIdC;

    /** 営業サポートCD */
    @Column(name = "opp_salessup_id")
    String oppSalessupId;

    /** SE社員番号 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 依頼案件種別CD */
    @Column(name = "req_opp_class_cd")
    String reqOppClassCd;

    /** カテゴリCD */
    @Column(name = "category_cd")
    String categoryCd;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the ankenIdC.
     * 
     * @return the ankenIdC
     */
    public String getAnkenIdC() {
        return ankenIdC;
    }

    /** 
     * Sets the ankenIdC.
     * 
     * @param ankenIdC the ankenIdC
     */
    public void setAnkenIdC(String ankenIdC) {
        this.ankenIdC = ankenIdC;
    }

    /** 
     * Returns the oppSalessupId.
     * 
     * @return the oppSalessupId
     */
    public String getOppSalessupId() {
        return oppSalessupId;
    }

    /** 
     * Sets the oppSalessupId.
     * 
     * @param oppSalessupId the oppSalessupId
     */
    public void setOppSalessupId(String oppSalessupId) {
        this.oppSalessupId = oppSalessupId;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the reqOppClassCd.
     * 
     * @return the reqOppClassCd
     */
    public String getReqOppClassCd() {
        return reqOppClassCd;
    }

    /** 
     * Sets the reqOppClassCd.
     * 
     * @param reqOppClassCd the reqOppClassCd
     */
    public void setReqOppClassCd(String reqOppClassCd) {
        this.reqOppClassCd = reqOppClassCd;
    }

    /** 
     * Returns the categoryCd.
     * 
     * @return the categoryCd
     */
    public String getCategoryCd() {
        return categoryCd;
    }

    /** 
     * Sets the categoryCd.
     * 
     * @param categoryCd the categoryCd
     */
    public void setCategoryCd(String categoryCd) {
        this.categoryCd = categoryCd;
    }
}