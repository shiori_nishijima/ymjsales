package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyGroupDemandEstimationsListener implements EntityListener<MonthlyGroupDemandEstimations> {

    @Override
    public void preInsert(MonthlyGroupDemandEstimations entity, PreInsertContext<MonthlyGroupDemandEstimations> context) {
    }

    @Override
    public void preUpdate(MonthlyGroupDemandEstimations entity, PreUpdateContext<MonthlyGroupDemandEstimations> context) {
    }

    @Override
    public void preDelete(MonthlyGroupDemandEstimations entity, PreDeleteContext<MonthlyGroupDemandEstimations> context) {
    }

    @Override
    public void postInsert(MonthlyGroupDemandEstimations entity, PostInsertContext<MonthlyGroupDemandEstimations> context) {
    }

    @Override
    public void postUpdate(MonthlyGroupDemandEstimations entity, PostUpdateContext<MonthlyGroupDemandEstimations> context) {
    }

    @Override
    public void postDelete(MonthlyGroupDemandEstimations entity, PostDeleteContext<MonthlyGroupDemandEstimations> context) {
    }
}