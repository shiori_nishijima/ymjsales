package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceMarketInfosListener implements EntityListener<TechServiceMarketInfos> {

    @Override
    public void preInsert(TechServiceMarketInfos entity, PreInsertContext<TechServiceMarketInfos> context) {
    }

    @Override
    public void preUpdate(TechServiceMarketInfos entity, PreUpdateContext<TechServiceMarketInfos> context) {
    }

    @Override
    public void preDelete(TechServiceMarketInfos entity, PreDeleteContext<TechServiceMarketInfos> context) {
    }

    @Override
    public void postInsert(TechServiceMarketInfos entity, PostInsertContext<TechServiceMarketInfos> context) {
    }

    @Override
    public void postUpdate(TechServiceMarketInfos entity, PostUpdateContext<TechServiceMarketInfos> context) {
    }

    @Override
    public void postDelete(TechServiceMarketInfos entity, PostDeleteContext<TechServiceMarketInfos> context) {
    }
}