package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MarketInfosListener.class)
@Table(name = "market_infos")
public class MarketInfos {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 情報区分 */
    @Column(name = "info_div")
    String infoDiv;

    /** アーティクルNo */
    @Column(name = "art_no")
    String artNo;

    /** セリアル */
    @Column(name = "serial")
    String serial;

    /** サイズ */
    @Column(name = "size")
    String size;

    /** パターン */
    @Column(name = "pattern")
    String pattern;

    /** 損傷部位 */
    @Column(name = "damaged_part")
    String damagedPart;

    /** 損傷タイプ */
    @Column(name = "damaged_type")
    String damagedType;

    /** 損傷名 */
    @Column(name = "damaged_name")
    String damagedName;

    /** 性能 */
    @Column(name = "performance")
    String performance;

    /** 使用状況 */
    @Column(name = "usages")
    String usages;

    /** 路面 */
    @Column(name = "road_surface")
    String roadSurface;

    /** 車種 */
    @Column(name = "car_type")
    String carType;

    /** 車体形状 */
    @Column(name = "car_body_shape")
    String carBodyShape;

    /** カーメーカー */
    @Column(name = "car_maker")
    String carMaker;

    /** 装着期間 */
    @Column(name = "mounting_term")
    Integer mountingTerm;

    /** 走行距離 */
    @Column(name = "mileage")
    Integer mileage;

    /** 走行距離単位 */
    @Column(name = "mileage_unit")
    String mileageUnit;

    /** 詳細 */
    @Column(name = "description")
    String description;

    /** マーク */
    @Column(name = "mark")
    boolean mark;

    /** 訪問ID */
    @Column(name = "call_id")
    String callId;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者（ユーザID） */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者（ユーザID） */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the infoDiv.
     * 
     * @return the infoDiv
     */
    public String getInfoDiv() {
        return infoDiv;
    }

    /** 
     * Sets the infoDiv.
     * 
     * @param infoDiv the infoDiv
     */
    public void setInfoDiv(String infoDiv) {
        this.infoDiv = infoDiv;
    }

    /** 
     * Returns the artNo.
     * 
     * @return the artNo
     */
    public String getArtNo() {
        return artNo;
    }

    /** 
     * Sets the artNo.
     * 
     * @param artNo the artNo
     */
    public void setArtNo(String artNo) {
        this.artNo = artNo;
    }

    /** 
     * Returns the serial.
     * 
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /** 
     * Sets the serial.
     * 
     * @param serial the serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /** 
     * Returns the size.
     * 
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /** 
     * Sets the size.
     * 
     * @param size the size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /** 
     * Returns the pattern.
     * 
     * @return the pattern
     */
    public String getPattern() {
        return pattern;
    }

    /** 
     * Sets the pattern.
     * 
     * @param pattern the pattern
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /** 
     * Returns the damagedPart.
     * 
     * @return the damagedPart
     */
    public String getDamagedPart() {
        return damagedPart;
    }

    /** 
     * Sets the damagedPart.
     * 
     * @param damagedPart the damagedPart
     */
    public void setDamagedPart(String damagedPart) {
        this.damagedPart = damagedPart;
    }

    /** 
     * Returns the damagedType.
     * 
     * @return the damagedType
     */
    public String getDamagedType() {
        return damagedType;
    }

    /** 
     * Sets the damagedType.
     * 
     * @param damagedType the damagedType
     */
    public void setDamagedType(String damagedType) {
        this.damagedType = damagedType;
    }

    /** 
     * Returns the damagedName.
     * 
     * @return the damagedName
     */
    public String getDamagedName() {
        return damagedName;
    }

    /** 
     * Sets the damagedName.
     * 
     * @param damagedName the damagedName
     */
    public void setDamagedName(String damagedName) {
        this.damagedName = damagedName;
    }

    /** 
     * Returns the performance.
     * 
     * @return the performance
     */
    public String getPerformance() {
        return performance;
    }

    /** 
     * Sets the performance.
     * 
     * @param performance the performance
     */
    public void setPerformance(String performance) {
        this.performance = performance;
    }

    /** 
     * Returns the usages.
     * 
     * @return the usages
     */
    public String getUsages() {
        return usages;
    }

    /** 
     * Sets the usages.
     * 
     * @param usages the usages
     */
    public void setUsages(String usages) {
        this.usages = usages;
    }

    /** 
     * Returns the roadSurface.
     * 
     * @return the roadSurface
     */
    public String getRoadSurface() {
        return roadSurface;
    }

    /** 
     * Sets the roadSurface.
     * 
     * @param roadSurface the roadSurface
     */
    public void setRoadSurface(String roadSurface) {
        this.roadSurface = roadSurface;
    }

    /** 
     * Returns the carType.
     * 
     * @return the carType
     */
    public String getCarType() {
        return carType;
    }

    /** 
     * Sets the carType.
     * 
     * @param carType the carType
     */
    public void setCarType(String carType) {
        this.carType = carType;
    }

    /** 
     * Returns the carBodyShape.
     * 
     * @return the carBodyShape
     */
    public String getCarBodyShape() {
        return carBodyShape;
    }

    /** 
     * Sets the carBodyShape.
     * 
     * @param carBodyShape the carBodyShape
     */
    public void setCarBodyShape(String carBodyShape) {
        this.carBodyShape = carBodyShape;
    }

    /** 
     * Returns the carMaker.
     * 
     * @return the carMaker
     */
    public String getCarMaker() {
        return carMaker;
    }

    /** 
     * Sets the carMaker.
     * 
     * @param carMaker the carMaker
     */
    public void setCarMaker(String carMaker) {
        this.carMaker = carMaker;
    }

    /** 
     * Returns the mountingTerm.
     * 
     * @return the mountingTerm
     */
    public Integer getMountingTerm() {
        return mountingTerm;
    }

    /** 
     * Sets the mountingTerm.
     * 
     * @param mountingTerm the mountingTerm
     */
    public void setMountingTerm(Integer mountingTerm) {
        this.mountingTerm = mountingTerm;
    }

    /** 
     * Returns the mileage.
     * 
     * @return the mileage
     */
    public Integer getMileage() {
        return mileage;
    }

    /** 
     * Sets the mileage.
     * 
     * @param mileage the mileage
     */
    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    /** 
     * Returns the mileageUnit.
     * 
     * @return the mileageUnit
     */
    public String getMileageUnit() {
        return mileageUnit;
    }

    /** 
     * Sets the mileageUnit.
     * 
     * @param mileageUnit the mileageUnit
     */
    public void setMileageUnit(String mileageUnit) {
        this.mileageUnit = mileageUnit;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the mark.
     * 
     * @return the mark
     */
    public boolean isMark() {
        return mark;
    }

    /** 
     * Sets the mark.
     * 
     * @param mark the mark
     */
    public void setMark(boolean mark) {
        this.mark = mark;
    }

    /** 
     * Returns the callId.
     * 
     * @return the callId
     */
    public String getCallId() {
        return callId;
    }

    /** 
     * Sets the callId.
     * 
     * @param callId the callId
     */
    public void setCallId(String callId) {
        this.callId = callId;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}