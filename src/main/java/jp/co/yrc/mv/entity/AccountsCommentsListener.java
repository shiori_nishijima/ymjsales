package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class AccountsCommentsListener implements EntityListener<AccountsComments> {

    @Override
    public void preInsert(AccountsComments entity, PreInsertContext<AccountsComments> context) {
    }

    @Override
    public void preUpdate(AccountsComments entity, PreUpdateContext<AccountsComments> context) {
    }

    @Override
    public void preDelete(AccountsComments entity, PreDeleteContext<AccountsComments> context) {
    }

    @Override
    public void postInsert(AccountsComments entity, PostInsertContext<AccountsComments> context) {
    }

    @Override
    public void postUpdate(AccountsComments entity, PostUpdateContext<AccountsComments> context) {
    }

    @Override
    public void postDelete(AccountsComments entity, PostDeleteContext<AccountsComments> context) {
    }
}