package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class AccountsListener implements EntityListener<Accounts> {

    @Override
    public void preInsert(Accounts entity, PreInsertContext<Accounts> context) {
    }

    @Override
    public void preUpdate(Accounts entity, PreUpdateContext<Accounts> context) {
    }

    @Override
    public void preDelete(Accounts entity, PreDeleteContext<Accounts> context) {
    }

    @Override
    public void postInsert(Accounts entity, PostInsertContext<Accounts> context) {
    }

    @Override
    public void postUpdate(Accounts entity, PostUpdateContext<Accounts> context) {
    }

    @Override
    public void postDelete(Accounts entity, PostDeleteContext<Accounts> context) {
    }
}