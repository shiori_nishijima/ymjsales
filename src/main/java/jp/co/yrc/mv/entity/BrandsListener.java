package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class BrandsListener implements EntityListener<Brands> {

    @Override
    public void preInsert(Brands entity, PreInsertContext<Brands> context) {
    }

    @Override
    public void preUpdate(Brands entity, PreUpdateContext<Brands> context) {
    }

    @Override
    public void preDelete(Brands entity, PreDeleteContext<Brands> context) {
    }

    @Override
    public void postInsert(Brands entity, PostInsertContext<Brands> context) {
    }

    @Override
    public void postUpdate(Brands entity, PostUpdateContext<Brands> context) {
    }

    @Override
    public void postDelete(Brands entity, PostDeleteContext<Brands> context) {
    }
}