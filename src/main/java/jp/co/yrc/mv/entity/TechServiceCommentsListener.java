package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCommentsListener implements EntityListener<TechServiceComments> {

    @Override
    public void preInsert(TechServiceComments entity, PreInsertContext<TechServiceComments> context) {
    }

    @Override
    public void preUpdate(TechServiceComments entity, PreUpdateContext<TechServiceComments> context) {
    }

    @Override
    public void preDelete(TechServiceComments entity, PreDeleteContext<TechServiceComments> context) {
    }

    @Override
    public void postInsert(TechServiceComments entity, PostInsertContext<TechServiceComments> context) {
    }

    @Override
    public void postUpdate(TechServiceComments entity, PostUpdateContext<TechServiceComments> context) {
    }

    @Override
    public void postDelete(TechServiceComments entity, PostDeleteContext<TechServiceComments> context) {
    }
}