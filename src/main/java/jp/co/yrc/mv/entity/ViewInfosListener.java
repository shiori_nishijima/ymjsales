package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ViewInfosListener implements EntityListener<ViewInfos> {

    @Override
    public void preInsert(ViewInfos entity, PreInsertContext<ViewInfos> context) {
    }

    @Override
    public void preUpdate(ViewInfos entity, PreUpdateContext<ViewInfos> context) {
    }

    @Override
    public void preDelete(ViewInfos entity, PreDeleteContext<ViewInfos> context) {
    }

    @Override
    public void postInsert(ViewInfos entity, PostInsertContext<ViewInfos> context) {
    }

    @Override
    public void postUpdate(ViewInfos entity, PostUpdateContext<ViewInfos> context) {
    }

    @Override
    public void postDelete(ViewInfos entity, PostDeleteContext<ViewInfos> context) {
    }
}