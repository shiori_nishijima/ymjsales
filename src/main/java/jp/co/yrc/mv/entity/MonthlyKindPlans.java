package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyKindPlansListener.class)
@Table(name = "monthly_kind_plans")
public class MonthlyKindPlans {

    /**  */
    @Id
    @Column(name = "year")
    Integer year;

    /**  */
    @Id
    @Column(name = "month")
    Integer month;

    /**  */
    @Id
    @Column(name = "user_id")
    String userId;

    /**  */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /**  */
    @Column(name = "pcr_summer_sales")
    BigDecimal pcrSummerSales;

    /**  */
    @Column(name = "pcr_summer_margin")
    BigDecimal pcrSummerMargin;

    /**  */
    @Column(name = "pcr_summer_hq_margin")
    BigDecimal pcrSummerHqMargin;

    /**  */
    @Column(name = "pcr_summer_number")
    BigDecimal pcrSummerNumber;

    /**  */
    @Column(name = "pcr_pure_snow_sales")
    BigDecimal pcrPureSnowSales;

    /**  */
    @Column(name = "pcr_pure_snow_margin")
    BigDecimal pcrPureSnowMargin;

    /**  */
    @Column(name = "pcr_pure_snow_hq_margin")
    BigDecimal pcrPureSnowHqMargin;

    /**  */
    @Column(name = "pcr_pure_snow_number")
    BigDecimal pcrPureSnowNumber;

    /**  */
    @Column(name = "van_summer_sales")
    BigDecimal vanSummerSales;

    /**  */
    @Column(name = "van_summer_margin")
    BigDecimal vanSummerMargin;

    /**  */
    @Column(name = "van_summer_hq_margin")
    BigDecimal vanSummerHqMargin;

    /**  */
    @Column(name = "van_summer_number")
    BigDecimal vanSummerNumber;

    /**  */
    @Column(name = "van_pure_snow_sales")
    BigDecimal vanPureSnowSales;

    /**  */
    @Column(name = "van_pure_snow_margin")
    BigDecimal vanPureSnowMargin;

    /**  */
    @Column(name = "van_pure_snow_hq_margin")
    BigDecimal vanPureSnowHqMargin;

    /**  */
    @Column(name = "van_pure_snow_number")
    BigDecimal vanPureSnowNumber;

    /**  */
    @Column(name = "pcb_summer_sales")
    BigDecimal pcbSummerSales;

    /**  */
    @Column(name = "pcb_summer_margin")
    BigDecimal pcbSummerMargin;

    /**  */
    @Column(name = "pcb_summer_hq_margin")
    BigDecimal pcbSummerHqMargin;

    /**  */
    @Column(name = "pcb_summer_number")
    BigDecimal pcbSummerNumber;

    /**  */
    @Column(name = "pcb_pure_snow_sales")
    BigDecimal pcbPureSnowSales;

    /**  */
    @Column(name = "pcb_pure_snow_margin")
    BigDecimal pcbPureSnowMargin;

    /**  */
    @Column(name = "pcb_pure_snow_hq_margin")
    BigDecimal pcbPureSnowHqMargin;

    /**  */
    @Column(name = "pcb_pure_snow_number")
    BigDecimal pcbPureSnowNumber;

    /**  */
    @Column(name = "lt_summer_sales")
    BigDecimal ltSummerSales;

    /**  */
    @Column(name = "lt_summer_margin")
    BigDecimal ltSummerMargin;

    /**  */
    @Column(name = "lt_summer_hq_margin")
    BigDecimal ltSummerHqMargin;

    /**  */
    @Column(name = "lt_summer_number")
    BigDecimal ltSummerNumber;

    /**  */
    @Column(name = "lt_snow_sales")
    BigDecimal ltSnowSales;

    /**  */
    @Column(name = "lt_snow_margin")
    BigDecimal ltSnowMargin;

    /**  */
    @Column(name = "lt_snow_hq_margin")
    BigDecimal ltSnowHqMargin;

    /**  */
    @Column(name = "lt_snow_number")
    BigDecimal ltSnowNumber;

    /**  */
    @Column(name = "tb_summer_sales")
    BigDecimal tbSummerSales;

    /**  */
    @Column(name = "tb_summer_margin")
    BigDecimal tbSummerMargin;

    /**  */
    @Column(name = "tb_summer_hq_margin")
    BigDecimal tbSummerHqMargin;

    /**  */
    @Column(name = "tb_summer_number")
    BigDecimal tbSummerNumber;

    /**  */
    @Column(name = "tb_snow_sales")
    BigDecimal tbSnowSales;

    /**  */
    @Column(name = "tb_snow_margin")
    BigDecimal tbSnowMargin;

    /**  */
    @Column(name = "tb_snow_hq_margin")
    BigDecimal tbSnowHqMargin;

    /**  */
    @Column(name = "tb_snow_number")
    BigDecimal tbSnowNumber;

    /**  */
    @Column(name = "or_id_sales")
    BigDecimal orIdSales;

    /**  */
    @Column(name = "or_id_margin")
    BigDecimal orIdMargin;

    /**  */
    @Column(name = "or_id_hq_margin")
    BigDecimal orIdHqMargin;

    /**  */
    @Column(name = "or_id_number")
    BigDecimal orIdNumber;

    /**  */
    @Column(name = "tifu_sales")
    BigDecimal tifuSales;

    /**  */
    @Column(name = "tifu_margin")
    BigDecimal tifuMargin;

    /**  */
    @Column(name = "tifu_hq_margin")
    BigDecimal tifuHqMargin;

    /**  */
    @Column(name = "tifu_number")
    BigDecimal tifuNumber;

    /**  */
    @Column(name = "other_sales")
    BigDecimal otherSales;

    /**  */
    @Column(name = "other_margin")
    BigDecimal otherMargin;

    /**  */
    @Column(name = "other_hq_margin")
    BigDecimal otherHqMargin;

    /**  */
    @Column(name = "other_number")
    BigDecimal otherNumber;

    /**  */
    @Column(name = "work_sales")
    BigDecimal workSales;

    /**  */
    @Column(name = "work_margin")
    BigDecimal workMargin;

    /**  */
    @Column(name = "work_hq_margin")
    BigDecimal workHqMargin;

    /**  */
    @Column(name = "work_number")
    BigDecimal workNumber;

    /**  */
    @Column(name = "retread_hq_margin")
    BigDecimal retreadHqMargin;

    /**  */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /**  */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /**  */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /**  */
    @Column(name = "created_by")
    String createdBy;

    /**  */
    @Column(name = "retread_sales")
    BigDecimal retreadSales;

    /**  */
    @Column(name = "retread_margin")
    BigDecimal retreadMargin;

    /**  */
    @Column(name = "retread_number")
    BigDecimal retreadNumber;

    /**  */
    @Column(name = "retread_lt_summer_sales")
    BigDecimal retreadLtSummerSales;

    /**  */
    @Column(name = "retread_lt_summer_margin")
    BigDecimal retreadLtSummerMargin;

    /**  */
    @Column(name = "retread_lt_summer_hq_margin")
    BigDecimal retreadLtSummerHqMargin;

    /**  */
    @Column(name = "retread_lt_summer_number")
    BigDecimal retreadLtSummerNumber;

    /**  */
    @Column(name = "retread_tb_summer_sales")
    BigDecimal retreadTbSummerSales;

    /**  */
    @Column(name = "retread_tb_summer_margin")
    BigDecimal retreadTbSummerMargin;

    /**  */
    @Column(name = "retread_tb_summer_hq_margin")
    BigDecimal retreadTbSummerHqMargin;

    /**  */
    @Column(name = "retread_tb_summer_number")
    BigDecimal retreadTbSummerNumber;

    /**  */
    @Column(name = "retread_special_tire_summer_sales")
    BigDecimal retreadSpecialTireSummerSales;

    /**  */
    @Column(name = "retread_special_tire_summer_margin")
    BigDecimal retreadSpecialTireSummerMargin;

    /**  */
    @Column(name = "retread_special_tire_summer_hq_margin")
    BigDecimal retreadSpecialTireSummerHqMargin;

    /**  */
    @Column(name = "retread_special_tire_summer_number")
    BigDecimal retreadSpecialTireSummerNumber;

    /**  */
    @Column(name = "retread_lt_snow_sales")
    BigDecimal retreadLtSnowSales;

    /**  */
    @Column(name = "retread_lt_snow_margin")
    BigDecimal retreadLtSnowMargin;

    /**  */
    @Column(name = "retread_lt_snow_hq_margin")
    BigDecimal retreadLtSnowHqMargin;

    /**  */
    @Column(name = "retread_lt_snow_number")
    BigDecimal retreadLtSnowNumber;

    /**  */
    @Column(name = "retread_tb_snow_sales")
    BigDecimal retreadTbSnowSales;

    /**  */
    @Column(name = "retread_tb_snow_margin")
    BigDecimal retreadTbSnowMargin;

    /**  */
    @Column(name = "retread_tb_snow_hq_margin")
    BigDecimal retreadTbSnowHqMargin;

    /**  */
    @Column(name = "retread_tb_snow_number")
    BigDecimal retreadTbSnowNumber;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the pcrSummerSales.
     * 
     * @return the pcrSummerSales
     */
    public BigDecimal getPcrSummerSales() {
        return pcrSummerSales;
    }

    /** 
     * Sets the pcrSummerSales.
     * 
     * @param pcrSummerSales the pcrSummerSales
     */
    public void setPcrSummerSales(BigDecimal pcrSummerSales) {
        this.pcrSummerSales = pcrSummerSales;
    }

    /** 
     * Returns the pcrSummerMargin.
     * 
     * @return the pcrSummerMargin
     */
    public BigDecimal getPcrSummerMargin() {
        return pcrSummerMargin;
    }

    /** 
     * Sets the pcrSummerMargin.
     * 
     * @param pcrSummerMargin the pcrSummerMargin
     */
    public void setPcrSummerMargin(BigDecimal pcrSummerMargin) {
        this.pcrSummerMargin = pcrSummerMargin;
    }

    /** 
     * Returns the pcrSummerHqMargin.
     * 
     * @return the pcrSummerHqMargin
     */
    public BigDecimal getPcrSummerHqMargin() {
        return pcrSummerHqMargin;
    }

    /** 
     * Sets the pcrSummerHqMargin.
     * 
     * @param pcrSummerHqMargin the pcrSummerHqMargin
     */
    public void setPcrSummerHqMargin(BigDecimal pcrSummerHqMargin) {
        this.pcrSummerHqMargin = pcrSummerHqMargin;
    }

    /** 
     * Returns the pcrSummerNumber.
     * 
     * @return the pcrSummerNumber
     */
    public BigDecimal getPcrSummerNumber() {
        return pcrSummerNumber;
    }

    /** 
     * Sets the pcrSummerNumber.
     * 
     * @param pcrSummerNumber the pcrSummerNumber
     */
    public void setPcrSummerNumber(BigDecimal pcrSummerNumber) {
        this.pcrSummerNumber = pcrSummerNumber;
    }

    /** 
     * Returns the pcrPureSnowSales.
     * 
     * @return the pcrPureSnowSales
     */
    public BigDecimal getPcrPureSnowSales() {
        return pcrPureSnowSales;
    }

    /** 
     * Sets the pcrPureSnowSales.
     * 
     * @param pcrPureSnowSales the pcrPureSnowSales
     */
    public void setPcrPureSnowSales(BigDecimal pcrPureSnowSales) {
        this.pcrPureSnowSales = pcrPureSnowSales;
    }

    /** 
     * Returns the pcrPureSnowMargin.
     * 
     * @return the pcrPureSnowMargin
     */
    public BigDecimal getPcrPureSnowMargin() {
        return pcrPureSnowMargin;
    }

    /** 
     * Sets the pcrPureSnowMargin.
     * 
     * @param pcrPureSnowMargin the pcrPureSnowMargin
     */
    public void setPcrPureSnowMargin(BigDecimal pcrPureSnowMargin) {
        this.pcrPureSnowMargin = pcrPureSnowMargin;
    }

    /** 
     * Returns the pcrPureSnowHqMargin.
     * 
     * @return the pcrPureSnowHqMargin
     */
    public BigDecimal getPcrPureSnowHqMargin() {
        return pcrPureSnowHqMargin;
    }

    /** 
     * Sets the pcrPureSnowHqMargin.
     * 
     * @param pcrPureSnowHqMargin the pcrPureSnowHqMargin
     */
    public void setPcrPureSnowHqMargin(BigDecimal pcrPureSnowHqMargin) {
        this.pcrPureSnowHqMargin = pcrPureSnowHqMargin;
    }

    /** 
     * Returns the pcrPureSnowNumber.
     * 
     * @return the pcrPureSnowNumber
     */
    public BigDecimal getPcrPureSnowNumber() {
        return pcrPureSnowNumber;
    }

    /** 
     * Sets the pcrPureSnowNumber.
     * 
     * @param pcrPureSnowNumber the pcrPureSnowNumber
     */
    public void setPcrPureSnowNumber(BigDecimal pcrPureSnowNumber) {
        this.pcrPureSnowNumber = pcrPureSnowNumber;
    }

    /** 
     * Returns the vanSummerSales.
     * 
     * @return the vanSummerSales
     */
    public BigDecimal getVanSummerSales() {
        return vanSummerSales;
    }

    /** 
     * Sets the vanSummerSales.
     * 
     * @param vanSummerSales the vanSummerSales
     */
    public void setVanSummerSales(BigDecimal vanSummerSales) {
        this.vanSummerSales = vanSummerSales;
    }

    /** 
     * Returns the vanSummerMargin.
     * 
     * @return the vanSummerMargin
     */
    public BigDecimal getVanSummerMargin() {
        return vanSummerMargin;
    }

    /** 
     * Sets the vanSummerMargin.
     * 
     * @param vanSummerMargin the vanSummerMargin
     */
    public void setVanSummerMargin(BigDecimal vanSummerMargin) {
        this.vanSummerMargin = vanSummerMargin;
    }

    /** 
     * Returns the vanSummerHqMargin.
     * 
     * @return the vanSummerHqMargin
     */
    public BigDecimal getVanSummerHqMargin() {
        return vanSummerHqMargin;
    }

    /** 
     * Sets the vanSummerHqMargin.
     * 
     * @param vanSummerHqMargin the vanSummerHqMargin
     */
    public void setVanSummerHqMargin(BigDecimal vanSummerHqMargin) {
        this.vanSummerHqMargin = vanSummerHqMargin;
    }

    /** 
     * Returns the vanSummerNumber.
     * 
     * @return the vanSummerNumber
     */
    public BigDecimal getVanSummerNumber() {
        return vanSummerNumber;
    }

    /** 
     * Sets the vanSummerNumber.
     * 
     * @param vanSummerNumber the vanSummerNumber
     */
    public void setVanSummerNumber(BigDecimal vanSummerNumber) {
        this.vanSummerNumber = vanSummerNumber;
    }

    /** 
     * Returns the vanPureSnowSales.
     * 
     * @return the vanPureSnowSales
     */
    public BigDecimal getVanPureSnowSales() {
        return vanPureSnowSales;
    }

    /** 
     * Sets the vanPureSnowSales.
     * 
     * @param vanPureSnowSales the vanPureSnowSales
     */
    public void setVanPureSnowSales(BigDecimal vanPureSnowSales) {
        this.vanPureSnowSales = vanPureSnowSales;
    }

    /** 
     * Returns the vanPureSnowMargin.
     * 
     * @return the vanPureSnowMargin
     */
    public BigDecimal getVanPureSnowMargin() {
        return vanPureSnowMargin;
    }

    /** 
     * Sets the vanPureSnowMargin.
     * 
     * @param vanPureSnowMargin the vanPureSnowMargin
     */
    public void setVanPureSnowMargin(BigDecimal vanPureSnowMargin) {
        this.vanPureSnowMargin = vanPureSnowMargin;
    }

    /** 
     * Returns the vanPureSnowHqMargin.
     * 
     * @return the vanPureSnowHqMargin
     */
    public BigDecimal getVanPureSnowHqMargin() {
        return vanPureSnowHqMargin;
    }

    /** 
     * Sets the vanPureSnowHqMargin.
     * 
     * @param vanPureSnowHqMargin the vanPureSnowHqMargin
     */
    public void setVanPureSnowHqMargin(BigDecimal vanPureSnowHqMargin) {
        this.vanPureSnowHqMargin = vanPureSnowHqMargin;
    }

    /** 
     * Returns the vanPureSnowNumber.
     * 
     * @return the vanPureSnowNumber
     */
    public BigDecimal getVanPureSnowNumber() {
        return vanPureSnowNumber;
    }

    /** 
     * Sets the vanPureSnowNumber.
     * 
     * @param vanPureSnowNumber the vanPureSnowNumber
     */
    public void setVanPureSnowNumber(BigDecimal vanPureSnowNumber) {
        this.vanPureSnowNumber = vanPureSnowNumber;
    }

    /** 
     * Returns the pcbSummerSales.
     * 
     * @return the pcbSummerSales
     */
    public BigDecimal getPcbSummerSales() {
        return pcbSummerSales;
    }

    /** 
     * Sets the pcbSummerSales.
     * 
     * @param pcbSummerSales the pcbSummerSales
     */
    public void setPcbSummerSales(BigDecimal pcbSummerSales) {
        this.pcbSummerSales = pcbSummerSales;
    }

    /** 
     * Returns the pcbSummerMargin.
     * 
     * @return the pcbSummerMargin
     */
    public BigDecimal getPcbSummerMargin() {
        return pcbSummerMargin;
    }

    /** 
     * Sets the pcbSummerMargin.
     * 
     * @param pcbSummerMargin the pcbSummerMargin
     */
    public void setPcbSummerMargin(BigDecimal pcbSummerMargin) {
        this.pcbSummerMargin = pcbSummerMargin;
    }

    /** 
     * Returns the pcbSummerHqMargin.
     * 
     * @return the pcbSummerHqMargin
     */
    public BigDecimal getPcbSummerHqMargin() {
        return pcbSummerHqMargin;
    }

    /** 
     * Sets the pcbSummerHqMargin.
     * 
     * @param pcbSummerHqMargin the pcbSummerHqMargin
     */
    public void setPcbSummerHqMargin(BigDecimal pcbSummerHqMargin) {
        this.pcbSummerHqMargin = pcbSummerHqMargin;
    }

    /** 
     * Returns the pcbSummerNumber.
     * 
     * @return the pcbSummerNumber
     */
    public BigDecimal getPcbSummerNumber() {
        return pcbSummerNumber;
    }

    /** 
     * Sets the pcbSummerNumber.
     * 
     * @param pcbSummerNumber the pcbSummerNumber
     */
    public void setPcbSummerNumber(BigDecimal pcbSummerNumber) {
        this.pcbSummerNumber = pcbSummerNumber;
    }

    /** 
     * Returns the pcbPureSnowSales.
     * 
     * @return the pcbPureSnowSales
     */
    public BigDecimal getPcbPureSnowSales() {
        return pcbPureSnowSales;
    }

    /** 
     * Sets the pcbPureSnowSales.
     * 
     * @param pcbPureSnowSales the pcbPureSnowSales
     */
    public void setPcbPureSnowSales(BigDecimal pcbPureSnowSales) {
        this.pcbPureSnowSales = pcbPureSnowSales;
    }

    /** 
     * Returns the pcbPureSnowMargin.
     * 
     * @return the pcbPureSnowMargin
     */
    public BigDecimal getPcbPureSnowMargin() {
        return pcbPureSnowMargin;
    }

    /** 
     * Sets the pcbPureSnowMargin.
     * 
     * @param pcbPureSnowMargin the pcbPureSnowMargin
     */
    public void setPcbPureSnowMargin(BigDecimal pcbPureSnowMargin) {
        this.pcbPureSnowMargin = pcbPureSnowMargin;
    }

    /** 
     * Returns the pcbPureSnowHqMargin.
     * 
     * @return the pcbPureSnowHqMargin
     */
    public BigDecimal getPcbPureSnowHqMargin() {
        return pcbPureSnowHqMargin;
    }

    /** 
     * Sets the pcbPureSnowHqMargin.
     * 
     * @param pcbPureSnowHqMargin the pcbPureSnowHqMargin
     */
    public void setPcbPureSnowHqMargin(BigDecimal pcbPureSnowHqMargin) {
        this.pcbPureSnowHqMargin = pcbPureSnowHqMargin;
    }

    /** 
     * Returns the pcbPureSnowNumber.
     * 
     * @return the pcbPureSnowNumber
     */
    public BigDecimal getPcbPureSnowNumber() {
        return pcbPureSnowNumber;
    }

    /** 
     * Sets the pcbPureSnowNumber.
     * 
     * @param pcbPureSnowNumber the pcbPureSnowNumber
     */
    public void setPcbPureSnowNumber(BigDecimal pcbPureSnowNumber) {
        this.pcbPureSnowNumber = pcbPureSnowNumber;
    }

    /** 
     * Returns the ltSummerSales.
     * 
     * @return the ltSummerSales
     */
    public BigDecimal getLtSummerSales() {
        return ltSummerSales;
    }

    /** 
     * Sets the ltSummerSales.
     * 
     * @param ltSummerSales the ltSummerSales
     */
    public void setLtSummerSales(BigDecimal ltSummerSales) {
        this.ltSummerSales = ltSummerSales;
    }

    /** 
     * Returns the ltSummerMargin.
     * 
     * @return the ltSummerMargin
     */
    public BigDecimal getLtSummerMargin() {
        return ltSummerMargin;
    }

    /** 
     * Sets the ltSummerMargin.
     * 
     * @param ltSummerMargin the ltSummerMargin
     */
    public void setLtSummerMargin(BigDecimal ltSummerMargin) {
        this.ltSummerMargin = ltSummerMargin;
    }

    /** 
     * Returns the ltSummerHqMargin.
     * 
     * @return the ltSummerHqMargin
     */
    public BigDecimal getLtSummerHqMargin() {
        return ltSummerHqMargin;
    }

    /** 
     * Sets the ltSummerHqMargin.
     * 
     * @param ltSummerHqMargin the ltSummerHqMargin
     */
    public void setLtSummerHqMargin(BigDecimal ltSummerHqMargin) {
        this.ltSummerHqMargin = ltSummerHqMargin;
    }

    /** 
     * Returns the ltSummerNumber.
     * 
     * @return the ltSummerNumber
     */
    public BigDecimal getLtSummerNumber() {
        return ltSummerNumber;
    }

    /** 
     * Sets the ltSummerNumber.
     * 
     * @param ltSummerNumber the ltSummerNumber
     */
    public void setLtSummerNumber(BigDecimal ltSummerNumber) {
        this.ltSummerNumber = ltSummerNumber;
    }

    /** 
     * Returns the ltSnowSales.
     * 
     * @return the ltSnowSales
     */
    public BigDecimal getLtSnowSales() {
        return ltSnowSales;
    }

    /** 
     * Sets the ltSnowSales.
     * 
     * @param ltSnowSales the ltSnowSales
     */
    public void setLtSnowSales(BigDecimal ltSnowSales) {
        this.ltSnowSales = ltSnowSales;
    }

    /** 
     * Returns the ltSnowMargin.
     * 
     * @return the ltSnowMargin
     */
    public BigDecimal getLtSnowMargin() {
        return ltSnowMargin;
    }

    /** 
     * Sets the ltSnowMargin.
     * 
     * @param ltSnowMargin the ltSnowMargin
     */
    public void setLtSnowMargin(BigDecimal ltSnowMargin) {
        this.ltSnowMargin = ltSnowMargin;
    }

    /** 
     * Returns the ltSnowHqMargin.
     * 
     * @return the ltSnowHqMargin
     */
    public BigDecimal getLtSnowHqMargin() {
        return ltSnowHqMargin;
    }

    /** 
     * Sets the ltSnowHqMargin.
     * 
     * @param ltSnowHqMargin the ltSnowHqMargin
     */
    public void setLtSnowHqMargin(BigDecimal ltSnowHqMargin) {
        this.ltSnowHqMargin = ltSnowHqMargin;
    }

    /** 
     * Returns the ltSnowNumber.
     * 
     * @return the ltSnowNumber
     */
    public BigDecimal getLtSnowNumber() {
        return ltSnowNumber;
    }

    /** 
     * Sets the ltSnowNumber.
     * 
     * @param ltSnowNumber the ltSnowNumber
     */
    public void setLtSnowNumber(BigDecimal ltSnowNumber) {
        this.ltSnowNumber = ltSnowNumber;
    }

    /** 
     * Returns the tbSummerSales.
     * 
     * @return the tbSummerSales
     */
    public BigDecimal getTbSummerSales() {
        return tbSummerSales;
    }

    /** 
     * Sets the tbSummerSales.
     * 
     * @param tbSummerSales the tbSummerSales
     */
    public void setTbSummerSales(BigDecimal tbSummerSales) {
        this.tbSummerSales = tbSummerSales;
    }

    /** 
     * Returns the tbSummerMargin.
     * 
     * @return the tbSummerMargin
     */
    public BigDecimal getTbSummerMargin() {
        return tbSummerMargin;
    }

    /** 
     * Sets the tbSummerMargin.
     * 
     * @param tbSummerMargin the tbSummerMargin
     */
    public void setTbSummerMargin(BigDecimal tbSummerMargin) {
        this.tbSummerMargin = tbSummerMargin;
    }

    /** 
     * Returns the tbSummerHqMargin.
     * 
     * @return the tbSummerHqMargin
     */
    public BigDecimal getTbSummerHqMargin() {
        return tbSummerHqMargin;
    }

    /** 
     * Sets the tbSummerHqMargin.
     * 
     * @param tbSummerHqMargin the tbSummerHqMargin
     */
    public void setTbSummerHqMargin(BigDecimal tbSummerHqMargin) {
        this.tbSummerHqMargin = tbSummerHqMargin;
    }

    /** 
     * Returns the tbSummerNumber.
     * 
     * @return the tbSummerNumber
     */
    public BigDecimal getTbSummerNumber() {
        return tbSummerNumber;
    }

    /** 
     * Sets the tbSummerNumber.
     * 
     * @param tbSummerNumber the tbSummerNumber
     */
    public void setTbSummerNumber(BigDecimal tbSummerNumber) {
        this.tbSummerNumber = tbSummerNumber;
    }

    /** 
     * Returns the tbSnowSales.
     * 
     * @return the tbSnowSales
     */
    public BigDecimal getTbSnowSales() {
        return tbSnowSales;
    }

    /** 
     * Sets the tbSnowSales.
     * 
     * @param tbSnowSales the tbSnowSales
     */
    public void setTbSnowSales(BigDecimal tbSnowSales) {
        this.tbSnowSales = tbSnowSales;
    }

    /** 
     * Returns the tbSnowMargin.
     * 
     * @return the tbSnowMargin
     */
    public BigDecimal getTbSnowMargin() {
        return tbSnowMargin;
    }

    /** 
     * Sets the tbSnowMargin.
     * 
     * @param tbSnowMargin the tbSnowMargin
     */
    public void setTbSnowMargin(BigDecimal tbSnowMargin) {
        this.tbSnowMargin = tbSnowMargin;
    }

    /** 
     * Returns the tbSnowHqMargin.
     * 
     * @return the tbSnowHqMargin
     */
    public BigDecimal getTbSnowHqMargin() {
        return tbSnowHqMargin;
    }

    /** 
     * Sets the tbSnowHqMargin.
     * 
     * @param tbSnowHqMargin the tbSnowHqMargin
     */
    public void setTbSnowHqMargin(BigDecimal tbSnowHqMargin) {
        this.tbSnowHqMargin = tbSnowHqMargin;
    }

    /** 
     * Returns the tbSnowNumber.
     * 
     * @return the tbSnowNumber
     */
    public BigDecimal getTbSnowNumber() {
        return tbSnowNumber;
    }

    /** 
     * Sets the tbSnowNumber.
     * 
     * @param tbSnowNumber the tbSnowNumber
     */
    public void setTbSnowNumber(BigDecimal tbSnowNumber) {
        this.tbSnowNumber = tbSnowNumber;
    }

    /** 
     * Returns the orIdSales.
     * 
     * @return the orIdSales
     */
    public BigDecimal getOrIdSales() {
        return orIdSales;
    }

    /** 
     * Sets the orIdSales.
     * 
     * @param orIdSales the orIdSales
     */
    public void setOrIdSales(BigDecimal orIdSales) {
        this.orIdSales = orIdSales;
    }

    /** 
     * Returns the orIdMargin.
     * 
     * @return the orIdMargin
     */
    public BigDecimal getOrIdMargin() {
        return orIdMargin;
    }

    /** 
     * Sets the orIdMargin.
     * 
     * @param orIdMargin the orIdMargin
     */
    public void setOrIdMargin(BigDecimal orIdMargin) {
        this.orIdMargin = orIdMargin;
    }

    /** 
     * Returns the orIdHqMargin.
     * 
     * @return the orIdHqMargin
     */
    public BigDecimal getOrIdHqMargin() {
        return orIdHqMargin;
    }

    /** 
     * Sets the orIdHqMargin.
     * 
     * @param orIdHqMargin the orIdHqMargin
     */
    public void setOrIdHqMargin(BigDecimal orIdHqMargin) {
        this.orIdHqMargin = orIdHqMargin;
    }

    /** 
     * Returns the orIdNumber.
     * 
     * @return the orIdNumber
     */
    public BigDecimal getOrIdNumber() {
        return orIdNumber;
    }

    /** 
     * Sets the orIdNumber.
     * 
     * @param orIdNumber the orIdNumber
     */
    public void setOrIdNumber(BigDecimal orIdNumber) {
        this.orIdNumber = orIdNumber;
    }

    /** 
     * Returns the tifuSales.
     * 
     * @return the tifuSales
     */
    public BigDecimal getTifuSales() {
        return tifuSales;
    }

    /** 
     * Sets the tifuSales.
     * 
     * @param tifuSales the tifuSales
     */
    public void setTifuSales(BigDecimal tifuSales) {
        this.tifuSales = tifuSales;
    }

    /** 
     * Returns the tifuMargin.
     * 
     * @return the tifuMargin
     */
    public BigDecimal getTifuMargin() {
        return tifuMargin;
    }

    /** 
     * Sets the tifuMargin.
     * 
     * @param tifuMargin the tifuMargin
     */
    public void setTifuMargin(BigDecimal tifuMargin) {
        this.tifuMargin = tifuMargin;
    }

    /** 
     * Returns the tifuHqMargin.
     * 
     * @return the tifuHqMargin
     */
    public BigDecimal getTifuHqMargin() {
        return tifuHqMargin;
    }

    /** 
     * Sets the tifuHqMargin.
     * 
     * @param tifuHqMargin the tifuHqMargin
     */
    public void setTifuHqMargin(BigDecimal tifuHqMargin) {
        this.tifuHqMargin = tifuHqMargin;
    }

    /** 
     * Returns the tifuNumber.
     * 
     * @return the tifuNumber
     */
    public BigDecimal getTifuNumber() {
        return tifuNumber;
    }

    /** 
     * Sets the tifuNumber.
     * 
     * @param tifuNumber the tifuNumber
     */
    public void setTifuNumber(BigDecimal tifuNumber) {
        this.tifuNumber = tifuNumber;
    }

    /** 
     * Returns the otherSales.
     * 
     * @return the otherSales
     */
    public BigDecimal getOtherSales() {
        return otherSales;
    }

    /** 
     * Sets the otherSales.
     * 
     * @param otherSales the otherSales
     */
    public void setOtherSales(BigDecimal otherSales) {
        this.otherSales = otherSales;
    }

    /** 
     * Returns the otherMargin.
     * 
     * @return the otherMargin
     */
    public BigDecimal getOtherMargin() {
        return otherMargin;
    }

    /** 
     * Sets the otherMargin.
     * 
     * @param otherMargin the otherMargin
     */
    public void setOtherMargin(BigDecimal otherMargin) {
        this.otherMargin = otherMargin;
    }

    /** 
     * Returns the otherHqMargin.
     * 
     * @return the otherHqMargin
     */
    public BigDecimal getOtherHqMargin() {
        return otherHqMargin;
    }

    /** 
     * Sets the otherHqMargin.
     * 
     * @param otherHqMargin the otherHqMargin
     */
    public void setOtherHqMargin(BigDecimal otherHqMargin) {
        this.otherHqMargin = otherHqMargin;
    }

    /** 
     * Returns the otherNumber.
     * 
     * @return the otherNumber
     */
    public BigDecimal getOtherNumber() {
        return otherNumber;
    }

    /** 
     * Sets the otherNumber.
     * 
     * @param otherNumber the otherNumber
     */
    public void setOtherNumber(BigDecimal otherNumber) {
        this.otherNumber = otherNumber;
    }

    /** 
     * Returns the workSales.
     * 
     * @return the workSales
     */
    public BigDecimal getWorkSales() {
        return workSales;
    }

    /** 
     * Sets the workSales.
     * 
     * @param workSales the workSales
     */
    public void setWorkSales(BigDecimal workSales) {
        this.workSales = workSales;
    }

    /** 
     * Returns the workMargin.
     * 
     * @return the workMargin
     */
    public BigDecimal getWorkMargin() {
        return workMargin;
    }

    /** 
     * Sets the workMargin.
     * 
     * @param workMargin the workMargin
     */
    public void setWorkMargin(BigDecimal workMargin) {
        this.workMargin = workMargin;
    }

    /** 
     * Returns the workHqMargin.
     * 
     * @return the workHqMargin
     */
    public BigDecimal getWorkHqMargin() {
        return workHqMargin;
    }

    /** 
     * Sets the workHqMargin.
     * 
     * @param workHqMargin the workHqMargin
     */
    public void setWorkHqMargin(BigDecimal workHqMargin) {
        this.workHqMargin = workHqMargin;
    }

    /** 
     * Returns the workNumber.
     * 
     * @return the workNumber
     */
    public BigDecimal getWorkNumber() {
        return workNumber;
    }

    /** 
     * Sets the workNumber.
     * 
     * @param workNumber the workNumber
     */
    public void setWorkNumber(BigDecimal workNumber) {
        this.workNumber = workNumber;
    }

    /** 
     * Returns the retreadHqMargin.
     * 
     * @return the retreadHqMargin
     */
    public BigDecimal getRetreadHqMargin() {
        return retreadHqMargin;
    }

    /** 
     * Sets the retreadHqMargin.
     * 
     * @param retreadHqMargin the retreadHqMargin
     */
    public void setRetreadHqMargin(BigDecimal retreadHqMargin) {
        this.retreadHqMargin = retreadHqMargin;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the retreadSales.
     * 
     * @return the retreadSales
     */
    public BigDecimal getRetreadSales() {
        return retreadSales;
    }

    /** 
     * Sets the retreadSales.
     * 
     * @param retreadSales the retreadSales
     */
    public void setRetreadSales(BigDecimal retreadSales) {
        this.retreadSales = retreadSales;
    }

    /** 
     * Returns the retreadMargin.
     * 
     * @return the retreadMargin
     */
    public BigDecimal getRetreadMargin() {
        return retreadMargin;
    }

    /** 
     * Sets the retreadMargin.
     * 
     * @param retreadMargin the retreadMargin
     */
    public void setRetreadMargin(BigDecimal retreadMargin) {
        this.retreadMargin = retreadMargin;
    }

    /** 
     * Returns the retreadNumber.
     * 
     * @return the retreadNumber
     */
    public BigDecimal getRetreadNumber() {
        return retreadNumber;
    }

    /** 
     * Sets the retreadNumber.
     * 
     * @param retreadNumber the retreadNumber
     */
    public void setRetreadNumber(BigDecimal retreadNumber) {
        this.retreadNumber = retreadNumber;
    }

    /** 
     * Returns the retreadLtSummerSales.
     * 
     * @return the retreadLtSummerSales
     */
    public BigDecimal getRetreadLtSummerSales() {
        return retreadLtSummerSales;
    }

    /** 
     * Sets the retreadLtSummerSales.
     * 
     * @param retreadLtSummerSales the retreadLtSummerSales
     */
    public void setRetreadLtSummerSales(BigDecimal retreadLtSummerSales) {
        this.retreadLtSummerSales = retreadLtSummerSales;
    }

    /** 
     * Returns the retreadLtSummerMargin.
     * 
     * @return the retreadLtSummerMargin
     */
    public BigDecimal getRetreadLtSummerMargin() {
        return retreadLtSummerMargin;
    }

    /** 
     * Sets the retreadLtSummerMargin.
     * 
     * @param retreadLtSummerMargin the retreadLtSummerMargin
     */
    public void setRetreadLtSummerMargin(BigDecimal retreadLtSummerMargin) {
        this.retreadLtSummerMargin = retreadLtSummerMargin;
    }

    /** 
     * Returns the retreadLtSummerHqMargin.
     * 
     * @return the retreadLtSummerHqMargin
     */
    public BigDecimal getRetreadLtSummerHqMargin() {
        return retreadLtSummerHqMargin;
    }

    /** 
     * Sets the retreadLtSummerHqMargin.
     * 
     * @param retreadLtSummerHqMargin the retreadLtSummerHqMargin
     */
    public void setRetreadLtSummerHqMargin(BigDecimal retreadLtSummerHqMargin) {
        this.retreadLtSummerHqMargin = retreadLtSummerHqMargin;
    }

    /** 
     * Returns the retreadLtSummerNumber.
     * 
     * @return the retreadLtSummerNumber
     */
    public BigDecimal getRetreadLtSummerNumber() {
        return retreadLtSummerNumber;
    }

    /** 
     * Sets the retreadLtSummerNumber.
     * 
     * @param retreadLtSummerNumber the retreadLtSummerNumber
     */
    public void setRetreadLtSummerNumber(BigDecimal retreadLtSummerNumber) {
        this.retreadLtSummerNumber = retreadLtSummerNumber;
    }

    /** 
     * Returns the retreadTbSummerSales.
     * 
     * @return the retreadTbSummerSales
     */
    public BigDecimal getRetreadTbSummerSales() {
        return retreadTbSummerSales;
    }

    /** 
     * Sets the retreadTbSummerSales.
     * 
     * @param retreadTbSummerSales the retreadTbSummerSales
     */
    public void setRetreadTbSummerSales(BigDecimal retreadTbSummerSales) {
        this.retreadTbSummerSales = retreadTbSummerSales;
    }

    /** 
     * Returns the retreadTbSummerMargin.
     * 
     * @return the retreadTbSummerMargin
     */
    public BigDecimal getRetreadTbSummerMargin() {
        return retreadTbSummerMargin;
    }

    /** 
     * Sets the retreadTbSummerMargin.
     * 
     * @param retreadTbSummerMargin the retreadTbSummerMargin
     */
    public void setRetreadTbSummerMargin(BigDecimal retreadTbSummerMargin) {
        this.retreadTbSummerMargin = retreadTbSummerMargin;
    }

    /** 
     * Returns the retreadTbSummerHqMargin.
     * 
     * @return the retreadTbSummerHqMargin
     */
    public BigDecimal getRetreadTbSummerHqMargin() {
        return retreadTbSummerHqMargin;
    }

    /** 
     * Sets the retreadTbSummerHqMargin.
     * 
     * @param retreadTbSummerHqMargin the retreadTbSummerHqMargin
     */
    public void setRetreadTbSummerHqMargin(BigDecimal retreadTbSummerHqMargin) {
        this.retreadTbSummerHqMargin = retreadTbSummerHqMargin;
    }

    /** 
     * Returns the retreadTbSummerNumber.
     * 
     * @return the retreadTbSummerNumber
     */
    public BigDecimal getRetreadTbSummerNumber() {
        return retreadTbSummerNumber;
    }

    /** 
     * Sets the retreadTbSummerNumber.
     * 
     * @param retreadTbSummerNumber the retreadTbSummerNumber
     */
    public void setRetreadTbSummerNumber(BigDecimal retreadTbSummerNumber) {
        this.retreadTbSummerNumber = retreadTbSummerNumber;
    }

    /** 
     * Returns the retreadSpecialTireSummerSales.
     * 
     * @return the retreadSpecialTireSummerSales
     */
    public BigDecimal getRetreadSpecialTireSummerSales() {
        return retreadSpecialTireSummerSales;
    }

    /** 
     * Sets the retreadSpecialTireSummerSales.
     * 
     * @param retreadSpecialTireSummerSales the retreadSpecialTireSummerSales
     */
    public void setRetreadSpecialTireSummerSales(BigDecimal retreadSpecialTireSummerSales) {
        this.retreadSpecialTireSummerSales = retreadSpecialTireSummerSales;
    }

    /** 
     * Returns the retreadSpecialTireSummerMargin.
     * 
     * @return the retreadSpecialTireSummerMargin
     */
    public BigDecimal getRetreadSpecialTireSummerMargin() {
        return retreadSpecialTireSummerMargin;
    }

    /** 
     * Sets the retreadSpecialTireSummerMargin.
     * 
     * @param retreadSpecialTireSummerMargin the retreadSpecialTireSummerMargin
     */
    public void setRetreadSpecialTireSummerMargin(BigDecimal retreadSpecialTireSummerMargin) {
        this.retreadSpecialTireSummerMargin = retreadSpecialTireSummerMargin;
    }

    /** 
     * Returns the retreadSpecialTireSummerHqMargin.
     * 
     * @return the retreadSpecialTireSummerHqMargin
     */
    public BigDecimal getRetreadSpecialTireSummerHqMargin() {
        return retreadSpecialTireSummerHqMargin;
    }

    /** 
     * Sets the retreadSpecialTireSummerHqMargin.
     * 
     * @param retreadSpecialTireSummerHqMargin the retreadSpecialTireSummerHqMargin
     */
    public void setRetreadSpecialTireSummerHqMargin(BigDecimal retreadSpecialTireSummerHqMargin) {
        this.retreadSpecialTireSummerHqMargin = retreadSpecialTireSummerHqMargin;
    }

    /** 
     * Returns the retreadSpecialTireSummerNumber.
     * 
     * @return the retreadSpecialTireSummerNumber
     */
    public BigDecimal getRetreadSpecialTireSummerNumber() {
        return retreadSpecialTireSummerNumber;
    }

    /** 
     * Sets the retreadSpecialTireSummerNumber.
     * 
     * @param retreadSpecialTireSummerNumber the retreadSpecialTireSummerNumber
     */
    public void setRetreadSpecialTireSummerNumber(BigDecimal retreadSpecialTireSummerNumber) {
        this.retreadSpecialTireSummerNumber = retreadSpecialTireSummerNumber;
    }

    /** 
     * Returns the retreadLtSnowSales.
     * 
     * @return the retreadLtSnowSales
     */
    public BigDecimal getRetreadLtSnowSales() {
        return retreadLtSnowSales;
    }

    /** 
     * Sets the retreadLtSnowSales.
     * 
     * @param retreadLtSnowSales the retreadLtSnowSales
     */
    public void setRetreadLtSnowSales(BigDecimal retreadLtSnowSales) {
        this.retreadLtSnowSales = retreadLtSnowSales;
    }

    /** 
     * Returns the retreadLtSnowMargin.
     * 
     * @return the retreadLtSnowMargin
     */
    public BigDecimal getRetreadLtSnowMargin() {
        return retreadLtSnowMargin;
    }

    /** 
     * Sets the retreadLtSnowMargin.
     * 
     * @param retreadLtSnowMargin the retreadLtSnowMargin
     */
    public void setRetreadLtSnowMargin(BigDecimal retreadLtSnowMargin) {
        this.retreadLtSnowMargin = retreadLtSnowMargin;
    }

    /** 
     * Returns the retreadLtSnowHqMargin.
     * 
     * @return the retreadLtSnowHqMargin
     */
    public BigDecimal getRetreadLtSnowHqMargin() {
        return retreadLtSnowHqMargin;
    }

    /** 
     * Sets the retreadLtSnowHqMargin.
     * 
     * @param retreadLtSnowHqMargin the retreadLtSnowHqMargin
     */
    public void setRetreadLtSnowHqMargin(BigDecimal retreadLtSnowHqMargin) {
        this.retreadLtSnowHqMargin = retreadLtSnowHqMargin;
    }

    /** 
     * Returns the retreadLtSnowNumber.
     * 
     * @return the retreadLtSnowNumber
     */
    public BigDecimal getRetreadLtSnowNumber() {
        return retreadLtSnowNumber;
    }

    /** 
     * Sets the retreadLtSnowNumber.
     * 
     * @param retreadLtSnowNumber the retreadLtSnowNumber
     */
    public void setRetreadLtSnowNumber(BigDecimal retreadLtSnowNumber) {
        this.retreadLtSnowNumber = retreadLtSnowNumber;
    }

    /** 
     * Returns the retreadTbSnowSales.
     * 
     * @return the retreadTbSnowSales
     */
    public BigDecimal getRetreadTbSnowSales() {
        return retreadTbSnowSales;
    }

    /** 
     * Sets the retreadTbSnowSales.
     * 
     * @param retreadTbSnowSales the retreadTbSnowSales
     */
    public void setRetreadTbSnowSales(BigDecimal retreadTbSnowSales) {
        this.retreadTbSnowSales = retreadTbSnowSales;
    }

    /** 
     * Returns the retreadTbSnowMargin.
     * 
     * @return the retreadTbSnowMargin
     */
    public BigDecimal getRetreadTbSnowMargin() {
        return retreadTbSnowMargin;
    }

    /** 
     * Sets the retreadTbSnowMargin.
     * 
     * @param retreadTbSnowMargin the retreadTbSnowMargin
     */
    public void setRetreadTbSnowMargin(BigDecimal retreadTbSnowMargin) {
        this.retreadTbSnowMargin = retreadTbSnowMargin;
    }

    /** 
     * Returns the retreadTbSnowHqMargin.
     * 
     * @return the retreadTbSnowHqMargin
     */
    public BigDecimal getRetreadTbSnowHqMargin() {
        return retreadTbSnowHqMargin;
    }

    /** 
     * Sets the retreadTbSnowHqMargin.
     * 
     * @param retreadTbSnowHqMargin the retreadTbSnowHqMargin
     */
    public void setRetreadTbSnowHqMargin(BigDecimal retreadTbSnowHqMargin) {
        this.retreadTbSnowHqMargin = retreadTbSnowHqMargin;
    }

    /** 
     * Returns the retreadTbSnowNumber.
     * 
     * @return the retreadTbSnowNumber
     */
    public BigDecimal getRetreadTbSnowNumber() {
        return retreadTbSnowNumber;
    }

    /** 
     * Sets the retreadTbSnowNumber.
     * 
     * @param retreadTbSnowNumber the retreadTbSnowNumber
     */
    public void setRetreadTbSnowNumber(BigDecimal retreadTbSnowNumber) {
        this.retreadTbSnowNumber = retreadTbSnowNumber;
    }
}