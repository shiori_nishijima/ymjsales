package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OpportunitiesListener implements EntityListener<Opportunities> {

    @Override
    public void preInsert(Opportunities entity, PreInsertContext<Opportunities> context) {
    }

    @Override
    public void preUpdate(Opportunities entity, PreUpdateContext<Opportunities> context) {
    }

    @Override
    public void preDelete(Opportunities entity, PreDeleteContext<Opportunities> context) {
    }

    @Override
    public void postInsert(Opportunities entity, PostInsertContext<Opportunities> context) {
    }

    @Override
    public void postUpdate(Opportunities entity, PostUpdateContext<Opportunities> context) {
    }

    @Override
    public void postDelete(Opportunities entity, PostDeleteContext<Opportunities> context) {
    }
}