package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MstTantoSSosikiListener implements EntityListener<MstTantoSSosiki> {

    @Override
    public void preInsert(MstTantoSSosiki entity, PreInsertContext<MstTantoSSosiki> context) {
    }

    @Override
    public void preUpdate(MstTantoSSosiki entity, PreUpdateContext<MstTantoSSosiki> context) {
    }

    @Override
    public void preDelete(MstTantoSSosiki entity, PreDeleteContext<MstTantoSSosiki> context) {
    }

    @Override
    public void postInsert(MstTantoSSosiki entity, PostInsertContext<MstTantoSSosiki> context) {
    }

    @Override
    public void postUpdate(MstTantoSSosiki entity, PostUpdateContext<MstTantoSSosiki> context) {
    }

    @Override
    public void postDelete(MstTantoSSosiki entity, PostDeleteContext<MstTantoSSosiki> context) {
    }
}