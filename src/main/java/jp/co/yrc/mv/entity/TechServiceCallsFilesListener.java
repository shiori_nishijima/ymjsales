package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCallsFilesListener implements EntityListener<TechServiceCallsFiles> {

    @Override
    public void preInsert(TechServiceCallsFiles entity, PreInsertContext<TechServiceCallsFiles> context) {
    }

    @Override
    public void preUpdate(TechServiceCallsFiles entity, PreUpdateContext<TechServiceCallsFiles> context) {
    }

    @Override
    public void preDelete(TechServiceCallsFiles entity, PreDeleteContext<TechServiceCallsFiles> context) {
    }

    @Override
    public void postInsert(TechServiceCallsFiles entity, PostInsertContext<TechServiceCallsFiles> context) {
    }

    @Override
    public void postUpdate(TechServiceCallsFiles entity, PostUpdateContext<TechServiceCallsFiles> context) {
    }

    @Override
    public void postDelete(TechServiceCallsFiles entity, PostDeleteContext<TechServiceCallsFiles> context) {
    }
}