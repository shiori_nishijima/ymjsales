package jp.co.yrc.mv.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OppSeListener.class)
@Table(name = "opp_se")
public class OppSe {

    /** SE案件CD */
    @Id
    @Column(name = "id")
    String id;

    /** 名称 */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細（未使用） */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 営業担当者 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** ステータス */
    @Column(name = "status")
    String status;

    /** SE受注確度（%） */
    @Column(name = "seorderaccuracy")
    String seorderaccuracy;

    /** プロジェクト名 */
    @Column(name = "projectname")
    String projectname;

    /** SE戦略分類 */
    @Column(name = "sestrategicclassification")
    String sestrategicclassification;

    /** SE案件概要 */
    @Column(name = "seitemsummary")
    String seitemsummary;

    /** 売上（一時金） */
    @Column(name = "salestempnum")
    Double salestempnum;

    /** 売上（月額） */
    @Column(name = "salesmonthnum")
    Double salesmonthnum;

    /** 契約期間（ヶ月） */
    @Column(name = "contracttermmonthlybasis")
    Integer contracttermmonthlybasis;

    /** PS売上（一時金） */
    @Column(name = "pssalestempnum")
    Double pssalestempnum;

    /** PS売上（月額） */
    @Column(name = "pssalesmonthnum")
    Double pssalesmonthnum;

    /** PS計上予定時期 */
    @Column(name = "psbudgetschedule")
    Date psbudgetschedule;

    /** 受注失注理由 */
    @Column(name = "causeorderdfailure")
    String causeorderdfailure;

    /** 予備 */
    @Column(name = "spare")
    String spare;

    /**  */
    @Column(name = "calling_agent")
    String callingAgent;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /** 
     * Sets the status.
     * 
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /** 
     * Returns the seorderaccuracy.
     * 
     * @return the seorderaccuracy
     */
    public String getSeorderaccuracy() {
        return seorderaccuracy;
    }

    /** 
     * Sets the seorderaccuracy.
     * 
     * @param seorderaccuracy the seorderaccuracy
     */
    public void setSeorderaccuracy(String seorderaccuracy) {
        this.seorderaccuracy = seorderaccuracy;
    }

    /** 
     * Returns the projectname.
     * 
     * @return the projectname
     */
    public String getProjectname() {
        return projectname;
    }

    /** 
     * Sets the projectname.
     * 
     * @param projectname the projectname
     */
    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    /** 
     * Returns the sestrategicclassification.
     * 
     * @return the sestrategicclassification
     */
    public String getSestrategicclassification() {
        return sestrategicclassification;
    }

    /** 
     * Sets the sestrategicclassification.
     * 
     * @param sestrategicclassification the sestrategicclassification
     */
    public void setSestrategicclassification(String sestrategicclassification) {
        this.sestrategicclassification = sestrategicclassification;
    }

    /** 
     * Returns the seitemsummary.
     * 
     * @return the seitemsummary
     */
    public String getSeitemsummary() {
        return seitemsummary;
    }

    /** 
     * Sets the seitemsummary.
     * 
     * @param seitemsummary the seitemsummary
     */
    public void setSeitemsummary(String seitemsummary) {
        this.seitemsummary = seitemsummary;
    }

    /** 
     * Returns the salestempnum.
     * 
     * @return the salestempnum
     */
    public Double getSalestempnum() {
        return salestempnum;
    }

    /** 
     * Sets the salestempnum.
     * 
     * @param salestempnum the salestempnum
     */
    public void setSalestempnum(Double salestempnum) {
        this.salestempnum = salestempnum;
    }

    /** 
     * Returns the salesmonthnum.
     * 
     * @return the salesmonthnum
     */
    public Double getSalesmonthnum() {
        return salesmonthnum;
    }

    /** 
     * Sets the salesmonthnum.
     * 
     * @param salesmonthnum the salesmonthnum
     */
    public void setSalesmonthnum(Double salesmonthnum) {
        this.salesmonthnum = salesmonthnum;
    }

    /** 
     * Returns the contracttermmonthlybasis.
     * 
     * @return the contracttermmonthlybasis
     */
    public Integer getContracttermmonthlybasis() {
        return contracttermmonthlybasis;
    }

    /** 
     * Sets the contracttermmonthlybasis.
     * 
     * @param contracttermmonthlybasis the contracttermmonthlybasis
     */
    public void setContracttermmonthlybasis(Integer contracttermmonthlybasis) {
        this.contracttermmonthlybasis = contracttermmonthlybasis;
    }

    /** 
     * Returns the pssalestempnum.
     * 
     * @return the pssalestempnum
     */
    public Double getPssalestempnum() {
        return pssalestempnum;
    }

    /** 
     * Sets the pssalestempnum.
     * 
     * @param pssalestempnum the pssalestempnum
     */
    public void setPssalestempnum(Double pssalestempnum) {
        this.pssalestempnum = pssalestempnum;
    }

    /** 
     * Returns the pssalesmonthnum.
     * 
     * @return the pssalesmonthnum
     */
    public Double getPssalesmonthnum() {
        return pssalesmonthnum;
    }

    /** 
     * Sets the pssalesmonthnum.
     * 
     * @param pssalesmonthnum the pssalesmonthnum
     */
    public void setPssalesmonthnum(Double pssalesmonthnum) {
        this.pssalesmonthnum = pssalesmonthnum;
    }

    /** 
     * Returns the psbudgetschedule.
     * 
     * @return the psbudgetschedule
     */
    public Date getPsbudgetschedule() {
        return psbudgetschedule;
    }

    /** 
     * Sets the psbudgetschedule.
     * 
     * @param psbudgetschedule the psbudgetschedule
     */
    public void setPsbudgetschedule(Date psbudgetschedule) {
        this.psbudgetschedule = psbudgetschedule;
    }

    /** 
     * Returns the causeorderdfailure.
     * 
     * @return the causeorderdfailure
     */
    public String getCauseorderdfailure() {
        return causeorderdfailure;
    }

    /** 
     * Sets the causeorderdfailure.
     * 
     * @param causeorderdfailure the causeorderdfailure
     */
    public void setCauseorderdfailure(String causeorderdfailure) {
        this.causeorderdfailure = causeorderdfailure;
    }

    /** 
     * Returns the spare.
     * 
     * @return the spare
     */
    public String getSpare() {
        return spare;
    }

    /** 
     * Sets the spare.
     * 
     * @param spare the spare
     */
    public void setSpare(String spare) {
        this.spare = spare;
    }

    /** 
     * Returns the callingAgent.
     * 
     * @return the callingAgent
     */
    public String getCallingAgent() {
        return callingAgent;
    }

    /** 
     * Sets the callingAgent.
     * 
     * @param callingAgent the callingAgent
     */
    public void setCallingAgent(String callingAgent) {
        this.callingAgent = callingAgent;
    }
}