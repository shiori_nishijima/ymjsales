package jp.co.yrc.mv.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = SvCancelListener.class)
@Table(name = "sv_cancel")
public class SvCancel {

    /** 解約サービスCD */
    @Id
    @Column(name = "id")
    String id;

    /** 名称(未使用) */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細(未使用) */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** アサイン先(未使用) */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 種別 */
    @Column(name = "class")
    String kind;

    /** サービス分類 */
    @Column(name = "cate")
    String cate;

    /** 月額 */
    @Column(name = "month_num")
    Double monthNum;

    /** 一時金 */
    @Column(name = "temp_num")
    Double tempNum;

    /** 回線数 */
    @Column(name = "line_num")
    Double lineNum;

    /** 提供予定年 */
    @Column(name = "yyyy")
    String yyyy;

    /** 提供予定月 */
    @Column(name = "mm")
    String mm;

    /** 新規区分 */
    @Column(name = "billiard_form")
    String billiardForm;

    /** 機種コード */
    @Column(name = "billiard_model_code")
    String billiardModelCode;

    /** メーカー */
    @Column(name = "billiard_maker")
    String billiardMaker;

    /** 機種 */
    @Column(name = "billiard_model")
    String billiardModel;

    /** 色 */
    @Column(name = "billiard_color")
    String billiardColor;

    /** 月額（変動利益） */
    @Column(name = "month_num_variable_profits")
    Double monthNumVariableProfits;

    /** 一時金（変動利益） */
    @Column(name = "temp_num_variable_profits")
    Double tempNumVariableProfits;

    /** 掲載期間（ヶ月） */
    @Column(name = "posted_month_age")
    Integer postedMonthAge;

    /** 玉繰り希望日 */
    @Column(name = "billiard_choice_day")
    Date billiardChoiceDay;

    /** 玉繰りメモ */
    @Column(name = "billiard_memo")
    String billiardMemo;

    /**  */
    @Column(name = "calling_agent")
    String callingAgent;

    /** 月額（現地通貨） */
    @Column(name = "month_num_local_currency")
    Double monthNumLocalCurrency;

    /** 月額（現地通貨）単位 */
    @Column(name = "month_num_unit")
    String monthNumUnit;

    /** 一時金（現地通貨） */
    @Column(name = "temp_num_local_currency")
    Double tempNumLocalCurrency;

    /** 一時金（現地通貨）単位 */
    @Column(name = "temp_num_unit")
    String tempNumUnit;

    /**  */
    @Column(name = "month_age")
    Integer monthAge;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the kind.
     * 
     * @return the kind
     */
    public String getKind() {
        return kind;
    }

    /** 
     * Sets the kind.
     * 
     * @param class the kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /** 
     * Returns the cate.
     * 
     * @return the cate
     */
    public String getCate() {
        return cate;
    }

    /** 
     * Sets the cate.
     * 
     * @param cate the cate
     */
    public void setCate(String cate) {
        this.cate = cate;
    }

    /** 
     * Returns the monthNum.
     * 
     * @return the monthNum
     */
    public Double getMonthNum() {
        return monthNum;
    }

    /** 
     * Sets the monthNum.
     * 
     * @param monthNum the monthNum
     */
    public void setMonthNum(Double monthNum) {
        this.monthNum = monthNum;
    }

    /** 
     * Returns the tempNum.
     * 
     * @return the tempNum
     */
    public Double getTempNum() {
        return tempNum;
    }

    /** 
     * Sets the tempNum.
     * 
     * @param tempNum the tempNum
     */
    public void setTempNum(Double tempNum) {
        this.tempNum = tempNum;
    }

    /** 
     * Returns the lineNum.
     * 
     * @return the lineNum
     */
    public Double getLineNum() {
        return lineNum;
    }

    /** 
     * Sets the lineNum.
     * 
     * @param lineNum the lineNum
     */
    public void setLineNum(Double lineNum) {
        this.lineNum = lineNum;
    }

    /** 
     * Returns the yyyy.
     * 
     * @return the yyyy
     */
    public String getYyyy() {
        return yyyy;
    }

    /** 
     * Sets the yyyy.
     * 
     * @param yyyy the yyyy
     */
    public void setYyyy(String yyyy) {
        this.yyyy = yyyy;
    }

    /** 
     * Returns the mm.
     * 
     * @return the mm
     */
    public String getMm() {
        return mm;
    }

    /** 
     * Sets the mm.
     * 
     * @param mm the mm
     */
    public void setMm(String mm) {
        this.mm = mm;
    }

    /** 
     * Returns the billiardForm.
     * 
     * @return the billiardForm
     */
    public String getBilliardForm() {
        return billiardForm;
    }

    /** 
     * Sets the billiardForm.
     * 
     * @param billiardForm the billiardForm
     */
    public void setBilliardForm(String billiardForm) {
        this.billiardForm = billiardForm;
    }

    /** 
     * Returns the billiardModelCode.
     * 
     * @return the billiardModelCode
     */
    public String getBilliardModelCode() {
        return billiardModelCode;
    }

    /** 
     * Sets the billiardModelCode.
     * 
     * @param billiardModelCode the billiardModelCode
     */
    public void setBilliardModelCode(String billiardModelCode) {
        this.billiardModelCode = billiardModelCode;
    }

    /** 
     * Returns the billiardMaker.
     * 
     * @return the billiardMaker
     */
    public String getBilliardMaker() {
        return billiardMaker;
    }

    /** 
     * Sets the billiardMaker.
     * 
     * @param billiardMaker the billiardMaker
     */
    public void setBilliardMaker(String billiardMaker) {
        this.billiardMaker = billiardMaker;
    }

    /** 
     * Returns the billiardModel.
     * 
     * @return the billiardModel
     */
    public String getBilliardModel() {
        return billiardModel;
    }

    /** 
     * Sets the billiardModel.
     * 
     * @param billiardModel the billiardModel
     */
    public void setBilliardModel(String billiardModel) {
        this.billiardModel = billiardModel;
    }

    /** 
     * Returns the billiardColor.
     * 
     * @return the billiardColor
     */
    public String getBilliardColor() {
        return billiardColor;
    }

    /** 
     * Sets the billiardColor.
     * 
     * @param billiardColor the billiardColor
     */
    public void setBilliardColor(String billiardColor) {
        this.billiardColor = billiardColor;
    }

    /** 
     * Returns the monthNumVariableProfits.
     * 
     * @return the monthNumVariableProfits
     */
    public Double getMonthNumVariableProfits() {
        return monthNumVariableProfits;
    }

    /** 
     * Sets the monthNumVariableProfits.
     * 
     * @param monthNumVariableProfits the monthNumVariableProfits
     */
    public void setMonthNumVariableProfits(Double monthNumVariableProfits) {
        this.monthNumVariableProfits = monthNumVariableProfits;
    }

    /** 
     * Returns the tempNumVariableProfits.
     * 
     * @return the tempNumVariableProfits
     */
    public Double getTempNumVariableProfits() {
        return tempNumVariableProfits;
    }

    /** 
     * Sets the tempNumVariableProfits.
     * 
     * @param tempNumVariableProfits the tempNumVariableProfits
     */
    public void setTempNumVariableProfits(Double tempNumVariableProfits) {
        this.tempNumVariableProfits = tempNumVariableProfits;
    }

    /** 
     * Returns the postedMonthAge.
     * 
     * @return the postedMonthAge
     */
    public Integer getPostedMonthAge() {
        return postedMonthAge;
    }

    /** 
     * Sets the postedMonthAge.
     * 
     * @param postedMonthAge the postedMonthAge
     */
    public void setPostedMonthAge(Integer postedMonthAge) {
        this.postedMonthAge = postedMonthAge;
    }

    /** 
     * Returns the billiardChoiceDay.
     * 
     * @return the billiardChoiceDay
     */
    public Date getBilliardChoiceDay() {
        return billiardChoiceDay;
    }

    /** 
     * Sets the billiardChoiceDay.
     * 
     * @param billiardChoiceDay the billiardChoiceDay
     */
    public void setBilliardChoiceDay(Date billiardChoiceDay) {
        this.billiardChoiceDay = billiardChoiceDay;
    }

    /** 
     * Returns the billiardMemo.
     * 
     * @return the billiardMemo
     */
    public String getBilliardMemo() {
        return billiardMemo;
    }

    /** 
     * Sets the billiardMemo.
     * 
     * @param billiardMemo the billiardMemo
     */
    public void setBilliardMemo(String billiardMemo) {
        this.billiardMemo = billiardMemo;
    }

    /** 
     * Returns the callingAgent.
     * 
     * @return the callingAgent
     */
    public String getCallingAgent() {
        return callingAgent;
    }

    /** 
     * Sets the callingAgent.
     * 
     * @param callingAgent the callingAgent
     */
    public void setCallingAgent(String callingAgent) {
        this.callingAgent = callingAgent;
    }

    /** 
     * Returns the monthNumLocalCurrency.
     * 
     * @return the monthNumLocalCurrency
     */
    public Double getMonthNumLocalCurrency() {
        return monthNumLocalCurrency;
    }

    /** 
     * Sets the monthNumLocalCurrency.
     * 
     * @param monthNumLocalCurrency the monthNumLocalCurrency
     */
    public void setMonthNumLocalCurrency(Double monthNumLocalCurrency) {
        this.monthNumLocalCurrency = monthNumLocalCurrency;
    }

    /** 
     * Returns the monthNumUnit.
     * 
     * @return the monthNumUnit
     */
    public String getMonthNumUnit() {
        return monthNumUnit;
    }

    /** 
     * Sets the monthNumUnit.
     * 
     * @param monthNumUnit the monthNumUnit
     */
    public void setMonthNumUnit(String monthNumUnit) {
        this.monthNumUnit = monthNumUnit;
    }

    /** 
     * Returns the tempNumLocalCurrency.
     * 
     * @return the tempNumLocalCurrency
     */
    public Double getTempNumLocalCurrency() {
        return tempNumLocalCurrency;
    }

    /** 
     * Sets the tempNumLocalCurrency.
     * 
     * @param tempNumLocalCurrency the tempNumLocalCurrency
     */
    public void setTempNumLocalCurrency(Double tempNumLocalCurrency) {
        this.tempNumLocalCurrency = tempNumLocalCurrency;
    }

    /** 
     * Returns the tempNumUnit.
     * 
     * @return the tempNumUnit
     */
    public String getTempNumUnit() {
        return tempNumUnit;
    }

    /** 
     * Sets the tempNumUnit.
     * 
     * @param tempNumUnit the tempNumUnit
     */
    public void setTempNumUnit(String tempNumUnit) {
        this.tempNumUnit = tempNumUnit;
    }

    /** 
     * Returns the monthAge.
     * 
     * @return the monthAge
     */
    public Integer getMonthAge() {
        return monthAge;
    }

    /** 
     * Sets the monthAge.
     * 
     * @param monthAge the monthAge
     */
    public void setMonthAge(Integer monthAge) {
        this.monthAge = monthAge;
    }
}