package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyCommentsListener.class)
@Table(name = "monthly_comments")
public class MonthlyComments {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 年 */
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Column(name = "month")
    Integer month;

    /** ユーザID */
    @Column(name = "user_id")
    String userId;

    /** 
     *  訪問回数 
     *  MV側からは更新不可 
     */
    @Column(name = "plan_visit_num", updatable = false)
    BigDecimal planVisitNum;

    /** 
     *  情報件数 
     *  MV側からは更新不可 
     */
    @Column(name = "plan_info_num", updatable = false)
    BigDecimal planInfoNum;

    /** 
     *  動タ需要係数
     *  MV側からは更新不可 
     */
    @Column(name = "plan_demand_coefficient", updatable = false)
    BigDecimal planDemandCoefficient;

    /** 
     *  担当者コメント
     *  MV側からは更新不可 
     */
    @Column(name = "user_comment", updatable = false)
    String userComment;

    /** 上司コメント1 */
    @Column(name = "boss_comment1")
    String bossComment1;

    /** 上司コメント2 */
    @Column(name = "boss_comment2")
    String bossComment2;

    /** 上司コメント3 */
    @Column(name = "boss_comment3")
    String bossComment3;

    /** 上司コメント4 */
    @Column(name = "boss_comment4")
    String bossComment4;

    /** 上司コメント5 */
    @Column(name = "boss_comment5")
    String bossComment5;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the planVisitNum.
     * 
     * @return the planVisitNum
     */
    public BigDecimal getPlanVisitNum() {
        return planVisitNum;
    }

    /** 
     * Sets the planVisitNum.
     * 
     * @param planVisitNum the planVisitNum
     */
    public void setPlanVisitNum(BigDecimal planVisitNum) {
        this.planVisitNum = planVisitNum;
    }

    /** 
     * Returns the planInfoNum.
     * 
     * @return the planInfoNum
     */
    public BigDecimal getPlanInfoNum() {
        return planInfoNum;
    }

    /** 
     * Sets the planInfoNum.
     * 
     * @param planInfoNum the planInfoNum
     */
    public void setPlanInfoNum(BigDecimal planInfoNum) {
        this.planInfoNum = planInfoNum;
    }

    /** 
     * Returns the planDemandCoefficient.
     * 
     * @return the planDemandCoefficient
     */
    public BigDecimal getPlanDemandCoefficient() {
        return planDemandCoefficient;
    }

    /** 
     * Sets the planDemandCoefficient.
     * 
     * @param planDemandCoefficient the planDemandCoefficient
     */
    public void setPlanDemandCoefficient(BigDecimal planDemandCoefficient) {
        this.planDemandCoefficient = planDemandCoefficient;
    }

    /** 
     * Returns the userComment.
     * 
     * @return the userComment
     */
    public String getUserComment() {
        return userComment;
    }

    /** 
     * Sets the userComment.
     * 
     * @param userComment the userComment
     */
    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    /** 
     * Returns the bossComment1.
     * 
     * @return the bossComment1
     */
    public String getBossComment1() {
        return bossComment1;
    }

    /** 
     * Sets the bossComment1.
     * 
     * @param bossComment1 the bossComment1
     */
    public void setBossComment1(String bossComment1) {
        this.bossComment1 = bossComment1;
    }

    /** 
     * Returns the bossComment2.
     * 
     * @return the bossComment2
     */
    public String getBossComment2() {
        return bossComment2;
    }

    /** 
     * Sets the bossComment2.
     * 
     * @param bossComment2 the bossComment2
     */
    public void setBossComment2(String bossComment2) {
        this.bossComment2 = bossComment2;
    }

    /** 
     * Returns the bossComment3.
     * 
     * @return the bossComment3
     */
    public String getBossComment3() {
        return bossComment3;
    }

    /** 
     * Sets the bossComment3.
     * 
     * @param bossComment3 the bossComment3
     */
    public void setBossComment3(String bossComment3) {
        this.bossComment3 = bossComment3;
    }

    /** 
     * Returns the bossComment4.
     * 
     * @return the bossComment4
     */
    public String getBossComment4() {
        return bossComment4;
    }

    /** 
     * Sets the bossComment4.
     * 
     * @param bossComment4 the bossComment4
     */
    public void setBossComment4(String bossComment4) {
        this.bossComment4 = bossComment4;
    }

    /** 
     * Returns the bossComment5.
     * 
     * @return the bossComment5
     */
    public String getBossComment5() {
        return bossComment5;
    }

    /** 
     * Sets the bossComment5.
     * 
     * @param bossComment5 the bossComment5
     */
    public void setBossComment5(String bossComment5) {
        this.bossComment5 = bossComment5;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}