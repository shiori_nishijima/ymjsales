package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyMarketDealsListener implements EntityListener<MonthlyMarketDeals> {

    @Override
    public void preInsert(MonthlyMarketDeals entity, PreInsertContext<MonthlyMarketDeals> context) {
    }

    @Override
    public void preUpdate(MonthlyMarketDeals entity, PreUpdateContext<MonthlyMarketDeals> context) {
    }

    @Override
    public void preDelete(MonthlyMarketDeals entity, PreDeleteContext<MonthlyMarketDeals> context) {
    }

    @Override
    public void postInsert(MonthlyMarketDeals entity, PostInsertContext<MonthlyMarketDeals> context) {
    }

    @Override
    public void postUpdate(MonthlyMarketDeals entity, PostUpdateContext<MonthlyMarketDeals> context) {
    }

    @Override
    public void postDelete(MonthlyMarketDeals entity, PostDeleteContext<MonthlyMarketDeals> context) {
    }
}