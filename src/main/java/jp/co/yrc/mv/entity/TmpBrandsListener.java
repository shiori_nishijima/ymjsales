package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpBrandsListener implements EntityListener<TmpBrands> {

    @Override
    public void preInsert(TmpBrands entity, PreInsertContext<TmpBrands> context) {
    }

    @Override
    public void preUpdate(TmpBrands entity, PreUpdateContext<TmpBrands> context) {
    }

    @Override
    public void preDelete(TmpBrands entity, PreDeleteContext<TmpBrands> context) {
    }

    @Override
    public void postInsert(TmpBrands entity, PostInsertContext<TmpBrands> context) {
    }

    @Override
    public void postUpdate(TmpBrands entity, PostUpdateContext<TmpBrands> context) {
    }

    @Override
    public void postDelete(TmpBrands entity, PostDeleteContext<TmpBrands> context) {
    }
}