package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ShopStocksListener implements EntityListener<ShopStocks> {

    @Override
    public void preInsert(ShopStocks entity, PreInsertContext<ShopStocks> context) {
    }

    @Override
    public void preUpdate(ShopStocks entity, PreUpdateContext<ShopStocks> context) {
    }

    @Override
    public void preDelete(ShopStocks entity, PreDeleteContext<ShopStocks> context) {
    }

    @Override
    public void postInsert(ShopStocks entity, PostInsertContext<ShopStocks> context) {
    }

    @Override
    public void postUpdate(ShopStocks entity, PostUpdateContext<ShopStocks> context) {
    }

    @Override
    public void postDelete(ShopStocks entity, PostDeleteContext<ShopStocks> context) {
    }
}