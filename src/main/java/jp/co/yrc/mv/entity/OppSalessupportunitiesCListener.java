package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OppSalessupportunitiesCListener implements EntityListener<OppSalessupportunitiesC> {

    @Override
    public void preInsert(OppSalessupportunitiesC entity, PreInsertContext<OppSalessupportunitiesC> context) {
    }

    @Override
    public void preUpdate(OppSalessupportunitiesC entity, PreUpdateContext<OppSalessupportunitiesC> context) {
    }

    @Override
    public void preDelete(OppSalessupportunitiesC entity, PreDeleteContext<OppSalessupportunitiesC> context) {
    }

    @Override
    public void postInsert(OppSalessupportunitiesC entity, PostInsertContext<OppSalessupportunitiesC> context) {
    }

    @Override
    public void postUpdate(OppSalessupportunitiesC entity, PostUpdateContext<OppSalessupportunitiesC> context) {
    }

    @Override
    public void postDelete(OppSalessupportunitiesC entity, PostDeleteContext<OppSalessupportunitiesC> context) {
    }
}