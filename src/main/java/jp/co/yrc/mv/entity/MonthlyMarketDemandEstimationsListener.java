package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyMarketDemandEstimationsListener implements EntityListener<MonthlyMarketDemandEstimations> {

    @Override
    public void preInsert(MonthlyMarketDemandEstimations entity, PreInsertContext<MonthlyMarketDemandEstimations> context) {
    }

    @Override
    public void preUpdate(MonthlyMarketDemandEstimations entity, PreUpdateContext<MonthlyMarketDemandEstimations> context) {
    }

    @Override
    public void preDelete(MonthlyMarketDemandEstimations entity, PreDeleteContext<MonthlyMarketDemandEstimations> context) {
    }

    @Override
    public void postInsert(MonthlyMarketDemandEstimations entity, PostInsertContext<MonthlyMarketDemandEstimations> context) {
    }

    @Override
    public void postUpdate(MonthlyMarketDemandEstimations entity, PostUpdateContext<MonthlyMarketDemandEstimations> context) {
    }

    @Override
    public void postDelete(MonthlyMarketDemandEstimations entity, PostDeleteContext<MonthlyMarketDemandEstimations> context) {
    }
}