package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MastServicelineListener implements EntityListener<MastServiceline> {

    @Override
    public void preInsert(MastServiceline entity, PreInsertContext<MastServiceline> context) {
    }

    @Override
    public void preUpdate(MastServiceline entity, PreUpdateContext<MastServiceline> context) {
    }

    @Override
    public void preDelete(MastServiceline entity, PreDeleteContext<MastServiceline> context) {
    }

    @Override
    public void postInsert(MastServiceline entity, PostInsertContext<MastServiceline> context) {
    }

    @Override
    public void postUpdate(MastServiceline entity, PostUpdateContext<MastServiceline> context) {
    }

    @Override
    public void postDelete(MastServiceline entity, PostDeleteContext<MastServiceline> context) {
    }
}