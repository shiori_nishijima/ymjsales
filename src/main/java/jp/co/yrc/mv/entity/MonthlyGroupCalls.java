package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyGroupCallsListener.class)
@Table(name = "monthly_group_calls")
public class MonthlyGroupCalls {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** Aランク訪問予定数 */
    @Column(name = "rank_a_plan_count")
    BigDecimal rankAPlanCount;

    /** Aランク訪問数 */
    @Column(name = "rank_a_call_count")
    BigDecimal rankACallCount;

    /** Aランク情報数 */
    @Column(name = "rank_a_info_count")
    BigDecimal rankAInfoCount;

    /** Bランク訪問予定数 */
    @Column(name = "rank_b_plan_count")
    BigDecimal rankBPlanCount;

    /** Bランク訪問数 */
    @Column(name = "rank_b_call_count")
    BigDecimal rankBCallCount;

    /** Bランク情報数 */
    @Column(name = "rank_b_info_count")
    BigDecimal rankBInfoCount;

    /** Cランク訪問予定数 */
    @Column(name = "rank_c_plan_count")
    BigDecimal rankCPlanCount;

    /** Cランク訪問数 */
    @Column(name = "rank_c_call_count")
    BigDecimal rankCCallCount;

    /** Cランク情報数 */
    @Column(name = "rank_c_info_count")
    BigDecimal rankCInfoCount;

    /** 重点訪問予定数 */
    @Column(name = "important_plan_count")
    BigDecimal importantPlanCount;

    /** 重点訪問数 */
    @Column(name = "important_call_count")
    BigDecimal importantCallCount;

    /** 重点情報数 */
    @Column(name = "important_info_count")
    BigDecimal importantInfoCount;

    /** 新規訪問予定数 */
    @Column(name = "new_plan_count")
    BigDecimal newPlanCount;

    /** 新規訪問数 */
    @Column(name = "new_call_count")
    BigDecimal newCallCount;

    /** 新規情報数 */
    @Column(name = "new_info_count")
    BigDecimal newInfoCount;

    /** 深耕訪問予定数 */
    @Column(name = "deep_plowing_plan_count")
    BigDecimal deepPlowingPlanCount;

    /** 深耕訪問件数 */
    @Column(name = "deep_plowing_call_count")
    BigDecimal deepPlowingCallCount;

    /** 深耕情報数 */
    @Column(name = "deep_plowing_info_count")
    BigDecimal deepPlowingInfoCount;

    /** Y-CP訪問予定数 */
    @Column(name = "y_cp_plan_count")
    BigDecimal yCpPlanCount;

    /** Y-CP訪問件数 */
    @Column(name = "y_cp_call_count")
    BigDecimal yCpCallCount;

    /** Y-CP情報件数 */
    @Column(name = "y_cp_info_count")
    BigDecimal yCpInfoCount;

    /** 他社CP訪問予定数 */
    @Column(name = "other_cp_plan_count")
    BigDecimal otherCpPlanCount;

    /** 他社CP訪問数 */
    @Column(name = "other_cp_call_count")
    BigDecimal otherCpCallCount;

    /** 他社CP情報数 */
    @Column(name = "other_cp_info_count")
    BigDecimal otherCpInfoCount;

    /** グループ1訪問予定数 */
    @Column(name = "group_one_plan_count")
    BigDecimal groupOnePlanCount;

    /** グループ1訪問数 */
    @Column(name = "group_one_call_count")
    BigDecimal groupOneCallCount;

    /** グループ1情報数 */
    @Column(name = "group_one_info_count")
    BigDecimal groupOneInfoCount;

    /** グループ2訪問予定数 */
    @Column(name = "group_two_plan_count")
    BigDecimal groupTwoPlanCount;

    /** グループ2訪問数 */
    @Column(name = "group_two_call_count")
    BigDecimal groupTwoCallCount;

    /** グループ2情報数 */
    @Column(name = "group_two_info_count")
    BigDecimal groupTwoInfoCount;

    /** グループ3訪問予定数 */
    @Column(name = "group_three_plan_count")
    BigDecimal groupThreePlanCount;

    /** グループ3訪問数 */
    @Column(name = "group_three_call_count")
    BigDecimal groupThreeCallCount;

    /** グループ3情報数 */
    @Column(name = "group_three_info_count")
    BigDecimal groupThreeInfoCount;

    /** グループ4訪問予定数 */
    @Column(name = "group_four_plan_count")
    BigDecimal groupFourPlanCount;

    /** グループ4訪問数 */
    @Column(name = "group_four_call_count")
    BigDecimal groupFourCallCount;

    /** グループ4情報数 */
    @Column(name = "group_four_info_count")
    BigDecimal groupFourInfoCount;

    /** グループ5訪問予定数 */
    @Column(name = "group_five_plan_count")
    BigDecimal groupFivePlanCount;

    /** グループ5訪問数 */
    @Column(name = "group_five_call_count")
    BigDecimal groupFiveCallCount;

    /** グループ5情報数 */
    @Column(name = "group_five_info_count")
    BigDecimal groupFiveInfoCount;

    /** 担当外訪問予定数 */
    @Column(name = "outside_plan_count")
    BigDecimal outsidePlanCount;

    /** 担当外訪問数 */
    @Column(name = "outside_call_count")
    BigDecimal outsideCallCount;

    /** 担当外情報数 */
    @Column(name = "outside_info_count")
    BigDecimal outsideInfoCount;

    /** 担当外Aランク訪問予定数 */
    @Column(name = "outside_rank_a_plan_count")
    BigDecimal outsideRankAPlanCount;

    /** 担当外Aランク訪問数 */
    @Column(name = "outside_rank_a_call_count")
    BigDecimal outsideRankACallCount;

    /** 担当外Aランク情報数 */
    @Column(name = "outside_rank_a_info_count")
    BigDecimal outsideRankAInfoCount;

    /** 担当外Bランク訪問予定数 */
    @Column(name = "outside_rank_b_plan_count")
    BigDecimal outsideRankBPlanCount;

    /** 担当外Bランク訪問数 */
    @Column(name = "outside_rank_b_call_count")
    BigDecimal outsideRankBCallCount;

    /** 担当外Bランク情報数 */
    @Column(name = "outside_rank_b_info_count")
    BigDecimal outsideRankBInfoCount;

    /** 担当外Cランク訪問予定数 */
    @Column(name = "outside_rank_c_plan_count")
    BigDecimal outsideRankCPlanCount;

    /** 担当外Cランク訪問数 */
    @Column(name = "outside_rank_c_call_count")
    BigDecimal outsideRankCCallCount;

    /** 担当外Cランク情報数 */
    @Column(name = "outside_rank_c_info_count")
    BigDecimal outsideRankCInfoCount;

    /** 担当外重点訪問予定数 */
    @Column(name = "outside_important_plan_count")
    BigDecimal outsideImportantPlanCount;

    /** 担当外重点訪問数 */
    @Column(name = "outside_important_call_count")
    BigDecimal outsideImportantCallCount;

    /** 担当外重点情報数 */
    @Column(name = "outside_important_info_count")
    BigDecimal outsideImportantInfoCount;

    /** 担当外新規訪問予定数 */
    @Column(name = "outside_new_plan_count")
    BigDecimal outsideNewPlanCount;

    /** 担当外新規訪問数 */
    @Column(name = "outside_new_call_count")
    BigDecimal outsideNewCallCount;

    /** 担当外新規情報数 */
    @Column(name = "outside_new_info_count")
    BigDecimal outsideNewInfoCount;

    /** 担当外深耕訪問予定数 */
    @Column(name = "outside_deep_plowing_plan_count")
    BigDecimal outsideDeepPlowingPlanCount;

    /** 担当外深耕訪問件数 */
    @Column(name = "outside_deep_plowing_call_count")
    BigDecimal outsideDeepPlowingCallCount;

    /** 担当外深耕情報数 */
    @Column(name = "outside_deep_plowing_info_count")
    BigDecimal outsideDeepPlowingInfoCount;

    /** 担当外Y-CP訪問予定数 */
    @Column(name = "outside_y_cp_plan_count")
    BigDecimal outsideYCpPlanCount;

    /** 担当外Y-CP訪問件数 */
    @Column(name = "outside_y_cp_call_count")
    BigDecimal outsideYCpCallCount;

    /** 担当外Y-CP情報件数 */
    @Column(name = "outside_y_cp_info_count")
    BigDecimal outsideYCpInfoCount;

    /** 担当外他社CP訪問予定数 */
    @Column(name = "outside_other_cp_plan_count")
    BigDecimal outsideOtherCpPlanCount;

    /** 担当外他社CP訪問数 */
    @Column(name = "outside_other_cp_call_count")
    BigDecimal outsideOtherCpCallCount;

    /** 担当外他社CP情報数 */
    @Column(name = "outside_other_cp_info_count")
    BigDecimal outsideOtherCpInfoCount;

    /** 担当外グループ1訪問予定数 */
    @Column(name = "outside_group_one_plan_count")
    BigDecimal outsideGroupOnePlanCount;

    /** 担当外グループ1訪問数 */
    @Column(name = "outside_group_one_call_count")
    BigDecimal outsideGroupOneCallCount;

    /** 担当外グループ1情報数 */
    @Column(name = "outside_group_one_info_count")
    BigDecimal outsideGroupOneInfoCount;

    /** 担当外グループ2訪問予定数 */
    @Column(name = "outside_group_two_plan_count")
    BigDecimal outsideGroupTwoPlanCount;

    /** 担当外グループ2訪問数 */
    @Column(name = "outside_group_two_call_count")
    BigDecimal outsideGroupTwoCallCount;

    /** 担当外グループ2情報数 */
    @Column(name = "outside_group_two_info_count")
    BigDecimal outsideGroupTwoInfoCount;

    /** 担当外グループ3訪問予定数 */
    @Column(name = "outside_group_three_plan_count")
    BigDecimal outsideGroupThreePlanCount;

    /** 担当外グループ3訪問数 */
    @Column(name = "outside_group_three_call_count")
    BigDecimal outsideGroupThreeCallCount;

    /** 担当外グループ3情報数 */
    @Column(name = "outside_group_three_info_count")
    BigDecimal outsideGroupThreeInfoCount;

    /** 担当外グループ4訪問予定数 */
    @Column(name = "outside_group_four_plan_count")
    BigDecimal outsideGroupFourPlanCount;

    /** 担当外グループ4訪問数 */
    @Column(name = "outside_group_four_call_count")
    BigDecimal outsideGroupFourCallCount;

    /** 担当外グループ4情報数 */
    @Column(name = "outside_group_four_info_count")
    BigDecimal outsideGroupFourInfoCount;

    /** 担当外グループ5訪問予定数 */
    @Column(name = "outside_group_five_plan_count")
    BigDecimal outsideGroupFivePlanCount;

    /** 担当外グループ5訪問数 */
    @Column(name = "outside_group_five_call_count")
    BigDecimal outsideGroupFiveCallCount;

    /** 担当外グループ5情報数 */
    @Column(name = "outside_group_five_info_count")
    BigDecimal outsideGroupFiveInfoCount;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the rankAPlanCount.
     * 
     * @return the rankAPlanCount
     */
    public BigDecimal getRankAPlanCount() {
        return rankAPlanCount;
    }

    /** 
     * Sets the rankAPlanCount.
     * 
     * @param rankAPlanCount the rankAPlanCount
     */
    public void setRankAPlanCount(BigDecimal rankAPlanCount) {
        this.rankAPlanCount = rankAPlanCount;
    }

    /** 
     * Returns the rankACallCount.
     * 
     * @return the rankACallCount
     */
    public BigDecimal getRankACallCount() {
        return rankACallCount;
    }

    /** 
     * Sets the rankACallCount.
     * 
     * @param rankACallCount the rankACallCount
     */
    public void setRankACallCount(BigDecimal rankACallCount) {
        this.rankACallCount = rankACallCount;
    }

    /** 
     * Returns the rankAInfoCount.
     * 
     * @return the rankAInfoCount
     */
    public BigDecimal getRankAInfoCount() {
        return rankAInfoCount;
    }

    /** 
     * Sets the rankAInfoCount.
     * 
     * @param rankAInfoCount the rankAInfoCount
     */
    public void setRankAInfoCount(BigDecimal rankAInfoCount) {
        this.rankAInfoCount = rankAInfoCount;
    }

    /** 
     * Returns the rankBPlanCount.
     * 
     * @return the rankBPlanCount
     */
    public BigDecimal getRankBPlanCount() {
        return rankBPlanCount;
    }

    /** 
     * Sets the rankBPlanCount.
     * 
     * @param rankBPlanCount the rankBPlanCount
     */
    public void setRankBPlanCount(BigDecimal rankBPlanCount) {
        this.rankBPlanCount = rankBPlanCount;
    }

    /** 
     * Returns the rankBCallCount.
     * 
     * @return the rankBCallCount
     */
    public BigDecimal getRankBCallCount() {
        return rankBCallCount;
    }

    /** 
     * Sets the rankBCallCount.
     * 
     * @param rankBCallCount the rankBCallCount
     */
    public void setRankBCallCount(BigDecimal rankBCallCount) {
        this.rankBCallCount = rankBCallCount;
    }

    /** 
     * Returns the rankBInfoCount.
     * 
     * @return the rankBInfoCount
     */
    public BigDecimal getRankBInfoCount() {
        return rankBInfoCount;
    }

    /** 
     * Sets the rankBInfoCount.
     * 
     * @param rankBInfoCount the rankBInfoCount
     */
    public void setRankBInfoCount(BigDecimal rankBInfoCount) {
        this.rankBInfoCount = rankBInfoCount;
    }

    /** 
     * Returns the rankCPlanCount.
     * 
     * @return the rankCPlanCount
     */
    public BigDecimal getRankCPlanCount() {
        return rankCPlanCount;
    }

    /** 
     * Sets the rankCPlanCount.
     * 
     * @param rankCPlanCount the rankCPlanCount
     */
    public void setRankCPlanCount(BigDecimal rankCPlanCount) {
        this.rankCPlanCount = rankCPlanCount;
    }

    /** 
     * Returns the rankCCallCount.
     * 
     * @return the rankCCallCount
     */
    public BigDecimal getRankCCallCount() {
        return rankCCallCount;
    }

    /** 
     * Sets the rankCCallCount.
     * 
     * @param rankCCallCount the rankCCallCount
     */
    public void setRankCCallCount(BigDecimal rankCCallCount) {
        this.rankCCallCount = rankCCallCount;
    }

    /** 
     * Returns the rankCInfoCount.
     * 
     * @return the rankCInfoCount
     */
    public BigDecimal getRankCInfoCount() {
        return rankCInfoCount;
    }

    /** 
     * Sets the rankCInfoCount.
     * 
     * @param rankCInfoCount the rankCInfoCount
     */
    public void setRankCInfoCount(BigDecimal rankCInfoCount) {
        this.rankCInfoCount = rankCInfoCount;
    }

    /** 
     * Returns the importantPlanCount.
     * 
     * @return the importantPlanCount
     */
    public BigDecimal getImportantPlanCount() {
        return importantPlanCount;
    }

    /** 
     * Sets the importantPlanCount.
     * 
     * @param importantPlanCount the importantPlanCount
     */
    public void setImportantPlanCount(BigDecimal importantPlanCount) {
        this.importantPlanCount = importantPlanCount;
    }

    /** 
     * Returns the importantCallCount.
     * 
     * @return the importantCallCount
     */
    public BigDecimal getImportantCallCount() {
        return importantCallCount;
    }

    /** 
     * Sets the importantCallCount.
     * 
     * @param importantCallCount the importantCallCount
     */
    public void setImportantCallCount(BigDecimal importantCallCount) {
        this.importantCallCount = importantCallCount;
    }

    /** 
     * Returns the importantInfoCount.
     * 
     * @return the importantInfoCount
     */
    public BigDecimal getImportantInfoCount() {
        return importantInfoCount;
    }

    /** 
     * Sets the importantInfoCount.
     * 
     * @param importantInfoCount the importantInfoCount
     */
    public void setImportantInfoCount(BigDecimal importantInfoCount) {
        this.importantInfoCount = importantInfoCount;
    }

    /** 
     * Returns the newPlanCount.
     * 
     * @return the newPlanCount
     */
    public BigDecimal getNewPlanCount() {
        return newPlanCount;
    }

    /** 
     * Sets the newPlanCount.
     * 
     * @param newPlanCount the newPlanCount
     */
    public void setNewPlanCount(BigDecimal newPlanCount) {
        this.newPlanCount = newPlanCount;
    }

    /** 
     * Returns the newCallCount.
     * 
     * @return the newCallCount
     */
    public BigDecimal getNewCallCount() {
        return newCallCount;
    }

    /** 
     * Sets the newCallCount.
     * 
     * @param newCallCount the newCallCount
     */
    public void setNewCallCount(BigDecimal newCallCount) {
        this.newCallCount = newCallCount;
    }

    /** 
     * Returns the newInfoCount.
     * 
     * @return the newInfoCount
     */
    public BigDecimal getNewInfoCount() {
        return newInfoCount;
    }

    /** 
     * Sets the newInfoCount.
     * 
     * @param newInfoCount the newInfoCount
     */
    public void setNewInfoCount(BigDecimal newInfoCount) {
        this.newInfoCount = newInfoCount;
    }

    /** 
     * Returns the deepPlowingPlanCount.
     * 
     * @return the deepPlowingPlanCount
     */
    public BigDecimal getDeepPlowingPlanCount() {
        return deepPlowingPlanCount;
    }

    /** 
     * Sets the deepPlowingPlanCount.
     * 
     * @param deepPlowingPlanCount the deepPlowingPlanCount
     */
    public void setDeepPlowingPlanCount(BigDecimal deepPlowingPlanCount) {
        this.deepPlowingPlanCount = deepPlowingPlanCount;
    }

    /** 
     * Returns the deepPlowingCallCount.
     * 
     * @return the deepPlowingCallCount
     */
    public BigDecimal getDeepPlowingCallCount() {
        return deepPlowingCallCount;
    }

    /** 
     * Sets the deepPlowingCallCount.
     * 
     * @param deepPlowingCallCount the deepPlowingCallCount
     */
    public void setDeepPlowingCallCount(BigDecimal deepPlowingCallCount) {
        this.deepPlowingCallCount = deepPlowingCallCount;
    }

    /** 
     * Returns the deepPlowingInfoCount.
     * 
     * @return the deepPlowingInfoCount
     */
    public BigDecimal getDeepPlowingInfoCount() {
        return deepPlowingInfoCount;
    }

    /** 
     * Sets the deepPlowingInfoCount.
     * 
     * @param deepPlowingInfoCount the deepPlowingInfoCount
     */
    public void setDeepPlowingInfoCount(BigDecimal deepPlowingInfoCount) {
        this.deepPlowingInfoCount = deepPlowingInfoCount;
    }

    /** 
     * Returns the yCpPlanCount.
     * 
     * @return the yCpPlanCount
     */
    public BigDecimal getYCpPlanCount() {
        return yCpPlanCount;
    }

    /** 
     * Sets the yCpPlanCount.
     * 
     * @param yCpPlanCount the yCpPlanCount
     */
    public void setYCpPlanCount(BigDecimal yCpPlanCount) {
        this.yCpPlanCount = yCpPlanCount;
    }

    /** 
     * Returns the yCpCallCount.
     * 
     * @return the yCpCallCount
     */
    public BigDecimal getYCpCallCount() {
        return yCpCallCount;
    }

    /** 
     * Sets the yCpCallCount.
     * 
     * @param yCpCallCount the yCpCallCount
     */
    public void setYCpCallCount(BigDecimal yCpCallCount) {
        this.yCpCallCount = yCpCallCount;
    }

    /** 
     * Returns the yCpInfoCount.
     * 
     * @return the yCpInfoCount
     */
    public BigDecimal getYCpInfoCount() {
        return yCpInfoCount;
    }

    /** 
     * Sets the yCpInfoCount.
     * 
     * @param yCpInfoCount the yCpInfoCount
     */
    public void setYCpInfoCount(BigDecimal yCpInfoCount) {
        this.yCpInfoCount = yCpInfoCount;
    }

    /** 
     * Returns the otherCpPlanCount.
     * 
     * @return the otherCpPlanCount
     */
    public BigDecimal getOtherCpPlanCount() {
        return otherCpPlanCount;
    }

    /** 
     * Sets the otherCpPlanCount.
     * 
     * @param otherCpPlanCount the otherCpPlanCount
     */
    public void setOtherCpPlanCount(BigDecimal otherCpPlanCount) {
        this.otherCpPlanCount = otherCpPlanCount;
    }

    /** 
     * Returns the otherCpCallCount.
     * 
     * @return the otherCpCallCount
     */
    public BigDecimal getOtherCpCallCount() {
        return otherCpCallCount;
    }

    /** 
     * Sets the otherCpCallCount.
     * 
     * @param otherCpCallCount the otherCpCallCount
     */
    public void setOtherCpCallCount(BigDecimal otherCpCallCount) {
        this.otherCpCallCount = otherCpCallCount;
    }

    /** 
     * Returns the otherCpInfoCount.
     * 
     * @return the otherCpInfoCount
     */
    public BigDecimal getOtherCpInfoCount() {
        return otherCpInfoCount;
    }

    /** 
     * Sets the otherCpInfoCount.
     * 
     * @param otherCpInfoCount the otherCpInfoCount
     */
    public void setOtherCpInfoCount(BigDecimal otherCpInfoCount) {
        this.otherCpInfoCount = otherCpInfoCount;
    }

    /** 
     * Returns the groupOnePlanCount.
     * 
     * @return the groupOnePlanCount
     */
    public BigDecimal getGroupOnePlanCount() {
        return groupOnePlanCount;
    }

    /** 
     * Sets the groupOnePlanCount.
     * 
     * @param groupOnePlanCount the groupOnePlanCount
     */
    public void setGroupOnePlanCount(BigDecimal groupOnePlanCount) {
        this.groupOnePlanCount = groupOnePlanCount;
    }

    /** 
     * Returns the groupOneCallCount.
     * 
     * @return the groupOneCallCount
     */
    public BigDecimal getGroupOneCallCount() {
        return groupOneCallCount;
    }

    /** 
     * Sets the groupOneCallCount.
     * 
     * @param groupOneCallCount the groupOneCallCount
     */
    public void setGroupOneCallCount(BigDecimal groupOneCallCount) {
        this.groupOneCallCount = groupOneCallCount;
    }

    /** 
     * Returns the groupOneInfoCount.
     * 
     * @return the groupOneInfoCount
     */
    public BigDecimal getGroupOneInfoCount() {
        return groupOneInfoCount;
    }

    /** 
     * Sets the groupOneInfoCount.
     * 
     * @param groupOneInfoCount the groupOneInfoCount
     */
    public void setGroupOneInfoCount(BigDecimal groupOneInfoCount) {
        this.groupOneInfoCount = groupOneInfoCount;
    }

    /** 
     * Returns the groupTwoPlanCount.
     * 
     * @return the groupTwoPlanCount
     */
    public BigDecimal getGroupTwoPlanCount() {
        return groupTwoPlanCount;
    }

    /** 
     * Sets the groupTwoPlanCount.
     * 
     * @param groupTwoPlanCount the groupTwoPlanCount
     */
    public void setGroupTwoPlanCount(BigDecimal groupTwoPlanCount) {
        this.groupTwoPlanCount = groupTwoPlanCount;
    }

    /** 
     * Returns the groupTwoCallCount.
     * 
     * @return the groupTwoCallCount
     */
    public BigDecimal getGroupTwoCallCount() {
        return groupTwoCallCount;
    }

    /** 
     * Sets the groupTwoCallCount.
     * 
     * @param groupTwoCallCount the groupTwoCallCount
     */
    public void setGroupTwoCallCount(BigDecimal groupTwoCallCount) {
        this.groupTwoCallCount = groupTwoCallCount;
    }

    /** 
     * Returns the groupTwoInfoCount.
     * 
     * @return the groupTwoInfoCount
     */
    public BigDecimal getGroupTwoInfoCount() {
        return groupTwoInfoCount;
    }

    /** 
     * Sets the groupTwoInfoCount.
     * 
     * @param groupTwoInfoCount the groupTwoInfoCount
     */
    public void setGroupTwoInfoCount(BigDecimal groupTwoInfoCount) {
        this.groupTwoInfoCount = groupTwoInfoCount;
    }

    /** 
     * Returns the groupThreePlanCount.
     * 
     * @return the groupThreePlanCount
     */
    public BigDecimal getGroupThreePlanCount() {
        return groupThreePlanCount;
    }

    /** 
     * Sets the groupThreePlanCount.
     * 
     * @param groupThreePlanCount the groupThreePlanCount
     */
    public void setGroupThreePlanCount(BigDecimal groupThreePlanCount) {
        this.groupThreePlanCount = groupThreePlanCount;
    }

    /** 
     * Returns the groupThreeCallCount.
     * 
     * @return the groupThreeCallCount
     */
    public BigDecimal getGroupThreeCallCount() {
        return groupThreeCallCount;
    }

    /** 
     * Sets the groupThreeCallCount.
     * 
     * @param groupThreeCallCount the groupThreeCallCount
     */
    public void setGroupThreeCallCount(BigDecimal groupThreeCallCount) {
        this.groupThreeCallCount = groupThreeCallCount;
    }

    /** 
     * Returns the groupThreeInfoCount.
     * 
     * @return the groupThreeInfoCount
     */
    public BigDecimal getGroupThreeInfoCount() {
        return groupThreeInfoCount;
    }

    /** 
     * Sets the groupThreeInfoCount.
     * 
     * @param groupThreeInfoCount the groupThreeInfoCount
     */
    public void setGroupThreeInfoCount(BigDecimal groupThreeInfoCount) {
        this.groupThreeInfoCount = groupThreeInfoCount;
    }

    /** 
     * Returns the groupFourPlanCount.
     * 
     * @return the groupFourPlanCount
     */
    public BigDecimal getGroupFourPlanCount() {
        return groupFourPlanCount;
    }

    /** 
     * Sets the groupFourPlanCount.
     * 
     * @param groupFourPlanCount the groupFourPlanCount
     */
    public void setGroupFourPlanCount(BigDecimal groupFourPlanCount) {
        this.groupFourPlanCount = groupFourPlanCount;
    }

    /** 
     * Returns the groupFourCallCount.
     * 
     * @return the groupFourCallCount
     */
    public BigDecimal getGroupFourCallCount() {
        return groupFourCallCount;
    }

    /** 
     * Sets the groupFourCallCount.
     * 
     * @param groupFourCallCount the groupFourCallCount
     */
    public void setGroupFourCallCount(BigDecimal groupFourCallCount) {
        this.groupFourCallCount = groupFourCallCount;
    }

    /** 
     * Returns the groupFourInfoCount.
     * 
     * @return the groupFourInfoCount
     */
    public BigDecimal getGroupFourInfoCount() {
        return groupFourInfoCount;
    }

    /** 
     * Sets the groupFourInfoCount.
     * 
     * @param groupFourInfoCount the groupFourInfoCount
     */
    public void setGroupFourInfoCount(BigDecimal groupFourInfoCount) {
        this.groupFourInfoCount = groupFourInfoCount;
    }

    /** 
     * Returns the groupFivePlanCount.
     * 
     * @return the groupFivePlanCount
     */
    public BigDecimal getGroupFivePlanCount() {
        return groupFivePlanCount;
    }

    /** 
     * Sets the groupFivePlanCount.
     * 
     * @param groupFivePlanCount the groupFivePlanCount
     */
    public void setGroupFivePlanCount(BigDecimal groupFivePlanCount) {
        this.groupFivePlanCount = groupFivePlanCount;
    }

    /** 
     * Returns the groupFiveCallCount.
     * 
     * @return the groupFiveCallCount
     */
    public BigDecimal getGroupFiveCallCount() {
        return groupFiveCallCount;
    }

    /** 
     * Sets the groupFiveCallCount.
     * 
     * @param groupFiveCallCount the groupFiveCallCount
     */
    public void setGroupFiveCallCount(BigDecimal groupFiveCallCount) {
        this.groupFiveCallCount = groupFiveCallCount;
    }

    /** 
     * Returns the groupFiveInfoCount.
     * 
     * @return the groupFiveInfoCount
     */
    public BigDecimal getGroupFiveInfoCount() {
        return groupFiveInfoCount;
    }

    /** 
     * Sets the groupFiveInfoCount.
     * 
     * @param groupFiveInfoCount the groupFiveInfoCount
     */
    public void setGroupFiveInfoCount(BigDecimal groupFiveInfoCount) {
        this.groupFiveInfoCount = groupFiveInfoCount;
    }

    /** 
     * Returns the outsidePlanCount.
     * 
     * @return the outsidePlanCount
     */
    public BigDecimal getOutsidePlanCount() {
        return outsidePlanCount;
    }

    /** 
     * Sets the outsidePlanCount.
     * 
     * @param outsidePlanCount the outsidePlanCount
     */
    public void setOutsidePlanCount(BigDecimal outsidePlanCount) {
        this.outsidePlanCount = outsidePlanCount;
    }

    /** 
     * Returns the outsideCallCount.
     * 
     * @return the outsideCallCount
     */
    public BigDecimal getOutsideCallCount() {
        return outsideCallCount;
    }

    /** 
     * Sets the outsideCallCount.
     * 
     * @param outsideCallCount the outsideCallCount
     */
    public void setOutsideCallCount(BigDecimal outsideCallCount) {
        this.outsideCallCount = outsideCallCount;
    }

    /** 
     * Returns the outsideInfoCount.
     * 
     * @return the outsideInfoCount
     */
    public BigDecimal getOutsideInfoCount() {
        return outsideInfoCount;
    }

    /** 
     * Sets the outsideInfoCount.
     * 
     * @param outsideInfoCount the outsideInfoCount
     */
    public void setOutsideInfoCount(BigDecimal outsideInfoCount) {
        this.outsideInfoCount = outsideInfoCount;
    }

    /** 
     * Returns the outsideRankAPlanCount.
     * 
     * @return the outsideRankAPlanCount
     */
    public BigDecimal getOutsideRankAPlanCount() {
        return outsideRankAPlanCount;
    }

    /** 
     * Sets the outsideRankAPlanCount.
     * 
     * @param outsideRankAPlanCount the outsideRankAPlanCount
     */
    public void setOutsideRankAPlanCount(BigDecimal outsideRankAPlanCount) {
        this.outsideRankAPlanCount = outsideRankAPlanCount;
    }

    /** 
     * Returns the outsideRankACallCount.
     * 
     * @return the outsideRankACallCount
     */
    public BigDecimal getOutsideRankACallCount() {
        return outsideRankACallCount;
    }

    /** 
     * Sets the outsideRankACallCount.
     * 
     * @param outsideRankACallCount the outsideRankACallCount
     */
    public void setOutsideRankACallCount(BigDecimal outsideRankACallCount) {
        this.outsideRankACallCount = outsideRankACallCount;
    }

    /** 
     * Returns the outsideRankAInfoCount.
     * 
     * @return the outsideRankAInfoCount
     */
    public BigDecimal getOutsideRankAInfoCount() {
        return outsideRankAInfoCount;
    }

    /** 
     * Sets the outsideRankAInfoCount.
     * 
     * @param outsideRankAInfoCount the outsideRankAInfoCount
     */
    public void setOutsideRankAInfoCount(BigDecimal outsideRankAInfoCount) {
        this.outsideRankAInfoCount = outsideRankAInfoCount;
    }

    /** 
     * Returns the outsideRankBPlanCount.
     * 
     * @return the outsideRankBPlanCount
     */
    public BigDecimal getOutsideRankBPlanCount() {
        return outsideRankBPlanCount;
    }

    /** 
     * Sets the outsideRankBPlanCount.
     * 
     * @param outsideRankBPlanCount the outsideRankBPlanCount
     */
    public void setOutsideRankBPlanCount(BigDecimal outsideRankBPlanCount) {
        this.outsideRankBPlanCount = outsideRankBPlanCount;
    }

    /** 
     * Returns the outsideRankBCallCount.
     * 
     * @return the outsideRankBCallCount
     */
    public BigDecimal getOutsideRankBCallCount() {
        return outsideRankBCallCount;
    }

    /** 
     * Sets the outsideRankBCallCount.
     * 
     * @param outsideRankBCallCount the outsideRankBCallCount
     */
    public void setOutsideRankBCallCount(BigDecimal outsideRankBCallCount) {
        this.outsideRankBCallCount = outsideRankBCallCount;
    }

    /** 
     * Returns the outsideRankBInfoCount.
     * 
     * @return the outsideRankBInfoCount
     */
    public BigDecimal getOutsideRankBInfoCount() {
        return outsideRankBInfoCount;
    }

    /** 
     * Sets the outsideRankBInfoCount.
     * 
     * @param outsideRankBInfoCount the outsideRankBInfoCount
     */
    public void setOutsideRankBInfoCount(BigDecimal outsideRankBInfoCount) {
        this.outsideRankBInfoCount = outsideRankBInfoCount;
    }

    /** 
     * Returns the outsideRankCPlanCount.
     * 
     * @return the outsideRankCPlanCount
     */
    public BigDecimal getOutsideRankCPlanCount() {
        return outsideRankCPlanCount;
    }

    /** 
     * Sets the outsideRankCPlanCount.
     * 
     * @param outsideRankCPlanCount the outsideRankCPlanCount
     */
    public void setOutsideRankCPlanCount(BigDecimal outsideRankCPlanCount) {
        this.outsideRankCPlanCount = outsideRankCPlanCount;
    }

    /** 
     * Returns the outsideRankCCallCount.
     * 
     * @return the outsideRankCCallCount
     */
    public BigDecimal getOutsideRankCCallCount() {
        return outsideRankCCallCount;
    }

    /** 
     * Sets the outsideRankCCallCount.
     * 
     * @param outsideRankCCallCount the outsideRankCCallCount
     */
    public void setOutsideRankCCallCount(BigDecimal outsideRankCCallCount) {
        this.outsideRankCCallCount = outsideRankCCallCount;
    }

    /** 
     * Returns the outsideRankCInfoCount.
     * 
     * @return the outsideRankCInfoCount
     */
    public BigDecimal getOutsideRankCInfoCount() {
        return outsideRankCInfoCount;
    }

    /** 
     * Sets the outsideRankCInfoCount.
     * 
     * @param outsideRankCInfoCount the outsideRankCInfoCount
     */
    public void setOutsideRankCInfoCount(BigDecimal outsideRankCInfoCount) {
        this.outsideRankCInfoCount = outsideRankCInfoCount;
    }

    /** 
     * Returns the outsideImportantPlanCount.
     * 
     * @return the outsideImportantPlanCount
     */
    public BigDecimal getOutsideImportantPlanCount() {
        return outsideImportantPlanCount;
    }

    /** 
     * Sets the outsideImportantPlanCount.
     * 
     * @param outsideImportantPlanCount the outsideImportantPlanCount
     */
    public void setOutsideImportantPlanCount(BigDecimal outsideImportantPlanCount) {
        this.outsideImportantPlanCount = outsideImportantPlanCount;
    }

    /** 
     * Returns the outsideImportantCallCount.
     * 
     * @return the outsideImportantCallCount
     */
    public BigDecimal getOutsideImportantCallCount() {
        return outsideImportantCallCount;
    }

    /** 
     * Sets the outsideImportantCallCount.
     * 
     * @param outsideImportantCallCount the outsideImportantCallCount
     */
    public void setOutsideImportantCallCount(BigDecimal outsideImportantCallCount) {
        this.outsideImportantCallCount = outsideImportantCallCount;
    }

    /** 
     * Returns the outsideImportantInfoCount.
     * 
     * @return the outsideImportantInfoCount
     */
    public BigDecimal getOutsideImportantInfoCount() {
        return outsideImportantInfoCount;
    }

    /** 
     * Sets the outsideImportantInfoCount.
     * 
     * @param outsideImportantInfoCount the outsideImportantInfoCount
     */
    public void setOutsideImportantInfoCount(BigDecimal outsideImportantInfoCount) {
        this.outsideImportantInfoCount = outsideImportantInfoCount;
    }

    /** 
     * Returns the outsideNewPlanCount.
     * 
     * @return the outsideNewPlanCount
     */
    public BigDecimal getOutsideNewPlanCount() {
        return outsideNewPlanCount;
    }

    /** 
     * Sets the outsideNewPlanCount.
     * 
     * @param outsideNewPlanCount the outsideNewPlanCount
     */
    public void setOutsideNewPlanCount(BigDecimal outsideNewPlanCount) {
        this.outsideNewPlanCount = outsideNewPlanCount;
    }

    /** 
     * Returns the outsideNewCallCount.
     * 
     * @return the outsideNewCallCount
     */
    public BigDecimal getOutsideNewCallCount() {
        return outsideNewCallCount;
    }

    /** 
     * Sets the outsideNewCallCount.
     * 
     * @param outsideNewCallCount the outsideNewCallCount
     */
    public void setOutsideNewCallCount(BigDecimal outsideNewCallCount) {
        this.outsideNewCallCount = outsideNewCallCount;
    }

    /** 
     * Returns the outsideNewInfoCount.
     * 
     * @return the outsideNewInfoCount
     */
    public BigDecimal getOutsideNewInfoCount() {
        return outsideNewInfoCount;
    }

    /** 
     * Sets the outsideNewInfoCount.
     * 
     * @param outsideNewInfoCount the outsideNewInfoCount
     */
    public void setOutsideNewInfoCount(BigDecimal outsideNewInfoCount) {
        this.outsideNewInfoCount = outsideNewInfoCount;
    }

    /** 
     * Returns the outsideDeepPlowingPlanCount.
     * 
     * @return the outsideDeepPlowingPlanCount
     */
    public BigDecimal getOutsideDeepPlowingPlanCount() {
        return outsideDeepPlowingPlanCount;
    }

    /** 
     * Sets the outsideDeepPlowingPlanCount.
     * 
     * @param outsideDeepPlowingPlanCount the outsideDeepPlowingPlanCount
     */
    public void setOutsideDeepPlowingPlanCount(BigDecimal outsideDeepPlowingPlanCount) {
        this.outsideDeepPlowingPlanCount = outsideDeepPlowingPlanCount;
    }

    /** 
     * Returns the outsideDeepPlowingCallCount.
     * 
     * @return the outsideDeepPlowingCallCount
     */
    public BigDecimal getOutsideDeepPlowingCallCount() {
        return outsideDeepPlowingCallCount;
    }

    /** 
     * Sets the outsideDeepPlowingCallCount.
     * 
     * @param outsideDeepPlowingCallCount the outsideDeepPlowingCallCount
     */
    public void setOutsideDeepPlowingCallCount(BigDecimal outsideDeepPlowingCallCount) {
        this.outsideDeepPlowingCallCount = outsideDeepPlowingCallCount;
    }

    /** 
     * Returns the outsideDeepPlowingInfoCount.
     * 
     * @return the outsideDeepPlowingInfoCount
     */
    public BigDecimal getOutsideDeepPlowingInfoCount() {
        return outsideDeepPlowingInfoCount;
    }

    /** 
     * Sets the outsideDeepPlowingInfoCount.
     * 
     * @param outsideDeepPlowingInfoCount the outsideDeepPlowingInfoCount
     */
    public void setOutsideDeepPlowingInfoCount(BigDecimal outsideDeepPlowingInfoCount) {
        this.outsideDeepPlowingInfoCount = outsideDeepPlowingInfoCount;
    }

    /** 
     * Returns the outsideYCpPlanCount.
     * 
     * @return the outsideYCpPlanCount
     */
    public BigDecimal getOutsideYCpPlanCount() {
        return outsideYCpPlanCount;
    }

    /** 
     * Sets the outsideYCpPlanCount.
     * 
     * @param outsideYCpPlanCount the outsideYCpPlanCount
     */
    public void setOutsideYCpPlanCount(BigDecimal outsideYCpPlanCount) {
        this.outsideYCpPlanCount = outsideYCpPlanCount;
    }

    /** 
     * Returns the outsideYCpCallCount.
     * 
     * @return the outsideYCpCallCount
     */
    public BigDecimal getOutsideYCpCallCount() {
        return outsideYCpCallCount;
    }

    /** 
     * Sets the outsideYCpCallCount.
     * 
     * @param outsideYCpCallCount the outsideYCpCallCount
     */
    public void setOutsideYCpCallCount(BigDecimal outsideYCpCallCount) {
        this.outsideYCpCallCount = outsideYCpCallCount;
    }

    /** 
     * Returns the outsideYCpInfoCount.
     * 
     * @return the outsideYCpInfoCount
     */
    public BigDecimal getOutsideYCpInfoCount() {
        return outsideYCpInfoCount;
    }

    /** 
     * Sets the outsideYCpInfoCount.
     * 
     * @param outsideYCpInfoCount the outsideYCpInfoCount
     */
    public void setOutsideYCpInfoCount(BigDecimal outsideYCpInfoCount) {
        this.outsideYCpInfoCount = outsideYCpInfoCount;
    }

    /** 
     * Returns the outsideOtherCpPlanCount.
     * 
     * @return the outsideOtherCpPlanCount
     */
    public BigDecimal getOutsideOtherCpPlanCount() {
        return outsideOtherCpPlanCount;
    }

    /** 
     * Sets the outsideOtherCpPlanCount.
     * 
     * @param outsideOtherCpPlanCount the outsideOtherCpPlanCount
     */
    public void setOutsideOtherCpPlanCount(BigDecimal outsideOtherCpPlanCount) {
        this.outsideOtherCpPlanCount = outsideOtherCpPlanCount;
    }

    /** 
     * Returns the outsideOtherCpCallCount.
     * 
     * @return the outsideOtherCpCallCount
     */
    public BigDecimal getOutsideOtherCpCallCount() {
        return outsideOtherCpCallCount;
    }

    /** 
     * Sets the outsideOtherCpCallCount.
     * 
     * @param outsideOtherCpCallCount the outsideOtherCpCallCount
     */
    public void setOutsideOtherCpCallCount(BigDecimal outsideOtherCpCallCount) {
        this.outsideOtherCpCallCount = outsideOtherCpCallCount;
    }

    /** 
     * Returns the outsideOtherCpInfoCount.
     * 
     * @return the outsideOtherCpInfoCount
     */
    public BigDecimal getOutsideOtherCpInfoCount() {
        return outsideOtherCpInfoCount;
    }

    /** 
     * Sets the outsideOtherCpInfoCount.
     * 
     * @param outsideOtherCpInfoCount the outsideOtherCpInfoCount
     */
    public void setOutsideOtherCpInfoCount(BigDecimal outsideOtherCpInfoCount) {
        this.outsideOtherCpInfoCount = outsideOtherCpInfoCount;
    }

    /** 
     * Returns the outsideGroupOnePlanCount.
     * 
     * @return the outsideGroupOnePlanCount
     */
    public BigDecimal getOutsideGroupOnePlanCount() {
        return outsideGroupOnePlanCount;
    }

    /** 
     * Sets the outsideGroupOnePlanCount.
     * 
     * @param outsideGroupOnePlanCount the outsideGroupOnePlanCount
     */
    public void setOutsideGroupOnePlanCount(BigDecimal outsideGroupOnePlanCount) {
        this.outsideGroupOnePlanCount = outsideGroupOnePlanCount;
    }

    /** 
     * Returns the outsideGroupOneCallCount.
     * 
     * @return the outsideGroupOneCallCount
     */
    public BigDecimal getOutsideGroupOneCallCount() {
        return outsideGroupOneCallCount;
    }

    /** 
     * Sets the outsideGroupOneCallCount.
     * 
     * @param outsideGroupOneCallCount the outsideGroupOneCallCount
     */
    public void setOutsideGroupOneCallCount(BigDecimal outsideGroupOneCallCount) {
        this.outsideGroupOneCallCount = outsideGroupOneCallCount;
    }

    /** 
     * Returns the outsideGroupOneInfoCount.
     * 
     * @return the outsideGroupOneInfoCount
     */
    public BigDecimal getOutsideGroupOneInfoCount() {
        return outsideGroupOneInfoCount;
    }

    /** 
     * Sets the outsideGroupOneInfoCount.
     * 
     * @param outsideGroupOneInfoCount the outsideGroupOneInfoCount
     */
    public void setOutsideGroupOneInfoCount(BigDecimal outsideGroupOneInfoCount) {
        this.outsideGroupOneInfoCount = outsideGroupOneInfoCount;
    }

    /** 
     * Returns the outsideGroupTwoPlanCount.
     * 
     * @return the outsideGroupTwoPlanCount
     */
    public BigDecimal getOutsideGroupTwoPlanCount() {
        return outsideGroupTwoPlanCount;
    }

    /** 
     * Sets the outsideGroupTwoPlanCount.
     * 
     * @param outsideGroupTwoPlanCount the outsideGroupTwoPlanCount
     */
    public void setOutsideGroupTwoPlanCount(BigDecimal outsideGroupTwoPlanCount) {
        this.outsideGroupTwoPlanCount = outsideGroupTwoPlanCount;
    }

    /** 
     * Returns the outsideGroupTwoCallCount.
     * 
     * @return the outsideGroupTwoCallCount
     */
    public BigDecimal getOutsideGroupTwoCallCount() {
        return outsideGroupTwoCallCount;
    }

    /** 
     * Sets the outsideGroupTwoCallCount.
     * 
     * @param outsideGroupTwoCallCount the outsideGroupTwoCallCount
     */
    public void setOutsideGroupTwoCallCount(BigDecimal outsideGroupTwoCallCount) {
        this.outsideGroupTwoCallCount = outsideGroupTwoCallCount;
    }

    /** 
     * Returns the outsideGroupTwoInfoCount.
     * 
     * @return the outsideGroupTwoInfoCount
     */
    public BigDecimal getOutsideGroupTwoInfoCount() {
        return outsideGroupTwoInfoCount;
    }

    /** 
     * Sets the outsideGroupTwoInfoCount.
     * 
     * @param outsideGroupTwoInfoCount the outsideGroupTwoInfoCount
     */
    public void setOutsideGroupTwoInfoCount(BigDecimal outsideGroupTwoInfoCount) {
        this.outsideGroupTwoInfoCount = outsideGroupTwoInfoCount;
    }

    /** 
     * Returns the outsideGroupThreePlanCount.
     * 
     * @return the outsideGroupThreePlanCount
     */
    public BigDecimal getOutsideGroupThreePlanCount() {
        return outsideGroupThreePlanCount;
    }

    /** 
     * Sets the outsideGroupThreePlanCount.
     * 
     * @param outsideGroupThreePlanCount the outsideGroupThreePlanCount
     */
    public void setOutsideGroupThreePlanCount(BigDecimal outsideGroupThreePlanCount) {
        this.outsideGroupThreePlanCount = outsideGroupThreePlanCount;
    }

    /** 
     * Returns the outsideGroupThreeCallCount.
     * 
     * @return the outsideGroupThreeCallCount
     */
    public BigDecimal getOutsideGroupThreeCallCount() {
        return outsideGroupThreeCallCount;
    }

    /** 
     * Sets the outsideGroupThreeCallCount.
     * 
     * @param outsideGroupThreeCallCount the outsideGroupThreeCallCount
     */
    public void setOutsideGroupThreeCallCount(BigDecimal outsideGroupThreeCallCount) {
        this.outsideGroupThreeCallCount = outsideGroupThreeCallCount;
    }

    /** 
     * Returns the outsideGroupThreeInfoCount.
     * 
     * @return the outsideGroupThreeInfoCount
     */
    public BigDecimal getOutsideGroupThreeInfoCount() {
        return outsideGroupThreeInfoCount;
    }

    /** 
     * Sets the outsideGroupThreeInfoCount.
     * 
     * @param outsideGroupThreeInfoCount the outsideGroupThreeInfoCount
     */
    public void setOutsideGroupThreeInfoCount(BigDecimal outsideGroupThreeInfoCount) {
        this.outsideGroupThreeInfoCount = outsideGroupThreeInfoCount;
    }

    /** 
     * Returns the outsideGroupFourPlanCount.
     * 
     * @return the outsideGroupFourPlanCount
     */
    public BigDecimal getOutsideGroupFourPlanCount() {
        return outsideGroupFourPlanCount;
    }

    /** 
     * Sets the outsideGroupFourPlanCount.
     * 
     * @param outsideGroupFourPlanCount the outsideGroupFourPlanCount
     */
    public void setOutsideGroupFourPlanCount(BigDecimal outsideGroupFourPlanCount) {
        this.outsideGroupFourPlanCount = outsideGroupFourPlanCount;
    }

    /** 
     * Returns the outsideGroupFourCallCount.
     * 
     * @return the outsideGroupFourCallCount
     */
    public BigDecimal getOutsideGroupFourCallCount() {
        return outsideGroupFourCallCount;
    }

    /** 
     * Sets the outsideGroupFourCallCount.
     * 
     * @param outsideGroupFourCallCount the outsideGroupFourCallCount
     */
    public void setOutsideGroupFourCallCount(BigDecimal outsideGroupFourCallCount) {
        this.outsideGroupFourCallCount = outsideGroupFourCallCount;
    }

    /** 
     * Returns the outsideGroupFourInfoCount.
     * 
     * @return the outsideGroupFourInfoCount
     */
    public BigDecimal getOutsideGroupFourInfoCount() {
        return outsideGroupFourInfoCount;
    }

    /** 
     * Sets the outsideGroupFourInfoCount.
     * 
     * @param outsideGroupFourInfoCount the outsideGroupFourInfoCount
     */
    public void setOutsideGroupFourInfoCount(BigDecimal outsideGroupFourInfoCount) {
        this.outsideGroupFourInfoCount = outsideGroupFourInfoCount;
    }

    /** 
     * Returns the outsideGroupFivePlanCount.
     * 
     * @return the outsideGroupFivePlanCount
     */
    public BigDecimal getOutsideGroupFivePlanCount() {
        return outsideGroupFivePlanCount;
    }

    /** 
     * Sets the outsideGroupFivePlanCount.
     * 
     * @param outsideGroupFivePlanCount the outsideGroupFivePlanCount
     */
    public void setOutsideGroupFivePlanCount(BigDecimal outsideGroupFivePlanCount) {
        this.outsideGroupFivePlanCount = outsideGroupFivePlanCount;
    }

    /** 
     * Returns the outsideGroupFiveCallCount.
     * 
     * @return the outsideGroupFiveCallCount
     */
    public BigDecimal getOutsideGroupFiveCallCount() {
        return outsideGroupFiveCallCount;
    }

    /** 
     * Sets the outsideGroupFiveCallCount.
     * 
     * @param outsideGroupFiveCallCount the outsideGroupFiveCallCount
     */
    public void setOutsideGroupFiveCallCount(BigDecimal outsideGroupFiveCallCount) {
        this.outsideGroupFiveCallCount = outsideGroupFiveCallCount;
    }

    /** 
     * Returns the outsideGroupFiveInfoCount.
     * 
     * @return the outsideGroupFiveInfoCount
     */
    public BigDecimal getOutsideGroupFiveInfoCount() {
        return outsideGroupFiveInfoCount;
    }

    /** 
     * Sets the outsideGroupFiveInfoCount.
     * 
     * @param outsideGroupFiveInfoCount the outsideGroupFiveInfoCount
     */
    public void setOutsideGroupFiveInfoCount(BigDecimal outsideGroupFiveInfoCount) {
        this.outsideGroupFiveInfoCount = outsideGroupFiveInfoCount;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}