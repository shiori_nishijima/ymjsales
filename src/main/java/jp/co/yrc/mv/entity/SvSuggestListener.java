package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SvSuggestListener implements EntityListener<SvSuggest> {

    @Override
    public void preInsert(SvSuggest entity, PreInsertContext<SvSuggest> context) {
    }

    @Override
    public void preUpdate(SvSuggest entity, PreUpdateContext<SvSuggest> context) {
    }

    @Override
    public void preDelete(SvSuggest entity, PreDeleteContext<SvSuggest> context) {
    }

    @Override
    public void postInsert(SvSuggest entity, PostInsertContext<SvSuggest> context) {
    }

    @Override
    public void postUpdate(SvSuggest entity, PostUpdateContext<SvSuggest> context) {
    }

    @Override
    public void postDelete(SvSuggest entity, PostDeleteContext<SvSuggest> context) {
    }
}