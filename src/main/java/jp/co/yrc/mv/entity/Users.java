package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = UsersListener.class)
@Table(name = "users")
public class Users {

    /** ユーザID */
    @Id
    @Column(name = "id")
    String id;

    /** ユーザID名（取引先マスタ用） */
    @Column(name = "id_for_customer")
    String idForCustomer;

    /** パスワード */
    @Column(name = "user_hash")
    String userHash;

    /** 名 */
    @Column(name = "first_name")
    String firstName;

    /** 姓 */
    @Column(name = "last_name")
    String lastName;

    /** 権限 */
    @Column(name = "authorization")
    String authorization;

    /** 所属会社 */
    @Column(name = "company")
    String company;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the idForCustomer.
     * 
     * @return the idForCustomer
     */
    public String getIdForCustomer() {
        return idForCustomer;
    }

    /** 
     * Sets the idForCustomer.
     * 
     * @param idForCustomer the idForCustomer
     */
    public void setIdForCustomer(String idForCustomer) {
        this.idForCustomer = idForCustomer;
    }

    /** 
     * Returns the userHash.
     * 
     * @return the userHash
     */
    public String getUserHash() {
        return userHash;
    }

    /** 
     * Sets the userHash.
     * 
     * @param userHash the userHash
     */
    public void setUserHash(String userHash) {
        this.userHash = userHash;
    }

    /** 
     * Returns the firstName.
     * 
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /** 
     * Sets the firstName.
     * 
     * @param firstName the firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /** 
     * Returns the lastName.
     * 
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /** 
     * Sets the lastName.
     * 
     * @param lastName the lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /** 
     * Returns the authorization.
     * 
     * @return the authorization
     */
    public String getAuthorization() {
        return authorization;
    }

    /** 
     * Sets the company.
     * 
     * @param company the company
     */
    public void setCompany(String company) {
        this.company = company;
    }
    
    /** 
     * Returns the company.
     * 
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /** 
     * Sets the authorization.
     * 
     * @param authorization the authorization
     */
    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}