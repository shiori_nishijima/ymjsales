package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = CompassKindsListener.class)
@Table(name = "compass_kinds")
public class CompassKinds {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 大分類1 */
    @Column(name = "large_class1")
    String largeClass1;

    /** 大分類2 */
    @Column(name = "large_class2")
    String largeClass2;

    /** 中分類 */
    @Column(name = "middle_class")
    String middleClass;

    /** 小分類 */
    @Column(name = "small_class")
    String smallClass;

    /** シリーズコード */
    @Column(name = "series_cd")
    String seriesCd;

    /** スノー区分 */
    @Column(name = "snow_div")
    String snowDiv;

    /** 削除フラグ */
    @Column(name = "deleted")
    byte[] deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the largeClass1.
     * 
     * @return the largeClass1
     */
    public String getLargeClass1() {
        return largeClass1;
    }

    /** 
     * Sets the largeClass1.
     * 
     * @param largeClass1 the largeClass1
     */
    public void setLargeClass1(String largeClass1) {
        this.largeClass1 = largeClass1;
    }

    /** 
     * Returns the largeClass2.
     * 
     * @return the largeClass2
     */
    public String getLargeClass2() {
        return largeClass2;
    }

    /** 
     * Sets the largeClass2.
     * 
     * @param largeClass2 the largeClass2
     */
    public void setLargeClass2(String largeClass2) {
        this.largeClass2 = largeClass2;
    }

    /** 
     * Returns the middleClass.
     * 
     * @return the middleClass
     */
    public String getMiddleClass() {
        return middleClass;
    }

    /** 
     * Sets the middleClass.
     * 
     * @param middleClass the middleClass
     */
    public void setMiddleClass(String middleClass) {
        this.middleClass = middleClass;
    }

    /** 
     * Returns the smallClass.
     * 
     * @return the smallClass
     */
    public String getSmallClass() {
        return smallClass;
    }

    /** 
     * Sets the smallClass.
     * 
     * @param smallClass the smallClass
     */
    public void setSmallClass(String smallClass) {
        this.smallClass = smallClass;
    }

    /** 
     * Returns the seriesCd.
     * 
     * @return the seriesCd
     */
    public String getSeriesCd() {
        return seriesCd;
    }

    /** 
     * Sets the seriesCd.
     * 
     * @param seriesCd the seriesCd
     */
    public void setSeriesCd(String seriesCd) {
        this.seriesCd = seriesCd;
    }

    /** 
     * Returns the snowDiv.
     * 
     * @return the snowDiv
     */
    public String getSnowDiv() {
        return snowDiv;
    }

    /** 
     * Sets the snowDiv.
     * 
     * @param snowDiv the snowDiv
     */
    public void setSnowDiv(String snowDiv) {
        this.snowDiv = snowDiv;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public byte[] getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(byte[] deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}