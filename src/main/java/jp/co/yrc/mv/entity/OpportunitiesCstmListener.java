package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OpportunitiesCstmListener implements EntityListener<OpportunitiesCstm> {

    @Override
    public void preInsert(OpportunitiesCstm entity, PreInsertContext<OpportunitiesCstm> context) {
    }

    @Override
    public void preUpdate(OpportunitiesCstm entity, PreUpdateContext<OpportunitiesCstm> context) {
    }

    @Override
    public void preDelete(OpportunitiesCstm entity, PreDeleteContext<OpportunitiesCstm> context) {
    }

    @Override
    public void postInsert(OpportunitiesCstm entity, PostInsertContext<OpportunitiesCstm> context) {
    }

    @Override
    public void postUpdate(OpportunitiesCstm entity, PostUpdateContext<OpportunitiesCstm> context) {
    }

    @Override
    public void postDelete(OpportunitiesCstm entity, PostDeleteContext<OpportunitiesCstm> context) {
    }
}