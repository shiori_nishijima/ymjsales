package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyMarketPlansListener.class)
@Table(name = "monthly_market_plans")
public class MonthlyMarketPlans {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** SS売上 */
    @Column(name = "ss_sales")
    BigDecimal ssSales;

    /** SS粗利 */
    @Column(name = "ss_margin")
    BigDecimal ssMargin;

    /** SS本数 */
    @Column(name = "ss_number")
    BigDecimal ssNumber;

    /** RS売上 */
    @Column(name = "rs_sales")
    BigDecimal rsSales;

    /** RS粗利 */
    @Column(name = "rs_margin")
    BigDecimal rsMargin;

    /** RS本数 */
    @Column(name = "rs_number")
    BigDecimal rsNumber;

    /** CD売上 */
    @Column(name = "cd_sales")
    BigDecimal cdSales;

    /** CD粗利 */
    @Column(name = "cd_margin")
    BigDecimal cdMargin;

    /** CD本数 */
    @Column(name = "cd_number")
    BigDecimal cdNumber;

    /** PS売上 */
    @Column(name = "ps_sales")
    BigDecimal psSales;

    /** PS粗利 */
    @Column(name = "ps_margin")
    BigDecimal psMargin;

    /** PS本数 */
    @Column(name = "ps_number")
    BigDecimal psNumber;

    /** CS売上 */
    @Column(name = "cs_sales")
    BigDecimal csSales;

    /** CS粗利 */
    @Column(name = "cs_margin")
    BigDecimal csMargin;

    /** CS本数 */
    @Column(name = "cs_number")
    BigDecimal csNumber;

    /** HC売上 */
    @Column(name = "hc_sales")
    BigDecimal hcSales;

    /** HC粗利 */
    @Column(name = "hc_margin")
    BigDecimal hcMargin;

    /** HC本数 */
    @Column(name = "hc_number")
    BigDecimal hcNumber;

    /** リース売上 */
    @Column(name = "lease_sales")
    BigDecimal leaseSales;

    /** リース粗利 */
    @Column(name = "lease_margin")
    BigDecimal leaseMargin;

    /** リース本数 */
    @Column(name = "lease_number")
    BigDecimal leaseNumber;

    /** 間他売上 */
    @Column(name = "indirect_other_sales")
    BigDecimal indirectOtherSales;

    /** 間他粗利 */
    @Column(name = "indirect_other_margin")
    BigDecimal indirectOtherMargin;

    /** 間他本数 */
    @Column(name = "indirect_other_number")
    BigDecimal indirectOtherNumber;

    /** トラック売上 */
    @Column(name = "truck_sales")
    BigDecimal truckSales;

    /** トラック粗利 */
    @Column(name = "truck_margin")
    BigDecimal truckMargin;

    /** トラック本数 */
    @Column(name = "truck_number")
    BigDecimal truckNumber;

    /** バス売上 */
    @Column(name = "bus_sales")
    BigDecimal busSales;

    /** バス粗利 */
    @Column(name = "bus_margin")
    BigDecimal busMargin;

    /** バス本数 */
    @Column(name = "bus_number")
    BigDecimal busNumber;

    /** ハイタク売上 */
    @Column(name = "hire_taxi_sales")
    BigDecimal hireTaxiSales;

    /** ハイタク粗利 */
    @Column(name = "hire_taxi_margin")
    BigDecimal hireTaxiMargin;

    /** ハイタク本数 */
    @Column(name = "hire_taxi_number")
    BigDecimal hireTaxiNumber;

    /** 直他売上 */
    @Column(name = "direct_other_sales")
    BigDecimal directOtherSales;

    /** 直他粗利 */
    @Column(name = "direct_other_margin")
    BigDecimal directOtherMargin;

    /** 直他本数 */
    @Column(name = "direct_other_number")
    BigDecimal directOtherNumber;

    /** 小売売上 */
    @Column(name = "retail_sales")
    BigDecimal retailSales;

    /** 小売粗利 */
    @Column(name = "retail_margin")
    BigDecimal retailMargin;

    /** 小売本数 */
    @Column(name = "retail_number")
    BigDecimal retailNumber;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the ssSales.
     * 
     * @return the ssSales
     */
    public BigDecimal getSsSales() {
        return ssSales;
    }

    /** 
     * Sets the ssSales.
     * 
     * @param ssSales the ssSales
     */
    public void setSsSales(BigDecimal ssSales) {
        this.ssSales = ssSales;
    }

    /** 
     * Returns the ssMargin.
     * 
     * @return the ssMargin
     */
    public BigDecimal getSsMargin() {
        return ssMargin;
    }

    /** 
     * Sets the ssMargin.
     * 
     * @param ssMargin the ssMargin
     */
    public void setSsMargin(BigDecimal ssMargin) {
        this.ssMargin = ssMargin;
    }

    /** 
     * Returns the ssNumber.
     * 
     * @return the ssNumber
     */
    public BigDecimal getSsNumber() {
        return ssNumber;
    }

    /** 
     * Sets the ssNumber.
     * 
     * @param ssNumber the ssNumber
     */
    public void setSsNumber(BigDecimal ssNumber) {
        this.ssNumber = ssNumber;
    }

    /** 
     * Returns the rsSales.
     * 
     * @return the rsSales
     */
    public BigDecimal getRsSales() {
        return rsSales;
    }

    /** 
     * Sets the rsSales.
     * 
     * @param rsSales the rsSales
     */
    public void setRsSales(BigDecimal rsSales) {
        this.rsSales = rsSales;
    }

    /** 
     * Returns the rsMargin.
     * 
     * @return the rsMargin
     */
    public BigDecimal getRsMargin() {
        return rsMargin;
    }

    /** 
     * Sets the rsMargin.
     * 
     * @param rsMargin the rsMargin
     */
    public void setRsMargin(BigDecimal rsMargin) {
        this.rsMargin = rsMargin;
    }

    /** 
     * Returns the rsNumber.
     * 
     * @return the rsNumber
     */
    public BigDecimal getRsNumber() {
        return rsNumber;
    }

    /** 
     * Sets the rsNumber.
     * 
     * @param rsNumber the rsNumber
     */
    public void setRsNumber(BigDecimal rsNumber) {
        this.rsNumber = rsNumber;
    }

    /** 
     * Returns the cdSales.
     * 
     * @return the cdSales
     */
    public BigDecimal getCdSales() {
        return cdSales;
    }

    /** 
     * Sets the cdSales.
     * 
     * @param cdSales the cdSales
     */
    public void setCdSales(BigDecimal cdSales) {
        this.cdSales = cdSales;
    }

    /** 
     * Returns the cdMargin.
     * 
     * @return the cdMargin
     */
    public BigDecimal getCdMargin() {
        return cdMargin;
    }

    /** 
     * Sets the cdMargin.
     * 
     * @param cdMargin the cdMargin
     */
    public void setCdMargin(BigDecimal cdMargin) {
        this.cdMargin = cdMargin;
    }

    /** 
     * Returns the cdNumber.
     * 
     * @return the cdNumber
     */
    public BigDecimal getCdNumber() {
        return cdNumber;
    }

    /** 
     * Sets the cdNumber.
     * 
     * @param cdNumber the cdNumber
     */
    public void setCdNumber(BigDecimal cdNumber) {
        this.cdNumber = cdNumber;
    }

    /** 
     * Returns the psSales.
     * 
     * @return the psSales
     */
    public BigDecimal getPsSales() {
        return psSales;
    }

    /** 
     * Sets the psSales.
     * 
     * @param psSales the psSales
     */
    public void setPsSales(BigDecimal psSales) {
        this.psSales = psSales;
    }

    /** 
     * Returns the psMargin.
     * 
     * @return the psMargin
     */
    public BigDecimal getPsMargin() {
        return psMargin;
    }

    /** 
     * Sets the psMargin.
     * 
     * @param psMargin the psMargin
     */
    public void setPsMargin(BigDecimal psMargin) {
        this.psMargin = psMargin;
    }

    /** 
     * Returns the psNumber.
     * 
     * @return the psNumber
     */
    public BigDecimal getPsNumber() {
        return psNumber;
    }

    /** 
     * Sets the psNumber.
     * 
     * @param psNumber the psNumber
     */
    public void setPsNumber(BigDecimal psNumber) {
        this.psNumber = psNumber;
    }

    /** 
     * Returns the csSales.
     * 
     * @return the csSales
     */
    public BigDecimal getCsSales() {
        return csSales;
    }

    /** 
     * Sets the csSales.
     * 
     * @param csSales the csSales
     */
    public void setCsSales(BigDecimal csSales) {
        this.csSales = csSales;
    }

    /** 
     * Returns the csMargin.
     * 
     * @return the csMargin
     */
    public BigDecimal getCsMargin() {
        return csMargin;
    }

    /** 
     * Sets the csMargin.
     * 
     * @param csMargin the csMargin
     */
    public void setCsMargin(BigDecimal csMargin) {
        this.csMargin = csMargin;
    }

    /** 
     * Returns the csNumber.
     * 
     * @return the csNumber
     */
    public BigDecimal getCsNumber() {
        return csNumber;
    }

    /** 
     * Sets the csNumber.
     * 
     * @param csNumber the csNumber
     */
    public void setCsNumber(BigDecimal csNumber) {
        this.csNumber = csNumber;
    }

    /** 
     * Returns the hcSales.
     * 
     * @return the hcSales
     */
    public BigDecimal getHcSales() {
        return hcSales;
    }

    /** 
     * Sets the hcSales.
     * 
     * @param hcSales the hcSales
     */
    public void setHcSales(BigDecimal hcSales) {
        this.hcSales = hcSales;
    }

    /** 
     * Returns the hcMargin.
     * 
     * @return the hcMargin
     */
    public BigDecimal getHcMargin() {
        return hcMargin;
    }

    /** 
     * Sets the hcMargin.
     * 
     * @param hcMargin the hcMargin
     */
    public void setHcMargin(BigDecimal hcMargin) {
        this.hcMargin = hcMargin;
    }

    /** 
     * Returns the hcNumber.
     * 
     * @return the hcNumber
     */
    public BigDecimal getHcNumber() {
        return hcNumber;
    }

    /** 
     * Sets the hcNumber.
     * 
     * @param hcNumber the hcNumber
     */
    public void setHcNumber(BigDecimal hcNumber) {
        this.hcNumber = hcNumber;
    }

    /** 
     * Returns the leaseSales.
     * 
     * @return the leaseSales
     */
    public BigDecimal getLeaseSales() {
        return leaseSales;
    }

    /** 
     * Sets the leaseSales.
     * 
     * @param leaseSales the leaseSales
     */
    public void setLeaseSales(BigDecimal leaseSales) {
        this.leaseSales = leaseSales;
    }

    /** 
     * Returns the leaseMargin.
     * 
     * @return the leaseMargin
     */
    public BigDecimal getLeaseMargin() {
        return leaseMargin;
    }

    /** 
     * Sets the leaseMargin.
     * 
     * @param leaseMargin the leaseMargin
     */
    public void setLeaseMargin(BigDecimal leaseMargin) {
        this.leaseMargin = leaseMargin;
    }

    /** 
     * Returns the leaseNumber.
     * 
     * @return the leaseNumber
     */
    public BigDecimal getLeaseNumber() {
        return leaseNumber;
    }

    /** 
     * Sets the leaseNumber.
     * 
     * @param leaseNumber the leaseNumber
     */
    public void setLeaseNumber(BigDecimal leaseNumber) {
        this.leaseNumber = leaseNumber;
    }

    /** 
     * Returns the indirectOtherSales.
     * 
     * @return the indirectOtherSales
     */
    public BigDecimal getIndirectOtherSales() {
        return indirectOtherSales;
    }

    /** 
     * Sets the indirectOtherSales.
     * 
     * @param indirectOtherSales the indirectOtherSales
     */
    public void setIndirectOtherSales(BigDecimal indirectOtherSales) {
        this.indirectOtherSales = indirectOtherSales;
    }

    /** 
     * Returns the indirectOtherMargin.
     * 
     * @return the indirectOtherMargin
     */
    public BigDecimal getIndirectOtherMargin() {
        return indirectOtherMargin;
    }

    /** 
     * Sets the indirectOtherMargin.
     * 
     * @param indirectOtherMargin the indirectOtherMargin
     */
    public void setIndirectOtherMargin(BigDecimal indirectOtherMargin) {
        this.indirectOtherMargin = indirectOtherMargin;
    }

    /** 
     * Returns the indirectOtherNumber.
     * 
     * @return the indirectOtherNumber
     */
    public BigDecimal getIndirectOtherNumber() {
        return indirectOtherNumber;
    }

    /** 
     * Sets the indirectOtherNumber.
     * 
     * @param indirectOtherNumber the indirectOtherNumber
     */
    public void setIndirectOtherNumber(BigDecimal indirectOtherNumber) {
        this.indirectOtherNumber = indirectOtherNumber;
    }

    /** 
     * Returns the truckSales.
     * 
     * @return the truckSales
     */
    public BigDecimal getTruckSales() {
        return truckSales;
    }

    /** 
     * Sets the truckSales.
     * 
     * @param truckSales the truckSales
     */
    public void setTruckSales(BigDecimal truckSales) {
        this.truckSales = truckSales;
    }

    /** 
     * Returns the truckMargin.
     * 
     * @return the truckMargin
     */
    public BigDecimal getTruckMargin() {
        return truckMargin;
    }

    /** 
     * Sets the truckMargin.
     * 
     * @param truckMargin the truckMargin
     */
    public void setTruckMargin(BigDecimal truckMargin) {
        this.truckMargin = truckMargin;
    }

    /** 
     * Returns the truckNumber.
     * 
     * @return the truckNumber
     */
    public BigDecimal getTruckNumber() {
        return truckNumber;
    }

    /** 
     * Sets the truckNumber.
     * 
     * @param truckNumber the truckNumber
     */
    public void setTruckNumber(BigDecimal truckNumber) {
        this.truckNumber = truckNumber;
    }

    /** 
     * Returns the busSales.
     * 
     * @return the busSales
     */
    public BigDecimal getBusSales() {
        return busSales;
    }

    /** 
     * Sets the busSales.
     * 
     * @param busSales the busSales
     */
    public void setBusSales(BigDecimal busSales) {
        this.busSales = busSales;
    }

    /** 
     * Returns the busMargin.
     * 
     * @return the busMargin
     */
    public BigDecimal getBusMargin() {
        return busMargin;
    }

    /** 
     * Sets the busMargin.
     * 
     * @param busMargin the busMargin
     */
    public void setBusMargin(BigDecimal busMargin) {
        this.busMargin = busMargin;
    }

    /** 
     * Returns the busNumber.
     * 
     * @return the busNumber
     */
    public BigDecimal getBusNumber() {
        return busNumber;
    }

    /** 
     * Sets the busNumber.
     * 
     * @param busNumber the busNumber
     */
    public void setBusNumber(BigDecimal busNumber) {
        this.busNumber = busNumber;
    }

    /** 
     * Returns the hireTaxiSales.
     * 
     * @return the hireTaxiSales
     */
    public BigDecimal getHireTaxiSales() {
        return hireTaxiSales;
    }

    /** 
     * Sets the hireTaxiSales.
     * 
     * @param hireTaxiSales the hireTaxiSales
     */
    public void setHireTaxiSales(BigDecimal hireTaxiSales) {
        this.hireTaxiSales = hireTaxiSales;
    }

    /** 
     * Returns the hireTaxiMargin.
     * 
     * @return the hireTaxiMargin
     */
    public BigDecimal getHireTaxiMargin() {
        return hireTaxiMargin;
    }

    /** 
     * Sets the hireTaxiMargin.
     * 
     * @param hireTaxiMargin the hireTaxiMargin
     */
    public void setHireTaxiMargin(BigDecimal hireTaxiMargin) {
        this.hireTaxiMargin = hireTaxiMargin;
    }

    /** 
     * Returns the hireTaxiNumber.
     * 
     * @return the hireTaxiNumber
     */
    public BigDecimal getHireTaxiNumber() {
        return hireTaxiNumber;
    }

    /** 
     * Sets the hireTaxiNumber.
     * 
     * @param hireTaxiNumber the hireTaxiNumber
     */
    public void setHireTaxiNumber(BigDecimal hireTaxiNumber) {
        this.hireTaxiNumber = hireTaxiNumber;
    }

    /** 
     * Returns the directOtherSales.
     * 
     * @return the directOtherSales
     */
    public BigDecimal getDirectOtherSales() {
        return directOtherSales;
    }

    /** 
     * Sets the directOtherSales.
     * 
     * @param directOtherSales the directOtherSales
     */
    public void setDirectOtherSales(BigDecimal directOtherSales) {
        this.directOtherSales = directOtherSales;
    }

    /** 
     * Returns the directOtherMargin.
     * 
     * @return the directOtherMargin
     */
    public BigDecimal getDirectOtherMargin() {
        return directOtherMargin;
    }

    /** 
     * Sets the directOtherMargin.
     * 
     * @param directOtherMargin the directOtherMargin
     */
    public void setDirectOtherMargin(BigDecimal directOtherMargin) {
        this.directOtherMargin = directOtherMargin;
    }

    /** 
     * Returns the directOtherNumber.
     * 
     * @return the directOtherNumber
     */
    public BigDecimal getDirectOtherNumber() {
        return directOtherNumber;
    }

    /** 
     * Sets the directOtherNumber.
     * 
     * @param directOtherNumber the directOtherNumber
     */
    public void setDirectOtherNumber(BigDecimal directOtherNumber) {
        this.directOtherNumber = directOtherNumber;
    }

    /** 
     * Returns the retailSales.
     * 
     * @return the retailSales
     */
    public BigDecimal getRetailSales() {
        return retailSales;
    }

    /** 
     * Sets the retailSales.
     * 
     * @param retailSales the retailSales
     */
    public void setRetailSales(BigDecimal retailSales) {
        this.retailSales = retailSales;
    }

    /** 
     * Returns the retailMargin.
     * 
     * @return the retailMargin
     */
    public BigDecimal getRetailMargin() {
        return retailMargin;
    }

    /** 
     * Sets the retailMargin.
     * 
     * @param retailMargin the retailMargin
     */
    public void setRetailMargin(BigDecimal retailMargin) {
        this.retailMargin = retailMargin;
    }

    /** 
     * Returns the retailNumber.
     * 
     * @return the retailNumber
     */
    public BigDecimal getRetailNumber() {
        return retailNumber;
    }

    /** 
     * Sets the retailNumber.
     * 
     * @param retailNumber the retailNumber
     */
    public void setRetailNumber(BigDecimal retailNumber) {
        this.retailNumber = retailNumber;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}