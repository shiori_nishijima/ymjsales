package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsUsersListener implements EntityListener<CallsUsers> {

    @Override
    public void preInsert(CallsUsers entity, PreInsertContext<CallsUsers> context) {
    }

    @Override
    public void preUpdate(CallsUsers entity, PreUpdateContext<CallsUsers> context) {
    }

    @Override
    public void preDelete(CallsUsers entity, PreDeleteContext<CallsUsers> context) {
    }

    @Override
    public void postInsert(CallsUsers entity, PostInsertContext<CallsUsers> context) {
    }

    @Override
    public void postUpdate(CallsUsers entity, PostUpdateContext<CallsUsers> context) {
    }

    @Override
    public void postDelete(CallsUsers entity, PostDeleteContext<CallsUsers> context) {
    }
}