package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SvCancelListener implements EntityListener<SvCancel> {

    @Override
    public void preInsert(SvCancel entity, PreInsertContext<SvCancel> context) {
    }

    @Override
    public void preUpdate(SvCancel entity, PreUpdateContext<SvCancel> context) {
    }

    @Override
    public void preDelete(SvCancel entity, PreDeleteContext<SvCancel> context) {
    }

    @Override
    public void postInsert(SvCancel entity, PostInsertContext<SvCancel> context) {
    }

    @Override
    public void postUpdate(SvCancel entity, PostUpdateContext<SvCancel> context) {
    }

    @Override
    public void postDelete(SvCancel entity, PostDeleteContext<SvCancel> context) {
    }
}