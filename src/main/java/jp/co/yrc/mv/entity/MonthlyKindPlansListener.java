package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyKindPlansListener implements EntityListener<MonthlyKindPlans> {

    @Override
    public void preInsert(MonthlyKindPlans entity, PreInsertContext<MonthlyKindPlans> context) {
    }

    @Override
    public void preUpdate(MonthlyKindPlans entity, PreUpdateContext<MonthlyKindPlans> context) {
    }

    @Override
    public void preDelete(MonthlyKindPlans entity, PreDeleteContext<MonthlyKindPlans> context) {
    }

    @Override
    public void postInsert(MonthlyKindPlans entity, PostInsertContext<MonthlyKindPlans> context) {
    }

    @Override
    public void postUpdate(MonthlyKindPlans entity, PostUpdateContext<MonthlyKindPlans> context) {
    }

    @Override
    public void postDelete(MonthlyKindPlans entity, PostDeleteContext<MonthlyKindPlans> context) {
    }
}