package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class KeymansListener implements EntityListener<Keymans> {

    @Override
    public void preInsert(Keymans entity, PreInsertContext<Keymans> context) {
    }

    @Override
    public void preUpdate(Keymans entity, PreUpdateContext<Keymans> context) {
    }

    @Override
    public void preDelete(Keymans entity, PreDeleteContext<Keymans> context) {
    }

    @Override
    public void postInsert(Keymans entity, PostInsertContext<Keymans> context) {
    }

    @Override
    public void postUpdate(Keymans entity, PostUpdateContext<Keymans> context) {
    }

    @Override
    public void postDelete(Keymans entity, PostDeleteContext<Keymans> context) {
    }
}