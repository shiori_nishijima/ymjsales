package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class DailyCommentsListener implements EntityListener<DailyComments> {

    @Override
    public void preInsert(DailyComments entity, PreInsertContext<DailyComments> context) {
    }

    @Override
    public void preUpdate(DailyComments entity, PreUpdateContext<DailyComments> context) {
    }

    @Override
    public void preDelete(DailyComments entity, PreDeleteContext<DailyComments> context) {
    }

    @Override
    public void postInsert(DailyComments entity, PostInsertContext<DailyComments> context) {
    }

    @Override
    public void postUpdate(DailyComments entity, PostUpdateContext<DailyComments> context) {
    }

    @Override
    public void postDelete(DailyComments entity, PostDeleteContext<DailyComments> context) {
    }
}