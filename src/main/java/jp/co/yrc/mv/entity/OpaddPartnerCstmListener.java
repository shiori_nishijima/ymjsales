package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OpaddPartnerCstmListener implements EntityListener<OpaddPartnerCstm> {

    @Override
    public void preInsert(OpaddPartnerCstm entity, PreInsertContext<OpaddPartnerCstm> context) {
    }

    @Override
    public void preUpdate(OpaddPartnerCstm entity, PreUpdateContext<OpaddPartnerCstm> context) {
    }

    @Override
    public void preDelete(OpaddPartnerCstm entity, PreDeleteContext<OpaddPartnerCstm> context) {
    }

    @Override
    public void postInsert(OpaddPartnerCstm entity, PostInsertContext<OpaddPartnerCstm> context) {
    }

    @Override
    public void postUpdate(OpaddPartnerCstm entity, PostUpdateContext<OpaddPartnerCstm> context) {
    }

    @Override
    public void postDelete(OpaddPartnerCstm entity, PostDeleteContext<OpaddPartnerCstm> context) {
    }
}