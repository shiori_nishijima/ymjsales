package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpUsersListener implements EntityListener<TmpUsers> {

    @Override
    public void preInsert(TmpUsers entity, PreInsertContext<TmpUsers> context) {
    }

    @Override
    public void preUpdate(TmpUsers entity, PreUpdateContext<TmpUsers> context) {
    }

    @Override
    public void preDelete(TmpUsers entity, PreDeleteContext<TmpUsers> context) {
    }

    @Override
    public void postInsert(TmpUsers entity, PostInsertContext<TmpUsers> context) {
    }

    @Override
    public void postUpdate(TmpUsers entity, PostUpdateContext<TmpUsers> context) {
    }

    @Override
    public void postDelete(TmpUsers entity, PostDeleteContext<TmpUsers> context) {
    }
}