package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SelectionItemsListener implements EntityListener<SelectionItems> {

    @Override
    public void preInsert(SelectionItems entity, PreInsertContext<SelectionItems> context) {
    }

    @Override
    public void preUpdate(SelectionItems entity, PreUpdateContext<SelectionItems> context) {
    }

    @Override
    public void preDelete(SelectionItems entity, PreDeleteContext<SelectionItems> context) {
    }

    @Override
    public void postInsert(SelectionItems entity, PostInsertContext<SelectionItems> context) {
    }

    @Override
    public void postUpdate(SelectionItems entity, PostUpdateContext<SelectionItems> context) {
    }

    @Override
    public void postDelete(SelectionItems entity, PostDeleteContext<SelectionItems> context) {
    }
}