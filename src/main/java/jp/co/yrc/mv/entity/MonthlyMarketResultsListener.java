package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyMarketResultsListener implements EntityListener<MonthlyMarketResults> {

    @Override
    public void preInsert(MonthlyMarketResults entity, PreInsertContext<MonthlyMarketResults> context) {
    }

    @Override
    public void preUpdate(MonthlyMarketResults entity, PreUpdateContext<MonthlyMarketResults> context) {
    }

    @Override
    public void preDelete(MonthlyMarketResults entity, PreDeleteContext<MonthlyMarketResults> context) {
    }

    @Override
    public void postInsert(MonthlyMarketResults entity, PostInsertContext<MonthlyMarketResults> context) {
    }

    @Override
    public void postUpdate(MonthlyMarketResults entity, PostUpdateContext<MonthlyMarketResults> context) {
    }

    @Override
    public void postDelete(MonthlyMarketResults entity, PostDeleteContext<MonthlyMarketResults> context) {
    }
}