package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class PlansListener implements EntityListener<Plans> {

    @Override
    public void preInsert(Plans entity, PreInsertContext<Plans> context) {
    }

    @Override
    public void preUpdate(Plans entity, PreUpdateContext<Plans> context) {
    }

    @Override
    public void preDelete(Plans entity, PreDeleteContext<Plans> context) {
    }

    @Override
    public void postInsert(Plans entity, PostInsertContext<Plans> context) {
    }

    @Override
    public void postUpdate(Plans entity, PostUpdateContext<Plans> context) {
    }

    @Override
    public void postDelete(Plans entity, PostDeleteContext<Plans> context) {
    }
}