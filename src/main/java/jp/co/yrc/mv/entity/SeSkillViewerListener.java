package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SeSkillViewerListener implements EntityListener<SeSkillViewer> {

    @Override
    public void preInsert(SeSkillViewer entity, PreInsertContext<SeSkillViewer> context) {
    }

    @Override
    public void preUpdate(SeSkillViewer entity, PreUpdateContext<SeSkillViewer> context) {
    }

    @Override
    public void preDelete(SeSkillViewer entity, PreDeleteContext<SeSkillViewer> context) {
    }

    @Override
    public void postInsert(SeSkillViewer entity, PostInsertContext<SeSkillViewer> context) {
    }

    @Override
    public void postUpdate(SeSkillViewer entity, PostUpdateContext<SeSkillViewer> context) {
    }

    @Override
    public void postDelete(SeSkillViewer entity, PostDeleteContext<SeSkillViewer> context) {
    }
}