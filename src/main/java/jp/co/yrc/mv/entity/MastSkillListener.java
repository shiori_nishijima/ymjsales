package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MastSkillListener implements EntityListener<MastSkill> {

    @Override
    public void preInsert(MastSkill entity, PreInsertContext<MastSkill> context) {
    }

    @Override
    public void preUpdate(MastSkill entity, PreUpdateContext<MastSkill> context) {
    }

    @Override
    public void preDelete(MastSkill entity, PreDeleteContext<MastSkill> context) {
    }

    @Override
    public void postInsert(MastSkill entity, PostInsertContext<MastSkill> context) {
    }

    @Override
    public void postUpdate(MastSkill entity, PostUpdateContext<MastSkill> context) {
    }

    @Override
    public void postDelete(MastSkill entity, PostDeleteContext<MastSkill> context) {
    }
}