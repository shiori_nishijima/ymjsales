package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MstTantoSSosikiHistoryListener implements EntityListener<MstTantoSSosikiHistory> {

    @Override
    public void preInsert(MstTantoSSosikiHistory entity, PreInsertContext<MstTantoSSosikiHistory> context) {
    }

    @Override
    public void preUpdate(MstTantoSSosikiHistory entity, PreUpdateContext<MstTantoSSosikiHistory> context) {
    }

    @Override
    public void preDelete(MstTantoSSosikiHistory entity, PreDeleteContext<MstTantoSSosikiHistory> context) {
    }

    @Override
    public void postInsert(MstTantoSSosikiHistory entity, PostInsertContext<MstTantoSSosikiHistory> context) {
    }

    @Override
    public void postUpdate(MstTantoSSosikiHistory entity, PostUpdateContext<MstTantoSSosikiHistory> context) {
    }

    @Override
    public void postDelete(MstTantoSSosikiHistory entity, PostDeleteContext<MstTantoSSosikiHistory> context) {
    }
}