package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CompassKindsListener implements EntityListener<CompassKinds> {

    @Override
    public void preInsert(CompassKinds entity, PreInsertContext<CompassKinds> context) {
    }

    @Override
    public void preUpdate(CompassKinds entity, PreUpdateContext<CompassKinds> context) {
    }

    @Override
    public void preDelete(CompassKinds entity, PreDeleteContext<CompassKinds> context) {
    }

    @Override
    public void postInsert(CompassKinds entity, PostInsertContext<CompassKinds> context) {
    }

    @Override
    public void postUpdate(CompassKinds entity, PostUpdateContext<CompassKinds> context) {
    }

    @Override
    public void postDelete(CompassKinds entity, PostDeleteContext<CompassKinds> context) {
    }
}