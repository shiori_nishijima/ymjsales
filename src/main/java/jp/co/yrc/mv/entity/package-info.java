/**
 * <code>gradle gen</code>で自動生成されるdomaのentity。<br>
 * 実績系などの数値は{@link Long}→{@link java.math.BigDecimal}に変換している。<br>
 * Tinyintは boolean[]になるのでbooleanに変換してメソッド名を修正している。<br>
 */
package jp.co.yrc.mv.entity;

