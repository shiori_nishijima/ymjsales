package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = ProductsListener.class)
@Table(name = "products")
public class Products {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** サイズ */
    @Column(name = "size")
    String size;

    /** Compass品種 */
    @Column(name = "compass_kind")
    String compassKind;

    /** 削除フラグ */
    @Column(name = "deleted")
    byte[] deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the size.
     * 
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /** 
     * Sets the size.
     * 
     * @param size the size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /** 
     * Returns the compassKind.
     * 
     * @return the compassKind
     */
    public String getCompassKind() {
        return compassKind;
    }

    /** 
     * Sets the compassKind.
     * 
     * @param compassKind the compassKind
     */
    public void setCompassKind(String compassKind) {
        this.compassKind = compassKind;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public byte[] getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(byte[] deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}