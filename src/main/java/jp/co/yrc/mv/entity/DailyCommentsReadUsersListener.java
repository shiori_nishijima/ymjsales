package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class DailyCommentsReadUsersListener implements EntityListener<DailyCommentsReadUsers> {

    @Override
    public void preInsert(DailyCommentsReadUsers entity, PreInsertContext<DailyCommentsReadUsers> context) {
    }

    @Override
    public void preUpdate(DailyCommentsReadUsers entity, PreUpdateContext<DailyCommentsReadUsers> context) {
    }

    @Override
    public void preDelete(DailyCommentsReadUsers entity, PreDeleteContext<DailyCommentsReadUsers> context) {
    }

    @Override
    public void postInsert(DailyCommentsReadUsers entity, PostInsertContext<DailyCommentsReadUsers> context) {
    }

    @Override
    public void postUpdate(DailyCommentsReadUsers entity, PostUpdateContext<DailyCommentsReadUsers> context) {
    }

    @Override
    public void postDelete(DailyCommentsReadUsers entity, PostDeleteContext<DailyCommentsReadUsers> context) {
    }
}