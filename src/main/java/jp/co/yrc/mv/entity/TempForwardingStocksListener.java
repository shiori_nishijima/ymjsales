package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TempForwardingStocksListener implements EntityListener<TempForwardingStocks> {

    @Override
    public void preInsert(TempForwardingStocks entity, PreInsertContext<TempForwardingStocks> context) {
    }

    @Override
    public void preUpdate(TempForwardingStocks entity, PreUpdateContext<TempForwardingStocks> context) {
    }

    @Override
    public void preDelete(TempForwardingStocks entity, PreDeleteContext<TempForwardingStocks> context) {
    }

    @Override
    public void postInsert(TempForwardingStocks entity, PostInsertContext<TempForwardingStocks> context) {
    }

    @Override
    public void postUpdate(TempForwardingStocks entity, PostUpdateContext<TempForwardingStocks> context) {
    }

    @Override
    public void postDelete(TempForwardingStocks entity, PostDeleteContext<TempForwardingStocks> context) {
    }
}