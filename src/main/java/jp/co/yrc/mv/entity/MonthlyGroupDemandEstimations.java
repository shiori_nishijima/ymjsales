package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyGroupDemandEstimationsListener.class)
@Table(name = "monthly_group_demand_estimations")
public class MonthlyGroupDemandEstimations {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** Aランク年間推定需要 */
    @Column(name = "rank_a_year_demand_estimation")
    BigDecimal rankAYearDemandEstimation;

    /** Bランク年間推定需要 */
    @Column(name = "rank_b_year_demand_estimation")
    BigDecimal rankBYearDemandEstimation;

    /** Cランク年間推定需要 */
    @Column(name = "rank_c_year_demand_estimation")
    BigDecimal rankCYearDemandEstimation;

    /** 重点年間推定需要 */
    @Column(name = "important_year_demand_estimation")
    BigDecimal importantYearDemandEstimation;

    /** 新規年間推定需要 */
    @Column(name = "new_year_demand_estimation")
    BigDecimal newYearDemandEstimation;

    /** 深耕年間推定需要 */
    @Column(name = "deep_plowing_year_demand_estimation")
    BigDecimal deepPlowingYearDemandEstimation;

    /** Y-CP年間推定需要 */
    @Column(name = "y_cp_year_demand_estimation")
    BigDecimal yCpYearDemandEstimation;

    /** 他社CP年間推定需要 */
    @Column(name = "other_cp_year_demand_estimation")
    BigDecimal otherCpYearDemandEstimation;

    /** グループ1年間推定需要 */
    @Column(name = "group_one_year_demand_estimation")
    BigDecimal groupOneYearDemandEstimation;

    /** グループ2年間推定需要 */
    @Column(name = "group_two_year_demand_estimation")
    BigDecimal groupTwoYearDemandEstimation;

    /** グループ3年間推定需要 */
    @Column(name = "group_three_year_demand_estimation")
    BigDecimal groupThreeYearDemandEstimation;

    /** グループ4年間推定需要 */
    @Column(name = "group_four_year_demand_estimation")
    BigDecimal groupFourYearDemandEstimation;

    /** グループ5年間推定需要 */
    @Column(name = "group_five_year_demand_estimation")
    BigDecimal groupFiveYearDemandEstimation;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the rankAYearDemandEstimation.
     * 
     * @return the rankAYearDemandEstimation
     */
    public BigDecimal getRankAYearDemandEstimation() {
        return rankAYearDemandEstimation;
    }

    /** 
     * Sets the rankAYearDemandEstimation.
     * 
     * @param rankAYearDemandEstimation the rankAYearDemandEstimation
     */
    public void setRankAYearDemandEstimation(BigDecimal rankAYearDemandEstimation) {
        this.rankAYearDemandEstimation = rankAYearDemandEstimation;
    }

    /** 
     * Returns the rankBYearDemandEstimation.
     * 
     * @return the rankBYearDemandEstimation
     */
    public BigDecimal getRankBYearDemandEstimation() {
        return rankBYearDemandEstimation;
    }

    /** 
     * Sets the rankBYearDemandEstimation.
     * 
     * @param rankBYearDemandEstimation the rankBYearDemandEstimation
     */
    public void setRankBYearDemandEstimation(BigDecimal rankBYearDemandEstimation) {
        this.rankBYearDemandEstimation = rankBYearDemandEstimation;
    }

    /** 
     * Returns the rankCYearDemandEstimation.
     * 
     * @return the rankCYearDemandEstimation
     */
    public BigDecimal getRankCYearDemandEstimation() {
        return rankCYearDemandEstimation;
    }

    /** 
     * Sets the rankCYearDemandEstimation.
     * 
     * @param rankCYearDemandEstimation the rankCYearDemandEstimation
     */
    public void setRankCYearDemandEstimation(BigDecimal rankCYearDemandEstimation) {
        this.rankCYearDemandEstimation = rankCYearDemandEstimation;
    }

    /** 
     * Returns the importantYearDemandEstimation.
     * 
     * @return the importantYearDemandEstimation
     */
    public BigDecimal getImportantYearDemandEstimation() {
        return importantYearDemandEstimation;
    }

    /** 
     * Sets the importantYearDemandEstimation.
     * 
     * @param importantYearDemandEstimation the importantYearDemandEstimation
     */
    public void setImportantYearDemandEstimation(BigDecimal importantYearDemandEstimation) {
        this.importantYearDemandEstimation = importantYearDemandEstimation;
    }

    /** 
     * Returns the newYearDemandEstimation.
     * 
     * @return the newYearDemandEstimation
     */
    public BigDecimal getNewYearDemandEstimation() {
        return newYearDemandEstimation;
    }

    /** 
     * Sets the newYearDemandEstimation.
     * 
     * @param newYearDemandEstimation the newYearDemandEstimation
     */
    public void setNewYearDemandEstimation(BigDecimal newYearDemandEstimation) {
        this.newYearDemandEstimation = newYearDemandEstimation;
    }

    /** 
     * Returns the deepPlowingYearDemandEstimation.
     * 
     * @return the deepPlowingYearDemandEstimation
     */
    public BigDecimal getDeepPlowingYearDemandEstimation() {
        return deepPlowingYearDemandEstimation;
    }

    /** 
     * Sets the deepPlowingYearDemandEstimation.
     * 
     * @param deepPlowingYearDemandEstimation the deepPlowingYearDemandEstimation
     */
    public void setDeepPlowingYearDemandEstimation(BigDecimal deepPlowingYearDemandEstimation) {
        this.deepPlowingYearDemandEstimation = deepPlowingYearDemandEstimation;
    }

    /** 
     * Returns the yCpYearDemandEstimation.
     * 
     * @return the yCpYearDemandEstimation
     */
    public BigDecimal getYCpYearDemandEstimation() {
        return yCpYearDemandEstimation;
    }

    /** 
     * Sets the yCpYearDemandEstimation.
     * 
     * @param yCpYearDemandEstimation the yCpYearDemandEstimation
     */
    public void setYCpYearDemandEstimation(BigDecimal yCpYearDemandEstimation) {
        this.yCpYearDemandEstimation = yCpYearDemandEstimation;
    }

    /** 
     * Returns the otherCpYearDemandEstimation.
     * 
     * @return the otherCpYearDemandEstimation
     */
    public BigDecimal getOtherCpYearDemandEstimation() {
        return otherCpYearDemandEstimation;
    }

    /** 
     * Sets the otherCpYearDemandEstimation.
     * 
     * @param otherCpYearDemandEstimation the otherCpYearDemandEstimation
     */
    public void setOtherCpYearDemandEstimation(BigDecimal otherCpYearDemandEstimation) {
        this.otherCpYearDemandEstimation = otherCpYearDemandEstimation;
    }

    /** 
     * Returns the groupOneYearDemandEstimation.
     * 
     * @return the groupOneYearDemandEstimation
     */
    public BigDecimal getGroupOneYearDemandEstimation() {
        return groupOneYearDemandEstimation;
    }

    /** 
     * Sets the groupOneYearDemandEstimation.
     * 
     * @param groupOneYearDemandEstimation the groupOneYearDemandEstimation
     */
    public void setGroupOneYearDemandEstimation(BigDecimal groupOneYearDemandEstimation) {
        this.groupOneYearDemandEstimation = groupOneYearDemandEstimation;
    }

    /** 
     * Returns the groupTwoYearDemandEstimation.
     * 
     * @return the groupTwoYearDemandEstimation
     */
    public BigDecimal getGroupTwoYearDemandEstimation() {
        return groupTwoYearDemandEstimation;
    }

    /** 
     * Sets the groupTwoYearDemandEstimation.
     * 
     * @param groupTwoYearDemandEstimation the groupTwoYearDemandEstimation
     */
    public void setGroupTwoYearDemandEstimation(BigDecimal groupTwoYearDemandEstimation) {
        this.groupTwoYearDemandEstimation = groupTwoYearDemandEstimation;
    }

    /** 
     * Returns the groupThreeYearDemandEstimation.
     * 
     * @return the groupThreeYearDemandEstimation
     */
    public BigDecimal getGroupThreeYearDemandEstimation() {
        return groupThreeYearDemandEstimation;
    }

    /** 
     * Sets the groupThreeYearDemandEstimation.
     * 
     * @param groupThreeYearDemandEstimation the groupThreeYearDemandEstimation
     */
    public void setGroupThreeYearDemandEstimation(BigDecimal groupThreeYearDemandEstimation) {
        this.groupThreeYearDemandEstimation = groupThreeYearDemandEstimation;
    }

    /** 
     * Returns the groupFourYearDemandEstimation.
     * 
     * @return the groupFourYearDemandEstimation
     */
    public BigDecimal getGroupFourYearDemandEstimation() {
        return groupFourYearDemandEstimation;
    }

    /** 
     * Sets the groupFourYearDemandEstimation.
     * 
     * @param groupFourYearDemandEstimation the groupFourYearDemandEstimation
     */
    public void setGroupFourYearDemandEstimation(BigDecimal groupFourYearDemandEstimation) {
        this.groupFourYearDemandEstimation = groupFourYearDemandEstimation;
    }

    /** 
     * Returns the groupFiveYearDemandEstimation.
     * 
     * @return the groupFiveYearDemandEstimation
     */
    public BigDecimal getGroupFiveYearDemandEstimation() {
        return groupFiveYearDemandEstimation;
    }

    /** 
     * Sets the groupFiveYearDemandEstimation.
     * 
     * @param groupFiveYearDemandEstimation the groupFiveYearDemandEstimation
     */
    public void setGroupFiveYearDemandEstimation(BigDecimal groupFiveYearDemandEstimation) {
        this.groupFiveYearDemandEstimation = groupFiveYearDemandEstimation;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}