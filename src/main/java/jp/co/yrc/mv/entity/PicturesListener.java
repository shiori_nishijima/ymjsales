package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class PicturesListener implements EntityListener<Pictures> {

    @Override
    public void preInsert(Pictures entity, PreInsertContext<Pictures> context) {
    }

    @Override
    public void preUpdate(Pictures entity, PreUpdateContext<Pictures> context) {
    }

    @Override
    public void preDelete(Pictures entity, PreDeleteContext<Pictures> context) {
    }

    @Override
    public void postInsert(Pictures entity, PostInsertContext<Pictures> context) {
    }

    @Override
    public void postUpdate(Pictures entity, PostUpdateContext<Pictures> context) {
    }

    @Override
    public void postDelete(Pictures entity, PostDeleteContext<Pictures> context) {
    }
}