package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ProposalDocumentsListener implements EntityListener<ProposalDocuments> {

    @Override
    public void preInsert(ProposalDocuments entity, PreInsertContext<ProposalDocuments> context) {
    }

    @Override
    public void preUpdate(ProposalDocuments entity, PreUpdateContext<ProposalDocuments> context) {
    }

    @Override
    public void preDelete(ProposalDocuments entity, PreDeleteContext<ProposalDocuments> context) {
    }

    @Override
    public void postInsert(ProposalDocuments entity, PostInsertContext<ProposalDocuments> context) {
    }

    @Override
    public void postUpdate(ProposalDocuments entity, PostUpdateContext<ProposalDocuments> context) {
    }

    @Override
    public void postDelete(ProposalDocuments entity, PostDeleteContext<ProposalDocuments> context) {
    }
}