package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MastBilliardListener.class)
@Table(name = "mast_billiard")
public class MastBilliard {

    /** 機種コード */
    @Id
    @Column(name = "id")
    String id;

    /** 機種コード(未使用) */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者ID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者ID */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細(未使用) */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** アサイン先(未使用) */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** メーカー */
    @Column(name = "billiard_maker")
    String billiardMaker;

    /** モデル */
    @Column(name = "billiard_model")
    String billiardModel;

    /** 色 */
    @Column(name = "billiard_color")
    String billiardColor;

    /** 表示順 */
    @Column(name = "billiard_order")
    String billiardOrder;

    /** 非表示フラグ */
    @Column(name = "display_disabled")
    Integer displayDisabled;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the billiardMaker.
     * 
     * @return the billiardMaker
     */
    public String getBilliardMaker() {
        return billiardMaker;
    }

    /** 
     * Sets the billiardMaker.
     * 
     * @param billiardMaker the billiardMaker
     */
    public void setBilliardMaker(String billiardMaker) {
        this.billiardMaker = billiardMaker;
    }

    /** 
     * Returns the billiardModel.
     * 
     * @return the billiardModel
     */
    public String getBilliardModel() {
        return billiardModel;
    }

    /** 
     * Sets the billiardModel.
     * 
     * @param billiardModel the billiardModel
     */
    public void setBilliardModel(String billiardModel) {
        this.billiardModel = billiardModel;
    }

    /** 
     * Returns the billiardColor.
     * 
     * @return the billiardColor
     */
    public String getBilliardColor() {
        return billiardColor;
    }

    /** 
     * Sets the billiardColor.
     * 
     * @param billiardColor the billiardColor
     */
    public void setBilliardColor(String billiardColor) {
        this.billiardColor = billiardColor;
    }

    /** 
     * Returns the billiardOrder.
     * 
     * @return the billiardOrder
     */
    public String getBilliardOrder() {
        return billiardOrder;
    }

    /** 
     * Sets the billiardOrder.
     * 
     * @param billiardOrder the billiardOrder
     */
    public void setBilliardOrder(String billiardOrder) {
        this.billiardOrder = billiardOrder;
    }

    /** 
     * Returns the displayDisabled.
     * 
     * @return the displayDisabled
     */
    public Integer getDisplayDisabled() {
        return displayDisabled;
    }

    /** 
     * Sets the displayDisabled.
     * 
     * @param displayDisabled the displayDisabled
     */
    public void setDisplayDisabled(Integer displayDisabled) {
        this.displayDisabled = displayDisabled;
    }
}