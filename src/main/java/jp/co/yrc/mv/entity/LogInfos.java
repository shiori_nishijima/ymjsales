package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = LogInfosListener.class)
@Table(name = "log_infos")
public class LogInfos {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** ログレベル */
    @Column(name = "level")
    String level;

    /** ログインユーザID */
    @Column(name = "user_id")
    String userId;

    /** ログインタイムスタンプ */
    @Column(name = "login_timestamp")
    Timestamp loginTimestamp;

    /** イベント発生タイムスタンプ */
    @Column(name = "event_timestamp")
    Timestamp eventTimestamp;

    /** 内容 */
    @Column(name = "description")
    String description;

    /** ユーザエージェント */
    @Column(name = "user_agent")
    String userAgent;

    /**  */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the level.
     * 
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /** 
     * Sets the level.
     * 
     * @param level the level
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the loginTimestamp.
     * 
     * @return the loginTimestamp
     */
    public Timestamp getLoginTimestamp() {
        return loginTimestamp;
    }

    /** 
     * Sets the loginTimestamp.
     * 
     * @param loginTimestamp the loginTimestamp
     */
    public void setLoginTimestamp(Timestamp loginTimestamp) {
        this.loginTimestamp = loginTimestamp;
    }

    /** 
     * Returns the eventTimestamp.
     * 
     * @return the eventTimestamp
     */
    public Timestamp getEventTimestamp() {
        return eventTimestamp;
    }

    /** 
     * Sets the eventTimestamp.
     * 
     * @param eventTimestamp the eventTimestamp
     */
    public void setEventTimestamp(Timestamp eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the userAgent.
     * 
     * @return the userAgent
     */
    public String getUserAgent() {
        return userAgent;
    }

    /** 
     * Sets the userAgent.
     * 
     * @param userAgent the userAgent
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }
}