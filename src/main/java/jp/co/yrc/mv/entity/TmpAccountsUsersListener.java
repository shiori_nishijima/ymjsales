package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpAccountsUsersListener implements EntityListener<TmpAccountsUsers> {

    @Override
    public void preInsert(TmpAccountsUsers entity, PreInsertContext<TmpAccountsUsers> context) {
    }

    @Override
    public void preUpdate(TmpAccountsUsers entity, PreUpdateContext<TmpAccountsUsers> context) {
    }

    @Override
    public void preDelete(TmpAccountsUsers entity, PreDeleteContext<TmpAccountsUsers> context) {
    }

    @Override
    public void postInsert(TmpAccountsUsers entity, PostInsertContext<TmpAccountsUsers> context) {
    }

    @Override
    public void postUpdate(TmpAccountsUsers entity, PostUpdateContext<TmpAccountsUsers> context) {
    }

    @Override
    public void postDelete(TmpAccountsUsers entity, PostDeleteContext<TmpAccountsUsers> context) {
    }
}