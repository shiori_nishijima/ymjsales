package jp.co.yrc.mv.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OpportunitiesListener.class)
@Table(name = "opportunities")
public class Opportunities {

    /** 案件CD */
    @Id
    @Column(name = "id")
    String id;

    /** 案件名 */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細 */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** アサイン先（営業担当者）id */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 案件区分 */
    @Column(name = "opportunity_type")
    String opportunityType;

    /** キャンペーンID SPM案件ID（未使用） */
    @Column(name = "campaign_id")
    String campaignId;

    /** リードソース */
    @Column(name = "lead_source")
    String leadSource;

    /** 金額（未使用） */
    @Column(name = "amount")
    Double amount;

    /** 金額USD（未使用） */
    @Column(name = "amount_usdollar")
    Double amountUsdollar;

    /** 通貨ID（未使用） */
    @Column(name = "currency_id")
    String currencyId;

    /** 決着日 */
    @Column(name = "date_closed")
    Date dateClosed;

    /** 次ステップ（未使用） */
    @Column(name = "next_step")
    String nextStep;

    /** キャンペーンCD */
    @Column(name = "sales_stage")
    String salesStage;

    /** 確度 (%)（未使用） */
    @Column(name = "probability")
    Double probability;

    /** 上長id */
    @Column(name = "owner_id")
    String ownerId;

    /** 登録時企業CD */
    @Column(name = "sup_id")
    String supId;

    /**  */
    @Column(name = "calling_agent")
    String callingAgent;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the opportunityType.
     * 
     * @return the opportunityType
     */
    public String getOpportunityType() {
        return opportunityType;
    }

    /** 
     * Sets the opportunityType.
     * 
     * @param opportunityType the opportunityType
     */
    public void setOpportunityType(String opportunityType) {
        this.opportunityType = opportunityType;
    }

    /** 
     * Returns the campaignId.
     * 
     * @return the campaignId
     */
    public String getCampaignId() {
        return campaignId;
    }

    /** 
     * Sets the campaignId.
     * 
     * @param campaignId the campaignId
     */
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /** 
     * Returns the leadSource.
     * 
     * @return the leadSource
     */
    public String getLeadSource() {
        return leadSource;
    }

    /** 
     * Sets the leadSource.
     * 
     * @param leadSource the leadSource
     */
    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    /** 
     * Returns the amount.
     * 
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /** 
     * Sets the amount.
     * 
     * @param amount the amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /** 
     * Returns the amountUsdollar.
     * 
     * @return the amountUsdollar
     */
    public Double getAmountUsdollar() {
        return amountUsdollar;
    }

    /** 
     * Sets the amountUsdollar.
     * 
     * @param amountUsdollar the amountUsdollar
     */
    public void setAmountUsdollar(Double amountUsdollar) {
        this.amountUsdollar = amountUsdollar;
    }

    /** 
     * Returns the currencyId.
     * 
     * @return the currencyId
     */
    public String getCurrencyId() {
        return currencyId;
    }

    /** 
     * Sets the currencyId.
     * 
     * @param currencyId the currencyId
     */
    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    /** 
     * Returns the dateClosed.
     * 
     * @return the dateClosed
     */
    public Date getDateClosed() {
        return dateClosed;
    }

    /** 
     * Sets the dateClosed.
     * 
     * @param dateClosed the dateClosed
     */
    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

    /** 
     * Returns the nextStep.
     * 
     * @return the nextStep
     */
    public String getNextStep() {
        return nextStep;
    }

    /** 
     * Sets the nextStep.
     * 
     * @param nextStep the nextStep
     */
    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    /** 
     * Returns the salesStage.
     * 
     * @return the salesStage
     */
    public String getSalesStage() {
        return salesStage;
    }

    /** 
     * Sets the salesStage.
     * 
     * @param salesStage the salesStage
     */
    public void setSalesStage(String salesStage) {
        this.salesStage = salesStage;
    }

    /** 
     * Returns the probability.
     * 
     * @return the probability
     */
    public Double getProbability() {
        return probability;
    }

    /** 
     * Sets the probability.
     * 
     * @param probability the probability
     */
    public void setProbability(Double probability) {
        this.probability = probability;
    }

    /** 
     * Returns the ownerId.
     * 
     * @return the ownerId
     */
    public String getOwnerId() {
        return ownerId;
    }

    /** 
     * Sets the ownerId.
     * 
     * @param ownerId the ownerId
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /** 
     * Returns the supId.
     * 
     * @return the supId
     */
    public String getSupId() {
        return supId;
    }

    /** 
     * Sets the supId.
     * 
     * @param supId the supId
     */
    public void setSupId(String supId) {
        this.supId = supId;
    }

    /** 
     * Returns the callingAgent.
     * 
     * @return the callingAgent
     */
    public String getCallingAgent() {
        return callingAgent;
    }

    /** 
     * Sets the callingAgent.
     * 
     * @param callingAgent the callingAgent
     */
    public void setCallingAgent(String callingAgent) {
        this.callingAgent = callingAgent;
    }
}