package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCallsUsersListener implements EntityListener<TechServiceCallsUsers> {

    @Override
    public void preInsert(TechServiceCallsUsers entity, PreInsertContext<TechServiceCallsUsers> context) {
    }

    @Override
    public void preUpdate(TechServiceCallsUsers entity, PreUpdateContext<TechServiceCallsUsers> context) {
    }

    @Override
    public void preDelete(TechServiceCallsUsers entity, PreDeleteContext<TechServiceCallsUsers> context) {
    }

    @Override
    public void postInsert(TechServiceCallsUsers entity, PostInsertContext<TechServiceCallsUsers> context) {
    }

    @Override
    public void postUpdate(TechServiceCallsUsers entity, PostUpdateContext<TechServiceCallsUsers> context) {
    }

    @Override
    public void postDelete(TechServiceCallsUsers entity, PostDeleteContext<TechServiceCallsUsers> context) {
    }
}