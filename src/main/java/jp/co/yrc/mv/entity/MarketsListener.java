package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MarketsListener implements EntityListener<Markets> {

    @Override
    public void preInsert(Markets entity, PreInsertContext<Markets> context) {
    }

    @Override
    public void preUpdate(Markets entity, PreUpdateContext<Markets> context) {
    }

    @Override
    public void preDelete(Markets entity, PreDeleteContext<Markets> context) {
    }

    @Override
    public void postInsert(Markets entity, PostInsertContext<Markets> context) {
    }

    @Override
    public void postUpdate(Markets entity, PostUpdateContext<Markets> context) {
    }

    @Override
    public void postDelete(Markets entity, PostDeleteContext<Markets> context) {
    }
}