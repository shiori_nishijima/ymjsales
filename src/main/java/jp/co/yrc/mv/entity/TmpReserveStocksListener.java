package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpReserveStocksListener implements EntityListener<TmpReserveStocks> {

    @Override
    public void preInsert(TmpReserveStocks entity, PreInsertContext<TmpReserveStocks> context) {
    }

    @Override
    public void preUpdate(TmpReserveStocks entity, PreUpdateContext<TmpReserveStocks> context) {
    }

    @Override
    public void preDelete(TmpReserveStocks entity, PreDeleteContext<TmpReserveStocks> context) {
    }

    @Override
    public void postInsert(TmpReserveStocks entity, PostInsertContext<TmpReserveStocks> context) {
    }

    @Override
    public void postUpdate(TmpReserveStocks entity, PostUpdateContext<TmpReserveStocks> context) {
    }

    @Override
    public void postDelete(TmpReserveStocks entity, PostDeleteContext<TmpReserveStocks> context) {
    }
}