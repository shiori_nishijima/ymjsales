package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class FixturesListener implements EntityListener<Fixtures> {

    @Override
    public void preInsert(Fixtures entity, PreInsertContext<Fixtures> context) {
    }

    @Override
    public void preUpdate(Fixtures entity, PreUpdateContext<Fixtures> context) {
    }

    @Override
    public void preDelete(Fixtures entity, PreDeleteContext<Fixtures> context) {
    }

    @Override
    public void postInsert(Fixtures entity, PostInsertContext<Fixtures> context) {
    }

    @Override
    public void postUpdate(Fixtures entity, PostUpdateContext<Fixtures> context) {
    }

    @Override
    public void postDelete(Fixtures entity, PostDeleteContext<Fixtures> context) {
    }
}