package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = ResultsListener.class)
@Table(name = "results")
public class Results {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** 日 */
    @Id
    @Column(name = "date")
    Integer date;

    /** 販社コード */
    @Id
    @Column(name = "sales_company_code")
    String salesCompanyCode;

    /** 販社得意先コード */
    @Id
    @Column(name = "account_id")
    String accountId;

    /** 販管得意先累計計画品種月実績年月 */
    @Column(name = "sga_year_month")
    Integer sgaYearMonth;

    /** 販社共通品種コード */
    @Id
    @Column(name = "compass_kind")
    String compassKind;

    /** 販社部門コード */
    @Column(name = "department_code")
    String departmentCode;

    /** 営業所コード */
    @Column(name = "sales_office_code")
    String salesOfficeCode;

    /** 販社従業員コード */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 販社得意先業種区分 */
    @Column(name = "accoount_category")
    String accoountCategory;

    /** 販管得意先累計計画品種月実績年度 */
    @Column(name = "sga_year")
    Integer sgaYear;

    /** 販管得意先累計計画品種月実績期区分 */
    @Column(name = "sga_period_div")
    Integer sgaPeriodDiv;

    /** 販社品種分類ＹＲＣ区分 */
    @Column(name = "product_class_yrc_div")
    String productClassYrcDiv;

    /** 動タ区分 */
    @Column(name = "large_class1")
    String largeClass1;

    /** 販社共通スノー区分 */
    @Column(name = "large_class2")
    String largeClass2;

    /** 販社品種分類大分類コード */
    @Column(name = "middle_class")
    String middleClass;

    /** 販社品種分類中分類コード */
    @Column(name = "small_class")
    String smallClass;

    /** 販管計画品種コード */
    @Column(name = "product_kind_cd")
    String productKindCd;

    /** 販管得意先累計計画品種日実績売上本数 */
    @Column(name = "number")
    BigDecimal number;

    /** 販管得意先累計計画品種日実績売上金額 */
    @Column(name = "sales")
    BigDecimal sales;

    /** 販管得意先累計計画品種日実績基準金額 */
    @Column(name = "base_amount")
    BigDecimal baseAmount;

    /** 販管得意先累計計画品種日実績粗利金額 */
    @Column(name = "margin")
    BigDecimal margin;

    /** 販管得意先累計計画品種月実績売上本数 */
    @Column(name = "monthly_number")
    BigDecimal monthlyNumber;

    /** 販管得意先累計計画品種月実績売上金額 */
    @Column(name = "monthly_sales")
    BigDecimal monthlySales;

    /** 販管得意先累計計画品種月実績基準金額 */
    @Column(name = "monthly_base_amount")
    BigDecimal monthlyBaseAmount;

    /** 販管得意先累計計画品種月実績粗利金額 */
    @Column(name = "monthly_margin")
    BigDecimal monthlyMargin;

    /** 販管得意先累計計画品種月実績更新日 */
    @Column(name = "date_updated")
    String dateUpdated;

    /** 販管得意先累計計画品種日実績粗利金額本社 */
    @Column(name = "hq_margin")
    BigDecimal hqMargin;

    /** 販管得意先累計計画品種月実績粗利金額本社 */
    @Column(name = "monthly_hq_margin")
    BigDecimal monthlyHqMargin;

    /** 販管得意先累計実績更新区分 */
    @Column(name = "modified_div")
    String modifiedDiv;

    /** 需要ＪＡＴＭＡ地域コード */
    @Column(name = "jatma_cd")
    String jatmaCd;

    /** 販社得意先業種種別区分 */
    @Column(name = "kind_div")
    String kindDiv;

    /** 販管得意先累計計画品種月実績締め区分 */
    @Column(name = "closing_div")
    String closingDiv;

    /** 削除済み */
    @Column(name = "deleted")
    byte[] deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the date.
     * 
     * @return the date
     */
    public Integer getDate() {
        return date;
    }

    /** 
     * Sets the date.
     * 
     * @param date the date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /** 
     * Returns the salesCompanyCode.
     * 
     * @return the salesCompanyCode
     */
    public String getSalesCompanyCode() {
        return salesCompanyCode;
    }

    /** 
     * Sets the salesCompanyCode.
     * 
     * @param salesCompanyCode the salesCompanyCode
     */
    public void setSalesCompanyCode(String salesCompanyCode) {
        this.salesCompanyCode = salesCompanyCode;
    }

    /** 
     * Returns the accountId.
     * 
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /** 
     * Sets the accountId.
     * 
     * @param accountId the accountId
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /** 
     * Returns the sgaYearMonth.
     * 
     * @return the sgaYearMonth
     */
    public Integer getSgaYearMonth() {
        return sgaYearMonth;
    }

    /** 
     * Sets the sgaYearMonth.
     * 
     * @param sgaYearMonth the sgaYearMonth
     */
    public void setSgaYearMonth(Integer sgaYearMonth) {
        this.sgaYearMonth = sgaYearMonth;
    }

    /** 
     * Returns the compassKind.
     * 
     * @return the compassKind
     */
    public String getCompassKind() {
        return compassKind;
    }

    /** 
     * Sets the compassKind.
     * 
     * @param compassKind the compassKind
     */
    public void setCompassKind(String compassKind) {
        this.compassKind = compassKind;
    }

    /** 
     * Returns the departmentCode.
     * 
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /** 
     * Sets the departmentCode.
     * 
     * @param departmentCode the departmentCode
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /** 
     * Returns the salesOfficeCode.
     * 
     * @return the salesOfficeCode
     */
    public String getSalesOfficeCode() {
        return salesOfficeCode;
    }

    /** 
     * Sets the salesOfficeCode.
     * 
     * @param salesOfficeCode the salesOfficeCode
     */
    public void setSalesOfficeCode(String salesOfficeCode) {
        this.salesOfficeCode = salesOfficeCode;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the accoountCategory.
     * 
     * @return the accoountCategory
     */
    public String getAccoountCategory() {
        return accoountCategory;
    }

    /** 
     * Sets the accoountCategory.
     * 
     * @param accoountCategory the accoountCategory
     */
    public void setAccoountCategory(String accoountCategory) {
        this.accoountCategory = accoountCategory;
    }

    /** 
     * Returns the sgaYear.
     * 
     * @return the sgaYear
     */
    public Integer getSgaYear() {
        return sgaYear;
    }

    /** 
     * Sets the sgaYear.
     * 
     * @param sgaYear the sgaYear
     */
    public void setSgaYear(Integer sgaYear) {
        this.sgaYear = sgaYear;
    }

    /** 
     * Returns the sgaPeriodDiv.
     * 
     * @return the sgaPeriodDiv
     */
    public Integer getSgaPeriodDiv() {
        return sgaPeriodDiv;
    }

    /** 
     * Sets the sgaPeriodDiv.
     * 
     * @param sgaPeriodDiv the sgaPeriodDiv
     */
    public void setSgaPeriodDiv(Integer sgaPeriodDiv) {
        this.sgaPeriodDiv = sgaPeriodDiv;
    }

    /** 
     * Returns the productClassYrcDiv.
     * 
     * @return the productClassYrcDiv
     */
    public String getProductClassYrcDiv() {
        return productClassYrcDiv;
    }

    /** 
     * Sets the productClassYrcDiv.
     * 
     * @param productClassYrcDiv the productClassYrcDiv
     */
    public void setProductClassYrcDiv(String productClassYrcDiv) {
        this.productClassYrcDiv = productClassYrcDiv;
    }

    /** 
     * Returns the largeClass1.
     * 
     * @return the largeClass1
     */
    public String getLargeClass1() {
        return largeClass1;
    }

    /** 
     * Sets the largeClass1.
     * 
     * @param largeClass1 the largeClass1
     */
    public void setLargeClass1(String largeClass1) {
        this.largeClass1 = largeClass1;
    }

    /** 
     * Returns the largeClass2.
     * 
     * @return the largeClass2
     */
    public String getLargeClass2() {
        return largeClass2;
    }

    /** 
     * Sets the largeClass2.
     * 
     * @param largeClass2 the largeClass2
     */
    public void setLargeClass2(String largeClass2) {
        this.largeClass2 = largeClass2;
    }

    /** 
     * Returns the middleClass.
     * 
     * @return the middleClass
     */
    public String getMiddleClass() {
        return middleClass;
    }

    /** 
     * Sets the middleClass.
     * 
     * @param middleClass the middleClass
     */
    public void setMiddleClass(String middleClass) {
        this.middleClass = middleClass;
    }

    /** 
     * Returns the smallClass.
     * 
     * @return the smallClass
     */
    public String getSmallClass() {
        return smallClass;
    }

    /** 
     * Sets the smallClass.
     * 
     * @param smallClass the smallClass
     */
    public void setSmallClass(String smallClass) {
        this.smallClass = smallClass;
    }

    /** 
     * Returns the productKindCd.
     * 
     * @return the productKindCd
     */
    public String getProductKindCd() {
        return productKindCd;
    }

    /** 
     * Sets the productKindCd.
     * 
     * @param productKindCd the productKindCd
     */
    public void setProductKindCd(String productKindCd) {
        this.productKindCd = productKindCd;
    }

    /** 
     * Returns the number.
     * 
     * @return the number
     */
    public BigDecimal getNumber() {
        return number;
    }

    /** 
     * Sets the number.
     * 
     * @param number the number
     */
    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    /** 
     * Returns the sales.
     * 
     * @return the sales
     */
    public BigDecimal getSales() {
        return sales;
    }

    /** 
     * Sets the sales.
     * 
     * @param sales the sales
     */
    public void setSales(BigDecimal sales) {
        this.sales = sales;
    }

    /** 
     * Returns the baseAmount.
     * 
     * @return the baseAmount
     */
    public BigDecimal getBaseAmount() {
        return baseAmount;
    }

    /** 
     * Sets the baseAmount.
     * 
     * @param baseAmount the baseAmount
     */
    public void setBaseAmount(BigDecimal baseAmount) {
        this.baseAmount = baseAmount;
    }

    /** 
     * Returns the margin.
     * 
     * @return the margin
     */
    public BigDecimal getMargin() {
        return margin;
    }

    /** 
     * Sets the margin.
     * 
     * @param margin the margin
     */
    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    /** 
     * Returns the monthlyNumber.
     * 
     * @return the monthlyNumber
     */
    public BigDecimal getMonthlyNumber() {
        return monthlyNumber;
    }

    /** 
     * Sets the monthlyNumber.
     * 
     * @param monthlyNumber the monthlyNumber
     */
    public void setMonthlyNumber(BigDecimal monthlyNumber) {
        this.monthlyNumber = monthlyNumber;
    }

    /** 
     * Returns the monthlySales.
     * 
     * @return the monthlySales
     */
    public BigDecimal getMonthlySales() {
        return monthlySales;
    }

    /** 
     * Sets the monthlySales.
     * 
     * @param monthlySales the monthlySales
     */
    public void setMonthlySales(BigDecimal monthlySales) {
        this.monthlySales = monthlySales;
    }

    /** 
     * Returns the monthlyBaseAmount.
     * 
     * @return the monthlyBaseAmount
     */
    public BigDecimal getMonthlyBaseAmount() {
        return monthlyBaseAmount;
    }

    /** 
     * Sets the monthlyBaseAmount.
     * 
     * @param monthlyBaseAmount the monthlyBaseAmount
     */
    public void setMonthlyBaseAmount(BigDecimal monthlyBaseAmount) {
        this.monthlyBaseAmount = monthlyBaseAmount;
    }

    /** 
     * Returns the monthlyMargin.
     * 
     * @return the monthlyMargin
     */
    public BigDecimal getMonthlyMargin() {
        return monthlyMargin;
    }

    /** 
     * Sets the monthlyMargin.
     * 
     * @param monthlyMargin the monthlyMargin
     */
    public void setMonthlyMargin(BigDecimal monthlyMargin) {
        this.monthlyMargin = monthlyMargin;
    }

    /** 
     * Returns the dateUpdated.
     * 
     * @return the dateUpdated
     */
    public String getDateUpdated() {
        return dateUpdated;
    }

    /** 
     * Sets the dateUpdated.
     * 
     * @param dateUpdated the dateUpdated
     */
    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    /** 
     * Returns the hqMargin.
     * 
     * @return the hqMargin
     */
    public BigDecimal getHqMargin() {
        return hqMargin;
    }

    /** 
     * Sets the hqMargin.
     * 
     * @param hqMargin the hqMargin
     */
    public void setHqMargin(BigDecimal hqMargin) {
        this.hqMargin = hqMargin;
    }

    /** 
     * Returns the monthlyHqMargin.
     * 
     * @return the monthlyHqMargin
     */
    public BigDecimal getMonthlyHqMargin() {
        return monthlyHqMargin;
    }

    /** 
     * Sets the monthlyHqMargin.
     * 
     * @param monthlyHqMargin the monthlyHqMargin
     */
    public void setMonthlyHqMargin(BigDecimal monthlyHqMargin) {
        this.monthlyHqMargin = monthlyHqMargin;
    }

    /** 
     * Returns the modifiedDiv.
     * 
     * @return the modifiedDiv
     */
    public String getModifiedDiv() {
        return modifiedDiv;
    }

    /** 
     * Sets the modifiedDiv.
     * 
     * @param modifiedDiv the modifiedDiv
     */
    public void setModifiedDiv(String modifiedDiv) {
        this.modifiedDiv = modifiedDiv;
    }

    /** 
     * Returns the jatmaCd.
     * 
     * @return the jatmaCd
     */
    public String getJatmaCd() {
        return jatmaCd;
    }

    /** 
     * Sets the jatmaCd.
     * 
     * @param jatmaCd the jatmaCd
     */
    public void setJatmaCd(String jatmaCd) {
        this.jatmaCd = jatmaCd;
    }

    /** 
     * Returns the kindDiv.
     * 
     * @return the kindDiv
     */
    public String getKindDiv() {
        return kindDiv;
    }

    /** 
     * Sets the kindDiv.
     * 
     * @param kindDiv the kindDiv
     */
    public void setKindDiv(String kindDiv) {
        this.kindDiv = kindDiv;
    }

    /** 
     * Returns the closingDiv.
     * 
     * @return the closingDiv
     */
    public String getClosingDiv() {
        return closingDiv;
    }

    /** 
     * Sets the closingDiv.
     * 
     * @param closingDiv the closingDiv
     */
    public void setClosingDiv(String closingDiv) {
        this.closingDiv = closingDiv;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public byte[] getDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(byte[] deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}