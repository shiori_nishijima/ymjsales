package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MstEigyoSosikiListener implements EntityListener<MstEigyoSosiki> {

    @Override
    public void preInsert(MstEigyoSosiki entity, PreInsertContext<MstEigyoSosiki> context) {
    }

    @Override
    public void preUpdate(MstEigyoSosiki entity, PreUpdateContext<MstEigyoSosiki> context) {
    }

    @Override
    public void preDelete(MstEigyoSosiki entity, PreDeleteContext<MstEigyoSosiki> context) {
    }

    @Override
    public void postInsert(MstEigyoSosiki entity, PostInsertContext<MstEigyoSosiki> context) {
    }

    @Override
    public void postUpdate(MstEigyoSosiki entity, PostUpdateContext<MstEigyoSosiki> context) {
    }

    @Override
    public void postDelete(MstEigyoSosiki entity, PostDeleteContext<MstEigyoSosiki> context) {
    }
}