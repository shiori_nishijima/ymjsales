package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsAccountsListener implements EntityListener<CallsAccounts> {

    @Override
    public void preInsert(CallsAccounts entity, PreInsertContext<CallsAccounts> context) {
    }

    @Override
    public void preUpdate(CallsAccounts entity, PreUpdateContext<CallsAccounts> context) {
    }

    @Override
    public void preDelete(CallsAccounts entity, PreDeleteContext<CallsAccounts> context) {
    }

    @Override
    public void postInsert(CallsAccounts entity, PostInsertContext<CallsAccounts> context) {
    }

    @Override
    public void postUpdate(CallsAccounts entity, PostUpdateContext<CallsAccounts> context) {
    }

    @Override
    public void postDelete(CallsAccounts entity, PostDeleteContext<CallsAccounts> context) {
    }
}