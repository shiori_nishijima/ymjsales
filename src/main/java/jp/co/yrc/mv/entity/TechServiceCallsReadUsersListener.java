package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCallsReadUsersListener implements EntityListener<TechServiceCallsReadUsers> {

    @Override
    public void preInsert(TechServiceCallsReadUsers entity, PreInsertContext<TechServiceCallsReadUsers> context) {
    }

    @Override
    public void preUpdate(TechServiceCallsReadUsers entity, PreUpdateContext<TechServiceCallsReadUsers> context) {
    }

    @Override
    public void preDelete(TechServiceCallsReadUsers entity, PreDeleteContext<TechServiceCallsReadUsers> context) {
    }

    @Override
    public void postInsert(TechServiceCallsReadUsers entity, PostInsertContext<TechServiceCallsReadUsers> context) {
    }

    @Override
    public void postUpdate(TechServiceCallsReadUsers entity, PostUpdateContext<TechServiceCallsReadUsers> context) {
    }

    @Override
    public void postDelete(TechServiceCallsReadUsers entity, PostDeleteContext<TechServiceCallsReadUsers> context) {
    }
}