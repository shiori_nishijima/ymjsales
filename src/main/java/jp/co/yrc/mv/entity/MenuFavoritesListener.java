package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MenuFavoritesListener implements EntityListener<MenuFavorites> {

    @Override
    public void preInsert(MenuFavorites entity, PreInsertContext<MenuFavorites> context) {
    }

    @Override
    public void preUpdate(MenuFavorites entity, PreUpdateContext<MenuFavorites> context) {
    }

    @Override
    public void preDelete(MenuFavorites entity, PreDeleteContext<MenuFavorites> context) {
    }

    @Override
    public void postInsert(MenuFavorites entity, PostInsertContext<MenuFavorites> context) {
    }

    @Override
    public void postUpdate(MenuFavorites entity, PostUpdateContext<MenuFavorites> context) {
    }

    @Override
    public void postDelete(MenuFavorites entity, PostDeleteContext<MenuFavorites> context) {
    }
}