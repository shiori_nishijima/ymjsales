package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyMarketPlansListener implements EntityListener<MonthlyMarketPlans> {

    @Override
    public void preInsert(MonthlyMarketPlans entity, PreInsertContext<MonthlyMarketPlans> context) {
    }

    @Override
    public void preUpdate(MonthlyMarketPlans entity, PreUpdateContext<MonthlyMarketPlans> context) {
    }

    @Override
    public void preDelete(MonthlyMarketPlans entity, PreDeleteContext<MonthlyMarketPlans> context) {
    }

    @Override
    public void postInsert(MonthlyMarketPlans entity, PostInsertContext<MonthlyMarketPlans> context) {
    }

    @Override
    public void postUpdate(MonthlyMarketPlans entity, PostUpdateContext<MonthlyMarketPlans> context) {
    }

    @Override
    public void postDelete(MonthlyMarketPlans entity, PostDeleteContext<MonthlyMarketPlans> context) {
    }
}