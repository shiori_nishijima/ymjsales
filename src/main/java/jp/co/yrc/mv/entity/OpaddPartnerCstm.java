package jp.co.yrc.mv.entity;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OpaddPartnerCstmListener.class)
@Table(name = "opadd_partner_cstm")
public class OpaddPartnerCstm {

    /** ID */
    @Id
    @Column(name = "id_c")
    String idC;

    /**  */
    @Column(name = "test_c")
    String testC;

    /** 企業番号 */
    @Column(name = "account_id_c")
    String accountIdC;

    /** 企業名（ダミー） */
    @Column(name = "account_name_c")
    String accountNameC;

    /** 
     * Returns the idC.
     * 
     * @return the idC
     */
    public String getIdC() {
        return idC;
    }

    /** 
     * Sets the idC.
     * 
     * @param idC the idC
     */
    public void setIdC(String idC) {
        this.idC = idC;
    }

    /** 
     * Returns the testC.
     * 
     * @return the testC
     */
    public String getTestC() {
        return testC;
    }

    /** 
     * Sets the testC.
     * 
     * @param testC the testC
     */
    public void setTestC(String testC) {
        this.testC = testC;
    }

    /** 
     * Returns the accountIdC.
     * 
     * @return the accountIdC
     */
    public String getAccountIdC() {
        return accountIdC;
    }

    /** 
     * Sets the accountIdC.
     * 
     * @param accountIdC the accountIdC
     */
    public void setAccountIdC(String accountIdC) {
        this.accountIdC = accountIdC;
    }

    /** 
     * Returns the accountNameC.
     * 
     * @return the accountNameC
     */
    public String getAccountNameC() {
        return accountNameC;
    }

    /** 
     * Sets the accountNameC.
     * 
     * @param accountNameC the accountNameC
     */
    public void setAccountNameC(String accountNameC) {
        this.accountNameC = accountNameC;
    }
}