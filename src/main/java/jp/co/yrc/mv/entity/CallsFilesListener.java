package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsFilesListener implements EntityListener<CallsFiles> {

    @Override
    public void preInsert(CallsFiles entity, PreInsertContext<CallsFiles> context) {
    }

    @Override
    public void preUpdate(CallsFiles entity, PreUpdateContext<CallsFiles> context) {
    }

    @Override
    public void preDelete(CallsFiles entity, PreDeleteContext<CallsFiles> context) {
    }

    @Override
    public void postInsert(CallsFiles entity, PostInsertContext<CallsFiles> context) {
    }

    @Override
    public void postUpdate(CallsFiles entity, PostUpdateContext<CallsFiles> context) {
    }

    @Override
    public void postDelete(CallsFiles entity, PostDeleteContext<CallsFiles> context) {
    }
}