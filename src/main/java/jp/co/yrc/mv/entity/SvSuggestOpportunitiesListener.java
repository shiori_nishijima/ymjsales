package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SvSuggestOpportunitiesListener implements EntityListener<SvSuggestOpportunities> {

    @Override
    public void preInsert(SvSuggestOpportunities entity, PreInsertContext<SvSuggestOpportunities> context) {
    }

    @Override
    public void preUpdate(SvSuggestOpportunities entity, PreUpdateContext<SvSuggestOpportunities> context) {
    }

    @Override
    public void preDelete(SvSuggestOpportunities entity, PreDeleteContext<SvSuggestOpportunities> context) {
    }

    @Override
    public void postInsert(SvSuggestOpportunities entity, PostInsertContext<SvSuggestOpportunities> context) {
    }

    @Override
    public void postUpdate(SvSuggestOpportunities entity, PostUpdateContext<SvSuggestOpportunities> context) {
    }

    @Override
    public void postDelete(SvSuggestOpportunities entity, PostDeleteContext<SvSuggestOpportunities> context) {
    }
}