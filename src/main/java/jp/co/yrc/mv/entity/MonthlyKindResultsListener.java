package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyKindResultsListener implements EntityListener<MonthlyKindResults> {

    @Override
    public void preInsert(MonthlyKindResults entity, PreInsertContext<MonthlyKindResults> context) {
    }

    @Override
    public void preUpdate(MonthlyKindResults entity, PreUpdateContext<MonthlyKindResults> context) {
    }

    @Override
    public void preDelete(MonthlyKindResults entity, PreDeleteContext<MonthlyKindResults> context) {
    }

    @Override
    public void postInsert(MonthlyKindResults entity, PostInsertContext<MonthlyKindResults> context) {
    }

    @Override
    public void postUpdate(MonthlyKindResults entity, PostUpdateContext<MonthlyKindResults> context) {
    }

    @Override
    public void postDelete(MonthlyKindResults entity, PostDeleteContext<MonthlyKindResults> context) {
    }
}