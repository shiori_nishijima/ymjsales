package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsLikeUsersListener implements EntityListener<CallsLikeUsers> {

    @Override
    public void preInsert(CallsLikeUsers entity, PreInsertContext<CallsLikeUsers> context) {
    }

    @Override
    public void preUpdate(CallsLikeUsers entity, PreUpdateContext<CallsLikeUsers> context) {
    }

    @Override
    public void preDelete(CallsLikeUsers entity, PreDeleteContext<CallsLikeUsers> context) {
    }

    @Override
    public void postInsert(CallsLikeUsers entity, PostInsertContext<CallsLikeUsers> context) {
    }

    @Override
    public void postUpdate(CallsLikeUsers entity, PostUpdateContext<CallsLikeUsers> context) {
    }

    @Override
    public void postDelete(CallsLikeUsers entity, PostDeleteContext<CallsLikeUsers> context) {
    }
}