package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = SvCancelOpportunitiesListener.class)
@Table(name = "sv_cancel_opportunities")
public class SvCancelOpportunities {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 解約サービスCD */
    @Column(name = "sv_cancel_ida")
    String svCancelIda;

    /** 案件CD */
    @Column(name = "opportunities_idb")
    String opportunitiesIdb;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the svCancelIda.
     * 
     * @return the svCancelIda
     */
    public String getSvCancelIda() {
        return svCancelIda;
    }

    /** 
     * Sets the svCancelIda.
     * 
     * @param svCancelIda the svCancelIda
     */
    public void setSvCancelIda(String svCancelIda) {
        this.svCancelIda = svCancelIda;
    }

    /** 
     * Returns the opportunitiesIdb.
     * 
     * @return the opportunitiesIdb
     */
    public String getOpportunitiesIdb() {
        return opportunitiesIdb;
    }

    /** 
     * Sets the opportunitiesIdb.
     * 
     * @param opportunitiesIdb the opportunitiesIdb
     */
    public void setOpportunitiesIdb(String opportunitiesIdb) {
        this.opportunitiesIdb = opportunitiesIdb;
    }
}