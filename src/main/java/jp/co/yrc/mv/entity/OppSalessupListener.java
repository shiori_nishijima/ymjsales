package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OppSalessupListener implements EntityListener<OppSalessup> {

    @Override
    public void preInsert(OppSalessup entity, PreInsertContext<OppSalessup> context) {
    }

    @Override
    public void preUpdate(OppSalessup entity, PreUpdateContext<OppSalessup> context) {
    }

    @Override
    public void preDelete(OppSalessup entity, PreDeleteContext<OppSalessup> context) {
    }

    @Override
    public void postInsert(OppSalessup entity, PostInsertContext<OppSalessup> context) {
    }

    @Override
    public void postUpdate(OppSalessup entity, PostUpdateContext<OppSalessup> context) {
    }

    @Override
    public void postDelete(OppSalessup entity, PostDeleteContext<OppSalessup> context) {
    }
}