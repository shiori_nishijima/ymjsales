package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class OppSeOpportunitiesCListener implements EntityListener<OppSeOpportunitiesC> {

    @Override
    public void preInsert(OppSeOpportunitiesC entity, PreInsertContext<OppSeOpportunitiesC> context) {
    }

    @Override
    public void preUpdate(OppSeOpportunitiesC entity, PreUpdateContext<OppSeOpportunitiesC> context) {
    }

    @Override
    public void preDelete(OppSeOpportunitiesC entity, PreDeleteContext<OppSeOpportunitiesC> context) {
    }

    @Override
    public void postInsert(OppSeOpportunitiesC entity, PostInsertContext<OppSeOpportunitiesC> context) {
    }

    @Override
    public void postUpdate(OppSeOpportunitiesC entity, PostUpdateContext<OppSeOpportunitiesC> context) {
    }

    @Override
    public void postDelete(OppSeOpportunitiesC entity, PostDeleteContext<OppSeOpportunitiesC> context) {
    }
}