package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MastSkillListener.class)
@Table(name = "mast_skill")
public class MastSkill {

    /** 依頼案件種別CD */
    @Id
    @Column(name = "id")
    String id;

    /** 依頼案件種別名称 */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細 */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** カテゴリCD */
    @Column(name = "category_cd")
    String categoryCd;

    /** カテゴリ名称 */
    @Column(name = "category_nm")
    String categoryNm;

    /** 表示順番号 */
    @Column(name = "orderno")
    Integer orderno;

    /** 対応内容算出係数 */
    @Column(name = "cal_coefficient")
    Double calCoefficient;

    /** 対応内容算出方法 */
    @Column(name = "cal_method")
    Integer calMethod;

    /** 予備 */
    @Column(name = "other1")
    String other1;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the categoryCd.
     * 
     * @return the categoryCd
     */
    public String getCategoryCd() {
        return categoryCd;
    }

    /** 
     * Sets the categoryCd.
     * 
     * @param categoryCd the categoryCd
     */
    public void setCategoryCd(String categoryCd) {
        this.categoryCd = categoryCd;
    }

    /** 
     * Returns the categoryNm.
     * 
     * @return the categoryNm
     */
    public String getCategoryNm() {
        return categoryNm;
    }

    /** 
     * Sets the categoryNm.
     * 
     * @param categoryNm the categoryNm
     */
    public void setCategoryNm(String categoryNm) {
        this.categoryNm = categoryNm;
    }

    /** 
     * Returns the orderno.
     * 
     * @return the orderno
     */
    public Integer getOrderno() {
        return orderno;
    }

    /** 
     * Sets the orderno.
     * 
     * @param orderno the orderno
     */
    public void setOrderno(Integer orderno) {
        this.orderno = orderno;
    }

    /** 
     * Returns the calCoefficient.
     * 
     * @return the calCoefficient
     */
    public Double getCalCoefficient() {
        return calCoefficient;
    }

    /** 
     * Sets the calCoefficient.
     * 
     * @param calCoefficient the calCoefficient
     */
    public void setCalCoefficient(Double calCoefficient) {
        this.calCoefficient = calCoefficient;
    }

    /** 
     * Returns the calMethod.
     * 
     * @return the calMethod
     */
    public Integer getCalMethod() {
        return calMethod;
    }

    /** 
     * Sets the calMethod.
     * 
     * @param calMethod the calMethod
     */
    public void setCalMethod(Integer calMethod) {
        this.calMethod = calMethod;
    }

    /** 
     * Returns the other1.
     * 
     * @return the other1
     */
    public String getOther1() {
        return other1;
    }

    /** 
     * Sets the other1.
     * 
     * @param other1 the other1
     */
    public void setOther1(String other1) {
        this.other1 = other1;
    }
}