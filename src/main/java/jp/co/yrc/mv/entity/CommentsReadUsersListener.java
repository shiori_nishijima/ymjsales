package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CommentsReadUsersListener implements EntityListener<CommentsReadUsers> {

    @Override
    public void preInsert(CommentsReadUsers entity, PreInsertContext<CommentsReadUsers> context) {
    }

    @Override
    public void preUpdate(CommentsReadUsers entity, PreUpdateContext<CommentsReadUsers> context) {
    }

    @Override
    public void preDelete(CommentsReadUsers entity, PreDeleteContext<CommentsReadUsers> context) {
    }

    @Override
    public void postInsert(CommentsReadUsers entity, PostInsertContext<CommentsReadUsers> context) {
    }

    @Override
    public void postUpdate(CommentsReadUsers entity, PostUpdateContext<CommentsReadUsers> context) {
    }

    @Override
    public void postDelete(CommentsReadUsers entity, PostDeleteContext<CommentsReadUsers> context) {
    }
}