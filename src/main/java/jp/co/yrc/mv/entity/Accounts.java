package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = AccountsListener.class)
@Table(name = "accounts")
public class Accounts {

    /** 取引先コード */
    @Id
    @Column(name = "id")
    String id;

    /** 名称 */
    @Column(name = "name")
    String name;

    /** 名称（母国語） */
    @Column(name = "name_native")
    String nameNative;

    /** 郵便番号 */
    @Column(name = "billing_address_postalcode")
    String billingAddressPostalcode;

    /** 住所 */
    @Column(name = "billing_address")
    String billingAddress;

    /** 住所（母国語） */
    @Column(name = "billing_address_native")
    String billingAddressNative;

    /** 親コード */
    @Column(name = "parent_id")
    String parentId;

    /** 担当者 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 販社コード */
    @Column(name = "sales_company_code")
    String salesCompanyCode;

    /** 部門コード */
    @Column(name = "department_code")
    String departmentCode;

    /** 営業所コード */
    @Column(name = "sales_office_code")
    String salesOfficeCode;

    /** 検索用名称 */
    @Column(name = "name_for_search")
    String nameForSearch;

    /** 検索用名称（母国語） */
    @Column(name = "name_for_search_native")
    String nameForSearchNative;

    /** 販路 */
    @Column(name = "market_id")
    String marketId;

    /** 銘柄 */
    @Column(name = "brand_id")
    String brandId;

    /** 持軒数カウント対象 */
    @Column(name = "own_count")
    boolean ownCount;

    /** 取引軒数カウント対象 */
    @Column(name = "deal_count")
    boolean dealCount;

    /** グルーピング重点 */
    @Column(name = "group_important")
    boolean groupImportant;

    /** グルーピング新規 */
    @Column(name = "group_new")
    boolean groupNew;

    /** グルーピング深耕 */
    @Column(name = "group_deep_plowing")
    boolean groupDeepPlowing;

    /** グルーピングYコンセプトショップ */
    @Column(name = "group_y_cp")
    boolean groupYCp;

    /** グルーピング他社コンセプトショップ */
    @Column(name = "group_other_cp")
    boolean groupOtherCp;

    /** グルーピング1G */
    @Column(name = "group_one")
    boolean groupOne;

    /** グルーピング2G */
    @Column(name = "group_two")
    boolean groupTwo;

    /** グルーピング3G */
    @Column(name = "group_three")
    boolean groupThree;

    /** グルーピング4G */
    @Column(name = "group_four")
    boolean groupFour;

    /** グルーピング5G */
    @Column(name = "group_five")
    boolean groupFive;

    /** 売上ランク */
    @Column(name = "sales_rank")
    String salesRank;

    /** 需要本数 */
    @Column(name = "demand_number")
    BigDecimal demandNumber;

    /** 推定ISSYH */
    @Column(name = "iss_yh")
    BigDecimal issYh;

    /** 推定ISS01 */
    @Column(name = "iss01")
    BigDecimal iss01;

    /** 推定ISS02 */
    @Column(name = "iss02")
    BigDecimal iss02;

    /** 推定ISS03 */
    @Column(name = "iss03")
    BigDecimal iss03;

    /** 推定ISS04 */
    @Column(name = "iss04")
    BigDecimal iss04;

    /** 推定ISS05 */
    @Column(name = "iss05")
    BigDecimal iss05;

    /** 推定ISS06 */
    @Column(name = "iss06")
    BigDecimal iss06;

    /** 推定ISS07 */
    @Column(name = "iss07")
    BigDecimal iss07;

    /** 推定ISS08 */
    @Column(name = "iss08")
    BigDecimal iss08;

    /** 推定ISS09 */
    @Column(name = "iss09")
    BigDecimal iss09;

    /** 推定ISS10 */
    @Column(name = "iss10")
    BigDecimal iss10;

    /** 未取引先フラグ */
    @Column(name = "undeal")
    boolean undeal;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the nameKana.
     * 
     * @return the nameKana
     */
    public String getNameNative() {
        return nameNative;
    }

    /** 
     * Sets the nameKana.
     * 
     * @param nameKana the nameKana
     */
    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }

    /** 
     * Returns the billingAddressPostalcode.
     * 
     * @return the billingAddressPostalcode
     */
    public String getBillingAddressPostalcode() {
        return billingAddressPostalcode;
    }

    /** 
     * Sets the billingAddressPostalcode.
     * 
     * @param billingAddressPostalcode the billingAddressPostalcode
     */
    public void setBillingAddressPostalcode(String billingAddressPostalcode) {
        this.billingAddressPostalcode = billingAddressPostalcode;
    }

    /** 
     * Returns the billingAddress.
     * 
     * @return the billingAddress
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /** 
     * Sets the billingAddress.
     * 
     * @param billingAddress the billingAddress
     */
    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /** 
     * Returns the billingAddressNative.
     * 
     * @return the billingAddressNative
     */
    public String getBillingAddressNative() {
        return billingAddressNative;
    }

    /** 
     * Sets the billingAddressNative.
     * 
     * @param billingAddressNative the billingAddressNative
     */
    public void setBillingAddressNative(String billingAddressNative) {
        this.billingAddressNative = billingAddressNative;
    }

    /** 
     * Returns the parentId.
     * 
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /** 
     * Sets the parentId.
     * 
     * @param parentId the parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the salesCompanyCode.
     * 
     * @return the salesCompanyCode
     */
    public String getSalesCompanyCode() {
        return salesCompanyCode;
    }

    /** 
     * Sets the salesCompanyCode.
     * 
     * @param salesCompanyCode the salesCompanyCode
     */
    public void setSalesCompanyCode(String salesCompanyCode) {
        this.salesCompanyCode = salesCompanyCode;
    }

    /** 
     * Returns the departmentCode.
     * 
     * @return the departmentCode
     */
    public String getDepartmentCode() {
        return departmentCode;
    }

    /** 
     * Sets the departmentCode.
     * 
     * @param departmentCode the departmentCode
     */
    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    /** 
     * Returns the salesOfficeCode.
     * 
     * @return the salesOfficeCode
     */
    public String getSalesOfficeCode() {
        return salesOfficeCode;
    }

    /** 
     * Sets the salesOfficeCode.
     * 
     * @param salesOfficeCode the salesOfficeCode
     */
    public void setSalesOfficeCode(String salesOfficeCode) {
        this.salesOfficeCode = salesOfficeCode;
    }

    /** 
     * Returns the nameForSearch.
     * 
     * @return the nameForSearch
     */
    public String getNameForSearch() {
        return nameForSearch;
    }

    /** 
     * Sets the nameForSearch.
     * 
     * @param nameForSearch the nameForSearch
     */
    public void setNameForSearch(String nameForSearch) {
        this.nameForSearch = nameForSearch;
    }

    /** 
     * Returns the nameForSearchNative.
     * 
     * @return the nameForSearchNative
     */
    public String getNameForSearchNative() {
        return nameForSearchNative;
    }

    /** 
     * Sets the nameForSearchNative.
     * 
     * @param nameForSearch the nameForSearchNative
     */
    public void setNameForSearchNative(String nameForSearchNative) {
        this.nameForSearchNative = nameForSearchNative;
    }

    /** 
     * Returns the marketId.
     * 
     * @return the marketId
     */
    public String getMarketId() {
        return marketId;
    }

    /** 
     * Sets the marketId.
     * 
     * @param marketId the marketId
     */
    public void setMarketId(String marketId) {
        this.marketId = marketId;
    }

    /** 
     * Returns the brandId.
     * 
     * @return the brandId
     */
    public String getBrandId() {
        return brandId;
    }

    /** 
     * Sets the brandId.
     * 
     * @param brandId the brandId
     */
    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    /** 
     * Returns the ownCount.
     * 
     * @return the ownCount
     */
    public boolean isOwnCount() {
        return ownCount;
    }

    /** 
     * Sets the ownCount.
     * 
     * @param ownCount the ownCount
     */
    public void setOwnCount(boolean ownCount) {
        this.ownCount = ownCount;
    }

    /** 
     * Returns the dealCount.
     * 
     * @return the dealCount
     */
    public boolean isDealCount() {
        return dealCount;
    }

    /** 
     * Sets the dealCount.
     * 
     * @param dealCount the dealCount
     */
    public void setDealCount(boolean dealCount) {
        this.dealCount = dealCount;
    }

    /** 
     * Returns the groupImportant.
     * 
     * @return the groupImportant
     */
    public boolean isGroupImportant() {
        return groupImportant;
    }

    /** 
     * Sets the groupImportant.
     * 
     * @param groupImportant the groupImportant
     */
    public void setGroupImportant(boolean groupImportant) {
        this.groupImportant = groupImportant;
    }

    /** 
     * Returns the groupNew.
     * 
     * @return the groupNew
     */
    public boolean isGroupNew() {
        return groupNew;
    }

    /** 
     * Sets the groupNew.
     * 
     * @param groupNew the groupNew
     */
    public void setGroupNew(boolean groupNew) {
        this.groupNew = groupNew;
    }

    /** 
     * Returns the groupDeepPlowing.
     * 
     * @return the groupDeepPlowing
     */
    public boolean isGroupDeepPlowing() {
        return groupDeepPlowing;
    }

    /** 
     * Sets the groupDeepPlowing.
     * 
     * @param groupDeepPlowing the groupDeepPlowing
     */
    public void setGroupDeepPlowing(boolean groupDeepPlowing) {
        this.groupDeepPlowing = groupDeepPlowing;
    }

    /** 
     * Returns the groupYCp.
     * 
     * @return the groupYCp
     */
    public boolean isGroupYCp() {
        return groupYCp;
    }

    /** 
     * Sets the groupYCp.
     * 
     * @param groupYCp the groupYCp
     */
    public void setGroupYCp(boolean groupYCp) {
        this.groupYCp = groupYCp;
    }

    /** 
     * Returns the groupOtherCp.
     * 
     * @return the groupOtherCp
     */
    public boolean isGroupOtherCp() {
        return groupOtherCp;
    }

    /** 
     * Sets the groupOtherCp.
     * 
     * @param groupOtherCp the groupOtherCp
     */
    public void setGroupOtherCp(boolean groupOtherCp) {
        this.groupOtherCp = groupOtherCp;
    }

    /** 
     * Returns the groupOne.
     * 
     * @return the groupOne
     */
    public boolean isGroupOne() {
        return groupOne;
    }

    /** 
     * Sets the groupOne.
     * 
     * @param groupOne the groupOne
     */
    public void setGroupOne(boolean groupOne) {
        this.groupOne = groupOne;
    }

    /** 
     * Returns the groupTwo.
     * 
     * @return the groupTwo
     */
    public boolean isGroupTwo() {
        return groupTwo;
    }

    /** 
     * Sets the groupTwo.
     * 
     * @param groupTwo the groupTwo
     */
    public void setGroupTwo(boolean groupTwo) {
        this.groupTwo = groupTwo;
    }

    /** 
     * Returns the groupThree.
     * 
     * @return the groupThree
     */
    public boolean isGroupThree() {
        return groupThree;
    }

    /** 
     * Sets the groupThree.
     * 
     * @param groupThree the groupThree
     */
    public void setGroupThree(boolean groupThree) {
        this.groupThree = groupThree;
    }

    /** 
     * Returns the groupFour.
     * 
     * @return the groupFour
     */
    public boolean isGroupFour() {
        return groupFour;
    }

    /** 
     * Sets the groupFour.
     * 
     * @param groupFour the groupFour
     */
    public void setGroupFour(boolean groupFour) {
        this.groupFour = groupFour;
    }

    /** 
     * Returns the groupFive.
     * 
     * @return the groupFive
     */
    public boolean isGroupFive() {
        return groupFive;
    }

    /** 
     * Sets the groupFive.
     * 
     * @param groupFive the groupFive
     */
    public void setGroupFive(boolean groupFive) {
        this.groupFive = groupFive;
    }

    /** 
     * Returns the salesRank.
     * 
     * @return the salesRank
     */
    public String getSalesRank() {
        return salesRank;
    }

    /** 
     * Sets the salesRank.
     * 
     * @param salesRank the salesRank
     */
    public void setSalesRank(String salesRank) {
        this.salesRank = salesRank;
    }

    /** 
     * Returns the demandNumber.
     * 
     * @return the demandNumber
     */
    public BigDecimal getDemandNumber() {
        return demandNumber;
    }

    /** 
     * Sets the demandNumber.
     * 
     * @param demandNumber the demandNumber
     */
    public void setDemandNumber(BigDecimal demandNumber) {
        this.demandNumber = demandNumber;
    }

    /** 
     * Returns the issYh.
     * 
     * @return the issYh
     */
    public BigDecimal getIssYh() {
        return issYh;
    }

    /** 
     * Sets the issYh.
     * 
     * @param issYh the issYh
     */
    public void setIssYh(BigDecimal issYh) {
        this.issYh = issYh;
    }

    public BigDecimal getIss01() {
		return iss01;
	}

	public void setIss01(BigDecimal iss01) {
		this.iss01 = iss01;
	}

	public BigDecimal getIss02() {
		return iss02;
	}

	public void setIss02(BigDecimal iss02) {
		this.iss02 = iss02;
	}

	public BigDecimal getIss03() {
		return iss03;
	}

	public void setIss03(BigDecimal iss03) {
		this.iss03 = iss03;
	}

	public BigDecimal getIss04() {
		return iss04;
	}

	public void setIss04(BigDecimal iss04) {
		this.iss04 = iss04;
	}

	public BigDecimal getIss05() {
		return iss05;
	}

	public void setIss05(BigDecimal iss05) {
		this.iss05 = iss05;
	}

	public BigDecimal getIss06() {
		return iss06;
	}

	public void setIss06(BigDecimal iss06) {
		this.iss06 = iss06;
	}

	public BigDecimal getIss07() {
		return iss07;
	}

	public void setIss07(BigDecimal iss07) {
		this.iss07 = iss07;
	}

	public BigDecimal getIss08() {
		return iss08;
	}

	public void setIss08(BigDecimal iss08) {
		this.iss08 = iss08;
	}

	public BigDecimal getIss09() {
		return iss09;
	}

	public void setIss09(BigDecimal iss09) {
		this.iss09 = iss09;
	}

	public BigDecimal getIss10() {
		return iss10;
	}

	public void setIss10(BigDecimal iss10) {
		this.iss10 = iss10;
	}

	/** 
     * Returns the undeal.
     * 
     * @return the undeal
     */
    public boolean isUndeal() {
        return undeal;
    }

    /** 
     * Sets the undeal.
     * 
     * @param undeal the undeal
     */
    public void setUndeal(boolean undeal) {
        this.undeal = undeal;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}