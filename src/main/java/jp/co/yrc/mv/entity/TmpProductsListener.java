package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TmpProductsListener implements EntityListener<TmpProducts> {

    @Override
    public void preInsert(TmpProducts entity, PreInsertContext<TmpProducts> context) {
    }

    @Override
    public void preUpdate(TmpProducts entity, PreUpdateContext<TmpProducts> context) {
    }

    @Override
    public void preDelete(TmpProducts entity, PreDeleteContext<TmpProducts> context) {
    }

    @Override
    public void postInsert(TmpProducts entity, PostInsertContext<TmpProducts> context) {
    }

    @Override
    public void postUpdate(TmpProducts entity, PostUpdateContext<TmpProducts> context) {
    }

    @Override
    public void postDelete(TmpProducts entity, PostDeleteContext<TmpProducts> context) {
    }
}