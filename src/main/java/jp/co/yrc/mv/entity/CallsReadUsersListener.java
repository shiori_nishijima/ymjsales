package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class CallsReadUsersListener implements EntityListener<CallsReadUsers> {

    @Override
    public void preInsert(CallsReadUsers entity, PreInsertContext<CallsReadUsers> context) {
    }

    @Override
    public void preUpdate(CallsReadUsers entity, PreUpdateContext<CallsReadUsers> context) {
    }

    @Override
    public void preDelete(CallsReadUsers entity, PreDeleteContext<CallsReadUsers> context) {
    }

    @Override
    public void postInsert(CallsReadUsers entity, PostInsertContext<CallsReadUsers> context) {
    }

    @Override
    public void postUpdate(CallsReadUsers entity, PostUpdateContext<CallsReadUsers> context) {
    }

    @Override
    public void postDelete(CallsReadUsers entity, PostDeleteContext<CallsReadUsers> context) {
    }
}