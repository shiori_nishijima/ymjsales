package jp.co.yrc.mv.entity;

import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MastServicelineListener.class)
@Table(name = "mast_serviceline")
public class MastServiceline {

    /** サービスラインCD */
    @Id
    @Column(name = "id")
    String id;

    /** サービスライン名称 */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細（未使用） */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** アサイン先（未使用） */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** サービスライン略称（未使用） */
    @Column(name = "svlsnm")
    String svlsnm;

    /** サービスライングループCD */
    @Column(name = "slgrcd")
    String slgrcd;

    /** サービスライングループ名称 */
    @Column(name = "slgrnm")
    String slgrnm;

    /** 表示順番号 */
    @Column(name = "ordrno")
    Integer ordrno;

    /** デフォルト予測数量区分 */
    @Column(name = "fcqtcl")
    Integer fcqtcl;

    /** 予測数量区分固定フラグ（未使用） */
    @Column(name = "fqfxfg")
    Integer fqfxfg;

    /** サービス開始年月要否フラグ（未使用） */
    @Column(name = "fmymfg")
    Integer fmymfg;

    /** サービス終了年月要否フラグ（未使用） */
    @Column(name = "toymfg")
    Integer toymfg;

    /** 評価回線数算出係数 */
    @Column(name = "evlnrt")
    Double evlnrt;

    /** キードライバ換算フラグ */
    @Column(name = "dvmtfg")
    Integer dvmtfg;

    /**  */
    @Column(name = "svdtfg")
    Double svdtfg;

    /** デフォルト予測数量区分名称 */
    @Column(name = "fcqtnm")
    String fcqtnm;

    /** キードライバーID */
    @Column(name = "key_driver_id")
    String keyDriverId;

    /** キードライバー名称 */
    @Column(name = "key_driver_name")
    String keyDriverName;

    /** キードライバー内訳表示順 */
    @Column(name = "key_driver_ordrno")
    Integer keyDriverOrdrno;

    /** 与信グループID */
    @Column(name = "lending_guarantee_id")
    String lendingGuaranteeId;

    /** 与信グループ名称 */
    @Column(name = "lending_guarantee_name")
    String lendingGuaranteeName;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the svlsnm.
     * 
     * @return the svlsnm
     */
    public String getSvlsnm() {
        return svlsnm;
    }

    /** 
     * Sets the svlsnm.
     * 
     * @param svlsnm the svlsnm
     */
    public void setSvlsnm(String svlsnm) {
        this.svlsnm = svlsnm;
    }

    /** 
     * Returns the slgrcd.
     * 
     * @return the slgrcd
     */
    public String getSlgrcd() {
        return slgrcd;
    }

    /** 
     * Sets the slgrcd.
     * 
     * @param slgrcd the slgrcd
     */
    public void setSlgrcd(String slgrcd) {
        this.slgrcd = slgrcd;
    }

    /** 
     * Returns the slgrnm.
     * 
     * @return the slgrnm
     */
    public String getSlgrnm() {
        return slgrnm;
    }

    /** 
     * Sets the slgrnm.
     * 
     * @param slgrnm the slgrnm
     */
    public void setSlgrnm(String slgrnm) {
        this.slgrnm = slgrnm;
    }

    /** 
     * Returns the ordrno.
     * 
     * @return the ordrno
     */
    public Integer getOrdrno() {
        return ordrno;
    }

    /** 
     * Sets the ordrno.
     * 
     * @param ordrno the ordrno
     */
    public void setOrdrno(Integer ordrno) {
        this.ordrno = ordrno;
    }

    /** 
     * Returns the fcqtcl.
     * 
     * @return the fcqtcl
     */
    public Integer getFcqtcl() {
        return fcqtcl;
    }

    /** 
     * Sets the fcqtcl.
     * 
     * @param fcqtcl the fcqtcl
     */
    public void setFcqtcl(Integer fcqtcl) {
        this.fcqtcl = fcqtcl;
    }

    /** 
     * Returns the fqfxfg.
     * 
     * @return the fqfxfg
     */
    public Integer getFqfxfg() {
        return fqfxfg;
    }

    /** 
     * Sets the fqfxfg.
     * 
     * @param fqfxfg the fqfxfg
     */
    public void setFqfxfg(Integer fqfxfg) {
        this.fqfxfg = fqfxfg;
    }

    /** 
     * Returns the fmymfg.
     * 
     * @return the fmymfg
     */
    public Integer getFmymfg() {
        return fmymfg;
    }

    /** 
     * Sets the fmymfg.
     * 
     * @param fmymfg the fmymfg
     */
    public void setFmymfg(Integer fmymfg) {
        this.fmymfg = fmymfg;
    }

    /** 
     * Returns the toymfg.
     * 
     * @return the toymfg
     */
    public Integer getToymfg() {
        return toymfg;
    }

    /** 
     * Sets the toymfg.
     * 
     * @param toymfg the toymfg
     */
    public void setToymfg(Integer toymfg) {
        this.toymfg = toymfg;
    }

    /** 
     * Returns the evlnrt.
     * 
     * @return the evlnrt
     */
    public Double getEvlnrt() {
        return evlnrt;
    }

    /** 
     * Sets the evlnrt.
     * 
     * @param evlnrt the evlnrt
     */
    public void setEvlnrt(Double evlnrt) {
        this.evlnrt = evlnrt;
    }

    /** 
     * Returns the dvmtfg.
     * 
     * @return the dvmtfg
     */
    public Integer getDvmtfg() {
        return dvmtfg;
    }

    /** 
     * Sets the dvmtfg.
     * 
     * @param dvmtfg the dvmtfg
     */
    public void setDvmtfg(Integer dvmtfg) {
        this.dvmtfg = dvmtfg;
    }

    /** 
     * Returns the svdtfg.
     * 
     * @return the svdtfg
     */
    public Double getSvdtfg() {
        return svdtfg;
    }

    /** 
     * Sets the svdtfg.
     * 
     * @param svdtfg the svdtfg
     */
    public void setSvdtfg(Double svdtfg) {
        this.svdtfg = svdtfg;
    }

    /** 
     * Returns the fcqtnm.
     * 
     * @return the fcqtnm
     */
    public String getFcqtnm() {
        return fcqtnm;
    }

    /** 
     * Sets the fcqtnm.
     * 
     * @param fcqtnm the fcqtnm
     */
    public void setFcqtnm(String fcqtnm) {
        this.fcqtnm = fcqtnm;
    }

    /** 
     * Returns the keyDriverId.
     * 
     * @return the keyDriverId
     */
    public String getKeyDriverId() {
        return keyDriverId;
    }

    /** 
     * Sets the keyDriverId.
     * 
     * @param keyDriverId the keyDriverId
     */
    public void setKeyDriverId(String keyDriverId) {
        this.keyDriverId = keyDriverId;
    }

    /** 
     * Returns the keyDriverName.
     * 
     * @return the keyDriverName
     */
    public String getKeyDriverName() {
        return keyDriverName;
    }

    /** 
     * Sets the keyDriverName.
     * 
     * @param keyDriverName the keyDriverName
     */
    public void setKeyDriverName(String keyDriverName) {
        this.keyDriverName = keyDriverName;
    }

    /** 
     * Returns the keyDriverOrdrno.
     * 
     * @return the keyDriverOrdrno
     */
    public Integer getKeyDriverOrdrno() {
        return keyDriverOrdrno;
    }

    /** 
     * Sets the keyDriverOrdrno.
     * 
     * @param keyDriverOrdrno the keyDriverOrdrno
     */
    public void setKeyDriverOrdrno(Integer keyDriverOrdrno) {
        this.keyDriverOrdrno = keyDriverOrdrno;
    }

    /** 
     * Returns the lendingGuaranteeId.
     * 
     * @return the lendingGuaranteeId
     */
    public String getLendingGuaranteeId() {
        return lendingGuaranteeId;
    }

    /** 
     * Sets the lendingGuaranteeId.
     * 
     * @param lendingGuaranteeId the lendingGuaranteeId
     */
    public void setLendingGuaranteeId(String lendingGuaranteeId) {
        this.lendingGuaranteeId = lendingGuaranteeId;
    }

    /** 
     * Returns the lendingGuaranteeName.
     * 
     * @return the lendingGuaranteeName
     */
    public String getLendingGuaranteeName() {
        return lendingGuaranteeName;
    }

    /** 
     * Sets the lendingGuaranteeName.
     * 
     * @param lendingGuaranteeName the lendingGuaranteeName
     */
    public void setLendingGuaranteeName(String lendingGuaranteeName) {
        this.lendingGuaranteeName = lendingGuaranteeName;
    }
}