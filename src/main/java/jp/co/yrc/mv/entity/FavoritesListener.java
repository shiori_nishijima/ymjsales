package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class FavoritesListener implements EntityListener<Favorites> {

    @Override
    public void preInsert(Favorites entity, PreInsertContext<Favorites> context) {
    }

    @Override
    public void preUpdate(Favorites entity, PreUpdateContext<Favorites> context) {
    }

    @Override
    public void preDelete(Favorites entity, PreDeleteContext<Favorites> context) {
    }

    @Override
    public void postInsert(Favorites entity, PostInsertContext<Favorites> context) {
    }

    @Override
    public void postUpdate(Favorites entity, PostUpdateContext<Favorites> context) {
    }

    @Override
    public void postDelete(Favorites entity, PostDeleteContext<Favorites> context) {
    }
}