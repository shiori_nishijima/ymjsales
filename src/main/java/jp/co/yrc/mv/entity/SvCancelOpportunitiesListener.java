package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class SvCancelOpportunitiesListener implements EntityListener<SvCancelOpportunities> {

    @Override
    public void preInsert(SvCancelOpportunities entity, PreInsertContext<SvCancelOpportunities> context) {
    }

    @Override
    public void preUpdate(SvCancelOpportunities entity, PreUpdateContext<SvCancelOpportunities> context) {
    }

    @Override
    public void preDelete(SvCancelOpportunities entity, PreDeleteContext<SvCancelOpportunities> context) {
    }

    @Override
    public void postInsert(SvCancelOpportunities entity, PostInsertContext<SvCancelOpportunities> context) {
    }

    @Override
    public void postUpdate(SvCancelOpportunities entity, PostUpdateContext<SvCancelOpportunities> context) {
    }

    @Override
    public void postDelete(SvCancelOpportunities entity, PostDeleteContext<SvCancelOpportunities> context) {
    }
}