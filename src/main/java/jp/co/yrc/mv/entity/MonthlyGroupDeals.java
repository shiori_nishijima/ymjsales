package jp.co.yrc.mv.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = MonthlyGroupDealsListener.class)
@Table(name = "monthly_group_deals")
public class MonthlyGroupDeals {

    /** 年 */
    @Id
    @Column(name = "year")
    Integer year;

    /** 月 */
    @Id
    @Column(name = "month")
    Integer month;

    /** ユーザーID */
    @Id
    @Column(name = "user_id")
    String userId;

    /** 所属組織 */
    @Id
    @Column(name = "eigyo_sosiki_cd")
    String eigyoSosikiCd;

    /** Aランク持軒数 */
    @Column(name = "rank_a_own_count")
    BigDecimal rankAOwnCount;

    /** Aランク取引軒数 */
    @Column(name = "rank_a_deal_count")
    BigDecimal rankADealCount;

    /** Aランク取引得M数 */
    @Column(name = "rank_a_account_master_count")
    BigDecimal rankAAccountMasterCount;

    /** Bランク持軒数 */
    @Column(name = "rank_b_own_count")
    BigDecimal rankBOwnCount;

    /** Bランク取引軒数 */
    @Column(name = "rank_b_deal_count")
    BigDecimal rankBDealCount;

    /** Bランク取引得M数 */
    @Column(name = "rank_b_account_master_count")
    BigDecimal rankBAccountMasterCount;

    /** Cランク持軒数 */
    @Column(name = "rank_c_own_count")
    BigDecimal rankCOwnCount;

    /** Cランク取引軒数 */
    @Column(name = "rank_c_deal_count")
    BigDecimal rankCDealCount;

    /** Cランク取引得M数 */
    @Column(name = "rank_c_account_master_count")
    BigDecimal rankCAccountMasterCount;

    /** 重点持軒数 */
    @Column(name = "important_own_count")
    BigDecimal importantOwnCount;

    /** 重点取引軒数 */
    @Column(name = "important_deal_count")
    BigDecimal importantDealCount;

    /** 重点取引得M数 */
    @Column(name = "important_account_master_count")
    BigDecimal importantAccountMasterCount;

    /** 新規持軒数 */
    @Column(name = "new_own_count")
    BigDecimal newOwnCount;

    /** 新規取引軒数 */
    @Column(name = "new_deal_count")
    BigDecimal newDealCount;

    /** 新規取引得M数 */
    @Column(name = "new_account_master_count")
    BigDecimal newAccountMasterCount;

    /** 深耕持軒数 */
    @Column(name = "deep_plowing_own_count")
    BigDecimal deepPlowingOwnCount;

    /** 深耕取引軒数 */
    @Column(name = "deep_plowing_deal_count")
    BigDecimal deepPlowingDealCount;

    /** 深耕取引得M数 */
    @Column(name = "deep_plowing_account_master_count")
    BigDecimal deepPlowingAccountMasterCount;

    /** Y-CP持軒数 */
    @Column(name = "y_cp_own_count")
    BigDecimal yCpOwnCount;

    /** Y-CP取引軒数 */
    @Column(name = "y_cp_deal_count")
    BigDecimal yCpDealCount;

    /** Y-CP取引得M数 */
    @Column(name = "y_cp_account_master_count")
    BigDecimal yCpAccountMasterCount;

    /** 他社CP持軒数 */
    @Column(name = "other_cp_own_count")
    BigDecimal otherCpOwnCount;

    /** 他社CP取引軒数 */
    @Column(name = "other_cp_deal_count")
    BigDecimal otherCpDealCount;

    /** 他社CP取引得M数 */
    @Column(name = "other_cp_account_master_count")
    BigDecimal otherCpAccountMasterCount;

    /** グループ1持軒数 */
    @Column(name = "group_one_own_count")
    BigDecimal groupOneOwnCount;

    /** グループ1取引数 */
    @Column(name = "group_one_deal_count")
    BigDecimal groupOneDealCount;

    /** グループ1取引得M数 */
    @Column(name = "group_one_account_master_count")
    BigDecimal groupOneAccountMasterCount;

    /** グループ2持軒数 */
    @Column(name = "group_two_own_count")
    BigDecimal groupTwoOwnCount;

    /** グループ2取引軒数 */
    @Column(name = "group_two_deal_count")
    BigDecimal groupTwoDealCount;

    /** グループ2取引得M数 */
    @Column(name = "group_two_account_master_count")
    BigDecimal groupTwoAccountMasterCount;

    /** グループ3持軒数 */
    @Column(name = "group_three_own_count")
    BigDecimal groupThreeOwnCount;

    /** グループ3取引軒数 */
    @Column(name = "group_three_deal_count")
    BigDecimal groupThreeDealCount;

    /** グループ3取引得M数 */
    @Column(name = "group_three_account_master_count")
    BigDecimal groupThreeAccountMasterCount;

    /** グループ4持軒数 */
    @Column(name = "group_four_own_count")
    BigDecimal groupFourOwnCount;

    /** グループ4取引軒数 */
    @Column(name = "group_four_deal_count")
    BigDecimal groupFourDealCount;

    /** グループ4取引得M数 */
    @Column(name = "group_four_account_master_count")
    BigDecimal groupFourAccountMasterCount;

    /** グループ5持軒数 */
    @Column(name = "group_five_own_count")
    BigDecimal groupFiveOwnCount;

    /** グループ5取引数 */
    @Column(name = "group_five_deal_count")
    BigDecimal groupFiveDealCount;

    /** グループ5取引得M数 */
    @Column(name = "group_five_account_master_count")
    BigDecimal groupFiveAccountMasterCount;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the year.
     * 
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /** 
     * Sets the year.
     * 
     * @param year the year
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * Returns the month.
     * 
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /** 
     * Sets the month.
     * 
     * @param month the month
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /** 
     * Returns the userId.
     * 
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /** 
     * Sets the userId.
     * 
     * @param userId the userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /** 
     * Returns the eigyoSosikiCd.
     * 
     * @return the eigyoSosikiCd
     */
    public String getEigyoSosikiCd() {
        return eigyoSosikiCd;
    }

    /** 
     * Sets the eigyoSosikiCd.
     * 
     * @param eigyoSosikiCd the eigyoSosikiCd
     */
    public void setEigyoSosikiCd(String eigyoSosikiCd) {
        this.eigyoSosikiCd = eigyoSosikiCd;
    }

    /** 
     * Returns the rankAOwnCount.
     * 
     * @return the rankAOwnCount
     */
    public BigDecimal getRankAOwnCount() {
        return rankAOwnCount;
    }

    /** 
     * Sets the rankAOwnCount.
     * 
     * @param rankAOwnCount the rankAOwnCount
     */
    public void setRankAOwnCount(BigDecimal rankAOwnCount) {
        this.rankAOwnCount = rankAOwnCount;
    }

    /** 
     * Returns the rankADealCount.
     * 
     * @return the rankADealCount
     */
    public BigDecimal getRankADealCount() {
        return rankADealCount;
    }

    /** 
     * Sets the rankADealCount.
     * 
     * @param rankADealCount the rankADealCount
     */
    public void setRankADealCount(BigDecimal rankADealCount) {
        this.rankADealCount = rankADealCount;
    }

    /** 
     * Returns the rankAAccountMasterCount.
     * 
     * @return the rankAAccountMasterCount
     */
    public BigDecimal getRankAAccountMasterCount() {
        return rankAAccountMasterCount;
    }

    /** 
     * Sets the rankAAccountMasterCount.
     * 
     * @param rankAAccountMasterCount the rankAAccountMasterCount
     */
    public void setRankAAccountMasterCount(BigDecimal rankAAccountMasterCount) {
        this.rankAAccountMasterCount = rankAAccountMasterCount;
    }

    /** 
     * Returns the rankBOwnCount.
     * 
     * @return the rankBOwnCount
     */
    public BigDecimal getRankBOwnCount() {
        return rankBOwnCount;
    }

    /** 
     * Sets the rankBOwnCount.
     * 
     * @param rankBOwnCount the rankBOwnCount
     */
    public void setRankBOwnCount(BigDecimal rankBOwnCount) {
        this.rankBOwnCount = rankBOwnCount;
    }

    /** 
     * Returns the rankBDealCount.
     * 
     * @return the rankBDealCount
     */
    public BigDecimal getRankBDealCount() {
        return rankBDealCount;
    }

    /** 
     * Sets the rankBDealCount.
     * 
     * @param rankBDealCount the rankBDealCount
     */
    public void setRankBDealCount(BigDecimal rankBDealCount) {
        this.rankBDealCount = rankBDealCount;
    }

    /** 
     * Returns the rankBAccountMasterCount.
     * 
     * @return the rankBAccountMasterCount
     */
    public BigDecimal getRankBAccountMasterCount() {
        return rankBAccountMasterCount;
    }

    /** 
     * Sets the rankBAccountMasterCount.
     * 
     * @param rankBAccountMasterCount the rankBAccountMasterCount
     */
    public void setRankBAccountMasterCount(BigDecimal rankBAccountMasterCount) {
        this.rankBAccountMasterCount = rankBAccountMasterCount;
    }

    /** 
     * Returns the rankCOwnCount.
     * 
     * @return the rankCOwnCount
     */
    public BigDecimal getRankCOwnCount() {
        return rankCOwnCount;
    }

    /** 
     * Sets the rankCOwnCount.
     * 
     * @param rankCOwnCount the rankCOwnCount
     */
    public void setRankCOwnCount(BigDecimal rankCOwnCount) {
        this.rankCOwnCount = rankCOwnCount;
    }

    /** 
     * Returns the rankCDealCount.
     * 
     * @return the rankCDealCount
     */
    public BigDecimal getRankCDealCount() {
        return rankCDealCount;
    }

    /** 
     * Sets the rankCDealCount.
     * 
     * @param rankCDealCount the rankCDealCount
     */
    public void setRankCDealCount(BigDecimal rankCDealCount) {
        this.rankCDealCount = rankCDealCount;
    }

    /** 
     * Returns the rankCAccountMasterCount.
     * 
     * @return the rankCAccountMasterCount
     */
    public BigDecimal getRankCAccountMasterCount() {
        return rankCAccountMasterCount;
    }

    /** 
     * Sets the rankCAccountMasterCount.
     * 
     * @param rankCAccountMasterCount the rankCAccountMasterCount
     */
    public void setRankCAccountMasterCount(BigDecimal rankCAccountMasterCount) {
        this.rankCAccountMasterCount = rankCAccountMasterCount;
    }

    /** 
     * Returns the importantOwnCount.
     * 
     * @return the importantOwnCount
     */
    public BigDecimal getImportantOwnCount() {
        return importantOwnCount;
    }

    /** 
     * Sets the importantOwnCount.
     * 
     * @param importantOwnCount the importantOwnCount
     */
    public void setImportantOwnCount(BigDecimal importantOwnCount) {
        this.importantOwnCount = importantOwnCount;
    }

    /** 
     * Returns the importantDealCount.
     * 
     * @return the importantDealCount
     */
    public BigDecimal getImportantDealCount() {
        return importantDealCount;
    }

    /** 
     * Sets the importantDealCount.
     * 
     * @param importantDealCount the importantDealCount
     */
    public void setImportantDealCount(BigDecimal importantDealCount) {
        this.importantDealCount = importantDealCount;
    }

    /** 
     * Returns the importantAccountMasterCount.
     * 
     * @return the importantAccountMasterCount
     */
    public BigDecimal getImportantAccountMasterCount() {
        return importantAccountMasterCount;
    }

    /** 
     * Sets the importantAccountMasterCount.
     * 
     * @param importantAccountMasterCount the importantAccountMasterCount
     */
    public void setImportantAccountMasterCount(BigDecimal importantAccountMasterCount) {
        this.importantAccountMasterCount = importantAccountMasterCount;
    }

    /** 
     * Returns the newOwnCount.
     * 
     * @return the newOwnCount
     */
    public BigDecimal getNewOwnCount() {
        return newOwnCount;
    }

    /** 
     * Sets the newOwnCount.
     * 
     * @param newOwnCount the newOwnCount
     */
    public void setNewOwnCount(BigDecimal newOwnCount) {
        this.newOwnCount = newOwnCount;
    }

    /** 
     * Returns the newDealCount.
     * 
     * @return the newDealCount
     */
    public BigDecimal getNewDealCount() {
        return newDealCount;
    }

    /** 
     * Sets the newDealCount.
     * 
     * @param newDealCount the newDealCount
     */
    public void setNewDealCount(BigDecimal newDealCount) {
        this.newDealCount = newDealCount;
    }

    /** 
     * Returns the newAccountMasterCount.
     * 
     * @return the newAccountMasterCount
     */
    public BigDecimal getNewAccountMasterCount() {
        return newAccountMasterCount;
    }

    /** 
     * Sets the newAccountMasterCount.
     * 
     * @param newAccountMasterCount the newAccountMasterCount
     */
    public void setNewAccountMasterCount(BigDecimal newAccountMasterCount) {
        this.newAccountMasterCount = newAccountMasterCount;
    }

    /** 
     * Returns the deepPlowingOwnCount.
     * 
     * @return the deepPlowingOwnCount
     */
    public BigDecimal getDeepPlowingOwnCount() {
        return deepPlowingOwnCount;
    }

    /** 
     * Sets the deepPlowingOwnCount.
     * 
     * @param deepPlowingOwnCount the deepPlowingOwnCount
     */
    public void setDeepPlowingOwnCount(BigDecimal deepPlowingOwnCount) {
        this.deepPlowingOwnCount = deepPlowingOwnCount;
    }

    /** 
     * Returns the deepPlowingDealCount.
     * 
     * @return the deepPlowingDealCount
     */
    public BigDecimal getDeepPlowingDealCount() {
        return deepPlowingDealCount;
    }

    /** 
     * Sets the deepPlowingDealCount.
     * 
     * @param deepPlowingDealCount the deepPlowingDealCount
     */
    public void setDeepPlowingDealCount(BigDecimal deepPlowingDealCount) {
        this.deepPlowingDealCount = deepPlowingDealCount;
    }

    /** 
     * Returns the deepPlowingAccountMasterCount.
     * 
     * @return the deepPlowingAccountMasterCount
     */
    public BigDecimal getDeepPlowingAccountMasterCount() {
        return deepPlowingAccountMasterCount;
    }

    /** 
     * Sets the deepPlowingAccountMasterCount.
     * 
     * @param deepPlowingAccountMasterCount the deepPlowingAccountMasterCount
     */
    public void setDeepPlowingAccountMasterCount(BigDecimal deepPlowingAccountMasterCount) {
        this.deepPlowingAccountMasterCount = deepPlowingAccountMasterCount;
    }

    /** 
     * Returns the yCpOwnCount.
     * 
     * @return the yCpOwnCount
     */
    public BigDecimal getYCpOwnCount() {
        return yCpOwnCount;
    }

    /** 
     * Sets the yCpOwnCount.
     * 
     * @param yCpOwnCount the yCpOwnCount
     */
    public void setYCpOwnCount(BigDecimal yCpOwnCount) {
        this.yCpOwnCount = yCpOwnCount;
    }

    /** 
     * Returns the yCpDealCount.
     * 
     * @return the yCpDealCount
     */
    public BigDecimal getYCpDealCount() {
        return yCpDealCount;
    }

    /** 
     * Sets the yCpDealCount.
     * 
     * @param yCpDealCount the yCpDealCount
     */
    public void setYCpDealCount(BigDecimal yCpDealCount) {
        this.yCpDealCount = yCpDealCount;
    }

    /** 
     * Returns the yCpAccountMasterCount.
     * 
     * @return the yCpAccountMasterCount
     */
    public BigDecimal getYCpAccountMasterCount() {
        return yCpAccountMasterCount;
    }

    /** 
     * Sets the yCpAccountMasterCount.
     * 
     * @param yCpAccountMasterCount the yCpAccountMasterCount
     */
    public void setYCpAccountMasterCount(BigDecimal yCpAccountMasterCount) {
        this.yCpAccountMasterCount = yCpAccountMasterCount;
    }

    /** 
     * Returns the otherCpOwnCount.
     * 
     * @return the otherCpOwnCount
     */
    public BigDecimal getOtherCpOwnCount() {
        return otherCpOwnCount;
    }

    /** 
     * Sets the otherCpOwnCount.
     * 
     * @param otherCpOwnCount the otherCpOwnCount
     */
    public void setOtherCpOwnCount(BigDecimal otherCpOwnCount) {
        this.otherCpOwnCount = otherCpOwnCount;
    }

    /** 
     * Returns the otherCpDealCount.
     * 
     * @return the otherCpDealCount
     */
    public BigDecimal getOtherCpDealCount() {
        return otherCpDealCount;
    }

    /** 
     * Sets the otherCpDealCount.
     * 
     * @param otherCpDealCount the otherCpDealCount
     */
    public void setOtherCpDealCount(BigDecimal otherCpDealCount) {
        this.otherCpDealCount = otherCpDealCount;
    }

    /** 
     * Returns the otherCpAccountMasterCount.
     * 
     * @return the otherCpAccountMasterCount
     */
    public BigDecimal getOtherCpAccountMasterCount() {
        return otherCpAccountMasterCount;
    }

    /** 
     * Sets the otherCpAccountMasterCount.
     * 
     * @param otherCpAccountMasterCount the otherCpAccountMasterCount
     */
    public void setOtherCpAccountMasterCount(BigDecimal otherCpAccountMasterCount) {
        this.otherCpAccountMasterCount = otherCpAccountMasterCount;
    }

    /** 
     * Returns the groupOneOwnCount.
     * 
     * @return the groupOneOwnCount
     */
    public BigDecimal getGroupOneOwnCount() {
        return groupOneOwnCount;
    }

    /** 
     * Sets the groupOneOwnCount.
     * 
     * @param groupOneOwnCount the groupOneOwnCount
     */
    public void setGroupOneOwnCount(BigDecimal groupOneOwnCount) {
        this.groupOneOwnCount = groupOneOwnCount;
    }

    /** 
     * Returns the groupOneDealCount.
     * 
     * @return the groupOneDealCount
     */
    public BigDecimal getGroupOneDealCount() {
        return groupOneDealCount;
    }

    /** 
     * Sets the groupOneDealCount.
     * 
     * @param groupOneDealCount the groupOneDealCount
     */
    public void setGroupOneDealCount(BigDecimal groupOneDealCount) {
        this.groupOneDealCount = groupOneDealCount;
    }

    /** 
     * Returns the groupOneAccountMasterCount.
     * 
     * @return the groupOneAccountMasterCount
     */
    public BigDecimal getGroupOneAccountMasterCount() {
        return groupOneAccountMasterCount;
    }

    /** 
     * Sets the groupOneAccountMasterCount.
     * 
     * @param groupOneAccountMasterCount the groupOneAccountMasterCount
     */
    public void setGroupOneAccountMasterCount(BigDecimal groupOneAccountMasterCount) {
        this.groupOneAccountMasterCount = groupOneAccountMasterCount;
    }

    /** 
     * Returns the groupTwoOwnCount.
     * 
     * @return the groupTwoOwnCount
     */
    public BigDecimal getGroupTwoOwnCount() {
        return groupTwoOwnCount;
    }

    /** 
     * Sets the groupTwoOwnCount.
     * 
     * @param groupTwoOwnCount the groupTwoOwnCount
     */
    public void setGroupTwoOwnCount(BigDecimal groupTwoOwnCount) {
        this.groupTwoOwnCount = groupTwoOwnCount;
    }

    /** 
     * Returns the groupTwoDealCount.
     * 
     * @return the groupTwoDealCount
     */
    public BigDecimal getGroupTwoDealCount() {
        return groupTwoDealCount;
    }

    /** 
     * Sets the groupTwoDealCount.
     * 
     * @param groupTwoDealCount the groupTwoDealCount
     */
    public void setGroupTwoDealCount(BigDecimal groupTwoDealCount) {
        this.groupTwoDealCount = groupTwoDealCount;
    }

    /** 
     * Returns the groupTwoAccountMasterCount.
     * 
     * @return the groupTwoAccountMasterCount
     */
    public BigDecimal getGroupTwoAccountMasterCount() {
        return groupTwoAccountMasterCount;
    }

    /** 
     * Sets the groupTwoAccountMasterCount.
     * 
     * @param groupTwoAccountMasterCount the groupTwoAccountMasterCount
     */
    public void setGroupTwoAccountMasterCount(BigDecimal groupTwoAccountMasterCount) {
        this.groupTwoAccountMasterCount = groupTwoAccountMasterCount;
    }

    /** 
     * Returns the groupThreeOwnCount.
     * 
     * @return the groupThreeOwnCount
     */
    public BigDecimal getGroupThreeOwnCount() {
        return groupThreeOwnCount;
    }

    /** 
     * Sets the groupThreeOwnCount.
     * 
     * @param groupThreeOwnCount the groupThreeOwnCount
     */
    public void setGroupThreeOwnCount(BigDecimal groupThreeOwnCount) {
        this.groupThreeOwnCount = groupThreeOwnCount;
    }

    /** 
     * Returns the groupThreeDealCount.
     * 
     * @return the groupThreeDealCount
     */
    public BigDecimal getGroupThreeDealCount() {
        return groupThreeDealCount;
    }

    /** 
     * Sets the groupThreeDealCount.
     * 
     * @param groupThreeDealCount the groupThreeDealCount
     */
    public void setGroupThreeDealCount(BigDecimal groupThreeDealCount) {
        this.groupThreeDealCount = groupThreeDealCount;
    }

    /** 
     * Returns the groupThreeAccountMasterCount.
     * 
     * @return the groupThreeAccountMasterCount
     */
    public BigDecimal getGroupThreeAccountMasterCount() {
        return groupThreeAccountMasterCount;
    }

    /** 
     * Sets the groupThreeAccountMasterCount.
     * 
     * @param groupThreeAccountMasterCount the groupThreeAccountMasterCount
     */
    public void setGroupThreeAccountMasterCount(BigDecimal groupThreeAccountMasterCount) {
        this.groupThreeAccountMasterCount = groupThreeAccountMasterCount;
    }

    /** 
     * Returns the groupFourOwnCount.
     * 
     * @return the groupFourOwnCount
     */
    public BigDecimal getGroupFourOwnCount() {
        return groupFourOwnCount;
    }

    /** 
     * Sets the groupFourOwnCount.
     * 
     * @param groupFourOwnCount the groupFourOwnCount
     */
    public void setGroupFourOwnCount(BigDecimal groupFourOwnCount) {
        this.groupFourOwnCount = groupFourOwnCount;
    }

    /** 
     * Returns the groupFourDealCount.
     * 
     * @return the groupFourDealCount
     */
    public BigDecimal getGroupFourDealCount() {
        return groupFourDealCount;
    }

    /** 
     * Sets the groupFourDealCount.
     * 
     * @param groupFourDealCount the groupFourDealCount
     */
    public void setGroupFourDealCount(BigDecimal groupFourDealCount) {
        this.groupFourDealCount = groupFourDealCount;
    }

    /** 
     * Returns the groupFourAccountMasterCount.
     * 
     * @return the groupFourAccountMasterCount
     */
    public BigDecimal getGroupFourAccountMasterCount() {
        return groupFourAccountMasterCount;
    }

    /** 
     * Sets the groupFourAccountMasterCount.
     * 
     * @param groupFourAccountMasterCount the groupFourAccountMasterCount
     */
    public void setGroupFourAccountMasterCount(BigDecimal groupFourAccountMasterCount) {
        this.groupFourAccountMasterCount = groupFourAccountMasterCount;
    }

    /** 
     * Returns the groupFiveOwnCount.
     * 
     * @return the groupFiveOwnCount
     */
    public BigDecimal getGroupFiveOwnCount() {
        return groupFiveOwnCount;
    }

    /** 
     * Sets the groupFiveOwnCount.
     * 
     * @param groupFiveOwnCount the groupFiveOwnCount
     */
    public void setGroupFiveOwnCount(BigDecimal groupFiveOwnCount) {
        this.groupFiveOwnCount = groupFiveOwnCount;
    }

    /** 
     * Returns the groupFiveDealCount.
     * 
     * @return the groupFiveDealCount
     */
    public BigDecimal getGroupFiveDealCount() {
        return groupFiveDealCount;
    }

    /** 
     * Sets the groupFiveDealCount.
     * 
     * @param groupFiveDealCount the groupFiveDealCount
     */
    public void setGroupFiveDealCount(BigDecimal groupFiveDealCount) {
        this.groupFiveDealCount = groupFiveDealCount;
    }

    /** 
     * Returns the groupFiveAccountMasterCount.
     * 
     * @return the groupFiveAccountMasterCount
     */
    public BigDecimal getGroupFiveAccountMasterCount() {
        return groupFiveAccountMasterCount;
    }

    /** 
     * Sets the groupFiveAccountMasterCount.
     * 
     * @param groupFiveAccountMasterCount the groupFiveAccountMasterCount
     */
    public void setGroupFiveAccountMasterCount(BigDecimal groupFiveAccountMasterCount) {
        this.groupFiveAccountMasterCount = groupFiveAccountMasterCount;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}