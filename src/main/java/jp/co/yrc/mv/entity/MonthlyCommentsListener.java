package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class MonthlyCommentsListener implements EntityListener<MonthlyComments> {

    @Override
    public void preInsert(MonthlyComments entity, PreInsertContext<MonthlyComments> context) {
    }

    @Override
    public void preUpdate(MonthlyComments entity, PreUpdateContext<MonthlyComments> context) {
    }

    @Override
    public void preDelete(MonthlyComments entity, PreDeleteContext<MonthlyComments> context) {
    }

    @Override
    public void postInsert(MonthlyComments entity, PostInsertContext<MonthlyComments> context) {
    }

    @Override
    public void postUpdate(MonthlyComments entity, PostUpdateContext<MonthlyComments> context) {
    }

    @Override
    public void postDelete(MonthlyComments entity, PostDeleteContext<MonthlyComments> context) {
    }
}