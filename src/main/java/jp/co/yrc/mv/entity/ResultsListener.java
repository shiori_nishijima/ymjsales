package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class ResultsListener implements EntityListener<Results> {

    @Override
    public void preInsert(Results entity, PreInsertContext<Results> context) {
    }

    @Override
    public void preUpdate(Results entity, PreUpdateContext<Results> context) {
    }

    @Override
    public void preDelete(Results entity, PreDeleteContext<Results> context) {
    }

    @Override
    public void postInsert(Results entity, PostInsertContext<Results> context) {
    }

    @Override
    public void postUpdate(Results entity, PostUpdateContext<Results> context) {
    }

    @Override
    public void postDelete(Results entity, PostDeleteContext<Results> context) {
    }
}