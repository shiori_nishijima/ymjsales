package jp.co.yrc.mv.entity;

import org.seasar.doma.jdbc.entity.EntityListener;
import org.seasar.doma.jdbc.entity.PostDeleteContext;
import org.seasar.doma.jdbc.entity.PostInsertContext;
import org.seasar.doma.jdbc.entity.PostUpdateContext;
import org.seasar.doma.jdbc.entity.PreDeleteContext;
import org.seasar.doma.jdbc.entity.PreInsertContext;
import org.seasar.doma.jdbc.entity.PreUpdateContext;

/**
 * 
 */
public class TechServiceCallsListener implements EntityListener<TechServiceCalls> {

    @Override
    public void preInsert(TechServiceCalls entity, PreInsertContext<TechServiceCalls> context) {
    }

    @Override
    public void preUpdate(TechServiceCalls entity, PreUpdateContext<TechServiceCalls> context) {
    }

    @Override
    public void preDelete(TechServiceCalls entity, PreDeleteContext<TechServiceCalls> context) {
    }

    @Override
    public void postInsert(TechServiceCalls entity, PostInsertContext<TechServiceCalls> context) {
    }

    @Override
    public void postUpdate(TechServiceCalls entity, PostUpdateContext<TechServiceCalls> context) {
    }

    @Override
    public void postDelete(TechServiceCalls entity, PostDeleteContext<TechServiceCalls> context) {
    }
}