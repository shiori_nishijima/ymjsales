package jp.co.yrc.mv.entity;

import java.sql.Date;
import java.sql.Timestamp;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = OppSalessupListener.class)
@Table(name = "opp_salessup")
public class OppSalessup {

    /** 営業サポートCD */
    @Id
    @Column(name = "id")
    String id;

    /** 名称(未使用) */
    @Column(name = "name")
    String name;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新者 */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 詳細 */
    @Column(name = "description")
    String description;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 営業担当者ID */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 役割 */
    @Column(name = "role_proposed")
    String roleProposed;

    /**  */
    @Column(name = "ps_sales")
    Double psSales;

    /**  */
    @Column(name = "calling_agent")
    String callingAgent;

    /**  */
    @Column(name = "req_opp_class_cd")
    String reqOppClassCd;

    /**  */
    @Column(name = "comp_date_from")
    Date compDateFrom;

    /**  */
    @Column(name = "comp_date_to")
    Date compDateTo;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /** 
     * Returns the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /** 
     * Sets the description.
     * 
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the roleProposed.
     * 
     * @return the roleProposed
     */
    public String getRoleProposed() {
        return roleProposed;
    }

    /** 
     * Sets the roleProposed.
     * 
     * @param roleProposed the roleProposed
     */
    public void setRoleProposed(String roleProposed) {
        this.roleProposed = roleProposed;
    }

    /** 
     * Returns the psSales.
     * 
     * @return the psSales
     */
    public Double getPsSales() {
        return psSales;
    }

    /** 
     * Sets the psSales.
     * 
     * @param psSales the psSales
     */
    public void setPsSales(Double psSales) {
        this.psSales = psSales;
    }

    /** 
     * Returns the callingAgent.
     * 
     * @return the callingAgent
     */
    public String getCallingAgent() {
        return callingAgent;
    }

    /** 
     * Sets the callingAgent.
     * 
     * @param callingAgent the callingAgent
     */
    public void setCallingAgent(String callingAgent) {
        this.callingAgent = callingAgent;
    }

    /** 
     * Returns the reqOppClassCd.
     * 
     * @return the reqOppClassCd
     */
    public String getReqOppClassCd() {
        return reqOppClassCd;
    }

    /** 
     * Sets the reqOppClassCd.
     * 
     * @param reqOppClassCd the reqOppClassCd
     */
    public void setReqOppClassCd(String reqOppClassCd) {
        this.reqOppClassCd = reqOppClassCd;
    }

    /** 
     * Returns the compDateFrom.
     * 
     * @return the compDateFrom
     */
    public Date getCompDateFrom() {
        return compDateFrom;
    }

    /** 
     * Sets the compDateFrom.
     * 
     * @param compDateFrom the compDateFrom
     */
    public void setCompDateFrom(Date compDateFrom) {
        this.compDateFrom = compDateFrom;
    }

    /** 
     * Returns the compDateTo.
     * 
     * @return the compDateTo
     */
    public Date getCompDateTo() {
        return compDateTo;
    }

    /** 
     * Sets the compDateTo.
     * 
     * @param compDateTo the compDateTo
     */
    public void setCompDateTo(Date compDateTo) {
        this.compDateTo = compDateTo;
    }
}