package jp.co.yrc.mv.entity;

import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = DailyCommentsListener.class)
@Table(name = "daily_comments")
public class DailyComments {

	/** ID */
	@Id
	@Column(name = "id")
	String id;

	/** 年 */
	@Column(name = "year")
	Integer year;

	/** 月 */
	@Column(name = "month")
	Integer month;

	/** 日 */
	@Column(name = "date")
	Integer date;

	/** ユーザID */
	@Column(name = "user_id")
	String userId;

	/** 担当者コメント */
	@Column(name = "user_comment")
	String userComment;

	/** 上司コメント */
	@Column(name = "boss_comment")
	String bossComment;

	/** 完了 */
	@Column(name = "completed")
	boolean completed;

	/** 入力日 */
	@Column(name = "date_entered")
	Timestamp dateEntered;

	/** 更新日 */
	@Column(name = "date_modified")
	Timestamp dateModified;

	/** 更新ユーザID */
	@Column(name = "modified_user_id")
	String modifiedUserId;

	/** 作成者 */
	@Column(name = "created_by")
	String createdBy;

	/** 削除済み */
	@Column(name = "deleted")
	boolean deleted;

	/**
	 * Returns the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the year.
	 * 
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 * 
	 * @param year
	 *            the year
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * Returns the month.
	 * 
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 * 
	 * @param month
	 *            the month
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * Returns the date.
	 * 
	 * @return the date
	 */
	public Integer getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            the date
	 */
	public void setDate(Integer date) {
		this.date = date;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            the userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Returns the userComment.
	 * 
	 * @return the userComment
	 */
	public String getUserComment() {
		return userComment;
	}

	/**
	 * Sets the userComment.
	 * 
	 * @param userComment
	 *            the userComment
	 */
	public void setUserComment(String userComment) {
		this.userComment = userComment;
	}

	/**
	 * Returns the bossComment.
	 * 
	 * @return the bossComment
	 */
	public String getBossComment() {
		return bossComment;
	}

	/**
	 * Sets the bossComment.
	 * 
	 * @param bossComment
	 *            the bossComment
	 */
	public void setBossComment(String bossComment) {
		this.bossComment = bossComment;
	}

	/**
	 * Returns the completed.
	 * 
	 * @return the completed
	 */
	public boolean isCompleted() {
		return completed;
	}

	/**
	 * Sets the completed.
	 * 
	 * @param completed
	 *            the completed
	 */
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	/**
	 * Returns the dateEntered.
	 * 
	 * @return the dateEntered
	 */
	public Timestamp getDateEntered() {
		return dateEntered;
	}

	/**
	 * Sets the dateEntered.
	 * 
	 * @param dateEntered
	 *            the dateEntered
	 */
	public void setDateEntered(Timestamp dateEntered) {
		this.dateEntered = dateEntered;
	}

	/**
	 * Returns the dateModified.
	 * 
	 * @return the dateModified
	 */
	public Timestamp getDateModified() {
		return dateModified;
	}

	/**
	 * Sets the dateModified.
	 * 
	 * @param dateModified
	 *            the dateModified
	 */
	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * Returns the modifiedUserId.
	 * 
	 * @return the modifiedUserId
	 */
	public String getModifiedUserId() {
		return modifiedUserId;
	}

	/**
	 * Sets the modifiedUserId.
	 * 
	 * @param modifiedUserId
	 *            the modifiedUserId
	 */
	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}

	/**
	 * Returns the createdBy.
	 * 
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy.
	 * 
	 * @param createdBy
	 *            the createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Returns the deleted.
	 * 
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Sets the deleted.
	 * 
	 * @param deleted
	 *            the deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}