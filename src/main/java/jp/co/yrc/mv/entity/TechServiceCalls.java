package jp.co.yrc.mv.entity;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.Table;

/**
 * 
 */
@Entity(listener = TechServiceCallsListener.class)
@Table(name = "tech_service_calls")
public class TechServiceCalls {

    /** ID */
    @Id
    @Column(name = "id")
    String id;

    /** 訪問ID */
    @Column(name = "call_id")
    String callId;

    /** 訪問件名 */
    @Column(name = "name")
    String name;

    /** 担当者 */
    @Column(name = "assigned_user_id")
    String assignedUserId;

    /** 訪問日時(from) */
    @Column(name = "date_start")
    Timestamp dateStart;

    /** 終了日 */
    @Column(name = "date_end")
    Date dateEnd;

    /** 関連先Type */
    @Column(name = "parent_type")
    String parentType;

    /** ステータス */
    @Column(name = "status")
    String status;

    /** 関連先ID */
    @Column(name = "parent_id")
    String parentId;

    /** 情報区分 */
    @Column(name = "info_div")
    String infoDiv;

    /** メーカー */
    @Column(name = "maker")
    String maker;

    /** 品種 */
    @Column(name = "kind")
    String kind;

    /** カテゴリ */
    @Column(name = "category")
    String category;

    /** 感度 */
    @Column(name = "sensitivity")
    String sensitivity;

    /** 訪問時（To） */
    @Column(name = "time_end_c")
    Time timeEndC;

    /** 内容 */
    @Column(name = "com_free_c")
    String comFreeC;

    /** 訪問理由 */
    @Column(name = "division_c")
    String divisionC;

    /** 予定としてカウント */
    @Column(name = "is_plan")
    boolean plan;

    /** 予定数 */
    @Column(name = "planned_count")
    Integer plannedCount;

    /** 実績数 */
    @Column(name = "held_count")
    Integer heldCount;

    /** 削除済み */
    @Column(name = "deleted")
    boolean deleted;

    /** 入力日 */
    @Column(name = "date_entered")
    Timestamp dateEntered;

    /** 更新日 */
    @Column(name = "date_modified")
    Timestamp dateModified;

    /** 更新ユーザID */
    @Column(name = "modified_user_id")
    String modifiedUserId;

    /** 作成者 */
    @Column(name = "created_by")
    String createdBy;

    /** 
     * Returns the id.
     * 
     * @return the id
     */
    public String getId() {
        return id;
    }

    /** 
     * Sets the id.
     * 
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /** 
     * Returns the callId.
     * 
     * @return the callId
     */
    public String getCallId() {
        return callId;
    }

    /** 
     * Sets the callId.
     * 
     * @param callId the callId
     */
    public void setCallId(String callId) {
        this.callId = callId;
    }

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the assignedUserId.
     * 
     * @return the assignedUserId
     */
    public String getAssignedUserId() {
        return assignedUserId;
    }

    /** 
     * Sets the assignedUserId.
     * 
     * @param assignedUserId the assignedUserId
     */
    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }

    /** 
     * Returns the dateStart.
     * 
     * @return the dateStart
     */
    public Timestamp getDateStart() {
        return dateStart;
    }

    /** 
     * Sets the dateStart.
     * 
     * @param dateStart the dateStart
     */
    public void setDateStart(Timestamp dateStart) {
        this.dateStart = dateStart;
    }

    /** 
     * Returns the dateEnd.
     * 
     * @return the dateEnd
     */
    public Date getDateEnd() {
        return dateEnd;
    }

    /** 
     * Sets the dateEnd.
     * 
     * @param dateEnd the dateEnd
     */
    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    /** 
     * Returns the parentType.
     * 
     * @return the parentType
     */
    public String getParentType() {
        return parentType;
    }

    /** 
     * Sets the parentType.
     * 
     * @param parentType the parentType
     */
    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    /** 
     * Returns the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /** 
     * Sets the status.
     * 
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /** 
     * Returns the parentId.
     * 
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /** 
     * Sets the parentId.
     * 
     * @param parentId the parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /** 
     * Returns the infoDiv.
     * 
     * @return the infoDiv
     */
    public String getInfoDiv() {
        return infoDiv;
    }

    /** 
     * Sets the infoDiv.
     * 
     * @param infoDiv the infoDiv
     */
    public void setInfoDiv(String infoDiv) {
        this.infoDiv = infoDiv;
    }

    /** 
     * Returns the maker.
     * 
     * @return the maker
     */
    public String getMaker() {
        return maker;
    }

    /** 
     * Sets the maker.
     * 
     * @param maker the maker
     */
    public void setMaker(String maker) {
        this.maker = maker;
    }

    /** 
     * Returns the kind.
     * 
     * @return the kind
     */
    public String getKind() {
        return kind;
    }

    /** 
     * Sets the kind.
     * 
     * @param kind the kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /** 
     * Returns the category.
     * 
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /** 
     * Sets the category.
     * 
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /** 
     * Returns the sensitivity.
     * 
     * @return the sensitivity
     */
    public String getSensitivity() {
        return sensitivity;
    }

    /** 
     * Sets the sensitivity.
     * 
     * @param sensitivity the sensitivity
     */
    public void setSensitivity(String sensitivity) {
        this.sensitivity = sensitivity;
    }

    /** 
     * Returns the timeEndC.
     * 
     * @return the timeEndC
     */
    public Time getTimeEndC() {
        return timeEndC;
    }

    /** 
     * Sets the timeEndC.
     * 
     * @param timeEndC the timeEndC
     */
    public void setTimeEndC(Time timeEndC) {
        this.timeEndC = timeEndC;
    }

    /** 
     * Returns the comFreeC.
     * 
     * @return the comFreeC
     */
    public String getComFreeC() {
        return comFreeC;
    }

    /** 
     * Sets the comFreeC.
     * 
     * @param comFreeC the comFreeC
     */
    public void setComFreeC(String comFreeC) {
        this.comFreeC = comFreeC;
    }

    /** 
     * Returns the divisionC.
     * 
     * @return the divisionC
     */
    public String getDivisionC() {
        return divisionC;
    }

    /** 
     * Sets the divisionC.
     * 
     * @param divisionC the divisionC
     */
    public void setDivisionC(String divisionC) {
        this.divisionC = divisionC;
    }

    /** 
     * Returns the plan.
     * 
     * @return the plan
     */
    public boolean isPlan() {
        return plan;
    }

    /** 
     * Sets the plan.
     * 
     * @param plan the plan
     */
    public void setPlan(boolean plan) {
        this.plan = plan;
    }

    /** 
     * Returns the plannedCount.
     * 
     * @return the plannedCount
     */
    public Integer getPlannedCount() {
        return plannedCount;
    }

    /** 
     * Sets the plannedCount.
     * 
     * @param plannedCount the plannedCount
     */
    public void setPlannedCount(Integer plannedCount) {
        this.plannedCount = plannedCount;
    }

    /** 
     * Returns the heldCount.
     * 
     * @return the heldCount
     */
    public Integer getHeldCount() {
        return heldCount;
    }

    /** 
     * Sets the heldCount.
     * 
     * @param heldCount the heldCount
     */
    public void setHeldCount(Integer heldCount) {
        this.heldCount = heldCount;
    }

    /** 
     * Returns the deleted.
     * 
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /** 
     * Sets the deleted.
     * 
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /** 
     * Returns the dateEntered.
     * 
     * @return the dateEntered
     */
    public Timestamp getDateEntered() {
        return dateEntered;
    }

    /** 
     * Sets the dateEntered.
     * 
     * @param dateEntered the dateEntered
     */
    public void setDateEntered(Timestamp dateEntered) {
        this.dateEntered = dateEntered;
    }

    /** 
     * Returns the dateModified.
     * 
     * @return the dateModified
     */
    public Timestamp getDateModified() {
        return dateModified;
    }

    /** 
     * Sets the dateModified.
     * 
     * @param dateModified the dateModified
     */
    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    /** 
     * Returns the modifiedUserId.
     * 
     * @return the modifiedUserId
     */
    public String getModifiedUserId() {
        return modifiedUserId;
    }

    /** 
     * Sets the modifiedUserId.
     * 
     * @param modifiedUserId the modifiedUserId
     */
    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    /** 
     * Returns the createdBy.
     * 
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /** 
     * Sets the createdBy.
     * 
     * @param createdBy the createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}