package jp.co.yrc.mv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.CallsAccountsDao;
import jp.co.yrc.mv.dto.CallsAccountsDto;

@Service
public class CallsAccountsService {
	@Autowired
	CallsAccountsDao callsAccountsDao;

	/**
	 * 検索します。
	 * 担当得意先である情報のみ検索します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者コード
	 * @param idForCustomer 得意先用ユーザID
	 * @param group グルーピング
	 * @return DTOリスト
	 */
	public List<CallsAccountsDto> findTantoCallsAccountList(int year, int month, List<String> div1, String div2, String div3, 
			String tanto, String idForCustomer, String group) {
		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		int yearMonth = DateUtils.toYearMonth(year, month);
		return callsAccountsDao.findTantoCallsAccountList(year, month, yearMonth, div1, div2, div3, tanto, idForCustomer, group, isHistory);
	}

	/**
	 * 検索します。
	 * 営業組織条件関係なく、担当者に紐づく全てを検索します。
	 * 
	 * 過去検索でも得意先履歴を利用しないのでグルーピングや営業組織など
	 * 変更のありうる項目を取得する場合には利用できません。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param tanto 担当者コード
	 * @return DTOリスト
	 */
	public List<CallsAccountsDto> find(int year, int month, String tanto) {
		return callsAccountsDao.find(year, month, tanto);
	}
}
