package jp.co.yrc.mv.service;

import java.util.List;

import jp.co.yrc.mv.dao.MarketsDao;
import jp.co.yrc.mv.entity.Markets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MarketsService {

	@Autowired
	MarketsDao marketsDao;

	public List<Markets> listAll() {
		return marketsDao.listAll();
	}
}
