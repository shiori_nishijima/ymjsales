package jp.co.yrc.mv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.dao.OpportunitiesDao;
import jp.co.yrc.mv.dto.OpportunitiesDto;
import jp.co.yrc.mv.dto.OpportunitiesSearchConditionDto;

@Service
public class OpportunitiesService {
	
	@Autowired
	OpportunitiesDao opportunitiesDao;
	
	public List<OpportunitiesDto> find(OpportunitiesSearchConditionDto condition) {
		List<OpportunitiesDto> list = opportunitiesDao.findOpportunityList(condition);
		return list;
	}
}
