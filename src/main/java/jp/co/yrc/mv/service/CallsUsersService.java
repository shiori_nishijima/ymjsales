package jp.co.yrc.mv.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.CallsUsersDao;
import jp.co.yrc.mv.dto.CallsUsersDto;
import jp.co.yrc.mv.dto.CallsUsersWithGroupInfoDto;

@Service
public class CallsUsersService {
	@Autowired
	CallsUsersDao callsUsersDao;

	public List<CallsUsersDto> findByCallId(String callId) {
		return callsUsersDao.findByCallId(callId, false);
	}

	public List<CallsUsersDto> findByCallId4Tech(String callId) {
		return callsUsersDao.findByCallId(callId, true);
	}

	/**
	 * DBの社内同行者ユーザリストを名前付きカンマ区切り文字列にします。
	 * 
	 * @param companionIds
	 * @return
	 */
	public List<String> convertCompanionListToNames(List<CallsUsersDto> list) {
		List<String> result = new ArrayList<String>();
		for (CallsUsersDto users : list) {

			StringBuilder builder = new StringBuilder();
			builder.append(users.getLastName());
			builder.append(users.getFirstName());
			builder.append("[");
			builder.append(users.getUserId());
			builder.append("]");
			result.add(builder.toString());

		}
		return result;
	}

	public List<CallsUsersWithGroupInfoDto> countWithGroupInfo(int year, int month, int yearMonth, String div1,
			String div2, String div3, String userId) {
		boolean isHistory = !DateUtils.isCurrentYearMonth(year, month);

		Calendar c = DateUtils.getCalendar();
		c.set(year, month - 1, 1, 0, 0, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date from = c.getTime();

		c = DateUtils.getYearMonthCalendar(c, 1);
		Date to = c.getTime();

		return callsUsersDao.countWithGroupInfo(year, month, yearMonth, from, to, div1, div2, div3, userId, isHistory);
	}
}