package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.List;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.AccountsDao;
import jp.co.yrc.mv.dao.CallsDao;
import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.DemandDto;
import jp.co.yrc.mv.dto.GroupDealDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

	@Autowired
	AccountsDao accountsDao;

	@Autowired
	CallsDao callsDao;

	@Autowired
	SelectionItemsService selectionItemsService;

	public AccountsDto findAccountInfoById(Integer year, Integer month, String id) {

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return accountsDao.findAccountInfoById(yearMonth, id, yearMonth < nowYearMonth);

	}

	/**
	 * 推定需要を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param idForCustomer
	 * @param isHistory
	 * @return
	 */
	public List<DemandDto> sumGroupDemand(Integer year, Integer month, List<String> div1, String div2, String div3,
			String idForCustomer, boolean isHistory) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return accountsDao.sumGroupDemand(yearMonth, div1, div2, div3, idForCustomer, isHistory);
	}

	/**
	 * 軒数を集計する
	 * 
	 * @param year
	 * @param month
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param idForCustomer
	 * @param isHistory
	 * @return
	 */
	public GroupDealDto sumGroupDeal(Integer year, Integer month, List<String> div1, String div2, String div3,
			String idForCustomer, boolean isHistory) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return accountsDao.sumGroupDeal(year, month, yearMonth, div1, div2, div3, idForCustomer, isHistory);
	}

	/**
	 * 担当得意先をリストする。
	 * 
	 * @param year
	 * @param month
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param userId
	 * @param isHistory
	 * @return
	 */
	public List<AccountsDto> listAssignedAccount(Integer year, Integer month, String div1, String div2, String div3,
			String userId, boolean isHistory) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return accountsDao.listAssignedAccount(year, month, yearMonth, div1, div2, div3, userId, isHistory);
	}

	/**
	 * Callsをベースに担当外の得意先をリストする。
	 * 
	 * @param year
	 * @param month
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param userId
	 * @param isHistory
	 * @return
	 */
	public List<AccountsDto> listOutsideAccount(Integer year, Integer month, String div1, String div2, String div3,
			String userId, boolean isHistory) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return callsDao
				.listOutsideAccount(yearMonth, from.getTime(), to.getTime(), userId, div1, div2, div3, isHistory);
	}

}
