package jp.co.yrc.mv.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.IDUtils;
import jp.co.yrc.mv.dao.DailyCommentsDao;
import jp.co.yrc.mv.dao.DailyCommentsReadUsersDao;
import jp.co.yrc.mv.dao.UsersDao;
import jp.co.yrc.mv.dto.DailyCommentDto;
import jp.co.yrc.mv.entity.DailyComments;
import jp.co.yrc.mv.entity.DailyCommentsReadUsers;
import jp.co.yrc.mv.entity.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DailyCommentsService {

	@Autowired
	DailyCommentsDao dailyCommentsDao;

	@Autowired
	DailyCommentsReadUsersDao dailyCommentsReadUsersDao;

	@Autowired
	UsersDao usersDao;

	public DailyCommentDto getDailyComments(int year, int month, int date, String userId, List<String> corpCodes) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return dailyCommentsDao.selectByCorpCdAndUserId(year, month, date, userId, corpCodes, yearMonth < nowYearMonth);
	}

	public List<DailyCommentDto> listCommentWithCallCount(int year, int month, int date, List<String> div1,
			String paramDiv1, String div2, String div3, String userId, String loginUserId) {

		Calendar fromCalendar = Calendar.getInstance();
		// 日付が0以下なら、月検索
		fromCalendar.set(year, month - 1, date > 0 ? date : 1, 0, 0, 0);
		fromCalendar.set(Calendar.MILLISECOND, 0);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(fromCalendar.getTime());
		if (date > 0) {
			toCalendar.add(Calendar.DATE, 1);
		} else {
			toCalendar.add(Calendar.MONTH, 1);
		}
		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Date from = fromCalendar.getTime();
		Date to = toCalendar.getTime();

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return dailyCommentsDao.listCommentWithCallCount(year, month, date, from, to, div1, div2, div3,
				userId, loginUserId, yearMonth < nowYearMonth);
	}

	public int countCommentWithCallCount(int year, int month, int date, List<String> div1, String paramDiv1,
			String div2, String div3, String userId) {

		Calendar fromCalender = Calendar.getInstance();
		// 日付が0以下なら、月検索
		fromCalender.set(year, month - 1, date > 0 ? date : 1, 0, 0, 0);
		fromCalender.set(Calendar.MILLISECOND, 0);

		Calendar toCalender = Calendar.getInstance();
		toCalender.setTime(fromCalender.getTime());
		if (date > 0) {
			toCalender.add(Calendar.DATE, 1);
		} else {
			toCalender.add(Calendar.MONTH, 1);
		}
		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return dailyCommentsDao.countCommentWithCallCount(year, month, date, from, to, div1, div2, div3,
				userId, yearMonth < nowYearMonth);
	}

	/**
	 * ユーザーテーブルのログイン者のレコードをロックします。 このロックで、SFA側からの登録、更新と衝突を回避します。 保存します
	 * 
	 * @param dailyComment
	 */
	public DailyComments saveBossComment(DailyComments dailyComment) {
		Users users = usersDao.selectByIdForUpdate(dailyComment.getModifiedUserId());
		if (users == null) {
			throw new RuntimeException(dailyComment.getModifiedUserId() + " Not Found.");
		}
		DailyComments dbDailyComment = dailyCommentsDao.selectByIdNotDeleted(dailyComment.getId());
		if (dbDailyComment != null) {
			dbDailyComment.setBossComment(dailyComment.getBossComment());
			dbDailyComment.setModifiedUserId(dailyComment.getModifiedUserId());
			dbDailyComment.setDateModified(DateUtils.getDBTimestamp());
			dailyCommentsDao.updateBossComment(dbDailyComment);
			return dbDailyComment;
		} else {

			dailyComment.setId(IDUtils.generateGuid());
			dailyComment.setDateModified(DateUtils.getDBTimestamp());
			dailyComment.setDateEntered(dailyComment.getDateModified());
			dailyComment.setCompleted(false);
			dailyComment.setDeleted(false);
			dailyCommentsDao.insert(dailyComment);
			return dailyComment;
		}
	}

	/**
	 * 確認済みにする
	 *
	 * @param calls
	 */
	public List<DailyCommentsReadUsers> confirm(Set<String> id, String userId) {

		// 重複排除
		for (Iterator<String> it = id.iterator(); it.hasNext();) {
			if (dailyCommentsReadUsersDao.countByUserIdAndCommentId(userId, it.next()) > 0) {
				it.remove();
			}
		}
		Timestamp date = DateUtils.getDBTimestamp();
		List<DailyCommentsReadUsers> save = new ArrayList<>();

		for (Iterator<String> it = id.iterator(); it.hasNext();) {
			String commentId = it.next();
			DailyCommentsReadUsers o = new DailyCommentsReadUsers();
			o.setId(IDUtils.generateGuid());
			o.setDailyCommentId(commentId);
			o.setDateModified(date);
			o.setDeleted(Constants.Delete.False.toBoolean());
			o.setUserId(userId);
			save.add(o);
		}
		dailyCommentsReadUsersDao.insert(save);

		return save;
	}

}
