package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.UsersDao;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.Users;

@Service
public class UsersService {

	@Autowired
	UsersDao usersDao;

	/**
	 * idで検索します。
	 * 
	 * @return
	 */
	public Users selectUserById(String id) {
		return usersDao.selectById(id);
	}
	
	/**
	 * divisionCのアイテムを取得
	 * 
	 * @return
	 */
	public UserSosikiDto selectUserByDivCd(String id, List<String> corpCodes, String div1, String div3, String div2,
			int year, int month) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return usersDao.selectUserByDivCd(id, corpCodes, div1, div2, div3, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * 
	 * @return
	 */
	public List<UserSosikiDto> listUser(String userId, List<String> div1, String div2, String div3, int year, int month) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return usersDao.listUser(userId, null, div1, div2, div3, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * 
	 * @return
	 */
	public UserSosikiDto selectUserById(String userId, List<String> div1, String div2, String div3, int year, int month) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		List<UserSosikiDto> list = usersDao.listUser(userId, null, div1, div2, div3, year, month,
				yearMonth < nowYearMonth);
		return list.isEmpty() ? null : list.get(0);
	}

	/**
	 * idForCustomerでユーザー検索
	 * 
	 * @return
	 */
	public UserSosikiDto selectUserByIdForCustomer(String idForCustomer, List<String> div1, String div2, String div3, 
			int year, int month) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		List<UserSosikiDto> list = usersDao.listUser(null, idForCustomer, div1, div2, div3, year, month,
				yearMonth < nowYearMonth);
		return list.isEmpty() ? null : list.get(0);
	}

}
