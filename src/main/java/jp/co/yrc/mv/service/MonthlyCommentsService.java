package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.List;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.MonthlyCommentsDao;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.entity.MonthlyComments;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonthlyCommentsService {

	@Autowired
	MonthlyCommentsDao monthlyCommentsDao;

	/**
	 * 年・月・会社コード・ユーザIDを用いて検索します。
	 * @param year 年
	 * @param month 月
	 * @param userId ユーザID
	 * @return DTO
	 */
	public MonthlyCommentsDto getMonthlyComments(int year, int month, String userId, List<String> corpCodes) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return monthlyCommentsDao.selectByCorpCdAndUserId(year, month, userId, corpCodes, yearMonth < nowYearMonth);
	}

	/**
	 * 保存します。
	 * 
	 * @param monthlyComment エンティティ
	 */
	public void save(MonthlyComments monthlyComment) {
		MonthlyComments dbMonthlyComment = monthlyCommentsDao.selectById(monthlyComment.getId());
		if (dbMonthlyComment != null) {
			// MV側で更新しない項目はエンティティ定義（updatable）で更新対象から外している
			dbMonthlyComment.setBossComment1(monthlyComment.getBossComment1());
			dbMonthlyComment.setBossComment2(monthlyComment.getBossComment2());
			dbMonthlyComment.setBossComment3(monthlyComment.getBossComment3());
			dbMonthlyComment.setBossComment4(monthlyComment.getBossComment4());
			dbMonthlyComment.setBossComment5(monthlyComment.getBossComment5());
			dbMonthlyComment.setModifiedUserId(monthlyComment.getModifiedUserId());
			dbMonthlyComment.setDateModified(DateUtils.getDBTimestamp());
			monthlyCommentsDao.update(dbMonthlyComment);
		}
	}
	
	/**
	 * 年・月・ユーザIDを用いて検索します。
	 * @param year 年
	 * @param month 月
	 * @param userId ユーザID
	 * @return DTO
	 */
	public MonthlyCommentsDto selectByCondition(int year, int month, String userId) {
		return monthlyCommentsDao.selectByCondition(year, month, userId);
	}
}
