package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.List;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.MstEigyoSosikiDao;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MstEigyoSosikiService {

	@Autowired
	MstEigyoSosikiDao mstEigyoSosikiDao;

	/**
	 * すべての組織リストを取得します。
	 * 
	 * @param corpCodes 会社コード
	 * @param year 年
	 * @param month 月
	 * @return　DTOリスト
	 */
	public List<EigyoSosikiDto> listSosikiTree(List<String> corpCodes, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return mstEigyoSosikiDao.listSosikiTree(corpCodes, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * ユーザの所属する組織リストを取得します。 ユーザの氏名も同時に取得します。
	 * 
	 * @param corpCodes
	 * @param userId
	 * @param year 年
	 * @param month 月
	 * @return DTOリスト
	 */
	public List<EigyoSosikiDto> listMySosiki(List<String> corpCodes, String userId, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao.listMySosikiTree(corpCodes, userId, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * 組織エンティティを検索します。
	 * 
	 * @param div1 div1コード
	 * @param div2 div2コード
	 * @param div3 div3コード
	 * @param year 年
	 * @param month 月
	 * @return エンティティ
	 */
	public MstEigyoSosiki findIdByDivCd(String div1, String div2, String div3, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao.findByDivCd(div1, div2, div3, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * ユーザの所属する組織リストを取得します。
	 * 
	 * @param corpCd
	 * @param userId
	 * @return エンティティリスト
	 */
	public List<MstEigyoSosiki> findByUserId(List<String> corpCodes, String userId, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao.findByUserId(corpCodes, userId, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * 指定されたコードに紐付く組織をリスト
	 * 
	 * @param corpCodes
	 * @return
	 * @return DTOリスト
	 */
	public List<MstEigyoSosiki> listBelongCompany(List<String> div1, String div2, List<String> corpCodes, int year,
			int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao.listBelongCompany(div1, div2, corpCodes, year, month, yearMonth < nowYearMonth);
	}

	/**
	 * すべての組織リストを取得します。
	 * 
	 * @param corpCodes
	 * @return
	 * @return DTOリスト
	 */
	public List<MstEigyoSosiki> listCompany(List<String> div1, String div2, String div3, String userId,
			List<String> corpCodes, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao
				.listCompany(div1, div2, div3, userId, corpCodes, year, month, yearMonth < nowYearMonth);
	}

	public List<EigyoSosikiDto> listMySosikiTreeFromHistory(List<String> corpCodes, String userId, int year, int month) {
		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		return mstEigyoSosikiDao.listMySosikiTreeFromHistory(corpCodes, userId, year, month, yearMonth < nowYearMonth);
	}
}
