package jp.co.yrc.mv.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.IDUtils;
import jp.co.yrc.mv.dao.MenuFavoritesDao;
import jp.co.yrc.mv.entity.MenuFavorites;

@Service
public class MenuFavoritesService {

	@Autowired
	MenuFavoritesDao menuFavoritesDao;

	public List<MenuFavorites> listByUserId(String userId) {
		return menuFavoritesDao.listByUserId(userId);
	}

	public void updateFavorites(List<String> urls, String userId) {

		MenuFavorites condition = new MenuFavorites();
		condition.setUserId(userId);
		menuFavoritesDao.deleteByUserId(userId);

		List<MenuFavorites> insert = new ArrayList<>();

		Timestamp time = DateUtils.getDBTimestamp();

		for (String s : urls) {
			MenuFavorites m = new MenuFavorites();
			m.setId(IDUtils.generateGuid());
			m.setUrl(s);
			m.setUserId(userId);
			m.setDeleted(false);
			m.setDateModified(time);
			insert.add(m);
		}
		menuFavoritesDao.insert(insert);

	}
}
