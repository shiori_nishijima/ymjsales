package jp.co.yrc.mv.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.IDUtils;
import jp.co.yrc.mv.dao.CallsDao;
import jp.co.yrc.mv.dao.CallsFilesDao;
import jp.co.yrc.mv.dao.CallsLikeUsersDao;
import jp.co.yrc.mv.dao.CallsReadUsersDao;
import jp.co.yrc.mv.dao.CallsUsersDao;
import jp.co.yrc.mv.dao.TechServiceCallsLikeUsersDao;
import jp.co.yrc.mv.dao.TechServiceCallsReadUsersDao;
import jp.co.yrc.mv.dao.UsersDao;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.CallsSearchConditionDto;
import jp.co.yrc.mv.dto.MinMaxDateDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerAccountDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerSalesDto;
import jp.co.yrc.mv.dto.UnvisitAccountDto;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.dto.WeekAccountCallDto;
import jp.co.yrc.mv.entity.Calls;
import jp.co.yrc.mv.entity.CallsFiles;
import jp.co.yrc.mv.entity.CallsLikeUsers;
import jp.co.yrc.mv.entity.CallsReadUsers;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.entity.TechServiceCalls;
import jp.co.yrc.mv.entity.TechServiceCallsLikeUsers;
import jp.co.yrc.mv.entity.TechServiceCallsReadUsers;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CallsService {
	@Autowired
	CallsDao callsDao;

	@Autowired
	CallsUsersDao callsUsersDao;

	@Autowired
	CallsLikeUsersDao callsLikeUsersDao;

	@Autowired
	TechServiceCallsLikeUsersDao techServiceCallsLikeUsersDao;

	@Autowired
	CallsReadUsersDao callsReadUsersDao;

	@Autowired
	TechServiceCallsReadUsersDao techServiceCallsReadUsersDao;

	@Autowired
	UsersDao usersDao;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	CallsFilesDao callsFilesDao;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	/**
	 * 指定した年月の訪問リストを取得します。
	 * 担当外訪問は省いて検索します。
	 * 訪問理由・社内同行者が複数存在する場合はカンマ区切りで返却します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param yearMonth 年月
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者コード
	 * @param idForCustomer 得意先用ユーザID
	 * @param group グルーピング
	 * @return DTOリスト
	 */
	public List<CallsDto> findMonthlyCalls(int year, int month, List<String> div1, String div2, String div3, 
			String tanto, String idForCustomer, String group) {

		// 指定月のFrom、Toを作成
		Calendar fromCalender = DateUtils.getYearMonthCalendar(year, month);
		Calendar toCalender = DateUtils.getYearMonthCalendar(fromCalender, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();
		boolean isHistory = DateUtils.isPastYearMonth(year, month);

		List<CallsDto> dtoList = callsDao.findMonthlyCalls(yearMonth, from, to, 
				div1, div2, div3, tanto, idForCustomer, group, isHistory);
		List<SelectionItems> divisionCList = selectionItemsService.listDivisionC();
		for (CallsDto dto : dtoList) {
			setCompanionNames(dto);
			setDivisionCLabel(divisionCList, dto);
		}
		return dtoList;
	}

	/**
	 * 社内同行者の重複を排除して設定します。
	 * 
	 * @param dto 訪問DTO
	 */
	protected void setCompanionNames(CallsDto dto) {
		if (StringUtils.isNotEmpty(dto.getCompanionNames())) {
			Set<String> set = new HashSet<>(); // 同じラベルの重複を排除する
			String[] companionNames = dto.getCompanionNames().split(",");
			for (String companionName : companionNames) {
				set.add(companionName);
			}
			dto.setCompanionNames(Arrays.toString(set.toArray()).replace("[", "").replace("]", ""));
		}
	}
	
	/**
	 * 訪問理由コードをラベルに変換して設定します。
	 * 
	 * @param divisionCList　選択項目リスト
	 * @param dto 訪問DTO
	 */
	protected void setDivisionCLabel(List<SelectionItems> divisionCList, CallsDto dto) {
		if (StringUtils.isNotEmpty(dto.getDivisionC())) {
			Set<String> set = new HashSet<>(); // 同じラベルの重複を排除する
			String[] divisionCs = dto.getDivisionC().split(",");
			for (String divisionC : divisionCs) {
				String divisionCLabel = getDivisionCLabel(divisionCList, divisionC);
				if (divisionCLabel != null) {
					set.add(divisionCLabel);
				}
			}
			dto.setDivisionC(Arrays.toString(set.toArray()).replace("[", "").replace("]", ""));
		}
	}
	
	/**
	 * 訪問理由に対応するラベルを取得する。
	 * 
	 * @param divisionCList 選択項目リスト
	 * @param divisionC 訪問理由コード
	 * @return ラベル
	 */
	protected String getDivisionCLabel(List<SelectionItems> divisionCList, String divisionC) {
		for (SelectionItems item : divisionCList) {
			if (divisionC.equals(item.getValue())) {
				return item.getLabel();
			}
		}
		return null;
	}
	
	/**
	 * 訪問理由に対応するラベルを取得します。
	 * 
	 * @param divisionCList 選択項目リスト
	 * @param divisionC 訪問理由コード
	 * @return ラベル
	 */
	protected String getDivisionCValue(List<SelectionItems> divisionCList, String divisionC) {
		for (SelectionItems item : divisionCList) {
			if (divisionC.equals(item.getValue())) {
				return item.getLabel();
			}
		}
		return null;
	}
	
	/**
	 * 指定した年月の得意先ごとの訪問実績数を検索します。
	 * 担当外訪問は省いて検索します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param yearMonth 年月
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者コード
	 * @param idForCustomer 得意先用ユーザID
	 * @param group グルーピング
	 * @return DTOリスト
	 */
	public List<CallsDto> findMonthlyTantoCallCountByAccount(int year, int month, List<String> div1, 
			String div2, String div3, String tanto, String group) {

		// 指定月のFrom、Toを作成
		Calendar fromCalender = DateUtils.getYearMonthCalendar(year, month);
		Calendar toCalender = DateUtils.getYearMonthCalendar(fromCalender, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();
		boolean isHistory = DateUtils.isPastYearMonth(year, month);

		List<CallsDto> dtoList = callsDao.findTantoCallCountByAccount(yearMonth, from, to, 
				div1, div2, div3, tanto, group, isHistory);		
		return dtoList;
	}
	
	/**
	 * 指定した年月日の得意先ごとの訪問実績数を検索します。
	 * 担当外訪問は省いて検索します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param yearMonth 年月
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者コード
	 * @param group グルーピング
	 * @return DTOリスト
	 */
	public List<CallsDto> findDailyTantoCallCountByAccount(int year, int month, int date, List<String> div1, 
			String div2, String div3, String tanto, String group) {

		// 指定月のFrom、Toを作成
		Calendar fromCalender = DateUtils.getYearMonthDateCalendar(year, month, date);
		Calendar toCalender = DateUtils.getYearMonthDateCalendar(fromCalender, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();
		boolean isHistory = DateUtils.isPastYearMonth(year, month);

		List<CallsDto> dtoList = callsDao.findTantoCallCountByAccount(yearMonth, from, to, 
				div1, div2, div3, tanto, group, isHistory);		
		return dtoList;
	}
	
	/**
	 * 既読にする
	 *
	 * @param calls
	 */
	public void toRead(Set<TechServiceCalls> calls, String userId) {

		// 重複排除
		Set<String> insertCalls = new HashSet<>();
		Set<String> insertTechCalls = new HashSet<>();

		for (Iterator<TechServiceCalls> it = calls.iterator(); it.hasNext();) {
			TechServiceCalls tc = it.next();
			if (StringUtils.isEmpty(tc.getCallId())) {
				if (callsReadUsersDao.countByUserIdAndCallId(userId, tc.getId(), false) < 1) {
					insertCalls.add(tc.getId());
				}
			} else {
				if (callsReadUsersDao.countByUserIdAndCallId(userId, tc.getId(), true) < 1) {
					insertTechCalls.add(tc.getId());
				}
			}
		}
		Timestamp date = DateUtils.getDBTimestamp();
		List<CallsReadUsers> save = setupReadList(insertCalls, userId, date);
		callsReadUsersDao.insert(save);

		List<TechServiceCallsReadUsers> techSave = convertCallsReadUsersToTech(setupReadList(insertTechCalls, userId,
				date));
		techServiceCallsReadUsersDao.insert(techSave);

	}

	protected List<CallsReadUsers> setupReadList(Set<String> calls, String userId, Timestamp date) {

		List<CallsReadUsers> save = new ArrayList<CallsReadUsers>();
		for (Iterator<String> it = calls.iterator(); it.hasNext();) {
			String callId = it.next();
			CallsReadUsers o = new CallsReadUsers();
			o.setId(IDUtils.generateGuid());
			o.setCallId(callId);
			o.setDateModified(date);
			o.setDeleted(Constants.Delete.False.toBoolean());
			o.setUserId(userId);
			save.add(o);
		}
		return save;
	}

	protected List<TechServiceCallsReadUsers> convertCallsReadUsersToTech(List<CallsReadUsers> list) {

		List<TechServiceCallsReadUsers> result = new ArrayList<>();
		for (Iterator<CallsReadUsers> it = list.iterator(); it.hasNext();) {
			result.add(dozerBeanMapper.map(it.next(), TechServiceCallsReadUsers.class));
		}
		return result;
	}

	/**
	 * 指定月の日別グルーピングの訪問を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public List<CallCountInfoDto> sumDailyGroupCall(int year, int month, String userId, List<String> div1, String div2,
			String div3, boolean isHistory) {
		return sumGroupCall(year, month, userId, div1, div2, div3, true, isHistory);
	}

	/**
	 * 指定年月の日別得意先別訪問を集計します。
	 * 組織条件を利用せず、担当者に紐づく訪問を全て集計します。
	 * 
	 * 過去検索でも得意先履歴を利用しないのでグルーピングや営業組織など
	 * 変更のありうる項目を検索条件としたり取得したりする場合には利用できません。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param userId ユーザID
	 * @return DTOリスト
	 */
	public List<CallCountInfoDto> sumDailyAccountCall(int year, int month, String userId) {
		return sumDailyAccountCall(year, month, userId, null, null, null, null, null, true);
	}

	/**
	 * 指定年月の日別得意先別訪問を集計します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param userId ユーザID
	 * @param div1 販社コード
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param marketId 販路
	 * @param group グルーピング
	 * @return DTOリスト
	 */
	public List<CallCountInfoDto> sumDailyAccountCall(int year, int month, String userId, String div1, String div2,
			String div3, String marketId, String group) {
		return sumDailyAccountCall(year, month, userId, div1, div2, div3, marketId, group, false);
	}

	/**
	 * 指定年月の日別得意先別訪問を集計します。
	 * 
	 * 組織条件を無視した場合、過去検索でも得意先履歴を利用しないのでグルーピングや営業組織など
	 * 変更のありうる項目を検索条件としたり取得したりする場合には利用できません。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param userId ユーザID
	 * @param div1 販社コード
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param marketId 販路
	 * @param group グルーピング
	 * @param canIgnoreSosikiCondition 組織条件を無視できるかどうか
	 * @return DTOリスト
	 */
	public List<CallCountInfoDto> sumDailyAccountCall(int year, int month, String userId, String div1, String div2,
			String div3, String marketId, String group, boolean canIgnoreSosikiCondition) {
		Calendar from = DateUtils.getYearMonthCalendar(year, month);
		Calendar to = DateUtils.getYearMonthCalendar(from, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		
		return callsDao.sumAccountCall(yearMonth, from.getTime(), to.getTime(), userId, div1, div2, div3, true,
				isHistory, marketId, group, canIgnoreSosikiCondition);
	}

	/**
	 * 組織を区別した日次訪問の集計。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param accountId
	 * @return
	 */
	public List<CallCountInfoDto> listDailyUserCallDistinctionSosoki(int year, int month, String userId, String div1,
			String div2, String div3) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return callsDao.listDailyUserCallDistinctionSosoki(yearMonth, from.getTime(), to.getTime(), userId, div1, div2,
				div3, yearMonth < nowYearMonth);
	}

	/**
	 * 指定月のグルーピング毎の訪問を集計する
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public List<CallCountInfoDto> sumMonthlyGroupCall(int year, int month, String userId, List<String> div1,
			String div2, String div3, boolean isHistory) {
		return sumGroupCall(year, month, userId, div1, div2, div3, false, isHistory);
	}

	/**
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isDaily
	 *            日次で集計するか
	 * @param isHistory
	 * @return
	 */
	protected List<CallCountInfoDto> sumGroupCall(int year, int month, String userId, List<String> div1, String div2,
			String div3, boolean isDaily, boolean isHistory) {

		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		return callsDao.sumGroupCall(yearMonth, from.getTime(), to.getTime(), userId, div1, div2, div3, isDaily,
				isHistory);
	}

	/**
	 * 指定月の日別のユーザーの訪問を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public List<CallCountInfoDto> listDailyUserCall(int year, int month, String userId, String div1, String div2,
			String div3, boolean isHistory) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		return callsDao.sumUserCall(yearMonth, from.getTime(), to.getTime(), userId, div1, div2, div3, true, isHistory);

	}

	/**
	 * SUTATUSがHeldの訪問をリストする。
	 * 
	 * @param year
	 * @param month
	 * @param accountId
	 * @param userId
	 * @return
	 */
	public List<Calls> listAccountHeldCall(int year, int month, String accountId, String userId) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		return callsDao.listAccountHeldCall(from.getTime(), to.getTime(), accountId, userId);

	}

	/**
	 * 指定条件の訪問を検索する
	 * 
	 * @param condition
	 * @return
	 */
	public List<CallsDto> find(CallsSearchConditionDto condition) {
		condition.setLocale(LocaleContextHolder.getLocale().getLanguage());
		List<CallsDto> list = callsDao.findByCondition(condition);

		return list;
	}

	/**
	 * 指定条件の訪問をカウントする
	 * 
	 * @param condition
	 * @return
	 */
	@Deprecated
	public int count(CallsSearchConditionDto condition) {

		return callsDao.countByCondition(condition);
	}

	/**
	 * コメントを検索する
	 * 
	 * @param callId
	 * @param userId
	 * @return
	 */
	public CallsDto findCommentsInfo(String callId, String userId) {
		return callsDao.findCommentInfo(callId, userId, false);
	}

	/**
	 * コメントを検索する
	 * 
	 * @param callId
	 * @param userId
	 * @return
	 */
	public CallsDto findCommentsInfo4Tech(String callId, String userId) {
		return callsDao.findCommentInfo(callId, userId, true);
	}

	/**
	 * Likeする
	 * 
	 * @param callId
	 * @param userId
	 */
	public void like(String callId, String userId) {

		// 重複登録対応
		if (callsLikeUsersDao.findByCallIdAndUserId(callId, userId, false) != null) {
			return;
		}

		CallsLikeUsers e = new CallsLikeUsers();
		e.setCallId(callId);
		e.setUserId(userId);
		e.setId(IDUtils.generateGuid());
		e.setDateModified(DateUtils.getDBTimestamp());
		e.setDeleted(Constants.Delete.False.toBoolean());
		callsLikeUsersDao.insert(e);
	}

	/**
	 * Likeする
	 * 
	 * @param callId
	 * @param userId
	 */
	public void like4Tech(String callId, String userId) {

		// 重複登録対応
		if (callsLikeUsersDao.findByCallIdAndUserId(callId, userId, true) != null) {
			return;
		}

		TechServiceCallsLikeUsers e = new TechServiceCallsLikeUsers();
		e.setCallId(callId);
		e.setUserId(userId);
		e.setId(IDUtils.generateGuid());
		e.setDateModified(DateUtils.getDBTimestamp());
		e.setDeleted(Constants.Delete.False.toBoolean());
		techServiceCallsLikeUsersDao.insert(e);
	}

	/**
	 * Likeを解除する
	 * 
	 * @param callId
	 * @param userId
	 */
	public void unlike(String callId, String userId) {
		CallsLikeUsers e = callsLikeUsersDao.findByCallIdAndUserId(callId, userId, false);
		callsLikeUsersDao.delete(e);
	}

	/**
	 * Likeを解除する
	 * 
	 * @param callId
	 * @param userId
	 */
	public void unlike4Tech(String callId, String userId) {
		CallsLikeUsers e = callsLikeUsersDao.findByCallIdAndUserId(callId, userId, true);
		techServiceCallsLikeUsersDao.delete(dozerBeanMapper.map(e, TechServiceCallsLikeUsers.class));
	}

	/**
	 * 訪問日の最大最小を取得する。
	 * 
	 * @return
	 */
	public MinMaxDateDto getExistCallsDate() {
		return callsDao.findCallsMinMaxDate();
	}

	/**
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param paramDiv1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public List<UnvisitAccountDto> listUnvisitAccounts(int year, int month, String userId, List<String> div1,
			String paramDiv1, String div2, String div3) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		List<UnvisitAccountDto> resultList = callsDao.listUnvisitAccounts(yearMonth, year, month, from.getTime(),
				to.getTime(), now.getTime(), userId, div1, div2, div3, yearMonth < nowYearMonth);

		if (yearMonth != nowYearMonth) {
			// 過去、未来の場合は前日迄の未訪問数を表示しない
			for (UnvisitAccountDto dto : resultList) {
				dto.setYesterdayPlan(null);
			}
		}

		return resultList;
	}

	/**
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public int countUnvisitAccounts(int year, int month, String userId, List<String> div1, String div2, String div3) {
		Calendar from = Calendar.getInstance();
		from.set(year, month - 1, 1, 0, 0, 0);
		from.set(Calendar.MILLISECOND, 0);

		Calendar to = Calendar.getInstance();
		to.setTime(from.getTime());
		to.add(Calendar.MONTH, 1);

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		return callsDao.countUnvisitAccounts(yearMonth, year, month, from.getTime(), to.getTime(), userId, div1, div2,
				div3, yearMonth < nowYearMonth);

	}

	/**
	 * 個人別訪問実績を集計します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param div1 販社コード
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @return 結果リスト
	 */
	public List<MonthVisitResultPerSalesDto> sumMonthVisitResultPerSales(Integer year, Integer month,
			String div1, String div2, String div3) {

		// 指定月のFrom、Toを作成
		Calendar fromCalender = DateUtils.getYearMonthCalendar(year, month);
		Calendar toCalender = DateUtils.getYearMonthCalendar(fromCalender, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();
		
		// 指定月の前月のFrom、Toの作成
		fromCalender.add(Calendar.MONTH, -1);
		toCalender.add(Calendar.MONTH, -1);
		Integer yearLastMonth = DateUtils.toYearMonth(fromCalender);
		Date lastMonthFrom = fromCalender.getTime();
		Date lastMonthTo = toCalender.getTime();

		List<SelectionItems> items = selectionItemsService.listDivisionC();
		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		// 全てを選択できない前提でdiv1はリストにしていない
		return callsDao.sumMonthVisitResultPerSales(year, month, yearMonth, from, to, yearLastMonth, lastMonthFrom,
				lastMonthTo, div1, div2, div3, items, isHistory);
	}

	/**
	 * 得意先別訪問実績を集計します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param div1 販社コード
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param market 販路
	 * @param group グルーピング
	 * @return 結果リスト
	 */
	public List<MonthVisitResultPerAccountDto> sumMonthVisitResultPerAccount(Integer year, Integer month, String div1,
			String div2, String div3, String market, String group) {

		// 指定月のFrom、Toを作成
		Calendar fromCalender = DateUtils.getYearMonthCalendar(year, month);
		Calendar toCalender = DateUtils.getYearMonthCalendar(fromCalender, 1);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		Date from = fromCalender.getTime();
		Date to = toCalender.getTime();
		
		// 指定月の前月のFrom、Toの作成
		fromCalender.add(Calendar.MONTH, -1);
		toCalender.add(Calendar.MONTH, -1);
		Integer yearLastMonth = DateUtils.toYearMonth(fromCalender);
		Date lastMonthFrom = fromCalender.getTime();
		Date lastMonthTo = toCalender.getTime();
		
		List<SelectionItems> items = selectionItemsService.listDivisionC();
		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		// 全てを選択できない前提でdiv1はリストにしていない
		return callsDao.sumMonthVisitResultPerAccount(year, month, yearMonth, from, to, yearLastMonth, lastMonthFrom,
				lastMonthTo, div1, div2, div3, items, isHistory, market, group);
	}

	/**
	 * 日付・得意先別の訪問計画、訪問実績を集計します。
	 * 
	 * @param userId 担当者ID
	 * @param from 取得したい週の月曜日
	 * @param to 取得したい週の日曜日
	 * @return 結果リスト
	 */
	public List<WeekAccountCallDto> sumAccountWeekCall(String userId, Date from, Date to) {
		return callsDao.sumWeekAccountCall(userId, from, to);
	}

	/**
	 * 訪問に紐づく感度、情報区分、内容を取得します。<br>
	 * 感度、情報区分のどちらも入力されているもののみを取得します。
	 * 
	 * @param userId 担当者ID
	 * @param from 取得したい週の月曜日
	 * @param to 取得したい週の日曜日
	 * @return 結果リスト
	 */
	public List<CallsDto> findSensitivityCommentInfo (String userId, Date from, Date to) {
		return callsDao.findSensitivityCommentInfo(userId, from, to);
	}

	public List<UserSosikiDto> listCallReadUser(String callId) {
		return callsReadUsersDao.selectByUserIdAndCallId(callId, false);
	}

	public List<UserSosikiDto> listCallReadUser4Tech(String callId) {
		return callsReadUsersDao.selectByUserIdAndCallId(callId, true);
	}

	public CallsFiles selectFile1(String id) {
		return callsFilesDao.selectFile1(id, false);
	}

	public CallsFiles selectFile2(String id) {
		return callsFilesDao.selectFile2(id, false);
	}

	public CallsFiles selectFile3(String id) {
		return callsFilesDao.selectFile3(id, false);
	}

	public CallsFiles selectFile14Tech(String id) {
		return callsFilesDao.selectFile1(id, true);
	}

	public CallsFiles selectFile24Tech(String id) {
		return callsFilesDao.selectFile2(id, true);
	}

	public CallsFiles selectFile34Tech(String id) {
		return callsFilesDao.selectFile3(id, true);
	}
}
