package jp.co.yrc.mv.service;

import java.sql.Timestamp;
import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.IDUtils;
import jp.co.yrc.mv.dao.CallsDao;
import jp.co.yrc.mv.dao.CommentsDao;
import jp.co.yrc.mv.dao.CommentsReadUsersDao;
import jp.co.yrc.mv.dao.TechServiceCommentsDao;
import jp.co.yrc.mv.dao.TechServiceCommentsReadUsersDao;
import jp.co.yrc.mv.dto.CommentsDto;
import jp.co.yrc.mv.entity.Comments;
import jp.co.yrc.mv.entity.CommentsReadUsers;
import jp.co.yrc.mv.entity.TechServiceComments;
import jp.co.yrc.mv.entity.TechServiceCommentsReadUsers;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentsService {
	@Autowired
	CommentsDao commentsDao;

	@Autowired
	TechServiceCommentsDao techServiceCommentsDao;

	@Autowired
	CallsDao callsDao;

	@Autowired
	TechServiceCommentsReadUsersDao techServiceCommentsReadUsersDao;

	@Autowired
	CommentsReadUsersDao commentsReadUsersDao;

	@Autowired
	MstEigyoSosikiService eigyoSosikiService;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	public Comments findOne(String id) {
		return commentsDao.selectById(id);
	}

	public TechServiceComments findOne4Tech(String id) {
		return techServiceCommentsDao.selectById(id);
	}

	public int save(Comments c, String userId) {

		setupInsertValue(c, userId);
		c.setParentType(Constants.ParentType.Calls.toString());
		return commentsDao.insert(c);
	}

	public int save4Tech(Comments c, String userId) {

		setupInsertValue(c, userId);
		TechServiceComments tc = dozerBeanMapper.map(c, TechServiceComments.class);
		tc.setParentType(Constants.ParentType.TechServiceCalls.toString());
		return techServiceCommentsDao.insert(tc);
	}

	protected void setupInsertValue(Comments c, String userId) {
		Timestamp date = DateUtils.getDBTimestamp();
		c.setId(IDUtils.generateGuid());
		c.setName(null);
		c.setDateEntered(date);
		c.setDateModified(date);
		c.setModifiedUserId(userId);
		c.setCreatedBy(userId);
		c.setDeleted(Constants.Delete.False.toBoolean());
		c.setAssignedUserId(userId);
	}

	/**
	 * 自分を既読にする
	 * 
	 * @param callId
	 */
	public void toRead(String callId, String userId) {

		if (commentsReadUsersDao.findByCallIdAndUserId(callId, userId, false) == null) {
			CommentsReadUsers commentsReadUsers = createToReadValue(callId, userId);
			commentsReadUsersDao.insert(commentsReadUsers);
		}
	}

	/**
	 * 自分を既読にする
	 * 
	 * @param callId
	 */
	public void toRead4Tech(String callId, String userId) {

		if (commentsReadUsersDao.findByCallIdAndUserId(callId, userId, true) == null) {
			CommentsReadUsers commentsReadUsers = createToReadValue(callId, userId);

			techServiceCommentsReadUsersDao.insert(dozerBeanMapper.map(commentsReadUsers,
					TechServiceCommentsReadUsers.class));
		}
	}

	protected CommentsReadUsers createToReadValue(String callId, String userId) {
		Timestamp date = DateUtils.getDBTimestamp();
		CommentsReadUsers commentsReadUsers = new CommentsReadUsers();
		commentsReadUsers.setCallId(callId);
		commentsReadUsers.setUserId(userId);
		commentsReadUsers.setId(IDUtils.generateGuid());
		commentsReadUsers.setDateModified(date);
		commentsReadUsers.setDeleted(Constants.Delete.False.toBoolean());
		return commentsReadUsers;
	}

	/**
	 * 自分以外のユーザーを未読にする
	 * 
	 * @param callId
	 */
	public void toUnread(String callId, String userId) {
		List<CommentsReadUsers> list = commentsReadUsersDao.findByCallIdAndUserIdNot(callId, userId, false);
		commentsReadUsersDao.delete(list);
	}

	/**
	 * 自分以外のユーザーを未読にする
	 * 
	 * @param callId
	 */
	public void toUnread4Tech(String callId, String userId) {
		List<CommentsReadUsers> list = commentsReadUsersDao.findByCallIdAndUserIdNot(callId, userId, true);
		for (CommentsReadUsers u : list) {
			techServiceCommentsReadUsersDao.delete(dozerBeanMapper.map(u, TechServiceCommentsReadUsers.class));
		}
	}

	/**
	 * 訪問のコメントをリストします
	 * 
	 * @param callId
	 * @return
	 */
	public List<CommentsDto> listCallsComment(String callId) {

		List<CommentsDto> result = commentsDao.listCallsComment(callId, false);
		return result;
	}

	/**
	 * 訪問のコメントをリストします
	 * 
	 * @param callId
	 * @return
	 */
	public List<CommentsDto> listCallsComment4Tech(String callId) {

		List<CommentsDto> result = commentsDao.listCallsComment(callId, true);
		return result;
	}

	/**
	 * 確認コメントでないコメントの数をカウントします
	 * 
	 * @param callId
	 * @return
	 */
	public int countNonConfirmationCallsComment(String callId) {
		return commentsDao.countNonConfirmationCallsComment(callId, false);
	}

	/**
	 * 確認コメントでないコメントの数をカウントします
	 * 
	 * @param callId
	 * @return
	 */
	public int countNonConfirmationCallsComment4Tech(String callId) {
		return commentsDao.countNonConfirmationCallsComment(callId, true);
	}

	/**
	 * コメントを削除します。
	 * 
	 * @param comment
	 *            削除対象コメントエンティティ
	 */
	public void delete(Comments comment) {
		commentsDao.delete(comment);
	}

	/**
	 * コメントを削除します。
	 * 
	 * @param comment
	 *            削除対象コメントエンティティ
	 */
	public void delete4Tech(TechServiceComments comment) {
		techServiceCommentsDao.delete(comment);
	}
}
