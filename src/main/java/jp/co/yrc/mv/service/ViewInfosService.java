package jp.co.yrc.mv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.dao.ViewInfosDao;
import jp.co.yrc.mv.entity.ViewInfos;

@Service
public class ViewInfosService {

	@Autowired
	ViewInfosDao viewInfosDao;

	/**
	 * ViewInfosのアイテムを取得
	 * 
	 * @return
	 */
	public List<ViewInfos> listLogInfosView(String url ) {
		return viewInfosDao.listViewInfos(url);
	}

}
