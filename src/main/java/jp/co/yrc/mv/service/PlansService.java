package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.dao.MonthlyKindPlansDao;
import jp.co.yrc.mv.dao.PlansDao;
import jp.co.yrc.mv.dto.CompassControlResultDto;
import jp.co.yrc.mv.entity.MonthlyKindPlans;

/**
 * 実績系のService
 * 
 * @author Norihiko
 *
 */
@Service
public class PlansService {

	@Autowired
	MonthlyKindPlansDao monthlyKindPlansDao;

	@Autowired
	PlansDao plansDao;

	/**
	 * 月のグループ別実績を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public MonthlyKindPlans sumMonthlyKindPlan(int year, int month, String userId, List<String> div1, String div2,
			String div3) {

		int yearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		MonthlyKindPlans m = monthlyKindPlansDao.sum(year, month, userId, div1, div2, div3, yearMonth < nowYearMonth);
		if (m == null) {
			m = new MonthlyKindPlans();
		}

		m.setPcrSummerSales(MathUtils.toDefaultZero(m.getPcrSummerSales()));
		m.setPcrSummerMargin(MathUtils.toDefaultZero(m.getPcrSummerMargin()));
		m.setPcrSummerHqMargin(MathUtils.toDefaultZero(m.getPcrSummerHqMargin()));
		m.setPcrSummerNumber(MathUtils.toDefaultZero(m.getPcrSummerNumber()));

		m.setPcrPureSnowSales(MathUtils.toDefaultZero(m.getPcrPureSnowSales()));
		m.setPcrPureSnowMargin(MathUtils.toDefaultZero(m.getPcrPureSnowMargin()));
		m.setPcrPureSnowHqMargin(MathUtils.toDefaultZero(m.getPcrPureSnowHqMargin()));
		m.setPcrPureSnowNumber(MathUtils.toDefaultZero(m.getPcrPureSnowNumber()));

		m.setVanSummerSales(MathUtils.toDefaultZero(m.getVanSummerSales()));
		m.setVanSummerMargin(MathUtils.toDefaultZero(m.getVanSummerMargin()));
		m.setVanSummerHqMargin(MathUtils.toDefaultZero(m.getVanSummerHqMargin()));
		m.setVanSummerNumber(MathUtils.toDefaultZero(m.getVanSummerNumber()));

		m.setVanPureSnowSales(MathUtils.toDefaultZero(m.getVanPureSnowSales()));
		m.setVanPureSnowMargin(MathUtils.toDefaultZero(m.getVanPureSnowMargin()));
		m.setVanPureSnowHqMargin(MathUtils.toDefaultZero(m.getVanPureSnowHqMargin()));
		m.setVanPureSnowNumber(MathUtils.toDefaultZero(m.getVanPureSnowNumber()));

		m.setLtSummerSales(MathUtils.toDefaultZero(m.getLtSummerSales()));
		m.setLtSummerMargin(MathUtils.toDefaultZero(m.getLtSummerMargin()));
		m.setLtSummerHqMargin(MathUtils.toDefaultZero(m.getLtSummerHqMargin()));
		m.setLtSummerNumber(MathUtils.toDefaultZero(m.getLtSummerNumber()));

		m.setLtSnowSales(MathUtils.toDefaultZero(m.getLtSnowSales()));
		m.setLtSnowMargin(MathUtils.toDefaultZero(m.getLtSnowMargin()));
		m.setLtSnowHqMargin(MathUtils.toDefaultZero(m.getLtSnowHqMargin()));
		m.setLtSnowNumber(MathUtils.toDefaultZero(m.getLtSnowNumber()));

		m.setTbSummerSales(MathUtils.toDefaultZero(m.getTbSummerSales()));
		m.setTbSummerMargin(MathUtils.toDefaultZero(m.getTbSummerMargin()));
		m.setTbSummerHqMargin(MathUtils.toDefaultZero(m.getTbSummerHqMargin()));
		m.setTbSummerNumber(MathUtils.toDefaultZero(m.getTbSummerNumber()));

		m.setTbSnowSales(MathUtils.toDefaultZero(m.getTbSnowSales()));
		m.setTbSnowMargin(MathUtils.toDefaultZero(m.getTbSnowMargin()));
		m.setTbSnowHqMargin(MathUtils.toDefaultZero(m.getTbSnowHqMargin()));
		m.setTbSnowNumber(MathUtils.toDefaultZero(m.getTbSnowNumber()));

		m.setOrIdSales(MathUtils.toDefaultZero(m.getOrIdSales()));
		m.setOrIdMargin(MathUtils.toDefaultZero(m.getOrIdMargin()));
		m.setOrIdHqMargin(MathUtils.toDefaultZero(m.getOrIdHqMargin()));
		m.setOrIdNumber(MathUtils.toDefaultZero(m.getOrIdNumber()));

		m.setTifuSales(MathUtils.toDefaultZero(m.getTifuSales()));
		m.setTifuMargin(MathUtils.toDefaultZero(m.getTifuMargin()));
		m.setTifuHqMargin(MathUtils.toDefaultZero(m.getTifuHqMargin()));
		m.setTifuNumber(MathUtils.toDefaultZero(m.getTifuNumber()));

		m.setOtherSales(MathUtils.toDefaultZero(m.getOtherSales()));
		m.setOtherMargin(MathUtils.toDefaultZero(m.getOtherMargin()));
		m.setOtherHqMargin(MathUtils.toDefaultZero(m.getOtherHqMargin()));
		// monthlyKindResults.setOthreNumber(monthlyKindResults.getOthreNumber(),
		// kindMarketTotalDto.getOthreNumber()));

		m.setWorkSales(MathUtils.toDefaultZero(m.getWorkSales()));
		m.setWorkMargin(MathUtils.toDefaultZero(m.getWorkMargin()));
		m.setWorkHqMargin(MathUtils.toDefaultZero(m.getWorkHqMargin()));
		// monthlyKindResults.setworkNumber(monthlyKindResults.getworkNumber(),
		// kindMarketTotalDto.getworkNumber()));

		m.setPcbSummerSales(MathUtils.toDefaultZero(m.getPcbSummerSales()));
		m.setPcbSummerMargin(MathUtils.toDefaultZero(m.getPcbSummerMargin()));
		m.setPcbSummerHqMargin(MathUtils.toDefaultZero(m.getPcbSummerHqMargin()));
		m.setPcbSummerNumber(MathUtils.toDefaultZero(m.getPcbSummerNumber()));

		m.setPcbPureSnowSales(MathUtils.toDefaultZero(m.getPcbPureSnowSales()));
		m.setPcbPureSnowMargin(MathUtils.toDefaultZero(m.getPcbPureSnowMargin()));
		m.setPcbPureSnowHqMargin(MathUtils.toDefaultZero(m.getPcbPureSnowHqMargin()));
		m.setPcbPureSnowNumber(MathUtils.toDefaultZero(m.getPcbPureSnowNumber()));

		return m;
	}

	public List<CompassControlResultDto> sumCompassControl(int year, int month, String userId, List<String> div1,
			String div2, String div3) {

		return plansDao.sumCompassControl(year, month, userId, div1, div2, div3);
	}
}
