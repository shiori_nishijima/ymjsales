package jp.co.yrc.mv.service;

import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.dao.SelectionItemsDao;
import jp.co.yrc.mv.dao.SelectionItemsSecondLevelDao;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.entity.SelectionItemsSecondLevel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SelectionItemsService {

	@Autowired
	SelectionItemsDao selectionItemsDao;

	@Autowired
	SelectionItemsSecondLevelDao selectionItemsSecondLevelDao;

	/**
	 * divisionCのアイテムを取得
	 * 
	 * @return
	 */
	public List<SelectionItems> listDivisionC() {
		List<SelectionItems> selectionItems = selectionItemsDao.listByEntityNameAndPropertyName(
				Constants.SelectionItems.Calls.getName(), Constants.SelectionItems.Calls.DIVISION_C, LocaleContextHolder.getLocale().getLanguage());
		return selectionItems;
	}

	/**
	 * CompassKindのアイテムを取得
	 * 
	 * @return
	 */
	public List<SelectionItems> listCompassKindLargeClass2() {
		List<SelectionItems> selectionItems = selectionItemsDao.listByEntityNameAndPropertyName(
				Constants.SelectionItems.CompassKind.getName(), Constants.SelectionItems.CompassKind.LARGE_CLASS_2, LocaleContextHolder.getLocale().getLanguage());
		return selectionItems;
	}

	/**
	 * SelectionItemsSecondLevelのアイテムを取得
	 * 
	 * @return
	 */
	public List<SelectionItemsSecondLevel> listSecondLevelBySelectionItems(SelectionItems items) {
		SelectionItemsSecondLevel entity = new SelectionItemsSecondLevel();
		entity.setKeyEntityName(items.getEntityName());
		entity.setKeyPropertyName(items.getPropertyName());
		entity.setKeyValue(items.getValue());
		List<SelectionItemsSecondLevel> selectionItems = selectionItemsSecondLevelDao.findByEntity(entity, LocaleContextHolder.getLocale().getLanguage());
		return selectionItems;
	}
}
