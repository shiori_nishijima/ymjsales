package jp.co.yrc.mv.service;

import static jp.co.yrc.mv.common.Constants.LogInfos.TimeUnit.Hour;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.seasar.doma.jdbc.IterationContext;
import org.seasar.doma.jdbc.PostIterationCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.LogInfo;
import jp.co.yrc.mv.common.Constants.LogInfos.UserAgent;
import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.IDUtils;
import jp.co.yrc.mv.dao.LogInfosDao;
import jp.co.yrc.mv.dto.LogInfosDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.dto.YearMonthDto;
import jp.co.yrc.mv.entity.LogInfos;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.ViewInfos;

@Service
@Transactional
public class LogInfoService {

	public static final String URL_REGEX = "($|\\?.*|/.*)";

	@Autowired
	LogInfosDao logInfosDao;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	ViewInfosService viewInfosService;

	@Autowired
	MstEigyoSosikiService eigyoSosikiService;

	/**
	 * 画像などのファイルはログ対象外
	 */
	static final Pattern PATTERN = Pattern.compile("^(images|css|js)($|/)");

	public List<LogInfosDto> listAccessLog(Date start, Date end, List<String> div1, String div2, String div3,
			String userId, final String timeunit, String url, UserInfoDto userInfo) {

		Date now = DateUtils.getDBDate();
		Calendar c = Calendar.getInstance();
		c.setTime(now);
		int todayYear = c.get(Calendar.YEAR);
		int todayMonth = c.get(Calendar.MONTH) + 1;

		List<YearMonthDto> terms = new ArrayList<>();

		Calendar startCal = Calendar.getInstance();
		startCal.setTime(start);

		if (Hour.getValue().equals(timeunit)) {
			end = new Date(start.getTime());
		}
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(end);

		endCal.set(Calendar.HOUR_OF_DAY, 23);
		endCal.set(Calendar.MINUTE, 59);
		endCal.set(Calendar.SECOND, 59);
		endCal.set(Calendar.MILLISECOND, 999);

		for (int i = DateUtils.toYearMonth(startCal), endYearMonth = DateUtils
				.toYearMonth(endCal); i <= endYearMonth; startCal.add(Calendar.MONTH,
						1), i = DateUtils.toYearMonth(startCal)) {

			YearMonthDto o = new YearMonthDto();
			o.setYear(startCal.get(Calendar.YEAR));
			o.setMonth(startCal.get(Calendar.MONTH) + 1);
			terms.add(o);
		}

		List<MstEigyoSosiki> sosikis = eigyoSosikiService.findByUserId(userInfo.getCorpCodes(), userInfo.getUsername(),
				todayYear, todayMonth);

		final List<ViewInfos> viewList = viewInfosService.listLogInfosView(url);
		List<LogInfosDto> list = logInfosDao.selectAccessLog(start, endCal.getTime(), div1, div2, div3, userId,
				todayYear, todayMonth, terms, new LogInfoSumCallback(viewList, timeunit,
						Role.getByCode(userInfo.getAuthorization()).getParent(), sosikis));

		return list;
	}

	public void access(String uri, String contextPath, Map<String, String[]> paramMap, String ua, String userId,
			Date loginDate) {

		uri = StringUtils.defaultString(uri);
		if (uri.startsWith(contextPath)) {
			uri = uri.replaceAll("^" + contextPath + "/?", "");

			if (!PATTERN.matcher(uri).find()) {
				Date date = DateUtils.getDBDate();
				Timestamp time = new Timestamp(date.getTime());
				LogInfos e = new LogInfos();

				// リクエストパラメータがあればくっつける
				// GETでもPOSTでも同等に扱う、今後GETのみにする必要があるかもしれないがいったんこのまま
				StringBuilder sb = new StringBuilder(paramMap.isEmpty() ? uri : uri + "?");
				for (Entry<String, String[]> entry : paramMap.entrySet()) {

					String name = entry.getKey();
					String[] values = entry.getValue();

					for (String value : values) {
						sb.append(name).append("=").append(StringUtils.defaultString(value)).append("&");
					}
				}

				// 最後の＆を削除
				if (sb.length() - 1 == sb.lastIndexOf("&")) {
					sb.replace(sb.length() - 1, sb.length(), "");
				}

				e.setDateEntered(time);
				e.setDescription(sb.toString());
				e.setEventTimestamp(time);
				e.setId(IDUtils.generateGuid());
				e.setLevel(LogInfo.Level.ACCESS_MV);
				e.setLoginTimestamp(loginDate != null ? new Timestamp(loginDate.getTime()) : null);
				e.setUserAgent(ua);
				e.setUserId(userId);

				logInfosDao.insert(e);
			}

		}
	}

	private static class LogInfoSumCallback implements PostIterationCallback<List<LogInfosDto>, LogInfosDto> {

		List<ViewInfos> viewList;

		String timeUnit;

		Role role;

		List<MstEigyoSosiki> sosikis;

		Map<Key, LogInfosDto> map = new LinkedHashMap<>();

		public LogInfoSumCallback(List<ViewInfos> viewList, String timeUnit, Role role, List<MstEigyoSosiki> sosikis) {
			super();
			this.viewList = viewList;
			this.timeUnit = timeUnit;
			this.role = role;
			this.sosikis = sosikis;
		}

		@Override
		public List<LogInfosDto> iterate(LogInfosDto target, IterationContext context) {

			Key key = checkUrl(viewList, target);
			if (key == null) {
				return null;
			}
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(target.getEventTimestamp().getTime());
			// 月
			if (Constants.LogInfos.TimeUnit.Month.getValue().equals(timeUnit)) {

				target.setTime(c.get(Calendar.MONTH) + 1);
			}

			// 日
			else if (Constants.LogInfos.TimeUnit.Day.getValue().equals(timeUnit)) {
				target.setTime(c.get(Calendar.DAY_OF_MONTH));
			}

			// 時間
			else {
				target.setTime(c.get(Calendar.HOUR_OF_DAY));
			}

			target.setClientType(
					target.getUserAgent().contains("iPad") ? UserAgent.IPad.getValue() : UserAgent.PC.getValue());

			boolean sumIndividual = isSumIndividual(target);
			key.time = target.getTime();
			key.clientType = target.getClientType();
			key.eigyoSosikiCd = target.getEigyoSosikiCd();

			// 個人集計するかどうか、しない場合はNULLで営業所合計とする
			key.userId = sumIndividual ? target.getUserId() : null;

			LogInfosDto o = map.get(key);
			if (o == null) {
				o = target;
				if (!sumIndividual) {
					o.setUserId(null);
				}
				map.put(key, o);
			}
			o.setCount(o.getCount().add(BigDecimal.ONE));

			return null;
		}

		/**
		 * All<br>
		 * &nbsp; ・全部個人<br>
		 * 本部権限 [Multiple, Company]<br>
		 * &nbsp; ・自拠点内は個人 <br>
		 * &nbsp; ・他拠点は営業所合計<br>
		 * Co権限[Department] <br>
		 * &nbsp; ・自Co内は個人 <br>
		 * &nbsp; ・自営業本部内他Coは営業所<br>
		 * 営業所権限[SalesOffice, Sales]<br>
		 * &nbsp; ・自営業所は個人<br>
		 * &nbsp; ・自Co内他営業所は営業所<br>
		 * 
		 * @param target
		 * @return 個人集計の場合は<code>true</code>を返す
		 */
		protected boolean isSumIndividual(LogInfosDto target) {
			switch (role) {
			case All:
				return true;
			case Multiple:
			case Company:
				for (MstEigyoSosiki s : sosikis) {
					if (Objects.equals(s.getDiv1Cd(), target.getDiv1())) {
						return true;
					}
				}
				break;

			case Department:
				for (MstEigyoSosiki s : sosikis) {
					if (Objects.equals(s.getDiv1Cd(), target.getDiv1())
							&& Objects.equals(s.getDiv2Cd(), target.getDiv2())) {
						return true;
					}
				}
				break;
			case SalesOffice:
			case Sales:
				for (MstEigyoSosiki s : sosikis) {
					if (Objects.equals(s.getDiv1Cd(), target.getDiv1())
							&& Objects.equals(s.getDiv2Cd(), target.getDiv2())
							&& Objects.equals(s.getDiv3Cd(), target.getDiv3())) {
						return true;
					}
				}
			default:
				break;
			}
			return false;
		}

		protected Key checkUrl(final List<ViewInfos> viewList, LogInfosDto o) {
			for (ViewInfos s : viewList) {
				if (o.getDescription().matches("^" + s.getUrl() + URL_REGEX)) {
					Key key = new Key();
					key.view = s.getUrl();
					o.setView(s.getUrl());
					o.setViewName(s.getName());
					return key;
				}
			}
			return null;
		}

		@Override
		public List<LogInfosDto> postIterate(List<LogInfosDto> result, IterationContext context) {

			List<LogInfosDto> list = new ArrayList<>(map.values());
			return list;
		}
	}

	private static class Key {

		String eigyoSosikiCd;
		String userId;
		String view;
		String clientType;
		long time;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;

			result = prime * result + ((view == null) ? 0 : view.hashCode());
			result = prime * result + ((eigyoSosikiCd == null) ? 0 : eigyoSosikiCd.hashCode());
			result = prime * result + ((userId == null) ? 0 : userId.hashCode());
			result = prime * result + (int) (time ^ (time >>> 32));
			result = prime * result + ((clientType == null) ? 0 : clientType.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Key)) {
				return false;
			}
			Key other = (Key) obj;

			if (view == null) {
				if (other.view != null) {
					return false;
				}
			} else if (!view.equals(other.view)) {
				return false;
			}

			if (eigyoSosikiCd == null) {
				if (other.eigyoSosikiCd != null) {
					return false;
				}
			} else if (!eigyoSosikiCd.equals(other.eigyoSosikiCd)) {
				return false;
			}

			if (userId == null) {
				if (other.userId != null) {
					return false;
				}
			} else if (!userId.equals(other.userId)) {
				return false;
			}

			if (time != other.time) {
				return false;
			}

			if (clientType == null) {
				if (other.clientType != null) {
					return false;
				}
			} else if (!clientType.equals(other.clientType)) {
				return false;
			}
			return true;
		}
	}
}
