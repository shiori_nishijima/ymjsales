package jp.co.yrc.mv.service;

import jp.co.yrc.mv.dao.MarketInfosDao;
import jp.co.yrc.mv.dto.MarketInfoDto;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class MarketInfosService {

	@Autowired
	MarketInfosDao marketInfosDao;


	public MarketInfoDto selectById(String id, String techCallId) {
		
		return marketInfosDao.selectByCallId(id, StringUtils.isNotEmpty(techCallId), LocaleContextHolder.getLocale().getLanguage());

	}


}
