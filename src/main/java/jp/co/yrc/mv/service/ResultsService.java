package jp.co.yrc.mv.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.dao.MonthlyKindPlansDao;
import jp.co.yrc.mv.dao.MonthlyKindResultsDao;
import jp.co.yrc.mv.dao.ResultsDao;
import jp.co.yrc.mv.dto.AccountKindResultDto;
import jp.co.yrc.mv.dto.CompassControlResultDto;
import jp.co.yrc.mv.dto.MinMaxDto;
import jp.co.yrc.mv.entity.MonthlyGroupResults;
import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.entity.Results;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 実績系のService
 * 
 * @author Norihiko
 *
 */
@Service
public class ResultsService {

	@Autowired
	ResultsDao resultsDao;

	@Autowired
	MonthlyKindResultsDao monthlyKindResultsDao;

	@Autowired
	MonthlyKindPlansDao monthlyKindPlansDao;

	public MinMaxDto findMinMaxYear() {
		return resultsDao.findMinMaxYear();
	}

	/**
	 * 月のグループ別実績を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public MonthlyGroupResults sumMonthlyGroupResult(int year, int month, String userId, List<String> div1, String div2,
			String div3, boolean isHistory) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return resultsDao.sumGroupResult(year, month, null, yearMonth, userId, div1, div2, div3, isHistory);
	}

	/**
	 * 月の日別のユーザー実績をリストする。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public List<Results> listDailyUserResult(int year, int month, String userId, List<String> div1, String div2,
			String div3, boolean isHistory) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return resultsDao.listDailyUserResult(year, month, yearMonth, userId, div1, div2, div3, isHistory);
	}

	/**
	 * 月の日別の得意先実績をリストする。
	 * 
	 * @param year
	 * @param month
	 * @param accountId
	 * @return
	 */
	public List<Results> listDailyAccountResult(int year, int month, String accountId) {
		return resultsDao.listDailyAccountResult(year, month, accountId);
	}

	/**
	 * 月のグループ別実績を集計する。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return MonthlyKindResults
	 */
	public MonthlyKindResults sumMonthlyKindResult(int year, int month, String userId, List<String> div1, String div2,
			String div3) {
		Integer yearMonth = DateUtils.toYearMonth(year, month);

		Calendar paramCal = Calendar.getInstance();
		paramCal.set(year, month - 1, 1, 0, 0, 0);
		paramCal.set(Calendar.MILLISECOND, 0);

		Date firstDate = paramCal.getTime();
		paramCal.add(Calendar.MONTH, 1);
		Date lastDate = paramCal.getTime();

		Date now = DateUtils.getDBDate();
		Calendar nowCal = Calendar.getInstance();
		nowCal.setTime(now);
		int nowYearMonth = DateUtils.toYearMonth(nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH) + 1);

		boolean currentMonth = firstDate.getTime() <= now.getTime() && now.getTime() < lastDate.getTime();

		MonthlyKindResults m = monthlyKindResultsDao.sum(year, month, userId, div1, div2, div3,
				yearMonth < nowYearMonth);
		if (m == null) {
			m = new MonthlyKindResults();
		}

		// 当月の場合は当日を足し込む
		MonthlyKindResults daily = null;
		if (currentMonth) {
			daily = resultsDao.sumKindResult(year, month, nowCal.get(Calendar.DATE), yearMonth, userId, div1, div2,
					div3, !currentMonth);

		}

		if (daily == null) {
			daily = new MonthlyKindResults();
		}

		m.setPcrSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getPcrSummerSales()), daily.getPcrSummerSales()));
		m.setPcrSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrSummerMargin()), daily.getPcrSummerMargin()));
		m.setPcrSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrSummerHqMargin()), daily.getPcrSummerHqMargin()));
		m.setPcrSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrSummerNumber()), daily.getPcrSummerNumber()));

		m.setPcrPureSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrPureSnowSales()), daily.getPcrPureSnowSales()));
		m.setPcrPureSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrPureSnowMargin()), daily.getPcrPureSnowMargin()));
		m.setPcrPureSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrPureSnowHqMargin()), daily.getPcrPureSnowHqMargin()));
		m.setPcrPureSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcrPureSnowNumber()), daily.getPcrPureSnowNumber()));

		m.setVanSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getVanSummerSales()), daily.getVanSummerSales()));
		m.setVanSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanSummerMargin()), daily.getVanSummerMargin()));
		m.setVanSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanSummerHqMargin()), daily.getVanSummerHqMargin()));
		m.setVanSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanSummerNumber()), daily.getVanSummerNumber()));

		m.setVanPureSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanPureSnowSales()), daily.getVanPureSnowSales()));
		m.setVanPureSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanPureSnowMargin()), daily.getVanPureSnowMargin()));
		m.setVanPureSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanPureSnowHqMargin()), daily.getVanPureSnowHqMargin()));
		m.setVanPureSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getVanPureSnowNumber()), daily.getVanPureSnowNumber()));

		m.setLtSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getLtSummerSales()), daily.getLtSummerSales()));
		m.setLtSummerMargin(MathUtils.add(MathUtils.toDefaultZero(m.getLtSummerMargin()), daily.getLtSummerMargin()));
		m.setLtSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getLtSummerHqMargin()), daily.getLtSummerHqMargin()));
		m.setLtSummerNumber(MathUtils.add(MathUtils.toDefaultZero(m.getLtSummerNumber()), daily.getLtSummerNumber()));

		m.setLtSnowSales(MathUtils.add(MathUtils.toDefaultZero(m.getLtSnowSales()), daily.getLtSnowSales()));
		m.setLtSnowMargin(MathUtils.add(MathUtils.toDefaultZero(m.getLtSnowMargin()), daily.getLtSnowMargin()));
		m.setLtSnowHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getLtSnowHqMargin()), daily.getLtSnowHqMargin()));
		m.setLtSnowNumber(MathUtils.add(MathUtils.toDefaultZero(m.getLtSnowNumber()), daily.getLtSnowNumber()));

		m.setLtAsSales(MathUtils.add(MathUtils.toDefaultZero(m.getLtAsSales()), daily.getLtAsSales()));
		m.setLtAsMargin(MathUtils.add(MathUtils.toDefaultZero(m.getLtAsMargin()), daily.getLtAsMargin()));
		m.setLtAsHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getLtAsHqMargin()), daily.getLtAsHqMargin()));
		m.setLtAsNumber(MathUtils.add(MathUtils.toDefaultZero(m.getLtAsNumber()), daily.getLtAsNumber()));

		m.setLtPureSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getLtPureSnowSales()), daily.getLtPureSnowSales()));
		m.setLtPureSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getLtPureSnowMargin()), daily.getLtPureSnowMargin()));
		m.setLtPureSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getLtPureSnowHqMargin()), daily.getLtPureSnowHqMargin()));
		m.setLtPureSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getLtPureSnowNumber()), daily.getLtPureSnowNumber()));

		m.setTbSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getTbSummerSales()), daily.getTbSummerSales()));
		m.setTbSummerMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTbSummerMargin()), daily.getTbSummerMargin()));
		m.setTbSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getTbSummerHqMargin()), daily.getTbSummerHqMargin()));
		m.setTbSummerNumber(MathUtils.add(MathUtils.toDefaultZero(m.getTbSummerNumber()), daily.getTbSummerNumber()));

		m.setTbSnowSales(MathUtils.add(MathUtils.toDefaultZero(m.getTbSnowSales()), daily.getTbSnowSales()));
		m.setTbSnowMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTbSnowMargin()), daily.getTbSnowMargin()));
		m.setTbSnowHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTbSnowHqMargin()), daily.getTbSnowHqMargin()));
		m.setTbSnowNumber(MathUtils.add(MathUtils.toDefaultZero(m.getTbSnowNumber()), daily.getTbSnowNumber()));

		m.setTbAsSales(MathUtils.add(MathUtils.toDefaultZero(m.getTbAsSales()), daily.getTbAsSales()));
		m.setTbAsMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTbAsMargin()), daily.getTbAsMargin()));
		m.setTbAsHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTbAsHqMargin()), daily.getTbAsHqMargin()));
		m.setTbAsNumber(MathUtils.add(MathUtils.toDefaultZero(m.getTbAsNumber()), daily.getTbAsNumber()));

		m.setTbPureSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getTbPureSnowSales()), daily.getTbPureSnowSales()));
		m.setTbPureSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getTbPureSnowMargin()), daily.getTbPureSnowMargin()));
		m.setTbPureSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getTbPureSnowHqMargin()), daily.getTbPureSnowHqMargin()));
		m.setTbPureSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getTbPureSnowNumber()), daily.getTbPureSnowNumber()));

		m.setIdSales(MathUtils.add(MathUtils.toDefaultZero(m.getIdSales()), daily.getIdSales()));
		m.setIdMargin(MathUtils.add(MathUtils.toDefaultZero(m.getIdMargin()), daily.getIdMargin()));
		m.setIdHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getIdHqMargin()), daily.getIdHqMargin()));
		m.setIdNumber(MathUtils.add(MathUtils.toDefaultZero(m.getIdNumber()), daily.getIdNumber()));

		m.setOrSales(MathUtils.add(MathUtils.toDefaultZero(m.getOrSales()), daily.getOrSales()));
		m.setOrMargin(MathUtils.add(MathUtils.toDefaultZero(m.getOrMargin()), daily.getOrMargin()));
		m.setOrHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getOrHqMargin()), daily.getOrHqMargin()));
		m.setOrNumber(MathUtils.add(MathUtils.toDefaultZero(m.getOrNumber()), daily.getOrNumber()));

		m.setTifuSales(MathUtils.add(MathUtils.toDefaultZero(m.getTifuSales()), daily.getTifuSales()));
		m.setTifuMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTifuMargin()), daily.getTifuMargin()));
		m.setTifuHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getTifuHqMargin()), daily.getTifuHqMargin()));
		m.setTifuNumber(MathUtils.add(MathUtils.toDefaultZero(m.getTifuNumber()), daily.getTifuNumber()));

		m.setOtherSales(MathUtils.add(MathUtils.toDefaultZero(m.getOtherSales()), daily.getOtherSales()));
		m.setOtherMargin(MathUtils.add(MathUtils.toDefaultZero(m.getOtherMargin()), daily.getOtherMargin()));
		m.setOtherHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getOtherHqMargin()), daily.getOtherHqMargin()));
		// monthlyKindResults.setOthreNumber(MathUtils.add(monthlyKindResults.getOthreNumber(),
		// kindMarketTotalDto.getOthreNumber()));

		m.setWorkSales(MathUtils.add(MathUtils.toDefaultZero(m.getWorkSales()), daily.getWorkSales()));
		m.setWorkMargin(MathUtils.add(MathUtils.toDefaultZero(m.getWorkMargin()), daily.getWorkMargin()));
		m.setWorkHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getWorkHqMargin()), daily.getWorkHqMargin()));
		// monthlyKindResults.setworkNumber(MathUtils.add(monthlyKindResults.getworkNumber(),
		// kindMarketTotalDto.getworkNumber()));

		m.setPcbSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getPcbSummerSales()), daily.getPcbSummerSales()));
		m.setPcbSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbSummerMargin()), daily.getPcbSummerMargin()));
		m.setPcbSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbSummerHqMargin()), daily.getPcbSummerHqMargin()));
		m.setPcbSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbSummerNumber()), daily.getPcbSummerNumber()));

		m.setPcbPureSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbPureSnowSales()), daily.getPcbPureSnowSales()));
		m.setPcbPureSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbPureSnowMargin()), daily.getPcbPureSnowMargin()));
		m.setPcbPureSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbPureSnowHqMargin()), daily.getPcbPureSnowHqMargin()));
		m.setPcbPureSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getPcbPureSnowNumber()), daily.getPcbPureSnowNumber()));

		m.setRetreadSales(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadSales()), daily.getRetreadSales()));
		m.setRetreadMargin(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadMargin()), daily.getRetreadMargin()));
		m.setRetreadHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadHqMargin()), daily.getRetreadHqMargin()));
		m.setRetreadNumber(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadNumber()), daily.getRetreadNumber()));

		m.setRetreadLtSummerSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSummerSales()), daily.getRetreadLtSummerSales()));
		m.setRetreadLtSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSummerMargin()), daily.getRetreadLtSummerMargin()));
		m.setRetreadLtSummerHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSummerHqMargin()),
				daily.getRetreadLtSummerHqMargin()));
		m.setRetreadLtSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSummerNumber()), daily.getRetreadLtSummerNumber()));

		m.setRetreadTbSummerSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSummerSales()), daily.getRetreadTbSummerSales()));
		m.setRetreadTbSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSummerMargin()), daily.getRetreadTbSummerMargin()));
		m.setRetreadTbSummerHqMargin(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSummerHqMargin()),
				daily.getRetreadTbSummerHqMargin()));
		m.setRetreadTbSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSummerNumber()), daily.getRetreadTbSummerNumber()));

		m.setRetreadSpecialTireSummerSales(MathUtils.add(MathUtils.toDefaultZero(m.getRetreadSpecialTireSummerSales()),
				daily.getRetreadSpecialTireSummerSales()));
		m.setRetreadSpecialTireSummerMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadSpecialTireSummerMargin()),
						daily.getRetreadSpecialTireSummerMargin()));
		m.setRetreadSpecialTireSummerHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadSpecialTireSummerHqMargin()),
						daily.getRetreadSpecialTireSummerHqMargin()));
		m.setRetreadSpecialTireSummerNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadSpecialTireSummerNumber()),
						daily.getRetreadSpecialTireSummerNumber()));

		m.setRetreadLtSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSnowSales()), daily.getRetreadLtSnowSales()));
		m.setRetreadLtSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSnowMargin()), daily.getRetreadLtSnowMargin()));
		m.setRetreadLtSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSnowHqMargin()), daily.getRetreadLtSnowHqMargin()));
		m.setRetreadLtSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadLtSnowNumber()), daily.getRetreadLtSnowNumber()));

		m.setRetreadTbSnowSales(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSnowSales()), daily.getRetreadTbSnowSales()));
		m.setRetreadTbSnowMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSnowMargin()), daily.getRetreadTbSnowMargin()));
		m.setRetreadTbSnowHqMargin(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSnowHqMargin()), daily.getRetreadTbSnowHqMargin()));
		m.setRetreadTbSnowNumber(
				MathUtils.add(MathUtils.toDefaultZero(m.getRetreadTbSnowNumber()), daily.getRetreadTbSnowNumber()));

		return m;
	}

	/**
	 * 指定した年月の実績を集計します。
	 * 
	 * @param year
	 *            年
	 * @param month
	 *            月
	 * @param userId
	 *            ユーザID
	 * @param div1
	 *            販社コードリスト
	 * @param div2
	 *            部門コード
	 * @param div3
	 *            営業所コード
	 * @param group
	 *            グルーピング
	 * @return DTOリスト
	 */
	public List<AccountKindResultDto> sumMonthlyAccountKindResult(int year, int month, String userId, List<String> div1,
			String div2, String div3, String group) {

		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return resultsDao.sumAccountKindResult(year, month, null, yearMonth, userId, div1, div2, div3, group,
				isHistory);
	}

	/**
	 * 指定した年月日の実績を集計します。
	 * 
	 * @param year
	 *            年
	 * @param month
	 *            月
	 * @param date
	 *            日
	 * @param userId
	 *            ユーザID
	 * @param div1
	 *            販社コードリスト
	 * @param div2
	 *            部門コード
	 * @param div3
	 *            営業所コード
	 * @param group
	 *            グルーピング
	 * @return DTOリスト
	 */
	public List<AccountKindResultDto> sumDailyAccountKindResult(int year, int month, int date, String userId,
			List<String> div1, String div2, String div3, String group) {

		boolean isHistory = DateUtils.isPastYearMonth(year, month);
		Integer yearMonth = DateUtils.toYearMonth(year, month);
		return resultsDao.sumAccountKindResult(year, month, date, yearMonth, userId, div1, div2, div3, group,
				isHistory);
	}

	public List<CompassControlResultDto> sumDailyCompassControl(int year, int month, int date, String userId, List<String> div1,
			String div2, String div3) {

		return resultsDao.sumCompassControl(year, month, date, userId, div1, div2, div3);
	}

	public List<CompassControlResultDto> sumMonthlyCompassControl(int year, int month, String userId, List<String> div1, String div2,
			String div3) {

		return resultsDao.sumCompassControl(year, month, null, userId, div1, div2, div3);
	}

}
