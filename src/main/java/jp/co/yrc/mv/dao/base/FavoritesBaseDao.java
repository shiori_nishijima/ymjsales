package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Favorites;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface FavoritesBaseDao {

    /**
     * @param id
     * @return the Favorites entity
     */
    @Select
    Favorites selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Favorites entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Favorites entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Favorites entity);
}