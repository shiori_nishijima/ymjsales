package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MstEigyoSosikiHistory;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MstEigyoSosikiHistoryBaseDao {

    /**
     * @param eigyoSosikiCd
     * @param year
     * @param month
     * @return the MstEigyoSosikiHistory entity
     */
    @Select
    MstEigyoSosikiHistory selectById(String eigyoSosikiCd, Integer year, Integer month);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MstEigyoSosikiHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MstEigyoSosikiHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MstEigyoSosikiHistory entity);
}