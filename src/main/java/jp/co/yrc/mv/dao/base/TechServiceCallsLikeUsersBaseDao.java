package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCallsLikeUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCallsLikeUsersBaseDao {

    /**
     * @param id
     * @return the TechServiceCallsLikeUsers entity
     */
    @Select
    TechServiceCallsLikeUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCallsLikeUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCallsLikeUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCallsLikeUsers entity);
}