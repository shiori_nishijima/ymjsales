package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OppSalessup;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OppSalessupBaseDao {

    /**
     * @param id
     * @return the OppSalessup entity
     */
    @Select
    OppSalessup selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OppSalessup entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OppSalessup entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OppSalessup entity);
}