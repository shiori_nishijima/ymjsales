package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OpportunitiesCstm;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OpportunitiesCstmBaseDao {

    /**
     * @param idC
     * @return the OpportunitiesCstm entity
     */
    @Select
    OpportunitiesCstm selectById(String idC);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OpportunitiesCstm entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OpportunitiesCstm entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OpportunitiesCstm entity);
}