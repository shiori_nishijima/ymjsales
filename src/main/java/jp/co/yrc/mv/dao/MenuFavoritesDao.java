package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.BatchInsert;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Select;

import jp.co.yrc.mv.entity.MenuFavorites;
import jp.co.yrc.mv.framework.AppConfig;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MenuFavoritesDao extends jp.co.yrc.mv.dao.base.MenuFavoritesBaseDao {

	@Select
	List<MenuFavorites> listByUserId(String userId);

	@BatchInsert
	int[] insert(List<MenuFavorites> entities);

	@Delete(sqlFile = true)
	int deleteByUserId(String userId);

}
