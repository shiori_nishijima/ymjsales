package jp.co.yrc.mv.dao;

import jp.co.yrc.mv.dto.MarketInfoDto;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MarketInfosDao extends jp.co.yrc.mv.dao.base.MarketInfosBaseDao {

	@Select
	MarketInfoDto selectByCallId(String callId, boolean tech, String locale);

}