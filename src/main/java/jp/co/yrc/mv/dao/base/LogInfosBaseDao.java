package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.LogInfos;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface LogInfosBaseDao {

    /**
     * @param id
     * @return the LogInfos entity
     */
    @Select
    LogInfos selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(LogInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(LogInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(LogInfos entity);
}