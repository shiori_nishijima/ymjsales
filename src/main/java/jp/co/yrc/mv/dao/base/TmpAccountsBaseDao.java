package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpAccounts;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpAccountsBaseDao {

    /**
     * @param id
     * @return the TmpAccounts entity
     */
    @Select
    TmpAccounts selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpAccounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpAccounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpAccounts entity);
}