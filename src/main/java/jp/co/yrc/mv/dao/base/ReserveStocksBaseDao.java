package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.ReserveStocks;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface ReserveStocksBaseDao {

    /**
     * @param id
     * @return the ReserveStocks entity
     */
    @Select
    ReserveStocks selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ReserveStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ReserveStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ReserveStocks entity);
}