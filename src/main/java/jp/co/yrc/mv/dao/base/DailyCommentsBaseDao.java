package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.DailyComments;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface DailyCommentsBaseDao {

    /**
     * @param id
     * @return the DailyComments entity
     */
    @Select
    DailyComments selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(DailyComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(DailyComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(DailyComments entity);
}