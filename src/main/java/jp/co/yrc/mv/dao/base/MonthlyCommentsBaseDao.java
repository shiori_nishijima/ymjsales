package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MonthlyComments;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MonthlyCommentsBaseDao {

    /**
     * @param id
     * @return the MonthlyComments entity
     */
    @Select
    MonthlyComments selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MonthlyComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MonthlyComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MonthlyComments entity);
}