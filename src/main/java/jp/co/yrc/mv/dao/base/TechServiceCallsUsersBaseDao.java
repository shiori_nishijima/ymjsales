package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCallsUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCallsUsersBaseDao {

    /**
     * @param id
     * @return the TechServiceCallsUsers entity
     */
    @Select
    TechServiceCallsUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCallsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCallsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCallsUsers entity);
}