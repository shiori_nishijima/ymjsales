package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MonthlyKindResultsBaseDao {

    /**
     * @param year
     * @param month
     * @param userId
     * @param eigyoSosikiCd
     * @return the MonthlyKindResults entity
     */
    @Select
    MonthlyKindResults selectById(Integer year, Integer month, String userId, String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MonthlyKindResults entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MonthlyKindResults entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MonthlyKindResults entity);
}