package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Brands;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface BrandsBaseDao {

    /**
     * @param id
     * @return the Brands entity
     */
    @Select
    Brands selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Brands entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Brands entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Brands entity);
}