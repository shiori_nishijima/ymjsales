package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpAccountsUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpAccountsUsersBaseDao {

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpAccountsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpAccountsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpAccountsUsers entity);
}