package jp.co.yrc.mv.dao;

import java.util.Date;
import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.jdbc.IterationCallback;

import jp.co.yrc.mv.dto.LogInfosDto;
import jp.co.yrc.mv.dto.YearMonthDto;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface LogInfosDao extends jp.co.yrc.mv.dao.base.LogInfosBaseDao {

	@Select(iterate = true)
	public List<LogInfosDto> selectAccessLog(Date start, Date end, List<String> div1, String div2, String div3,
			String userId, int todayYear, int todayMonth, List<YearMonthDto> terms,
			IterationCallback<List<LogInfosDto>, LogInfosDto> callback);

}