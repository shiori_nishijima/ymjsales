package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCalls;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCallsBaseDao {

    /**
     * @param id
     * @return the TechServiceCalls entity
     */
    @Select
    TechServiceCalls selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCalls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCalls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCalls entity);
}