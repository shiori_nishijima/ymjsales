package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dto.CompassControlResultDto;
import jp.co.yrc.mv.entity.Plans;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface PlansDao extends jp.co.yrc.mv.dao.base.PlansBaseDao {

	@Select
	Plans sumUserPlan(Integer year, Integer month, String userId, List<String> div1, String div2, String div3);
	
	@Select
	List<CompassControlResultDto> sumCompassControl(Integer year, Integer month, String userId, List<String> div1, String div2, String div3);
}