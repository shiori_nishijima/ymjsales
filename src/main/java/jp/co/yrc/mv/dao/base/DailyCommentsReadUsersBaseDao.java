package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.DailyCommentsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface DailyCommentsReadUsersBaseDao {

    /**
     * @param id
     * @return the DailyCommentsReadUsers entity
     */
    @Select
    DailyCommentsReadUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(DailyCommentsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(DailyCommentsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(DailyCommentsReadUsers entity);
}