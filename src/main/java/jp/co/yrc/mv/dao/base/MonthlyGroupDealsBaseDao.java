package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MonthlyGroupDeals;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MonthlyGroupDealsBaseDao {

    /**
     * @param year
     * @param month
     * @param userId
     * @param eigyoSosikiCd
     * @return the MonthlyGroupDeals entity
     */
    @Select
    MonthlyGroupDeals selectById(Integer year, Integer month, String userId, String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MonthlyGroupDeals entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MonthlyGroupDeals entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MonthlyGroupDeals entity);
}