package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.AccountsComments;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface AccountsCommentsBaseDao {

    /**
     * @param id
     * @return the AccountsComments entity
     */
    @Select
    AccountsComments selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(AccountsComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(AccountsComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(AccountsComments entity);
}