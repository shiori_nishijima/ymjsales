package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpProducts;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpProductsBaseDao {

    /**
     * @param id
     * @return the TmpProducts entity
     */
    @Select
    TmpProducts selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpProducts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpProducts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpProducts entity);
}