package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpBrands;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpBrandsBaseDao {

    /**
     * @param id
     * @return the TmpBrands entity
     */
    @Select
    TmpBrands selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpBrands entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpBrands entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpBrands entity);
}