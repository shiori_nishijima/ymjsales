package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpUsersBaseDao {

    /**
     * @param id
     * @return the TmpUsers entity
     */
    @Select
    TmpUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpUsers entity);
}