package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SelectionItemsSecondLevel;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SelectionItemsSecondLevelBaseDao {

    /**
     * @param keyEntityName
     * @param keyPropertyName
     * @param keyValue
     * @param propertyName
     * @param value
     * @return the SelectionItemsSecondLevel entity
     */
    @Select
    SelectionItemsSecondLevel selectById(String keyEntityName, String keyPropertyName, String keyValue, String propertyName, String value, String keyLocale);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SelectionItemsSecondLevel entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SelectionItemsSecondLevel entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SelectionItemsSecondLevel entity);
}