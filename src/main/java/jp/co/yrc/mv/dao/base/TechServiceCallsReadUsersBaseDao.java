package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCallsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCallsReadUsersBaseDao {

    /**
     * @param id
     * @return the TechServiceCallsReadUsers entity
     */
    @Select
    TechServiceCallsReadUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCallsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCallsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCallsReadUsers entity);
}