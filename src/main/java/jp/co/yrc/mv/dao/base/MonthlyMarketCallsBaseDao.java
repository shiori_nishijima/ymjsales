package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MonthlyMarketCalls;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MonthlyMarketCallsBaseDao {

    /**
     * @param year
     * @param month
     * @param userId
     * @param eigyoSosikiCd
     * @return the MonthlyMarketCalls entity
     */
    @Select
    MonthlyMarketCalls selectById(Integer year, Integer month, String userId, String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MonthlyMarketCalls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MonthlyMarketCalls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MonthlyMarketCalls entity);
}