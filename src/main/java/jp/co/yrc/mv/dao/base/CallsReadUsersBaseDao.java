package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.CallsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CallsReadUsersBaseDao {

    /**
     * @param id
     * @return the CallsReadUsers entity
     */
    @Select
    CallsReadUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(CallsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(CallsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(CallsReadUsers entity);
}