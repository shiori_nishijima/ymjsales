package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Users;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface UsersBaseDao {

    /**
     * @param id
     * @return the Users entity
     */
    @Select
    Users selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Users entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Users entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Users entity);
}