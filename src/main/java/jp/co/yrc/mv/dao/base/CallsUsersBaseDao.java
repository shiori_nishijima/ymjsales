package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.CallsUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CallsUsersBaseDao {

    /**
     * @param id
     * @return the CallsUsers entity
     */
    @Select
    CallsUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(CallsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(CallsUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(CallsUsers entity);
}