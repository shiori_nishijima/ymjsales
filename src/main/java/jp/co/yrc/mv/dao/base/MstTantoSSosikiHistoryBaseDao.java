package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MstTantoSSosikiHistory;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MstTantoSSosikiHistoryBaseDao {

    /**
     * @param tantoCd
     * @param eigyoSosikiCd
     * @param year
     * @param month
     * @return the MstTantoSSosikiHistory entity
     */
    @Select
    MstTantoSSosikiHistory selectById(String tantoCd, String eigyoSosikiCd, Integer year, Integer month);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MstTantoSSosikiHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MstTantoSSosikiHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MstTantoSSosikiHistory entity);
}