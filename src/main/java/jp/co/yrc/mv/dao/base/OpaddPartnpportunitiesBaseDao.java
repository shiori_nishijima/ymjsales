package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OpaddPartnpportunities;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OpaddPartnpportunitiesBaseDao {

    /**
     * @param id
     * @return the OpaddPartnpportunities entity
     */
    @Select
    OpaddPartnpportunities selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OpaddPartnpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OpaddPartnpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OpaddPartnpportunities entity);
}