package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCommentsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCommentsReadUsersBaseDao {

    /**
     * @param id
     * @return the TechServiceCommentsReadUsers entity
     */
    @Select
    TechServiceCommentsReadUsers selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCommentsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCommentsReadUsers entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCommentsReadUsers entity);
}