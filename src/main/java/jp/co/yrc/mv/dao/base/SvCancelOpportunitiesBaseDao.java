package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SvCancelOpportunities;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SvCancelOpportunitiesBaseDao {

    /**
     * @param id
     * @return the SvCancelOpportunities entity
     */
    @Select
    SvCancelOpportunities selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SvCancelOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SvCancelOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SvCancelOpportunities entity);
}