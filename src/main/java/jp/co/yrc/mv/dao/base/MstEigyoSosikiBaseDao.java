package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MstEigyoSosikiBaseDao {

    /**
     * @param eigyoSosikiCd
     * @return the MstEigyoSosiki entity
     */
    @Select
    MstEigyoSosiki selectById(String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MstEigyoSosiki entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MstEigyoSosiki entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MstEigyoSosiki entity);
}