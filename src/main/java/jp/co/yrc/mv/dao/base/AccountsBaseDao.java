package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Accounts;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface AccountsBaseDao {

    /**
     * @param id
     * @return the Accounts entity
     */
    @Select
    Accounts selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Accounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Accounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Accounts entity);
}