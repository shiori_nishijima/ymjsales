package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceComments;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCommentsBaseDao {

    /**
     * @param id
     * @return the TechServiceComments entity
     */
    @Select
    TechServiceComments selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceComments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceComments entity);
}