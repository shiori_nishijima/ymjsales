package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MastBilliard;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MastBilliardBaseDao {

    /**
     * @param id
     * @return the MastBilliard entity
     */
    @Select
    MastBilliard selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MastBilliard entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MastBilliard entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MastBilliard entity);
}