package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.entity.SelectionItemsSecondLevel;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface SelectionItemsSecondLevelDao extends jp.co.yrc.mv.dao.base.SelectionItemsSecondLevelBaseDao {

	@Select
	List<SelectionItemsSecondLevel> findByEntity(SelectionItemsSecondLevel entity, String keyLocale);
}