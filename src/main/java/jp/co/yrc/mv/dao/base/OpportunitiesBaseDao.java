package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Opportunities;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OpportunitiesBaseDao {

    /**
     * @param id
     * @return the Opportunities entity
     */
    @Select
    Opportunities selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Opportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Opportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Opportunities entity);
}