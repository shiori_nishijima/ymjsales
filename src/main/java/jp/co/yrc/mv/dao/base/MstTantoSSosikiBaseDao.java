package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MstTantoSSosiki;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MstTantoSSosikiBaseDao {

    /**
     * @param tantoCd
     * @param eigyoSosikiCd
     * @return the MstTantoSSosiki entity
     */
    @Select
    MstTantoSSosiki selectById(String tantoCd, String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MstTantoSSosiki entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MstTantoSSosiki entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MstTantoSSosiki entity);
}