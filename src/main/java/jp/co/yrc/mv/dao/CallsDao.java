package jp.co.yrc.mv.dao;

import java.util.Date;
import java.util.List;

import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.CallsSearchConditionDto;
import jp.co.yrc.mv.dto.MinMaxDateDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerAccountDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerSalesDto;
import jp.co.yrc.mv.dto.UnvisitAccountDto;
import jp.co.yrc.mv.dto.WeekAccountCallDto;
import jp.co.yrc.mv.entity.Calls;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CallsDao extends jp.co.yrc.mv.dao.base.CallsBaseDao {

	/**
	 * 指定した年月の訪問リストを取得します。
	 * 担当外訪問は省いて検索します。
	 * 社内同行者が複数存在する場合は一行データに改行で紐づけて返却します。
	 * 
	 * @param yearMonth 年月
	 * @param from 訪問日From
	 * @param to 訪問日To
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者
	 * @param idForCustomer 得意先用ユーザID
	 * @param group グルーピング
	 * @param isHistory 過去検索かどうか
	 * @return DTOリスト
	 */
	@Select
	List<CallsDto> findMonthlyCalls(int yearMonth, Date from, Date to, List<String> div1, String div2, String div3, 
			String tanto, String idForCustomer, String group, boolean isHistory);
	
	/**
	 * 得意先ごとの訪問実績数を検索します。
	 * 担当外訪問は省いて検索します。
	 * 
	 * @param yearMonth 年月
	 * @param from 訪問日From
	 * @param to 訪問日To
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者
	 * @param group グルーピング
	 * @param isHistory 過去検索かどうか
	 * @return DTOリスト
	 */
	@Select
	List<CallsDto> findTantoCallCountByAccount(int yearMonth, Date from, Date to, List<String> div1, String div2, String div3, 
			String tanto, String group, boolean isHistory);	
	
	@Select
	List<Calls> listAccountHeldCall(Date from, Date to, String accountId, String userId);

	/**
	 * 組織は区別しない
	 * 
	 * @param yearMonth
	 * @param from
	 * @param to
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isDaily
	 * @param isHistory
	 * @return
	 */
	@Select
	List<CallCountInfoDto> sumUserCall(int yearMonth, Date from, Date to, String userId, String div1, String div2,
			String div3, boolean isDaily, boolean isHistory);

	/**
	 * 組織を区別する
	 * 
	 * @param yearMonth
	 * @param from
	 * @param to
	 * @param userId
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	@Select
	List<CallCountInfoDto> listDailyUserCallDistinctionSosoki(int yearMonth, Date from, Date to, String userId,
			String div1, String div2, String div3, boolean isHistory);

	@Select
	List<CallCountInfoDto> sumGroupCall(int yearMonth, Date from, Date to, String userId, List<String> div1,
			String div2, String div3, boolean isDaily, boolean isHistory);

	@Select
	List<CallsDto> findByCondition(CallsSearchConditionDto o);

	/**
	 * ロジック側でカウントするので使わない。
	 * 使う場合はSQLの修正要
	 * @param o
	 * @return
	 */
	@Deprecated
	@Select
	int countByCondition(CallsSearchConditionDto o);

	@Select
	CallsDto findCommentInfo(String callId, String userId, boolean tech);

	@Select
	List<AccountsDto> listOutsideAccount(int yearMonth, Date from, Date to, String userId, String div1, String div2,
			String div3, boolean isHistory);

	@Select
	MinMaxDateDto findCallsMinMaxDate();

	@Select
	List<UnvisitAccountDto> listUnvisitAccounts(int yearMonth, int year, int month, Date from, Date to, Date today,
			String userId, List<String> div1, String div2, String div3, boolean isHistory);

	@Select
	int countUnvisitAccounts(int yearMonth, int year, int month, Date from, Date to, String userId, List<String> div1,
			String div2, String div3, boolean isHistory);

	@Select
	List<CallCountInfoDto> sumAccountCall(int yearMonth, Date from, Date to, String userId, String div1, String div2,
			String div3, boolean isDaily, boolean isHistory, String marketId, String group, boolean canIgnoreSosikiCondition);

	@Select
	List<MonthVisitResultPerSalesDto> sumMonthVisitResultPerSales(int year, int month, int yearMonth, Date from,
			Date to, int yearLastMonth, Date lastMonthFrom, Date lastMonthTo, String div1, String div2, String div3, 
			List<SelectionItems> items, boolean isHistory);

	@Select
	List<MonthVisitResultPerAccountDto> sumMonthVisitResultPerAccount(int year, int month, int yearMonth, Date from,
			Date to, int yearLastMonth, Date lastMonthFrom, Date lastMonthTo, String div1, String div2, String div3,
			List<SelectionItems> items, boolean isHistory, String marketId, String group);

	@Select
	List<WeekAccountCallDto> sumWeekAccountCall(String userId, Date from, Date to);
	
	@Select
	List<CallsDto> findSensitivityCommentInfo(String userId, Date from, Date to);
}