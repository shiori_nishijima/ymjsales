package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.CompassKinds;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CompassKindsBaseDao {

    /**
     * @param id
     * @return the CompassKinds entity
     */
    @Select
    CompassKinds selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(CompassKinds entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(CompassKinds entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(CompassKinds entity);
}