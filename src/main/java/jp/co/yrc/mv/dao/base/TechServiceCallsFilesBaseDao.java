package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceCallsFiles;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceCallsFilesBaseDao {

    /**
     * @param id
     * @return the TechServiceCallsFiles entity
     */
    @Select
    TechServiceCallsFiles selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceCallsFiles entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceCallsFiles entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceCallsFiles entity);
}