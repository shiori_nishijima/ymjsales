package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.AccountsHistory;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface AccountsHistoryBaseDao {

    /**
     * @param id
     * @param year
     * @param month
     * @return the AccountsHistory entity
     */
    @Select
    AccountsHistory selectById(String id, Integer year, Integer month);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(AccountsHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(AccountsHistory entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(AccountsHistory entity);
}