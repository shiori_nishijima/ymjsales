package jp.co.yrc.mv.dao;

import java.util.Date;
import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dto.CallsUsersDto;
import jp.co.yrc.mv.dto.CallsUsersWithGroupInfoDto;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CallsUsersDao extends jp.co.yrc.mv.dao.base.CallsUsersBaseDao {

	@Select
	List<CallsUsersDto> findByCallId(String callId, boolean tech);

	@Select
	List<CallsUsersWithGroupInfoDto> countWithGroupInfo(int year, int month, int yearMonth, Date from, Date to,
			String div1, String div2, String div3, String userId, boolean isHistory);

}