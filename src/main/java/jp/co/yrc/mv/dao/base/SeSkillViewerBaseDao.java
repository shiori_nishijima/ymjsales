package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SeSkillViewer;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SeSkillViewerBaseDao {

    /**
     * @param id
     * @return the SeSkillViewer entity
     */
    @Select
    SeSkillViewer selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SeSkillViewer entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SeSkillViewer entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SeSkillViewer entity);
}