package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpTempForwardingStocks;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpTempForwardingStocksBaseDao {

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpTempForwardingStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpTempForwardingStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpTempForwardingStocks entity);
}