package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.Users;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface UsersDao extends jp.co.yrc.mv.dao.base.UsersBaseDao {

	/**
	 * ユーザを検索します。
	 * 
	 * @param id
	 * @return the Users entity
	 */
	@Select
	Users selectLoginUser(String id);

	/**
	 * 
	 * @param id
	 * @return the Users entity
	 */
	@Select
	UserSosikiDto selectUserByDivCd(String id, List<String> corpCodes, String div1, String div3, String div2, int year,
			int month, boolean isHistory);

	/**
	 * 
	 * @param id
	 * @return the Users entity
	 */
	@Select
	List<UserSosikiDto> listUser(String userId, String idForCustomer, List<String> div1, String div2, String div3,
			int year, int month, boolean isHistory);

	/**
	 * ログイン時のみ使用のため、履歴は見ない
	 * 
	 * @param id
	 * @return the Users entity
	 */
	@Select
	Users selectByIdForUpdate(String id);
}