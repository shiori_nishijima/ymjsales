package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dto.AccountKindResultDto;
import jp.co.yrc.mv.dto.CompassControlResultDto;
import jp.co.yrc.mv.dto.MinMaxDto;
import jp.co.yrc.mv.entity.MonthlyGroupResults;
import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.entity.Results;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface ResultsDao extends jp.co.yrc.mv.dao.base.ResultsBaseDao {

	@Select
	MinMaxDto findMinMaxYear();

	@Select
	MonthlyGroupResults sumGroupResult(Integer year, Integer month, Integer date, Integer yearMonth, String userId,
			List<String> div1, String div2, String div3, boolean isHistory);

	@Select
	MonthlyKindResults sumKindResult(Integer year, Integer month, Integer date, Integer yearMonth, String userId,
			List<String> div1, String div2, String div3, boolean isHistory);

	@Select
	List<AccountKindResultDto> sumAccountKindResult(Integer year, Integer month, Integer date, Integer yearMonth,
			String userId, List<String> div1, String div2, String div3, String group, boolean isHistory);

	@Select
	List<Results> sumMarketResult(Integer year, Integer month, Integer date, Integer yearMonth, String userId,
			List<String> div1, String div2, String div3, boolean isHistory);

	@Select
	List<Results> listDailyUserResult(Integer year, Integer month, Integer yearMonth, String userId, List<String> div1,
			String div2, String div3, boolean isHistory);

	@Select
	List<Results> listDailyAccountResult(Integer year, Integer month, String accountId);

	@Select
	List<CompassControlResultDto> sumCompassControl(Integer year, Integer month, Integer day, String userId,
			List<String> div1, String div2, String div3);

}