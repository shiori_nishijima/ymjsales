package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MastSkill;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MastSkillBaseDao {

    /**
     * @param id
     * @return the MastSkill entity
     */
    @Select
    MastSkill selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MastSkill entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MastSkill entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MastSkill entity);
}