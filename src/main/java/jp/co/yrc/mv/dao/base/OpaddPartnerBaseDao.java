package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OpaddPartner;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OpaddPartnerBaseDao {

    /**
     * @param id
     * @return the OpaddPartner entity
     */
    @Select
    OpaddPartner selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OpaddPartner entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OpaddPartner entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OpaddPartner entity);
}