package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Results;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface ResultsBaseDao {

    /**
     * @param year
     * @param month
     * @param date
     * @param salesCompanyCode
     * @param accountId
     * @param compassKind
     * @return the Results entity
     */
    @Select
    Results selectById(Integer year, Integer month, Integer date, String salesCompanyCode, String accountId, String compassKind);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Results entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Results entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Results entity);
}