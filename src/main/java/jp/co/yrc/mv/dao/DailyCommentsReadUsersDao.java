package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dao.base.DailyCommentsReadUsersBaseDao;
import jp.co.yrc.mv.entity.DailyCommentsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.BatchInsert;
import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface DailyCommentsReadUsersDao extends DailyCommentsReadUsersBaseDao {

	@Select
	int countByUserIdAndCommentId(String userId, String dailyCommentId);

	@BatchInsert
	int[] insert(List<DailyCommentsReadUsers> list);

}