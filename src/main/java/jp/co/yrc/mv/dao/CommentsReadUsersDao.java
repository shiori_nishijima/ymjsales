package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.entity.CommentsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.BatchDelete;
import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CommentsReadUsersDao extends jp.co.yrc.mv.dao.base.CommentsReadUsersBaseDao {

	@Select
	CommentsReadUsers findByCallIdAndUserId(String callId, String userId, boolean tech);

	@Select
	List<CommentsReadUsers> findByCallIdAndUserIdNot(String callId, String userId, boolean tech);

	@BatchDelete
	int[] delete(List<CommentsReadUsers> list);
}