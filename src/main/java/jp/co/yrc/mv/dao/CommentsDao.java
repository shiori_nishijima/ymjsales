package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dto.CommentsDto;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CommentsDao extends jp.co.yrc.mv.dao.base.CommentsBaseDao {

	@Select
	List<CommentsDto> listCallsComment(String callId, boolean tech);

	@Select
	int countNonConfirmationCallsComment(String callId, boolean tech);
}