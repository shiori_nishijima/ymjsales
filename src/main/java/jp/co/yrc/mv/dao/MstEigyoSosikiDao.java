package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MstEigyoSosikiDao extends jp.co.yrc.mv.dao.base.MstEigyoSosikiBaseDao {

	@Select
	List<EigyoSosikiDto> listSosikiTree(List<String> corpCodes, int year, int month, boolean isHistory);

	@Select
	EigyoSosikiDto findByDivCd(String div1, String div2, String div3, int year, int month, boolean isHistory);

	/**
	 * ユーザに紐づくdiv1リストを検索します。
	 * 
	 * @param userId ユーザID
	 * @return div1リスト
	 */
	@Select
	List<String> listDiv1ByUserId(String userId);

	@Select
	List<EigyoSosikiDto> listMySosikiTree(List<String> corpCodes, String userName, int year, int month,
			boolean isHistory);
	
	/**
	 * 現在の所属組織の過去情報を取得します。
	 * @param corpCodes
	 * @param userName
	 * @param year
	 * @param month
	 * @param isHistory
	 * @return
	 */
	@Select
	List<EigyoSosikiDto> listMySosikiTreeFromHistory(List<String> corpCodes, String userName, int year, int month,
			boolean isHistory);

	/**
	 * ログイン時のみに使用する。履歴対応しない
	 * 
	 * @return div1リスト
	 */
	@Select
	List<String> listDiv1();

	@Select
	List<MstEigyoSosiki> findByUserId(List<String> corpCodes, String userId, int year, int month, boolean isHistory);

	@Select
	List<MstEigyoSosiki> listCompany(List<String> div1, String div2, String div3, String userId,
			List<String> corpCodes, int year, int month, boolean isHistory);

	@Select
	List<MstEigyoSosiki> listBelongCompany(List<String> div1, String div2, List<String> corpCodes, int year, int month,
			boolean isHistory);
}