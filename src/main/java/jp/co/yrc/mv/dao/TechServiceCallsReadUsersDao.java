package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.entity.TechServiceCallsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.BatchInsert;
import org.seasar.doma.Dao;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface TechServiceCallsReadUsersDao extends jp.co.yrc.mv.dao.base.TechServiceCallsReadUsersBaseDao {

	@BatchInsert
	int[] insert(List<TechServiceCallsReadUsers> list);
}