package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Calls;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CallsBaseDao {

    /**
     * @param id
     * @return the Calls entity
     */
    @Select
    Calls selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Calls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Calls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Calls entity);
}