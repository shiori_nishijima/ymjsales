package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.CallsAccounts;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CallsAccountsBaseDao {

    /**
     * @param id
     * @return the CallsAccounts entity
     */
    @Select
    CallsAccounts selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(CallsAccounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(CallsAccounts entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(CallsAccounts entity);
}