package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.AccountsOpportunities;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface AccountsOpportunitiesBaseDao {

    /**
     * @param id
     * @return the AccountsOpportunities entity
     */
    @Select
    AccountsOpportunities selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(AccountsOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(AccountsOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(AccountsOpportunities entity);
}