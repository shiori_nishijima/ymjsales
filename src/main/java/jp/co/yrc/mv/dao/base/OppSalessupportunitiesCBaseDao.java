package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OppSalessupportunitiesC;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OppSalessupportunitiesCBaseDao {

    /**
     * @param id
     * @return the OppSalessupportunitiesC entity
     */
    @Select
    OppSalessupportunitiesC selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OppSalessupportunitiesC entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OppSalessupportunitiesC entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OppSalessupportunitiesC entity);
}