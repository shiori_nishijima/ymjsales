package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Comments;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CommentsBaseDao {

    /**
     * @param id
     * @return the Comments entity
     */
    @Select
    Comments selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Comments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Comments entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Comments entity);
}