package jp.co.yrc.mv.dao;

import java.util.Date;
import java.util.List;

import jp.co.yrc.mv.dao.base.DailyCommentsBaseDao;
import jp.co.yrc.mv.dto.DailyCommentDto;
import jp.co.yrc.mv.entity.DailyComments;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface DailyCommentsDao extends DailyCommentsBaseDao {

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Select
	DailyComments selectByIdNotDeleted(String id);

	/**
	 * 検索します。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @return
	 */
	@Select
	DailyCommentDto selectByCondition(int year, int month, int date, String userId);

	/**
	 * 日報を検索します。
	 * データが以下の状態の場合、日報として検索します。
	 * 1. 日報コメントあり、訪問あり
	 * 2. 日報コメントあり、訪問なし
	 * 3. 日報コメントなし、訪問あり
	 * 
	 * @param year 年
	 * @param month 月
	 * @param date 日
	 * @param from 検索日From
	 * @param to 検索日To
	 * @param div1　div1コード
	 * @param div2　div2コード
	 * @param div3　div3コード
	 * @param userId ユーザID
	 * @param loginUserId ログインユーザID
	 * @param isHistory 過去検索かどうか
	 * @return DailyCommentDto
	 */
	@Select
	List<DailyCommentDto> listCommentWithCallCount(int year, int month, int date, Date from, Date to,
			List<String> div1, String div2, String div3, String userId, String loginUserId, boolean isHistory);

	/**
	 * 日報を検索してカウントを取得します。
	 * データが以下の状態の場合、日報としてカウントします。
	 * 1. 日報コメントあり、訪問あり
	 * 2. 日報コメントあり、訪問なし
	 * 3. 日報コメントなし、訪問あり
	 * 
	 * @param year 年
	 * @param month 月
	 * @param date 日
	 * @param from 検索日From
	 * @param to 検索日To
	 * @param div1　div1コード
	 * @param div2　div2コード
	 * @param div3　div3コード
	 * @param userId ユーザID
	 * @param isHistory 過去検索かどうか
	 * @return DailyCommentDto
	 */
	@Select
	int countCommentWithCallCount(int year, int month, int date, Date from, Date to, List<String> div1,
			String div2, String div3, String userId, boolean isHistory);

	/**
	 * 検索します。 ユーザ氏名、営業組織名も同時に取得します。 複数所属している場合はいずれか1件を取得します。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @param corpCodes
	 * @return
	 */
	@Select
	DailyCommentDto selectByCorpCdAndUserId(int year, int month, int date, String userId, List<String> corpCodes,
			boolean isHistory);

	/**
	 * 検索します。
	 * 
	 * @param year
	 * @param month
	 * @param userId
	 * @return
	 */
	@Select
	List<DailyComments> listMonthlyComment(int year, int month, String userId);

	@Update(sqlFile = true)
	int updateBossComment(DailyComments o);
}