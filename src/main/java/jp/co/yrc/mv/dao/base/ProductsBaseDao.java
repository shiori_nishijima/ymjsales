package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Products;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface ProductsBaseDao {

    /**
     * @param id
     * @return the Products entity
     */
    @Select
    Products selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Products entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Products entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Products entity);
}