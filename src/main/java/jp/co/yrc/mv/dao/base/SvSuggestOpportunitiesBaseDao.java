package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SvSuggestOpportunities;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SvSuggestOpportunitiesBaseDao {

    /**
     * @param id
     * @return the SvSuggestOpportunities entity
     */
    @Select
    SvSuggestOpportunities selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SvSuggestOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SvSuggestOpportunities entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SvSuggestOpportunities entity);
}