package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dto.KindReserveStocksDto;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface ReserveStocksDao extends jp.co.yrc.mv.dao.base.ReserveStocksBaseDao {

	@Select
	List<KindReserveStocksDto> sum(String userId, List<String> div1, String div2, String div3);
}
