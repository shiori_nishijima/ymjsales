package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SvCancel;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SvCancelBaseDao {

    /**
     * @param id
     * @return the SvCancel entity
     */
    @Select
    SvCancel selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SvCancel entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SvCancel entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SvCancel entity);
}