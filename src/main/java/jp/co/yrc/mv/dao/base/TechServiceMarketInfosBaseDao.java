package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TechServiceMarketInfos;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TechServiceMarketInfosBaseDao {

    /**
     * @param id
     * @return the TechServiceMarketInfos entity
     */
    @Select
    TechServiceMarketInfos selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TechServiceMarketInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TechServiceMarketInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TechServiceMarketInfos entity);
}