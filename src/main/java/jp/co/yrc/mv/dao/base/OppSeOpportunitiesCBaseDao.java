package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OppSeOpportunitiesC;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OppSeOpportunitiesCBaseDao {

    /**
     * @param id
     * @return the OppSeOpportunitiesC entity
     */
    @Select
    OppSeOpportunitiesC selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OppSeOpportunitiesC entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OppSeOpportunitiesC entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OppSeOpportunitiesC entity);
}