package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.CallsReadUsers;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.BatchInsert;
import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CallsReadUsersDao extends jp.co.yrc.mv.dao.base.CallsReadUsersBaseDao {

	@Select
	int countByUserIdAndCallId(String userId, String callId, boolean tech);

	@Select
	List<UserSosikiDto> selectByUserIdAndCallId(String callId, boolean tech);

	@BatchInsert
	int[] insert(List<CallsReadUsers> list);
}