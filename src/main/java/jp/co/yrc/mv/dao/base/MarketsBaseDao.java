package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Markets;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MarketsBaseDao {

    /**
     * @param id
     * @return the Markets entity
     */
    @Select
    Markets selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Markets entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Markets entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Markets entity);
}