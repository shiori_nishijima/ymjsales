package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Fixtures;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface FixturesBaseDao {

    /**
     * @param id
     * @return the Fixtures entity
     */
    @Select
    Fixtures selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Fixtures entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Fixtures entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Fixtures entity);
}