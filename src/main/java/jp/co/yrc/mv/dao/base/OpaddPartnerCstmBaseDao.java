package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.OpaddPartnerCstm;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface OpaddPartnerCstmBaseDao {

    /**
     * @param idC
     * @return the OpaddPartnerCstm entity
     */
    @Select
    OpaddPartnerCstm selectById(String idC);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(OpaddPartnerCstm entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(OpaddPartnerCstm entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(OpaddPartnerCstm entity);
}