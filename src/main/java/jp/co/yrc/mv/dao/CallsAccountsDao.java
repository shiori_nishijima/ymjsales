package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dao.annotation.AutowireableDao;
import jp.co.yrc.mv.dao.base.CallsAccountsBaseDao;
import jp.co.yrc.mv.dto.CallsAccountsDto;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@AutowireableDao
public interface CallsAccountsDao extends CallsAccountsBaseDao {

	/**
	 * 検索します。
	 * 担当得意先の情報のみ検索します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param yearMonth 年月
	 * @param div1 販社コードリスト
	 * @param div2 部門コード
	 * @param div3 営業所コード
	 * @param tanto 担当者コード
	 * @param idForCustomer 得意先用ユーザID
	 * @param group グルーピング
	 * @param isHistory 過去検索かどうか
	 * @return DTOリスト
	 */
	@Select
	public List<CallsAccountsDto> findTantoCallsAccountList(int year, int month, int yearMonth, List<String> div1, String div2, String div3, 
			String tanto, String idForCustomer, String group, boolean isHistory);
	
	/**
	 * 検索します。
	 * 営業組織条件関係なく、担当者に紐づく全てを検索します。
	 * 
	 * 過去検索でも得意先履歴を利用しないのでグルーピングや営業組織など
	 * 変更のありうる項目を取得する場合には利用できません。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param tanto 担当者コード
	 * @return DTOリスト
	 */
	@Select
	public List<CallsAccountsDto> find(int year, int month, String tanto);
}