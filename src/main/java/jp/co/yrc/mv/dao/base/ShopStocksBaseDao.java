package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.ShopStocks;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface ShopStocksBaseDao {

    /**
     * @param id
     * @return the ShopStocks entity
     */
    @Select
    ShopStocks selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ShopStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ShopStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ShopStocks entity);
}