package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpMarkets;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpMarketsBaseDao {

    /**
     * @param id
     * @return the TmpMarkets entity
     */
    @Select
    TmpMarkets selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpMarkets entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpMarkets entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpMarkets entity);
}