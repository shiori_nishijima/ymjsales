package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.entity.Markets;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MarketsDao extends  jp.co.yrc.mv.dao.base.MarketsBaseDao {

	@Select
	public List<Markets> listAll();
}