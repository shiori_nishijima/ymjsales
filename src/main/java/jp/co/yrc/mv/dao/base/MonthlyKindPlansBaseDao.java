package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MonthlyKindPlans;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MonthlyKindPlansBaseDao {

    /**
     * @param year
     * @param month
     * @param userId
     * @param eigyoSosikiCd
     * @return the MonthlyKindPlans entity
     */
    @Select
    MonthlyKindPlans selectById(Integer year, Integer month, String userId, String eigyoSosikiCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MonthlyKindPlans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MonthlyKindPlans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MonthlyKindPlans entity);
}