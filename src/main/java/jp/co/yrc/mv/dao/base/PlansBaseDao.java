package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Plans;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface PlansBaseDao {

    /**
     * @param year
     * @param month
     * @param salesCompanyCode
     * @param departmentCode
     * @param salesOfficeCode
     * @param assignedUserId
     * @param sgaPlanTermDiv
     * @param productClassYrcDiv
     * @param largeClass1
     * @param largeClass2
     * @param middleClass
     * @param smallClass
     * @param productKindCd
     * @return the Plans entity
     */
    @Select
    Plans selectById(Integer year, Integer month, String salesCompanyCode, String departmentCode, String salesOfficeCode, String assignedUserId, String sgaPlanTermDiv, String productClassYrcDiv, String largeClass1, String largeClass2, String middleClass, String smallClass, String productKindCd);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Plans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Plans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Plans entity);
}