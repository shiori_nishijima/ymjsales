package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.ViewInfos;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface ViewInfosBaseDao {

    /**
     * @param url
     * @return the ViewInfos entity
     */
    @Select
    ViewInfos selectById(String url);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(ViewInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(ViewInfos entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(ViewInfos entity);
}