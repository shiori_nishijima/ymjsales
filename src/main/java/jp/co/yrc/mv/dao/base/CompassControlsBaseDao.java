package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.CompassControls;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface CompassControlsBaseDao {

    /**
     * @param year
     * @param month
     * @param reportDisplaySeq
     * @param compassKind
     * @return the CompassControls entity
     */
    @Select
    CompassControls selectById(Integer year, Integer month, Integer reportDisplaySeq, String compassKind);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(CompassControls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(CompassControls entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(CompassControls entity);
}