package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.entity.MonthlyGroupResults;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MonthlyGroupResultsDao extends jp.co.yrc.mv.dao.base.MonthlyGroupResultsBaseDao {

	@Select
	MonthlyGroupResults sum(Integer year, Integer month, String userId, List<String> div1, String div2, String div3,
			boolean isHistory);
}
