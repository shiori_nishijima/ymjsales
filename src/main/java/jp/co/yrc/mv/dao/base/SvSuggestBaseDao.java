package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SvSuggest;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SvSuggestBaseDao {

    /**
     * @param id
     * @return the SvSuggest entity
     */
    @Select
    SvSuggest selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SvSuggest entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SvSuggest entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SvSuggest entity);
}