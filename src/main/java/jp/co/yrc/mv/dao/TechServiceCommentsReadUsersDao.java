package jp.co.yrc.mv.dao;

import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface TechServiceCommentsReadUsersDao extends jp.co.yrc.mv.dao.base.TechServiceCommentsReadUsersBaseDao {

}