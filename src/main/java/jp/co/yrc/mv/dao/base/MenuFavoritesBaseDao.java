package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.MenuFavorites;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface MenuFavoritesBaseDao {

    /**
     * @param id
     * @return the MenuFavorites entity
     */
    @Select
    MenuFavorites selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(MenuFavorites entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(MenuFavorites entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(MenuFavorites entity);
}