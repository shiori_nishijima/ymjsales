package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Keymans;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface KeymansBaseDao {

    /**
     * @param id
     * @return the Keymans entity
     */
    @Select
    Keymans selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Keymans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Keymans entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Keymans entity);
}