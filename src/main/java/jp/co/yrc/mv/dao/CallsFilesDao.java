package jp.co.yrc.mv.dao;

import jp.co.yrc.mv.dao.annotation.AutowireableDao;
import jp.co.yrc.mv.entity.CallsFiles;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

@Dao(config = AppConfig.class)
@AutowireableDao
public interface CallsFilesDao extends jp.co.yrc.mv.dao.base.CallsFilesBaseDao {

	@Select
	CallsFiles selectFile1(String id, boolean tech);

	@Select
	CallsFiles selectFile2(String id, boolean tech);

	@Select
	CallsFiles selectFile3(String id, boolean tech);
}
