package jp.co.yrc.mv.dao;

import java.util.List;

import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.DemandDto;
import jp.co.yrc.mv.dto.GroupDealDto;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface AccountsDao extends jp.co.yrc.mv.dao.base.AccountsBaseDao {

	@Select
	List<DemandDto> sumGroupDemand(Integer yearMonth, List<String> div1, String div2, String div3, String userId,
			boolean isHistory);

	@Select
	GroupDealDto sumGroupDeal(Integer year, Integer month, Integer yearMonth, List<String> div1, String div2,
			String div3, String userId, boolean isHistory);

	@Select
	List<AccountsDto> listAssignedAccount(Integer year, Integer month, Integer yearMonth, String div1, String div2,
			String div3, String userId, boolean isHistory);

	@Select
	int countOwn(Integer yearMonth, List<String> div1, String div2, String div3, String userId, boolean isHistory);

	@Select
	AccountsDto findAccountInfoById(int yearMonth, String id, boolean isHistory);
}
