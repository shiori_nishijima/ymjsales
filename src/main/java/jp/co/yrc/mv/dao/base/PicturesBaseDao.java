package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.Pictures;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface PicturesBaseDao {

    /**
     * @param id
     * @return the Pictures entity
     */
    @Select
    Pictures selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(Pictures entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(Pictures entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(Pictures entity);
}