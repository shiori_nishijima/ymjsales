package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface SelectionItemsBaseDao {

    /**
     * @param entityName
     * @param propertyName
     * @param value
     * @return the SelectionItems entity
     */
    @Select
    SelectionItems selectById(String entityName, String propertyName, String value, String locale);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(SelectionItems entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(SelectionItems entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(SelectionItems entity);
}