package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dao.base.OpportunitiesBaseDao;
import jp.co.yrc.mv.dto.OpportunitiesDto;
import jp.co.yrc.mv.dto.OpportunitiesSearchConditionDto;
import jp.co.yrc.mv.framework.AppConfig;

@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface OpportunitiesDao extends OpportunitiesBaseDao {
	
	@Select
	List<OpportunitiesDto> findOpportunityList(OpportunitiesSearchConditionDto o);
}
