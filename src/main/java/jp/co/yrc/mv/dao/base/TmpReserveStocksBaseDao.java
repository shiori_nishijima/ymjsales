package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TmpReserveStocks;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TmpReserveStocksBaseDao {

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TmpReserveStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TmpReserveStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TmpReserveStocks entity);
}