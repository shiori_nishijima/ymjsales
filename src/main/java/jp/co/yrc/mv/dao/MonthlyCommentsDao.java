package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface MonthlyCommentsDao extends jp.co.yrc.mv.dao.base.MonthlyCommentsBaseDao {

	/**
	 * 検索します。
	 * @param year
	 * @param month
	 * @param userId
	 * @return
	 */
	@Select
	MonthlyCommentsDto selectByCondition(int year, int month, String userId);
	
	/**
	 * 検索します。
	 * ユーザ氏名、営業組織名も同時に取得します。
	 * 複数所属している場合はいずれか1件を取得します。
	 * @param year
	 * @param month
	 * @param userId
	 * @param corpCodes
	 * @return
	 */
	@Select
	MonthlyCommentsDto selectByCorpCdAndUserId(int year, int month, String userId, List<String> corpCodes,
			boolean isHistory);
}