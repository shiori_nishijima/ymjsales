package jp.co.yrc.mv.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

import jp.co.yrc.mv.entity.ViewInfos;
import jp.co.yrc.mv.framework.AppConfig;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface ViewInfosDao extends jp.co.yrc.mv.dao.base.ViewInfosBaseDao {

	@Select
	List<ViewInfos> listViewInfos(String url);
}