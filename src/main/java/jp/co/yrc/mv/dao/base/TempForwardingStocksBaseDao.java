package jp.co.yrc.mv.dao.base;

import jp.co.yrc.mv.entity.TempForwardingStocks;
import jp.co.yrc.mv.framework.AppConfig;
import org.seasar.doma.Dao;
import org.seasar.doma.Delete;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

/**
 */
@Dao(config = AppConfig.class)
public interface TempForwardingStocksBaseDao {

    /**
     * @param id
     * @return the TempForwardingStocks entity
     */
    @Select
    TempForwardingStocks selectById(String id);

    /**
     * @param entity
     * @return affected rows
     */
    @Insert
    int insert(TempForwardingStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Update
    int update(TempForwardingStocks entity);

    /**
     * @param entity
     * @return affected rows
     */
    @Delete
    int delete(TempForwardingStocks entity);
}