package jp.co.yrc.mv.dao;

import jp.co.yrc.mv.entity.CallsLikeUsers;
import jp.co.yrc.mv.framework.AppConfig;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;

/**
 */
@Dao(config = AppConfig.class)
@jp.co.yrc.mv.dao.annotation.AutowireableDao
public interface CallsLikeUsersDao extends jp.co.yrc.mv.dao.base.CallsLikeUsersBaseDao {

	@Select
	CallsLikeUsers findByCallIdAndUserId(String callId, String userId, boolean tech);
}