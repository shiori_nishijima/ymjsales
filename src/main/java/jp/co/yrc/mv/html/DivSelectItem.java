package jp.co.yrc.mv.html;

import java.util.List;

/**
 * 営業組織リスト生成用。
 * 
 */
public class DivSelectItem {
	public List<SelectItem> div1;
	public List<SelectItemGroup> div2;
	public List<SelectItemGroup> div3;
	public List<SelectItemGroup> tanto;
}

