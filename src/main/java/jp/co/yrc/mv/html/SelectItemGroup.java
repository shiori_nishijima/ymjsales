package jp.co.yrc.mv.html;

import java.util.ArrayList;
import java.util.List;

/**
 * SelectタグのGroupを表します。
 *
 */
public class SelectItemGroup extends SelectItem {

	private List<SelectItem> items = new ArrayList<SelectItem>();

	public SelectItemGroup(String label) {
		super(label, null);
	}

	public List<SelectItem> getItems() {
		return items;
	}

	public void setItems(List<SelectItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SelectItem other = (SelectItem) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}

}
