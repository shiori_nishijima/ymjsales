package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;
import static jp.co.yrc.mv.common.MathUtils.toK;
import static jp.co.yrc.mv.common.MathUtils.toPercentage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.ResultsType;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.dto.AccountKindResultDto;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectGroupingHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.MonthlyCommentsService;
import jp.co.yrc.mv.service.ResultsService;

@Controller
@Transactional
public class AccountActualReferenceController {

	@Autowired
	ResultsService resultsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SelectGroupingHelper selectGroupingHelper;

	@Autowired
	MonthlyCommentsService commentsService;

	@Autowired
	CallsService callsService;
	
	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/accountActualReference.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);
		param.setResultType(ResultsType.NUMBER.getValue());
		param.setSearchMode("real");

		selectYearsHelper.create(model);
		model.addAttribute(param);
		model.addAttribute(new DateConditionResult());
		selectGroupingHelper.createGroup(model);
		setResultType(model, param);
		return "accountActualReference";
	}

	/**
	 * 検索時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/accountActualReference.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {
		selectYearsHelper.create(model);
		selectGroupingHelper.createGroup(model);
		model.addAttribute(param);
		setResultType(model, param);
		List<Result> list = listResult(param, principal);
		model.addAttribute("list", list);
		model.addAttribute(createDateConditionResult(param));
		return "accountActualReference";
	}

	/**
	 * 検索条件をもとに年月日From、Toの文字列を保持するオブジェクトを生成します。
	 * 
	 * @param param パラメータ
	 * @return DateConditionResult
	 */
	protected DateConditionResult createDateConditionResult(SearchCondition param) {
		DateConditionResult result = new DateConditionResult();
		Calendar cal = DateUtils.getYearMonthCalendar(param.getYear(), param.getMonth());
		result.setDateFrom(Constants.DateFormat.yyyyMMdd.getFormat().format(cal.getTime()));
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		result.setDateTo(Constants.DateFormat.yyyyMMdd.getFormat().format(cal.getTime()));
		return result;
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @return 結果リスト
	 */
	protected List<Result> listResult(SearchCondition param, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		String group = selectGroupingHelper.allToNull(param.group);

		List<AccountKindResultDto> list = resultsService.sumMonthlyAccountKindResult(param.getYear(), param.getMonth(),
				tanto, Collections.singletonList(div1), div2, div3, group);

		// 需要本数計算用
		MonthlyCommentsDto commentsDto = commentsService.selectByCondition(param.getYear(), param.getMonth(), userDetailsHelper.getUserId(principal));

		Map<String, Result> map = new LinkedHashMap<>();
		for (AccountKindResultDto o : list) {
			Result r = createResult(o, commentsDto);
			map.put(o.getId(), r);
		}
		
		// 訪問回数
		List<CallsDto> callList = callsService.findMonthlyTantoCallCountByAccount(param.getYear(), param.getMonth(), 
				Collections.singletonList(div1), div2, div3, tanto, group);
		for (CallsDto call : callList) {
			Result r = map.get(call.getParentId());
			if (r != null) {
				r.setPlannedCount(call.getPlannedCount());
				r.setHeldCount(call.getHeldCount());
			}
		}

		boolean currentMonth = DateUtils.isCurrentYearMonth(param.getYear(), param.getMonth());
		if (currentMonth) {			
			boolean isToday = !"yesterday".equals(param.getSearchMode());

			// 本日分を検索
			Calendar nowCal = DateUtils.getCalendar();
			List<AccountKindResultDto> todayList = resultsService.sumDailyAccountKindResult(param.getYear(),
					param.getMonth(), nowCal.get(Calendar.DATE), tanto, Collections.singletonList(div1), div2, div3,
					group);

			for (AccountKindResultDto o : todayList) {
				Result r = map.get(o.getId());
				// 通常あり得ない
				if (r == null) {
					continue;
				}
				BigDecimal numberTodayTire = toDefaultZero(o.getPcrSummerNumber())
						.add(toDefaultZero(o.getPcrPureSnowNumber())).add(toDefaultZero(o.getVanSummerNumber()))
						.add(toDefaultZero(o.getVanPureSnowNumber())).add(toDefaultZero(o.getPcbSummerNumber()))
						.add(toDefaultZero(o.getPcbPureSnowNumber())).add(toDefaultZero(o.getLtSummerNumber()))
						.add(toDefaultZero(o.getLtSnowNumber())).add(toDefaultZero(o.getTbSummerNumber()))
						.add(toDefaultZero(o.getTbSnowNumber())).add(toDefaultZero(o.getIdNumber()))
						.add(toDefaultZero(o.getOrNumber()));
				BigDecimal salesTodayTire = toDefaultZero(o.getPcrSummerSales())
						.add(toDefaultZero(o.getPcrPureSnowSales())).add(toDefaultZero(o.getVanSummerSales()))
						.add(toDefaultZero(o.getVanPureSnowSales())).add(toDefaultZero(o.getPcbSummerSales()))
						.add(toDefaultZero(o.getPcbPureSnowSales())).add(toDefaultZero(o.getLtSummerSales()))
						.add(toDefaultZero(o.getLtSnowSales())).add(toDefaultZero(o.getTbSummerSales()))
						.add(toDefaultZero(o.getTbSnowSales())).add(toDefaultZero(o.getIdSales()))
						.add(toDefaultZero(o.getOrSales()));

				BigDecimal numberTodayPcr = toDefaultZero(o.getPcrSummerNumber()).add(
						toDefaultZero(o.getPcrPureSnowNumber()));
				BigDecimal salesTodayPcr = toDefaultZero(o.getPcrSummerSales()).add(
						toDefaultZero(o.getPcrPureSnowSales()));
				BigDecimal numberTodayTbs = toDefaultZero(o.getTbSummerNumber()).add(
						toDefaultZero(o.getTbSnowNumber()));
				BigDecimal salesTodayTbs = toDefaultZero(o.getTbSummerSales()).add(toDefaultZero(o.getTbSnowSales()));
				BigDecimal numberTodayVan = toDefaultZero(o.getVanSummerNumber()).add(
						toDefaultZero(o.getVanPureSnowNumber()));
				BigDecimal salesTodayVan = toDefaultZero(o.getVanSummerSales()).add(toDefaultZero(o.getVanPureSnowSales()));
				BigDecimal numberTodayLtr = toDefaultZero(o.getLtrSummerNumber()).add(
						toDefaultZero(o.getLtrSnowNumber()));
				BigDecimal salesTodayLtr = toDefaultZero(o.getLtrSummerSales()).add(toDefaultZero(o.getLtrSnowSales()));

				BigDecimal salesTodayTotal = toDefaultZero(o.getPcrSummerSales())
						.add(toDefaultZero(o.getPcrPureSnowSales()))
						.add(toDefaultZero(o.getVanSummerSales())).add(toDefaultZero(o.getVanPureSnowSales()))
						.add(toDefaultZero(o.getPcbSummerSales())).add(toDefaultZero(o.getPcbPureSnowSales()))
						.add(toDefaultZero(o.getLtSummerSales())).add(toDefaultZero(o.getLtSnowSales()))
						.add(toDefaultZero(o.getTbSummerSales())).add(toDefaultZero(o.getTbSnowSales()))
						.add(toDefaultZero(o.getIdSales())).add(toDefaultZero(o.getOrSales()))
						.add(toDefaultZero(o.getTifuSales())).add(toDefaultZero(o.getRetreadSales()))
						.add(toDefaultZero(o.getOtherSales())).add(toDefaultZero(o.getWorkSales()));

				if (isToday) {
					// 検索した値を当日の項目に設定
					r.getNumber().setTodayTire(numberTodayTire);
					r.getSales().setTodayTire(salesTodayTire);
					r.getNumber().setTodayPcr(numberTodayPcr);
					r.getSales().setTodayPcr(salesTodayPcr);
					r.getNumber().setTodayTbs(numberTodayTbs);
					r.getSales().setTodayTbs(salesTodayTbs);
					r.getNumber().setTodayVan(numberTodayVan);
					r.getSales().setTodayVan(salesTodayVan);
					r.getNumber().setTodayLtr(numberTodayLtr);
					r.getSales().setTodayLtr(salesTodayLtr);
					r.getSales().setTodayTotal(salesTodayTotal);

				} else {
					// 検索結果が月の集計のため、前日なら「月の集計-当日の集計」で前日までを求める。
					r.getNumber().setTire(r.getNumber().getTire().subtract(numberTodayTire));
					r.getSales().setTire(r.getSales().getTire().subtract(salesTodayTire));
					r.getNumber().setPcr(r.getNumber().getPcr().subtract(numberTodayPcr));
					r.getSales().setPcr(r.getSales().getPcr().subtract(salesTodayPcr));
					r.getNumber().setTbs(r.getNumber().getTbs().subtract(numberTodayTbs));
					r.getSales().setTbs(r.getSales().getTbs().subtract(salesTodayTbs));
					r.getNumber().setVan(r.getNumber().getVan().subtract(numberTodayVan));
					r.getSales().setVan(r.getSales().getVan().subtract(salesTodayVan));
					r.getNumber().setLtr(r.getNumber().getLtr().subtract(numberTodayLtr));
					r.getSales().setLtr(r.getSales().getLtr().subtract(salesTodayLtr));
					r.getSales().setTotal(r.getSales().getTotal().subtract(salesTodayTotal));
				}				
			}			
		}

		// 伸長率のために去年の値を求める
		List<AccountKindResultDto> lastYearList = resultsService.sumMonthlyAccountKindResult(param.getYear() - 1,
				param.getMonth(), tanto, Collections.singletonList(div1), div2, div3, group);
		for (AccountKindResultDto o : lastYearList) {
			Result r = map.get(o.getId());
			if (r == null) {
				continue;
			}
			setElongation(r, o);
		}

		List<Result> result = new ArrayList<>(map.values());

		// 金額を1000円単位に
		for (Result r : result) {
			r.getSales().setTodayTire(toK(r.getSales().getTodayTire()));
			r.getSales().setTodayPcr(toK(r.getSales().getTodayPcr()));
			r.getSales().setTodayTbs(toK(r.getSales().getTodayTbs()));
			r.getSales().setTodayVan(toK(r.getSales().getTodayVan()));
			r.getSales().setTodayLtr(toK(r.getSales().getTodayLtr()));
			r.getSales().setTodayTotal(toK(r.getSales().getTodayTotal()));
			r.getSales().setTire(toK(r.getSales().getTire()));
			r.getSales().setPcr(toK(r.getSales().getPcr()));
			r.getSales().setTbs(toK(r.getSales().getTbs()));
			r.getSales().setVan(toK(r.getSales().getVan()));
			r.getSales().setLtr(toK(r.getSales().getLtr()));
			r.getSales().setTotal(toK(r.getSales().getTotal()));
		}

		return result;
	}

	/**
	 * 結果を生成します。
	 * 
	 * @param o
	 * @param commentsDto
	 * @return 
	 */
	protected Result createResult(AccountKindResultDto o, MonthlyCommentsDto commentsDto) {
		Result r = new Result();
		r.setId(o.getId());
		r.setAccountName(o.getName());
		r.setMarketName(o.getMarketName());
		r.setRank(o.getSalesRank());
		r.setFirstName(o.getFirstName());
		r.setLastName(o.getLastName());

		BigDecimal planDemandCoefficient = commentsDto == null ? BigDecimal.ZERO : commentsDto.getPlanDemandCoefficient();
		if (planDemandCoefficient == null) {
			planDemandCoefficient = BigDecimal.ZERO;
		} else {
			planDemandCoefficient = planDemandCoefficient.divide(MathUtils.HUNDRED);
		}
		r.setDemandNumber(toDefaultZero(o.getDemandNumber()).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP));

		r.setGroupImportant(o.isGroupImportant());
		r.setGroupNew(o.isGroupNew());
		r.setGroupDeepPlowing(o.isGroupDeepPlowing());
		r.setGroupYCp(o.isGroupYCp());
		r.setGroupOtherCp(o.isGroupOtherCp());
		r.setGroupOne(o.isGroupOne());
		r.setGroupTwo(o.isGroupTwo());
		r.setGroupThree(o.isGroupThree());
		r.setGroupFour(o.isGroupFour());
		r.setGroupFive(o.isGroupFive());

		r.getNumber().setTire(
				toDefaultZero(o.getPcrSummerNumber()).add(toDefaultZero(o.getPcrPureSnowNumber()))
						.add(toDefaultZero(o.getVanSummerNumber())).add(toDefaultZero(o.getVanPureSnowNumber()))
						.add(toDefaultZero(o.getPcbSummerNumber())).add(toDefaultZero(o.getPcbPureSnowNumber()))
						.add(toDefaultZero(o.getLtSummerNumber())).add(toDefaultZero(o.getLtSnowNumber()))
						.add(toDefaultZero(o.getTbSummerNumber())).add(toDefaultZero(o.getTbSnowNumber()))
						.add(toDefaultZero(o.getIdNumber())).add(toDefaultZero(o.getOrNumber())));
		r.getSales().setTire(
				toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales()))
						.add(toDefaultZero(o.getVanSummerSales())).add(toDefaultZero(o.getVanPureSnowSales()))
						.add(toDefaultZero(o.getPcbSummerSales())).add(toDefaultZero(o.getPcbPureSnowSales()))
						.add(toDefaultZero(o.getLtSummerSales())).add(toDefaultZero(o.getLtSnowSales()))
						.add(toDefaultZero(o.getTbSummerSales())).add(toDefaultZero(o.getTbSnowSales()))
						.add(toDefaultZero(o.getIdSales())).add(toDefaultZero(o.getOrSales())));
		r.getNumber().setPcr(toDefaultZero(o.getPcrSummerNumber()).add(toDefaultZero(o.getPcrPureSnowNumber())));
		r.getSales().setPcr(toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales())));
		r.getNumber().setTbs(toDefaultZero(o.getTbSummerNumber()).add(toDefaultZero(o.getTbSnowNumber())));
		r.getSales().setTbs(toDefaultZero(o.getTbSummerSales()).add(toDefaultZero(o.getTbSnowSales())));
		r.getNumber().setVan(toDefaultZero(o.getVanSummerNumber()).add(toDefaultZero(o.getVanPureSnowNumber())));
		r.getSales().setVan(toDefaultZero(o.getVanSummerSales()).add(toDefaultZero(o.getVanPureSnowSales())));
		r.getNumber().setLtr(toDefaultZero(o.getLtrSummerNumber()).add(toDefaultZero(o.getLtrSnowNumber())));
		r.getSales().setLtr(toDefaultZero(o.getLtrSummerSales()).add(toDefaultZero(o.getLtrSnowSales())));

		r.setIss(toPercentage(r.getNumber().getTire(), o.getDemandNumber()));

		r.getSales().setTotal(
				toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales()))
					.add(toDefaultZero(o.getVanSummerSales())).add(toDefaultZero(o.getVanPureSnowSales()))
					.add(toDefaultZero(o.getPcbSummerSales())).add(toDefaultZero(o.getPcbPureSnowSales()))
					.add(toDefaultZero(o.getLtSummerSales())).add(toDefaultZero(o.getLtSnowSales()))
					.add(toDefaultZero(o.getTbSummerSales())).add(toDefaultZero(o.getTbSnowSales()))
					.add(toDefaultZero(o.getIdSales())).add(toDefaultZero(o.getOrSales()))
					.add(toDefaultZero(o.getTifuSales())).add(toDefaultZero(o.getRetreadSales()))
					.add(toDefaultZero(o.getOtherSales())).add(toDefaultZero(o.getWorkSales())));
		return r;
	}

	/**
	 * 伸長率を設定します。
	 * 
	 * @param r 結果
	 * @param o 検索結果DTO
	 */
	protected void setElongation(Result r, AccountKindResultDto o) {
		BigDecimal tireNumber = toDefaultZero(o.getPcrSummerNumber()).add(toDefaultZero(o.getPcrPureSnowNumber()))
				.add(toDefaultZero(o.getVanSummerNumber())).add(toDefaultZero(o.getVanPureSnowNumber()))
				.add(toDefaultZero(o.getPcbSummerNumber())).add(toDefaultZero(o.getPcbPureSnowNumber()))
				.add(toDefaultZero(o.getLtSummerNumber())).add(toDefaultZero(o.getLtSnowNumber()))
				.add(toDefaultZero(o.getTbSummerNumber())).add(toDefaultZero(o.getTbSnowNumber()))
				.add(toDefaultZero(o.getIdNumber())).add(toDefaultZero(o.getOrNumber()));
		BigDecimal tireSales = toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales()))
				.add(toDefaultZero(o.getVanSummerSales())).add(toDefaultZero(o.getVanPureSnowSales()))
				.add(toDefaultZero(o.getPcbSummerSales())).add(toDefaultZero(o.getPcbPureSnowSales()))
				.add(toDefaultZero(o.getLtSummerSales())).add(toDefaultZero(o.getLtSnowSales()))
				.add(toDefaultZero(o.getTbSummerSales())).add(toDefaultZero(o.getTbSnowSales()))
				.add(toDefaultZero(o.getIdSales())).add(toDefaultZero(o.getOrSales()));
		r.getNumber().setTireElongation(toPercentage(r.getNumber().getTire(), tireNumber));
		r.getSales().setTireElongation(toPercentage(r.getSales().getTire(), tireSales));

		BigDecimal pcrNumber = toDefaultZero(o.getPcrSummerNumber()).add(toDefaultZero(o.getPcrPureSnowNumber()));
		BigDecimal pcrSales = toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales()));
		r.getNumber().setPcrElongation(toPercentage(r.getNumber().getPcr(), pcrNumber));
		r.getSales().setPcrElongation(toPercentage(r.getSales().getPcr(), pcrSales));

		BigDecimal tbsNumber = toDefaultZero(o.getTbSummerNumber()).add(toDefaultZero(o.getTbSnowNumber()));
		BigDecimal tbsSales = toDefaultZero(o.getTbSummerSales()).add(toDefaultZero(o.getTbSnowSales()));
		r.getNumber().setTbsElongation(toPercentage(r.getNumber().getTbs(), tbsNumber));
		r.getSales().setTbsElongation(toPercentage(r.getSales().getTbs(), tbsSales));

		BigDecimal vanNumber = toDefaultZero(o.getVanSummerNumber()).add(toDefaultZero(o.getVanPureSnowNumber()));
		BigDecimal vanSales = toDefaultZero(o.getVanSummerSales()).add(toDefaultZero(o.getVanPureSnowSales()));
		r.getNumber().setVanElongation(toPercentage(r.getNumber().getVan(), vanNumber));
		r.getSales().setVanElongation(toPercentage(r.getSales().getVan(), vanSales));

		BigDecimal ltrNumber = toDefaultZero(o.getLtrSummerNumber()).add(toDefaultZero(o.getLtrSnowNumber()));
		BigDecimal ltrSales = toDefaultZero(o.getLtrSummerSales()).add(toDefaultZero(o.getLtrSnowSales()));
		r.getNumber().setLtrElongation(toPercentage(r.getNumber().getLtr(), ltrNumber));
		r.getSales().setLtrElongation(toPercentage(r.getSales().getLtr(), ltrSales));

		BigDecimal totalSales = toDefaultZero(
				toDefaultZero(o.getPcrSummerSales()).add(toDefaultZero(o.getPcrPureSnowSales()))
				.add(toDefaultZero(o.getVanSummerSales())).add(toDefaultZero(o.getVanPureSnowSales()))
				.add(toDefaultZero(o.getPcbSummerSales())).add(toDefaultZero(o.getPcbPureSnowSales()))
				.add(toDefaultZero(o.getLtSummerSales())).add(toDefaultZero(o.getLtSnowSales()))
				.add(toDefaultZero(o.getTbSummerSales())).add(toDefaultZero(o.getTbSnowSales()))
				.add(toDefaultZero(o.getIdSales())).add(toDefaultZero(o.getOrSales()))
				.add(toDefaultZero(o.getTifuSales())).add(toDefaultZero(o.getRetreadSales()))
				.add(toDefaultZero(o.getOtherSales())).add(toDefaultZero(o.getWorkSales())));
		r.getSales().setTotalElongation(toPercentage(r.getSales().getTotal(), totalSales));
	}

	/**
	 * 本数・金額表示用項目を設定します。
	 * @param model
	 * @param condition
	 */
	protected void setResultType(Model model, SearchCondition condition) {
		List<SelectItem> resultTypes = new ArrayList<>();
		resultTypes.add(new SelectItem(ResultsType.NUMBER.getLabel(), ResultsType.NUMBER.getValue()));
		resultTypes.add(new SelectItem(ResultsType.SALES.getLabel(), ResultsType.SALES.getValue()));

		model.addAttribute("resultTypes", resultTypes);
	}

	public static class Amount {
		BigDecimal todayTire = BigDecimal.ZERO;
		BigDecimal todayPcr = BigDecimal.ZERO;
		BigDecimal todayTbs = BigDecimal.ZERO;
		BigDecimal todayVan = BigDecimal.ZERO;
		BigDecimal todayLtr = BigDecimal.ZERO;
		BigDecimal todayTotal = BigDecimal.ZERO;
		BigDecimal tire = BigDecimal.ZERO;
		BigDecimal pcr = BigDecimal.ZERO;
		BigDecimal tbs = BigDecimal.ZERO;
		BigDecimal van = BigDecimal.ZERO;
		BigDecimal ltr = BigDecimal.ZERO;
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal tireElongation = BigDecimal.ZERO;
		BigDecimal pcrElongation = BigDecimal.ZERO;
		BigDecimal tbsElongation = BigDecimal.ZERO;
		BigDecimal vanElongation = BigDecimal.ZERO;
		BigDecimal ltrElongation = BigDecimal.ZERO;
		BigDecimal totalElongation = BigDecimal.ZERO;

		public BigDecimal getTodayTire() {
			return todayTire;
		}

		public void setTodayTire(BigDecimal todayTire) {
			this.todayTire = todayTire;
		}

		public BigDecimal getTodayPcr() {
			return todayPcr;
		}

		public void setTodayPcr(BigDecimal todayPcr) {
			this.todayPcr = todayPcr;
		}

		public BigDecimal getTodayTbs() {
			return todayTbs;
		}

		public void setTodayTbs(BigDecimal todayTbs) {
			this.todayTbs = todayTbs;
		}

		public BigDecimal getTire() {
			return tire;
		}

		public void setTire(BigDecimal tire) {
			this.tire = tire;
		}

		public BigDecimal getPcr() {
			return pcr;
		}

		public void setPcr(BigDecimal pcr) {
			this.pcr = pcr;
		}

		public BigDecimal getTbs() {
			return tbs;
		}

		public void setTbs(BigDecimal tbs) {
			this.tbs = tbs;
		}

		public BigDecimal getTireElongation() {
			return tireElongation;
		}

		public void setTireElongation(BigDecimal tireElongation) {
			this.tireElongation = tireElongation;
		}

		public BigDecimal getPcrElongation() {
			return pcrElongation;
		}

		public void setPcrElongation(BigDecimal pcrElongation) {
			this.pcrElongation = pcrElongation;
		}

		public BigDecimal getTbsElongation() {
			return tbsElongation;
		}

		public void setTbsElongation(BigDecimal tbsElongation) {
			this.tbsElongation = tbsElongation;
		}

		public BigDecimal getTodayVan() {
			return todayVan;
		}

		public void setTodayVan(BigDecimal todayVan) {
			this.todayVan = todayVan;
		}

		public BigDecimal getTodayLtr() {
			return todayLtr;
		}

		public void setTodayLtr(BigDecimal todayLtr) {
			this.todayLtr = todayLtr;
		}

		public BigDecimal getVan() {
			return van;
		}

		public void setVan(BigDecimal van) {
			this.van = van;
		}

		public BigDecimal getLtr() {
			return ltr;
		}

		public void setLtr(BigDecimal ltr) {
			this.ltr = ltr;
		}

		public BigDecimal getVanElongation() {
			return vanElongation;
		}

		public void setVanElongation(BigDecimal vanElongation) {
			this.vanElongation = vanElongation;
		}

		public BigDecimal getLtrElongation() {
			return ltrElongation;
		}

		public void setLtrElongation(BigDecimal ltrElongation) {
			this.ltrElongation = ltrElongation;
		}

		public BigDecimal getTodayTotal() {
			return todayTotal;
		}

		public void setTodayTotal(BigDecimal todayTotal) {
			this.todayTotal = todayTotal;
		}

		public BigDecimal getTotal() {
			return total;
		}

		public void setTotal(BigDecimal total) {
			this.total = total;
		}

		public BigDecimal getTotalElongation() {
			return totalElongation;
		}

		public void setTotalElongation(BigDecimal totalElongation) {
			this.totalElongation = totalElongation;
		}

	}

	public static class Result {
		String id;
		String accountName;
		String marketName;
		String rank;
		String firstName;
		String lastName;
		Integer plannedCount = 0;
		Integer heldCount = 0;
		BigDecimal iss;
		BigDecimal demandNumber;
		boolean groupImportant;
		boolean groupNew;
		boolean groupDeepPlowing;
		boolean groupYCp;
		boolean groupOtherCp;
		boolean groupOne;
		boolean groupTwo;
		boolean groupThree;
		boolean groupFour;
		boolean groupFive;
		Amount number = new Amount();
		Amount sales = new Amount();

		public String getAccountName() {
			return accountName;
		}

		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}

		public String getMarketName() {
			return marketName;
		}

		public void setMarketName(String marketName) {
			this.marketName = marketName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public Integer getPlannedCount() {
			return plannedCount;
		}

		public void setPlannedCount(Integer plannedCount) {
			this.plannedCount = plannedCount;
		}

		public Integer getHeldCount() {
			return heldCount;
		}

		public void setHeldCount(Integer heldCount) {
			this.heldCount = heldCount;
		}

		public BigDecimal getIss() {
			return iss;
		}

		public void setIss(BigDecimal iss) {
			this.iss = iss;
		}

		public BigDecimal getDemandNumber() {
			return demandNumber;
		}

		public void setDemandNumber(BigDecimal demandNumber) {
			this.demandNumber = demandNumber;
		}

		public boolean isGroupImportant() {
			return groupImportant;
		}

		public void setGroupImportant(boolean groupImportant) {
			this.groupImportant = groupImportant;
		}

		public boolean isGroupNew() {
			return groupNew;
		}

		public void setGroupNew(boolean groupNew) {
			this.groupNew = groupNew;
		}

		public boolean isGroupDeepPlowing() {
			return groupDeepPlowing;
		}

		public void setGroupDeepPlowing(boolean groupDeepPlowing) {
			this.groupDeepPlowing = groupDeepPlowing;
		}

		public boolean isGroupYCp() {
			return groupYCp;
		}

		public void setGroupYCp(boolean groupYCp) {
			this.groupYCp = groupYCp;
		}

		public boolean isGroupOtherCp() {
			return groupOtherCp;
		}

		public void setGroupOtherCp(boolean groupOtherCp) {
			this.groupOtherCp = groupOtherCp;
		}

		public boolean isGroupOne() {
			return groupOne;
		}

		public void setGroupOne(boolean groupOne) {
			this.groupOne = groupOne;
		}

		public boolean isGroupTwo() {
			return groupTwo;
		}

		public void setGroupTwo(boolean groupTwo) {
			this.groupTwo = groupTwo;
		}

		public boolean isGroupThree() {
			return groupThree;
		}

		public void setGroupThree(boolean groupThree) {
			this.groupThree = groupThree;
		}

		public boolean isGroupFour() {
			return groupFour;
		}

		public void setGroupFour(boolean groupFour) {
			this.groupFour = groupFour;
		}

		public boolean isGroupFive() {
			return groupFive;
		}

		public void setGroupFive(boolean groupFive) {
			this.groupFive = groupFive;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getRank() {
			return rank;
		}

		public void setRank(String rank) {
			this.rank = rank;
		}

		public Amount getNumber() {
			return number;
		}

		public void setNumber(Amount number) {
			this.number = number;
		}

		public Amount getSales() {
			return sales;
		}

		public void setSales(Amount sales) {
			this.sales = sales;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {

		String group;

		public String getGroup() {
			return group;
		}

		public void setGroup(String group) {
			this.group = group;
		}

		private String searchMode;

		public String getSearchMode() {
			return searchMode;
		}

		public void setSearchMode(String searchMode) {
			this.searchMode = searchMode;
		}
	}
	
	public static class DateConditionResult {
		String dateFrom;
		String dateTo;
		public String getDateFrom() {
			return dateFrom;
		}
		public void setDateFrom(String dateFrom) {
			this.dateFrom = dateFrom;
		}
		public String getDateTo() {
			return dateTo;
		}
		public void setDateTo(String dateTo) {
			this.dateTo = dateTo;
		}
	}

}
