package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.Constants.LogInfos.TimeUnit.Day;
import static jp.co.yrc.mv.common.Constants.LogInfos.TimeUnit.Month;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.ObjectUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.LogInfos.TimeUnit;
import jp.co.yrc.mv.common.Constants.LogInfos.UserAgent;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.LogInfosDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.entity.ViewInfos;
import jp.co.yrc.mv.helper.SelectLogInfosTimeUnitHelper;
import jp.co.yrc.mv.helper.SelectLogInfosUserAgentHelper;
import jp.co.yrc.mv.helper.SelectLogInfosViewHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.LogInfoService;
import jp.co.yrc.mv.service.SelectionItemsService;
import jp.co.yrc.mv.service.ViewInfosService;

@Controller
@Transactional
public class LogInfoController {

	/**
	 * 時間の表示は7時始まり
	 */
	private static final int[] HOUR = new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1,
			2, 3, 4, 5, 6 };

	@Value("${show.year.range.from}")
	int rangeFrom;

	@Value("${show.year.range.to}")
	int rangeTo;

	@Value("${logInfo.csv.header.1}")
	String csvHeader1;
	@Value("${logInfo.csv.header.2}")
	String csvHeader2;
	@Value("${logInfo.csv.header.3}")
	String csvHeader3;
	@Value("${logInfo.csv.month}")
	String csvMonth;
	@Value("${logInfo.csv.day}")
	String csvDay;
	@Value("${logInfo.csv.hour}")
	String csvHour;
	@Value("${loginfo.csv.filename}")
	String csvFilename;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Autowired
	LogInfoService logInfoService;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	SelectLogInfosUserAgentHelper selectLogInfosUserAgentHelper;

	@Autowired
	SelectLogInfosViewHelper selectLogInfosViewHelper;

	@Autowired
	SelectLogInfosTimeUnitHelper selectLogInfosTimeUnitHelper;

	@Autowired
	ViewInfosService viewInfosService;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/logInfo.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Date now = DateUtils.getDBDate();

		setMinMaxDate(param, now);

		Calendar from = Calendar.getInstance();
		from.setTime(now);
		from.set(Calendar.MONTH, 0);
		from.set(Calendar.DATE, 1);
		from.set(Calendar.HOUR_OF_DAY, 0);
		from.set(Calendar.MINUTE, 0);
		from.set(Calendar.SECOND, 0);
		from.set(Calendar.MILLISECOND, 0);

		param.dateFrom = from.getTime();

		Calendar to = Calendar.getInstance();
		to.setTime(now);
		to.add(Calendar.MONTH, 1);
		to.set(Calendar.DATE, 0);
		to.set(Calendar.HOUR_OF_DAY, 0);
		to.set(Calendar.MINUTE, 0);
		to.set(Calendar.SECOND, 0);
		to.set(Calendar.MILLISECOND, 0);
		param.dateTo = to.getTime();

		// デフォルトは月表示
		param.timeUnit = Month.getValue();

		Map<Integer, Header> headerMap = createHeader(param.dateFrom, param.dateTo, param.timeUnit);
		// 縦合計用
		Header total = new Header();
		total.total = true;

		List<Header> header = new ArrayList<>(headerMap.values());
		header.add(0, total);
		model.addAttribute("header", header);
		model.addAttribute("logInfoList", Collections.EMPTY_LIST);

		model.addAttribute("currentTimeUnit", Month.getLabel());
		model.addAttribute("currentTimeUnitTarget", Constants.DateFormat.yyyy.getFormat().format(param.getDateFrom()));

		selectYearsHelper.create(model);
		selectLogInfosTimeUnitHelper.createTimeUnit(model, false);
		selectLogInfosUserAgentHelper.createUserAgent(model);
		selectLogInfosViewHelper.createView(model);

		model.addAttribute(param);

		return "logInfo";

	}

	/**
	 * 検索押下表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/logInfo.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {

		Date now = DateUtils.getDBDate();
		Calendar c = Calendar.getInstance();
		c.setTime(now);

		setMinMaxDate(param, now);

		Map<Integer, Header> headerMap = createHeader(param.dateFrom, param.dateTo, param.timeUnit);

		Header total = new Header();
		total.total = true;
		List<Header> header = new ArrayList<>(headerMap.values());
		header.add(0, total);
		model.addAttribute("header", header);

		TimeUnit currentTimeUnit = TimeUnit.getByValue(param.timeUnit);
		model.addAttribute("currentTimeUnit", currentTimeUnit.getLabel());

		if (TimeUnit.Month == currentTimeUnit) {
			model.addAttribute("currentTimeUnitTarget",
					Constants.DateFormat.yyyy.getFormat().format(param.getDateFrom()));
		} else if (TimeUnit.Day == currentTimeUnit) {
			model.addAttribute("currentTimeUnitTarget",
					Constants.DateFormat.yyyyMM.getFormat().format(param.getDateFrom()));
		} else {
			model.addAttribute("currentTimeUnitTarget",
					Constants.DateFormat.yyyyMMdd.getFormat().format(param.getDateFrom()));
		}

		model.addAttribute(param);

		selectYearsHelper.create(model);
		selectLogInfosTimeUnitHelper.createTimeUnit(model, false);
		selectLogInfosUserAgentHelper.createUserAgent(model);
		selectLogInfosViewHelper.createView(model);

		Collection<Result> result = listResult(param, principal, headerMap);

		model.addAttribute("logInfoList", result);

		return "logInfo";
	}

	@RequestMapping(value = "/logInfoCsvDownload.html", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ByteArrayResource> download(SearchCondition param, Model model, Principal principal) {

		Map<Integer, Header> headerMap = createHeader(param.dateFrom, param.dateTo, param.timeUnit);

		Header total = new Header();
		total.total = true;
		List<Header> headerList = new ArrayList<>(headerMap.values());

		Collection<Result> list = listResult(param, principal, headerMap);

		HttpHeaders httpHeaders = new HttpHeaders();

		SimpleDateFormat fomat = Constants.DateFormat.yyyyMMdd.getFormat();
		String timeUnitName;
		String headerTerm = csvHeader2;
		if (Month.getValue().equals(param.timeUnit)) {
			timeUnitName = csvMonth;
			headerTerm = String.format(headerTerm,
					String.format("%s～%s", fomat.format(param.dateFrom), fomat.format(param.dateTo)));
		} else if (Day.getValue().equals(param.timeUnit)) {
			timeUnitName = csvDay;
			headerTerm = String.format(headerTerm,
					String.format("%s～%s", fomat.format(param.dateFrom), fomat.format(param.dateTo)));
		} else {
			timeUnitName = csvHour;
			headerTerm = String.format(headerTerm, fomat.format(param.dateFrom));
		}

		try {
			httpHeaders.setContentDispositionFormData("attachment",
					new URLCodec().encode(String.format(csvFilename, timeUnitName)));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		httpHeaders.setContentType(MediaType.valueOf("text/csv;charset=Shift_jis"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos);
				OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("MS932"));
				BufferedWriter bw = new BufferedWriter(osw);
				ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {

			String headerDat = "";
			for (Header b : headerList) {
				if (Month.getValue().equals(param.timeUnit)) {
					headerDat = headerDat + String.format("%s/%s", b.year, b.count);
				} else if (Day.getValue().equals(param.timeUnit)) {
					headerDat = headerDat + String.format("%s/%s/%s", b.year, b.month, b.count);
				} else {
					headerDat = headerDat + b.count;
				}
				headerDat = headerDat + ",";
			}

			// ヘッダー書き込み
			csvWriter.write(String.format(csvHeader1, timeUnitName));
			csvWriter.write(headerTerm);
			String csvHeader = String.format(csvHeader3, headerDat);
			String[] headerArr = csvHeader.split(",");
			csvWriter.write(headerArr);

			if (list != null) {
				for (Result r : list) {
					List<Object> csvCommon = new ArrayList<>();
					csvCommon.add(r.getViewName());
					csvCommon.add(r.getDiv1Nm());
					csvCommon.add(r.getDiv2Nm());
					csvCommon.add(r.getDiv3Nm());
					if (r.getUserId() == null) {
						csvCommon.add(null);
						csvCommon.add(null);
					} else {
						csvCommon.add(r.getUserId());
						csvCommon.add(r.getUserName());
					}

					if (!UserAgent.PC.getValue().equals(param.userAgent)
							&& r.getIpadSummary().getCount().compareTo(BigDecimal.ZERO) > 0) {
						List<Object> ipads = new ArrayList<>(csvCommon);
						ipads.add(UserAgent.IPad.getLabel());

						for (Breakdown ipad : r.getIpad().values()) {
							ipads.add(ipad.getCount());
						}
						ipads.add(r.getIpadSummary().getCount());
						csvWriter.write(ipads);
					}

					if (!UserAgent.IPad.getValue().equals(param.userAgent)
							&& r.getPcSummary().getCount().compareTo(BigDecimal.ZERO) > 0) {
						List<Object> pcs = new ArrayList<>(csvCommon);
						pcs.add(UserAgent.PC.getLabel());
						for (Breakdown pc : r.getPc().values()) {
							pcs.add(pc.getCount());
						}
						pcs.add(r.getPcSummary().getCount());
						csvWriter.write(pcs);
					}

				}
			}
			csvWriter.flush();
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), httpHeaders, HttpStatus.OK);

	}

	protected Map<Integer, Header> createHeader(Date dateFrom, Date dateTo, String timeunit) {
		Calendar calenderFrom = Calendar.getInstance();
		calenderFrom.setTime(dateFrom);
		calenderFrom.set(Calendar.HOUR_OF_DAY, 0);
		calenderFrom.set(Calendar.MINUTE, 0);
		calenderFrom.set(Calendar.SECOND, 0);
		calenderFrom.set(Calendar.MILLISECOND, 0);

		Calendar calenderTo = Calendar.getInstance();
		calenderTo.setTime(dateTo);
		calenderTo.set(Calendar.HOUR_OF_DAY, 0);
		calenderTo.set(Calendar.MINUTE, 0);
		calenderTo.set(Calendar.SECOND, 0);
		calenderTo.set(Calendar.MILLISECOND, 0);

		Map<Integer, Header> header = new LinkedHashMap<>();
		if (Month.getValue().equals(timeunit)) {
			for (int i = calenderFrom.get(Calendar.MONTH) + 1; i <= calenderTo.get(Calendar.MONTH) + 1; i++) {
				Header b = new Header();
				b.count = new BigDecimal(i);
				b.year = calenderFrom.get(Calendar.YEAR);
				header.put(i, b);
			}
		}

		else if (Day.getValue().equals(timeunit)) {

			for (int i = calenderFrom.get(Calendar.DATE), max = calenderTo.get(Calendar.DATE); i <= max; i++) {
				Header b = new Header();
				b.count = new BigDecimal(i);
				b.year = calenderFrom.get(Calendar.YEAR);
				b.month = calenderFrom.get(Calendar.MONTH) + 1;
				header.put(i, b);
			}

		} else {

			for (int i : HOUR) {
				Header b = new Header();
				b.count = new BigDecimal(i);
				header.put(i, b);
			}
		}

		return header;
	}

	protected Collection<Result> listResult(SearchCondition param, Principal principal, Map<Integer, Header> header) {

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1, 0, 0, 0);

		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		String url = selectLogInfosViewHelper.checkSoshikiParamValue(param.view);

		// アクセスURL > [営業組織・ユーザーID] > 集計オブジェクトのMAP
		Map<String, Map<Key, Result>> map = new LinkedHashMap<>();

		List<ViewInfos> logInfosViews = viewInfosService.listLogInfosView(url);

		// アクセスURL毎に入れ物を作る
		for (ViewInfos view : logInfosViews) {
			map.put(view.getUrl(), new LinkedHashMap<Key, Result>());
		}

		List<String> div1 = soshikiHelper.createSearchSalesCompanyCodeList(param.getDiv1(),
				userDetailsHelper.getUserInfo(principal), now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		List<LogInfosDto> list = logInfoService.listAccessLog(param.getDateFrom(), param.getDateTo(), div1, div2, div3,
				tanto, param.timeUnit, url, userDetailsHelper.getUserInfo(principal));

		for (LogInfosDto o : list) {

			long time = o.getTime();

			// アクセスURLに対応するMAPを取得する
			Map<Key, Result> viewMap = map.get(o.getView());

			Key key = new Key(o.getEigyoSosikiCd(), o.getUserId());
			Result r = viewMap.get(key);
			if (r == null) {
				r = new Result();
				r.setEigyoSosikiNm(o.getEigyoSosikiNm());
				r.setDiv1Nm(o.getDiv1Nm());
				r.setDiv2Nm(o.getDiv2Nm());
				r.setDiv3Nm(o.getDiv3Nm());
				r.setUserId(o.getUserId());

				r.div1 = o.getDiv1();
				r.div2 = o.getDiv2();
				r.div3 = o.getDiv3();

				// ユーザーIDが入って以内場合は部署単位の集計
				if (o.getUserId() != null) {
					r.setUserName(o.getLastName() + " " + o.getFirstName());
				}
				r.setViewName(o.getViewName());
				viewMap.put(key, r);

				for (Entry<Integer, Header> entry : header.entrySet()) {
					r.getIpad().put(entry.getKey(), new Breakdown());
					r.getPc().put(entry.getKey(), new Breakdown());
				}
			}
			Breakdown b = null;
			if (UserAgent.IPad.getValue().equals(o.getClientType())) {
				b = r.getIpad().get((int) time);
				r.getIpadSummary().count = r.getIpadSummary().count.add(o.getCount());
			} else {
				b = r.getPc().get((int) time);
				r.getPcSummary().count = r.getPcSummary().count.add(o.getCount());
			}
			b.setCount(o.getCount());
		}

		List<Result> result = new ArrayList<>();

		for (Map<Key, Result> viewMap : map.values()) {

			List<Result> resultValues = new ArrayList<>();
			resultValues.addAll(viewMap.values());
			Collections.sort(resultValues, new ResultComparator());
			result.addAll(resultValues);
		}

		return result;
	}

	private static class ResultComparator implements Comparator<Result> {

		@Override
		public int compare(Result o1, Result o2) {

			int result = ObjectUtils.compare(o1.div1, o2.div1);
			if (result != 0) {
				return result;
			}
			result = ObjectUtils.compare(o1.div2, o2.div2);
			if (result != 0) {
				return result;
			}
			result = ObjectUtils.compare(o1.div3, o2.div3);
			if (result != 0) {
				return result;
			}
			result = ObjectUtils.compare(o1.userId, o2.userId);
			if (result != 0) {
				return result;
			}

			return 0;
		}

	}

	private static class Key {

		public Key(String eigyoSosikiCd, String userId) {
			super();
			this.eigyoSosikiCd = eigyoSosikiCd;
			this.userId = userId;
		}

		String eigyoSosikiCd;
		String userId;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((eigyoSosikiCd == null) ? 0 : eigyoSosikiCd.hashCode());
			result = prime * result + ((userId == null) ? 0 : userId.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Key)) {
				return false;
			}
			Key other = (Key) obj;
			if (eigyoSosikiCd == null) {
				if (other.eigyoSosikiCd != null) {
					return false;
				}
			} else if (!eigyoSosikiCd.equals(other.eigyoSosikiCd)) {
				return false;
			}
			if (userId == null) {
				if (other.userId != null) {
					return false;
				}
			} else if (!userId.equals(other.userId)) {
				return false;
			}
			return true;
		}

	}

	public static class Header extends Breakdown {
		int year;
		int month;
	}

	public static class Breakdown {
		boolean total;
		BigDecimal count = BigDecimal.ZERO;

		public Breakdown() {
			super();
		}

		public Breakdown(boolean total) {
			super();
			this.total = total;
		}

		public Breakdown(boolean total, BigDecimal count) {
			super();
			this.total = total;
			this.count = count;
		}

		/**
		 * @return the count
		 */
		public BigDecimal getCount() {
			return count;
		}

		/**
		 * @param count
		 *            the count to set
		 */
		public void setCount(BigDecimal count) {
			this.count = count;
		}

		public boolean isTotal() {
			return total;
		}

		public void setTotal(boolean total) {
			this.total = total;
		}

	}

	public static class Result {
		Map<Integer, Breakdown> pc = new LinkedHashMap<>();
		Map<Integer, Breakdown> ipad = new LinkedHashMap<>();

		Breakdown pcSummary = new Breakdown();
		Breakdown ipadSummary = new Breakdown();

		String viewName;
		String eigyoSosikiNm;
		String userName;
		String userId;

		String div1Nm;
		String div2Nm;
		String div3Nm;

		String div1;
		String div2;
		String div3;

		BigDecimal plan = BigDecimal.ZERO;
		BigDecimal result = BigDecimal.ZERO;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		/**
		 * @return the pc
		 */
		public Map<Integer, Breakdown> getPc() {
			return pc;
		}

		/**
		 * @param pc
		 *            the pc to set
		 */
		public void setPc(Map<Integer, Breakdown> pc) {
			this.pc = pc;
		}

		/**
		 * @return the ipad
		 */
		public Map<Integer, Breakdown> getIpad() {
			return ipad;
		}

		/**
		 * @param ipad
		 *            the ipad to set
		 */
		public void setIpad(Map<Integer, Breakdown> ipad) {
			this.ipad = ipad;
		}

		/**
		 * @return the viewName
		 */
		public String getViewName() {
			return viewName;
		}

		/**
		 * @param viewName
		 *            the viewName to set
		 */
		public void setViewName(String viewName) {
			this.viewName = viewName;
		}

		/**
		 * @return the eigyoSosikiNm
		 */
		public String getEigyoSosikiNm() {
			return eigyoSosikiNm;
		}

		/**
		 * @param eigyoSosikiNm
		 *            the eigyoSosikiNm to set
		 */
		public void setEigyoSosikiNm(String eigyoSosikiNm) {
			this.eigyoSosikiNm = eigyoSosikiNm;
		}

		/**
		 * @return the userName
		 */
		public String getUserName() {
			return userName;
		}

		/**
		 * @param userName
		 *            the userName to set
		 */
		public void setUserName(String userName) {
			this.userName = userName;
		}

		public Breakdown getPcSummary() {
			return pcSummary;
		}

		public void setPcSummary(Breakdown pcSummary) {
			this.pcSummary = pcSummary;
		}

		public Breakdown getIpadSummary() {
			return ipadSummary;
		}

		public void setIpadSummary(Breakdown ipadSummary) {
			this.ipadSummary = ipadSummary;
		}

		public String getDiv1Nm() {
			return div1Nm;
		}

		public void setDiv1Nm(String div1Nm) {
			this.div1Nm = div1Nm;
		}

		public String getDiv2Nm() {
			return div2Nm;
		}

		public void setDiv2Nm(String div2Nm) {
			this.div2Nm = div2Nm;
		}

		public String getDiv3Nm() {
			return div3Nm;
		}

		public void setDiv3Nm(String div3Nm) {
			this.div3Nm = div3Nm;
		}

	}

	protected void setMinMaxDate(SearchCondition param, Date now) {
		// DBからでなくpropertyからとる
		Calendar max = Calendar.getInstance();
		max.setTime(now);
		// max.add(Calendar.YEAR, rangeTo);
		// max.set(Calendar.MONTH, 11);
		// max.set(Calendar.DATE, max.getActualMaximum(Calendar.DATE));

		Calendar min = Calendar.getInstance();
		min.add(Calendar.YEAR, -rangeFrom);
		min.set(Calendar.MONTH, 0);
		min.set(Calendar.DATE, min.getActualMinimum(Calendar.DATE));

		param.minDate = min.getTime();
		param.maxDate = max.getTime();
	}

	public static class SearchCondition extends SearchConditionBaseDto {

		private String timeUnit;
		private String view;
		private String userAgent;

		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date dateTo;
		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date dateFrom;

		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date maxDate;
		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date minDate;

		public String getTimeUnit() {
			return timeUnit;
		}

		public void setTimeUnit(String timeUnit) {
			this.timeUnit = timeUnit;
		}

		public String getUserAgent() {
			return userAgent;
		}

		public void setUserAgent(String userAgent) {
			this.userAgent = userAgent;
		}

		public String getView() {
			return view;
		}

		public void setView(String view) {
			this.view = view;
		}

		public Date getDateTo() {
			return dateTo;
		}

		public void setDateTo(Date dateTo) {
			this.dateTo = dateTo;
		}

		public Date getDateFrom() {
			return dateFrom;
		}

		public void setDateFrom(Date dateFrom) {
			this.dateFrom = dateFrom;
		}

		public Date getMaxDate() {
			return maxDate;
		}

		public void setMaxDate(Date maxDate) {
			this.maxDate = maxDate;
		}

		public Date getMinDate() {
			return minDate;
		}

		public void setMinDate(Date minDate) {
			this.minDate = minDate;
		}

	}

}
