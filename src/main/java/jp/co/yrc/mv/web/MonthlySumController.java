package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.MathUtils.HUNDRED;
import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;
import static jp.co.yrc.mv.common.MathUtils.toK;
import static jp.co.yrc.mv.common.MathUtils.toPercentage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.Constants.Group;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.MonthlyCommentsDao;
import jp.co.yrc.mv.dao.PlansDao;
import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.DemandDto;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.dto.GroupDealDto;
import jp.co.yrc.mv.dto.GroupTotalDto;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.Calls;
import jp.co.yrc.mv.entity.MonthlyGroupResults;
import jp.co.yrc.mv.entity.Plans;
import jp.co.yrc.mv.entity.Results;
import jp.co.yrc.mv.helper.GroupHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.AccountsService;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.MstEigyoSosikiService;
import jp.co.yrc.mv.service.ResultsService;

@Controller
@Transactional
public class MonthlySumController {

	@Autowired
	ResultsService resultsService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	CallsService callsService;

	@Autowired
	GroupHelper groupHelper;

	@Autowired
	AccountsService accountsService;

	@Autowired
	MonthlyCommentsDao monthlyCommentsDao;

	@Autowired
	PlansDao plansDao;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@RequestMapping(value = "/monthlySum.html", method = RequestMethod.GET)
	public String init(MonthlySumSearchCondition param, Model model, Principal principal) {
		setupNengatsu(model);
		// デフォルト
		param.setModeChange("all");
		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);
		model.addAttribute(param);

		SimpleDateFormat formatter = DateUtils.getDayOfWeekFormat();

		// カレンダー
		List<DailySum> daily = new ArrayList<DailySum>();
		for (int i = 1, max = c.getActualMaximum(Calendar.DATE); i <= max; i++, c.add(Calendar.DATE, 1)) {
			DailySum total = new DailySum();
			total.date = i;
			total.dayOfWeek = formatter.format(c.getTime());
			daily.add(total);
		}

		MonthlyCommentsDto monthlyComments = getPlanDemandCoefficient(param, userDetailsHelper.getUserId(principal));
		model.addAttribute("planDemandCoefficient", monthlyComments.getPlanDemandCoefficient());

		// dummyで空の結果を入れる（何も入れないと画面で落ちる）
		model.addAttribute(new SumTotal());
		model.addAttribute("sumDaily", daily);
		model.addAttribute(param);

		// コメント入力用にログイン者の情報をとる
		UserInfoDto user = userDetailsHelper.getUserInfo(principal);
		List<EigyoSosikiDto> list = mstEigyoSosikiService.listMySosiki(user.getCorpCodes(), user.getId(), param.year,
				param.month);
		// 組織は複数所属しているので最初に取れたものを設定
		EigyoSosikiDto eigyoSosikiDto = list.get(0);
		MonthlySumUserInfo userInfo = new MonthlySumUserInfo();
		userInfo.setFirstName(user.getFirstName());
		userInfo.setLastName(user.getLastName());
		userInfo.setEigyoSosiki(eigyoSosikiDto.getEigyoSosikiNm());
		model.addAttribute("userInfo", userInfo);

		return "monthlySum";
	}

	@RequestMapping(value = "/monthlySum.html", method = RequestMethod.POST)
	public String search(MonthlySumSearchCondition param, Model model, Principal principal) {

		MonthlyCommentsDto monthlyComments = getPlanDemandCoefficient(param, userDetailsHelper.getUserId(principal));

		setupNengatsu(model);

		int month = param.getMonth();
		int year = param.getYear();

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());

		@SuppressWarnings("unchecked")
		List<String> companies = div1 == null ? Collections.EMPTY_LIST : Collections.singletonList(div1);

		SimpleDateFormat formatter = DateUtils.getDayOfWeekFormat();

		List<DailySum> daily = new ArrayList<DailySum>();
		Calendar c = Calendar.getInstance();
		c.set(year, month - 1, 1, 0, 0, 0);
		// 検索年月の日数分の入れ物を作成
		for (int i = 1, max = c.getActualMaximum(Calendar.DATE); i <= max; i++, c.add(Calendar.DATE, 1)) {
			DailySum total = new DailySum();
			total.date = i;
			total.dayOfWeek = formatter.format(c.getTime());
			daily.add(total);
		}

		// ↑のカウントしてるところで月が繰り上がるので、もう一度日付を作る
		Calendar conditionDate = Calendar.getInstance();
		conditionDate.set(year, month - 1, 1, 0, 0, 0);
		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		now.set(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1, 0, 0, 0);
		boolean isHistory = now.compareTo(conditionDate) > 0;

		// 集計表のカレンダー部
		List<CallCountInfoDto> groupCounts = callsService.sumDailyGroupCall(year, month, param.getTanto(), companies,
				div2, div3, isHistory);

		for (CallCountInfoDto o : groupCounts) {
			Date date = o.getDateStart();
			int day = DateUtils.getDay(date);
			DailySum total = daily.get(day - 1);
			setGroupCall(o, total);
		}

		Plans plan = plansDao.sumUserPlan(year, month, param.getTanto(), companies, div2, div3);

		// 担当得意先別・担当外得意先別カレンダー部
		List<CallCountInfoDto> dailyUserCall = callsService.listDailyUserCall(year, month, param.getTanto(), div1,
				div2, div3, isHistory);
		for (CallCountInfoDto o : dailyUserCall) {
			Date date = o.getDateStart();
			int day = DateUtils.getDay(date);
			DailySum total = daily.get(day - 1);
			total.setResult(total.getResult().add(toDefaultZero(o.getCallCount())));
			total.setPlan(total.getPlan().add(toDefaultZero(o.getPlannedCount())));
		}

		// 日ごとのユーザーの実績集計
		List<Results> dailyUserResult = resultsService.listDailyUserResult(year, month, param.getTanto(), companies,
				div2, div3, isHistory);
		for (Results o : dailyUserResult) {
			int day = o.getDate();
			DailySum total = daily.get(day - 1);
			total.setResultNumber(o.getNumber());
		}

		GroupTotalDto groupTotalDto = new GroupTotalDto();

		// 集計表
		MonthlyGroupResults monthlyGroupResults = resultsService.sumMonthlyGroupResult(year, month, param.getTanto(),
				companies, div2, div3, isHistory);
		if (monthlyGroupResults != null) {
			dozerBeanMapper.map(monthlyGroupResults, groupTotalDto);
		}

		List<DemandDto> demandList = accountsService.sumGroupDemand(year, month, companies, div2, div3,
				param.getTanto(), isHistory);
		groupHelper.setDemand(demandList, groupTotalDto);

		GroupDealDto sumDeals = accountsService.sumGroupDeal(year, month, companies, div2, div3, param.getTanto(),
				isHistory);
		if (sumDeals != null) {
			dozerBeanMapper.map(sumDeals, groupTotalDto);
		}

		List<CallCountInfoDto> callList = callsService.sumMonthlyGroupCall(year, month, param.getTanto(), companies,
				div2, div3, isHistory);
		groupHelper.setCall(callList, groupTotalDto);

		SumTotal sumTotal = createTotal(groupTotalDto, monthlyComments, plan);

		// 担当得意先別
		List<AccountsDto> assignedAccounts = accountsService.listAssignedAccount(year, month, div1, div2, div3,
				param.getTanto(), isHistory);

		List<AccountInfo> assigned = new ArrayList<AccountInfo>();
		setAccountInfo(param, month, year, daily, assignedAccounts, assigned, param.getTanto(), monthlyComments);

		// 担当外得意先別
		List<AccountsDto> outsideAccounts = accountsService.listOutsideAccount(year, month, div1, div2, div3,
				param.getTanto(), isHistory);
		List<AccountInfo> outside = new ArrayList<AccountInfo>();
		setAccountInfo(param, month, year, daily, outsideAccounts, outside, param.getTanto(), monthlyComments);

		model.addAttribute("planDemandCoefficient", monthlyComments.getPlanDemandCoefficient());

		model.addAttribute(sumTotal);
		model.addAttribute("sumDaily", daily);
		model.addAttribute("assigned", assigned);
		model.addAttribute("outside", outside);
		model.addAttribute(param);

		// 月報コメントのユーザー設定
		UserInfoDto user = userDetailsHelper.getUserInfo(principal);
		// 検索日付を使うと署名が過去の組織になるので、現在の日付を使う
		List<EigyoSosikiDto> list = mstEigyoSosikiService.listMySosiki(user.getCorpCodes(), user.getId(),
				now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		EigyoSosikiDto eigyoSosikiDto = list.get(0);
		MonthlySumUserInfo userInfo = new MonthlySumUserInfo();
		userInfo.setFirstName(user.getFirstName());
		userInfo.setLastName(user.getLastName());
		userInfo.setEigyoSosiki(eigyoSosikiDto.getEigyoSosikiNm());
		model.addAttribute("userInfo", userInfo);

		return "monthlySum";
	}

	/**
	 * 画面表示用のDTOに整形
	 * 
	 * @param groupTotalDto
	 * @param commentsDto
	 * @param plan
	 * @return
	 */
	protected SumTotal createTotal(GroupTotalDto groupTotalDto, MonthlyCommentsDto commentsDto, Plans plan) {
		SumTotal result = new SumTotal();

		if (plan == null) {
			plan = new Plans();
		}

		BigDecimal planDemandCoefficient = commentsDto.getPlanDemandCoefficient();
		if (planDemandCoefficient.compareTo(BigDecimal.ZERO) != 0) {
			planDemandCoefficient = planDemandCoefficient.divide(HUNDRED);
		}

		result.number.plan = toDefaultZero(plan.getNumber());
		result.sales.plan = toDefaultZero(plan.getSales());
		result.margin.plan = toDefaultZero(plan.getMargin());

		result.demand.plan = toDefaultZero(groupTotalDto.getRankADemandEstimation())
				.add(toDefaultZero(groupTotalDto.getRankBDemandEstimation()))
				.add(toDefaultZero(groupTotalDto.getRankCDemandEstimation())).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP);

		result.demand.rankA = toDefaultZero(groupTotalDto.getRankADemandEstimation()).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP);
		result.demand.rankB = toDefaultZero(groupTotalDto.getRankBDemandEstimation()).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP);
		result.demand.rankC = toDefaultZero(groupTotalDto.getRankCDemandEstimation()).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP);
		result.demand.important = toDefaultZero(groupTotalDto.getImportantDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.newCustomer = toDefaultZero(groupTotalDto.getNewDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.deepPlowing = toDefaultZero(groupTotalDto.getDeepPlowingDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.yCp = toDefaultZero(groupTotalDto.getYCpDemandEstimation()).multiply(planDemandCoefficient)
				.setScale(0, RoundingMode.HALF_UP);
		result.demand.otherCp = toDefaultZero(groupTotalDto.getOtherCpDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.groupOne = toDefaultZero(groupTotalDto.getGroupOneDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.groupTwo = toDefaultZero(groupTotalDto.getGroupTwoDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.groupThree = toDefaultZero(groupTotalDto.getGroupThreeDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.groupFour = toDefaultZero(groupTotalDto.getGroupFourDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);
		result.demand.groupFive = toDefaultZero(groupTotalDto.getGroupFiveDemandEstimation()).multiply(
				planDemandCoefficient).setScale(0, RoundingMode.HALF_UP);

		result.number.result = toDefaultZero(groupTotalDto.getRankANumber()).add(
				toDefaultZero(groupTotalDto.getRankBNumber())).add(toDefaultZero(groupTotalDto.getRankCNumber()));

		result.number.rankA = groupTotalDto.getRankANumber();
		result.number.rankB = groupTotalDto.getRankBNumber();
		result.number.rankC = groupTotalDto.getRankCNumber();
		result.number.important = groupTotalDto.getImportantNumber();
		result.number.newCustomer = groupTotalDto.getNewNumber();
		result.number.deepPlowing = groupTotalDto.getDeepPlowingNumber();
		result.number.yCp = groupTotalDto.getYCpNumber();
		result.number.otherCp = groupTotalDto.getOtherCpNumber();
		// result.number.outside = groupTotalDto.getOutsideNumber();
		result.number.groupOne = groupTotalDto.getGroupOneNumber();
		result.number.groupTwo = groupTotalDto.getGroupTwoNumber();
		result.number.groupThree = groupTotalDto.getGroupThreeNumber();
		result.number.groupFour = groupTotalDto.getGroupFourNumber();
		result.number.groupFive = groupTotalDto.getGroupFiveNumber();

		result.iss.plan = toPercentage(result.number.plan, result.demand.plan);
		result.iss.result = toPercentage(result.number.result, result.demand.plan);
		result.iss.rankA = toPercentage(result.number.rankA, result.demand.rankA);
		result.iss.rankB = toPercentage(result.number.rankB, result.demand.rankB);
		result.iss.rankC = toPercentage(result.number.rankC, result.demand.rankC);
		result.iss.important = toPercentage(result.number.important, result.demand.important);
		result.iss.newCustomer = toPercentage(result.number.newCustomer, result.demand.newCustomer);
		result.iss.deepPlowing = toPercentage(result.number.deepPlowing, result.demand.deepPlowing);
		result.iss.yCp = toPercentage(result.number.yCp, result.demand.yCp);
		result.iss.otherCp = toPercentage(result.number.otherCp, result.demand.otherCp);
		result.iss.groupOne = toPercentage(result.number.groupOne, result.demand.groupOne);
		result.iss.groupTwo = toPercentage(result.number.groupTwo, result.demand.groupTwo);
		result.iss.groupThree = toPercentage(result.number.groupThree, result.demand.groupThree);
		result.iss.groupFour = toPercentage(result.number.groupFour, result.demand.groupFour);
		result.iss.groupFive = toPercentage(result.number.groupFive, result.demand.groupFive);

		result.sales.result = toDefaultZero(groupTotalDto.getRankASales()).add(
				toDefaultZero(groupTotalDto.getRankBSales())).add(toDefaultZero(groupTotalDto.getRankCSales()));
		result.sales.rankA = toK(groupTotalDto.getRankASales());
		result.sales.rankB = toK(groupTotalDto.getRankBSales());
		result.sales.rankC = toK(groupTotalDto.getRankCSales());
		result.sales.important = toK(groupTotalDto.getImportantSales());
		result.sales.newCustomer = toK(groupTotalDto.getNewSales());
		result.sales.deepPlowing = toK(groupTotalDto.getDeepPlowingSales());
		result.sales.yCp = toK(groupTotalDto.getYCpSales());
		result.sales.otherCp = toK(groupTotalDto.getOtherCpSales());
		result.sales.groupOne = toK(groupTotalDto.getGroupOneSales());
		result.sales.groupTwo = toK(groupTotalDto.getGroupTwoSales());
		result.sales.groupThree = toK(groupTotalDto.getGroupThreeSales());
		result.sales.groupFour = toK(groupTotalDto.getGroupFourSales());
		result.sales.groupFive = toK(groupTotalDto.getGroupFiveSales());

		result.margin.result = toDefaultZero(groupTotalDto.getRankAMargin()).add(
				toDefaultZero(groupTotalDto.getRankBMargin())).add(toDefaultZero(groupTotalDto.getRankCMargin()));
		result.margin.rankA = toK(groupTotalDto.getRankAMargin());
		result.margin.rankB = toK(groupTotalDto.getRankBMargin());
		result.margin.rankC = toK(groupTotalDto.getRankCMargin());
		result.margin.important = toK(groupTotalDto.getImportantMargin());
		result.margin.newCustomer = toK(groupTotalDto.getNewMargin());
		result.margin.deepPlowing = toK(groupTotalDto.getDeepPlowingMargin());
		result.margin.yCp = toK(groupTotalDto.getYCpMargin());
		result.margin.otherCp = toK(groupTotalDto.getOtherCpMargin());
		result.margin.groupOne = toK(groupTotalDto.getGroupOneMargin());
		result.margin.groupTwo = toK(groupTotalDto.getGroupTwoMargin());
		result.margin.groupThree = toK(groupTotalDto.getGroupThreeMargin());
		result.margin.groupFour = toK(groupTotalDto.getGroupFourMargin());
		result.margin.groupFive = toK(groupTotalDto.getGroupFiveMargin());

		result.rate.rankA = toPercentage(groupTotalDto.getRankAMargin(), groupTotalDto.getRankASales());
		result.rate.rankB = toPercentage(groupTotalDto.getRankBMargin(), groupTotalDto.getRankBSales());
		result.rate.rankC = toPercentage(groupTotalDto.getRankCMargin(), groupTotalDto.getRankCSales());
		result.rate.important = toPercentage(groupTotalDto.getImportantMargin(), groupTotalDto.getImportantSales());
		result.rate.newCustomer = toPercentage(groupTotalDto.getNewMargin(), groupTotalDto.getNewSales());
		result.rate.deepPlowing = toPercentage(groupTotalDto.getDeepPlowingMargin(), groupTotalDto.getNewSales());
		result.rate.yCp = toPercentage(groupTotalDto.getYCpMargin(), groupTotalDto.getDeepPlowingSales());
		result.rate.otherCp = toPercentage(groupTotalDto.getOtherCpMargin(), groupTotalDto.getDeepPlowingSales());
		result.rate.groupOne = toPercentage(groupTotalDto.getGroupOneMargin(), groupTotalDto.getGroupOneSales());
		result.rate.groupTwo = toPercentage(groupTotalDto.getGroupTwoMargin(), groupTotalDto.getGroupTwoSales());
		result.rate.groupThree = toPercentage(groupTotalDto.getGroupThreeMargin(), groupTotalDto.getGroupThreeSales());
		result.rate.groupFour = toPercentage(groupTotalDto.getGroupFourMargin(), groupTotalDto.getGroupFourSales());
		result.rate.groupFive = toPercentage(groupTotalDto.getGroupFiveMargin(), groupTotalDto.getGroupFiveSales());

		result.deal.result = toDefaultZero(groupTotalDto.getRankADealCount()).add(
				toDefaultZero(groupTotalDto.getRankBDealCount())).add(toDefaultZero(groupTotalDto.getRankCDealCount()));
		result.deal.plan = toDefaultZero(groupTotalDto.getRankADealPlanCount()).add(
				toDefaultZero(groupTotalDto.getRankBDealPlanCount())).add(
				toDefaultZero(groupTotalDto.getRankCDealPlanCount()));

		result.deal.rankA = groupTotalDto.getRankADealCount();
		result.deal.rankB = groupTotalDto.getRankBDealCount();
		result.deal.rankC = groupTotalDto.getRankCDealCount();
		result.deal.important = groupTotalDto.getImportantDealCount();
		result.deal.newCustomer = groupTotalDto.getNewDealCount();
		result.deal.deepPlowing = groupTotalDto.getDeepPlowingDealCount();
		result.deal.yCp = groupTotalDto.getYCpDealCount();
		result.deal.otherCp = groupTotalDto.getOtherCpDealCount();
		result.deal.groupOne = groupTotalDto.getGroupOneDealCount();
		result.deal.groupTwo = groupTotalDto.getGroupTwoDealCount();
		result.deal.groupThree = groupTotalDto.getGroupThreeDealCount();
		result.deal.groupFour = groupTotalDto.getGroupFourDealCount();
		result.deal.groupFive = groupTotalDto.getGroupFiveDealCount();

		result.own.plan = toDefaultZero(groupTotalDto.getRankAOwnCount()).add(
				toDefaultZero(groupTotalDto.getRankBOwnCount())).add(toDefaultZero(groupTotalDto.getRankCOwnCount()));
		result.own.rankA = groupTotalDto.getRankAOwnCount();
		result.own.rankB = groupTotalDto.getRankBOwnCount();
		result.own.rankC = groupTotalDto.getRankCOwnCount();
		result.own.important = groupTotalDto.getImportantOwnCount();
		result.own.newCustomer = groupTotalDto.getNewOwnCount();
		result.own.deepPlowing = groupTotalDto.getDeepPlowingOwnCount();
		result.own.yCp = groupTotalDto.getYCpOwnCount();
		result.own.otherCp = groupTotalDto.getOtherCpOwnCount();
		result.own.groupOne = groupTotalDto.getGroupOneOwnCount();
		result.own.groupTwo = groupTotalDto.getGroupTwoOwnCount();
		result.own.groupThree = groupTotalDto.getGroupThreeOwnCount();
		result.own.groupFour = groupTotalDto.getGroupFourOwnCount();
		result.own.groupFive = groupTotalDto.getGroupFiveOwnCount();

		result.call.result = toDefaultZero(groupTotalDto.getRankACallCount()).add(
				toDefaultZero(groupTotalDto.getRankBCallCount())).add(toDefaultZero(groupTotalDto.getRankCCallCount()));
		result.call.plan = toDefaultZero(groupTotalDto.getRankAPlanCount()).add(
				toDefaultZero(groupTotalDto.getRankBPlanCount())).add(toDefaultZero(groupTotalDto.getRankCPlanCount()));
		result.call.rankA = groupTotalDto.getRankACallCount();
		result.call.rankB = groupTotalDto.getRankBCallCount();
		result.call.rankC = groupTotalDto.getRankCCallCount();
		result.call.important = groupTotalDto.getImportantCallCount();
		result.call.newCustomer = groupTotalDto.getNewCallCount();
		result.call.deepPlowing = groupTotalDto.getDeepPlowingCallCount();
		result.call.yCp = groupTotalDto.getYCpCallCount();
		result.call.otherCp = groupTotalDto.getOtherCpCallCount();
		result.call.outside = groupTotalDto.getOutsideCallCount();
		result.call.groupOne = groupTotalDto.getGroupOneCallCount();
		result.call.groupTwo = groupTotalDto.getGroupTwoCallCount();
		result.call.groupThree = groupTotalDto.getGroupThreeCallCount();
		result.call.groupFour = groupTotalDto.getGroupFourCallCount();
		result.call.groupFive = groupTotalDto.getGroupFiveCallCount();

		result.rate.result = toPercentage(result.margin.result, result.sales.result);
		result.rate.plan = toPercentage(result.margin.plan, result.sales.plan);

		// 達成率
		result.number.achievement = toPercentage(result.number.result, result.number.plan);
		result.iss.achievement = toPercentage(result.number.achievement, result.demand.plan);
		result.sales.achievement = toPercentage(result.sales.result, result.sales.plan);
		result.margin.achievement = toPercentage(result.margin.result, result.margin.plan);

		result.rate.achievement = result.rate.result.subtract(result.rate.plan);
		result.deal.achievement = toPercentage(result.deal.result, result.deal.plan);
		result.call.achievement = toPercentage(result.call.result, result.call.plan);

		result.margin.result = toK(result.margin.result);
		result.sales.result = toK(result.sales.result);
		result.margin.plan = toK(result.margin.plan);
		result.sales.plan = toK(result.sales.plan);

		return result;

	}

	/**
	 * 得意先の情報を設定します。
	 * 
	 * @param param
	 * @param month
	 * @param year
	 * @param daily
	 * @param accounts
	 * @param infoList
	 * @param userId
	 * @param commentsDto
	 */
	protected void setAccountInfo(MonthlySumSearchCondition param, int month, int year, List<DailySum> daily,
			List<AccountsDto> accounts, List<AccountInfo> infoList, String userId, MonthlyCommentsDto commentsDto) {

		BigDecimal planDemandCoefficient = commentsDto.getPlanDemandCoefficient();
		if (planDemandCoefficient.compareTo(BigDecimal.ZERO) != 0) {
			planDemandCoefficient = planDemandCoefficient.divide(HUNDRED);
		}

		for (Iterator<AccountsDto> it = accounts.iterator(); it.hasNext();) {

			AccountsDto a = it.next();
			List<Results> reuslts = resultsService.listDailyAccountResult(year, month, a.getId());
			// ユーザーResultでなくアカウントResultとること
			List<Calls> dailyCallList = callsService.listAccountHeldCall(year, month, a.getId(), userId);
			List<AccountResult> accountResults = new ArrayList<AccountResult>();

			// 訪問が中心の行表示なので、Resultsの実績があっても訪問の実績が存在しないと表示しない。
			if (dailyCallList.isEmpty()) {
				it.remove();
			} else {

				AccountInfo info = new AccountInfo();
				infoList.add(info);

				for (int i = 0; i < daily.size(); i++) {
					accountResults.add(new AccountResult());
				}
				BigDecimal number = BigDecimal.ZERO;
				BigDecimal margin = BigDecimal.ZERO;
				BigDecimal sales = BigDecimal.ZERO;
				for (Results r : reuslts) {
					AccountResult accountResult = accountResults.get(r.getDate() - 1);
					accountResult.setNumber(r.getNumber());
					number = number.add(r.getNumber());
					margin = margin.add(r.getMargin());
					sales = sales.add(r.getSales());
				}

				BigDecimal call = BigDecimal.ZERO;
				for (Calls calls : dailyCallList) {
					Calendar statDate = Calendar.getInstance();
					statDate.setTime(calls.getDateStart());
					AccountResult accountResult = accountResults.get(statDate.get(Calendar.DAY_OF_MONTH) - 1);
					accountResult.setCall(new BigDecimal(calls.getHeldCount()));
					accountResult.setPlan(calls.isPlan());
					call = call.add(accountResult.getCall());
				}

				info.setCode(a.getId());
				info.setName(a.getName());

				info.setDaily(accountResults);
				info.setCall(call);
				info.setDeal(a.isDealCount());

				info.setMargin(toK(margin));
				info.setRate(toPercentage(margin, sales));
				info.setSales(toK(sales));
				info.setNumber(number);

				info.setDemand(toDefaultZero((a.getDemandNumber()).multiply(planDemandCoefficient).setScale(0,
						RoundingMode.HALF_UP)));
				info.setIss(toPercentage(info.getNumber(), info.getDemand()));
				info.setMarket(a.getMarketName());
				info.setDeepPolling(a.isGroupDeepPlowing());
				info.setG1(a.isGroupOne());
				info.setG2(a.isGroupTwo());
				info.setG3(a.isGroupThree());
				info.setG4(a.isGroupFour());
				info.setG5(a.isGroupFive());
				info.setImportant(a.isGroupImportant());
				info.setNewCustomer(a.isGroupNew());
				info.setOwn(a.isOwnCount());
				info.setRank(a.getSalesRank());
				info.setYCp(a.isGroupYCp());
				info.setOtherCp(a.isGroupOtherCp());
			}
		}
	}

	protected void setSumTotalColumn(SumColumn total, BigDecimal planCall, BigDecimal resultCall,
			BigDecimal resultNumber, BigDecimal rankA, BigDecimal rankB, BigDecimal rankC, BigDecimal important,
			BigDecimal newCustomer, BigDecimal deepPlowing, BigDecimal yCp, BigDecimal otherCp, BigDecimal outside,
			BigDecimal groupOne, BigDecimal groupTwo, BigDecimal groupThree, BigDecimal groupFour, BigDecimal groupFive) {

		total.setAchievement(toPercentage(planCall, resultCall));
		total.setPlan(planCall);
		total.setResult(resultCall);
		total.setResultNumber(resultNumber);
		total.setRankA(rankA);
		total.setRankB(rankB);
		total.setRankC(rankC);
		total.setImportant(important);
		total.setNewCustomer(newCustomer);
		total.setDeepPlowing(deepPlowing);
		total.setYCp(yCp);
		total.setOtherCp(otherCp);
		total.setOutside(outside);
		total.setGroupOne(groupOne);
		total.setGroupTwo(groupTwo);
		total.setGroupThree(groupThree);
		total.setGroupFour(groupFour);
		total.setGroupFive(groupFive);

	}

	/**
	 * グルーピング毎に訪問実績を設定
	 * 
	 * @param o
	 * @param total
	 */
	protected void setGroupCall(CallCountInfoDto o, SumColumn total) {

		switch (Group.getByCode(o.getGroupType())) {
		case RANK_A:
			total.setRankA(total.getRankA().add(o.getCallCount()));
			break;
		case RANK_B:
			total.setRankB(total.getRankB().add(o.getCallCount()));
			break;
		case RANK_C:
			total.setRankC(total.getRankC().add(o.getCallCount()));
			break;
		case GROUP_IMPORTANT:
			total.setImportant(total.getImportant().add(o.getCallCount()));
			break;
		case GROUP_NEW:
			total.setNewCustomer(total.getNewCustomer().add(o.getCallCount()));
			break;
		case GROUP_DEEP_PLOWING:
			total.setDeepPlowing(total.getDeepPlowing().add(o.getCallCount()));
			break;
		case GROUP_Y_CP:
			total.setYCp(total.getYCp().add(o.getCallCount()));
			break;
		case GROUP_OTHER_CP:
			total.setOtherCp(total.getOtherCp().add(o.getCallCount()));
			break;
		case GROUP_OUTSIDE:
			total.setOutside(total.getOutside().add(o.getCallCount()));
			break;
		case GROUP_ONE:
			total.setGroupOne(total.getGroupOne().add(o.getCallCount()));
			break;
		case GROUP_TWO:
			total.setGroupTwo(total.getGroupTwo().add(o.getCallCount()));
			break;
		case GROUP_THREE:
			total.setGroupThree(total.getGroupThree().add(o.getCallCount()));
			break;
		case GROUP_FOUR:
			total.setGroupFour(total.getGroupFour().add(o.getCallCount()));
			break;
		case GROUP_FIVE:
			total.setGroupFive(total.getGroupFive().add(o.getCallCount()));
			break;
		default:
			break;
		}
	}

	/**
	 * ログイン者の月報コメントより推定需要の取得
	 * 
	 * @param param
	 * @param userId
	 * @return
	 */
	protected MonthlyCommentsDto getPlanDemandCoefficient(MonthlySumSearchCondition param, String userId) {
		MonthlyCommentsDto monthlyComments = monthlyCommentsDao.selectByCondition(param.getYear(), param.getMonth(),
				userId);

		if (monthlyComments == null) {
			monthlyComments = new MonthlyCommentsDto();
		}
		if (monthlyComments.getPlanDemandCoefficient() == null) {
			monthlyComments.setPlanDemandCoefficient(BigDecimal.ZERO);
		}

		if (monthlyComments.getPlanInfoNum() == null) {
			monthlyComments.setPlanInfoNum(BigDecimal.ZERO);
		}

		if (monthlyComments.getPlanVisitNum() == null) {
			monthlyComments.setPlanVisitNum(BigDecimal.ZERO);
		}
		return monthlyComments;
	}

	protected void setupNengatsu(Model model) {
		selectYearsHelper.create(model);
	}

	public static class MonthlySumSearchCondition extends SearchConditionBaseDto {
		private Integer year;
		private Integer month;
		private String div1;
		private String div2;
		private String div3;
		@NotBlank
		private String tanto;
		private String modeChange;

		public String getModeChange() {
			return modeChange;
		}

		public void setModeChange(String modeChange) {
			this.modeChange = modeChange;
		}

		public Integer getYear() {
			return year;
		}

		public void setYear(Integer year) {
			this.year = year;
		}

		public Integer getMonth() {
			return month;
		}

		public void setMonth(Integer month) {
			this.month = month;
		}

		public String getDiv1() {
			return div1;
		}

		public void setDiv1(String div1) {
			this.div1 = div1;
		}

		public String getDiv2() {
			return div2;
		}

		public void setDiv2(String div2) {
			this.div2 = div2;
		}

		public String getDiv3() {
			return div3;
		}

		public void setDiv3(String div3) {
			this.div3 = div3;
		}

		public String getTanto() {
			return tanto;
		}

		public void setTanto(String tanto) {
			this.tanto = tanto;
		}
	}

	public static class AccountInfo {

		String name;
		String code;
		String market;
		String rank;
		boolean important;
		boolean own;
		boolean deal;
		boolean newCustomer;
		boolean deepPolling;
		boolean yCp;
		boolean otherCp;
		boolean g1;
		boolean g2;
		boolean g3;
		boolean g4;
		boolean g5;
		BigDecimal demand;
		BigDecimal iss;
		BigDecimal number;
		BigDecimal sales;
		BigDecimal margin;
		BigDecimal rate;
		BigDecimal call;
		List<AccountResult> daily;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMarket() {
			return market;
		}

		public void setMarket(String market) {
			this.market = market;
		}

		public String getRank() {
			return rank;
		}

		public void setRank(String rank) {
			this.rank = rank;
		}

		public boolean isImportant() {
			return important;
		}

		public void setImportant(boolean important) {
			this.important = important;
		}

		public boolean isOwn() {
			return own;
		}

		public void setOwn(boolean own) {
			this.own = own;
		}

		public boolean isDeal() {
			return deal;
		}

		public void setDeal(boolean deal) {
			this.deal = deal;
		}

		public boolean isNewCustomer() {
			return newCustomer;
		}

		public void setNewCustomer(boolean newCustomer) {
			this.newCustomer = newCustomer;
		}

		public boolean isDeepPolling() {
			return deepPolling;
		}

		public void setDeepPolling(boolean deepPolling) {
			this.deepPolling = deepPolling;
		}

		public boolean isyCp() {
			return yCp;
		}

		public void setYCp(boolean yCp) {
			this.yCp = yCp;
		}

		public boolean isOtherCp() {
			return otherCp;
		}

		public void setOtherCp(boolean otherCp) {
			this.otherCp = otherCp;
		}

		public boolean isG1() {
			return g1;
		}

		public void setG1(boolean g1) {
			this.g1 = g1;
		}

		public boolean isG2() {
			return g2;
		}

		public void setG2(boolean g2) {
			this.g2 = g2;
		}

		public boolean isG3() {
			return g3;
		}

		public void setG3(boolean g3) {
			this.g3 = g3;
		}

		public boolean isG4() {
			return g4;
		}

		public void setG4(boolean g4) {
			this.g4 = g4;
		}

		public boolean isG5() {
			return g5;
		}

		public void setG5(boolean g5) {
			this.g5 = g5;
		}

		public BigDecimal getDemand() {
			return demand;
		}

		public void setDemand(BigDecimal demand) {
			this.demand = demand;
		}

		public BigDecimal getIss() {
			return iss;
		}

		public void setIss(BigDecimal iss) {
			this.iss = iss;
		}

		public BigDecimal getNumber() {
			return number;
		}

		public void setNumber(BigDecimal number) {
			this.number = number;
		}

		public BigDecimal getSales() {
			return sales;
		}

		public void setSales(BigDecimal sales) {
			this.sales = sales;
		}

		public BigDecimal getMargin() {
			return margin;
		}

		public void setMargin(BigDecimal margin) {
			this.margin = margin;
		}

		public BigDecimal getRate() {
			return rate;
		}

		public void setRate(BigDecimal rate) {
			this.rate = rate;
		}

		public BigDecimal getCall() {
			return call;
		}

		public void setCall(BigDecimal call) {
			this.call = call;
		}

		public List<AccountResult> getDaily() {
			return daily;
		}

		public void setDaily(List<AccountResult> daily) {
			this.daily = daily;
		};

	}

	public static class AccountResult {
		BigDecimal number;
		BigDecimal call;
		boolean isPlan;

		public BigDecimal getNumber() {
			return number;
		}

		public void setNumber(BigDecimal number) {
			this.number = number;
		}

		public BigDecimal getCall() {
			return call;
		}

		public void setCall(BigDecimal call) {
			this.call = call;
		}

		public boolean isPlan() {
			return isPlan;
		}

		public void setPlan(boolean isPlan) {
			this.isPlan = isPlan;
		}

	}

	public static class SumTotal {
		private SumColumn demand = new SumColumn();
		private SumColumn iss = new SumColumn();
		private SumColumn number = new SumColumn();
		private SumColumn sales = new SumColumn();
		private SumColumn margin = new SumColumn();
		private SumColumn rate = new SumColumn();
		private SumColumn own = new SumColumn();
		private SumColumn deal = new SumColumn();
		private SumColumn call = new SumColumn();

		public SumColumn getDemand() {
			return demand;
		}

		public void setDemand(SumColumn demand) {
			this.demand = demand;
		}

		public SumColumn getIss() {
			return iss;
		}

		public void setIss(SumColumn iss) {
			this.iss = iss;
		}

		public SumColumn getNumber() {
			return number;
		}

		public void setNumber(SumColumn number) {
			this.number = number;
		}

		public SumColumn getSales() {
			return sales;
		}

		public void setSales(SumColumn sales) {
			this.sales = sales;
		}

		public SumColumn getMargin() {
			return margin;
		}

		public void setMargin(SumColumn margin) {
			this.margin = margin;
		}

		public SumColumn getRate() {
			return rate;
		}

		public void setRate(SumColumn rate) {
			this.rate = rate;
		}

		public SumColumn getOwn() {
			return own;
		}

		public void setOwn(SumColumn own) {
			this.own = own;
		}

		public SumColumn getDeal() {
			return deal;
		}

		public void setDeal(SumColumn deal) {
			this.deal = deal;
		}

		public SumColumn getCall() {
			return call;
		}

		public void setCall(SumColumn call) {
			this.call = call;
		}

	}

	public static class DailySum extends SumColumn {
		private int date;

		private String dayOfWeek;

		public int getDate() {
			return date;
		}

		public void setDate(int date) {
			this.date = date;
		}

		public String getDayOfWeek() {
			return dayOfWeek;
		}

		public void setDayOfWeek(String dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
		}

	}

	public static class SumColumn {

		private BigDecimal plan = BigDecimal.ZERO;
		private BigDecimal result = BigDecimal.ZERO;
		private BigDecimal resultNumber = BigDecimal.ZERO;
		private BigDecimal achievement = BigDecimal.ZERO;
		private BigDecimal rankA = BigDecimal.ZERO;
		private BigDecimal rankB = BigDecimal.ZERO;
		private BigDecimal rankC = BigDecimal.ZERO;
		private BigDecimal important = BigDecimal.ZERO;
		private BigDecimal newCustomer = BigDecimal.ZERO;
		private BigDecimal deepPlowing = BigDecimal.ZERO;
		private BigDecimal yCp = BigDecimal.ZERO;
		private BigDecimal otherCp = BigDecimal.ZERO;
		private BigDecimal outside = BigDecimal.ZERO;
		private BigDecimal groupOne = BigDecimal.ZERO;
		private BigDecimal groupTwo = BigDecimal.ZERO;
		private BigDecimal groupThree = BigDecimal.ZERO;
		private BigDecimal groupFour = BigDecimal.ZERO;
		private BigDecimal groupFive = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		public BigDecimal getResultNumber() {
			return resultNumber;
		}

		public void setResultNumber(BigDecimal resultNumber) {
			this.resultNumber = resultNumber;
		}

		public BigDecimal getAchievement() {
			return achievement;
		}

		public void setAchievement(BigDecimal achievement) {
			this.achievement = achievement;
		}

		public BigDecimal getRankA() {
			return rankA;
		}

		public void setRankA(BigDecimal rankA) {
			this.rankA = rankA;
		}

		public BigDecimal getRankB() {
			return rankB;
		}

		public void setRankB(BigDecimal rankB) {
			this.rankB = rankB;
		}

		public BigDecimal getRankC() {
			return rankC;
		}

		public void setRankC(BigDecimal rankC) {
			this.rankC = rankC;
		}

		public BigDecimal getImportant() {
			return important;
		}

		public void setImportant(BigDecimal important) {
			this.important = important;
		}

		public BigDecimal getNewCustomer() {
			return newCustomer;
		}

		public void setNewCustomer(BigDecimal newCustomer) {
			this.newCustomer = newCustomer;
		}

		public BigDecimal getDeepPlowing() {
			return deepPlowing;
		}

		public void setDeepPlowing(BigDecimal deepPlowing) {
			this.deepPlowing = deepPlowing;
		}

		public BigDecimal getYCp() {
			return yCp;
		}

		public void setYCp(BigDecimal yCp) {
			this.yCp = yCp;
		}

		public BigDecimal getOtherCp() {
			return otherCp;
		}

		public void setOtherCp(BigDecimal otherCp) {
			this.otherCp = otherCp;
		}

		public BigDecimal getOutside() {
			return outside;
		}

		public void setOutside(BigDecimal outside) {
			this.outside = outside;
		}

		public BigDecimal getGroupOne() {
			return groupOne;
		}

		public void setGroupOne(BigDecimal groupOne) {
			this.groupOne = groupOne;
		}

		public BigDecimal getGroupTwo() {
			return groupTwo;
		}

		public void setGroupTwo(BigDecimal groupTwo) {
			this.groupTwo = groupTwo;
		}

		public BigDecimal getGroupThree() {
			return groupThree;
		}

		public void setGroupThree(BigDecimal groupThree) {
			this.groupThree = groupThree;
		}

		public BigDecimal getGroupFour() {
			return groupFour;
		}

		public void setGroupFour(BigDecimal groupFour) {
			this.groupFour = groupFour;
		}

		public BigDecimal getGroupFive() {
			return groupFive;
		}

		public void setGroupFive(BigDecimal groupFive) {
			this.groupFive = groupFive;
		}
	}

	public static class MonthlySumUserInfo {
		private String firstName;
		private String lastName;
		private String eigyoSosiki;

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEigyoSosiki() {
			return eigyoSosiki;
		}

		public void setEigyoSosiki(String eigyoSosiki) {
			this.eigyoSosiki = eigyoSosiki;
		}
	}
}
