package jp.co.yrc.mv.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.WeekAccountCallDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.service.CallsService;

/**
 * 週報用コントローラクラス。
 *
 */
@Controller
@Transactional
public class WeeklyReportController {

	@Autowired
	CallsService callsService;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SoshikiHelper soshikiHelper;

	/** 感度：ネガティブな情報 */
	public static final String SENSITIVITY_NEGATIVE = "Negative";

	/** 感度：ポジティブな情報 */
	public static final String SENSITIVITY_POSITIVE = "Positive";

	@RequestMapping(value = "/weeklyReport.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {
		model.addAttribute(param);
		WeeklyReport wr = new WeeklyReport();

		Calendar c = DateUtils.getCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(DateUtils.toYearMonth(c));
		param.setWeek(c.get(Calendar.WEEK_OF_MONTH));
		selectYearsHelper.createYearMonthWeek(model);

		// 前週、今週の日付一覧を設定
		setDateOfWeek(param, wr);

		model.addAttribute("weeklyReport", wr);
		return "weeklyReport";
	}

	@RequestMapping(value = "/weeklyReport.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {
		model.addAttribute(param);
		WeeklyReport wr = new WeeklyReport();

		selectYearsHelper.createYearMonthWeek(model);
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		// 前週、今週の日付一覧を設定
		setDateOfWeek(param, wr);

		// 前週の行動を設定
		List<WeekAccountCallDto> resultList = callsService.sumAccountWeekCall(tanto, getLastMonday(param).getTime(),
				getThisSunday(param).getTime());
		setWeekCalls(wr.getLastWeekCalls(), resultList, true);

		// 今週の行動予定を設定
		resultList = callsService.sumAccountWeekCall(tanto, getThisMonday(param).getTime(),
				getNextSunday(param).getTime());
		setWeekCalls(wr.getThisWeekCalls(), resultList, false);

		// 訪問計画・訪問実績の合計値を設定
		for (DailyAccountCall call : wr.getLastWeekCalls()) {
			wr.setLastResultTotal(wr.getLastResultTotal() + call.getResult());
			wr.setLastPlanTotal(wr.getLastPlanTotal() + call.getPlan());
		}
		for (DailyAccountCall call : wr.getThisWeekCalls()) {
			wr.setThisPlanTotal(wr.getThisPlanTotal() + call.getPlan());
		}

		// 報告内容の設定
		List<CallsDto> callsList = callsService.findSensitivityCommentInfo(tanto, getLastMonday(param).getTime(),
				getThisSunday(param).getTime());

		for (CallsDto call : callsList) {
			Report report = new Report();
			BeanUtils.copyProperties(call, report);
			// -1以下：コメント無し、0：未読無し、1以上：未読有り
			report.commentReadCount = call.getCommentCount() < 1 ? -1 : call.getCommentReadCount() < 1 ? 1 : 0;

			if (SENSITIVITY_POSITIVE.equals(call.getSensitivity())) {
				wr.getGoodReport().add(report);
			} else if (SENSITIVITY_NEGATIVE.equals(call.getSensitivity())) {
				wr.getNegativeReport().add(report);
			} else {
				wr.getOtherReport().add(report);
			}
		}
		model.addAttribute("weeklyReport", wr);

		return "weeklyReport";
	}

	/**
	 * 週の行動を表示用DTOに設定。<br>
	 * 前週の場合、訪問計画と訪問実績を取得。訪問実績のある得意先のみ得意先リストに追加。<br>
	 * 今週の場合、訪問計画のみを取得。
	 * 
	 * @param weekCallsList
	 *            週の行動表示用リスト
	 * @param resultList
	 *            検索結果
	 * @param isLastWeek
	 *            前週の行動であるか
	 */
	void setWeekCalls(List<DailyAccountCall> weekCallsList, List<WeekAccountCallDto> resultList, boolean isLastWeek) {
		for (DailyAccountCall daily : weekCallsList) {
			for (WeekAccountCallDto result : resultList) {
				if (daily.getDate().compareTo(result.getDateStart()) == 0) {
					// 訪問計画
					daily.setPlan(daily.getPlan() + result.getPlannedCount().intValue());
					if (isLastWeek
							&& !(result.getPlannedCount().intValue() > 0 && result.getCallCount().intValue() == 0)) {
						// 訪問実績
						daily.setResult(daily.getResult() + result.getCallCount().intValue());
						// 得意先リスト
						setAccountList(daily, result);
					}
					if (!isLastWeek
							&& !(result.getCallCount().intValue() > 0 && result.getPlannedCount().intValue() == 0)) {
						// 得意先リスト
						setAccountList(daily, result);
					}
				}
			}
		}
	}

	void setAccountList(DailyAccountCall daily, WeekAccountCallDto result) {
		if (daily.accountsList.size() == 0) {
			daily.accountsList.add(result.getAccountName());
		} else {
			for (String account : daily.accountsList) {
				if (!account.equals(result.getAccountName())) {
					daily.accountsList.add(result.getAccountName());
					break;
				}
			}
		}
	}

	/**
	 * 表示用DTOに前週、今週の日付一覧を設定。
	 * 
	 * @param param
	 * @param wr
	 */
	void setDateOfWeek(SearchCondition param, WeeklyReport wr) {
		Calendar lastMonday = getLastMonday(param);
		Calendar thisMonday = getThisMonday(param);
		set7day(wr.getLastWeekCalls(), lastMonday);
		set7day(wr.getThisWeekCalls(), thisMonday);
	}

	void set7day(List<DailyAccountCall> weekCalls, Calendar monday) {
		for (int i = 0; i < 6; i++) {
			DailyAccountCall dailyAccountCall = new DailyAccountCall();
			dailyAccountCall.setDate(monday.getTime());
			weekCalls.add(dailyAccountCall);
			// 1日追加
			monday = DateUtils.getYearMonthDateCalendar(monday, 1);
		}
	}

	Calendar getLastMonday(SearchCondition param) {
		Calendar cal = getCalendar(param);
		cal.add(Calendar.WEEK_OF_MONTH, -1);
		cal.set(Calendar.DAY_OF_WEEK, 2);
		return cal;
	}

	Calendar getThisSunday(SearchCondition param) {
		Calendar cal = getCalendar(param);
		cal.set(Calendar.DAY_OF_WEEK, 1);
		return cal;
	}

	Calendar getThisMonday(SearchCondition param) {
		Calendar cal = getCalendar(param);
		cal.set(Calendar.DAY_OF_WEEK, 2);
		return cal;
	}

	Calendar getNextSunday(SearchCondition param) {
		Calendar cal = getCalendar(param);
		cal.add(Calendar.WEEK_OF_MONTH, 1);
		cal.set(Calendar.DAY_OF_WEEK, 1);
		return cal;
	}

	Calendar getCalendar(SearchCondition param) {
		Calendar cal = DateUtils.getCalendar();
		cal.set(Calendar.YEAR, param.getYear());
		cal.set(Calendar.MONTH, DateUtils.toMonth(param.getMonth()) - 1);
		cal.set(Calendar.WEEK_OF_MONTH, param.getWeek());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/**
	 * 画面表示用DTO
	 *
	 */
	public static class WeeklyReport {
		List<DailyAccountCall> lastWeekCalls = new ArrayList<>();
		List<DailyAccountCall> thisWeekCalls = new ArrayList<>();
		int thisPlanTotal;
		int lastResultTotal;
		int lastPlanTotal;
		List<Report> goodReport = new ArrayList<>();
		List<Report> negativeReport = new ArrayList<>();
		List<Report> otherReport = new ArrayList<>();

		public List<DailyAccountCall> getLastWeekCalls() {
			return lastWeekCalls;
		}

		public void setLastWeekCalls(List<DailyAccountCall> lastWeekCalls) {
			this.lastWeekCalls = lastWeekCalls;
		}

		public List<DailyAccountCall> getThisWeekCalls() {
			return thisWeekCalls;
		}

		public void setThisWeekCalls(List<DailyAccountCall> thisWeekCalls) {
			this.thisWeekCalls = thisWeekCalls;
		}

		public int getThisPlanTotal() {
			return thisPlanTotal;
		}

		public void setThisPlanTotal(int thisPlanTotal) {
			this.thisPlanTotal = thisPlanTotal;
		}

		public int getLastResultTotal() {
			return lastResultTotal;
		}

		public void setLastResultTotal(int lastResultTotal) {
			this.lastResultTotal = lastResultTotal;
		}

		public int getLastPlanTotal() {
			return lastPlanTotal;
		}

		public void setLastPlanTotal(int lastPlanTotal) {
			this.lastPlanTotal = lastPlanTotal;
		}

		public List<Report> getGoodReport() {
			return goodReport;
		}

		public void setGoodReport(List<Report> goodReport) {
			this.goodReport = goodReport;
		}

		public List<Report> getNegativeReport() {
			return negativeReport;
		}

		public void setNegativeReport(List<Report> negativeReport) {
			this.negativeReport = negativeReport;
		}

		public List<Report> getOtherReport() {
			return otherReport;
		}

		public void setOtherReport(List<Report> otherReport) {
			this.otherReport = otherReport;
		}
	}

	/**
	 * 画面表示：報告用DTO
	 *
	 */
	public static class Report {
		String id;
		String infoDiv;
		String comFreeC;
		int commentReadCount;
		int commentCount;
		boolean commentEntered;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getInfoDiv() {
			return infoDiv;
		}

		public void setInfoDiv(String infoDiv) {
			this.infoDiv = infoDiv;
		}

		public String getComFreeC() {
			return comFreeC;
		}

		public void setComFreeC(String comFreeC) {
			this.comFreeC = comFreeC;
		}

		public int getCommentReadCount() {
			return commentReadCount;
		}

		public void setCommentReadCount(int commentReadCount) {
			this.commentReadCount = commentReadCount;
		}

		public int getCommentCount() {
			return commentCount;
		}

		public void setCommentCount(int commentCount) {
			this.commentCount = commentCount;
		}

		public boolean isCommentEntered() {
			return commentEntered;
		}

		public void setCommentEntered(boolean commentEntered) {
			this.commentEntered = commentEntered;
		}
	}

	/**
	 * 画面表示：週の行動用DTO
	 *
	 */
	public static class DailyAccountCall {
		Date date;
		List<String> accountsList = new ArrayList<>();
		int plan;
		int result;

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public int getPlan() {
			return plan;
		}

		public void setPlan(int plan) {
			this.plan = plan;
		}

		public int getResult() {
			return result;
		}

		public void setResult(int result) {
			this.result = result;
		}

		public List<String> getAccountsList() {
			return accountsList;
		}

		public void setAccountsList(List<String> accountsList) {
			this.accountsList = accountsList;
		}
	}

	public static class SearchCondition extends SearchConditionBaseDto {
		protected Integer week;

		public Integer getWeek() {
			return week;
		}

		public void setWeek(Integer week) {
			this.week = week;
		}
	}
}
