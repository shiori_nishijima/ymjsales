package jp.co.yrc.mv.web;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.DailyCommentDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UnvisitAccountDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsService;

@Controller
@Transactional
public class UnvisitAccountListController {

	@Value("${show.year.range.from}")
	int rangeFrom;

	@Value("${show.year.range.to}")
	int rangeTo;

	@Autowired
	CallsService callsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Value("${limit}")
	int limit;

	@RequestMapping(value = "/unvisitAccountList.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		param.setYear(c.get(Calendar.YEAR));
		// 連動させるプルダウンの仕様上年が入る
		param.setMonth(c.get(Calendar.MONTH) + 1);

		selectYearsHelper.create(model);
		model.addAttribute(param);
		return "unvisitAccountList";

	}

	@RequestMapping(value = "/unvisitAccountList.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {

		selectYearsHelper.create(model);

		List<UnvisitAccountDto> result = listResult(param, principal);

		if (result == null) {
			model.addAttribute("limitOver", true);
		} else {
			model.addAttribute("result", result);
		}

		model.addAttribute(param);
		return "unvisitAccountList";
	}

	protected List<UnvisitAccountDto> listResult(SearchCondition param, Principal principal) {

		String paramDiv1 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		List<String> salesCompanyCodeList = soshikiHelper.createSearchSalesCompanyCodeList(param.getDiv1(), userInfo,
				param.getYear(), param.getMonth());

		if (limit < callsService.countUnvisitAccounts(param.getYear(), param.getMonth(), tanto, salesCompanyCodeList,
				div2, div3)) {
			return null;
		}

		List<UnvisitAccountDto> result = callsService.listUnvisitAccounts(param.getYear(), param.getMonth(), tanto,
				salesCompanyCodeList, paramDiv1, div2, div3);
		return result;
	}

	public static class SearchResult extends DailyCommentDto {

		private boolean confirm;
		private String visitDateString;
		private String confirmDateString;
		private boolean commentExists;
		private String targetDate;

		public boolean isCommentExists() {
			return commentExists;
		}

		public void setCommentExists(boolean commentExists) {
			this.commentExists = commentExists;
		}

		public boolean isConfirm() {
			return confirm;
		}

		public void setConfirm(boolean confirm) {
			this.confirm = confirm;
		}

		public String getVisitDateString() {
			return visitDateString;
		}

		public void setVisitDateString(String visitDateString) {
			this.visitDateString = visitDateString;
		}

		public String getConfirmDateString() {
			return confirmDateString;
		}

		public void setConfirmDateString(String confirmDateString) {
			this.confirmDateString = confirmDateString;
		}

		public String getTargetDate() {
			return targetDate;
		}

		public void setTargetDate(String targetDate) {
			this.targetDate = targetDate;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {

	}

}
