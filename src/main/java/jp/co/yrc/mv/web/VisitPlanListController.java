package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.CallsAccountsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.AccountsService;
import jp.co.yrc.mv.service.CallsAccountsService;
import jp.co.yrc.mv.service.CallsService;

/**
 * 訪問計画一覧用コントローラクラス。
 *
 */
@Controller
@Transactional
public class VisitPlanListController {

	@Autowired
	AccountsService accountsService;
	
	@Autowired
	CallsService callsService;

	@Autowired
	CallsAccountsService callsAccountsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;
	
	public static final String MODE_PLAN_HELD = "planHeld";
	public static final String MODE_NO_HELD = "noHeld";
	public static final String MODE_OTHER = "other";

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/visitPlanList.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		List<DailyCallInfo> header = createHeader(c);
		// 縦合計用
		DailyCallInfo total = new DailyCallInfo();
		total.total = true;
		header.add(0, total);
		model.addAttribute("header", header);
		// dummyで空の結果を入れる（何も入れないと画面で落ちる）
		model.addAttribute("planInfoList", Collections.EMPTY_LIST);
		model.addAttribute(param);

		selectYearsHelper.create(model);
		model.addAttribute(param);
		return "visitPlanList";
	}

	/**
	 * 検索時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/visitPlanList.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar(param.getYear(), param.getMonth());
		List<DailyCallInfo> header = createHeader(c);
		model.addAttribute("header", header);
		model.addAttribute(param);
		selectYearsHelper.create(model);

		Collection<AccountRow> result = listResult(param, principal, header);
		model.addAttribute("planInfoList", result);

		return "visitPlanList";
	}

	/**
	 * 指定した年月の持つ日数分の列ヘッダテンプレートを作成します。
	 * 
	 * @param c 指定年月を表すカレンダー
	 * @return 日情報リスト
	 */
	protected List<DailyCallInfo> createHeader(Calendar c) {
		SimpleDateFormat formatter = DateUtils.getDayOfWeekFormat();

		// カレンダー
		List<DailyCallInfo> header = new ArrayList<>();
		for (int i = 1, max = c.getActualMaximum(Calendar.DATE); i <= max; i++, c.add(Calendar.DATE, 1)) {
			DailyCallInfo daily = new DailyCallInfo();
			daily.date = i;
			daily.dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			daily.dayOfWeekLabel = formatter.format(c.getTime());
			header.add(daily);
		}

		return header;
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param header  ヘッダ情報
	 * @return 結果リスト
	 */
	protected Collection<AccountRow> listResult(SearchCondition param, Principal principal, List<DailyCallInfo> header) {

		// 個人に紐づくデータを全て表示するため、組織条件を利用しない。
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());
		Map<String, AccountRow> map = new TreeMap<>();
		
		// 訪問得意先をベースに得意先の一行データ枠を作成する
		List<CallsAccountsDto> accountList = callsAccountsService.find(param.getYear(), param.getMonth(), tanto);
		for (CallsAccountsDto dto : accountList) {
			createAccountRow(header, map, dto.getAccountId(), dto.getAccountName());
		}
		
		// 社内同行訪問がある得意先の場合は自動で追加。SFAのTOP画面に合わせる。
		// 先に得意先を確定させないと順番によって件数が追加されなくなる場合が出てくる。
		List<CallCountInfoDto> dailyUserCall = callsService.sumDailyAccountCall(param.getYear(), param.getMonth(), tanto);
		for (CallCountInfoDto o : dailyUserCall) {
			AccountRow row = map.get(o.getGroupType());
			if (row == null
					&& o.getCallUserCount().intValue() > 0) {
				// 社内同行訪問がある得意先の場合は自動で追加。SFAのTOP画面に合わせる。
				AccountsDto account = accountsService.findAccountInfoById(param.getYear(), param.getMonth(),
						o.getGroupType());
				createAccountRow(header, map, account.getId(), account.getName());
			}
		}

		// 件数の設定
		for (CallCountInfoDto o : dailyUserCall) {
			AccountRow row = map.get(o.getGroupType());
			if (row != null) {
				Date date = o.getDateStart();
				int day = DateUtils.getDay(date);

				DailyCallInfo dailyInfo = row.getDailyInfos().get(day - 1);
				dailyInfo.setResult(dailyInfo.getResult().add(toDefaultZero(o.getCallCount())));
				dailyInfo.setPlan(dailyInfo.getPlan().add(toDefaultZero(o.getPlannedCount())));

				// 行合計
				row.setResult(row.getResult().add(toDefaultZero(o.getCallCount())));
				row.setPlan(row.getPlan().add(toDefaultZero(o.getPlannedCount())));
			}
		}

		DailyCallInfo total = new DailyCallInfo();
		total.total = true;
		header.add(0, total);
		// mode設定
		Collection<AccountRow> collection = map.values();
		for (AccountRow info : collection) {
			info.setMode(info.plan.intValue() > 0 && info.result.intValue() > 0 ? MODE_PLAN_HELD : info.result.intValue() == 0 ? MODE_NO_HELD : MODE_OTHER);
		}
		return collection;
	}

	/**
	 * 得意先行用のオブジェクトを作成します。
	 * 
	 * @param header ヘッダ
	 * @param map データマップ
	 * @param accountId 得意先ID
	 * @param accountName 得意先名
	 */
	protected void createAccountRow(List<DailyCallInfo> header, Map<String, AccountRow> map, String accountId, String accountName) {
		AccountRow row = new AccountRow();
		row.setAccountId(accountId);
		row.setAccountName(accountName);

		// 1ヶ月分の日ごとの入れ物を作る
		for (DailyCallInfo dailyHeader : header) {
			DailyCallInfo daily = new DailyCallInfo();
			daily.setDate(dailyHeader.getDate());
			row.getDailyInfos().add(daily);
		}
		map.put(row.accountId, row);
	}

	public static class DailyCallInfo {
		int date;
		int dayOfWeek;
		String dayOfWeekLabel;
		boolean total;

		BigDecimal plan = BigDecimal.ZERO;
		BigDecimal result = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		public int getDate() {
			return date;
		}

		public void setDate(int date) {
			this.date = date;
		}

		public int getDayOfWeek() {
			return dayOfWeek;
		}

		public void setDayOfWeek(int dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
		}

		public String getDayOfWeekLabel() {
			return dayOfWeekLabel;
		}

		public void setDayOfWeekLabel(String dayOfWeekLabel) {
			this.dayOfWeekLabel = dayOfWeekLabel;
		}

		public boolean isTotal() {
			return total;
		}

		public void setTotal(boolean total) {
			this.total = total;
		}

	}

	public static class AccountRow {
		List<DailyCallInfo> dailyInfos = new ArrayList<>();
		String accountName;
		String accountId;
		String mode;
		// 行合計（計画）
		BigDecimal plan = BigDecimal.ZERO;
		// 行合計（実績）
		BigDecimal result = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		public List<DailyCallInfo> getDailyInfos() {
			return dailyInfos;
		}

		public void setDailyInfos(List<DailyCallInfo> dailyInfos) {
			this.dailyInfos = dailyInfos;
		}

		public String getAccountName() {
			return accountName;
		}

		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}

		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {

	}

}
