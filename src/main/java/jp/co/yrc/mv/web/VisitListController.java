package jp.co.yrc.mv.web;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.CorpCd;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.CallsSearchConditionDto;
import jp.co.yrc.mv.dto.CallsUsersDto;
import jp.co.yrc.mv.dto.DailyCommentDto;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.CallsFiles;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.helper.SearchDivHelper;
import jp.co.yrc.mv.helper.SelectCallsInfoDivHelper;
import jp.co.yrc.mv.helper.SelectMarketHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.CallsUsersService;
import jp.co.yrc.mv.service.DailyCommentsService;
import jp.co.yrc.mv.service.MarketInfosService;
import jp.co.yrc.mv.service.MstEigyoSosikiService;
import jp.co.yrc.mv.service.SelectionItemsService;
import jp.co.yrc.mv.service.UsersService;

/**
 * 訪問実績用コントローラクラス。
 *
 */
@Controller
@Transactional
public class VisitListController {

	@Value("${show.year.range.from}")
	int rangeFrom;

	@Value("${show.year.range.to}")
	int rangeTo;

	@Autowired
	DailyCommentsService dailyCommentsService;

	@Autowired
	CallsService callsService;

	@Autowired
	CallsUsersService callsUsersService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SearchDivHelper searchDivHelper;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	UsersService usersService;

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	MarketInfosService marketInfosService;

	@Autowired
	SelectCallsInfoDivHelper selectCallsInfoDivHelper;

	@Autowired
	SelectMarketHelper selectMarketHelper;
	
	@Autowired
	DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@Value("${limit}")
	int limit;

	@Value("${sfa.call.url}")
	String sfaCallUrl;

	public static final String MODE_VIST_LIST = "visitList";
	public static final String MODE_VISIT_LIST_ONE = "visitListOne";
	public static final String MODE_VISIT_VIEW = "visitListView";
	public static final String MODE_DAILY_REPORT_ADMIN = "dailyReportAdmin";
	public static final String MODE_DAILY_REPORT_SALES = "dailyReportSales";
	public static final String MODE_TECH = "tech";

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/visitList.html", method = RequestMethod.GET)
	public String initVisitList(VisitListSearchCondition param, Model model, Principal principal) {
		model.addAttribute("mode", MODE_VIST_LIST);
		init(param, model, principal);
		selectMarketHelper.createMarket(model);
		// デフォルトで実績、情報ありのみ表示-2015/10/05(月)のメールによる変更依頼
		param.heldOnly = true;
		param.hasInfoOnly = true;
		return "visitList";
	}

	/**
	 * 検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = { "/visitList.html", "/visitListView.html" }, method = RequestMethod.POST)
	public String findVisitList(VisitListSearchCondition param, Model model, Principal principal) {
		model.addAttribute("mode", param.getMode() == null ? MODE_VIST_LIST : param.getMode());
		find(param, model, principal);
		selectMarketHelper.createMarket(model);
		return "visitList";
	}

	/**
	 * 技サ用訪問実績初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/tech/visitList.html", method = RequestMethod.GET)
	public String initTechVisitList(VisitListSearchCondition param, Model model, Principal principal) {
		model.addAttribute("mode", MODE_TECH);
		init(param, model, principal);
		selectCallsInfoDivHelper.createInfoDiv(model);
		param.heldOnly = true;
		param.hasInfoOnly = true;
		return "visitList";
	}

	/**
	 * 技サ用訪問実績検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/tech/visitList.html", method = RequestMethod.POST)
	public String findTechVisitList(VisitListSearchCondition param, Model model, Principal principal) {
		model.addAttribute("mode", MODE_TECH);
		selectCallsInfoDivHelper.createInfoDiv(model);
		find(param, model, principal, true);
		return "visitList";
	}

	/**
	 * 訪問実績CSVダウンロード押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param fileName ファイル名
	 * @param header CSVヘッダ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = { "/visitListCsvDownload.html" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ByteArrayResource> downloadCsv(VisitListSearchCondition param, Model model, Principal principal) {
		return createCsv(param, principal, false);
	}

	/**
	 * 技サ用訪問実績CSVダウンロード押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param fileName ファイル名
	 * @param header CSVヘッダ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = { "/tech/visitListCsvDownload.html" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ByteArrayResource> donwloadTechCsv(VisitListSearchCondition param, Model model, Principal principal) {
		return createCsv(param, principal, true);
	}
	
	/**
	 * 訪問実績（技サ用含む）初期表示時の共通処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 */
	protected void init(VisitListSearchCondition param, Model model, Principal principal) {
		// Fromは必須。デフォルトとして当月1日を設定。
		Calendar c = DateUtils.getYearMonthCalendar();
		param.setDateFrom(c.getTime());
		setMinMaxDate(param);
		model.addAttribute(param);
	}

	/**
	 * CSVを作成します。
	 * 
	 * @param param パラメータ
	 * @param fileName ファイル名
	 * @param header CSVヘッダ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @param tech 技サ用訪問実績かどうか
	 * @return CSVデータ
	 */
	protected ResponseEntity<ByteArrayResource> createCsv(VisitListSearchCondition param, Principal principal, boolean tech) {

		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);
		// ロケールごとにCSVのファイル名、ヘッダを設定
		String fileName = message.getMessage("visitList.csv.filename");
		String header = message.getMessage("visitList.csv.header");

		List<SearchResult> list = listResult(param, principal, tech);
		HttpHeaders headers = new HttpHeaders();
		try {
			headers.setContentDispositionFormData("attachment", new URLCodec().encode(fileName));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		headers.setContentType(MediaType.valueOf("text/csv;charset=UTF-8"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos)) {
			// BOM付きのCSVファイルを生成する
			bos.write(0xef);
			bos.write(0xbb);
			bos.write(0xbf);
			try (OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("UTF-8"));
					BufferedWriter bw = new BufferedWriter(osw);
					ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {

				String csvHeader = header;
				String[] headerArr = csvHeader.split(",");

				csvWriter.writeHeader(headerArr);

				if (list != null) {
					for (SearchResult sr : list) {
						List<Object> csv = new ArrayList<>();
						csv.add(sr.getName());
						csv.add(sr.getAccountsName());
						csv.add(sr.getMarketName());
						csv.add(sr.getVisitDateStart());
						csv.add(sr.getDescription());
						csv.add(sr.getParentType());
						csv.add(sr.getStatus());
						StringBuilder divC = null;
						if (!sr.getDivisionC().isEmpty()) {
							divC = new StringBuilder();
							for (String s : sr.getDivisionC()) {
								divC.append(s);
								divC.append(",");
							}
							divC.delete(divC.length() - 1, divC.length());
						}
						csv.add(divC);

						csv.add(sr.getAccountDepartmentName());
						csv.add(sr.getAccountSalesOfficeName());
						csv.add(sr.getAssignedUserLastName() + " " + sr.getAssignedUserFirstName());

						StringBuilder callsUsers = null;
						if (!sr.getCallsUsers().isEmpty()) {
							callsUsers = new StringBuilder();
							for (String s : sr.getCallsUsers()) {
								callsUsers.append(s);
								callsUsers.append(",");
							}
							callsUsers.delete(callsUsers.length() - 1, callsUsers.length());
						}
						csv.add(callsUsers);
						csv.add(sr.getPlannedCount() < 0 ? 0 : sr.getPlannedCount());
						csv.add(sr.getHeldCount() < 0 ? 0 : sr.getHeldCount());
						csv.add(sr.getInfoDiv());
						csv.add(sr.getMaker());
						csv.add(sr.getKind());
						csv.add(sr.getCategory());
						csv.add(sr.getSensitivity());
						csv.add(sr.getLike() < 0 ? 0 : sr.getLike());

						// 訪問,件名,得意先名,販路,訪問日時,内容,関連先,ステータス,訪問理由,部門,営業所,担当者,社内同行者,予定数,実績数,情報区分,メーカー,品種,カテゴリ,感度,注目マーク数

						csvWriter.write(csv);
					}
				}
				csvWriter.flush();
			}
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), headers, HttpStatus.OK);
	}

	/**
	 * ファイル1をダウンロードします。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = { "/visitListDownload.html",
			"/tech/visitListDownload.html" }, method = RequestMethod.POST, params = "downloadId1")
	@ResponseBody
	public ResponseEntity<ByteArrayResource> download1(FileName param, Model model, Principal principal) {

		String id = param.getDownloadId1();
		CallsFiles cf = StringUtils.isEmpty(param.techCallId) ? callsService.selectFile1(id)
				: callsService.selectFile14Tech(id);
		String fileName = cf.getFile1Name();
		byte[] file = cf.getFile1();
		return createDownloadData(fileName, file);
	}

	/**
	 * ファイル2をダウンロードします。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = { "/visitListDownload.html",
			"/tech/visitListDownload.html" }, method = RequestMethod.POST, params = "downloadId2")
	@ResponseBody
	public ResponseEntity<ByteArrayResource> download2(FileName param, Model model, Principal principal) {

		String id = param.getDownloadId2();

		CallsFiles cf = StringUtils.isEmpty(param.techCallId) ? callsService.selectFile2(id)
				: callsService.selectFile24Tech(id);
		String fileName = cf.getFile2Name();
		byte[] file = cf.getFile2();

		return createDownloadData(fileName, file);
	}

	/**
	 * ファイル3をダウンロードします。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = { "/visitListDownload.html",
			"/tech/visitListDownload.html" }, method = RequestMethod.POST, params = "downloadId3")
	@ResponseBody
	public ResponseEntity<ByteArrayResource> download3(FileName param, Model model, Principal principal) {

		String id = param.getDownloadId3();
		CallsFiles cf = StringUtils.isEmpty(param.techCallId) ? callsService.selectFile3(id)
				: callsService.selectFile34Tech(id);
		String fileName = cf.getFile3Name();
		byte[] file = cf.getFile3();

		return createDownloadData(fileName, file);
	}

	/**
	 * ダウンロード用データを生成します。
	 * 
	 * @param fileName ファイル名
	 * @param file ファイルデータ
	 * @return レスポンスデータ
	 */
	protected ResponseEntity<ByteArrayResource> createDownloadData(String fileName, byte[] file) {
		HttpHeaders headers = new HttpHeaders();
		try {
			headers.setContentDispositionFormData("attachment", new URLCodec().encode(fileName));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}
		headers.setContentType(MediaType.valueOf("application/force-download"));
		return new ResponseEntity<>(new ByteArrayResource(file), headers, HttpStatus.OK);
	}

	/**
	 * 日報詳細（管理者）検索処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportAdmin.html", method = RequestMethod.POST)
	public String findDailyReportAdmin(DailyReportParam param, Model model, Principal principal) {

		model.addAttribute("mode", MODE_DAILY_REPORT_ADMIN);

		findDailyReport(param, model, principal);

		return "visitList";
	}

	/**
	 * 日報詳細（セールス）検索処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportSales.html", method = RequestMethod.POST)
	public String findDailyReportSales(DailyReportParam param, Model model, Principal principal) {

		model.addAttribute("mode", MODE_DAILY_REPORT_SALES);

		findDailyReport(param, model, principal);

		return "visitList";
	}

	/**
	 * 主キーで訪問を検索します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = {"/visitListOne.html/{id}/{dateFrom}", "/tech/visitListOne.html/{id}/{dateFrom}"} , method = RequestMethod.GET)
	public String findVisitById(@PathVariable String id, 
			@PathVariable @DateTimeFormat(pattern = "yyyyMMdd") Date dateFrom, Model model, Principal principal) {
		VisitListSearchCondition param = new VisitListSearchCondition();
		param.setId(id);
		param.setDateFrom(dateFrom);
		model.addAttribute("mode", MODE_VISIT_LIST_ONE);
		find(param, model, principal);
		return "visitList";
	}
	
	/**
	 * 検索を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 */
	protected void find(VisitListSearchCondition param, Model model, Principal principal) {
		find(param, model, principal, false);
	}

	/**
	 * 検索を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @param tech 技サ用訪問実績かどうか
	 */
	protected void find(VisitListSearchCondition param, Model model, Principal principal, boolean tech) {
		setMinMaxDate(param);
		List<SearchResult> result = listResult(param, principal, tech);
		if (result == null) {
			model.addAttribute("limitOver", true);
		} else {
			model.addAttribute("result", result);
		}
		model.addAttribute(param);
	}

	/**
	 * 日報を検索します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 */
	protected void findDailyReport(DailyReportParam param, Model model, Principal principal) {
		Calendar c = Calendar.getInstance();
		c.setTime(param.date);

		List<String> corpCdList = new ArrayList<String>();
		for (CorpCd corpCd : CorpCd.values()) {
			corpCdList.add(corpCd.getCode());
		}
		DailyCommentDto comment = dailyCommentsService.getDailyComments(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1,
				c.get(Calendar.DATE), param.userId, corpCdList);

		if (comment == null) {
			comment = new DailyCommentDto();
			comment.setUserId(param.userId);
		}

		UserSosikiDto user = usersService.selectUserByDivCd(param.userId, corpCdList,
				StringUtils.isEmpty(param.div1) ? null : param.div1,
				StringUtils.isEmpty(param.div2) ? null : param.div2,
				StringUtils.isEmpty(param.div3) ? null : param.div3, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		VisitListSearchCondition condition = new VisitListSearchCondition();

		condition.setIgnoreSosikiCondition(true); // 所属外の訪問を出すため、ユーザIDで取得
		condition.setDiv1(StringUtils.isEmpty(param.div1) ? Constants.SelectItrem.GROUP_VAL_ALL : param.div1);
		condition.setDiv2(StringUtils.isEmpty(param.div2) ? Constants.SelectItrem.GROUP_VAL_ALL : param.div2);
		condition.setDiv3(StringUtils.isEmpty(param.div3) ? Constants.SelectItrem.GROUP_VAL_ALL : param.div3);

		condition.setTanto(param.userId);
		condition.setDateFrom(param.date);
		condition.setDateTo(param.date);

		find(condition, model, principal);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());

		// 署名用ログインユーザー設定
		UserInfoDto loginUserInfo = userDetailsHelper.getUserInfo(principal);
		List<EigyoSosikiDto> list = mstEigyoSosikiService.listMySosiki(loginUserInfo.getCorpCodes(),
				loginUserInfo.getId(), now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		EigyoSosikiDto eigyoSosikiDto = list.get(0);
		UserSosikiDto loginUser = new UserSosikiDto();
		loginUser.setFirstName(loginUserInfo.getFirstName());
		loginUser.setLastName(loginUserInfo.getLastName());
		loginUser.setEigyoSosikiNm(eigyoSosikiDto.getEigyoSosikiNm());
		model.addAttribute("loginUser", loginUser);

		model.addAttribute("user", user);

		model.addAttribute("comment", comment == null ? new DailyCommentDto() : comment);

	}

	/**
	 * 日付FromToの最小最大範囲を設定します。
	 * 
	 * @param param パラメータ
	 */
	protected void setMinMaxDate(VisitListSearchCondition param) {
		// DBからでなくpropertyからとる
		Calendar max = DateUtils.getCalendar();
		max.add(Calendar.YEAR, rangeTo);
		max.set(Calendar.MONTH, 11);
		max.set(Calendar.DATE, max.getActualMaximum(Calendar.DATE));

		Calendar min = DateUtils.getCalendar();
		min.add(Calendar.YEAR, -rangeFrom);
		min.set(Calendar.MONTH, 0);
		min.set(Calendar.DATE, min.getActualMinimum(Calendar.DATE));

		param.minDate = min.getTime();
		param.maxDate = max.getTime();
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param tech 技サ用訪問実績かどうか
	 * @param 結果リスト
	 */
	protected List<SearchResult> listResult(VisitListSearchCondition param, Principal principal, boolean tech) {
		// 検索条件をDTOに設定
		CallsSearchConditionDto condition = new CallsSearchConditionDto();
		condition.setId(param.getId());
		condition.setMaxCount(limit + 1);
		
		Calendar fromCalender = DateUtils.getCalendar();
		fromCalender.setTime(param.getDateFrom());
		if (param.getDateTo() != null) {
			Calendar toCalendar = DateUtils.getCalendar();
			toCalendar.setTime(param.getDateTo());
			toCalendar.add(Calendar.DATE, 1);
			condition.setTo(toCalendar.getTime());
		}
		
		String marketId = selectMarketHelper.allToNull(param.getMarket());
		condition.setMarket(marketId);
		condition.setHeldOnly(param.isHeldOnly());
		condition.setHasInfoOnly(param.isHasInfoOnly());
		condition.setUnreadOnly(param.isUnreadOnly());
		condition.setTantoOnly(param.isTantoOnly());
		condition.setAccountId(param.getAccountId());

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		List<String> salesCompanyCodeList = soshikiHelper.createSearchSalesCompanyCodeList(param.getDiv1(), userInfo,
				fromCalender.get(Calendar.YEAR), fromCalender.get(Calendar.MONTH) + 1);
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());
		condition.setDiv1(salesCompanyCodeList);
		condition.setDiv2(div2);
		condition.setDiv3(div3);
		condition.setFrom(param.getDateFrom());
		condition.setAssignedUserId(tanto);
		condition.setUserId(userInfo.getId());

		// 担当者を指定した場合に、個人に紐づく情報を全て検索する場合に設定
		condition.setIgnoreSosikiCondition(param.canIgnoreSosikiCondition());

		condition.setYearMonth(DateUtils.toYearMonth(fromCalender));
		condition.setHistory(DateUtils.isPastYearMonth(fromCalender));

		condition.setCallsInfoDiv(selectCallsInfoDivHelper.allToNull(param.getCallsInfoDiv()));
		condition.setCheckExistsTech(tech);

		condition.setCompany(userInfo.getCompany());
		condition.setParentId(param.parentId);

		List<CallsDto> list = callsService.find(condition);

		if (tech) {
			condition.setTech(tech);
			condition.setCheckExistsTech(false);
			List<CallsDto> techList = callsService.find(condition);
			if (techList.size() > limit) {
				return null;
			}
			mergeTechCalls(list, techList);
		}

		if (list.size() > limit) {
			return null;
		}
		return createResultList(list);
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param list 検索結果リスト
	 * @return 結果リスト
	 */
	protected List<SearchResult> createResultList(List<CallsDto> list) {
		List<SearchResult> result = new ArrayList<>();

		SimpleDateFormat date = Constants.DateFormat.yyyyMMddHHmm.getFormat();
		SimpleDateFormat time = Constants.DateFormat.Time.getFormat();
		List<SelectionItems> items = selectionItemsService.listDivisionC();
		for (CallsDto c : list) {
			SearchResult r = new SearchResult();
			r.id = c.getId();
			r.callId = c.getCallId();
			r.name = c.getName();
			r.accountsName = c.getAccountsName();
			r.marketName = c.getMarketName();
			r.visitDateStart = date.format(c.getDateStart());
			r.visitDateEnd = time.format(c.getTimeEndC());
			r.description = c.getComFreeC();
			r.read = c.isCallsRead();
			// -1以下：コメント無し、0：未読無し、1以上：未読有り
			r.commentReadCount = c.getCommentCount() < 1 ? -1 : c.getCommentReadCount() < 1 ? 1 : 0;
			r.like = c.getCallsLikeCount();
			r.assignedUserFirstName = c.getAssignedUserFirstName();
			r.assignedUserLastName = c.getAssignedUserLastName();
			r.createdUserFirstName = c.getCreatedUserFirstName();
			r.createdUserLastName = c.getCreatedUserLastName();
			/* 訪問理由 */
			String[] divisionCCode = StringUtils.split(StringUtils.defaultString(c.getDivisionC()),
					Constants.Separator.CARET_COMMA);

			r.divisionC = new ArrayList<String>();
			r.commentEntered = c.isCommentEntered();
			r.callsReadCount = c.getCallsReadCount();
			for (String divisionC : divisionCCode) {
				for (SelectionItems item : items) {
					if (item.getValue().equals(divisionC)) {
						r.divisionC.add(item.getSortOrder(), item.getLabel());
					}
				}
			}

			r.plannedCount = c.getPlannedCount();
			r.heldCount = c.getHeldCount();
			r.isPlan = c.isPlan();
			/* 情報区分 */
			r.infoDiv = c.getInfoDiv();
			r.maker = c.getMaker();
			/* 品種 */
			r.kind = c.getKind();
			r.category = c.getCategory();
			/* 感度 */
			r.sensitivity = c.getSensitivity();

			r.parentType = c.getParentType();
			r.status = c.getStatus();

			List<CallsUsersDto> callsUsers = StringUtils.isEmpty(r.callId) ? callsUsersService.findByCallId(c.getId())
					: callsUsersService.findByCallId4Tech(c.getId());
			// 同行
			r.callsUsers = callsUsersService.convertCompanionListToNames(callsUsers);

			r.file1 = c.isFile1();
			r.file2 = c.isFile2();
			r.file3 = c.isFile3();

			r.marketInfo = c.isMarketInfo();

			Calendar cal = Calendar.getInstance();
			cal.setTime(c.getDateStart());
			// 月を跨ぐ検索があるので、訪問の年月で取得する
			MstEigyoSosiki sosiki = mstEigyoSosikiService.findIdByDivCd(c.getAccountsSalesCompanyCode(),
					c.getAccountsDepartmentCode(), c.getAccountsSalesOfficeCode(), cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH) + 1);
			if (sosiki != null) {
				r.setAccountDepartmentName(sosiki.getDiv2Nm());
				r.setAccountSalesOfficeName(sosiki.getDiv3Nm());
			}
			result.add(r);
		}
		return result;
	}
	
	/**
	 * 訪問（技サ用）と訪問をマージします。
	 * 
	 * @param list 訪問リスト
	 * @param techList 訪問（技サ用）リスト
	 */
	private void mergeTechCalls(List<CallsDto> list, List<CallsDto> techList) {
		for (Iterator<CallsDto> it = list.iterator(); it.hasNext();) {
			CallsDto o = it.next();
			for (Iterator<CallsDto> techIt = techList.iterator(); techIt.hasNext();) {
				CallsDto tech = techIt.next();
				if (o.getId().equals(tech.getCallId())) {
					dozerBeanMapper.map(tech, o);
					techIt.remove();
					break;
				}
			}
			if (o.isExsistsTech()) {
				// 訪問に対応する訪問（技サ用）が存在するパターンで、
				// 検索条件上、訪問は検索対象となるが訪問（技サ用）は検索対象とならないパターン（情報区分が変更されているなど）を削除するための処理。
				// 訪問（技サ用）が検索対象になった場合は上記map処理でisExistsTechがfalseとなる。
				it.remove();
			}
		}
		list.addAll(techList);
	}

	public static class SearchResult {

		private String id;
		private String callId;
		private String name;
		private String accountsName;
		private String marketName;
		private String visitDateStart;
		private String visitDateEnd;
		private String description;
		private boolean read;
		private int commentReadCount;
		private int like;
		private String assignedUserFirstName;
		private String assignedUserLastName;
		/** 訪問理由 */
		private List<String> divisionC;
		private Integer plannedCount;
		private Integer heldCount;
		private Boolean isPlan;
		/** 情報区分 */
		private String infoDiv;
		private String maker;
		/** 品種 */
		private String kind;
		private String category;
		/** 感度 */
		private String sensitivity;
		private String parentType;
		private String status;
		private List<String> callsUsers;

		private Integer callsReadCount;
		private Boolean commentEntered;

		private boolean file1;
		private boolean file2;
		private boolean file3;

		private boolean marketInfo;

		private String accountDepartmentName;
		private String accountSalesOfficeName;

		private String createdUserFirstName;
		private String createdUserLastName;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCallId() {
			return callId;
		}

		public void setCallId(String callId) {
			this.callId = callId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAccountsName() {
			return accountsName;
		}

		public void setAccountsName(String accountsName) {
			this.accountsName = accountsName;
		}

		public String getVisitDateEnd() {
			return visitDateEnd;
		}

		public void setVisitDateEnd(String visitDateEnd) {
			this.visitDateEnd = visitDateEnd;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public boolean isRead() {
			return read;
		}

		public void setRead(boolean read) {
			this.read = read;
		}

		public int getCommentReadCount() {
			return commentReadCount;
		}

		public void setCommentReadCount(int commentReadCount) {
			this.commentReadCount = commentReadCount;
		}

		public int getLike() {
			return like;
		}

		public void setLike(int like) {
			this.like = like;
		}

		public String getVisitDateStart() {
			return visitDateStart;
		}

		public void setVisitDateStart(String visitDateStart) {
			this.visitDateStart = visitDateStart;
		}

		public String getAssignedUserFirstName() {
			return assignedUserFirstName;
		}

		public void setAssignedUserFirstName(String assignedUserFirstName) {
			this.assignedUserFirstName = assignedUserFirstName;
		}

		public String getAssignedUserLastName() {
			return assignedUserLastName;
		}

		public void setAssignedUserLastName(String assignedUserLastName) {
			this.assignedUserLastName = assignedUserLastName;
		}

		public List<String> getDivisionC() {
			return divisionC;
		}

		public void setDivisionC(List<String> divisionC) {
			this.divisionC = divisionC;
		}

		public Integer getPlannedCount() {
			return plannedCount;
		}

		public void setPlannedCount(Integer plannedCount) {
			this.plannedCount = plannedCount;
		}

		public Integer getHeldCount() {
			return heldCount;
		}

		public void setHeldCount(Integer heldCount) {
			this.heldCount = heldCount;
		}

		public Boolean getIsPlan() {
			return isPlan;
		}

		public void setIsPlan(Boolean isPlan) {
			this.isPlan = isPlan;
		}

		public String getInfoDiv() {
			return infoDiv;
		}

		public void setInfoDiv(String infoDiv) {
			this.infoDiv = infoDiv;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getSensitivity() {
			return sensitivity;
		}

		public void setSensitivity(String sensitivity) {
			this.sensitivity = sensitivity;
		}

		public String getParentType() {
			return parentType;
		}

		public void setParentType(String parentType) {
			this.parentType = parentType;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getMaker() {
			return maker;
		}

		public void setMaker(String maker) {
			this.maker = maker;
		}

		public List<String> getCallsUsers() {
			return callsUsers;
		}

		public void setCallsUsers(List<String> callsUsers) {
			this.callsUsers = callsUsers;
		}

		public Integer getCallsReadCount() {
			return callsReadCount;
		}

		public void setCallsReadCount(Integer callsReadCount) {
			this.callsReadCount = callsReadCount;
		}

		public Boolean getCommentEntered() {
			return commentEntered;
		}

		public void setCommentEntered(Boolean commentEntered) {
			this.commentEntered = commentEntered;
		}

		public boolean isFile1() {
			return file1;
		}

		public void setFile1(boolean file1) {
			this.file1 = file1;
		}

		public boolean isFile2() {
			return file2;
		}

		public void setFile2(boolean file2) {
			this.file2 = file2;
		}

		public boolean isFile3() {
			return file3;
		}

		public void setFile3(boolean file3) {
			this.file3 = file3;
		}

		public boolean isMarketInfo() {
			return marketInfo;
		}

		public void setMarketInfo(boolean marketInfo) {
			this.marketInfo = marketInfo;
		}

		public String getAccountDepartmentName() {
			return accountDepartmentName;
		}

		public void setAccountDepartmentName(String accountDepartmentName) {
			this.accountDepartmentName = accountDepartmentName;
		}

		public String getAccountSalesOfficeName() {
			return accountSalesOfficeName;
		}

		public void setAccountSalesOfficeName(String accountSalesOfficeName) {
			this.accountSalesOfficeName = accountSalesOfficeName;
		}

		public String getMarketName() {
			return marketName;
		}

		public void setMarketName(String marketName) {
			this.marketName = marketName;
		}

		public String getCreatedUserFirstName() {
			return createdUserFirstName;
		}

		public void setCreatedUserFirstName(String createdUserFirstName) {
			this.createdUserFirstName = createdUserFirstName;
		}

		public String getCreatedUserLastName() {
			return createdUserLastName;
		}

		public void setCreatedUserLastName(String createdUserLastName) {
			this.createdUserLastName = createdUserLastName;
		}

	}

	public static class DailyReportParam {
		String userId;
		Date date;
		String div1;
		String div2;
		String div3;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public String getDiv1() {
			return div1;
		}

		public void setDiv1(String div1) {
			this.div1 = div1;
		}

		public String getDiv2() {
			return div2;
		}

		public void setDiv2(String div2) {
			this.div2 = div2;
		}

		public String getDiv3() {
			return div3;
		}

		public void setDiv3(String div3) {
			this.div3 = div3;
		}

	}

	public static class VisitListSearchCondition extends SearchConditionBaseDto {
		private String id;
		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date dateTo;
		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date dateFrom;

		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date maxDate;
		@DateTimeFormat(pattern = "yyyy/MM/dd")
		private Date minDate;

		private String market;
		
		private boolean heldOnly;
		private boolean hasInfoOnly;
		private boolean unreadOnly;
		private boolean tantoOnly;

		private String callsInfoDiv;
		private boolean ignoreSosikiCondition;
		
		private String mode;
		private String searchMode;
		
		private String accountId;
		
		private String parentId;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Date getDateTo() {
			return dateTo;
		}

		public void setDateTo(Date dateTo) {
			this.dateTo = dateTo;
		}

		public Date getDateFrom() {
			return dateFrom;
		}

		public void setDateFrom(Date dateFrom) {
			this.dateFrom = dateFrom;
		}

		public Date getMaxDate() {
			return maxDate;
		}

		public void setMaxDate(Date maxDate) {
			this.maxDate = maxDate;
		}

		public Date getMinDate() {
			return minDate;
		}

		public void setMinDate(Date minDate) {
			this.minDate = minDate;
		}

		public String getMarket() {
			return market;
		}

		public void setMarket(String market) {
			this.market = market;
		}

		public boolean isHeldOnly() {
			return heldOnly;
		}

		public void setHeldOnly(boolean heldOnly) {
			this.heldOnly = heldOnly;
		}

		public boolean isHasInfoOnly() {
			return hasInfoOnly;
		}

		public void setHasInfoOnly(boolean hasInfoOnly) {
			this.hasInfoOnly = hasInfoOnly;
		}

		public boolean isUnreadOnly() {
			return unreadOnly;
		}

		public void setUnreadOnly(boolean unreadOnly) {
			this.unreadOnly = unreadOnly;
		}

		public String getCallsInfoDiv() {
			return callsInfoDiv;
		}

		public void setCallsInfoDiv(String callsInfoDiv) {
			this.callsInfoDiv = callsInfoDiv;
		}

		public boolean canIgnoreSosikiCondition() {
			return ignoreSosikiCondition;
		}

		public void setIgnoreSosikiCondition(boolean ignoreSosikiCondition) {
			this.ignoreSosikiCondition = ignoreSosikiCondition;
		}

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}

		public boolean isTantoOnly() {
			return tantoOnly;
		}

		public void setTantoOnly(boolean tantoOnly) {
			this.tantoOnly = tantoOnly;
		}

		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		public String getSearchMode() {
			return searchMode;
		}

		public void setSearchMode(String searchMode) {
			this.searchMode = searchMode;
		}

		public String getParentId() {
			return parentId;
		}

		public void setParentId(String parentId) {
			this.parentId = parentId;
		}

	}

	public static class ReadList extends VisitListSearchCondition {

		private List<String> readCallId;

		public List<String> getReadCallId() {
			return readCallId;
		}

		public void setReadCallId(List<String> readCallId) {
			this.readCallId = readCallId;
		}

	}

	public static class FileName extends VisitListSearchCondition {

		private String downloadId1;
		private String downloadId2;
		private String downloadId3;
		private String techCallId;

		public String getDownloadId1() {
			return downloadId1;
		}

		public void setDownloadId1(String downloadId1) {
			this.downloadId1 = downloadId1;
		}

		public String getDownloadId2() {
			return downloadId2;
		}

		public void setDownloadId2(String downloadId2) {
			this.downloadId2 = downloadId2;
		}

		public String getDownloadId3() {
			return downloadId3;
		}

		public void setDownloadId3(String downloadId3) {
			this.downloadId3 = downloadId3;
		}

		public String getTechCallId() {
			return techCallId;
		}

		public void setTechCallId(String techCallId) {
			this.techCallId = techCallId;
		}

	}

}
