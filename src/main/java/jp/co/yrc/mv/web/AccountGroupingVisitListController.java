package jp.co.yrc.mv.web;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CallsAccountsDto;
import jp.co.yrc.mv.dto.CallsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.entity.Users;
import jp.co.yrc.mv.helper.SelectGroupingHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsAccountsService;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.SelectionItemsService;
import jp.co.yrc.mv.service.UsersService;

/**
 * 得意先グルーピング別訪問実績用コントローラクラス。
 *
 */
@Controller
@Transactional
public class AccountGroupingVisitListController {

	static int MAX_COUNT = 3000;
	
	@Autowired
	CallsAccountsService callsAccountsService;

	@Autowired
	CallsService callsService;

	@Autowired
	UsersService userService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SelectGroupingHelper selectGroupingHelper;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/accountGroupingVisitList.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		List<DayInfo> dayInfoList = createDayColumnTemplate(c);
		selectYearsHelper.create(model);
		selectGroupingHelper.createGroup(model, false, false);
		model.addAttribute(param);
		model.addAttribute("accountVisitList", Collections.EMPTY_LIST);
		model.addAttribute("dayInfoList", dayInfoList);

		return "accountGroupingVisitList";
	}
	
	/**
	 * 検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/accountGroupingVisitList.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {

		// 検索条件値の変換
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());
		String idForCustomer = "";
		if (tanto != null) {
			Users user = userService.selectUserById(tanto);
			idForCustomer = user.getIdForCustomer();
		}
		
		List<String> salesCompanyCodeList = soshikiHelper.createSearchSalesCompanyCodeList(param.getDiv1(), 
				userDetailsHelper.getUserInfo(principal), param.getYear(), param.getMonth());

		List<CallsAccountsDto> callAccountList = callsAccountsService.findTantoCallsAccountList(param.getYear(), param.getMonth(), salesCompanyCodeList, 
				div2, div3, tanto, idForCustomer, param.getGroup());
		List<CallsDto> callList = callsService.findMonthlyCalls(param.getYear(), param.getMonth(), salesCompanyCodeList, div2, div3, 
				tanto, idForCustomer, param.getGroup());
		
		List<DayInfo> dayInfoList = createDayColumnTemplate(DateUtils.getYearMonthCalendar(param.getYear(), param.getMonth()));
				
		selectYearsHelper.create(model);
		selectGroupingHelper.createGroup(model, false, false);
		model.addAttribute(param);

		List<AccountVisit> accountVisitList = createAccountVisitList(callAccountList, callList);
		if (accountVisitList.size() > MAX_COUNT) {
			model.addAttribute("limitOver", true);
			model.addAttribute("accountVisitList", Collections.EMPTY_LIST);
		} else {
			model.addAttribute("accountVisitList", accountVisitList);			
		}
		model.addAttribute("dayInfoList", dayInfoList);

		return "accountGroupingVisitList";
	}
	
	/**
	 * 得意先ごとに1ヵ月の訪問を紐づけたリストを作成します。
	 * 
	 * @param calendar 指定年月を表すカレンダー
	 * @return 日情報リスト
	 */
 	protected List<AccountVisit> createAccountVisitList(List<CallsAccountsDto> callAccountList, List<CallsDto> callList) {
 		List<AccountVisit> accountVisitList = new ArrayList<>();
 		for (CallsAccountsDto dto : callAccountList) {
 			int rowHeldCount = 0;
 			AccountVisit accountVisit = new AccountVisit();
 	 		dozerBeanMapper.map(dto, accountVisit);
 			for (CallsDto call : callList) {
 				if (accountVisit.getAccountId().equals(call.getParentId())) {
	 				VisitInfo visitInfo = new VisitInfo();
	 	 	 		dozerBeanMapper.map(call, visitInfo);
	 	 	 		visitInfo.divisionC = visitInfo.divisionC == null ? "" : visitInfo.divisionC.replace(",", "<br/>");
	 	 	 		visitInfo.companionNames = visitInfo.companionNames == null ? "" : visitInfo.companionNames.replace(",", "<br/>");
	 	 	 		accountVisit.getVisitInfoList().add(visitInfo);
	 	 	 		rowHeldCount += call.getHeldCount();
 				}
 			}
 			accountVisit.setRowHeldCount(rowHeldCount);
 	 		accountVisitList.add(accountVisit);
 		}
 		return accountVisitList;
 	}
 	
	/**
	 * 指定した年月の持つ日数分の列テンプレートを作成します。
	 * 
	 * @param calendar 指定年月を表すカレンダー
	 * @return 日情報リスト
	 */
 	protected List<DayInfo> createDayColumnTemplate(Calendar calendar) {
		SimpleDateFormat formatter = DateUtils.getDayOfWeekFormat();

		List<DayInfo> dayInfoList = new ArrayList<>();
		for (int i = 1, max = calendar.getActualMaximum(Calendar.DATE); i <= max; i++, calendar.add(Calendar.DATE, 1)) {
			DayInfo dayInfo = new DayInfo();
			dayInfo.date = i;
			dayInfo.dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			dayInfo.dayOfWeekLabel = formatter.format(calendar.getTime());
			dayInfoList.add(dayInfo);
		}
		return dayInfoList;
	}

	public static class SearchCondition extends SearchConditionBaseDto {
		String group;
		public String getGroup() {
			return group;
		}
		public void setGroup(String group) {
			this.group = group;
		}
	}
	
	public static class DayInfo {
		int date;
		int dayOfWeek;
		String dayOfWeekLabel;

		public int getDate() {
			return date;
		}
		public void setDate(int date) {
			this.date = date;
		}
		public int getDayOfWeek() {
			return dayOfWeek;
		}
		public void setDayOfWeek(int dayOfWeek) {
			this.dayOfWeek = dayOfWeek;
		}
		public String getDayOfWeekLabel() {
			return dayOfWeekLabel;
		}
		public void setDayOfWeekLabel(String dayOfWeekLabel) {
			this.dayOfWeekLabel = dayOfWeekLabel;
		}
	}
	
	public static class VisitInfo {
		String divisionC;
		String companionNames;
		int date;
		
		public String getDivisionC() {
			return divisionC;
		}
		public void setDivisionC(String divisionC) {
			this.divisionC = divisionC;
		}
		public String getCompanionNames() {
			return companionNames;
		}
		public void setCompanionNames(String companionNames) {
			this.companionNames = companionNames;
		}
		public int getDate() {
			return date;
		}		
		public void setDate(int date) {
			this.date = date;
		}
	}

	public static class AccountVisit {
		String accountId;
		String accountName;
		String lastName;
		String firstName;
		String div2Nm;
		String div3Nm;
		int rowHeldCount;
		List<VisitInfo> visitInfoList = new ArrayList<>();

		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getAccountName() {
			return accountName;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getDiv2Nm() {
			return div2Nm;
		}
		public void setDiv2Nm(String div2Nm) {
			this.div2Nm = div2Nm;
		}
		public String getDiv3Nm() {
			return div3Nm;
		}
		public void setDiv3Nm(String div3Nm) {
			this.div3Nm = div3Nm;
		}
		public List<VisitInfo> getVisitInfoList() {
			return visitInfoList;
		}
		public void setVisitInfoList(List<VisitInfo> visitInfoList) {
			this.visitInfoList = visitInfoList;
		}
		public int getRowHeldCount() {
			return rowHeldCount;
		}
		public void setRowHeldCount(int rowHeldCount) {
			this.rowHeldCount = rowHeldCount;
		}
	}
}
