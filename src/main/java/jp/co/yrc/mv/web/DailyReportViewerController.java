package jp.co.yrc.mv.web;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.DailyCommentDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.DailyCommentsService;

/**
 * 日報確認（管理者・セールス）用コントローラクラス。
 *
 */
@Controller
@Transactional
public class DailyReportViewerController {

	@Autowired
	DailyCommentsService dailyCommentsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Value("${limit}")
	int limit;

	private static final String MODE_SALES = "sales";

	private static final String MODE_ADMIN = "admin";

	/**
	 * 日報確認（管理者）初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportViewerAdmin.html", method = RequestMethod.GET)
	public String initAdmin(SearchCondition param, Model model, Principal principal) {
		init(param, model, principal);
		model.addAttribute("mode", MODE_ADMIN);
		return "dailyReportViewer";
	}

	/**
	 * 日報確認（セールス）初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportViewerSales.html", method = RequestMethod.GET)
	public String initSales(SearchCondition param, Model model, Principal principal) {
		init(param, model, principal);
		model.addAttribute("mode", MODE_SALES);
		return "dailyReportViewer";
	}

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 */
	protected void init(SearchCondition param, Model model, Principal principal) {
		Calendar c = DateUtils.getCalendar();
		param.setYear(c.get(Calendar.YEAR));
		// うるう年を正しく表示するために月ドロップダウンのvalueには年月を入れる
		param.setMonth(DateUtils.toYearMonth(c));
		param.setDate(c.get(Calendar.DATE));

		selectYearsHelper.createYearMonthDate(model);
		model.addAttribute(param);
	}

	/**
	 * 日報確認（管理者）検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportViewerAdmin.html", method = RequestMethod.POST)
	public String findAdmin(SearchCondition param, Model model, Principal principal) {
		find(param, model, principal);
		model.addAttribute("mode", MODE_ADMIN);
		return "dailyReportViewer";
	}
	
	/**
	 * 日報確認（セールス）検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/dailyReportViewerSales.html", method = RequestMethod.POST)
	public String findSales(SearchCondition param, Model model, Principal principal) {
		find(param, model, principal);
		model.addAttribute("mode", MODE_SALES);
		return "dailyReportViewer";
	}

	/**
	 * 検索押下時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	protected void find(SearchCondition param, Model model, Principal principal) {
		selectYearsHelper.createYearMonthDate(model);
		List<SearchResult> result = listResult(param, principal);
		if (result == null) {
			model.addAttribute("limitOver", true);
		} else {
			model.addAttribute("result", result);
		}
		model.addAttribute(param);
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param パラメータ
	 * @param principal プリンシパル
	 * @param 結果リスト
	 */
	protected List<SearchResult> listResult(SearchCondition param, Principal principal) {

		String paramDiv1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);

		List<String> salesCompanyCodeList = soshikiHelper.createSearchSalesCompanyCodeList(param.getDiv1(), userInfo,
				param.getYear(), DateUtils.toMonth(param.getMonth()));

		if (dailyCommentsService.countCommentWithCallCount(param.getYear(), DateUtils.toMonth(param.getMonth()),
				param.getDate(), salesCompanyCodeList, paramDiv1, div2, div3, tanto) > limit) {
			return null;
		}
		List<DailyCommentDto> list = dailyCommentsService.listCommentWithCallCount(param.getYear(),
				DateUtils.toMonth(param.getMonth()), param.getDate(), salesCompanyCodeList, paramDiv1, div2, div3,
				tanto, userInfo.getId());

		SimpleDateFormat date = Constants.DateFormat.yyyyMMdd.getFormat();
		List<SearchResult> result = new ArrayList<>();
		for (DailyCommentDto o : list) {
			SearchResult r = new SearchResult();
			result.add(r);
			dozerBeanMapper.map(o, r);

			// id == null はCommentが存在しない
			r.setCommentExists(r.getId() != null);
			if (r.isCommentExists()) {
				r.setConfirm(r.isReadLoginUser());
				if (r.getConfirmDate() != null) {
					r.setConfirmDateString(date.format(r.getConfirmDate()));
				}
			}

			r.setVisitDateString(date.format(r.getVisitDate()));
			r.setTargetDate(r.getVisitDateString());

			r.setBossFirstName(StringUtils.defaultString(r.getBossFirstName()));
			r.setBossLastName(StringUtils.defaultString(r.getBossLastName()));

		}
		return result;
	}

	public static class SearchResult extends DailyCommentDto {

		private boolean confirm;
		private String visitDateString;
		private String confirmDateString;
		private boolean commentExists;
		private String targetDate;

		public boolean isCommentExists() {
			return commentExists;
		}

		public void setCommentExists(boolean commentExists) {
			this.commentExists = commentExists;
		}

		public boolean isConfirm() {
			return confirm;
		}

		public void setConfirm(boolean confirm) {
			this.confirm = confirm;
		}

		public String getVisitDateString() {
			return visitDateString;
		}

		public void setVisitDateString(String visitDateString) {
			this.visitDateString = visitDateString;
		}

		public String getConfirmDateString() {
			return confirmDateString;
		}

		public void setConfirmDateString(String confirmDateString) {
			this.confirmDateString = confirmDateString;
		}

		public String getTargetDate() {
			return targetDate;
		}

		public void setTargetDate(String targetDate) {
			this.targetDate = targetDate;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {

	}

}
