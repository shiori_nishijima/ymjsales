package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;
import static jp.co.yrc.mv.common.MathUtils.toK;
import static jp.co.yrc.mv.common.MathUtils.toPercentage;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CompassControlResultDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectGroupingHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.PlansService;
import jp.co.yrc.mv.service.ResultsService;

@Controller
@Transactional
public class SalesAndKindActualReferenceController {

	@Autowired
	ResultsService resultsService;

	@Autowired
	PlansService plansService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SelectGroupingHelper selectGroupingHelper;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/salesAndKindActualReference.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		param.setSearchMode("real");

		selectYearsHelper.create(model);
		model.addAttribute(param);
		model.addAttribute("list", Collections.EMPTY_LIST);
		return "salesAndKindActualReference";
	}

	/**
	 * 検索時の処理を実行します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/salesAndKindActualReference.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {
		selectYearsHelper.create(model);
		selectGroupingHelper.createGroup(model);
		model.addAttribute(param);

		List<Result> list = listResult(param, principal);
		model.addAttribute("list", list);
		return "salesAndKindActualReference";
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param principal
	 *            プリンシパル
	 * @return 結果リスト
	 */
	protected List<Result> listResult(SearchCondition param, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		List<CompassControlResultDto> list = resultsService.sumMonthlyCompassControl(param.getYear(), param.getMonth(),
				tanto, Collections.singletonList(div1), div2, div3);

		Map<Result, Result> map = new TreeMap<>();
		Map<Integer, Result> totalMap = new LinkedHashMap<>();
		for (CompassControlResultDto o : list) {
			setTotal(map, totalMap, o);
			setResult(map, o);
		}

		boolean currentMonth = DateUtils.isCurrentYearMonth(param.getYear(), param.getMonth());
		if (currentMonth) {
			// 本日分を検索
			Calendar nowCal = DateUtils.getCalendar();
			List<CompassControlResultDto> todayList = resultsService.sumDailyCompassControl(param.getYear(),
					param.getMonth(), nowCal.get(Calendar.DATE), tanto, Collections.singletonList(div1), div2, div3);

			for (CompassControlResultDto o : todayList) {
				setToday(param, map, totalMap, o);
			}
		}

		// 伸長率のために去年の値を求める
		List<CompassControlResultDto> lastYearList = resultsService.sumMonthlyCompassControl(param.getYear() - 1,
				param.getMonth(), tanto, Collections.singletonList(div1), div2, div3);
		for (CompassControlResultDto o : lastYearList) {
			setElongation(map, totalMap, o);
		}

		List<CompassControlResultDto> planList = plansService.sumCompassControl(param.getYear(), param.getMonth(),
				tanto, Collections.singletonList(div1), div2, div3);

		for (CompassControlResultDto o : planList) {
			setPlanAndAchievementRate(map, o);
			setPlanTotal(map, totalMap, o);
		}

		for (Iterator<Entry<Integer, Result>> it = totalMap.entrySet().iterator(); it.hasNext();) {
			Entry<Integer, Result> e = it.next();

			Result r = e.getValue();
			calcTotalRate(r);
		}

		List<Result> result = new ArrayList<>(map.values());

		// 金額を1000円単位に
		for (Result r : result) {
			setK(r);
		}

		return result;
	}

	protected void setK(Result r) {
		r.sales.resultTotal = toK(r.sales.resultTotal);
		r.sales.resultToday = toK(r.sales.resultToday);
		r.sales.lastYearResultTotal = toK(r.sales.lastYearResultTotal);
		r.sales.plan = toK(r.sales.plan);

		r.margin.resultTotal = toK(r.margin.resultTotal);
		r.margin.resultToday = toK(r.margin.resultToday);
		r.margin.lastYearResultTotal = toK(r.margin.lastYearResultTotal);
		r.margin.plan = toK(r.margin.plan);
	}

	protected void calcTotalRate(Result r) {
		r.number.achievementRate = toPercentage(r.number.resultTotal, r.number.plan);
		r.sales.achievementRate = toPercentage(r.sales.resultTotal, r.sales.plan);
		r.margin.achievementRate = toPercentage(r.margin.resultTotal, r.margin.plan);

		r.number.elongation = toPercentage(r.number.resultTotal, r.number.lastYearResultTotal);
		r.sales.elongation = toPercentage(r.sales.resultTotal, r.sales.lastYearResultTotal);
		r.margin.elongation = toPercentage(r.margin.resultTotal, r.margin.lastYearResultTotal);
	}

	protected void setElongation(Map<Result, Result> map, Map<Integer, Result> totalMap, CompassControlResultDto o) {
		Result r = map.get(new Result(o.getReportDisplaySeq(), o.getPlanProductKind()));
		Result total = totalMap.get(o.getReportDisplaySeq());
		if (r == null) {
			return;
		}

		r.number.elongation = toPercentage(r.number.resultTotal, o.getNumber());
		r.sales.elongation = toPercentage(r.sales.resultTotal, o.getSales());
		r.margin.elongation = toPercentage(r.margin.resultTotal, o.getMargin());
		r.number.lastYearResultTotal = o.getNumber();
		r.sales.lastYearResultTotal = o.getSales();
		r.margin.lastYearResultTotal = o.getMargin();

		total.number.lastYearResultTotal = toDefaultZero(total.number.lastYearResultTotal)
				.add(toDefaultZero(o.getNumber()));
		total.sales.lastYearResultTotal = toDefaultZero(total.sales.lastYearResultTotal).add(o.getSales());
		total.margin.lastYearResultTotal = toDefaultZero(total.margin.lastYearResultTotal).add(o.getMargin());

	}

	protected Result setTotal(Map<Result, Result> map, Map<Integer, Result> totalMap, CompassControlResultDto o) {
		Result total = createTotal(map, totalMap, o);
		total.margin.resultTotal = total.margin.resultTotal.add(toDefaultZero(o.getMargin()));
		total.sales.resultTotal = total.sales.resultTotal.add(toDefaultZero(o.getSales()));
		total.number.resultTotal = total.number.resultTotal.add(toDefaultZero(o.getNumber()));
		return total;
	}

	protected Result createTotal(Map<Result, Result> map, Map<Integer, Result> totalMap, CompassControlResultDto o) {
		Result total = totalMap.get(o.getReportDisplaySeq());
		if (total == null) {
			total = new Result(o.getReportDisplaySeq());
			total.reportDisplayName = o.getReportDisplayName();
			total.total = true;
			totalMap.put(o.getReportDisplaySeq(), total);
			map.put(total, total);
		}
		return total;
	}

	protected void setToday(SearchCondition param, Map<Result, Result> map, Map<Integer, Result> totalMap,
			CompassControlResultDto o) {
		Result r = map.get(new Result(o.getReportDisplaySeq(), o.getPlanProductKind()));
		Result total = totalMap.get(o.getReportDisplaySeq());
		// 通常あり得ない
		if (r == null || total == null) {
			return;
		}

		boolean isToday = !"yesterday".equals(param.getSearchMode());
		if (isToday) {
			// 検索した値を当日の項目に設定
			r.number.resultToday = o.getNumber();
			r.sales.resultToday = o.getSales();
			r.margin.resultToday = o.getMargin();
			// 合計を足し込む
			total.number.resultToday = toDefaultZero(total.number.resultToday).add(toDefaultZero(o.getNumber()));
			total.sales.resultToday = toDefaultZero(total.sales.resultToday).add(toDefaultZero(o.getSales()));
			total.margin.resultToday = toDefaultZero(total.margin.resultToday).add(toDefaultZero(o.getMargin()));
		} else {
			// 検索結果が月の集計のため、前日なら「月の集計-当日の集計」で前日までを求める。
			r.number.resultTotal = toDefaultZero(r.number.resultTotal).subtract(toDefaultZero(o.getNumber()));
			r.sales.resultTotal = toDefaultZero(r.sales.resultTotal).subtract(toDefaultZero(o.getSales()));
			r.margin.resultTotal = toDefaultZero(r.margin.resultTotal).subtract(toDefaultZero(o.getMargin()));

			// totalからも引く
			total.number.resultTotal = toDefaultZero(total.number.resultTotal).subtract(toDefaultZero(o.getNumber()));
			total.sales.resultTotal = toDefaultZero(total.sales.resultTotal).subtract(toDefaultZero(o.getSales()));
			total.margin.resultTotal = toDefaultZero(total.margin.resultTotal).subtract(toDefaultZero(o.getMargin()));

		}
	}

	/**
	 * 計画と達成率を設定する
	 * 
	 * @param map
	 * @param o
	 */
	protected void setPlanAndAchievementRate(Map<Result, Result> map, CompassControlResultDto o) {
		Result r = map.get(new Result(o.getReportDisplaySeq(), o.getPlanProductKind()));
		if (r == null) {
			r = createResult(o);
			map.put(r, r);
		}

		r.number.plan = o.getNumber();
		r.sales.plan = o.getSales();
		r.margin.plan = o.getMargin();

		r.number.achievementRate = toPercentage(r.number.resultTotal, r.number.plan);
		r.sales.achievementRate = toPercentage(r.sales.resultTotal, r.sales.plan);
		r.margin.achievementRate = toPercentage(r.margin.resultTotal, r.margin.plan);

	}

	protected void setPlanTotal(Map<Result, Result> map, Map<Integer, Result> totalMap, CompassControlResultDto o) {
		Result r = totalMap.get(o.getReportDisplaySeq());

		if (r == null) {
			r = createTotal(map, totalMap, o);
		}

		r.number.plan = toDefaultZero(r.number.plan).add(toDefaultZero(o.getNumber()));
		r.sales.plan = toDefaultZero(r.sales.plan).add(toDefaultZero(o.getSales()));
		r.margin.plan = toDefaultZero(r.margin.plan).add(toDefaultZero(o.getMargin()));

	}

	/**
	 * 結果を生成します。
	 * 
	 * @param o
	 * @param commentsDto
	 * @return
	 */
	protected Result setResult(Map<Result, Result> map, CompassControlResultDto o) {
		Result r = createResult(o);

		r.margin.resultTotal = o.getMargin();
		r.sales.resultTotal = o.getSales();
		r.number.resultTotal = o.getNumber();

		map.put(r, r);
		return r;
	}

	protected Result createResult(CompassControlResultDto o) {
		Result r = new Result();

		r.compassKind = o.getCompassKind();
		r.compassKindName = o.getCompassKindName();
		r.planProductKind = o.getPlanProductKind();
		r.planProductKindName = o.getPlanProductKindName();
		r.reportDisplayName = o.getReportDisplayName();
		r.reportDisplaySeq = o.getReportDisplaySeq();
		r.total = false;
		return r;
	}

	public static class Result implements Comparable<Result> {

		public Result() {
		}

		public Result(Integer reportDisplaySeq) {
			this(reportDisplaySeq, null);
		}

		public Result(Integer reportDisplaySeq, String planProductKind) {
			this.reportDisplaySeq = reportDisplaySeq;
			this.planProductKind = planProductKind;
		}

		/** 帳票表示順 */
		Integer reportDisplaySeq;
		/** 帳票表示品種名称) */
		String reportDisplayName;
		/** 販管計画品種コード */
		String planProductKind;
		/** 販管計画品種名称 */
		String planProductKindName;
		/** 共通品種コード(Compass品種コード) */
		String compassKind;
		/** 共通品種名称(Compass品種名) */
		String compassKindName;

		boolean total;

		ResultDetail margin = new ResultDetail();
		ResultDetail sales = new ResultDetail();
		ResultDetail number = new ResultDetail();

		public ResultDetail getMargin() {
			return margin;
		}

		public void setMargin(ResultDetail margin) {
			this.margin = margin;
		}

		public ResultDetail getSales() {
			return sales;
		}

		public void setSales(ResultDetail sales) {
			this.sales = sales;
		}

		public ResultDetail getNumber() {
			return number;
		}

		public void setNumber(ResultDetail number) {
			this.number = number;
		}

		public Integer getReportDisplaySeq() {
			return reportDisplaySeq;
		}

		public void setReportDisplaySeq(Integer reportDisplaySeq) {
			this.reportDisplaySeq = reportDisplaySeq;
		}

		public String getReportDisplayName() {
			return reportDisplayName;
		}

		public void setReportDisplayName(String reportDisplayName) {
			this.reportDisplayName = reportDisplayName;
		}

		public String getPlanProductKind() {
			return planProductKind;
		}

		public void setPlanProductKind(String planProductKind) {
			this.planProductKind = planProductKind;
		}

		public String getPlanProductKindName() {
			return planProductKindName;
		}

		public void setPlanProductKindName(String planProductKindName) {
			this.planProductKindName = planProductKindName;
		}

		public String getCompassKind() {
			return compassKind;
		}

		public void setCompassKind(String compassKind) {
			this.compassKind = compassKind;
		}

		public String getCompassKindName() {
			return compassKindName;
		}

		public void setCompassKindName(String compassKindName) {
			this.compassKindName = compassKindName;
		}

		public boolean isTotal() {
			return total;
		}

		public void setTotal(boolean total) {
			this.total = total;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((planProductKind == null) ? 0 : planProductKind.hashCode());
			result = prime * result + ((reportDisplaySeq == null) ? 0 : reportDisplaySeq.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Result other = (Result) obj;
			if (planProductKind == null) {
				if (other.planProductKind != null)
					return false;
			} else if (!planProductKind.equals(other.planProductKind))
				return false;
			if (reportDisplaySeq == null) {
				if (other.reportDisplaySeq != null)
					return false;
			} else if (!reportDisplaySeq.equals(other.reportDisplaySeq))
				return false;
			return true;
		}

		@Override
		public int compareTo(Result o) {
			int result = reportDisplaySeq.compareTo(o.reportDisplaySeq);
			if (result == 0) {
				if (planProductKind != null && o.planProductKind != null) {
					result = planProductKind.compareTo(o.planProductKind);
				} else {
					result = Objects.equals(planProductKind, o.planProductKind) ? 0 : planProductKind == null ? -1 : 1;
				}
			}
			return result;
		}

	}

	public static class ResultDetail {
		BigDecimal plan = BigDecimal.ZERO;
		BigDecimal resultToday = BigDecimal.ZERO;
		BigDecimal resultTotal = BigDecimal.ZERO;
		BigDecimal lastYearResultTotal = BigDecimal.ZERO;
		BigDecimal achievementRate = BigDecimal.ZERO;
		BigDecimal elongation = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResultToday() {
			return resultToday;
		}

		public void setResultToday(BigDecimal resultToday) {
			this.resultToday = resultToday;
		}

		public BigDecimal getResultTotal() {
			return resultTotal;
		}

		public void setResultTotal(BigDecimal resultTotal) {
			this.resultTotal = resultTotal;
		}

		public BigDecimal getAchievementRate() {
			return achievementRate;
		}

		public void setAchievementRate(BigDecimal achievementRate) {
			this.achievementRate = achievementRate;
		}

		public BigDecimal getElongation() {
			return elongation;
		}

		public void setElongation(BigDecimal elongation) {
			this.elongation = elongation;
		}

		public BigDecimal getLastYearResultTotal() {
			return lastYearResultTotal;
		}

		public void setLastYearResultTotal(BigDecimal lastYearResultTotal) {
			this.lastYearResultTotal = lastYearResultTotal;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {

		private String searchMode;

		public String getSearchMode() {
			return searchMode;
		}

		public void setSearchMode(String searchMode) {
			this.searchMode = searchMode;
		}
	}

}
