package jp.co.yrc.mv.web;

import java.security.Principal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.OpportunitiesDto;
import jp.co.yrc.mv.dto.OpportunitiesSearchConditionDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.service.OpportunitiesService;

/**
 * 案件状況用コントローラクラス
 * 
 * @author komatsu
 *
 */

@Controller
@Transactional
public class OpportunityListController {

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	OpportunitiesService opportunitiesService;

	@Value("${limit}")
	int limit;
	/**
	 * 初期表示の処理を実行します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/opportunityList.html", method = RequestMethod.GET)
	public String initOpportunitiesList(SearchCondition param, Model model, Principal principal) {
		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);
		model.addAttribute(param);

		model.addAttribute(new DateConditionResult());

		selectYearsHelper.create(model);
		return "opportunityList";
	}

	/**
	 * 検索時の処理を実行します。
	 * 
	 * @param pram
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/opportunityList.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {
		selectYearsHelper.create(model);
		// ALL選択時に中身をnullにする
		param.setTanto(soshikiHelper.checkSoshikiParamValue(param.getTanto()));
		param.setDiv1(soshikiHelper.checkSoshikiParamValue(param.getDiv1()));
		param.setDiv2(soshikiHelper.checkSoshikiParamValue(param.getDiv2()));
		param.setDiv3(soshikiHelper.checkSoshikiParamValue(param.getDiv3()));

		int yearMonth =DateUtils.toYearMonth(param.getYear(), param.getMonth()); 
		
		OpportunitiesSearchConditionDto condition = new OpportunitiesSearchConditionDto();
		condition.setDiv1(param.getDiv1());
		condition.setDiv2(param.getDiv2());
		condition.setDiv3(param.getDiv3());
		condition.setTanto(param.getTanto());
		condition.setYearMonth(yearMonth);
		
		List<SearchResult> result = listResult(condition);
		if(result == null) {
			model.addAttribute("limitOver", true);
		} else {
			model.addAttribute("result", result);
		}
		model.addAttribute(createDateConditionResult(param));

		return "opportunityList";
	}

	/**
	 * 結果リストを生成します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param principal
	 *            プリンシパル
	 * @param header
	 *            ヘッダ情報
	 * @return 結果リスト
	 *
	 */
	protected List<SearchResult> listResult(OpportunitiesSearchConditionDto condition) {
		List<OpportunitiesDto> list = opportunitiesService.find(condition);

		if(list.size() > limit) {
			return null;
		}

		NumberFormat nfNum = NumberFormat.getNumberInstance();
		
		List<SearchResult> result = new ArrayList<>();
		for (OpportunitiesDto o : list) {
			SearchResult r = new SearchResult();
			r.opportunityName = o.getOpportunityName();
			r.name = o.getName();
			r.customer = o.getCustomer();
			r.step = o.getStep();
			r.parentId = o.getParentId();
			r.rank = o.getRank();
			r.demand = nfNum.format(o.getDemand());
			r.target = nfNum.format(o.getTarget());
			result.add(r);
		}

		return result;
	}

	/**
	 * 検索条件をもとに年月日From、Toの文字列を保持するオブジェクトを生成します。
	 * 
	 * @param param
	 *            パラメータ
	 * @return DateConditionResult
	 */
	protected DateConditionResult createDateConditionResult(SearchCondition param) {
		DateConditionResult result = new DateConditionResult();
		Calendar cal = DateUtils.getYearMonthCalendar(param.getYear(), param.getMonth());
		result.setDateFrom(Constants.DateFormat.yyyyMMdd.getFormat().format(cal.getTime()));
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		result.setDateTo(Constants.DateFormat.yyyyMMdd.getFormat().format(cal.getTime()));
		return result;
	}

	public static class SearchCondition extends SearchConditionBaseDto {
	}

	public static class SearchResult {
		private String parentId;
		private String opportunityName;
		private String name;
		private String customer;
		private String demand;
		private String target;
		private int step;
		private String rank;

		public String getParentId() {
			return parentId;
		}

		public void setParentId(String parentId) {
			this.parentId = parentId;
		}
		
		public String getOpportunityName() {
			return opportunityName;
		}

		public void setOpportunityName(String opportunityName) {
			this.opportunityName = opportunityName;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCustomer() {
			return customer;
		}

		public void setCustomer(String customer) {
			this.customer = customer;
		}
		
		public String getDemand() {
			return demand;
		}

		public void setDemand(String demand) {
			this.demand = demand;
		}
		public String getTarget() {
			return target;
		}

		public void setTarget(String target) {
			this.target = target;
		}

		public int getStep() {
			return step;
		}

		public void setStep(int step) {
			this.step = step;
		}

		public String getRank() {
			return rank;
		}

		public void setRank(String rank) {
			this.rank = rank;
		}
	}

	public static class DateConditionResult {
		String dateFrom;
		String dateTo;

		public String getDateFrom() {
			return dateFrom;
		}

		public void setDateFrom(String dateFrom) {
			this.dateFrom = dateFrom;
		}

		public String getDateTo() {
			return dateTo;
		}

		public void setDateTo(String dateTo) {
			this.dateTo = dateTo;
		}
	}
}
