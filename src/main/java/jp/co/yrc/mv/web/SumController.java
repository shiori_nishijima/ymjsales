package jp.co.yrc.mv.web;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dao.MonthlyCommentsDao;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.helper.SearchDivHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.SumHelper;
import jp.co.yrc.mv.helper.SumHelper.Result;
import jp.co.yrc.mv.helper.UserDetailsHelper;

@Controller
public class SumController {

	@Autowired
	SumHelper sumHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SearchDivHelper searchDivHelper;

	@Autowired
	MonthlyCommentsDao monthlyCommentsDao;

	@Autowired
	SoshikiHelper soshikiHelper;

	@RequestMapping(value = "/sum.html", method = RequestMethod.GET)
	public String init(SumSearchCondition param, Model model, Principal principal) {

		setupNengatsu(model);
		// デフォルトreal
		param.setSearchMode("real");
		param.setModeChange("all");
		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);
		model.addAttribute(param);

		MonthlyCommentsDto monthlyComments = getPlanDemandCoefficient(param, userDetailsHelper.getUserId(principal));
		model.addAttribute("planDemandCoefficient", monthlyComments.getPlanDemandCoefficient());
		model.addAttribute("planInfoNum", monthlyComments.getPlanInfoNum());
		model.addAttribute("planVisitNum", monthlyComments.getPlanVisitNum());

		// 表示で落ちないために空の結果を入れておく
		model.addAttribute("result", new Result());
		return "sum";
	}

	@RequestMapping(value = "/sum.html", method = RequestMethod.POST)
	public String showSum(@Valid SumSearchCondition param, Model model, BindingResult bindingResult, Principal principal) {

		setupNengatsu(model);
		model.addAttribute(param);

		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);

		Date date = DateUtils.getDBDate();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		Integer day = null;
		boolean isToday = false;
		// リアルまたは前日
		if (c.get(Calendar.YEAR) == param.getYear() && c.get(Calendar.MONTH) + 1 == param.getMonth()) {
			// 前日モードなら1日引く
			isToday = !"yesterday".equals(param.getSearchMode());
			day = isToday ? c.get(Calendar.DATE) : c.get(Calendar.DATE) - 1;
		}

		Result result = sumHelper.getResult(param.getYear(), param.getMonth(), day, userInfo, param.getTanto(),
				soshikiHelper.checkSoshikiParamValue(param.getDiv1()),
				soshikiHelper.checkSoshikiParamValue(param.getDiv2()),
				soshikiHelper.checkSoshikiParamValue(param.getDiv3()), isToday);

		Calendar targetDate = Calendar.getInstance();
		targetDate.setTime(date);
		targetDate.set(Calendar.YEAR, param.getYear());
		targetDate.set(Calendar.MONTH, param.getMonth() - 1);
		if (day != null) {
			targetDate.set(Calendar.DATE, day);
		}

		MonthlyCommentsDto monthlyComments = getPlanDemandCoefficient(param, userInfo.getId());
		model.addAttribute("planDemandCoefficient", monthlyComments.getPlanDemandCoefficient());
		model.addAttribute("planInfoNum", monthlyComments.getPlanInfoNum());
		model.addAttribute("planVisitNum", monthlyComments.getPlanVisitNum());
		model.addAttribute("targetDate", targetDate.getTime());
		model.addAttribute("outDate", date);
		model.addAttribute("result", result);
		return "sum";
	}

	protected MonthlyCommentsDto getPlanDemandCoefficient(SumSearchCondition param, String userId) {
		MonthlyCommentsDto monthlyComments = monthlyCommentsDao.selectByCondition(param.getYear(), param.getMonth(),
				userId);

		if (monthlyComments == null) {
			monthlyComments = new MonthlyCommentsDto();
		}
		if (monthlyComments.getPlanDemandCoefficient() == null) {
			monthlyComments.setPlanDemandCoefficient(BigDecimal.ZERO);
		}

		if (monthlyComments.getPlanInfoNum() == null) {
			monthlyComments.setPlanInfoNum(BigDecimal.ZERO);
		}

		if (monthlyComments.getPlanVisitNum() == null) {
			monthlyComments.setPlanVisitNum(BigDecimal.ZERO);
		}
		return monthlyComments;
	}

	protected void setupNengatsu(Model model) {
		selectYearsHelper.create(model);
	}

	public static class SumSearchCondition extends SearchConditionBaseDto {
		private String searchMode;
		private String modeChange;

		public String getSearchMode() {
			return searchMode;
		}

		public void setSearchMode(String searchMode) {
			this.searchMode = searchMode;
		}

		public String getModeChange() {
			return modeChange;
		}

		public void setModeChange(String modeChange) {
			this.modeChange = modeChange;
		}
	}
}
