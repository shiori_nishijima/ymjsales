package jp.co.yrc.mv.web;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
@RequestMapping(value = "/login")
public class LoginController {

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	/**
	 * ログイン失敗時のコントロールをする。Messageのだし分け。
	 * @param fail
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/fail", method = RequestMethod.GET)
	public String fail(Fail fail, RedirectAttributes attributes) {
		String code = fail.getCode();
		List<String> error = new ArrayList<String>();
		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);

		if ("both".equals(code)) {
			error.add(message.getMessage("jp.co.yrc.mv.error.user"));
			error.add(message.getMessage("jp.co.yrc.mv.error.password"));
		} else if ("user".equals(code)) {
			error.add(message.getMessage("jp.co.yrc.mv.error.user"));
		} else if ("password".equals(code)) {
			error.add(message.getMessage("jp.co.yrc.mv.error.password"));
		} else if ("badCredentials".equals(code)) {
			error.add(message.getMessage("jp.co.yrc.mv.error.login"));
		}
		attributes.addFlashAttribute("errors", error);

		return "redirect:/";
	}

	public static class Fail {
		private String code;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		private String userId;
		private String password;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}

	public static class UserForm {
		@NotBlank
		private String userId;
		@NotBlank
		private String password;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}
}
