package jp.co.yrc.mv.web;

import java.security.Principal;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.CallsUsersWithGroupInfoDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.CallsUsersService;

@Controller
@Transactional
public class CallsUsersCountController {

	@Value("${show.year.range.from}")
	int rangeFrom;

	@Value("${show.year.range.to}")
	int rangeTo;

	@Autowired
	CallsUsersService callsUsersService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Value("${limit}")
	int limit;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/callsUsersCount.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		// 縦合計用
		// dummyで空の結果を入れる（何も入れないと画面で落ちる）
		model.addAttribute("list", Collections.EMPTY_LIST);
		model.addAttribute(param);

		selectYearsHelper.create(model);
		model.addAttribute(param);
		return "callsUsersCount";
	}

	/**
	 * 検索時の処理を実行します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/callsUsersCount.html", method = RequestMethod.POST)
	public String find(SearchCondition param, Model model, Principal principal) {

		model.addAttribute(param);
		selectYearsHelper.create(model);

		List<CallsUsersWithGroupInfoDto> list = listResult(param);
		model.addAttribute("list", list);

		return "callsUsersCount";
	}

	/**
	 * 結果リストを生成します。
	 *
	 * @param param
	 *            パラメータ
	 * @param principal
	 *            プリンシパル
	 * @param header
	 *            ヘッダ情報
	 * @return 結果リスト
	 */
	protected List<CallsUsersWithGroupInfoDto> listResult(SearchCondition param) {

		// 過去検索時の得意先履歴条件にdiv1を利用。販社をまたいだ訪問はないため。
		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());
		String tanto = soshikiHelper.checkSoshikiParamValue(param.getTanto());

		// 個人に紐づくデータを全て表示する
		List<CallsUsersWithGroupInfoDto> list = callsUsersService.countWithGroupInfo(param.getYear(), param.getMonth(),
				DateUtils.toYearMonth(param.getYear(), param.getMonth()), div1, div2, div3, tanto);

		return list;
	}

	public static class SearchCondition extends SearchConditionBaseDto {

	}

}
