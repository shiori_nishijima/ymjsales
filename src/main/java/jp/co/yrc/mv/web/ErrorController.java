package jp.co.yrc.mv.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController {

	@Autowired
	ReloadableResourceBundleMessageSource messageSource;

	@RequestMapping(value = "/error/{errorCode}.html")
	public ModelAndView sendErrorPage(@PathVariable String errorCode) {
		MessageSourceAccessor message = new MessageSourceAccessor(messageSource);

		ModelAndView mav = new ModelAndView();
		mav.addObject("error", message.getMessage(errorCode));
		mav.setViewName("forward:/" + errorCode + ".jsp");
		return mav;
	}

}
