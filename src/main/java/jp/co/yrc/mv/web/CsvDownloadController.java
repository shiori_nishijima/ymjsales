package jp.co.yrc.mv.web;

import static jp.co.yrc.mv.common.MathUtils.HUNDRED;
import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.dao.DailyCommentsDao;
import jp.co.yrc.mv.dto.AccountsDto;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerAccountDto;
import jp.co.yrc.mv.dto.MonthVisitResultPerSalesDto;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.dto.UserSosikiDto;
import jp.co.yrc.mv.entity.DailyComments;
import jp.co.yrc.mv.entity.Markets;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.helper.SelectGroupingHelper;
import jp.co.yrc.mv.helper.SelectMarketHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.SoshikiHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.service.AccountsService;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.MarketsService;
import jp.co.yrc.mv.service.SelectionItemsService;
import jp.co.yrc.mv.service.UsersService;

@Controller
@Transactional
public class CsvDownloadController {

	@Autowired
	AccountsService accountsService;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SelectionItemsService selectionItemsService;

	@Autowired
	CallsService callsService;

	@Autowired
	UsersService usersService;

	@Autowired
	MarketsService marketsService;

	@Autowired
	SoshikiHelper soshikiHelper;

	@Autowired
	SelectGroupingHelper selectGroupingHelper;

	@Autowired
	SelectMarketHelper selectMarketHelper;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	DailyCommentsDao dailyCommentsDao;

	@Value("${csvDownload.monthVisitResultsPerSales.csv.header}")
	String monthVisitResultsPerSalesTemplate;

	@Value("${csvDownload.monthVisitResultsPerSales.csv.filename}")
	String monthVisitResultsPerSalesFilename;

	@Value("${csvDownload.monthVisitResultsPerAccount.csv.header}")
	String monthVisitResultsPerAccountTemplate;

	@Value("${csvDownload.monthVisitResultsPerAccount.csv.filename}")
	String monthVisitResultsPerAccountFilename;

	@Value("${csvDownload.dailyVisitResultsPerSales.csv.header}")
	String dailyVisitResultsPerSalesTemplate;

	@Value("${csvDownload.dailyVisitResultsPerSales.csv.filename}")
	String dailyVisitResultsPerSalesFilename;

	@Value("${csvDownload.dailyVisitResultsPerAccount.csv.header}")
	String dailyVisitResultsPerAccountTemplate;

	@Value("${csvDownload.dailyVisitResultsPerAccount.csv.filename}")
	String dailyVisitResultsPerAccountFilename;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/csvDownload.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {	
		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		selectYearsHelper.create(model);
		selectGroupingHelper.createGroup(model);
		selectMarketHelper.createMarket(model);
		model.addAttribute(param);

		return "csvDownload";
	}

	/**
	 * 日別訪問実績（個人別）
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = "/csvDownload.html", method = RequestMethod.POST, params = { "dailyVisitResultsPerSales" })
	@ResponseBody
	public ResponseEntity<ByteArrayResource> downloadDailyVisitResultsPerSales(@Validated SearchCondition param,
			Model model, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());

		Calendar c = Calendar.getInstance();
		c.set(param.year, param.month - 1, 1, 0, 0, 0);
		List<DailyInfo> dailyHeaderList = createHeader(c);
		String dailyHeaderString = "";
		for (DailyInfo daily : dailyHeaderList) {
			dailyHeaderString = dailyHeaderString + String.format("%s日,", daily.date);
		}

		List<CallCountInfoDto> dailyInfoList = callsService.listDailyUserCallDistinctionSosoki(param.year, param.month,
				null, div1, div2, div3);
		Map<UserKey, CallInfo> map = new LinkedHashMap<>();

		// データをユーザー、日付毎に整形。DBA側でソートしているのでソートは不要
		for (CallCountInfoDto o : dailyInfoList) {

			UserKey key = new UserKey(o.getGroupType(), o.getSalesCompanyCode(), o.getDepartmentCode(),
					o.getSalesOfficeCode());
			CallInfo info = map.get(key);

			// 初めての得意先の場合、データを作る
			if (info == null) {

				UserSosikiDto user = usersService.selectUserById(o.getGroupType(),
						Collections.singletonList(o.getSalesCompanyCode()), o.getDepartmentCode(),
						o.getSalesOfficeCode(), param.year, param.month);

				// ユーザーが存在しない場合は飛ばす。社内同行で他営業所の場合が該当
				if (user == null) {
					continue;
				}
				info = new CallInfo();
				map.put(key, info);
				info.setFirstName(user.getFirstName());
				info.setLastName(user.getLastName());
				info.setEigyoSosikiNm(user.getEigyoSosikiNm());

				// 1ヶ月分の入れ物を作る
				for (DailyInfo dailyHeader : dailyHeaderList) {
					DailyInfo daily = new DailyInfo();
					daily.setDate(dailyHeader.getDate());
					info.getDailyInfos().add(daily);
				}

				List<DailyComments> comments = dailyCommentsDao.listMonthlyComment(param.year, param.month,
						o.getGroupType());

				for (DailyComments comment : comments) {
					info.getDailyInfos().get(comment.getDate() - 1).setDailyCommentCompleted(comment.isCompleted());
				}

			}

			Date date = o.getDateStart();
			int day = DateUtils.getDay(date);

			DailyInfo dailyInfo = info.getDailyInfos().get(day - 1);
			dailyInfo.setResult(dailyInfo.getResult().add(toDefaultZero(o.getCallCount())));
			if (dailyInfo.isDailyCommentCompleted()) {

				// 日報が完了していないものは表示しないので足し込みを除外する
				// 合計
				info.setResult(info.getResult().add(toDefaultZero(o.getCallCount())));
			}
		}

		HttpHeaders headers = new HttpHeaders();

		try {
			headers.setContentDispositionFormData("attachment",
					new URLCodec().encode(String.format(dailyVisitResultsPerSalesFilename, param.year, param.month)));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		headers.setContentType(MediaType.valueOf("text/csv;charset=Shift_jis"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos);
				OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("MS932"));
				BufferedWriter bw = new BufferedWriter(osw);
				ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {
			String csvHeader = String.format(dailyVisitResultsPerSalesTemplate, dailyHeaderString);
			String[] headerArr = csvHeader.split(",");

			csvWriter.writeHeader(headerArr);

			for (CallInfo info : map.values()) {
				List<Object> csv = new ArrayList<>();

				csv.add(info.getEigyoSosikiNm());
				csv.add(StringUtils.defaultString(info.getLastName()) + StringUtils.defaultString(info.getFirstName()));
				csv.add(String.format("%1$04d年%2$02d月", param.year, param.month));
				csv.add(info.getResult());

				for (DailyInfo daily : info.getDailyInfos()) {
					if (daily.isDailyCommentCompleted()) {
						csv.add(daily.getResult());
					} else {
						// 日報が完了していないものはハイフンとする2015年9月15日打合せより
						csv.add("-");
					}
				}

				csvWriter.write(csv);
			}
			csvWriter.flush();
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), headers, HttpStatus.OK);
	}

	/**
	 * 日別訪問実績（得意先別）
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = "/csvDownload.html", method = RequestMethod.POST, params = { "dailyVisitResultsPerAccount" })
	@ResponseBody
	public ResponseEntity<ByteArrayResource> downloadDailyVisitResultsPerAccount(@Validated SearchCondition param,
			Model model, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());

		String group = selectGroupingHelper.allToNull(param.group);
		String marketId = selectMarketHelper.allToNull(param.market);

		Calendar c = Calendar.getInstance();
		c.set(param.year, param.month - 1, 1, 0, 0, 0);
		List<DailyInfo> dailyHeaderList = createHeader(c);
		String dailyHeaderString = "";
		for (DailyInfo daily : dailyHeaderList) {
			dailyHeaderString = dailyHeaderString + String.format("%s日,", daily.date);
		}

		List<CallCountInfoDto> dailyInfoList = callsService.sumDailyAccountCall(param.year, param.month, null, div1,
				div2, div3, marketId, group);
		Map<String, CallInfo> map = new LinkedHashMap<>();

		List<Markets> markets = marketsService.listAll();
		Map<String, String> marketLabels = new HashMap<>();
		for (Markets market : markets) {
			marketLabels.put(market.getId(), market.getName());
		}

		// データを得意先、日付毎に整形。DB側でソートしているのでソートは不要
		for (CallCountInfoDto o : dailyInfoList) {

			CallInfo info = map.get(o.getGroupType());

			// 初めての得意先の場合、データを作る
			if (info == null) {

				AccountsDto account = accountsService.findAccountInfoById(param.getYear(), param.getMonth(),
						o.getGroupType());

				UserSosikiDto user = usersService.selectUserByIdForCustomer(account.getAssignedUserId(),
						Collections.singletonList(account.getSalesCompanyCode()), account.getDepartmentCode(),
						account.getSalesOfficeCode(), param.year, param.month);

				// ユーザーが存在しない場合は飛ばす。過去でユーザーが移籍している等が考えられる
				if (user == null) {
					continue;
				}

				info = new CallInfo();
				map.put(o.getGroupType(), info);

				info.setFirstName(user.getFirstName());
				info.setLastName(user.getLastName());
				info.setEigyoSosikiNm(user.getEigyoSosikiNm());
				info.setMarket(marketLabels.get(account.getMarketId()));
				info.setAccountName(account.getName());
				info.setAccountId(account.getId());
				info.setGroupImportant(account.isGroupImportant());
				info.setGroupNew(account.isGroupNew());
				info.setGroupDeepPlowing(account.isGroupDeepPlowing());
				info.setGroupYCp(account.isGroupYCp());
				info.setGroupOtherCp(account.isGroupOtherCp());
				info.setGroupOne(account.isGroupOne());
				info.setGroupTwo(account.isGroupTwo());
				info.setGroupThree(account.isGroupThree());
				info.setGroupFour(account.isGroupFour());
				info.setGroupFive(account.isGroupFive());

				// 1ヶ月分の入れ物を作る
				for (DailyInfo dailyHeader : dailyHeaderList) {
					DailyInfo daily = new DailyInfo();
					daily.setDate(dailyHeader.getDate());
					info.getDailyInfos().add(daily);
				}
			}

			Date date = o.getDateStart();
			int day = DateUtils.getDay(date);

			DailyInfo dailyInfo = info.getDailyInfos().get(day - 1);
			dailyInfo.setResult(dailyInfo.getResult().add(toDefaultZero(o.getCallCount())));
			dailyInfo.setDailyCommentCompleted(o.isDailyCommentCompleted());
			// 合計
			info.setResult(info.getResult().add(toDefaultZero(o.getCallCount())));
		}

		HttpHeaders headers = new HttpHeaders();

		try {
			headers.setContentDispositionFormData("attachment",
					new URLCodec().encode(String.format(dailyVisitResultsPerAccountFilename, param.year, param.month)));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		headers.setContentType(MediaType.valueOf("text/csv;charset=Shift_jis"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos);
				OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("MS932"));
				BufferedWriter bw = new BufferedWriter(osw);
				ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {

			String csvHeader = String.format(dailyVisitResultsPerAccountTemplate, dailyHeaderString);
			String[] headerArr = csvHeader.split(",");

			csvWriter.writeHeader(headerArr);

			for (CallInfo info : map.values()) {
				List<Object> csv = new ArrayList<>();

				csv.add(info.getEigyoSosikiNm());
				csv.add(StringUtils.defaultString(info.getLastName()) + StringUtils.defaultString(info.getFirstName()));
				csv.add(String.format("%1$04d年%2$02d月", param.year, param.month));
				csv.add(info.getAccountName());
				csv.add(info.getAccountId());
				csv.add(info.getMarket());
				csv.add(info.isGroupImportant() ? "○" : "");
				csv.add(info.isGroupNew() ? "○" : "");
				csv.add(info.isGroupDeepPlowing() ? "○" : "");
				csv.add(info.isGroupYCp() ? "○" : "");
				csv.add(info.isGroupOtherCp() ? "○" : "");
				csv.add(info.isGroupOne() ? "○" : "");
				csv.add(info.isGroupTwo() ? "○" : "");
				csv.add(info.isGroupThree() ? "○" : "");
				csv.add(info.isGroupFour() ? "○" : "");
				csv.add(info.isGroupFive() ? "○" : "");
				csv.add(info.getResult());

				for (DailyInfo daily : info.getDailyInfos()) {
					csv.add(daily.getResult());
				}

				csvWriter.write(csv);
			}
			csvWriter.flush();
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), headers, HttpStatus.OK);
	}

	/**
	 * カレンダーのヘッダー（日付部分）を作成
	 * 
	 * @param c カレンダー
	 * @return 日付リスト
	 */
	protected List<DailyInfo> createHeader(Calendar c) {
		// カレンダー
		List<DailyInfo> header = new ArrayList<>();
		for (int i = 1, max = c.getActualMaximum(Calendar.DATE); i <= max; i++, c.add(Calendar.DATE, 1)) {
			DailyInfo daily = new DailyInfo();
			daily.date = i;
			header.add(daily);
		}
		return header;
	}

	/**
	 * 月次訪問実績（得意先別）
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = "/csvDownload.html", method = RequestMethod.POST, params = { "monthVisitResultsPerAccount" })
	@ResponseBody
	public ResponseEntity<ByteArrayResource> downloadMonthVisitResultsPerAccount(@Validated SearchCondition param,
			Model model, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());

		String group = selectGroupingHelper.allToNull(param.group);
		String market = selectMarketHelper.allToNull(param.market);

		List<MonthVisitResultPerAccountDto> list = callsService.sumMonthVisitResultPerAccount(param.year, param.month,
				div1, div2, div3, market, group);

		HttpHeaders headers = new HttpHeaders();

		try {
			headers.setContentDispositionFormData("attachment",
					new URLCodec().encode(String.format(monthVisitResultsPerAccountFilename, param.year, param.month)));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		headers.setContentType(MediaType.valueOf("text/csv;charset=Shift_jis"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos);
				OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("MS932"));
				BufferedWriter bw = new BufferedWriter(osw);
				ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {

			List<SelectionItems> divcList = selectionItemsService.listDivisionC();
			String divCNames = "";
			for (SelectionItems item : divcList) {
				divCNames = divCNames + item.getLabel() + ",";

			}

			String csvHeader = String.format(monthVisitResultsPerAccountTemplate, divCNames);
			String[] headerArr = csvHeader.split(",");

			csvWriter.writeHeader(headerArr);
			for (MonthVisitResultPerAccountDto dto : list) {
				List<Object> csv = new ArrayList<>();
				csv.add(dto.getEigyoSosikiNm());
				csv.add(StringUtils.defaultString(dto.getLastName()) + StringUtils.defaultString(dto.getFirstName()));
				csv.add(String.format("%1$04d年%2$02d月", param.year, param.month));
				csv.add(dto.getName());
				csv.add(dto.getParentId());
				csv.add(dto.getId());
				csv.add(dto.getMarketName());
				csv.add(dto.isGroupImportant() ? "○" : "");
				csv.add(dto.isGroupNew() ? "○" : "");
				csv.add(dto.isGroupDeepPlowing() ? "○" : "");
				csv.add(dto.isGroupYCp() ? "○" : "");
				csv.add(dto.isGroupOtherCp() ? "○" : "");
				csv.add(dto.isGroupOne() ? "○" : "");
				csv.add(dto.isGroupTwo() ? "○" : "");
				csv.add(dto.isGroupThree() ? "○" : "");
				csv.add(dto.isGroupFour() ? "○" : "");
				csv.add(dto.isGroupFive() ? "○" : "");
				csv.add(dto.getLastMonthCallCount());
				csv.add(dto.getPlannedCount());
				csv.add(dto.getCallCount());
				csv.add(dto.getInfoCount());
				/* DBに訪問理由を変更した場合はここを変更する必要あり  */
				csv.add(dto.getDivisionC01());
				csv.add(dto.getDivisionC02());
				csv.add(dto.getDivisionC03());
				csv.add(dto.getDivisionC04());
				csv.add(dto.getDivisionC05());
				csv.add(dto.getDivisionC06());
				csv.add(dto.getDivisionC07());
				csv.add(dto.getDivisionC08());
				csv.add(dto.getDivisionC09());
				csv.add(dto.getDivisionC10());
				csv.add(dto.getDivisionC11());
				csv.add(dto.getDivisionC12());
				csv.add(dto.getDivisionC21());
				csv.add(dto.getDivisionC22());
				csv.add(dto.getDivisionC23());
				csv.add(dto.getDivisionC24());
				csv.add(dto.getDivisionC31());
				csv.add(dto.getDivisionC32());
				csv.add(dto.getDivisionC33());
				csv.add(dto.getDivisionC41());
				csv.add(dto.getDivisionC42());
				csv.add(dto.getDivisionC43());
				csv.add(dto.getDivisionC51());
				csv.add(dto.getDivisionC52());
				csv.add(dto.getDivisionC53());
				csv.add(dto.getDivisionC71());
				csv.add(dto.getDivisionC72());
				csv.add(dto.getDivisionC73());
				csv.add(dto.getDivisionC74());
				csv.add(dto.getDivisionC81());
				csv.add(dto.getDivisionC82());
				csv.add(dto.getDivisionC83());
				csv.add(dto.getDivisionC84());
				csv.add(dto.getDivisionC98());
				csv.add(dto.getDivisionC99());
				csv.add(MathUtils.toK(dto.getSales()));

				csv.add(dto.getNumber());

				csvWriter.write(csv);
			}
			csvWriter.flush();
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), headers, HttpStatus.OK);
	}

	/**
	 * 月次訪問実績（個人別）
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return CSVデータ
	 */
	@RequestMapping(value = "/csvDownload.html", method = RequestMethod.POST, params = { "monthVisitResultsPerSales" })
	@ResponseBody
	public ResponseEntity<ByteArrayResource> downloadMonthVisitResultsPerSales(@Validated SearchCondition param,
			Model model, Principal principal) {

		String div1 = soshikiHelper.checkSoshikiParamValue(param.getDiv1());
		String div2 = soshikiHelper.checkSoshikiParamValue(param.getDiv2());
		String div3 = soshikiHelper.checkSoshikiParamValue(param.getDiv3());

		List<MonthVisitResultPerSalesDto> list = callsService.sumMonthVisitResultPerSales(param.year, param.month,
				div1, div2, div3);

		HttpHeaders headers = new HttpHeaders();
		try {
			headers.setContentDispositionFormData("attachment",
					new URLCodec().encode(String.format(monthVisitResultsPerSalesFilename, param.year, param.month)));
		} catch (EncoderException e) {
			// 通常は発生しない
			throw new RuntimeException(e);
		}

		headers.setContentType(MediaType.valueOf("text/csv;charset=Shift_jis"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (BufferedOutputStream bos = new BufferedOutputStream(baos);
				OutputStreamWriter osw = new OutputStreamWriter(bos, Charset.forName("MS932"));
				BufferedWriter bw = new BufferedWriter(osw);
				ICsvListWriter csvWriter = new CsvListWriter(bw, CsvPreference.EXCEL_PREFERENCE)) {

			List<SelectionItems> divcList = selectionItemsService.listDivisionC();
			String divCNames = "";
			for (SelectionItems item : divcList) {
				divCNames = divCNames + item.getLabel() + ",";

			}

			String csvHeader = String.format(monthVisitResultsPerSalesTemplate, divCNames);
			String[] headerArr = csvHeader.split(",");

			csvWriter.writeHeader(headerArr);
			for (MonthVisitResultPerSalesDto dto : list) {
				List<Object> csv = new ArrayList<>();
				csv.add(dto.getEigyoSosikiNm());
				csv.add(dto.getIdForCustomer());
				csv.add(StringUtils.defaultString(dto.getLastName()) + StringUtils.defaultString(dto.getFirstName()));
				csv.add(dto.getOwnCount());
				csv.add(dto.getDealCount());
				csv.add(dto.getDealResult());
				csv.add(MathUtils.toPercentage(dto.getDealResult(), dto.getDealCount()));
				csv.add(MathUtils.toPercentage(dto.getDealResult(), dto.getOwnCount()));
				csv.add(dto.getSs());
				csv.add(dto.getPs());
				csv.add(dto.getRs());
				csv.add(dto.getCd());
				csv.add(dto.getCs());
				csv.add(dto.getTruck());
				csv.add(dto.getBus());
				csv.add(dto.getHireTaxi());
				csv.add(dto.getPlanVisitNum());
				csv.add(dto.getCallCount());
				csv.add(MathUtils.toPercentage(dto.getCallCount(), dto.getPlanVisitNum()));
				csv.add(dto.getLastMonthCallCount());
				csv.add(MathUtils.toPercentage(dto.getCallCount(), dto.getLastMonthCallCount()));
				csv.add(dto.getInfoCount());
				/* DBに訪問理由を変更した場合はここを変更する必要あり  */
				csv.add(dto.getDivisionC01());
				csv.add(dto.getDivisionC02());
				csv.add(dto.getDivisionC03());
				csv.add(dto.getDivisionC04());
				csv.add(dto.getDivisionC05());
				csv.add(dto.getDivisionC06());
				csv.add(dto.getDivisionC07());
				csv.add(dto.getDivisionC08());
				csv.add(dto.getDivisionC09());
				csv.add(dto.getDivisionC10());
				csv.add(dto.getDivisionC11());
				csv.add(dto.getDivisionC12());
				csv.add(dto.getDivisionC21());
				csv.add(dto.getDivisionC22());
				csv.add(dto.getDivisionC23());
				csv.add(dto.getDivisionC24());
				csv.add(dto.getDivisionC31());
				csv.add(dto.getDivisionC32());
				csv.add(dto.getDivisionC33());
				csv.add(dto.getDivisionC41());
				csv.add(dto.getDivisionC42());
				csv.add(dto.getDivisionC43());
				csv.add(dto.getDivisionC51());
				csv.add(dto.getDivisionC52());
				csv.add(dto.getDivisionC53());
				csv.add(dto.getDivisionC71());
				csv.add(dto.getDivisionC72());
				csv.add(dto.getDivisionC73());
				csv.add(dto.getDivisionC74());
				csv.add(dto.getDivisionC81());
				csv.add(dto.getDivisionC82());
				csv.add(dto.getDivisionC83());
				csv.add(dto.getDivisionC84());
				csv.add(dto.getDivisionC98());
				csv.add(dto.getDivisionC99());

				BigDecimal planDemandCoefficient = dto.getPlanDemandCoefficient();
				if (planDemandCoefficient.compareTo(BigDecimal.ZERO) != 0) {
					planDemandCoefficient = planDemandCoefficient.divide(HUNDRED);
				}

				BigDecimal demand = dto.getDemandNumber().multiply(planDemandCoefficient)
						.setScale(0, RoundingMode.HALF_UP);

				csv.add(demand);
				csv.add(dto.getNumber());
				csv.add(MathUtils.toPercentage(dto.getNumber(), demand));

				csv.add(dto.getPlanNumber());
				csv.add(MathUtils.toPercentage(dto.getNumber(), dto.getPlanNumber()));

				csv.add(MathUtils.toK(dto.getSales()));

				csvWriter.write(csv);
			}
			csvWriter.flush();
		} catch (IOException e) {
			// ByteArrayOutputStreamnなので通常あり得ない
			throw new RuntimeException(e);
		}
		return new ResponseEntity<>(new ByteArrayResource(baos.toByteArray()), headers, HttpStatus.OK);
	}

	public static class DailyInfo {
		int date;

		BigDecimal plan = BigDecimal.ZERO;
		BigDecimal result = BigDecimal.ZERO;

		boolean dailyCommentCompleted;

		public boolean isDailyCommentCompleted() {
			return dailyCommentCompleted;
		}

		public void setDailyCommentCompleted(boolean dailyCommentCompleted) {
			this.dailyCommentCompleted = dailyCommentCompleted;
		}

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		public int getDate() {
			return date;
		}

		public void setDate(int date) {
			this.date = date;
		}

	}

	public static class CallInfo {
		List<DailyInfo> dailyInfos = new ArrayList<>();

		String lastName;
		String firstName;
		String eigyoSosikiNm;
		String accountName;
		String accountId;
		BigDecimal result = BigDecimal.ZERO;
		String market;
		boolean groupImportant;
		boolean groupNew;
		boolean groupDeepPlowing;
		boolean groupYCp;
		boolean groupOtherCp;
		boolean groupOne;
		boolean groupTwo;
		boolean groupThree;
		boolean groupFour;
		boolean groupFive;

		public String getMarket() {
			return market;
		}

		public void setMarket(String market) {
			this.market = market;
		}

		public boolean isGroupImportant() {
			return groupImportant;
		}

		public void setGroupImportant(boolean groupImportant) {
			this.groupImportant = groupImportant;
		}

		public boolean isGroupNew() {
			return groupNew;
		}

		public void setGroupNew(boolean groupNew) {
			this.groupNew = groupNew;
		}

		public boolean isGroupDeepPlowing() {
			return groupDeepPlowing;
		}

		public void setGroupDeepPlowing(boolean groupDeepPlowing) {
			this.groupDeepPlowing = groupDeepPlowing;
		}

		public boolean isGroupYCp() {
			return groupYCp;
		}

		public void setGroupYCp(boolean groupYCp) {
			this.groupYCp = groupYCp;
		}

		public boolean isGroupOtherCp() {
			return groupOtherCp;
		}

		public void setGroupOtherCp(boolean groupOtherCp) {
			this.groupOtherCp = groupOtherCp;
		}

		public boolean isGroupOne() {
			return groupOne;
		}

		public void setGroupOne(boolean groupOne) {
			this.groupOne = groupOne;
		}

		public boolean isGroupTwo() {
			return groupTwo;
		}

		public void setGroupTwo(boolean groupTwo) {
			this.groupTwo = groupTwo;
		}

		public boolean isGroupThree() {
			return groupThree;
		}

		public void setGroupThree(boolean groupThree) {
			this.groupThree = groupThree;
		}

		public boolean isGroupFour() {
			return groupFour;
		}

		public void setGroupFour(boolean groupFour) {
			this.groupFour = groupFour;
		}

		public boolean isGroupFive() {
			return groupFive;
		}

		public void setGroupFive(boolean groupFive) {
			this.groupFive = groupFive;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public BigDecimal getResult() {
			return result;
		}

		public void setResult(BigDecimal result) {
			this.result = result;
		}

		public List<DailyInfo> getDailyInfos() {
			return dailyInfos;
		}

		public void setDailyInfos(List<DailyInfo> dailyInfos) {
			this.dailyInfos = dailyInfos;
		}

		public String getAccountName() {
			return accountName;
		}

		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}

		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		public String getEigyoSosikiNm() {
			return eigyoSosikiNm;
		}

		public void setEigyoSosikiNm(String eigyoSosikiNm) {
			this.eigyoSosikiNm = eigyoSosikiNm;
		}

	}

	public static class SearchCondition extends SearchConditionBaseDto {
		@NotNull
		private Integer year;
		@NotNull
		private Integer month;
		@NotBlank
		private String div1;
		@NotBlank
		private String div2;
		@NotBlank
		private String div3;

		private String group;
		private String market;

		public Integer getYear() {
			return year;
		}

		public void setYear(Integer year) {
			this.year = year;
		}

		public Integer getMonth() {
			return month;
		}

		public void setMonth(Integer month) {
			this.month = month;
		}

		public String getDiv1() {
			return div1;
		}

		public void setDiv1(String div1) {
			this.div1 = div1;
		}

		public String getDiv2() {
			return div2;
		}

		public void setDiv2(String div2) {
			this.div2 = div2;
		}

		public String getDiv3() {
			return div3;
		}

		public void setDiv3(String div3) {
			this.div3 = div3;
		}

		public String getGroup() {
			return group;
		}

		public void setGroup(String group) {
			this.group = group;
		}

		public String getMarket() {
			return market;
		}

		public void setMarket(String market) {
			this.market = market;
		}

	}

	private static class UserKey {

		public UserKey(String userId, String salesCompanyCode, String departmentCode, String salesOfficeCode) {
			super();
			this.userId = userId;
			this.salesCompanyCode = salesCompanyCode;
			this.departmentCode = departmentCode;
			this.salesOfficeCode = salesOfficeCode;
		}

		private String userId;
		private String salesCompanyCode;
		private String departmentCode;
		private String salesOfficeCode;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((departmentCode == null) ? 0 : departmentCode.hashCode());
			result = prime * result + ((salesCompanyCode == null) ? 0 : salesCompanyCode.hashCode());
			result = prime * result + ((salesOfficeCode == null) ? 0 : salesOfficeCode.hashCode());
			result = prime * result + ((userId == null) ? 0 : userId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserKey other = (UserKey) obj;
			if (departmentCode == null) {
				if (other.departmentCode != null)
					return false;
			} else if (!departmentCode.equals(other.departmentCode))
				return false;
			if (salesCompanyCode == null) {
				if (other.salesCompanyCode != null)
					return false;
			} else if (!salesCompanyCode.equals(other.salesCompanyCode))
				return false;
			if (salesOfficeCode == null) {
				if (other.salesOfficeCode != null)
					return false;
			} else if (!salesOfficeCode.equals(other.salesOfficeCode))
				return false;
			if (userId == null) {
				if (other.userId != null)
					return false;
			} else if (!userId.equals(other.userId))
				return false;
			return true;
		}

	}

}
