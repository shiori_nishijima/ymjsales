package jp.co.yrc.mv.web;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.SearchConditionBaseDto;
import jp.co.yrc.mv.helper.SelectCompassKindHelper;
import jp.co.yrc.mv.helper.SelectResultsHelper;
import jp.co.yrc.mv.helper.SelectYearsHelper;
import jp.co.yrc.mv.helper.UserDetailsHelper;
import jp.co.yrc.mv.html.SelectItem;

@Controller
@Transactional
public class ChartKindAchievementRateController {

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	SelectYearsHelper selectYearsHelper;

	@Autowired
	SelectResultsHelper selectResultsHelper;

	@Autowired
	SelectCompassKindHelper selectCompassKindHelper;

	/**
	 * 初期表示時の処理を実行します。
	 * 
	 * @param param パラメータ
	 * @param model モデル
	 * @param principal プリンシパル
	 * @return 画面名
	 */
	@RequestMapping(value = "/chartKindAchievementRate.html", method = RequestMethod.GET)
	public String init(SearchCondition param, Model model, Principal principal) {

		Calendar c = DateUtils.getYearMonthCalendar();
		param.setYear(c.get(Calendar.YEAR));
		param.setMonth(c.get(Calendar.MONTH) + 1);

		model.addAttribute(param);

		selectCompassKindHelper.createMap(model, true);
		selectYearsHelper.create(model);
		List<SelectItem> net = selectResultsHelper.createNet(model, userDetailsHelper.getUserInfo(principal));
		List<SelectItem> type = selectResultsHelper.createType(model);

		// 初期表示先頭を選択しておく
		if (!net.isEmpty()) {
			param.setNet(net.get(0).getValue());
		}
		if (!type.isEmpty()) {
			param.setResultType(type.get(0).getValue());
		}
		return "chartKindAchievementRate";

	}

	public static class SearchCondition extends SearchConditionBaseDto {

	}
}
