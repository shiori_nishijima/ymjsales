package jp.co.yrc.mv.framework;

import javax.sql.DataSource;

import org.seasar.doma.jdbc.DomaAbstractConfig;
import org.seasar.doma.jdbc.JdbcLogger;
import org.seasar.doma.jdbc.dialect.Dialect;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

/**
 * domaの設定クラス。Springとの共存を実現。
 */
public class AppConfig extends DomaAbstractConfig {

	protected DataSource dataSource;

	protected Dialect dialect;

	private static JdbcLogger jdbclogger = new DomaLogger();

	/**
	 * {@link TransactionAwareDataSourceProxy}を返却することで、Springのトランザクションを使用する。
	 * @see org.seasar.doma.jdbc.Config#getDataSource()
	 */
	@Override
	public DataSource getDataSource() {
		return new TransactionAwareDataSourceProxy(dataSource);
	}

	@Override
	public Dialect getDialect() {
		return dialect;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setDialect(Dialect dialect) {
		this.dialect = dialect;
	}

	public JdbcLogger getJdbcLogger() {
		return jdbclogger;
	}

}