package jp.co.yrc.mv.framework;

import java.sql.SQLException;
import java.text.MessageFormat;

import org.seasar.doma.jdbc.JdbcLogger;
import org.seasar.doma.jdbc.Sql;
import org.seasar.doma.jdbc.SqlExecutionSkipCause;
import org.seasar.doma.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DomaLogger implements JdbcLogger {

	/** このインスタンスで使用するロガーです。 */
	protected final Logger logger = LoggerFactory.getLogger(DomaLogger.class);
	
	/** SQL専用のロガー トレースしやすいようにロガーを分ける */
	protected final Logger sqlLogger = LoggerFactory.getLogger(DomaLogger.class.getName() + ".sql");

	@Override
	public void logDaoMethodEntering(String callerClassName, String callerMethodName, Object... args) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, "ENTRY"));
		}
	}

	@Override
	public void logDaoMethodExiting(String callerClassName, String callerMethodName, Object result) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} RETURN {2}", callerClassName, callerMethodName, result));
		}
	}

	@Override
	public void logDaoMethodThrowing(String callerClassName, String callerMethodName, RuntimeException e) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} THROW {2}", callerClassName, callerMethodName, e.toString()));
		}
	}

	@Override
	public void logSqlExecutionSkipping(String callerClassName, String callerMethodName, SqlExecutionSkipCause cause) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, cause.name()));
		}
	}

	@Override
	public void logSql(String callerClassName, String callerMethodName, Sql<?> sql) {
		if (sqlLogger.isDebugEnabled()) {
			String formatSql = sql.getFormattedSql().replaceAll("\\r?\\n\\s*", " ").replaceAll("\\t", " ");

			sqlLogger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2076.getMessage(sql.getSqlFilePath(), formatSql)));
		}
	}

	@Override
	public void logLocalTransactionBegun(String callerClassName, String callerMethodName, String transactionId) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2063.getMessage(transactionId)));
		}
	}

	@Override
	public void logLocalTransactionEnded(String callerClassName, String callerMethodName, String transactionId) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2064.getMessage(transactionId)));
		}
	}

	@Override
	public void logLocalTransactionSavepointCreated(String callerClassName, String callerMethodName,
			String transactionId, String savepointName) {
	}

	@Override
	public void logLocalTransactionSavepointReleased(String callerClassName, String callerMethodName,
			String transactionId, String savepointName) {
	}

	@Override
	public void logLocalTransactionCommitted(String callerClassName, String callerMethodName, String transactionId) {
		logger.info(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
				Message.DOMA2067.getMessage(transactionId)));
	}

	@Override
	public void logLocalTransactionRolledback(String callerClassName, String callerMethodName, String transactionId) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2068.getMessage(transactionId)));
		}
	}

	@Override
	public void logLocalTransactionSavepointRolledback(String callerClassName, String callerMethodName,
			String transactionId, String savepointName) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2069.getMessage(transactionId, savepointName)));
		}

	}

	@Override
	public void logLocalTransactionRollbackFailure(String callerClassName, String callerMethodName,
			String transactionId, SQLException e) {
		if (logger.isDebugEnabled()) {
			logger.debug(MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
					Message.DOMA2070.getMessage(transactionId)));
		}
	}

	@Override
	public void logAutoCommitEnablingFailure(String callerClassName, String callerMethodName, SQLException e) {
		logger.error(
				MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, Message.DOMA2071.getMessage()),
				e);
	}

	@Override
	public void logTransactionIsolationSettingFailuer(String callerClassName, String callerMethodName,
			int transactionIsolationLevel, SQLException e) {
		logger.error(
				MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName,
						Message.DOMA2072.getMessage(transactionIsolationLevel)), e);
	}

	@Override
	public void logConnectionClosingFailure(String callerClassName, String callerMethodName, SQLException e) {
		logger.error(
				MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, Message.DOMA2073.getMessage()),
				e);
	}

	@Override
	public void logStatementClosingFailure(String callerClassName, String callerMethodName, SQLException e) {
		logger.error(
				MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, Message.DOMA2074.getMessage()),
				e);
	}

	@Override
	public void logResultSetClosingFailure(String callerClassName, String callerMethodName, SQLException e) {
		logger.error(
				MessageFormat.format("{0}#{1} {2}", callerClassName, callerMethodName, Message.DOMA2075.getMessage()),
				e);
	}

}
