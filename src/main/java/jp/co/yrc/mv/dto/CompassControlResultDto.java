package jp.co.yrc.mv.dto;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

import jp.co.yrc.mv.entity.Results;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CompassControlResultDto extends Results {

	/** 帳票表示順 */
	Integer reportDisplaySeq;

	/** 帳票表示品種名称) */
	String reportDisplayName;

	/** 販管計画品種コード */
	String planProductKind;

	/** 販管計画品種名称 */
	String planProductKindName;

	/** 共通品種名称(Compass品種名) */
	String compassKindName;

	public Integer getReportDisplaySeq() {
		return reportDisplaySeq;
	}

	public void setReportDisplaySeq(Integer reportDisplaySeq) {
		this.reportDisplaySeq = reportDisplaySeq;
	}

	public String getReportDisplayName() {
		return reportDisplayName;
	}

	public void setReportDisplayName(String reportDisplayName) {
		this.reportDisplayName = reportDisplayName;
	}

	public String getPlanProductKind() {
		return planProductKind;
	}

	public void setPlanProductKind(String planProductKind) {
		this.planProductKind = planProductKind;
	}

	public String getPlanProductKindName() {
		return planProductKindName;
	}

	public void setPlanProductKindName(String planProductKindName) {
		this.planProductKindName = planProductKindName;
	}

	public String getCompassKindName() {
		return compassKindName;
	}

	public void setCompassKindName(String compassKindName) {
		this.compassKindName = compassKindName;
	}

}
