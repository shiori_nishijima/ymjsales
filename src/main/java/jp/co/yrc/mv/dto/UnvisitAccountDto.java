package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class UnvisitAccountDto {

	String userId;
	String firstName;
	String lastName;
	String eigyoSosikiNm;
	String accountName;

    /** グルーピング重点 */
    @Column(name = "group_important")
    boolean groupImportant;

    /** グルーピング新規 */
    @Column(name = "group_new")
    boolean groupNew;

    /** グルーピング深耕 */
    @Column(name = "group_deep_plowing")
    boolean groupDeepPlowing;

    /** グルーピングYコンセプトショップ */
    @Column(name = "group_y_cp")
    boolean groupYCp;

    /** グルーピング他社コンセプトショップ */
    @Column(name = "group_other_cp")
    boolean groupOtherCp;
	
	
	BigDecimal thisMonthPlan;
	BigDecimal yesterdayPlan;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public boolean isGroupImportant() {
		return groupImportant;
	}

	public void setGroupImportant(boolean groupImportant) {
		this.groupImportant = groupImportant;
	}

	public boolean isGroupNew() {
		return groupNew;
	}

	public void setGroupNew(boolean groupNew) {
		this.groupNew = groupNew;
	}

	public boolean isGroupDeepPlowing() {
		return groupDeepPlowing;
	}

	public void setGroupDeepPlowing(boolean groupDeepPlowing) {
		this.groupDeepPlowing = groupDeepPlowing;
	}

	public boolean isGroupYCp() {
		return groupYCp;
	}

	public void setGroupYCp(boolean groupYCp) {
		this.groupYCp = groupYCp;
	}

	public boolean isGroupOtherCp() {
		return groupOtherCp;
	}

	public void setGroupOtherCp(boolean groupOtherCp) {
		this.groupOtherCp = groupOtherCp;
	}

	public BigDecimal getThisMonthPlan() {
		return thisMonthPlan;
	}

	public void setThisMonthPlan(BigDecimal thisMonthPlan) {
		this.thisMonthPlan = thisMonthPlan;
	}

	public BigDecimal getYesterdayPlan() {
		return yesterdayPlan;
	}

	public void setYesterdayPlan(BigDecimal yesterdayPlan) {
		this.yesterdayPlan = yesterdayPlan;
	}

}
