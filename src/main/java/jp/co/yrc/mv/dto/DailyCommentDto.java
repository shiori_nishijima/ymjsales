package jp.co.yrc.mv.dto;

import java.math.BigDecimal;
import java.util.Date;

import jp.co.yrc.mv.entity.DailyComments;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class DailyCommentDto extends DailyComments {

	String firstName;
	String lastName;
	String eigyoSosikiNm;

	String div1;
	String div2;
	String div3;
	BigDecimal plannedCount;
	BigDecimal callCount;
	BigDecimal infoCount;

	String bossFirstName;
	String bossLastName;
	Date confirmDate;
	Date visitDate;
	
	BigDecimal phoneCount;

	public BigDecimal getPhoneCount() {
		return phoneCount;
	}

	public void setPhoneCount(BigDecimal phoneCount) {
		this.phoneCount = phoneCount;
	}

	boolean readLoginUser;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public String getDiv1() {
		return div1;
	}

	public void setDiv1(String div1) {
		this.div1 = div1;
	}

	public String getDiv2() {
		return div2;
	}

	public void setDiv2(String div2) {
		this.div2 = div2;
	}

	public String getDiv3() {
		return div3;
	}

	public void setDiv3(String div3) {
		this.div3 = div3;
	}

	public BigDecimal getPlannedCount() {
		return plannedCount;
	}

	public void setPlannedCount(BigDecimal plannedCount) {
		this.plannedCount = plannedCount;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(BigDecimal infoCount) {
		this.infoCount = infoCount;
	}

	public String getBossFirstName() {
		return bossFirstName;
	}

	public void setBossFirstName(String bossFirstName) {
		this.bossFirstName = bossFirstName;
	}

	public String getBossLastName() {
		return bossLastName;
	}

	public void setBossLastName(String bossLastName) {
		this.bossLastName = bossLastName;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public Date getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public boolean isReadLoginUser() {
		return readLoginUser;
	}

	public void setReadLoginUser(boolean readLoginUser) {
		this.readLoginUser = readLoginUser;
	}

}
