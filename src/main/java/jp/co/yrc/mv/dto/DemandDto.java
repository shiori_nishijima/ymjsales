package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class DemandDto {

	String groupType;
	BigDecimal demandNumber;

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public BigDecimal getDemandNumber() {
		return demandNumber;
	}

	public void setDemandNumber(BigDecimal demandNumber) {
		this.demandNumber = demandNumber;
	}

}
