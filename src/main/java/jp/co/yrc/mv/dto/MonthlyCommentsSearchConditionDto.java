package jp.co.yrc.mv.dto;

public class MonthlyCommentsSearchConditionDto {
	String year;
	String month;
	String userId;
}
