package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

public class KindMarketTotalDto {

	/** 年 */
	Integer year;

	/** 月 */
	Integer month;

	/** ユーザーID */
	String userId;

	/** 所属組織 */
	String eigyoSosikiCd;

	/** PCRナツ売上 */
	BigDecimal pcrSummerSales;

	/** PCRナツ粗利 */
	BigDecimal pcrSummerMargin;

	/** PCRナツ本数 */
	BigDecimal pcrSummerNumber;

	/** PCR純S売上 */
	BigDecimal pcrPureSnowSales;

	/** PCR純S粗利 */
	BigDecimal pcrPureSnowMargin;

	/** PCR純S本数 */
	BigDecimal pcrPureSnowNumber;

	/** VANナツ売上 */
	BigDecimal vanSummerSales;

	/** VANナツ粗利 */
	BigDecimal vanSummerMargin;

	/** VANナツ本数 */
	BigDecimal vanSummerNumber;

	/** VAN純S売上 */
	BigDecimal vanPureSnowSales;

	/** VAN純S粗利 */
	BigDecimal vanPureSnowMargin;

	/** VAN純S本数 */
	BigDecimal vanPureSnowNumber;

	/** PCBナツ売上 */
	BigDecimal pcbSummerSales;

	/** PCBナツ粗利 */
	BigDecimal pcbSummerMargin;

	/** PCBナツ本数 */
	BigDecimal pcbSummerNumber;

	/** PCB純S売上 */
	BigDecimal pcbPureSnowSales;

	/** PCB純S粗利 */
	BigDecimal pcbPureSnowMargin;

	/** PCB純S本数 */
	BigDecimal pcbPureSnowNumber;

	/** LTナツ売上 */
	BigDecimal ltSummerSales;

	/** LTナツ粗利 */
	BigDecimal ltSummerMargin;

	/** LTナツ本数 */
	BigDecimal ltSummerNumber;

	/** LTスノー売上 */
	BigDecimal ltSnowSales;

	/** LTスノー粗利 */
	BigDecimal ltSnowMargin;

	/** LTスノー本数 */
	BigDecimal ltSnowNumber;

	/** LTAS売上 */
	BigDecimal ltAsSales;

	/** LTAS粗利 */
	BigDecimal ltAsMargin;

	/** LTAS本数 */
	BigDecimal ltAsNumber;

	/** LT純S売上 */
	BigDecimal ltPureSnowSales;

	/** LT純S粗利 */
	BigDecimal ltPureSnowMargin;

	/** LT純S本数 */
	BigDecimal ltPureSnowNumber;

	/** TBナツ売上 */
	BigDecimal tbSummerSales;

	/** TBナツ粗利 */
	BigDecimal tbSummerMargin;

	/** TBナツ本数 */
	BigDecimal tbSummerNumber;

	/** TBスノー売上 */
	BigDecimal tbSnowSales;

	/** TBスノー粗利 */
	BigDecimal tbSnowMargin;

	/** TBスノー本数 */
	BigDecimal tbSnowNumber;

	/** TBAS売上 */
	BigDecimal tbAsSales;

	/** TBAS粗利 */
	BigDecimal tbAsMargin;

	/** TBAS本数 */
	BigDecimal tbAsNumber;

	/** TB純S売上 */
	BigDecimal tbPureSnowSales;

	/** TB純S粗利 */
	BigDecimal tbPureSnowMargin;

	/** TB純S本数 */
	BigDecimal tbPureSnowNumber;

	/** ID売上 */
	BigDecimal idSales;

	/** ID粗利 */
	BigDecimal idMargin;

	/** ID本数 */
	BigDecimal idNumber;

	/** OR売上 */
	BigDecimal orSales;

	/** OR粗利 */
	BigDecimal orMargin;

	/** OR本数 */
	BigDecimal orNumber;

	/** チフ売上 */
	BigDecimal tifuSales;

	/** チフ粗利 */
	BigDecimal tifuMargin;

	/** チフ本数 */
	BigDecimal tifuNumber;

	/** その他売上 */
	BigDecimal otherSales;

	/** その他粗利 */
	BigDecimal otherMargin;

	/** その他本数 */
	BigDecimal otherNumber;

	/** 作業売上 */
	BigDecimal workSales;

	/** 作業粗利 */
	BigDecimal workMargin;

	/** 作業本数 */
	BigDecimal workNumber;

	/** リトレッド売上 */
	BigDecimal retreadSales;

	/** リトレッド粗利 */
	BigDecimal retreadMargin;

	/** リトレッド本数 */
	BigDecimal retreadNumber;

	/** SS売上 */
	BigDecimal ssSales;

	/** SS粗利 */
	BigDecimal ssMargin;

	/** SS本数 */
	BigDecimal ssNumber;

	/** RS売上 */
	BigDecimal rsSales;

	/** RS粗利 */
	BigDecimal rsMargin;

	/** RS本数 */
	BigDecimal rsNumber;

	/** CD売上 */
	BigDecimal cdSales;

	/** CD粗利 */
	BigDecimal cdMargin;

	/** CD本数 */
	BigDecimal cdNumber;

	/** PS売上 */
	BigDecimal psSales;

	/** PS粗利 */
	BigDecimal psMargin;

	/** PS本数 */
	BigDecimal psNumber;

	/** CS売上 */
	BigDecimal csSales;

	/** CS粗利 */
	BigDecimal csMargin;

	/** CS本数 */
	BigDecimal csNumber;

	/** HC売上 */
	BigDecimal hcSales;

	/** HC粗利 */
	BigDecimal hcMargin;

	/** HC本数 */
	BigDecimal hcNumber;

	/** リース売上 */
	BigDecimal leaseSales;

	/** リース粗利 */
	BigDecimal leaseMargin;

	/** リース本数 */
	BigDecimal leaseNumber;

	/** 間他売上 */
	BigDecimal indirectOtherSales;

	/** 間他粗利 */
	BigDecimal indirectOtherMargin;

	/** 間他本数 */
	BigDecimal indirectOtherNumber;

	/** トラック売上 */
	BigDecimal truckSales;

	/** トラック粗利 */
	BigDecimal truckMargin;

	/** トラック本数 */
	BigDecimal truckNumber;

	/** バス売上 */
	BigDecimal busSales;

	/** バス粗利 */
	BigDecimal busMargin;

	/** バス本数 */
	BigDecimal busNumber;

	/** ハイタク売上 */
	BigDecimal hireTaxiSales;

	/** ハイタク粗利 */
	BigDecimal hireTaxiMargin;

	/** ハイタク本数 */
	BigDecimal hireTaxiNumber;

	/** 直他売上 */
	BigDecimal directOtherSales;

	/** 直他粗利 */
	BigDecimal directOtherMargin;

	/** 直他本数 */
	BigDecimal directOtherNumber;

	/** 小売売上 */
	BigDecimal retailSales;

	/** 小売粗利 */
	BigDecimal retailMargin;

	/** 小売本数 */
	BigDecimal retailNumber;

	/** PCRナツ預り */
	BigDecimal pcrSummerStock;

	/** PCR純S預り */
	BigDecimal pcrPureSnowStock;

	/** VANナツ預り */
	BigDecimal vanSummerStock;

	/** VAN純S預り */
	BigDecimal vanPureSnowStock;

	/** PCBナツ預り */
	BigDecimal pcbSummerStock;

	/** PCB純S預り */
	BigDecimal pcbPureSnowStock;

	/** LTナツ預り */
	BigDecimal ltSummerStock;

	/** LTスノー預り */
	BigDecimal ltSnowStock;

	/** LTAS預り */
	BigDecimal ltAsStock;

	/** LT純S預り */
	BigDecimal ltPureSnowStock;

	/** TBナツ預り */
	BigDecimal tbSummerStock;

	/** TBスノー預り */
	BigDecimal tbSnowStock;

	/** TBAS預り */
	BigDecimal tbAsStock;

	/** TB純S預り */
	BigDecimal tbPureSnowStock;

	/** ID預り */
	BigDecimal idStock;

	/** OR預り */
	BigDecimal orStock;

	/** チフ預り */
	BigDecimal tifuStock;

	/** その他預り */
	BigDecimal otherStock;

	/** 作業預り */
	BigDecimal workStock;

	/** リトレッド預り */
	BigDecimal retreadStock;

	/** SS預り */
	BigDecimal ssStock;

	/** RS預り */
	BigDecimal rsStock;

	/** CD預り */
	BigDecimal cdStock;

	/** PS預り */
	BigDecimal psStock;

	/** CS預り */
	BigDecimal csStock;

	/** HC預り */
	BigDecimal hcStock;

	/** リース預り */
	BigDecimal leaseStock;

	/** 間他預り */
	BigDecimal indirectOtherStock;

	/** トラック預り */
	BigDecimal truckStock;

	/** バス預り */
	BigDecimal busStock;

	/** ハイタク預り */
	BigDecimal hireTaxiStock;

	/** 直他預り */
	BigDecimal directOtherStock;

	/** 小売預り */
	BigDecimal retailStock;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEigyoSosikiCd() {
		return eigyoSosikiCd;
	}

	public void setEigyoSosikiCd(String eigyoSosikiCd) {
		this.eigyoSosikiCd = eigyoSosikiCd;
	}

	public BigDecimal getPcrSummerSales() {
		return pcrSummerSales;
	}

	public void setPcrSummerSales(BigDecimal pcrSummerSales) {
		this.pcrSummerSales = pcrSummerSales;
	}

	public BigDecimal getPcrSummerMargin() {
		return pcrSummerMargin;
	}

	public void setPcrSummerMargin(BigDecimal pcrSummerMargin) {
		this.pcrSummerMargin = pcrSummerMargin;
	}

	public BigDecimal getPcrSummerNumber() {
		return pcrSummerNumber;
	}

	public void setPcrSummerNumber(BigDecimal pcrSummerNumber) {
		this.pcrSummerNumber = pcrSummerNumber;
	}

	public BigDecimal getPcrPureSnowSales() {
		return pcrPureSnowSales;
	}

	public void setPcrPureSnowSales(BigDecimal pcrPureSnowSales) {
		this.pcrPureSnowSales = pcrPureSnowSales;
	}

	public BigDecimal getPcrPureSnowMargin() {
		return pcrPureSnowMargin;
	}

	public void setPcrPureSnowMargin(BigDecimal pcrPureSnowMargin) {
		this.pcrPureSnowMargin = pcrPureSnowMargin;
	}

	public BigDecimal getPcrPureSnowNumber() {
		return pcrPureSnowNumber;
	}

	public void setPcrPureSnowNumber(BigDecimal pcrPureSnowNumber) {
		this.pcrPureSnowNumber = pcrPureSnowNumber;
	}

	public BigDecimal getVanSummerSales() {
		return vanSummerSales;
	}

	public void setVanSummerSales(BigDecimal vanSummerSales) {
		this.vanSummerSales = vanSummerSales;
	}

	public BigDecimal getVanSummerMargin() {
		return vanSummerMargin;
	}

	public void setVanSummerMargin(BigDecimal vanSummerMargin) {
		this.vanSummerMargin = vanSummerMargin;
	}

	public BigDecimal getVanSummerNumber() {
		return vanSummerNumber;
	}

	public void setVanSummerNumber(BigDecimal vanSummerNumber) {
		this.vanSummerNumber = vanSummerNumber;
	}

	public BigDecimal getVanPureSnowSales() {
		return vanPureSnowSales;
	}

	public void setVanPureSnowSales(BigDecimal vanPureSnowSales) {
		this.vanPureSnowSales = vanPureSnowSales;
	}

	public BigDecimal getVanPureSnowMargin() {
		return vanPureSnowMargin;
	}

	public void setVanPureSnowMargin(BigDecimal vanPureSnowMargin) {
		this.vanPureSnowMargin = vanPureSnowMargin;
	}

	public BigDecimal getVanPureSnowNumber() {
		return vanPureSnowNumber;
	}

	public void setVanPureSnowNumber(BigDecimal vanPureSnowNumber) {
		this.vanPureSnowNumber = vanPureSnowNumber;
	}

	public BigDecimal getLtSummerSales() {
		return ltSummerSales;
	}

	public void setLtSummerSales(BigDecimal ltSummerSales) {
		this.ltSummerSales = ltSummerSales;
	}

	public BigDecimal getLtSummerMargin() {
		return ltSummerMargin;
	}

	public void setLtSummerMargin(BigDecimal ltSummerMargin) {
		this.ltSummerMargin = ltSummerMargin;
	}

	public BigDecimal getLtSummerNumber() {
		return ltSummerNumber;
	}

	public void setLtSummerNumber(BigDecimal ltSummerNumber) {
		this.ltSummerNumber = ltSummerNumber;
	}

	public BigDecimal getLtSnowSales() {
		return ltSnowSales;
	}

	public void setLtSnowSales(BigDecimal ltSnowSales) {
		this.ltSnowSales = ltSnowSales;
	}

	public BigDecimal getLtSnowMargin() {
		return ltSnowMargin;
	}

	public void setLtSnowMargin(BigDecimal ltSnowMargin) {
		this.ltSnowMargin = ltSnowMargin;
	}

	public BigDecimal getLtSnowNumber() {
		return ltSnowNumber;
	}

	public void setLtSnowNumber(BigDecimal ltSnowNumber) {
		this.ltSnowNumber = ltSnowNumber;
	}

	public BigDecimal getLtAsSales() {
		return ltAsSales;
	}

	public void setLtAsSales(BigDecimal ltAsSales) {
		this.ltAsSales = ltAsSales;
	}

	public BigDecimal getLtAsMargin() {
		return ltAsMargin;
	}

	public void setLtAsMargin(BigDecimal ltAsMargin) {
		this.ltAsMargin = ltAsMargin;
	}

	public BigDecimal getLtAsNumber() {
		return ltAsNumber;
	}

	public void setLtAsNumber(BigDecimal ltAsNumber) {
		this.ltAsNumber = ltAsNumber;
	}

	public BigDecimal getLtPureSnowSales() {
		return ltPureSnowSales;
	}

	public void setLtPureSnowSales(BigDecimal ltPureSnowSales) {
		this.ltPureSnowSales = ltPureSnowSales;
	}

	public BigDecimal getLtPureSnowMargin() {
		return ltPureSnowMargin;
	}

	public void setLtPureSnowMargin(BigDecimal ltPureSnowMargin) {
		this.ltPureSnowMargin = ltPureSnowMargin;
	}

	public BigDecimal getLtPureSnowNumber() {
		return ltPureSnowNumber;
	}

	public void setLtPureSnowNumber(BigDecimal ltPureSnowNumber) {
		this.ltPureSnowNumber = ltPureSnowNumber;
	}

	public BigDecimal getTbSummerSales() {
		return tbSummerSales;
	}

	public void setTbSummerSales(BigDecimal tbSummerSales) {
		this.tbSummerSales = tbSummerSales;
	}

	public BigDecimal getTbSummerMargin() {
		return tbSummerMargin;
	}

	public void setTbSummerMargin(BigDecimal tbSummerMargin) {
		this.tbSummerMargin = tbSummerMargin;
	}

	public BigDecimal getTbSummerNumber() {
		return tbSummerNumber;
	}

	public void setTbSummerNumber(BigDecimal tbSummerNumber) {
		this.tbSummerNumber = tbSummerNumber;
	}

	public BigDecimal getTbSnowSales() {
		return tbSnowSales;
	}

	public void setTbSnowSales(BigDecimal tbSnowSales) {
		this.tbSnowSales = tbSnowSales;
	}

	public BigDecimal getTbSnowMargin() {
		return tbSnowMargin;
	}

	public void setTbSnowMargin(BigDecimal tbSnowMargin) {
		this.tbSnowMargin = tbSnowMargin;
	}

	public BigDecimal getTbSnowNumber() {
		return tbSnowNumber;
	}

	public void setTbSnowNumber(BigDecimal tbSnowNumber) {
		this.tbSnowNumber = tbSnowNumber;
	}

	public BigDecimal getTbAsSales() {
		return tbAsSales;
	}

	public void setTbAsSales(BigDecimal tbAsSales) {
		this.tbAsSales = tbAsSales;
	}

	public BigDecimal getTbAsMargin() {
		return tbAsMargin;
	}

	public void setTbAsMargin(BigDecimal tbAsMargin) {
		this.tbAsMargin = tbAsMargin;
	}

	public BigDecimal getTbAsNumber() {
		return tbAsNumber;
	}

	public void setTbAsNumber(BigDecimal tbAsNumber) {
		this.tbAsNumber = tbAsNumber;
	}

	public BigDecimal getTbPureSnowSales() {
		return tbPureSnowSales;
	}

	public void setTbPureSnowSales(BigDecimal tbPureSnowSales) {
		this.tbPureSnowSales = tbPureSnowSales;
	}

	public BigDecimal getTbPureSnowMargin() {
		return tbPureSnowMargin;
	}

	public void setTbPureSnowMargin(BigDecimal tbPureSnowMargin) {
		this.tbPureSnowMargin = tbPureSnowMargin;
	}

	public BigDecimal getTbPureSnowNumber() {
		return tbPureSnowNumber;
	}

	public void setTbPureSnowNumber(BigDecimal tbPureSnowNumber) {
		this.tbPureSnowNumber = tbPureSnowNumber;
	}

	public BigDecimal getIdSales() {
		return idSales;
	}

	public void setIdSales(BigDecimal idSales) {
		this.idSales = idSales;
	}

	public BigDecimal getIdMargin() {
		return idMargin;
	}

	public void setIdMargin(BigDecimal idMargin) {
		this.idMargin = idMargin;
	}

	public BigDecimal getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(BigDecimal idNumber) {
		this.idNumber = idNumber;
	}

	public BigDecimal getOrSales() {
		return orSales;
	}

	public void setOrSales(BigDecimal orSales) {
		this.orSales = orSales;
	}

	public BigDecimal getOrMargin() {
		return orMargin;
	}

	public void setOrMargin(BigDecimal orMargin) {
		this.orMargin = orMargin;
	}

	public BigDecimal getOrNumber() {
		return orNumber;
	}

	public void setOrNumber(BigDecimal orNumber) {
		this.orNumber = orNumber;
	}

	public BigDecimal getTifuSales() {
		return tifuSales;
	}

	public void setTifuSales(BigDecimal tifuSales) {
		this.tifuSales = tifuSales;
	}

	public BigDecimal getTifuMargin() {
		return tifuMargin;
	}

	public void setTifuMargin(BigDecimal tifuMargin) {
		this.tifuMargin = tifuMargin;
	}

	public BigDecimal getTifuNumber() {
		return tifuNumber;
	}

	public void setTifuNumber(BigDecimal tifuNumber) {
		this.tifuNumber = tifuNumber;
	}

	public BigDecimal getOtherSales() {
		return otherSales;
	}

	public void setOtherSales(BigDecimal otherSales) {
		this.otherSales = otherSales;
	}

	public BigDecimal getOtherMargin() {
		return otherMargin;
	}

	public void setOtherMargin(BigDecimal otherMargin) {
		this.otherMargin = otherMargin;
	}

	public BigDecimal getOtherNumber() {
		return otherNumber;
	}

	public void setOtherNumber(BigDecimal otherNumber) {
		this.otherNumber = otherNumber;
	}

	public BigDecimal getWorkSales() {
		return workSales;
	}

	public void setWorkSales(BigDecimal workSales) {
		this.workSales = workSales;
	}

	public BigDecimal getWorkMargin() {
		return workMargin;
	}

	public void setWorkMargin(BigDecimal workMargin) {
		this.workMargin = workMargin;
	}

	public BigDecimal getWorkNumber() {
		return workNumber;
	}

	public void setWorkNumber(BigDecimal workNumber) {
		this.workNumber = workNumber;
	}

	public BigDecimal getSsSales() {
		return ssSales;
	}

	public void setSsSales(BigDecimal ssSales) {
		this.ssSales = ssSales;
	}

	public BigDecimal getSsMargin() {
		return ssMargin;
	}

	public void setSsMargin(BigDecimal ssMargin) {
		this.ssMargin = ssMargin;
	}

	public BigDecimal getSsNumber() {
		return ssNumber;
	}

	public void setSsNumber(BigDecimal ssNumber) {
		this.ssNumber = ssNumber;
	}

	public BigDecimal getRsSales() {
		return rsSales;
	}

	public void setRsSales(BigDecimal rsSales) {
		this.rsSales = rsSales;
	}

	public BigDecimal getRsMargin() {
		return rsMargin;
	}

	public void setRsMargin(BigDecimal rsMargin) {
		this.rsMargin = rsMargin;
	}

	public BigDecimal getRsNumber() {
		return rsNumber;
	}

	public void setRsNumber(BigDecimal rsNumber) {
		this.rsNumber = rsNumber;
	}

	public BigDecimal getCdSales() {
		return cdSales;
	}

	public void setCdSales(BigDecimal cdSales) {
		this.cdSales = cdSales;
	}

	public BigDecimal getCdMargin() {
		return cdMargin;
	}

	public void setCdMargin(BigDecimal cdMargin) {
		this.cdMargin = cdMargin;
	}

	public BigDecimal getCdNumber() {
		return cdNumber;
	}

	public void setCdNumber(BigDecimal cdNumber) {
		this.cdNumber = cdNumber;
	}

	public BigDecimal getPsSales() {
		return psSales;
	}

	public void setPsSales(BigDecimal psSales) {
		this.psSales = psSales;
	}

	public BigDecimal getPsMargin() {
		return psMargin;
	}

	public void setPsMargin(BigDecimal psMargin) {
		this.psMargin = psMargin;
	}

	public BigDecimal getPsNumber() {
		return psNumber;
	}

	public void setPsNumber(BigDecimal psNumber) {
		this.psNumber = psNumber;
	}

	public BigDecimal getCsSales() {
		return csSales;
	}

	public void setCsSales(BigDecimal csSales) {
		this.csSales = csSales;
	}

	public BigDecimal getCsMargin() {
		return csMargin;
	}

	public void setCsMargin(BigDecimal csMargin) {
		this.csMargin = csMargin;
	}

	public BigDecimal getCsNumber() {
		return csNumber;
	}

	public void setCsNumber(BigDecimal csNumber) {
		this.csNumber = csNumber;
	}

	public BigDecimal getHcSales() {
		return hcSales;
	}

	public void setHcSales(BigDecimal hcSales) {
		this.hcSales = hcSales;
	}

	public BigDecimal getHcMargin() {
		return hcMargin;
	}

	public void setHcMargin(BigDecimal hcMargin) {
		this.hcMargin = hcMargin;
	}

	public BigDecimal getHcNumber() {
		return hcNumber;
	}

	public void setHcNumber(BigDecimal hcNumber) {
		this.hcNumber = hcNumber;
	}

	public BigDecimal getLeaseSales() {
		return leaseSales;
	}

	public void setLeaseSales(BigDecimal leaseSales) {
		this.leaseSales = leaseSales;
	}

	public BigDecimal getLeaseMargin() {
		return leaseMargin;
	}

	public void setLeaseMargin(BigDecimal leaseMargin) {
		this.leaseMargin = leaseMargin;
	}

	public BigDecimal getLeaseNumber() {
		return leaseNumber;
	}

	public void setLeaseNumber(BigDecimal leaseNumber) {
		this.leaseNumber = leaseNumber;
	}

	public BigDecimal getIndirectOtherSales() {
		return indirectOtherSales;
	}

	public void setIndirectOtherSales(BigDecimal indirectOtherSales) {
		this.indirectOtherSales = indirectOtherSales;
	}

	public BigDecimal getIndirectOtherMargin() {
		return indirectOtherMargin;
	}

	public void setIndirectOtherMargin(BigDecimal indirectOtherMargin) {
		this.indirectOtherMargin = indirectOtherMargin;
	}

	public BigDecimal getIndirectOtherNumber() {
		return indirectOtherNumber;
	}

	public void setIndirectOtherNumber(BigDecimal indirectOtherNumber) {
		this.indirectOtherNumber = indirectOtherNumber;
	}

	public BigDecimal getTruckSales() {
		return truckSales;
	}

	public void setTruckSales(BigDecimal truckSales) {
		this.truckSales = truckSales;
	}

	public BigDecimal getTruckMargin() {
		return truckMargin;
	}

	public void setTruckMargin(BigDecimal truckMargin) {
		this.truckMargin = truckMargin;
	}

	public BigDecimal getTruckNumber() {
		return truckNumber;
	}

	public void setTruckNumber(BigDecimal truckNumber) {
		this.truckNumber = truckNumber;
	}

	public BigDecimal getBusSales() {
		return busSales;
	}

	public void setBusSales(BigDecimal busSales) {
		this.busSales = busSales;
	}

	public BigDecimal getBusMargin() {
		return busMargin;
	}

	public void setBusMargin(BigDecimal busMargin) {
		this.busMargin = busMargin;
	}

	public BigDecimal getBusNumber() {
		return busNumber;
	}

	public void setBusNumber(BigDecimal busNumber) {
		this.busNumber = busNumber;
	}

	public BigDecimal getHireTaxiSales() {
		return hireTaxiSales;
	}

	public void setHireTaxiSales(BigDecimal hireTaxiSales) {
		this.hireTaxiSales = hireTaxiSales;
	}

	public BigDecimal getHireTaxiMargin() {
		return hireTaxiMargin;
	}

	public void setHireTaxiMargin(BigDecimal hireTaxiMargin) {
		this.hireTaxiMargin = hireTaxiMargin;
	}

	public BigDecimal getHireTaxiNumber() {
		return hireTaxiNumber;
	}

	public void setHireTaxiNumber(BigDecimal hireTaxiNumber) {
		this.hireTaxiNumber = hireTaxiNumber;
	}

	public BigDecimal getDirectOtherSales() {
		return directOtherSales;
	}

	public void setDirectOtherSales(BigDecimal directOtherSales) {
		this.directOtherSales = directOtherSales;
	}

	public BigDecimal getDirectOtherMargin() {
		return directOtherMargin;
	}

	public void setDirectOtherMargin(BigDecimal directOtherMargin) {
		this.directOtherMargin = directOtherMargin;
	}

	public BigDecimal getDirectOtherNumber() {
		return directOtherNumber;
	}

	public void setDirectOtherNumber(BigDecimal directOtherNumber) {
		this.directOtherNumber = directOtherNumber;
	}

	public BigDecimal getRetailSales() {
		return retailSales;
	}

	public void setRetailSales(BigDecimal retailSales) {
		this.retailSales = retailSales;
	}

	public BigDecimal getRetailMargin() {
		return retailMargin;
	}

	public void setRetailMargin(BigDecimal retailMargin) {
		this.retailMargin = retailMargin;
	}

	public BigDecimal getRetailNumber() {
		return retailNumber;
	}

	public void setRetailNumber(BigDecimal retailNumber) {
		this.retailNumber = retailNumber;
	}

	public BigDecimal getPcrSummerStock() {
		return pcrSummerStock;
	}

	public void setPcrSummerStock(BigDecimal pcrSummerStock) {
		this.pcrSummerStock = pcrSummerStock;
	}

	public BigDecimal getPcrPureSnowStock() {
		return pcrPureSnowStock;
	}

	public void setPcrPureSnowStock(BigDecimal pcrPureSnowStock) {
		this.pcrPureSnowStock = pcrPureSnowStock;
	}

	public BigDecimal getVanSummerStock() {
		return vanSummerStock;
	}

	public void setVanSummerStock(BigDecimal vanSummerStock) {
		this.vanSummerStock = vanSummerStock;
	}

	public BigDecimal getVanPureSnowStock() {
		return vanPureSnowStock;
	}

	public void setVanPureSnowStock(BigDecimal vanPureSnowStock) {
		this.vanPureSnowStock = vanPureSnowStock;
	}

	public BigDecimal getLtSummerStock() {
		return ltSummerStock;
	}

	public void setLtSummerStock(BigDecimal ltSummerStock) {
		this.ltSummerStock = ltSummerStock;
	}

	public BigDecimal getLtSnowStock() {
		return ltSnowStock;
	}

	public void setLtSnowStock(BigDecimal ltSnowStock) {
		this.ltSnowStock = ltSnowStock;
	}

	public BigDecimal getLtAsStock() {
		return ltAsStock;
	}

	public void setLtAsStock(BigDecimal ltAsStock) {
		this.ltAsStock = ltAsStock;
	}

	public BigDecimal getLtPureSnowStock() {
		return ltPureSnowStock;
	}

	public void setLtPureSnowStock(BigDecimal ltPureSnowStock) {
		this.ltPureSnowStock = ltPureSnowStock;
	}

	public BigDecimal getTbSummerStock() {
		return tbSummerStock;
	}

	public void setTbSummerStock(BigDecimal tbSummerStock) {
		this.tbSummerStock = tbSummerStock;
	}

	public BigDecimal getTbSnowStock() {
		return tbSnowStock;
	}

	public void setTbSnowStock(BigDecimal tbSnowStock) {
		this.tbSnowStock = tbSnowStock;
	}

	public BigDecimal getTbAsStock() {
		return tbAsStock;
	}

	public void setTbAsStock(BigDecimal tbAsStock) {
		this.tbAsStock = tbAsStock;
	}

	public BigDecimal getTbPureSnowStock() {
		return tbPureSnowStock;
	}

	public void setTbPureSnowStock(BigDecimal tbPureSnowStock) {
		this.tbPureSnowStock = tbPureSnowStock;
	}

	public BigDecimal getIdStock() {
		return idStock;
	}

	public void setIdStock(BigDecimal idStock) {
		this.idStock = idStock;
	}

	public BigDecimal getOrStock() {
		return orStock;
	}

	public void setOrStock(BigDecimal orStock) {
		this.orStock = orStock;
	}

	public BigDecimal getTifuStock() {
		return tifuStock;
	}

	public void setTifuStock(BigDecimal tifuStock) {
		this.tifuStock = tifuStock;
	}

	public BigDecimal getOtherStock() {
		return otherStock;
	}

	public void setOtherStock(BigDecimal otherStock) {
		this.otherStock = otherStock;
	}

	public BigDecimal getWorkStock() {
		return workStock;
	}

	public void setWorkStock(BigDecimal workStock) {
		this.workStock = workStock;
	}

	public BigDecimal getSsStock() {
		return ssStock;
	}

	public void setSsStock(BigDecimal ssStock) {
		this.ssStock = ssStock;
	}

	public BigDecimal getRsStock() {
		return rsStock;
	}

	public void setRsStock(BigDecimal rsStock) {
		this.rsStock = rsStock;
	}

	public BigDecimal getCdStock() {
		return cdStock;
	}

	public void setCdStock(BigDecimal cdStock) {
		this.cdStock = cdStock;
	}

	public BigDecimal getPsStock() {
		return psStock;
	}

	public void setPsStock(BigDecimal psStock) {
		this.psStock = psStock;
	}

	public BigDecimal getCsStock() {
		return csStock;
	}

	public void setCsStock(BigDecimal csStock) {
		this.csStock = csStock;
	}

	public BigDecimal getHcStock() {
		return hcStock;
	}

	public void setHcStock(BigDecimal hcStock) {
		this.hcStock = hcStock;
	}

	public BigDecimal getLeaseStock() {
		return leaseStock;
	}

	public void setLeaseStock(BigDecimal leaseStock) {
		this.leaseStock = leaseStock;
	}

	public BigDecimal getIndirectOtherStock() {
		return indirectOtherStock;
	}

	public void setIndirectOtherStock(BigDecimal indirectOtherStock) {
		this.indirectOtherStock = indirectOtherStock;
	}

	public BigDecimal getTruckStock() {
		return truckStock;
	}

	public void setTruckStock(BigDecimal truckStock) {
		this.truckStock = truckStock;
	}

	public BigDecimal getBusStock() {
		return busStock;
	}

	public void setBusStock(BigDecimal busStock) {
		this.busStock = busStock;
	}

	public BigDecimal getHireTaxiStock() {
		return hireTaxiStock;
	}

	public void setHireTaxiStock(BigDecimal hireTaxiStock) {
		this.hireTaxiStock = hireTaxiStock;
	}

	public BigDecimal getDirectOtherStock() {
		return directOtherStock;
	}

	public void setDirectOtherStock(BigDecimal directOtherStock) {
		this.directOtherStock = directOtherStock;
	}

	public BigDecimal getRetailStock() {
		return retailStock;
	}

	public void setRetailStock(BigDecimal retailStock) {
		this.retailStock = retailStock;
	}

	public BigDecimal getPcbSummerSales() {
		return pcbSummerSales;
	}

	public void setPcbSummerSales(BigDecimal pcbSummerSales) {
		this.pcbSummerSales = pcbSummerSales;
	}

	public BigDecimal getPcbSummerMargin() {
		return pcbSummerMargin;
	}

	public void setPcbSummerMargin(BigDecimal pcbSummerMargin) {
		this.pcbSummerMargin = pcbSummerMargin;
	}

	public BigDecimal getPcbSummerNumber() {
		return pcbSummerNumber;
	}

	public void setPcbSummerNumber(BigDecimal pcbSummerNumber) {
		this.pcbSummerNumber = pcbSummerNumber;
	}

	public BigDecimal getPcbPureSnowSales() {
		return pcbPureSnowSales;
	}

	public void setPcbPureSnowSales(BigDecimal pcbPureSnowSales) {
		this.pcbPureSnowSales = pcbPureSnowSales;
	}

	public BigDecimal getPcbPureSnowMargin() {
		return pcbPureSnowMargin;
	}

	public void setPcbPureSnowMargin(BigDecimal pcbPureSnowMargin) {
		this.pcbPureSnowMargin = pcbPureSnowMargin;
	}

	public BigDecimal getPcbPureSnowNumber() {
		return pcbPureSnowNumber;
	}

	public void setPcbPureSnowNumber(BigDecimal pcbPureSnowNumber) {
		this.pcbPureSnowNumber = pcbPureSnowNumber;
	}

	public BigDecimal getPcbSummerStock() {
		return pcbSummerStock;
	}

	public void setPcbSummerStock(BigDecimal pcbSummerStock) {
		this.pcbSummerStock = pcbSummerStock;
	}

	public BigDecimal getPcbPureSnowStock() {
		return pcbPureSnowStock;
	}

	public void setPcbPureSnowStock(BigDecimal pcbPureSnowStock) {
		this.pcbPureSnowStock = pcbPureSnowStock;
	}

	public BigDecimal getRetreadSales() {
		return retreadSales;
	}

	public void setRetreadSales(BigDecimal retreadSales) {
		this.retreadSales = retreadSales;
	}

	public BigDecimal getRetreadMargin() {
		return retreadMargin;
	}

	public void setRetreadMargin(BigDecimal retreadMargin) {
		this.retreadMargin = retreadMargin;
	}

	public BigDecimal getRetreadNumber() {
		return retreadNumber;
	}

	public void setRetreadNumber(BigDecimal retreadNumber) {
		this.retreadNumber = retreadNumber;
	}

	public BigDecimal getRetreadStock() {
		return retreadStock;
	}

	public void setRetreadStock(BigDecimal retreadStock) {
		this.retreadStock = retreadStock;
	}

}