package jp.co.yrc.mv.dto;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

import jp.co.yrc.mv.entity.Comments;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CommentsDto extends Comments {

	String firstName;
	String lastName;
	String idForCustomer;
	String eigyoSosikiNm;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdForCustomer() {
		return idForCustomer;
	}

	public void setIdForCustomer(String idForCustomer) {
		this.idForCustomer = idForCustomer;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

}
