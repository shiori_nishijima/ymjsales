package jp.co.yrc.mv.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CallCountInfoDto {

	private String groupType;
	private Date dateStart;
	private BigDecimal callCount;
	private BigDecimal infoCount;
	private BigDecimal callUserCount;
	private BigDecimal plannedCount;
	private String salesCompanyCode;
	private String departmentCode;
	private String salesOfficeCode;
	private boolean dailyCommentCompleted;

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(BigDecimal infoCount) {
		this.infoCount = infoCount;
	}

	public BigDecimal getCallUserCount() {
		return callUserCount;
	}

	public void setCallUserCount(BigDecimal callUserCount) {
		this.callUserCount = callUserCount;
	}

	public BigDecimal getPlannedCount() {
		return plannedCount;
	}

	public void setPlannedCount(BigDecimal plannedCount) {
		this.plannedCount = plannedCount;
	}

	public String getSalesCompanyCode() {
		return salesCompanyCode;
	}

	public void setSalesCompanyCode(String salesCompanyCode) {
		this.salesCompanyCode = salesCompanyCode;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getSalesOfficeCode() {
		return salesOfficeCode;
	}

	public void setSalesOfficeCode(String salesOfficeCode) {
		this.salesOfficeCode = salesOfficeCode;
	}

	public boolean isDailyCommentCompleted() {
		return dailyCommentCompleted;
	}

	public void setDailyCommentCompleted(boolean dailyCommentCompleted) {
		this.dailyCommentCompleted = dailyCommentCompleted;
	}

}
