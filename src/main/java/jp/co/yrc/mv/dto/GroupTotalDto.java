package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

public class GroupTotalDto {

	/** 年 */
	Integer year;

	/** 月 */
	Integer month;

	/** 日 */
	Integer date;

	/** Aランク訪問予定数 */
	BigDecimal rankAPlanCount;

	/** Aランク訪問数 */
	BigDecimal rankACallCount;

	/** Aランク情報数 */
	BigDecimal rankAInfoCount;

	/** Bランク訪問予定数 */
	BigDecimal rankBPlanCount;

	/** Bランク訪問数 */
	BigDecimal rankBCallCount;

	/** Bランク情報数 */
	BigDecimal rankBInfoCount;

	/** Cランク訪問予定数 */
	BigDecimal rankCPlanCount;

	/** Cランク訪問数 */
	BigDecimal rankCCallCount;

	/** Cランク情報数 */
	BigDecimal rankCInfoCount;

	/** 重点訪問予定数 */
	BigDecimal importantPlanCount;

	/** 重点訪問数 */
	BigDecimal importantCallCount;

	/** 重点情報数 */
	BigDecimal importantInfoCount;

	/** 新規訪問予定数 */
	BigDecimal newPlanCount;

	/** 新規訪問数 */
	BigDecimal newCallCount;

	/** 新規情報数 */
	BigDecimal newInfoCount;

	/** 深耕訪問予定数 */
	BigDecimal deepPlowingPlanCount;

	/** 深耕訪問件数 */
	BigDecimal deepPlowingCallCount;

	/** 深耕情報数 */
	BigDecimal deepPlowingInfoCount;

	/** Y-CP訪問予定数 */
	BigDecimal yCpPlanCount;

	/** Y-CP訪問件数 */
	BigDecimal yCpCallCount;

	/** Y-CP情報件数 */
	BigDecimal yCpInfoCount;

	/** 他社CP訪問予定数 */
	BigDecimal otherCpPlanCount;

	/** 他社CP訪問数 */
	BigDecimal otherCpCallCount;

	/** 他社CP情報数 */
	BigDecimal otherCpInfoCount;

	/** 担当外訪問予定数 */
	BigDecimal outsidePlanCount;

	/** 担当外訪問数 */
	BigDecimal outsideCallCount;

	/** 担当外情報数 */
	BigDecimal outsideInfoCount;

	/** グループ1訪問予定数 */
	BigDecimal groupOnePlanCount;

	/** グループ1訪問数 */
	BigDecimal groupOneCallCount;

	/** グループ1情報数 */
	BigDecimal groupOneInfoCount;

	/** グループ2訪問予定数 */
	BigDecimal groupTwoPlanCount;

	/** グループ2訪問数 */
	BigDecimal groupTwoCallCount;

	/** グループ2情報数 */
	BigDecimal groupTwoInfoCount;

	/** グループ3訪問予定数 */
	BigDecimal groupThreePlanCount;

	/** グループ3訪問数 */
	BigDecimal groupThreeCallCount;

	/** グループ3情報数 */
	BigDecimal groupThreeInfoCount;

	/** グループ4訪問予定数 */
	BigDecimal groupFourPlanCount;

	/** グループ4訪問数 */
	BigDecimal groupFourCallCount;

	/** グループ4情報数 */
	BigDecimal groupFourInfoCount;

	/** グループ5訪問予定数 */
	BigDecimal groupFivePlanCount;

	/** グループ5訪問数 */
	BigDecimal groupFiveCallCount;

	/** グループ5情報数 */
	BigDecimal groupFiveInfoCount;

	/** Aランク持軒数 */
	BigDecimal rankAOwnCount;

	/** Aランク取引軒数 */
	BigDecimal rankADealCount;
	BigDecimal rankADealPlanCount;

	/** Aランク取引得M数 */
	BigDecimal rankAAccountMasterCount;

	/** Bランク持軒数 */
	BigDecimal rankBOwnCount;

	/** Bランク取引軒数 */
	BigDecimal rankBDealCount;
	BigDecimal rankBDealPlanCount;

	/** Bランク取引得M数 */
	BigDecimal rankBAccountMasterCount;

	/** Cランク持軒数 */
	BigDecimal rankCOwnCount;

	/** Cランク取引軒数 */
	BigDecimal rankCDealCount;
	BigDecimal rankCDealPlanCount;

	/** Cランク取引得M数 */
	BigDecimal rankCAccountMasterCount;

	/** 重点持軒数 */
	BigDecimal importantOwnCount;

	/** 重点取引軒数 */
	BigDecimal importantDealCount;
	BigDecimal importantDealPlanCount;

	/** 重点取引得M数 */
	BigDecimal importantAccountMasterCount;

	/** 新規持軒数 */
	BigDecimal newOwnCount;

	/** 新規取引軒数 */
	BigDecimal newDealCount;
	BigDecimal newDealPlanCount;

	/** 新規取引得M数 */
	BigDecimal newAccountMasterCount;

	/** 深耕持軒数 */
	BigDecimal deepPlowingOwnCount;

	/** 深耕取引軒数 */
	BigDecimal deepPlowingDealCount;
	BigDecimal deepPlowingDealPlanCount;

	/** 深耕取引得M数 */
	BigDecimal deepPlowingAccountMasterCount;

	/** Y-CP持軒数 */
	BigDecimal yCpOwnCount;

	/** Y-CP取引軒数 */
	BigDecimal yCpDealCount;
	BigDecimal yCpDealPlanCount;

	/** Y-CP取引得M数 */
	BigDecimal yCpAccountMasterCount;

	/** 他社CP持軒数 */
	BigDecimal otherCpOwnCount;

	/** 他社CP取引軒数 */
	BigDecimal otherCpDealCount;
	BigDecimal otherCpDealPlanCount;

	/** 他社CP取引得M数 */
	BigDecimal otherCpAccountMasterCount;

	/** グループ1持軒数 */
	BigDecimal groupOneOwnCount;

	/** グループ1取引数 */
	BigDecimal groupOneDealCount;
	BigDecimal groupOneDealPlanCount;

	/** グループ1取引得M数 */
	BigDecimal groupOneAccountMasterCount;

	/** グループ2持軒数 */
	BigDecimal groupTwoOwnCount;

	/** グループ2取引軒数 */
	BigDecimal groupTwoDealCount;
	BigDecimal groupTwoDealPlanCount;

	/** グループ2取引得M数 */
	BigDecimal groupTwoAccountMasterCount;

	/** グループ3持軒数 */
	BigDecimal groupThreeOwnCount;

	/** グループ3取引軒数 */
	BigDecimal groupThreeDealCount;
	BigDecimal groupThreeDealPlanCount;

	/** グループ3取引得M数 */
	BigDecimal groupThreeAccountMasterCount;

	/** グループ4持軒数 */
	BigDecimal groupFourOwnCount;

	/** グループ4取引軒数 */
	BigDecimal groupFourDealCount;
	BigDecimal groupFourDealPlanCount;

	/** グループ4取引得M数 */
	BigDecimal groupFourAccountMasterCount;

	/** グループ5持軒数 */
	BigDecimal groupFiveOwnCount;

	/** グループ5取引数 */
	BigDecimal groupFiveDealCount;
	BigDecimal groupFiveDealPlanCount;

	/** グループ5取引得M数 */
	BigDecimal groupFiveAccountMasterCount;

	/** Aランク売上 */
	BigDecimal rankASales;

	/** Aランク粗利 */
	BigDecimal rankAMargin;

	/** Aランク本数 */
	BigDecimal rankANumber;

	/** Bランク売上 */
	BigDecimal rankBSales;

	/** Bランク粗利 */
	BigDecimal rankBMargin;

	/** Bランク本数 */
	BigDecimal rankBNumber;

	/** Cランク売上 */
	BigDecimal rankCSales;

	/** Cランク粗利 */
	BigDecimal rankCMargin;

	/** Cランク本数 */
	BigDecimal rankCNumber;

	/** 重点売上 */
	BigDecimal importantSales;

	/** 重点粗利 */
	BigDecimal importantMargin;

	/** 重点本数 */
	BigDecimal importantNumber;

	/** 新規売上 */
	BigDecimal newSales;

	/** 新規粗利 */
	BigDecimal newMargin;

	/** 新規本数 */
	BigDecimal newNumber;

	/** 深耕売上 */
	BigDecimal deepPlowingSales;

	/** 深耕利益 */
	BigDecimal deepPlowingMargin;

	/** 深耕本数 */
	BigDecimal deepPlowingNumber;

	/** Y-CP売上 */
	BigDecimal yCpSales;

	/** Y-CP粗利 */
	BigDecimal yCpMargin;

	/** Y-CP本数 */
	BigDecimal yCpNumber;

	/** 他社CP売上 */
	BigDecimal otherCpSales;

	/** 他社CP粗利 */
	BigDecimal otherCpMargin;

	/** 他社CP本数 */
	BigDecimal otherCpNumber;

	/** グループ1売上 */
	BigDecimal groupOneSales;

	/** グループ1粗利 */
	BigDecimal groupOneMargin;

	/** グループ1本数 */
	BigDecimal groupOneNumber;

	/** グループ2売上 */
	BigDecimal groupTwoSales;

	/** グループ2粗利 */
	BigDecimal groupTwoMargin;

	/** グループ2取本数 */
	BigDecimal groupTwoNumber;

	/** グループ3売上 */
	BigDecimal groupThreeSales;

	/** グループ3粗利 */
	BigDecimal groupThreeMargin;

	/** グループ3本数 */
	BigDecimal groupThreeNumber;

	/** グループ4売上 */
	BigDecimal groupFourSales;

	/** グループ4粗利 */
	BigDecimal groupFourMargin;

	/** グループ4本数 */
	BigDecimal groupFourNumber;

	/** グループ5売上 */
	BigDecimal groupFiveSales;

	/** グループ5粗利 */
	BigDecimal groupFiveMargin;

	/** グループ5本数 */
	BigDecimal groupFiveNumber;

	/** Aランク年間推定需要 */
	BigDecimal rankADemandEstimation;

	/** Bランク年間推定需要 */
	BigDecimal rankBDemandEstimation;

	/** Cランク年間推定需要 */
	BigDecimal rankCDemandEstimation;

	/** 重点年間推定需要 */
	BigDecimal importantDemandEstimation;

	/** 新規年間推定需要 */
	BigDecimal newDemandEstimation;

	/** 深耕年間推定需要 */
	BigDecimal deepPlowingDemandEstimation;

	/** Y-CP年間推定需要 */
	BigDecimal yCpDemandEstimation;

	/** 他社CP年間推定需要 */
	BigDecimal otherCpDemandEstimation;

	/** グループ1年間推定需要 */
	BigDecimal groupOneDemandEstimation;

	/** グループ2年間推定需要 */
	BigDecimal groupTwoDemandEstimation;

	/** グループ3年間推定需要 */
	BigDecimal groupThreeDemandEstimation;

	/** グループ4年間推定需要 */
	BigDecimal groupFourDemandEstimation;

	/** グループ5年間推定需要 */
	BigDecimal groupFiveDemandEstimation;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public BigDecimal getRankAPlanCount() {
		return rankAPlanCount;
	}

	public void setRankAPlanCount(BigDecimal rankAPlanCount) {
		this.rankAPlanCount = rankAPlanCount;
	}

	public BigDecimal getRankACallCount() {
		return rankACallCount;
	}

	public void setRankACallCount(BigDecimal rankACallCount) {
		this.rankACallCount = rankACallCount;
	}

	public BigDecimal getRankAInfoCount() {
		return rankAInfoCount;
	}

	public void setRankAInfoCount(BigDecimal rankAInfoCount) {
		this.rankAInfoCount = rankAInfoCount;
	}

	public BigDecimal getRankBPlanCount() {
		return rankBPlanCount;
	}

	public void setRankBPlanCount(BigDecimal rankBPlanCount) {
		this.rankBPlanCount = rankBPlanCount;
	}

	public BigDecimal getRankBCallCount() {
		return rankBCallCount;
	}

	public void setRankBCallCount(BigDecimal rankBCallCount) {
		this.rankBCallCount = rankBCallCount;
	}

	public BigDecimal getRankBInfoCount() {
		return rankBInfoCount;
	}

	public void setRankBInfoCount(BigDecimal rankBInfoCount) {
		this.rankBInfoCount = rankBInfoCount;
	}

	public BigDecimal getRankCPlanCount() {
		return rankCPlanCount;
	}

	public void setRankCPlanCount(BigDecimal rankCPlanCount) {
		this.rankCPlanCount = rankCPlanCount;
	}

	public BigDecimal getRankCCallCount() {
		return rankCCallCount;
	}

	public void setRankCCallCount(BigDecimal rankCCallCount) {
		this.rankCCallCount = rankCCallCount;
	}

	public BigDecimal getRankCInfoCount() {
		return rankCInfoCount;
	}

	public void setRankCInfoCount(BigDecimal rankCInfoCount) {
		this.rankCInfoCount = rankCInfoCount;
	}

	public BigDecimal getImportantPlanCount() {
		return importantPlanCount;
	}

	public void setImportantPlanCount(BigDecimal importantPlanCount) {
		this.importantPlanCount = importantPlanCount;
	}

	public BigDecimal getImportantCallCount() {
		return importantCallCount;
	}

	public void setImportantCallCount(BigDecimal importantCallCount) {
		this.importantCallCount = importantCallCount;
	}

	public BigDecimal getImportantInfoCount() {
		return importantInfoCount;
	}

	public void setImportantInfoCount(BigDecimal importantInfoCount) {
		this.importantInfoCount = importantInfoCount;
	}

	public BigDecimal getNewPlanCount() {
		return newPlanCount;
	}

	public void setNewPlanCount(BigDecimal newPlanCount) {
		this.newPlanCount = newPlanCount;
	}

	public BigDecimal getNewCallCount() {
		return newCallCount;
	}

	public void setNewCallCount(BigDecimal newCallCount) {
		this.newCallCount = newCallCount;
	}

	public BigDecimal getNewInfoCount() {
		return newInfoCount;
	}

	public void setNewInfoCount(BigDecimal newInfoCount) {
		this.newInfoCount = newInfoCount;
	}

	public BigDecimal getDeepPlowingPlanCount() {
		return deepPlowingPlanCount;
	}

	public void setDeepPlowingPlanCount(BigDecimal deepPlowingPlanCount) {
		this.deepPlowingPlanCount = deepPlowingPlanCount;
	}

	public BigDecimal getDeepPlowingCallCount() {
		return deepPlowingCallCount;
	}

	public void setDeepPlowingCallCount(BigDecimal deepPlowingCallCount) {
		this.deepPlowingCallCount = deepPlowingCallCount;
	}

	public BigDecimal getDeepPlowingInfoCount() {
		return deepPlowingInfoCount;
	}

	public void setDeepPlowingInfoCount(BigDecimal deepPlowingInfoCount) {
		this.deepPlowingInfoCount = deepPlowingInfoCount;
	}

	public BigDecimal getYCpPlanCount() {
		return yCpPlanCount;
	}

	public void setYCpPlanCount(BigDecimal yCpPlanCount) {
		this.yCpPlanCount = yCpPlanCount;
	}

	public BigDecimal getYCpCallCount() {
		return yCpCallCount;
	}

	public void setYCpCallCount(BigDecimal yCpCallCount) {
		this.yCpCallCount = yCpCallCount;
	}

	public BigDecimal getYCpInfoCount() {
		return yCpInfoCount;
	}

	public void setYCpInfoCount(BigDecimal yCpInfoCount) {
		this.yCpInfoCount = yCpInfoCount;
	}

	public BigDecimal getOtherCpPlanCount() {
		return otherCpPlanCount;
	}

	public void setOtherCpPlanCount(BigDecimal otherCpPlanCount) {
		this.otherCpPlanCount = otherCpPlanCount;
	}

	public BigDecimal getOtherCpCallCount() {
		return otherCpCallCount;
	}

	public void setOtherCpCallCount(BigDecimal otherCpCallCount) {
		this.otherCpCallCount = otherCpCallCount;
	}

	public BigDecimal getOtherCpInfoCount() {
		return otherCpInfoCount;
	}

	public void setOtherCpInfoCount(BigDecimal otherCpInfoCount) {
		this.otherCpInfoCount = otherCpInfoCount;
	}

	public BigDecimal getOutsidePlanCount() {
		return outsidePlanCount;
	}

	public void setOutsidePlanCount(BigDecimal outsidePlanCount) {
		this.outsidePlanCount = outsidePlanCount;
	}

	public BigDecimal getOutsideCallCount() {
		return outsideCallCount;
	}

	public void setOutsideCallCount(BigDecimal outsideCallCount) {
		this.outsideCallCount = outsideCallCount;
	}

	public BigDecimal getOutsideInfoCount() {
		return outsideInfoCount;
	}

	public void setOutsideInfoCount(BigDecimal outsideInfoCount) {
		this.outsideInfoCount = outsideInfoCount;
	}

	public BigDecimal getGroupOnePlanCount() {
		return groupOnePlanCount;
	}

	public void setGroupOnePlanCount(BigDecimal groupOnePlanCount) {
		this.groupOnePlanCount = groupOnePlanCount;
	}

	public BigDecimal getGroupOneCallCount() {
		return groupOneCallCount;
	}

	public void setGroupOneCallCount(BigDecimal groupOneCallCount) {
		this.groupOneCallCount = groupOneCallCount;
	}

	public BigDecimal getGroupOneInfoCount() {
		return groupOneInfoCount;
	}

	public void setGroupOneInfoCount(BigDecimal groupOneInfoCount) {
		this.groupOneInfoCount = groupOneInfoCount;
	}

	public BigDecimal getGroupTwoPlanCount() {
		return groupTwoPlanCount;
	}

	public void setGroupTwoPlanCount(BigDecimal groupTwoPlanCount) {
		this.groupTwoPlanCount = groupTwoPlanCount;
	}

	public BigDecimal getGroupTwoCallCount() {
		return groupTwoCallCount;
	}

	public void setGroupTwoCallCount(BigDecimal groupTwoCallCount) {
		this.groupTwoCallCount = groupTwoCallCount;
	}

	public BigDecimal getGroupTwoInfoCount() {
		return groupTwoInfoCount;
	}

	public void setGroupTwoInfoCount(BigDecimal groupTwoInfoCount) {
		this.groupTwoInfoCount = groupTwoInfoCount;
	}

	public BigDecimal getGroupThreePlanCount() {
		return groupThreePlanCount;
	}

	public void setGroupThreePlanCount(BigDecimal groupThreePlanCount) {
		this.groupThreePlanCount = groupThreePlanCount;
	}

	public BigDecimal getGroupThreeCallCount() {
		return groupThreeCallCount;
	}

	public void setGroupThreeCallCount(BigDecimal groupThreeCallCount) {
		this.groupThreeCallCount = groupThreeCallCount;
	}

	public BigDecimal getGroupThreeInfoCount() {
		return groupThreeInfoCount;
	}

	public void setGroupThreeInfoCount(BigDecimal groupThreeInfoCount) {
		this.groupThreeInfoCount = groupThreeInfoCount;
	}

	public BigDecimal getGroupFourPlanCount() {
		return groupFourPlanCount;
	}

	public void setGroupFourPlanCount(BigDecimal groupFourPlanCount) {
		this.groupFourPlanCount = groupFourPlanCount;
	}

	public BigDecimal getGroupFourCallCount() {
		return groupFourCallCount;
	}

	public void setGroupFourCallCount(BigDecimal groupFourCallCount) {
		this.groupFourCallCount = groupFourCallCount;
	}

	public BigDecimal getGroupFourInfoCount() {
		return groupFourInfoCount;
	}

	public void setGroupFourInfoCount(BigDecimal groupFourInfoCount) {
		this.groupFourInfoCount = groupFourInfoCount;
	}

	public BigDecimal getGroupFivePlanCount() {
		return groupFivePlanCount;
	}

	public void setGroupFivePlanCount(BigDecimal groupFivePlanCount) {
		this.groupFivePlanCount = groupFivePlanCount;
	}

	public BigDecimal getGroupFiveCallCount() {
		return groupFiveCallCount;
	}

	public void setGroupFiveCallCount(BigDecimal groupFiveCallCount) {
		this.groupFiveCallCount = groupFiveCallCount;
	}

	public BigDecimal getGroupFiveInfoCount() {
		return groupFiveInfoCount;
	}

	public void setGroupFiveInfoCount(BigDecimal groupFiveInfoCount) {
		this.groupFiveInfoCount = groupFiveInfoCount;
	}

	public BigDecimal getRankAOwnCount() {
		return rankAOwnCount;
	}

	public void setRankAOwnCount(BigDecimal rankAOwnCount) {
		this.rankAOwnCount = rankAOwnCount;
	}

	public BigDecimal getRankADealCount() {
		return rankADealCount;
	}

	public void setRankADealCount(BigDecimal rankADealCount) {
		this.rankADealCount = rankADealCount;
	}

	public BigDecimal getRankAAccountMasterCount() {
		return rankAAccountMasterCount;
	}

	public void setRankAAccountMasterCount(BigDecimal rankAAccountMasterCount) {
		this.rankAAccountMasterCount = rankAAccountMasterCount;
	}

	public BigDecimal getRankBOwnCount() {
		return rankBOwnCount;
	}

	public void setRankBOwnCount(BigDecimal rankBOwnCount) {
		this.rankBOwnCount = rankBOwnCount;
	}

	public BigDecimal getRankBDealCount() {
		return rankBDealCount;
	}

	public void setRankBDealCount(BigDecimal rankBDealCount) {
		this.rankBDealCount = rankBDealCount;
	}

	public BigDecimal getRankBAccountMasterCount() {
		return rankBAccountMasterCount;
	}

	public void setRankBAccountMasterCount(BigDecimal rankBAccountMasterCount) {
		this.rankBAccountMasterCount = rankBAccountMasterCount;
	}

	public BigDecimal getRankCOwnCount() {
		return rankCOwnCount;
	}

	public void setRankCOwnCount(BigDecimal rankCOwnCount) {
		this.rankCOwnCount = rankCOwnCount;
	}

	public BigDecimal getRankCDealCount() {
		return rankCDealCount;
	}

	public void setRankCDealCount(BigDecimal rankCDealCount) {
		this.rankCDealCount = rankCDealCount;
	}

	public BigDecimal getRankCAccountMasterCount() {
		return rankCAccountMasterCount;
	}

	public void setRankCAccountMasterCount(BigDecimal rankCAccountMasterCount) {
		this.rankCAccountMasterCount = rankCAccountMasterCount;
	}

	public BigDecimal getImportantOwnCount() {
		return importantOwnCount;
	}

	public void setImportantOwnCount(BigDecimal importantOwnCount) {
		this.importantOwnCount = importantOwnCount;
	}

	public BigDecimal getImportantDealCount() {
		return importantDealCount;
	}

	public void setImportantDealCount(BigDecimal importantDealCount) {
		this.importantDealCount = importantDealCount;
	}

	public BigDecimal getImportantAccountMasterCount() {
		return importantAccountMasterCount;
	}

	public void setImportantAccountMasterCount(BigDecimal importantAccountMasterCount) {
		this.importantAccountMasterCount = importantAccountMasterCount;
	}

	public BigDecimal getNewOwnCount() {
		return newOwnCount;
	}

	public void setNewOwnCount(BigDecimal newOwnCount) {
		this.newOwnCount = newOwnCount;
	}

	public BigDecimal getNewDealCount() {
		return newDealCount;
	}

	public void setNewDealCount(BigDecimal newDealCount) {
		this.newDealCount = newDealCount;
	}

	public BigDecimal getNewAccountMasterCount() {
		return newAccountMasterCount;
	}

	public void setNewAccountMasterCount(BigDecimal newAccountMasterCount) {
		this.newAccountMasterCount = newAccountMasterCount;
	}

	public BigDecimal getDeepPlowingOwnCount() {
		return deepPlowingOwnCount;
	}

	public void setDeepPlowingOwnCount(BigDecimal deepPlowingOwnCount) {
		this.deepPlowingOwnCount = deepPlowingOwnCount;
	}

	public BigDecimal getDeepPlowingDealCount() {
		return deepPlowingDealCount;
	}

	public void setDeepPlowingDealCount(BigDecimal deepPlowingDealCount) {
		this.deepPlowingDealCount = deepPlowingDealCount;
	}

	public BigDecimal getDeepPlowingAccountMasterCount() {
		return deepPlowingAccountMasterCount;
	}

	public void setDeepPlowingAccountMasterCount(BigDecimal deepPlowingAccountMasterCount) {
		this.deepPlowingAccountMasterCount = deepPlowingAccountMasterCount;
	}

	public BigDecimal getYCpOwnCount() {
		return yCpOwnCount;
	}

	public void setYCpOwnCount(BigDecimal yCpOwnCount) {
		this.yCpOwnCount = yCpOwnCount;
	}

	public BigDecimal getYCpDealCount() {
		return yCpDealCount;
	}

	public void setYCpDealCount(BigDecimal yCpDealCount) {
		this.yCpDealCount = yCpDealCount;
	}

	public BigDecimal getYCpAccountMasterCount() {
		return yCpAccountMasterCount;
	}

	public void setYCpAccountMasterCount(BigDecimal yCpAccountMasterCount) {
		this.yCpAccountMasterCount = yCpAccountMasterCount;
	}

	public BigDecimal getOtherCpOwnCount() {
		return otherCpOwnCount;
	}

	public void setOtherCpOwnCount(BigDecimal otherCpOwnCount) {
		this.otherCpOwnCount = otherCpOwnCount;
	}

	public BigDecimal getOtherCpDealCount() {
		return otherCpDealCount;
	}

	public void setOtherCpDealCount(BigDecimal otherCpDealCount) {
		this.otherCpDealCount = otherCpDealCount;
	}

	public BigDecimal getOtherCpAccountMasterCount() {
		return otherCpAccountMasterCount;
	}

	public void setOtherCpAccountMasterCount(BigDecimal otherCpAccountMasterCount) {
		this.otherCpAccountMasterCount = otherCpAccountMasterCount;
	}

	public BigDecimal getGroupOneOwnCount() {
		return groupOneOwnCount;
	}

	public void setGroupOneOwnCount(BigDecimal groupOneOwnCount) {
		this.groupOneOwnCount = groupOneOwnCount;
	}

	public BigDecimal getGroupOneDealCount() {
		return groupOneDealCount;
	}

	public void setGroupOneDealCount(BigDecimal groupOneDealCount) {
		this.groupOneDealCount = groupOneDealCount;
	}

	public BigDecimal getGroupOneAccountMasterCount() {
		return groupOneAccountMasterCount;
	}

	public void setGroupOneAccountMasterCount(BigDecimal groupOneAccountMasterCount) {
		this.groupOneAccountMasterCount = groupOneAccountMasterCount;
	}

	public BigDecimal getGroupTwoOwnCount() {
		return groupTwoOwnCount;
	}

	public void setGroupTwoOwnCount(BigDecimal groupTwoOwnCount) {
		this.groupTwoOwnCount = groupTwoOwnCount;
	}

	public BigDecimal getGroupTwoDealCount() {
		return groupTwoDealCount;
	}

	public void setGroupTwoDealCount(BigDecimal groupTwoDealCount) {
		this.groupTwoDealCount = groupTwoDealCount;
	}

	public BigDecimal getGroupTwoAccountMasterCount() {
		return groupTwoAccountMasterCount;
	}

	public void setGroupTwoAccountMasterCount(BigDecimal groupTwoAccountMasterCount) {
		this.groupTwoAccountMasterCount = groupTwoAccountMasterCount;
	}

	public BigDecimal getGroupThreeOwnCount() {
		return groupThreeOwnCount;
	}

	public void setGroupThreeOwnCount(BigDecimal groupThreeOwnCount) {
		this.groupThreeOwnCount = groupThreeOwnCount;
	}

	public BigDecimal getGroupThreeDealCount() {
		return groupThreeDealCount;
	}

	public void setGroupThreeDealCount(BigDecimal groupThreeDealCount) {
		this.groupThreeDealCount = groupThreeDealCount;
	}

	public BigDecimal getGroupThreeAccountMasterCount() {
		return groupThreeAccountMasterCount;
	}

	public void setGroupThreeAccountMasterCount(BigDecimal groupThreeAccountMasterCount) {
		this.groupThreeAccountMasterCount = groupThreeAccountMasterCount;
	}

	public BigDecimal getGroupFourOwnCount() {
		return groupFourOwnCount;
	}

	public void setGroupFourOwnCount(BigDecimal groupFourOwnCount) {
		this.groupFourOwnCount = groupFourOwnCount;
	}

	public BigDecimal getGroupFourDealCount() {
		return groupFourDealCount;
	}

	public void setGroupFourDealCount(BigDecimal groupFourDealCount) {
		this.groupFourDealCount = groupFourDealCount;
	}

	public BigDecimal getGroupFourAccountMasterCount() {
		return groupFourAccountMasterCount;
	}

	public void setGroupFourAccountMasterCount(BigDecimal groupFourAccountMasterCount) {
		this.groupFourAccountMasterCount = groupFourAccountMasterCount;
	}

	public BigDecimal getGroupFiveOwnCount() {
		return groupFiveOwnCount;
	}

	public void setGroupFiveOwnCount(BigDecimal groupFiveOwnCount) {
		this.groupFiveOwnCount = groupFiveOwnCount;
	}

	public BigDecimal getGroupFiveDealCount() {
		return groupFiveDealCount;
	}

	public void setGroupFiveDealCount(BigDecimal groupFiveDealCount) {
		this.groupFiveDealCount = groupFiveDealCount;
	}

	public BigDecimal getGroupFiveAccountMasterCount() {
		return groupFiveAccountMasterCount;
	}

	public void setGroupFiveAccountMasterCount(BigDecimal groupFiveAccountMasterCount) {
		this.groupFiveAccountMasterCount = groupFiveAccountMasterCount;
	}

	public BigDecimal getRankASales() {
		return rankASales;
	}

	public void setRankASales(BigDecimal rankASales) {
		this.rankASales = rankASales;
	}

	public BigDecimal getRankAMargin() {
		return rankAMargin;
	}

	public void setRankAMargin(BigDecimal rankAMargin) {
		this.rankAMargin = rankAMargin;
	}

	public BigDecimal getRankANumber() {
		return rankANumber;
	}

	public void setRankANumber(BigDecimal rankANumber) {
		this.rankANumber = rankANumber;
	}

	public BigDecimal getRankBSales() {
		return rankBSales;
	}

	public void setRankBSales(BigDecimal rankBSales) {
		this.rankBSales = rankBSales;
	}

	public BigDecimal getRankBMargin() {
		return rankBMargin;
	}

	public void setRankBMargin(BigDecimal rankBMargin) {
		this.rankBMargin = rankBMargin;
	}

	public BigDecimal getRankBNumber() {
		return rankBNumber;
	}

	public void setRankBNumber(BigDecimal rankBNumber) {
		this.rankBNumber = rankBNumber;
	}

	public BigDecimal getRankCSales() {
		return rankCSales;
	}

	public void setRankCSales(BigDecimal rankCSales) {
		this.rankCSales = rankCSales;
	}

	public BigDecimal getRankCMargin() {
		return rankCMargin;
	}

	public void setRankCMargin(BigDecimal rankCMargin) {
		this.rankCMargin = rankCMargin;
	}

	public BigDecimal getRankCNumber() {
		return rankCNumber;
	}

	public void setRankCNumber(BigDecimal rankCNumber) {
		this.rankCNumber = rankCNumber;
	}

	public BigDecimal getImportantSales() {
		return importantSales;
	}

	public void setImportantSales(BigDecimal importantSales) {
		this.importantSales = importantSales;
	}

	public BigDecimal getImportantMargin() {
		return importantMargin;
	}

	public void setImportantMargin(BigDecimal importantMargin) {
		this.importantMargin = importantMargin;
	}

	public BigDecimal getImportantNumber() {
		return importantNumber;
	}

	public void setImportantNumber(BigDecimal importantNumber) {
		this.importantNumber = importantNumber;
	}

	public BigDecimal getNewSales() {
		return newSales;
	}

	public void setNewSales(BigDecimal newSales) {
		this.newSales = newSales;
	}

	public BigDecimal getNewMargin() {
		return newMargin;
	}

	public void setNewMargin(BigDecimal newMargin) {
		this.newMargin = newMargin;
	}

	public BigDecimal getNewNumber() {
		return newNumber;
	}

	public void setNewNumber(BigDecimal newNumber) {
		this.newNumber = newNumber;
	}

	public BigDecimal getDeepPlowingSales() {
		return deepPlowingSales;
	}

	public void setDeepPlowingSales(BigDecimal deepPlowingSales) {
		this.deepPlowingSales = deepPlowingSales;
	}

	public BigDecimal getDeepPlowingMargin() {
		return deepPlowingMargin;
	}

	public void setDeepPlowingMargin(BigDecimal deepPlowingMargin) {
		this.deepPlowingMargin = deepPlowingMargin;
	}

	public BigDecimal getDeepPlowingNumber() {
		return deepPlowingNumber;
	}

	public void setDeepPlowingNumber(BigDecimal deepPlowingNumber) {
		this.deepPlowingNumber = deepPlowingNumber;
	}

	public BigDecimal getYCpSales() {
		return yCpSales;
	}

	public void setYCpSales(BigDecimal yCpSales) {
		this.yCpSales = yCpSales;
	}

	public BigDecimal getYCpMargin() {
		return yCpMargin;
	}

	public void setYCpMargin(BigDecimal yCpMargin) {
		this.yCpMargin = yCpMargin;
	}

	public BigDecimal getYCpNumber() {
		return yCpNumber;
	}

	public void setYCpNumber(BigDecimal yCpNumber) {
		this.yCpNumber = yCpNumber;
	}

	public BigDecimal getOtherCpSales() {
		return otherCpSales;
	}

	public void setOtherCpSales(BigDecimal otherCpSales) {
		this.otherCpSales = otherCpSales;
	}

	public BigDecimal getOtherCpMargin() {
		return otherCpMargin;
	}

	public void setOtherCpMargin(BigDecimal otherCpMargin) {
		this.otherCpMargin = otherCpMargin;
	}

	public BigDecimal getOtherCpNumber() {
		return otherCpNumber;
	}

	public void setOtherCpNumber(BigDecimal otherCpNumber) {
		this.otherCpNumber = otherCpNumber;
	}

	public BigDecimal getGroupOneSales() {
		return groupOneSales;
	}

	public void setGroupOneSales(BigDecimal groupOneSales) {
		this.groupOneSales = groupOneSales;
	}

	public BigDecimal getGroupOneMargin() {
		return groupOneMargin;
	}

	public void setGroupOneMargin(BigDecimal groupOneMargin) {
		this.groupOneMargin = groupOneMargin;
	}

	public BigDecimal getGroupOneNumber() {
		return groupOneNumber;
	}

	public void setGroupOneNumber(BigDecimal groupOneNumber) {
		this.groupOneNumber = groupOneNumber;
	}

	public BigDecimal getGroupTwoSales() {
		return groupTwoSales;
	}

	public void setGroupTwoSales(BigDecimal groupTwoSales) {
		this.groupTwoSales = groupTwoSales;
	}

	public BigDecimal getGroupTwoMargin() {
		return groupTwoMargin;
	}

	public void setGroupTwoMargin(BigDecimal groupTwoMargin) {
		this.groupTwoMargin = groupTwoMargin;
	}

	public BigDecimal getGroupTwoNumber() {
		return groupTwoNumber;
	}

	public void setGroupTwoNumber(BigDecimal groupTwoNumber) {
		this.groupTwoNumber = groupTwoNumber;
	}

	public BigDecimal getGroupThreeSales() {
		return groupThreeSales;
	}

	public void setGroupThreeSales(BigDecimal groupThreeSales) {
		this.groupThreeSales = groupThreeSales;
	}

	public BigDecimal getGroupThreeMargin() {
		return groupThreeMargin;
	}

	public void setGroupThreeMargin(BigDecimal groupThreeMargin) {
		this.groupThreeMargin = groupThreeMargin;
	}

	public BigDecimal getGroupThreeNumber() {
		return groupThreeNumber;
	}

	public void setGroupThreeNumber(BigDecimal groupThreeNumber) {
		this.groupThreeNumber = groupThreeNumber;
	}

	public BigDecimal getGroupFourSales() {
		return groupFourSales;
	}

	public void setGroupFourSales(BigDecimal groupFourSales) {
		this.groupFourSales = groupFourSales;
	}

	public BigDecimal getGroupFourMargin() {
		return groupFourMargin;
	}

	public void setGroupFourMargin(BigDecimal groupFourMargin) {
		this.groupFourMargin = groupFourMargin;
	}

	public BigDecimal getGroupFourNumber() {
		return groupFourNumber;
	}

	public void setGroupFourNumber(BigDecimal groupFourNumber) {
		this.groupFourNumber = groupFourNumber;
	}

	public BigDecimal getGroupFiveSales() {
		return groupFiveSales;
	}

	public void setGroupFiveSales(BigDecimal groupFiveSales) {
		this.groupFiveSales = groupFiveSales;
	}

	public BigDecimal getGroupFiveMargin() {
		return groupFiveMargin;
	}

	public void setGroupFiveMargin(BigDecimal groupFiveMargin) {
		this.groupFiveMargin = groupFiveMargin;
	}

	public BigDecimal getGroupFiveNumber() {
		return groupFiveNumber;
	}

	public void setGroupFiveNumber(BigDecimal groupFiveNumber) {
		this.groupFiveNumber = groupFiveNumber;
	}


	public BigDecimal getRankADemandEstimation() {
		return rankADemandEstimation;
	}

	public void setRankADemandEstimation(BigDecimal rankADemandEstimation) {
		this.rankADemandEstimation = rankADemandEstimation;
	}

	public BigDecimal getRankBDemandEstimation() {
		return rankBDemandEstimation;
	}

	public void setRankBDemandEstimation(BigDecimal rankBDemandEstimation) {
		this.rankBDemandEstimation = rankBDemandEstimation;
	}

	public BigDecimal getRankCDemandEstimation() {
		return rankCDemandEstimation;
	}

	public void setRankCDemandEstimation(BigDecimal rankCDemandEstimation) {
		this.rankCDemandEstimation = rankCDemandEstimation;
	}

	public BigDecimal getImportantDemandEstimation() {
		return importantDemandEstimation;
	}

	public void setImportantDemandEstimation(BigDecimal importantDemandEstimation) {
		this.importantDemandEstimation = importantDemandEstimation;
	}

	public BigDecimal getNewDemandEstimation() {
		return newDemandEstimation;
	}

	public void setNewDemandEstimation(BigDecimal newDemandEstimation) {
		this.newDemandEstimation = newDemandEstimation;
	}

	public BigDecimal getDeepPlowingDemandEstimation() {
		return deepPlowingDemandEstimation;
	}

	public void setDeepPlowingDemandEstimation(BigDecimal deepPlowingDemandEstimation) {
		this.deepPlowingDemandEstimation = deepPlowingDemandEstimation;
	}

	public BigDecimal getYCpDemandEstimation() {
		return yCpDemandEstimation;
	}

	public void setYCpDemandEstimation(BigDecimal yCpDemandEstimation) {
		this.yCpDemandEstimation = yCpDemandEstimation;
	}

	public BigDecimal getOtherCpDemandEstimation() {
		return otherCpDemandEstimation;
	}

	public void setOtherCpDemandEstimation(BigDecimal otherCpDemandEstimation) {
		this.otherCpDemandEstimation = otherCpDemandEstimation;
	}

	public BigDecimal getGroupOneDemandEstimation() {
		return groupOneDemandEstimation;
	}

	public void setGroupOneDemandEstimation(BigDecimal groupOneDemandEstimation) {
		this.groupOneDemandEstimation = groupOneDemandEstimation;
	}

	public BigDecimal getGroupTwoDemandEstimation() {
		return groupTwoDemandEstimation;
	}

	public void setGroupTwoDemandEstimation(BigDecimal groupTwoDemandEstimation) {
		this.groupTwoDemandEstimation = groupTwoDemandEstimation;
	}

	public BigDecimal getGroupThreeDemandEstimation() {
		return groupThreeDemandEstimation;
	}

	public void setGroupThreeDemandEstimation(BigDecimal groupThreeDemandEstimation) {
		this.groupThreeDemandEstimation = groupThreeDemandEstimation;
	}

	public BigDecimal getGroupFourDemandEstimation() {
		return groupFourDemandEstimation;
	}

	public void setGroupFourDemandEstimation(BigDecimal groupFourDemandEstimation) {
		this.groupFourDemandEstimation = groupFourDemandEstimation;
	}

	public BigDecimal getGroupFiveDemandEstimation() {
		return groupFiveDemandEstimation;
	}

	public void setGroupFiveDemandEstimation(BigDecimal groupFiveDemandEstimation) {
		this.groupFiveDemandEstimation = groupFiveDemandEstimation;
	}

	public BigDecimal getRankADealPlanCount() {
		return rankADealPlanCount;
	}

	public void setRankADealPlanCount(BigDecimal rankADealPlanCount) {
		this.rankADealPlanCount = rankADealPlanCount;
	}

	public BigDecimal getRankBDealPlanCount() {
		return rankBDealPlanCount;
	}

	public void setRankBDealPlanCount(BigDecimal rankBDealPlanCount) {
		this.rankBDealPlanCount = rankBDealPlanCount;
	}

	public BigDecimal getRankCDealPlanCount() {
		return rankCDealPlanCount;
	}

	public void setRankCDealPlanCount(BigDecimal rankCDealPlanCount) {
		this.rankCDealPlanCount = rankCDealPlanCount;
	}

	public BigDecimal getImportantDealPlanCount() {
		return importantDealPlanCount;
	}

	public void setImportantDealPlanCount(BigDecimal importantDealPlanCount) {
		this.importantDealPlanCount = importantDealPlanCount;
	}

	public BigDecimal getNewDealPlanCount() {
		return newDealPlanCount;
	}

	public void setNewDealPlanCount(BigDecimal newDealPlanCount) {
		this.newDealPlanCount = newDealPlanCount;
	}

	public BigDecimal getDeepPlowingDealPlanCount() {
		return deepPlowingDealPlanCount;
	}

	public void setDeepPlowingDealPlanCount(BigDecimal deepPlowingDealPlanCount) {
		this.deepPlowingDealPlanCount = deepPlowingDealPlanCount;
	}

	public BigDecimal getYCpDealPlanCount() {
		return yCpDealPlanCount;
	}

	public void setYCpDealPlanCount(BigDecimal yCpDealPlanCount) {
		this.yCpDealPlanCount = yCpDealPlanCount;
	}

	public BigDecimal getOtherCpDealPlanCount() {
		return otherCpDealPlanCount;
	}

	public void setOtherCpDealPlanCount(BigDecimal otherCpDealPlanCount) {
		this.otherCpDealPlanCount = otherCpDealPlanCount;
	}

	public BigDecimal getGroupOneDealPlanCount() {
		return groupOneDealPlanCount;
	}

	public void setGroupOneDealPlanCount(BigDecimal groupOneDealPlanCount) {
		this.groupOneDealPlanCount = groupOneDealPlanCount;
	}

	public BigDecimal getGroupTwoDealPlanCount() {
		return groupTwoDealPlanCount;
	}

	public void setGroupTwoDealPlanCount(BigDecimal groupTwoDealPlanCount) {
		this.groupTwoDealPlanCount = groupTwoDealPlanCount;
	}

	public BigDecimal getGroupThreeDealPlanCount() {
		return groupThreeDealPlanCount;
	}

	public void setGroupThreeDealPlanCount(BigDecimal groupThreeDealPlanCount) {
		this.groupThreeDealPlanCount = groupThreeDealPlanCount;
	}

	public BigDecimal getGroupFourDealPlanCount() {
		return groupFourDealPlanCount;
	}

	public void setGroupFourDealPlanCount(BigDecimal groupFourDealPlanCount) {
		this.groupFourDealPlanCount = groupFourDealPlanCount;
	}

	public BigDecimal getGroupFiveDealPlanCount() {
		return groupFiveDealPlanCount;
	}

	public void setGroupFiveDealPlanCount(BigDecimal groupFiveDealPlanCount) {
		this.groupFiveDealPlanCount = groupFiveDealPlanCount;
	}

}