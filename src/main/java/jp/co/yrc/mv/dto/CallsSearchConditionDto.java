package jp.co.yrc.mv.dto;

import java.util.Date;
import java.util.List;

public class CallsSearchConditionDto extends CallsDto {

	/**
	 * ログイン者のID
	 */
	private String userId;
	private List<String> div1;
	private String div2;
	private String div3;
	private Date from;
	private Date to;
	private int maxCount;
	private Integer year;
	private Integer month;
	private int yearMonth;
	private String market;
	private String accountId;

	private boolean history;

	private boolean heldOnly;
	private boolean hasInfoOnly;
	private boolean unreadOnly;
	private boolean tantoOnly;

	private String marketInfoInfoDiv;

	private String callsInfoDiv;

	private boolean tech;

	private boolean checkExistsTech;

	private boolean ignoreSosikiCondition;

	private String locale;

	private String company;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getDiv1() {
		return div1;
	}

	public void setDiv1(List<String> div1) {
		this.div1 = div1;
	}

	public String getDiv2() {
		return div2;
	}

	public void setDiv2(String div2) {
		this.div2 = div2;
	}

	public String getDiv3() {
		return div3;
	}

	public void setDiv3(String div3) {
		this.div3 = div3;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}

	public int getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(int yearMonth) {
		this.yearMonth = yearMonth;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public boolean isHeldOnly() {
		return heldOnly;
	}

	public void setHeldOnly(boolean heldOnly) {
		this.heldOnly = heldOnly;
	}

	public boolean isHasInfoOnly() {
		return hasInfoOnly;
	}

	public void setHasInfoOnly(boolean hasInfoOnly) {
		this.hasInfoOnly = hasInfoOnly;
	}

	public boolean isUnreadOnly() {
		return unreadOnly;
	}

	public void setUnreadOnly(boolean unreadOnly) {
		this.unreadOnly = unreadOnly;
	}

	public boolean isTech() {
		return tech;
	}

	public void setTech(boolean tech) {
		this.tech = tech;
	}

	public String getMarketInfoInfoDiv() {
		return marketInfoInfoDiv;
	}

	public void setMarketInfoInfoDiv(String marketInfoInfoDiv) {
		this.marketInfoInfoDiv = marketInfoInfoDiv;
	}

	public String getCallsInfoDiv() {
		return callsInfoDiv;
	}

	public void setCallsInfoDiv(String callsInfoDiv) {
		this.callsInfoDiv = callsInfoDiv;
	}

	public boolean isCheckExistsTech() {
		return checkExistsTech;
	}

	public void setCheckExistsTech(boolean checkExistsTech) {
		this.checkExistsTech = checkExistsTech;
	}

	public boolean canIgnoreSosikiCondition() {
		return ignoreSosikiCondition;
	}

	public void setIgnoreSosikiCondition(boolean ignoreSosikiCondition) {
		this.ignoreSosikiCondition = ignoreSosikiCondition;
	}

	public boolean isTantoOnly() {
		return tantoOnly;
	}

	public void setTantoOnly(boolean tantoOnly) {
		this.tantoOnly = tantoOnly;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
}
