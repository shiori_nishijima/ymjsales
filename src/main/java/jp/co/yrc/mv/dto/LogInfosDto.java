package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

import jp.co.yrc.mv.entity.LogInfos;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class LogInfosDto extends LogInfos {

	String div1;
	String div2;
	String div3;
	@Column(name = "div1_nm")
	String div1Nm;
	@Column(name = "div2_nm")
	String div2Nm;
	@Column(name = "div3_nm")
	String div3Nm;
	String eigyoSosikiCd;
	String eigyoSosikiNm;

	String view;
	String viewName;
	String clientType;
	String clientTypeName;

	String firstName;
	String lastName;

	BigDecimal count = BigDecimal.ZERO;

	long time;

	public String getDiv1Nm() {
		return div1Nm;
	}

	public void setDiv1Nm(String div1Nm) {
		this.div1Nm = div1Nm;
	}

	public String getDiv2Nm() {
		return div2Nm;
	}

	public void setDiv2Nm(String div2Nm) {
		this.div2Nm = div2Nm;
	}

	public String getDiv3Nm() {
		return div3Nm;
	}

	public void setDiv3Nm(String div3Nm) {
		this.div3Nm = div3Nm;
	}

	public String getEigyoSosikiCd() {
		return eigyoSosikiCd;
	}

	public void setEigyoSosikiCd(String eigyoSosikiCd) {
		this.eigyoSosikiCd = eigyoSosikiCd;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getDiv1() {
		return div1;
	}

	public void setDiv1(String div1) {
		this.div1 = div1;
	}

	public String getDiv2() {
		return div2;
	}

	public void setDiv2(String div2) {
		this.div2 = div2;
	}

	public String getDiv3() {
		return div3;
	}

	public void setDiv3(String div3) {
		this.div3 = div3;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getClientTypeName() {
		return clientTypeName;
	}

	public void setClientTypeName(String clientTypeName) {
		this.clientTypeName = clientTypeName;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	/**
	 * @return the count
	 */
	public BigDecimal getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(BigDecimal count) {
		this.count = count;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
