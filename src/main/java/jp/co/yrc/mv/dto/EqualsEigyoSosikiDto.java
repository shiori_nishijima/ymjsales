package jp.co.yrc.mv.dto;

import org.apache.commons.lang3.ObjectUtils;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class EqualsEigyoSosikiDto extends EigyoSosikiDto implements Comparable<EqualsEigyoSosikiDto> {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getEigyoSosikiCd() == null) ? 0 : getEigyoSosikiCd().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EqualsEigyoSosikiDto other = (EqualsEigyoSosikiDto) obj;
		if (getEigyoSosikiCd() == null) {
			if (other.getEigyoSosikiCd() != null)
				return false;
		} else if (!getEigyoSosikiCd().equals(other.getEigyoSosikiCd()))
			return false;
		return true;
	}

	@Override
	public int compareTo(EqualsEigyoSosikiDto o) {
		int result = ObjectUtils.compare(getDiv1Cd(), o.getDiv1Cd());
		if (result == 0) {
			result = ObjectUtils.compare(getDiv2Cd(), o.getDiv2Cd());
			if (result == 0) {
				result = ObjectUtils.compare(getDiv3Cd(), o.getDiv3Cd());
				if (result == 0) {
					return ObjectUtils.compare(getUserId(), o.getUserId());
				}
			}
		}
		return result;
	}

}
