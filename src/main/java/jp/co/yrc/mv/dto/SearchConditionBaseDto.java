package jp.co.yrc.mv.dto;

public class SearchConditionBaseDto {
	protected Integer year;
	protected Integer month;
	protected Integer date;
	protected String div1;
	protected String div2;
	protected String div3;
	protected String tanto;
	protected String middleClass;
	protected String largeClass2;
	protected String resultType;
	protected String net;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

	public String getDiv1() {
		return div1;
	}

	public void setDiv1(String div1) {
		this.div1 = div1;
	}

	public String getDiv2() {
		return div2;
	}

	public void setDiv2(String div2) {
		this.div2 = div2;
	}

	public String getDiv3() {
		return div3;
	}

	public void setDiv3(String div3) {
		this.div3 = div3;
	}

	public String getTanto() {
		return tanto;
	}

	public void setTanto(String tanto) {
		this.tanto = tanto;
	}

	public String getMiddleClass() {
		return middleClass;
	}

	public void setMiddleClass(String middleClass) {
		this.middleClass = middleClass;
	}

	public String getLargeClass2() {
		return largeClass2;
	}

	public void setLargeClass2(String largeClass2) {
		this.largeClass2 = largeClass2;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getNet() {
		return net;
	}

	public void setNet(String net) {
		this.net = net;
	}

}
