package jp.co.yrc.mv.dto;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

import jp.co.yrc.mv.entity.CallsAccounts;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CallsAccountsDto extends CallsAccounts {
	private String accountName;
	private String lastName;
	private String firstName;
	@Column(name = "div2_nm") // 2.2.0より前のバージョンでは数値のあとの大文字は認識しないのでこの指定が必要。
	private String div2Nm;
	@Column(name = "div3_nm")
	private String div3Nm;
	 
	public String getAccountName() {
		return accountName;
	}
	
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getDiv2Nm() {
		return div2Nm;
	}

	public void setDiv2Nm(String div2Nm) {
		this.div2Nm = div2Nm;
	}

	public String getDiv3Nm() {
		return div3Nm;
	}

	public void setDiv3Nm(String div3Nm) {
		this.div3Nm = div3Nm;
	}	
}
