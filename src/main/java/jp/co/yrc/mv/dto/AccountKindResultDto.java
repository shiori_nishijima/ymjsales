package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import jp.co.yrc.mv.entity.MonthlyKindResults;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class AccountKindResultDto extends MonthlyKindResults {
	
	/** MonthlyKindResultsに存在しない値はここに書く */
	/** LTRナツ売上 */
    BigDecimal ltrSummerSales;

	/** LTRナツ粗利*/
    BigDecimal ltrSummerMargin;

	/** LTRナツ粗利本社*/
    BigDecimal ltrSummerHqMargin;

	/** LTRナツ本数*/
	BigDecimal ltrSummerNumber;

	/** LTRスノー売上*/
    BigDecimal ltrSnowSales;

	/** LTRスノー粗利*/
    BigDecimal ltrSnowMargin;

	/** LTRスノー粗利本社*/
    BigDecimal ltrSnowHqMargin;

	/** LTRスノー本数*/
    BigDecimal ltrSnowNumber;

	/** 取引先コード */
	String id;

	/** 名称 */
	String name;

	/** 名称（母国語） */
	String nameNative;

	/** 郵便番号 */
	String billingAddressPostalcode;

	/** 住所 */
	String billingAddress;

	/** 住所（母国語） */
	String billingAddressNative;

	/** 親コード */
	String parentId;

	/** 担当者 */
	String assignedUserId;

	/** 販社コード */
	String salesCompanyCode;

	/** 部門コード */
	String departmentCode;

	/** 営業所コード */
	String salesOfficeCode;

	/** 検索用名称 */
	String nameForSearch;

	/** 検索用名称（母国語） */
	String nameForSearchNative;

	/** 販路 */
	String marketId;

	/** 銘柄 */
	String brandId;

	/** 持軒数カウント対象 */
	boolean ownCount;

	/** 取引軒数カウント対象 */
	boolean dealCount;

	/** グルーピング重点 */
	boolean groupImportant;

	/** グルーピング新規 */
	boolean groupNew;

	/** グルーピング深耕 */
	boolean groupDeepPlowing;

	/** グルーピングYコンセプトショップ */
	@Column(name = "group_y_cp")
	boolean groupYCp;

	/** グルーピング他社コンセプトショップ */
	boolean groupOtherCp;

	/** グルーピング1G */
	boolean groupOne;

	/** グルーピング2G */
	boolean groupTwo;

	/** グルーピング3G */
	boolean groupThree;

	/** グルーピング4G */
	boolean groupFour;

	/** グルーピング5G */
	boolean groupFive;

	/** 売上ランク */
	String salesRank;

	/** 需要本数 */
	BigDecimal demandNumber;

	/** 推定ISSYH */
	BigDecimal issYh;

	/** 推定ISSBS */
	BigDecimal issBs;

	/** 推定ISSDL */
	BigDecimal issDl;

	/** 推定ISSTY */
	BigDecimal issTy;

	/** 推定ISSGY */
	BigDecimal issGy;

	/** 推定ISSMI */
	BigDecimal issMi;

	/** 推定ISSPB */
	BigDecimal issPb;

	/** 推定ISSその他 */
	BigDecimal issOther;

	String marketName;

	String firstName;
	String lastName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    /** 
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /** 
     * Sets the name.
     * 
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Returns the nameKana.
     * 
     * @return the nameKana
     */
    public String getNameNative() {
        return nameNative;
    }

    /** 
     * Sets the nameKana.
     * 
     * @param nameKana the nameKana
     */
    public void setNameNative(String nameNative) {
        this.nameNative = nameNative;
    }

    /** 
     * Returns the billingAddressPostalcode.
     * 
     * @return the billingAddressPostalcode
     */
    public String getBillingAddressPostalcode() {
        return billingAddressPostalcode;
    }

    /** 
     * Sets the billingAddressPostalcode.
     * 
     * @param billingAddressPostalcode the billingAddressPostalcode
     */
    public void setBillingAddressPostalcode(String billingAddressPostalcode) {
        this.billingAddressPostalcode = billingAddressPostalcode;
    }

    /** 
     * Returns the billingAddress.
     * 
     * @return the billingAddress
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /** 
     * Sets the billingAddress.
     * 
     * @param billingAddress the billingAddress
     */
    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /** 
     * Returns the billingAddressNative.
     * 
     * @return the billingAddressNative
     */
    public String getBillingAddressNative() {
        return billingAddressNative;
    }

    /** 
     * Sets the billingAddressNative.
     * 
     * @param billingAddressNative the billingAddressNative
     */
    public void setBillingAddressNative(String billingAddressNative) {
        this.billingAddressNative = billingAddressNative;
    }
    
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	public String getSalesCompanyCode() {
		return salesCompanyCode;
	}

	public void setSalesCompanyCode(String salesCompanyCode) {
		this.salesCompanyCode = salesCompanyCode;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getSalesOfficeCode() {
		return salesOfficeCode;
	}

	public void setSalesOfficeCode(String salesOfficeCode) {
		this.salesOfficeCode = salesOfficeCode;
	}

	public String getNameForSearch() {
		return nameForSearch;
	}

	public void setNameForSearch(String nameForSearch) {
		this.nameForSearch = nameForSearch;
	}

	public String getNameForSearchNative() {
		return nameForSearchNative;
	}

	public void setNameForSearchNative(String nameForSearchNative) {
		this.nameForSearchNative = nameForSearchNative;
	}

	public String getMarketId() {
		return marketId;
	}

	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public boolean isOwnCount() {
		return ownCount;
	}

	public void setOwnCount(boolean ownCount) {
		this.ownCount = ownCount;
	}

	public boolean isDealCount() {
		return dealCount;
	}

	public void setDealCount(boolean dealCount) {
		this.dealCount = dealCount;
	}

	public boolean isGroupImportant() {
		return groupImportant;
	}

	public void setGroupImportant(boolean groupImportant) {
		this.groupImportant = groupImportant;
	}

	public boolean isGroupNew() {
		return groupNew;
	}

	public void setGroupNew(boolean groupNew) {
		this.groupNew = groupNew;
	}

	public boolean isGroupDeepPlowing() {
		return groupDeepPlowing;
	}

	public void setGroupDeepPlowing(boolean groupDeepPlowing) {
		this.groupDeepPlowing = groupDeepPlowing;
	}

	public boolean isGroupYCp() {
		return groupYCp;
	}

	public void setGroupYCp(boolean groupYCp) {
		this.groupYCp = groupYCp;
	}

	public boolean isGroupOtherCp() {
		return groupOtherCp;
	}

	public void setGroupOtherCp(boolean groupOtherCp) {
		this.groupOtherCp = groupOtherCp;
	}

	public boolean isGroupOne() {
		return groupOne;
	}

	public void setGroupOne(boolean groupOne) {
		this.groupOne = groupOne;
	}

	public boolean isGroupTwo() {
		return groupTwo;
	}

	public void setGroupTwo(boolean groupTwo) {
		this.groupTwo = groupTwo;
	}

	public boolean isGroupThree() {
		return groupThree;
	}

	public void setGroupThree(boolean groupThree) {
		this.groupThree = groupThree;
	}

	public boolean isGroupFour() {
		return groupFour;
	}

	public void setGroupFour(boolean groupFour) {
		this.groupFour = groupFour;
	}

	public boolean isGroupFive() {
		return groupFive;
	}

	public void setGroupFive(boolean groupFive) {
		this.groupFive = groupFive;
	}

	public String getSalesRank() {
		return salesRank;
	}

	public void setSalesRank(String salesRank) {
		this.salesRank = salesRank;
	}

	public BigDecimal getDemandNumber() {
		return demandNumber;
	}

	public void setDemandNumber(BigDecimal demandNumber) {
		this.demandNumber = demandNumber;
	}

	public BigDecimal getIssYh() {
		return issYh;
	}

	public void setIssYh(BigDecimal issYh) {
		this.issYh = issYh;
	}

	public BigDecimal getIssBs() {
		return issBs;
	}

	public void setIssBs(BigDecimal issBs) {
		this.issBs = issBs;
	}

	public BigDecimal getIssDl() {
		return issDl;
	}

	public void setIssDl(BigDecimal issDl) {
		this.issDl = issDl;
	}

	public BigDecimal getIssTy() {
		return issTy;
	}

	public void setIssTy(BigDecimal issTy) {
		this.issTy = issTy;
	}

	public BigDecimal getIssGy() {
		return issGy;
	}

	public void setIssGy(BigDecimal issGy) {
		this.issGy = issGy;
	}

	public BigDecimal getIssMi() {
		return issMi;
	}

	public void setIssMi(BigDecimal issMi) {
		this.issMi = issMi;
	}

	public BigDecimal getIssPb() {
		return issPb;
	}

	public void setIssPb(BigDecimal issPb) {
		this.issPb = issPb;
	}

	public BigDecimal getIssOther() {
		return issOther;
	}

	public void setIssOther(BigDecimal issOther) {
		this.issOther = issOther;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getLtrSummerSales() {
		return ltrSummerSales;
	}

	public void setLtrSummerSales(BigDecimal ltrSummerSales) {
		this.ltrSummerSales = ltrSummerSales;
	}

	public BigDecimal getLtrSummerMargin() {
		return ltrSummerMargin;
	}

	public void setLtrSummerMargin(BigDecimal ltrSummerMargin) {
		this.ltrSummerMargin = ltrSummerMargin;
	}

	public BigDecimal getLtrSummerHqMargin() {
		return ltrSummerHqMargin;
	}

	public void setLtrSummerHqMargin(BigDecimal ltrSummerHqMargin) {
		this.ltrSummerHqMargin = ltrSummerHqMargin;
	}

	public BigDecimal getLtrSummerNumber() {
		return ltrSummerNumber;
	}

	public void setLtrSummerNumber(BigDecimal ltrSummerNumber) {
		this.ltrSummerNumber = ltrSummerNumber;
	}

	public BigDecimal getLtrSnowSales() {
		return ltrSnowSales;
	}

	public void setLtrSnowSales(BigDecimal ltrSnowSales) {
		this.ltrSnowSales = ltrSnowSales;
	}

	public BigDecimal getLtrSnowMargin() {
		return ltrSnowMargin;
	}

	public void setLtrSnowMargin(BigDecimal ltrSnowMargin) {
		this.ltrSnowMargin = ltrSnowMargin;
	}

	public BigDecimal getLtrSnowHqMargin() {
		return ltrSnowHqMargin;
	}

	public void setLtrSnowHqMargin(BigDecimal ltrSnowHqMargin) {
		this.ltrSnowHqMargin = ltrSnowHqMargin;
	}

	public BigDecimal getLtrSnowNumber() {
		return ltrSnowNumber;
	}

	public void setLtrSnowNumber(BigDecimal ltrSnowNumber) {
		this.ltrSnowNumber = ltrSnowNumber;
	}

}
