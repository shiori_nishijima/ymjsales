package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class KindReserveStocksDto {

	private String type;
	private BigDecimal number;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

}
