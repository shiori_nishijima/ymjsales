package jp.co.yrc.mv.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class WeekAccountCallDto {
	private String groupType;
	private Date dateStart;
	private String ampm;
	private String accountName;
	private BigDecimal callCount;
	private BigDecimal plannedCount;

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getPlannedCount() {
		return plannedCount;
	}

	public void setPlannedCount(BigDecimal plannedCount) {
		this.plannedCount = plannedCount;
	}

	public String getAmpm() {
		return ampm;
	}

	public void setAmpm(String ampm) {
		this.ampm = ampm;
	}
}
