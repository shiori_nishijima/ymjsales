package jp.co.yrc.mv.dto;

import jp.co.yrc.mv.entity.Calls;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CallsDto extends Calls {
	String callId;
	String accountsName;
	int callsLikeCount;
	int callsReadCount;
	boolean callsRead;
	int commentReadCount;
	String assignedUserFirstName;
	String assignedUserLastName;
	String callsLikeId;
	String eigyoSosikiNm;
	int commentCount;
	boolean file1;
	boolean file2;
	boolean file3;
	boolean marketInfo;
	String accountsSalesCompanyCode;
	String accountsDepartmentCode;
	String accountsSalesOfficeCode;
	// 技サ訪問実績の場合のみ取得するのでデフォルトにブランクを設定
	String createdUserFirstName = "";
	String createdUserLastName = "";

	boolean commentEntered;

	boolean exsistsTech;

	String marketId;
	String marketName;
	
	/** 社内同行者 */
	String companionNames;
	/** 日 */
	int date;

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getAccountsName() {
		return accountsName;
	}

	public void setAccountsName(String accountsName) {
		this.accountsName = accountsName;
	}

	public int getCallsLikeCount() {
		return callsLikeCount;
	}

	public void setCallsLikeCount(int callsLikeCount) {
		this.callsLikeCount = callsLikeCount;
	}

	public int getCallsReadCount() {
		return callsReadCount;
	}

	public void setCallsReadCount(int callsReadCount) {
		this.callsReadCount = callsReadCount;
	}

	public int getCommentReadCount() {
		return commentReadCount;
	}

	public void setCommentReadCount(int commentReadCount) {
		this.commentReadCount = commentReadCount;
	}

	public String getAssignedUserFirstName() {
		return assignedUserFirstName;
	}

	public void setAssignedUserFirstName(String assignedUserFirstName) {
		this.assignedUserFirstName = assignedUserFirstName;
	}

	public String getAssignedUserLastName() {
		return assignedUserLastName;
	}

	public void setAssignedUserLastName(String assignedUserLastName) {
		this.assignedUserLastName = assignedUserLastName;
	}

	public String getCallsLikeId() {
		return callsLikeId;
	}

	public void setCallsLikeId(String callsLikeId) {
		this.callsLikeId = callsLikeId;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public boolean isCommentEntered() {
		return commentEntered;
	}

	public void setCommentEntered(boolean commentEntered) {
		this.commentEntered = commentEntered;
	}

	public boolean isCallsRead() {
		return callsRead;
	}

	public void setCallsRead(boolean callsRead) {
		this.callsRead = callsRead;
	}

	public boolean isFile1() {
		return file1;
	}

	public void setFile1(boolean file1) {
		this.file1 = file1;
	}

	public boolean isFile2() {
		return file2;
	}

	public void setFile2(boolean file2) {
		this.file2 = file2;
	}

	public boolean isFile3() {
		return file3;
	}

	public void setFile3(boolean file3) {
		this.file3 = file3;
	}

	public boolean isMarketInfo() {
		return marketInfo;
	}

	public void setMarketInfo(boolean marketInfo) {
		this.marketInfo = marketInfo;
	}

	public String getAccountsSalesCompanyCode() {
		return accountsSalesCompanyCode;
	}

	public void setAccountsSalesCompanyCode(String accountsSalesCompanyCode) {
		this.accountsSalesCompanyCode = accountsSalesCompanyCode;
	}

	public String getAccountsDepartmentCode() {
		return accountsDepartmentCode;
	}

	public void setAccountsDepartmentCode(String accountsDepartmentCode) {
		this.accountsDepartmentCode = accountsDepartmentCode;
	}

	public String getAccountsSalesOfficeCode() {
		return accountsSalesOfficeCode;
	}

	public void setAccountsSalesOfficeCode(String accountsSalesOfficeCode) {
		this.accountsSalesOfficeCode = accountsSalesOfficeCode;
	}

	public boolean isExsistsTech() {
		return exsistsTech;
	}

	public void setExsistsTech(boolean exsistsTech) {
		this.exsistsTech = exsistsTech;
	}

	public String getMarketId() {
		return marketId;
	}

	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public String getCompanionNames() {
		return companionNames;
	}

	public void setCompanionNames(String companionNames) {
		this.companionNames = companionNames;
	}

	public String getCreatedUserFirstName() {
		return createdUserFirstName;
	}

	public void setCreatedUserFirstName(String createdUserFirstName) {
		this.createdUserFirstName = createdUserFirstName;
	}

	public String getCreatedUserLastName() {
		return createdUserLastName;
	}

	public void setCreatedUserLastName(String createdUserLastName) {
		this.createdUserLastName = createdUserLastName;
	}

}
