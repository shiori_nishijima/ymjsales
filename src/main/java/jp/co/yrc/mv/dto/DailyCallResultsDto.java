package jp.co.yrc.mv.dto;

import jp.co.yrc.mv.entity.MonthlyMarketCalls;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class DailyCallResultsDto extends MonthlyMarketCalls {

	Integer date;

	public Integer getDate() {
		return date;
	}

	public void setDate(Integer date) {
		this.date = date;
	}

}
