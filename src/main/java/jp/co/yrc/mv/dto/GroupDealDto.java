package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import jp.co.yrc.mv.entity.MonthlyGroupDeals;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class GroupDealDto extends MonthlyGroupDeals {
	/** Aランク取引軒数 */
	@Column(name = "rank_a_deal_plan_count")
	BigDecimal rankADealPlanCount;

	/** Bランク取引軒数 */
	@Column(name = "rank_b_deal_plan_count")
	BigDecimal rankBDealPlanCount;

	/** Cランク取引軒数 */
	@Column(name = "rank_c_deal_plan_count")
	BigDecimal rankCDealPlanCount;

	/** 重点取引軒数 */
	@Column(name = "important_deal_plan_count")
	BigDecimal importantDealPlanCount;

	/** 新規取引軒数 */
	@Column(name = "new_deal_plan_count")
	BigDecimal newDealPlanCount;

	/** 深耕取引軒数 */
	@Column(name = "deep_plowing_deal_plan_count")
	BigDecimal deepPlowingDealPlanCount;

	/** Y-CP取引軒数 */
	@Column(name = "y_cp_deal_plan_count")
	BigDecimal yCpDealPlanCount;

	/** 他社CP取引軒数 */
	@Column(name = "other_cp_deal_plan_count")
	BigDecimal otherCpDealPlanCount;

	/** グループ1取引数 */
	@Column(name = "group_one_deal_plan_count")
	BigDecimal groupOneDealPlanCount;

	/** グループ2取引軒数 */
	@Column(name = "group_two_deal_plan_count")
	BigDecimal groupTwoDealPlanCount;

	/** グループ3取引軒数 */
	@Column(name = "group_three_deal_plan_count")
	BigDecimal groupThreeDealPlanCount;

	/** グループ4取引軒数 */
	@Column(name = "group_four_deal_plan_count")
	BigDecimal groupFourDealPlanCount;

	/** グループ5取引数 */
	@Column(name = "group_five_deal_plan_count")
	BigDecimal groupFiveDealPlanCount;

	public BigDecimal getRankADealPlanCount() {
		return rankADealPlanCount;
	}

	public void setRankADealPlanCount(BigDecimal rankADealPlanCount) {
		this.rankADealPlanCount = rankADealPlanCount;
	}

	public BigDecimal getRankBDealPlanCount() {
		return rankBDealPlanCount;
	}

	public void setRankBDealPlanCount(BigDecimal rankBDealPlanCount) {
		this.rankBDealPlanCount = rankBDealPlanCount;
	}

	public BigDecimal getRankCDealPlanCount() {
		return rankCDealPlanCount;
	}

	public void setRankCDealPlanCount(BigDecimal rankCDealPlanCount) {
		this.rankCDealPlanCount = rankCDealPlanCount;
	}

	public BigDecimal getImportantDealPlanCount() {
		return importantDealPlanCount;
	}

	public void setImportantDealPlanCount(BigDecimal importantDealPlanCount) {
		this.importantDealPlanCount = importantDealPlanCount;
	}

	public BigDecimal getNewDealPlanCount() {
		return newDealPlanCount;
	}

	public void setNewDealPlanCount(BigDecimal newDealPlanCount) {
		this.newDealPlanCount = newDealPlanCount;
	}

	public BigDecimal getDeepPlowingDealPlanCount() {
		return deepPlowingDealPlanCount;
	}

	public void setDeepPlowingDealPlanCount(BigDecimal deepPlowingDealPlanCount) {
		this.deepPlowingDealPlanCount = deepPlowingDealPlanCount;
	}

	public BigDecimal getyCpDealPlanCount() {
		return yCpDealPlanCount;
	}

	public void setyCpDealPlanCount(BigDecimal yCpDealPlanCount) {
		this.yCpDealPlanCount = yCpDealPlanCount;
	}

	public BigDecimal getOtherCpDealPlanCount() {
		return otherCpDealPlanCount;
	}

	public void setOtherCpDealPlanCount(BigDecimal otherCpDealPlanCount) {
		this.otherCpDealPlanCount = otherCpDealPlanCount;
	}

	public BigDecimal getGroupOneDealPlanCount() {
		return groupOneDealPlanCount;
	}

	public void setGroupOneDealPlanCount(BigDecimal groupOneDealPlanCount) {
		this.groupOneDealPlanCount = groupOneDealPlanCount;
	}

	public BigDecimal getGroupTwoDealPlanCount() {
		return groupTwoDealPlanCount;
	}

	public void setGroupTwoDealPlanCount(BigDecimal groupTwoDealPlanCount) {
		this.groupTwoDealPlanCount = groupTwoDealPlanCount;
	}

	public BigDecimal getGroupThreeDealPlanCount() {
		return groupThreeDealPlanCount;
	}

	public void setGroupThreeDealPlanCount(BigDecimal groupThreeDealPlanCount) {
		this.groupThreeDealPlanCount = groupThreeDealPlanCount;
	}

	public BigDecimal getGroupFourDealPlanCount() {
		return groupFourDealPlanCount;
	}

	public void setGroupFourDealPlanCount(BigDecimal groupFourDealPlanCount) {
		this.groupFourDealPlanCount = groupFourDealPlanCount;
	}

	public BigDecimal getGroupFiveDealPlanCount() {
		return groupFiveDealPlanCount;
	}

	public void setGroupFiveDealPlanCount(BigDecimal groupFiveDealPlanCount) {
		this.groupFiveDealPlanCount = groupFiveDealPlanCount;
	}

}
