package jp.co.yrc.mv.dto;

import jp.co.yrc.mv.entity.Users;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class UserSosikiDto extends Users {

	String div1;

	String div2;

	String div3;

	String eigyoSosikiNm;

	public String getDiv1() {
		return div1;
	}

	public void setDiv1(String div1) {
		this.div1 = div1;
	}

	public String getDiv2() {
		return div2;
	}

	public void setDiv2(String div2) {
		this.div2 = div2;
	}

	public String getDiv3() {
		return div3;
	}

	public void setDiv3(String div3) {
		this.div3 = div3;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

}
