package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

public class SimpleResult {

	BigDecimal margin;
	BigDecimal hqMargin;
	BigDecimal sales;
	BigDecimal number;

	public SimpleResult(BigDecimal margin, BigDecimal hqMargin, BigDecimal sales, BigDecimal number) {
		super();
		this.margin = margin;
		this.hqMargin = hqMargin;
		this.sales = sales;
		this.number = number;
	}

	public SimpleResult() {

	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getHqMargin() {
		return hqMargin;
	}

	public void setHqMargin(BigDecimal hqMargin) {
		this.hqMargin = hqMargin;
	}

}