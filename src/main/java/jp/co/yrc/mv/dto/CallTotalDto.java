package jp.co.yrc.mv.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class CallTotalDto {

	/** 年 */
	Integer year;

	/** 月 */
	Integer month;

	/** ユーザーID */
	String userId;

	/** 所属組織 */
	String eigyoSosikiCd;

	/** SS持軒数 */
	BigDecimal ssOwnCount;

	/** SS取引軒数 */
	BigDecimal ssDealCount;

	/** SS訪問数 */
	BigDecimal ssCallCount;

	/** SS情報数 */
	BigDecimal ssInfoCount;

	/** SS取引得M数 */
	BigDecimal ssAccountMasterCount;

	/** RS持軒数 */
	BigDecimal rsOwnCount;

	/** RS取引軒数 */
	BigDecimal rsDealCount;

	/** RS訪問数 */
	BigDecimal rsCallCount;

	/** RS取引得M数 */
	BigDecimal rsAccountMasterCount;

	BigDecimal rsInfoCount;

	/** CD持軒数 */
	BigDecimal cdOwnCount;

	/** CD取引軒数 */
	BigDecimal cdDealCount;

	/** CD訪問数 */
	BigDecimal cdCallCount;

	/** CD取引得M数 */
	BigDecimal cdAccountMasterCount;

	BigDecimal cdInfoCount;

	/** PS持軒数 */
	BigDecimal psOwnCount;

	/** PS取引軒数 */
	BigDecimal psDealCount;

	/** PS訪問数 */
	BigDecimal psCallCount;

	/** PS情報数 */
	BigDecimal psInfoCount;

	/** PS取引得M数 */
	BigDecimal psAccountMasterCount;

	/** CS持軒数 */
	BigDecimal csOwnCount;

	/** CS取引軒数 */
	BigDecimal csDealCount;

	/** CS訪問数 */
	BigDecimal csCallCount;

	/** CS情報数 */
	BigDecimal csInfoCount;

	/** CS取引得M数 */
	BigDecimal csAccountMasterCount;

	/** HC持軒数 */
	BigDecimal hcOwnCount;

	/** HC取引軒数 */
	BigDecimal hcDealCount;

	/** HC訪問数 */
	BigDecimal hcCallCount;

	/** HC情報数 */
	BigDecimal hcInfoCount;

	/** HC取引得M数 */
	BigDecimal hcAccountMasterCount;

	/** リース持軒数 */
	BigDecimal leaseOwnCount;

	/** リース取引軒数 */
	BigDecimal leaseDealCount;

	/** リース訪問数 */
	BigDecimal leaseCallCount;

	/** リース情報数 */
	BigDecimal leaseInfoCount;

	/** リース取引得M数 */
	BigDecimal leaseAccountMasterCount;

	/** 間他持軒数 */
	BigDecimal indirectOtherOwnCount;

	/** 間他取引軒数 */
	BigDecimal indirectOtherDealCount;

	/** 間他訪問数 */
	BigDecimal indirectOtherCallCount;

	/** 間他情報数 */
	BigDecimal indirectOtherInfoCount;

	/** 間他取引得M数 */
	BigDecimal indirectOtherAccountMasterCount;

	/** 担当外訪問数 */
	BigDecimal outsideCallCount;

	/** 担当外情報軒数 */
	BigDecimal outsideInfoCount;

	/** トラック持軒数 */
	BigDecimal truckOwnCount;

	/** トラック取引軒数 */
	BigDecimal truckDealCount;

	/** トラック訪問数 */
	BigDecimal truckCallCount;

	/** トラック情報数 */
	BigDecimal truckInfoCount;

	/** トラック取引得M数 */
	BigDecimal truckAccountMasterCount;

	/** バス持軒数 */
	BigDecimal busOwnCount;

	/** バス取引軒数 */
	BigDecimal busDealCount;

	/** バス訪問数 */
	BigDecimal busCallCount;

	/** バス情報数 */
	BigDecimal busInfoCount;

	/** バス取引得M数 */
	BigDecimal busAccountMasterCount;

	/** ハイタク持軒数 */
	BigDecimal hireTaxiOwnCount;

	/** ハイタク取引軒数 */
	BigDecimal hireTaxiDealCount;

	/** ハイタク訪問数 */
	BigDecimal hireTaxiCallCount;

	/** ハイタク情報数 */
	BigDecimal hireTaxiInfoCount;

	/** ハイタク取引得M数 */
	BigDecimal hireTaxiAccountMasterCount;

	/** 直他持軒数 */
	BigDecimal directOtherOwnCount;

	/** 直他取引軒数 */
	BigDecimal directOtherDealCount;

	/** 直他訪問数 */
	BigDecimal directOtherCallCount;

	/** 直他情報数 */
	BigDecimal directOtherInfoCount;

	/** 直他取引得M数 */
	BigDecimal directOtherAccountMasterCount;

	/** 小売持軒数 */
	BigDecimal retailOwnCount;

	/** 小売取引軒数 */
	BigDecimal retailDealCount;

	/** 小売訪問数 */
	BigDecimal retailCallCount;

	/** 小売情報数 */
	BigDecimal retailInfoCount;

	/** 小売取引得M数 */
	BigDecimal retailAccountMasterCount;

	/** 入力日 */
	Timestamp dateEntered;

	/** 更新日 */
	Timestamp dateModified;

	/** 更新者 */
	String modifiedUserId;

	/** 作成者 */
	String createdBy;

	BigDecimal ownPlanCount;

	/**
	 * Returns the year.
	 * 
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Sets the year.
	 * 
	 * @param year
	 *            the year
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * Returns the month.
	 * 
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * Sets the month.
	 * 
	 * @param month
	 *            the month
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            the userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Returns the eigyoSosikiCd.
	 * 
	 * @return the eigyoSosikiCd
	 */
	public String getEigyoSosikiCd() {
		return eigyoSosikiCd;
	}

	/**
	 * Sets the eigyoSosikiCd.
	 * 
	 * @param eigyoSosikiCd
	 *            the eigyoSosikiCd
	 */
	public void setEigyoSosikiCd(String eigyoSosikiCd) {
		this.eigyoSosikiCd = eigyoSosikiCd;
	}

	/**
	 * Returns the ssOwnCount.
	 * 
	 * @return the ssOwnCount
	 */
	public BigDecimal getSsOwnCount() {
		return ssOwnCount;
	}

	/**
	 * Sets the ssOwnCount.
	 * 
	 * @param ssOwnCount
	 *            the ssOwnCount
	 */
	public void setSsOwnCount(BigDecimal ssOwnCount) {
		this.ssOwnCount = ssOwnCount;
	}

	/**
	 * Returns the ssDealCount.
	 * 
	 * @return the ssDealCount
	 */
	public BigDecimal getSsDealCount() {
		return ssDealCount;
	}

	/**
	 * Sets the ssDealCount.
	 * 
	 * @param ssDealCount
	 *            the ssDealCount
	 */
	public void setSsDealCount(BigDecimal ssDealCount) {
		this.ssDealCount = ssDealCount;
	}

	/**
	 * Returns the ssCallCount.
	 * 
	 * @return the ssCallCount
	 */
	public BigDecimal getSsCallCount() {
		return ssCallCount;
	}

	/**
	 * Sets the ssCallCount.
	 * 
	 * @param ssCallCount
	 *            the ssCallCount
	 */
	public void setSsCallCount(BigDecimal ssCallCount) {
		this.ssCallCount = ssCallCount;
	}

	/**
	 * Returns the ssInfoCount.
	 * 
	 * @return the ssInfoCount
	 */
	public BigDecimal getSsInfoCount() {
		return ssInfoCount;
	}

	/**
	 * Sets the ssInfoCount.
	 * 
	 * @param ssInfoCount
	 *            the ssInfoCount
	 */
	public void setSsInfoCount(BigDecimal ssInfoCount) {
		this.ssInfoCount = ssInfoCount;
	}

	/**
	 * Returns the ssAccountMasterCount.
	 * 
	 * @return the ssAccountMasterCount
	 */
	public BigDecimal getSsAccountMasterCount() {
		return ssAccountMasterCount;
	}

	/**
	 * Sets the ssAccountMasterCount.
	 * 
	 * @param ssAccountMasterCount
	 *            the ssAccountMasterCount
	 */
	public void setSsAccountMasterCount(BigDecimal ssAccountMasterCount) {
		this.ssAccountMasterCount = ssAccountMasterCount;
	}

	/**
	 * Returns the rsOwnCount.
	 * 
	 * @return the rsOwnCount
	 */
	public BigDecimal getRsOwnCount() {
		return rsOwnCount;
	}

	/**
	 * Sets the rsOwnCount.
	 * 
	 * @param rsOwnCount
	 *            the rsOwnCount
	 */
	public void setRsOwnCount(BigDecimal rsOwnCount) {
		this.rsOwnCount = rsOwnCount;
	}

	/**
	 * Returns the rsDealCount.
	 * 
	 * @return the rsDealCount
	 */
	public BigDecimal getRsDealCount() {
		return rsDealCount;
	}

	/**
	 * Sets the rsDealCount.
	 * 
	 * @param rsDealCount
	 *            the rsDealCount
	 */
	public void setRsDealCount(BigDecimal rsDealCount) {
		this.rsDealCount = rsDealCount;
	}

	/**
	 * Returns the rsCallCount.
	 * 
	 * @return the rsCallCount
	 */
	public BigDecimal getRsCallCount() {
		return rsCallCount;
	}

	/**
	 * Sets the rsCallCount.
	 * 
	 * @param rsCallCount
	 *            the rsCallCount
	 */
	public void setRsCallCount(BigDecimal rsCallCount) {
		this.rsCallCount = rsCallCount;
	}

	/**
	 * Returns the rsAccountMasterCount.
	 * 
	 * @return the rsAccountMasterCount
	 */
	public BigDecimal getRsAccountMasterCount() {
		return rsAccountMasterCount;
	}

	/**
	 * Sets the rsAccountMasterCount.
	 * 
	 * @param rsAccountMasterCount
	 *            the rsAccountMasterCount
	 */
	public void setRsAccountMasterCount(BigDecimal rsAccountMasterCount) {
		this.rsAccountMasterCount = rsAccountMasterCount;
	}

	/**
	 * Returns the cdOwnCount.
	 * 
	 * @return the cdOwnCount
	 */
	public BigDecimal getCdOwnCount() {
		return cdOwnCount;
	}

	/**
	 * Sets the cdOwnCount.
	 * 
	 * @param cdOwnCount
	 *            the cdOwnCount
	 */
	public void setCdOwnCount(BigDecimal cdOwnCount) {
		this.cdOwnCount = cdOwnCount;
	}

	/**
	 * Returns the cdDealCount.
	 * 
	 * @return the cdDealCount
	 */
	public BigDecimal getCdDealCount() {
		return cdDealCount;
	}

	/**
	 * Sets the cdDealCount.
	 * 
	 * @param cdDealCount
	 *            the cdDealCount
	 */
	public void setCdDealCount(BigDecimal cdDealCount) {
		this.cdDealCount = cdDealCount;
	}

	/**
	 * Returns the cdCallCount.
	 * 
	 * @return the cdCallCount
	 */
	public BigDecimal getCdCallCount() {
		return cdCallCount;
	}

	/**
	 * Sets the cdCallCount.
	 * 
	 * @param cdCallCount
	 *            the cdCallCount
	 */
	public void setCdCallCount(BigDecimal cdCallCount) {
		this.cdCallCount = cdCallCount;
	}

	/**
	 * Returns the cdAccountMasterCount.
	 * 
	 * @return the cdAccountMasterCount
	 */
	public BigDecimal getCdAccountMasterCount() {
		return cdAccountMasterCount;
	}

	/**
	 * Sets the cdAccountMasterCount.
	 * 
	 * @param cdAccountMasterCount
	 *            the cdAccountMasterCount
	 */
	public void setCdAccountMasterCount(BigDecimal cdAccountMasterCount) {
		this.cdAccountMasterCount = cdAccountMasterCount;
	}

	/**
	 * Returns the psOwnCount.
	 * 
	 * @return the psOwnCount
	 */
	public BigDecimal getPsOwnCount() {
		return psOwnCount;
	}

	/**
	 * Sets the psOwnCount.
	 * 
	 * @param psOwnCount
	 *            the psOwnCount
	 */
	public void setPsOwnCount(BigDecimal psOwnCount) {
		this.psOwnCount = psOwnCount;
	}

	/**
	 * Returns the psDealCount.
	 * 
	 * @return the psDealCount
	 */
	public BigDecimal getPsDealCount() {
		return psDealCount;
	}

	/**
	 * Sets the psDealCount.
	 * 
	 * @param psDealCount
	 *            the psDealCount
	 */
	public void setPsDealCount(BigDecimal psDealCount) {
		this.psDealCount = psDealCount;
	}

	/**
	 * Returns the psCallCount.
	 * 
	 * @return the psCallCount
	 */
	public BigDecimal getPsCallCount() {
		return psCallCount;
	}

	/**
	 * Sets the psCallCount.
	 * 
	 * @param psCallCount
	 *            the psCallCount
	 */
	public void setPsCallCount(BigDecimal psCallCount) {
		this.psCallCount = psCallCount;
	}

	/**
	 * Returns the psInfoCount.
	 * 
	 * @return the psInfoCount
	 */
	public BigDecimal getPsInfoCount() {
		return psInfoCount;
	}

	/**
	 * Sets the psInfoCount.
	 * 
	 * @param psInfoCount
	 *            the psInfoCount
	 */
	public void setPsInfoCount(BigDecimal psInfoCount) {
		this.psInfoCount = psInfoCount;
	}

	/**
	 * Returns the psAccountMasterCount.
	 * 
	 * @return the psAccountMasterCount
	 */
	public BigDecimal getPsAccountMasterCount() {
		return psAccountMasterCount;
	}

	/**
	 * Sets the psAccountMasterCount.
	 * 
	 * @param psAccountMasterCount
	 *            the psAccountMasterCount
	 */
	public void setPsAccountMasterCount(BigDecimal psAccountMasterCount) {
		this.psAccountMasterCount = psAccountMasterCount;
	}

	/**
	 * Returns the csOwnCount.
	 * 
	 * @return the csOwnCount
	 */
	public BigDecimal getCsOwnCount() {
		return csOwnCount;
	}

	/**
	 * Sets the csOwnCount.
	 * 
	 * @param csOwnCount
	 *            the csOwnCount
	 */
	public void setCsOwnCount(BigDecimal csOwnCount) {
		this.csOwnCount = csOwnCount;
	}

	/**
	 * Returns the csDealCount.
	 * 
	 * @return the csDealCount
	 */
	public BigDecimal getCsDealCount() {
		return csDealCount;
	}

	/**
	 * Sets the csDealCount.
	 * 
	 * @param csDealCount
	 *            the csDealCount
	 */
	public void setCsDealCount(BigDecimal csDealCount) {
		this.csDealCount = csDealCount;
	}

	/**
	 * Returns the csCallCount.
	 * 
	 * @return the csCallCount
	 */
	public BigDecimal getCsCallCount() {
		return csCallCount;
	}

	/**
	 * Sets the csCallCount.
	 * 
	 * @param csCallCount
	 *            the csCallCount
	 */
	public void setCsCallCount(BigDecimal csCallCount) {
		this.csCallCount = csCallCount;
	}

	/**
	 * Returns the csInfoCount.
	 * 
	 * @return the csInfoCount
	 */
	public BigDecimal getCsInfoCount() {
		return csInfoCount;
	}

	/**
	 * Sets the csInfoCount.
	 * 
	 * @param csInfoCount
	 *            the csInfoCount
	 */
	public void setCsInfoCount(BigDecimal csInfoCount) {
		this.csInfoCount = csInfoCount;
	}

	/**
	 * Returns the csAccountMasterCount.
	 * 
	 * @return the csAccountMasterCount
	 */
	public BigDecimal getCsAccountMasterCount() {
		return csAccountMasterCount;
	}

	/**
	 * Sets the csAccountMasterCount.
	 * 
	 * @param csAccountMasterCount
	 *            the csAccountMasterCount
	 */
	public void setCsAccountMasterCount(BigDecimal csAccountMasterCount) {
		this.csAccountMasterCount = csAccountMasterCount;
	}

	/**
	 * Returns the hcOwnCount.
	 * 
	 * @return the hcOwnCount
	 */
	public BigDecimal getHcOwnCount() {
		return hcOwnCount;
	}

	/**
	 * Sets the hcOwnCount.
	 * 
	 * @param hcOwnCount
	 *            the hcOwnCount
	 */
	public void setHcOwnCount(BigDecimal hcOwnCount) {
		this.hcOwnCount = hcOwnCount;
	}

	/**
	 * Returns the hcDealCount.
	 * 
	 * @return the hcDealCount
	 */
	public BigDecimal getHcDealCount() {
		return hcDealCount;
	}

	/**
	 * Sets the hcDealCount.
	 * 
	 * @param hcDealCount
	 *            the hcDealCount
	 */
	public void setHcDealCount(BigDecimal hcDealCount) {
		this.hcDealCount = hcDealCount;
	}

	/**
	 * Returns the hcCallCount.
	 * 
	 * @return the hcCallCount
	 */
	public BigDecimal getHcCallCount() {
		return hcCallCount;
	}

	/**
	 * Sets the hcCallCount.
	 * 
	 * @param hcCallCount
	 *            the hcCallCount
	 */
	public void setHcCallCount(BigDecimal hcCallCount) {
		this.hcCallCount = hcCallCount;
	}

	/**
	 * Returns the hcInfoCount.
	 * 
	 * @return the hcInfoCount
	 */
	public BigDecimal getHcInfoCount() {
		return hcInfoCount;
	}

	/**
	 * Sets the hcInfoCount.
	 * 
	 * @param hcInfoCount
	 *            the hcInfoCount
	 */
	public void setHcInfoCount(BigDecimal hcInfoCount) {
		this.hcInfoCount = hcInfoCount;
	}

	/**
	 * Returns the hcAccountMasterCount.
	 * 
	 * @return the hcAccountMasterCount
	 */
	public BigDecimal getHcAccountMasterCount() {
		return hcAccountMasterCount;
	}

	/**
	 * Sets the hcAccountMasterCount.
	 * 
	 * @param hcAccountMasterCount
	 *            the hcAccountMasterCount
	 */
	public void setHcAccountMasterCount(BigDecimal hcAccountMasterCount) {
		this.hcAccountMasterCount = hcAccountMasterCount;
	}

	/**
	 * Returns the leaseOwnCount.
	 * 
	 * @return the leaseOwnCount
	 */
	public BigDecimal getLeaseOwnCount() {
		return leaseOwnCount;
	}

	/**
	 * Sets the leaseOwnCount.
	 * 
	 * @param leaseOwnCount
	 *            the leaseOwnCount
	 */
	public void setLeaseOwnCount(BigDecimal leaseOwnCount) {
		this.leaseOwnCount = leaseOwnCount;
	}

	/**
	 * Returns the leaseDealCount.
	 * 
	 * @return the leaseDealCount
	 */
	public BigDecimal getLeaseDealCount() {
		return leaseDealCount;
	}

	/**
	 * Sets the leaseDealCount.
	 * 
	 * @param leaseDealCount
	 *            the leaseDealCount
	 */
	public void setLeaseDealCount(BigDecimal leaseDealCount) {
		this.leaseDealCount = leaseDealCount;
	}

	/**
	 * Returns the leaseCallCount.
	 * 
	 * @return the leaseCallCount
	 */
	public BigDecimal getLeaseCallCount() {
		return leaseCallCount;
	}

	/**
	 * Sets the leaseCallCount.
	 * 
	 * @param leaseCallCount
	 *            the leaseCallCount
	 */
	public void setLeaseCallCount(BigDecimal leaseCallCount) {
		this.leaseCallCount = leaseCallCount;
	}

	/**
	 * Returns the leaseInfoCount.
	 * 
	 * @return the leaseInfoCount
	 */
	public BigDecimal getLeaseInfoCount() {
		return leaseInfoCount;
	}

	/**
	 * Sets the leaseInfoCount.
	 * 
	 * @param leaseInfoCount
	 *            the leaseInfoCount
	 */
	public void setLeaseInfoCount(BigDecimal leaseInfoCount) {
		this.leaseInfoCount = leaseInfoCount;
	}

	/**
	 * Returns the leaseAccountMasterCount.
	 * 
	 * @return the leaseAccountMasterCount
	 */
	public BigDecimal getLeaseAccountMasterCount() {
		return leaseAccountMasterCount;
	}

	/**
	 * Sets the leaseAccountMasterCount.
	 * 
	 * @param leaseAccountMasterCount
	 *            the leaseAccountMasterCount
	 */
	public void setLeaseAccountMasterCount(BigDecimal leaseAccountMasterCount) {
		this.leaseAccountMasterCount = leaseAccountMasterCount;
	}

	/**
	 * Returns the indirectOtherOwnCount.
	 * 
	 * @return the indirectOtherOwnCount
	 */
	public BigDecimal getIndirectOtherOwnCount() {
		return indirectOtherOwnCount;
	}

	/**
	 * Sets the indirectOtherOwnCount.
	 * 
	 * @param indirectOtherOwnCount
	 *            the indirectOtherOwnCount
	 */
	public void setIndirectOtherOwnCount(BigDecimal indirectOtherOwnCount) {
		this.indirectOtherOwnCount = indirectOtherOwnCount;
	}

	/**
	 * Returns the indirectOtherDealCount.
	 * 
	 * @return the indirectOtherDealCount
	 */
	public BigDecimal getIndirectOtherDealCount() {
		return indirectOtherDealCount;
	}

	/**
	 * Sets the indirectOtherDealCount.
	 * 
	 * @param indirectOtherDealCount
	 *            the indirectOtherDealCount
	 */
	public void setIndirectOtherDealCount(BigDecimal indirectOtherDealCount) {
		this.indirectOtherDealCount = indirectOtherDealCount;
	}

	/**
	 * Returns the indirectOtherCallCount.
	 * 
	 * @return the indirectOtherCallCount
	 */
	public BigDecimal getIndirectOtherCallCount() {
		return indirectOtherCallCount;
	}

	/**
	 * Sets the indirectOtherCallCount.
	 * 
	 * @param indirectOtherCallCount
	 *            the indirectOtherCallCount
	 */
	public void setIndirectOtherCallCount(BigDecimal indirectOtherCallCount) {
		this.indirectOtherCallCount = indirectOtherCallCount;
	}

	/**
	 * Returns the indirectOtherInfoCount.
	 * 
	 * @return the indirectOtherInfoCount
	 */
	public BigDecimal getIndirectOtherInfoCount() {
		return indirectOtherInfoCount;
	}

	/**
	 * Sets the indirectOtherInfoCount.
	 * 
	 * @param indirectOtherInfoCount
	 *            the indirectOtherInfoCount
	 */
	public void setIndirectOtherInfoCount(BigDecimal indirectOtherInfoCount) {
		this.indirectOtherInfoCount = indirectOtherInfoCount;
	}

	/**
	 * Returns the indirectOtherAccountMasterCount.
	 * 
	 * @return the indirectOtherAccountMasterCount
	 */
	public BigDecimal getIndirectOtherAccountMasterCount() {
		return indirectOtherAccountMasterCount;
	}

	/**
	 * Sets the indirectOtherAccountMasterCount.
	 * 
	 * @param indirectOtherAccountMasterCount
	 *            the indirectOtherAccountMasterCount
	 */
	public void setIndirectOtherAccountMasterCount(BigDecimal indirectOtherAccountMasterCount) {
		this.indirectOtherAccountMasterCount = indirectOtherAccountMasterCount;
	}

	/**
	 * Returns the outsideCallCount.
	 * 
	 * @return the outsideCallCount
	 */
	public BigDecimal getOutsideCallCount() {
		return outsideCallCount;
	}

	/**
	 * Sets the outsideCallCount.
	 * 
	 * @param outsideCallCount
	 *            the outsideCallCount
	 */
	public void setOutsideCallCount(BigDecimal outsideCallCount) {
		this.outsideCallCount = outsideCallCount;
	}

	/**
	 * Returns the outsideInfoCount.
	 * 
	 * @return the outsideInfoCount
	 */
	public BigDecimal getOutsideInfoCount() {
		return outsideInfoCount;
	}

	/**
	 * Sets the outsideInfoCount.
	 * 
	 * @param outsideInfoCount
	 *            the outsideInfoCount
	 */
	public void setOutsideInfoCount(BigDecimal outsideInfoCount) {
		this.outsideInfoCount = outsideInfoCount;
	}

	/**
	 * Returns the truckOwnCount.
	 * 
	 * @return the truckOwnCount
	 */
	public BigDecimal getTruckOwnCount() {
		return truckOwnCount;
	}

	/**
	 * Sets the truckOwnCount.
	 * 
	 * @param truckOwnCount
	 *            the truckOwnCount
	 */
	public void setTruckOwnCount(BigDecimal truckOwnCount) {
		this.truckOwnCount = truckOwnCount;
	}

	/**
	 * Returns the truckDealCount.
	 * 
	 * @return the truckDealCount
	 */
	public BigDecimal getTruckDealCount() {
		return truckDealCount;
	}

	/**
	 * Sets the truckDealCount.
	 * 
	 * @param truckDealCount
	 *            the truckDealCount
	 */
	public void setTruckDealCount(BigDecimal truckDealCount) {
		this.truckDealCount = truckDealCount;
	}

	/**
	 * Returns the truckCallCount.
	 * 
	 * @return the truckCallCount
	 */
	public BigDecimal getTruckCallCount() {
		return truckCallCount;
	}

	/**
	 * Sets the truckCallCount.
	 * 
	 * @param truckCallCount
	 *            the truckCallCount
	 */
	public void setTruckCallCount(BigDecimal truckCallCount) {
		this.truckCallCount = truckCallCount;
	}

	/**
	 * Returns the truckInfoCount.
	 * 
	 * @return the truckInfoCount
	 */
	public BigDecimal getTruckInfoCount() {
		return truckInfoCount;
	}

	/**
	 * Sets the truckInfoCount.
	 * 
	 * @param truckInfoCount
	 *            the truckInfoCount
	 */
	public void setTruckInfoCount(BigDecimal truckInfoCount) {
		this.truckInfoCount = truckInfoCount;
	}

	/**
	 * Returns the truckAccountMasterCount.
	 * 
	 * @return the truckAccountMasterCount
	 */
	public BigDecimal getTruckAccountMasterCount() {
		return truckAccountMasterCount;
	}

	/**
	 * Sets the truckAccountMasterCount.
	 * 
	 * @param truckAccountMasterCount
	 *            the truckAccountMasterCount
	 */
	public void setTruckAccountMasterCount(BigDecimal truckAccountMasterCount) {
		this.truckAccountMasterCount = truckAccountMasterCount;
	}

	/**
	 * Returns the busOwnCount.
	 * 
	 * @return the busOwnCount
	 */
	public BigDecimal getBusOwnCount() {
		return busOwnCount;
	}

	/**
	 * Sets the busOwnCount.
	 * 
	 * @param busOwnCount
	 *            the busOwnCount
	 */
	public void setBusOwnCount(BigDecimal busOwnCount) {
		this.busOwnCount = busOwnCount;
	}

	/**
	 * Returns the busDealCount.
	 * 
	 * @return the busDealCount
	 */
	public BigDecimal getBusDealCount() {
		return busDealCount;
	}

	/**
	 * Sets the busDealCount.
	 * 
	 * @param busDealCount
	 *            the busDealCount
	 */
	public void setBusDealCount(BigDecimal busDealCount) {
		this.busDealCount = busDealCount;
	}

	/**
	 * Returns the busCallCount.
	 * 
	 * @return the busCallCount
	 */
	public BigDecimal getBusCallCount() {
		return busCallCount;
	}

	/**
	 * Sets the busCallCount.
	 * 
	 * @param busCallCount
	 *            the busCallCount
	 */
	public void setBusCallCount(BigDecimal busCallCount) {
		this.busCallCount = busCallCount;
	}

	/**
	 * Returns the busInfoCount.
	 * 
	 * @return the busInfoCount
	 */
	public BigDecimal getBusInfoCount() {
		return busInfoCount;
	}

	/**
	 * Sets the busInfoCount.
	 * 
	 * @param busInfoCount
	 *            the busInfoCount
	 */
	public void setBusInfoCount(BigDecimal busInfoCount) {
		this.busInfoCount = busInfoCount;
	}

	/**
	 * Returns the busAccountMasterCount.
	 * 
	 * @return the busAccountMasterCount
	 */
	public BigDecimal getBusAccountMasterCount() {
		return busAccountMasterCount;
	}

	/**
	 * Sets the busAccountMasterCount.
	 * 
	 * @param busAccountMasterCount
	 *            the busAccountMasterCount
	 */
	public void setBusAccountMasterCount(BigDecimal busAccountMasterCount) {
		this.busAccountMasterCount = busAccountMasterCount;
	}

	/**
	 * Returns the hireTaxiOwnCount.
	 * 
	 * @return the hireTaxiOwnCount
	 */
	public BigDecimal getHireTaxiOwnCount() {
		return hireTaxiOwnCount;
	}

	/**
	 * Sets the hireTaxiOwnCount.
	 * 
	 * @param hireTaxiOwnCount
	 *            the hireTaxiOwnCount
	 */
	public void setHireTaxiOwnCount(BigDecimal hireTaxiOwnCount) {
		this.hireTaxiOwnCount = hireTaxiOwnCount;
	}

	/**
	 * Returns the hireTaxiDealCount.
	 * 
	 * @return the hireTaxiDealCount
	 */
	public BigDecimal getHireTaxiDealCount() {
		return hireTaxiDealCount;
	}

	/**
	 * Sets the hireTaxiDealCount.
	 * 
	 * @param hireTaxiDealCount
	 *            the hireTaxiDealCount
	 */
	public void setHireTaxiDealCount(BigDecimal hireTaxiDealCount) {
		this.hireTaxiDealCount = hireTaxiDealCount;
	}

	/**
	 * Returns the hireTaxiCallCount.
	 * 
	 * @return the hireTaxiCallCount
	 */
	public BigDecimal getHireTaxiCallCount() {
		return hireTaxiCallCount;
	}

	/**
	 * Sets the hireTaxiCallCount.
	 * 
	 * @param hireTaxiCallCount
	 *            the hireTaxiCallCount
	 */
	public void setHireTaxiCallCount(BigDecimal hireTaxiCallCount) {
		this.hireTaxiCallCount = hireTaxiCallCount;
	}

	/**
	 * Returns the hireTaxiInfoCount.
	 * 
	 * @return the hireTaxiInfoCount
	 */
	public BigDecimal getHireTaxiInfoCount() {
		return hireTaxiInfoCount;
	}

	/**
	 * Sets the hireTaxiInfoCount.
	 * 
	 * @param hireTaxiInfoCount
	 *            the hireTaxiInfoCount
	 */
	public void setHireTaxiInfoCount(BigDecimal hireTaxiInfoCount) {
		this.hireTaxiInfoCount = hireTaxiInfoCount;
	}

	/**
	 * Returns the hireTaxiAccountMasterCount.
	 * 
	 * @return the hireTaxiAccountMasterCount
	 */
	public BigDecimal getHireTaxiAccountMasterCount() {
		return hireTaxiAccountMasterCount;
	}

	/**
	 * Sets the hireTaxiAccountMasterCount.
	 * 
	 * @param hireTaxiAccountMasterCount
	 *            the hireTaxiAccountMasterCount
	 */
	public void setHireTaxiAccountMasterCount(BigDecimal hireTaxiAccountMasterCount) {
		this.hireTaxiAccountMasterCount = hireTaxiAccountMasterCount;
	}

	/**
	 * Returns the directOtherOwnCount.
	 * 
	 * @return the directOtherOwnCount
	 */
	public BigDecimal getDirectOtherOwnCount() {
		return directOtherOwnCount;
	}

	/**
	 * Sets the directOtherOwnCount.
	 * 
	 * @param directOtherOwnCount
	 *            the directOtherOwnCount
	 */
	public void setDirectOtherOwnCount(BigDecimal directOtherOwnCount) {
		this.directOtherOwnCount = directOtherOwnCount;
	}

	/**
	 * Returns the directOtherDealCount.
	 * 
	 * @return the directOtherDealCount
	 */
	public BigDecimal getDirectOtherDealCount() {
		return directOtherDealCount;
	}

	/**
	 * Sets the directOtherDealCount.
	 * 
	 * @param directOtherDealCount
	 *            the directOtherDealCount
	 */
	public void setDirectOtherDealCount(BigDecimal directOtherDealCount) {
		this.directOtherDealCount = directOtherDealCount;
	}

	/**
	 * Returns the directOtherCallCount.
	 * 
	 * @return the directOtherCallCount
	 */
	public BigDecimal getDirectOtherCallCount() {
		return directOtherCallCount;
	}

	/**
	 * Sets the directOtherCallCount.
	 * 
	 * @param directOtherCallCount
	 *            the directOtherCallCount
	 */
	public void setDirectOtherCallCount(BigDecimal directOtherCallCount) {
		this.directOtherCallCount = directOtherCallCount;
	}

	/**
	 * Returns the directOtherInfoCount.
	 * 
	 * @return the directOtherInfoCount
	 */
	public BigDecimal getDirectOtherInfoCount() {
		return directOtherInfoCount;
	}

	/**
	 * Sets the directOtherInfoCount.
	 * 
	 * @param directOtherInfoCount
	 *            the directOtherInfoCount
	 */
	public void setDirectOtherInfoCount(BigDecimal directOtherInfoCount) {
		this.directOtherInfoCount = directOtherInfoCount;
	}

	/**
	 * Returns the directOtherAccountMasterCount.
	 * 
	 * @return the directOtherAccountMasterCount
	 */
	public BigDecimal getDirectOtherAccountMasterCount() {
		return directOtherAccountMasterCount;
	}

	/**
	 * Sets the directOtherAccountMasterCount.
	 * 
	 * @param directOtherAccountMasterCount
	 *            the directOtherAccountMasterCount
	 */
	public void setDirectOtherAccountMasterCount(BigDecimal directOtherAccountMasterCount) {
		this.directOtherAccountMasterCount = directOtherAccountMasterCount;
	}

	/**
	 * Returns the retailOwnCount.
	 * 
	 * @return the retailOwnCount
	 */
	public BigDecimal getRetailOwnCount() {
		return retailOwnCount;
	}

	/**
	 * Sets the retailOwnCount.
	 * 
	 * @param retailOwnCount
	 *            the retailOwnCount
	 */
	public void setRetailOwnCount(BigDecimal retailOwnCount) {
		this.retailOwnCount = retailOwnCount;
	}

	/**
	 * Returns the retailDealCount.
	 * 
	 * @return the retailDealCount
	 */
	public BigDecimal getRetailDealCount() {
		return retailDealCount;
	}

	/**
	 * Sets the retailDealCount.
	 * 
	 * @param retailDealCount
	 *            the retailDealCount
	 */
	public void setRetailDealCount(BigDecimal retailDealCount) {
		this.retailDealCount = retailDealCount;
	}

	/**
	 * Returns the retailCallCount.
	 * 
	 * @return the retailCallCount
	 */
	public BigDecimal getRetailCallCount() {
		return retailCallCount;
	}

	/**
	 * Sets the retailCallCount.
	 * 
	 * @param retailCallCount
	 *            the retailCallCount
	 */
	public void setRetailCallCount(BigDecimal retailCallCount) {
		this.retailCallCount = retailCallCount;
	}

	/**
	 * Returns the retailInfoCount.
	 * 
	 * @return the retailInfoCount
	 */
	public BigDecimal getRetailInfoCount() {
		return retailInfoCount;
	}

	/**
	 * Sets the retailInfoCount.
	 * 
	 * @param retailInfoCount
	 *            the retailInfoCount
	 */
	public void setRetailInfoCount(BigDecimal retailInfoCount) {
		this.retailInfoCount = retailInfoCount;
	}

	/**
	 * Returns the retailAccountMasterCount.
	 * 
	 * @return the retailAccountMasterCount
	 */
	public BigDecimal getRetailAccountMasterCount() {
		return retailAccountMasterCount;
	}

	/**
	 * Sets the retailAccountMasterCount.
	 * 
	 * @param retailAccountMasterCount
	 *            the retailAccountMasterCount
	 */
	public void setRetailAccountMasterCount(BigDecimal retailAccountMasterCount) {
		this.retailAccountMasterCount = retailAccountMasterCount;
	}

	/**
	 * Returns the dateEntered.
	 * 
	 * @return the dateEntered
	 */
	public Timestamp getDateEntered() {
		return dateEntered;
	}

	/**
	 * Sets the dateEntered.
	 * 
	 * @param dateEntered
	 *            the dateEntered
	 */
	public void setDateEntered(Timestamp dateEntered) {
		this.dateEntered = dateEntered;
	}

	/**
	 * Returns the dateModified.
	 * 
	 * @return the dateModified
	 */
	public Timestamp getDateModified() {
		return dateModified;
	}

	/**
	 * Sets the dateModified.
	 * 
	 * @param dateModified
	 *            the dateModified
	 */
	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * Returns the modifiedUserId.
	 * 
	 * @return the modifiedUserId
	 */
	public String getModifiedUserId() {
		return modifiedUserId;
	}

	/**
	 * Sets the modifiedUserId.
	 * 
	 * @param modifiedUserId
	 *            the modifiedUserId
	 */
	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}

	/**
	 * Returns the createdBy.
	 * 
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy.
	 * 
	 * @param createdBy
	 *            the createdBy
	 */
	public void setCreatedBy(String createdBy) {

	}

	public BigDecimal getRsInfoCount() {
		return rsInfoCount;
	}

	public void setRsInfoCount(BigDecimal rsInfoCount) {
		this.rsInfoCount = rsInfoCount;
	}

	public BigDecimal getCdInfoCount() {
		return cdInfoCount;
	}

	public void setCdInfoCount(BigDecimal cdInfoCount) {
		this.cdInfoCount = cdInfoCount;
	}

	public BigDecimal getOwnPlanCount() {
		return ownPlanCount;
	}

	public void setOwnPlanCount(BigDecimal ownPlanCount) {
		this.ownPlanCount = ownPlanCount;
	}

}