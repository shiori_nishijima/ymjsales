package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class CallsUsersWithGroupInfoDto {
	String firstName;
	String lastName;

	@Column(name = "div2_nm")
	String div2Nm;
	@Column(name = "div3_nm")
	String div3Nm;

	BigDecimal callCount;

	/** グルーピング重点 */
	BigDecimal groupImportant;

	/** グルーピング新規 */
	BigDecimal groupNew;

	/** グルーピング深耕 */
	BigDecimal groupDeepPlowing;

	/** グルーピングYコンセプトショップ */
	@Column(name = "group_y_cp")
	BigDecimal groupYCp;

	/** グルーピング他社コンセプトショップ */
	BigDecimal groupOtherCp;

	/** グルーピング1G */
	BigDecimal groupOne;

	/** グルーピング2G */
	BigDecimal groupTwo;

	/** グルーピング3G */
	BigDecimal groupThree;

	/** グルーピング4G */
	BigDecimal groupFour;

	/** グルーピング5G */
	BigDecimal groupFive;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDiv2Nm() {
		return div2Nm;
	}

	public void setDiv2Nm(String div2Nm) {
		this.div2Nm = div2Nm;
	}

	public String getDiv3Nm() {
		return div3Nm;
	}

	public void setDiv3Nm(String div3Nm) {
		this.div3Nm = div3Nm;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getGroupImportant() {
		return groupImportant;
	}

	public void setGroupImportant(BigDecimal groupImportant) {
		this.groupImportant = groupImportant;
	}

	public BigDecimal getGroupNew() {
		return groupNew;
	}

	public void setGroupNew(BigDecimal groupNew) {
		this.groupNew = groupNew;
	}

	public BigDecimal getGroupDeepPlowing() {
		return groupDeepPlowing;
	}

	public void setGroupDeepPlowing(BigDecimal groupDeepPlowing) {
		this.groupDeepPlowing = groupDeepPlowing;
	}

	public BigDecimal getGroupYCp() {
		return groupYCp;
	}

	public void setGroupYCp(BigDecimal groupYCp) {
		this.groupYCp = groupYCp;
	}

	public BigDecimal getGroupOtherCp() {
		return groupOtherCp;
	}

	public void setGroupOtherCp(BigDecimal groupOtherCp) {
		this.groupOtherCp = groupOtherCp;
	}

	public BigDecimal getGroupOne() {
		return groupOne;
	}

	public void setGroupOne(BigDecimal groupOne) {
		this.groupOne = groupOne;
	}

	public BigDecimal getGroupTwo() {
		return groupTwo;
	}

	public void setGroupTwo(BigDecimal groupTwo) {
		this.groupTwo = groupTwo;
	}

	public BigDecimal getGroupThree() {
		return groupThree;
	}

	public void setGroupThree(BigDecimal groupThree) {
		this.groupThree = groupThree;
	}

	public BigDecimal getGroupFour() {
		return groupFour;
	}

	public void setGroupFour(BigDecimal groupFour) {
		this.groupFour = groupFour;
	}

	public BigDecimal getGroupFive() {
		return groupFive;
	}

	public void setGroupFive(BigDecimal groupFive) {
		this.groupFive = groupFive;
	}

}
