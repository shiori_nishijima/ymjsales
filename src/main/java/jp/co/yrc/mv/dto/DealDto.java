package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class DealDto {

	String groupType;
	BigDecimal masterCount;
	BigDecimal ownCount;
	BigDecimal dealPlanCount;
	BigDecimal dealCount;

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public BigDecimal getMasterCount() {
		return masterCount;
	}

	public void setMasterCount(BigDecimal masterCount) {
		this.masterCount = masterCount;
	}

	public BigDecimal getOwnCount() {
		return ownCount;
	}

	public void setOwnCount(BigDecimal ownCount) {
		this.ownCount = ownCount;
	}

	public BigDecimal getDealPlanCount() {
		return dealPlanCount;
	}

	public void setDealPlanCount(BigDecimal dealPlanCount) {
		this.dealPlanCount = dealPlanCount;
	}

	public BigDecimal getDealCount() {
		return dealCount;
	}

	public void setDealCount(BigDecimal dealCount) {
		this.dealCount = dealCount;
	}

}
