package jp.co.yrc.mv.dto;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

import jp.co.yrc.mv.entity.MarketInfos;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class MarketInfoDto extends MarketInfos {

	String infoDivName;
	String damagedPartName;
	String damagedNameName;
	String damagedTypeName;
	String performanceName;
	String roadSurfaceName;
	String carTypeName;
	String carMakerName;
	String carBodyShapeName;
	String mileageUnitName;
	String usagesName;

	public String getInfoDivName() {
		return infoDivName;
	}

	public void setInfoDivName(String infoDivName) {
		this.infoDivName = infoDivName;
	}

	public String getDamagedPartName() {
		return damagedPartName;
	}

	public void setDamagedPartName(String damagedPartName) {
		this.damagedPartName = damagedPartName;
	}

	public String getDamagedTypeName() {
		return damagedTypeName;
	}

	public void setDamagedTypeName(String damagedTypeName) {
		this.damagedTypeName = damagedTypeName;
	}

	public String getPerformanceName() {
		return performanceName;
	}

	public void setPerformanceName(String performanceName) {
		this.performanceName = performanceName;
	}

	public String getRoadSurfaceName() {
		return roadSurfaceName;
	}

	public void setRoadSurfaceName(String roadSurfaceName) {
		this.roadSurfaceName = roadSurfaceName;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}

	public String getCarBodyShapeName() {
		return carBodyShapeName;
	}

	public void setCarBodyShapeName(String carBodyShapeName) {
		this.carBodyShapeName = carBodyShapeName;
	}

	public String getMileageUnitName() {
		return mileageUnitName;
	}

	public void setMileageUnitName(String mileageUnitName) {
		this.mileageUnitName = mileageUnitName;
	}

	public String getUsagesName() {
		return usagesName;
	}

	public void setUsagesName(String usagesName) {
		this.usagesName = usagesName;
	}

	public String getDamagedNameName() {
		return damagedNameName;
	}

	public void setDamagedNameName(String damagedNameName) {
		this.damagedNameName = damagedNameName;
	}

	public String getCarMakerName() {
		return carMakerName;
	}

	public void setCarMakerName(String carMakerName) {
		this.carMakerName = carMakerName;
	}

}
