package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import jp.co.yrc.mv.entity.Results;

import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class GroupResultsDto extends Results {

	BigDecimal yearDemandNumber;
	BigDecimal yearYhDemandNumber;
	BigDecimal monthlyDemand;

	public BigDecimal getYearDemandNumber() {
		return yearDemandNumber;
	}

	public void setYearDemandNumber(BigDecimal yearDemandNumber) {
		this.yearDemandNumber = yearDemandNumber;
	}

	public BigDecimal getYearYhDemandNumber() {
		return yearYhDemandNumber;
	}

	public void setYearYhDemandNumber(BigDecimal yearYhDemandNumber) {
		this.yearYhDemandNumber = yearYhDemandNumber;
	}

	public BigDecimal getMonthlyDemand() {
		return monthlyDemand;
	}

	public void setMonthlyDemand(BigDecimal monthlyDemand) {
		this.monthlyDemand = monthlyDemand;
	}

}
