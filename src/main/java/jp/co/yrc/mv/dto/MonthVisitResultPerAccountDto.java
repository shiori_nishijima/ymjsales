package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class MonthVisitResultPerAccountDto {

	String id;
	String parentId;
	String assignedUserId;
	String name;
	String firstName;
	String lastName;
	String eigyoSosikiNm;
	String marketName;
	boolean groupImportant;
	boolean groupNew;
	boolean groupDeepPlowing;
	@Column(name = "group_y_cp")
	boolean groupYCp;
	boolean groupOtherCp;
	boolean groupOne;
	boolean groupTwo;
	boolean groupThree;
	boolean groupFour;
	boolean groupFive;

	BigDecimal sales;
	BigDecimal margin;
	BigDecimal number;

	BigDecimal plannedCount;
	BigDecimal callCount;
	BigDecimal infoCount;
	BigDecimal lastMonthCallCount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(String assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public boolean isGroupImportant() {
		return groupImportant;
	}

	public void setGroupImportant(boolean groupImportant) {
		this.groupImportant = groupImportant;
	}

	public boolean isGroupNew() {
		return groupNew;
	}

	public void setGroupNew(boolean groupNew) {
		this.groupNew = groupNew;
	}

	public boolean isGroupDeepPlowing() {
		return groupDeepPlowing;
	}

	public void setGroupDeepPlowing(boolean groupDeepPlowing) {
		this.groupDeepPlowing = groupDeepPlowing;
	}

	public boolean isGroupYCp() {
		return groupYCp;
	}

	public void setGroupYCp(boolean groupYCp) {
		this.groupYCp = groupYCp;
	}

	public boolean isGroupOtherCp() {
		return groupOtherCp;
	}

	public void setGroupOtherCp(boolean groupOtherCp) {
		this.groupOtherCp = groupOtherCp;
	}

	public boolean isGroupOne() {
		return groupOne;
	}

	public void setGroupOne(boolean groupOne) {
		this.groupOne = groupOne;
	}

	public boolean isGroupTwo() {
		return groupTwo;
	}

	public void setGroupTwo(boolean groupTwo) {
		this.groupTwo = groupTwo;
	}

	public boolean isGroupThree() {
		return groupThree;
	}

	public void setGroupThree(boolean groupThree) {
		this.groupThree = groupThree;
	}

	public boolean isGroupFour() {
		return groupFour;
	}

	public void setGroupFour(boolean groupFour) {
		this.groupFour = groupFour;
	}

	public boolean isGroupFive() {
		return groupFive;
	}

	public void setGroupFive(boolean groupFive) {
		this.groupFive = groupFive;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public BigDecimal getPlannedCount() {
		return plannedCount;
	}

	public void setPlannedCount(BigDecimal plannedCount) {
		this.plannedCount = plannedCount;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(BigDecimal infoCount) {
		this.infoCount = infoCount;
	}

	public BigDecimal getLastMonthCallCount() {
		return lastMonthCallCount;
	}

	public void setLastMonthCallCount(BigDecimal lastMonthCallCount) {
		this.lastMonthCallCount = lastMonthCallCount;
	}

	/* ここはDB追加削除で修正が必要 */
	@Column(name = "division_c_01")
	BigDecimal divisionC01;
	@Column(name = "division_c_02")
	BigDecimal divisionC02;
	@Column(name = "division_c_03")
	BigDecimal divisionC03;
	@Column(name = "division_c_04")
	BigDecimal divisionC04;
	@Column(name = "division_c_05")
	BigDecimal divisionC05;
	@Column(name = "division_c_06")
	BigDecimal divisionC06;
	@Column(name = "division_c_07")
	BigDecimal divisionC07;
	@Column(name = "division_c_08")
	BigDecimal divisionC08;
	@Column(name = "division_c_09")
	BigDecimal divisionC09;
	@Column(name = "division_c_10")
	BigDecimal divisionC10;
	@Column(name = "division_c_11")
	BigDecimal divisionC11;
	@Column(name = "division_c_12")
	BigDecimal divisionC12;
	@Column(name = "division_c_21")
	BigDecimal divisionC21;
	@Column(name = "division_c_22")
	BigDecimal divisionC22;
	@Column(name = "division_c_23")
	BigDecimal divisionC23;
	@Column(name = "division_c_24")
	BigDecimal divisionC24;
	@Column(name = "division_c_31")
	BigDecimal divisionC31;
	@Column(name = "division_c_32")
	BigDecimal divisionC32;
	@Column(name = "division_c_33")
	BigDecimal divisionC33;
	@Column(name = "division_c_41")
	BigDecimal divisionC41;
	@Column(name = "division_c_42")
	BigDecimal divisionC42;
	@Column(name = "division_c_43")
	BigDecimal divisionC43;
	@Column(name = "division_c_51")
	BigDecimal divisionC51;
	@Column(name = "division_c_52")
	BigDecimal divisionC52;
	@Column(name = "division_c_53")
	BigDecimal divisionC53;
	@Column(name = "division_c_71")
	BigDecimal divisionC71;
	@Column(name = "division_c_72")
	BigDecimal divisionC72;
	@Column(name = "division_c_73")
	BigDecimal divisionC73;
	@Column(name = "division_c_74")
	BigDecimal divisionC74;
	@Column(name = "division_c_81")
	BigDecimal divisionC81;
	@Column(name = "division_c_82")
	BigDecimal divisionC82;
	@Column(name = "division_c_83")
	BigDecimal divisionC83;
	@Column(name = "division_c_84")
	BigDecimal divisionC84;
	@Column(name = "division_c_98")
	BigDecimal divisionC98;
	@Column(name = "division_c_99")
	BigDecimal divisionC99;

	public BigDecimal getDivisionC01() {
		return divisionC01;
	}

	public void setDivisionC01(BigDecimal divisionC01) {
		this.divisionC01 = divisionC01;
	}

	public BigDecimal getDivisionC02() {
		return divisionC02;
	}

	public void setDivisionC02(BigDecimal divisionC02) {
		this.divisionC02 = divisionC02;
	}

	public BigDecimal getDivisionC03() {
		return divisionC03;
	}

	public void setDivisionC03(BigDecimal divisionC03) {
		this.divisionC03 = divisionC03;
	}

	public BigDecimal getDivisionC04() {
		return divisionC04;
	}

	public void setDivisionC04(BigDecimal divisionC04) {
		this.divisionC04 = divisionC04;
	}

	public BigDecimal getDivisionC05() {
		return divisionC05;
	}

	public void setDivisionC05(BigDecimal divisionC05) {
		this.divisionC05 = divisionC05;
	}

	public BigDecimal getDivisionC06() {
		return divisionC06;
	}

	public void setDivisionC06(BigDecimal divisionC06) {
		this.divisionC06 = divisionC06;
	}

	public BigDecimal getDivisionC07() {
		return divisionC07;
	}

	public void setDivisionC07(BigDecimal divisionC07) {
		this.divisionC07 = divisionC07;
	}

	public BigDecimal getDivisionC08() {
		return divisionC08;
	}

	public void setDivisionC08(BigDecimal divisionC08) {
		this.divisionC08 = divisionC08;
	}

	public BigDecimal getDivisionC09() {
		return divisionC09;
	}

	public void setDivisionC09(BigDecimal divisionC09) {
		this.divisionC09 = divisionC09;
	}

	public BigDecimal getDivisionC10() {
		return divisionC10;
	}

	public void setDivisionC10(BigDecimal divisionC10) {
		this.divisionC10 = divisionC10;
	}

	public BigDecimal getDivisionC11() {
		return divisionC11;
	}

	public void setDivisionC11(BigDecimal divisionC11) {
		this.divisionC11 = divisionC11;
	}

	public BigDecimal getDivisionC12() {
		return divisionC12;
	}

	public void setDivisionC12(BigDecimal divisionC12) {
		this.divisionC12 = divisionC12;
	}

	public BigDecimal getDivisionC21() {
		return divisionC21;
	}

	public void setDivisionC21(BigDecimal divisionC21) {
		this.divisionC21 = divisionC21;
	}

	public BigDecimal getDivisionC22() {
		return divisionC22;
	}

	public void setDivisionC22(BigDecimal divisionC22) {
		this.divisionC22 = divisionC22;
	}

	public BigDecimal getDivisionC23() {
		return divisionC23;
	}

	public void setDivisionC23(BigDecimal divisionC23) {
		this.divisionC23 = divisionC23;
	}

	public BigDecimal getDivisionC24() {
		return divisionC24;
	}

	public void setDivisionC24(BigDecimal divisionC24) {
		this.divisionC24 = divisionC24;
	}

	public BigDecimal getDivisionC31() {
		return divisionC31;
	}

	public void setDivisionC31(BigDecimal divisionC31) {
		this.divisionC31 = divisionC31;
	}

	public BigDecimal getDivisionC32() {
		return divisionC32;
	}

	public void setDivisionC32(BigDecimal divisionC32) {
		this.divisionC32 = divisionC32;
	}

	public BigDecimal getDivisionC33() {
		return divisionC33;
	}

	public void setDivisionC33(BigDecimal divisionC33) {
		this.divisionC33 = divisionC33;
	}

	public BigDecimal getDivisionC41() {
		return divisionC41;
	}

	public void setDivisionC41(BigDecimal divisionC41) {
		this.divisionC41 = divisionC41;
	}

	public BigDecimal getDivisionC42() {
		return divisionC42;
	}

	public void setDivisionC42(BigDecimal divisionC42) {
		this.divisionC42 = divisionC42;
	}

	public BigDecimal getDivisionC43() {
		return divisionC43;
	}

	public void setDivisionC43(BigDecimal divisionC43) {
		this.divisionC43 = divisionC43;
	}

	public BigDecimal getDivisionC51() {
		return divisionC51;
	}

	public void setDivisionC51(BigDecimal divisionC51) {
		this.divisionC51 = divisionC51;
	}

	public BigDecimal getDivisionC52() {
		return divisionC52;
	}

	public void setDivisionC52(BigDecimal divisionC52) {
		this.divisionC52 = divisionC52;
	}

	public BigDecimal getDivisionC53() {
		return divisionC53;
	}

	public void setDivisionC53(BigDecimal divisionC53) {
		this.divisionC53 = divisionC53;
	}

	public BigDecimal getDivisionC71() {
		return divisionC71;
	}

	public void setDivisionC71(BigDecimal divisionC71) {
		this.divisionC71 = divisionC71;
	}

	public BigDecimal getDivisionC72() {
		return divisionC72;
	}

	public void setDivisionC72(BigDecimal divisionC72) {
		this.divisionC72 = divisionC72;
	}

	public BigDecimal getDivisionC73() {
		return divisionC73;
	}

	public void setDivisionC73(BigDecimal divisionC73) {
		this.divisionC73 = divisionC73;
	}

	public BigDecimal getDivisionC74() {
		return divisionC74;
	}

	public void setDivisionC74(BigDecimal divisionC74) {
		this.divisionC74 = divisionC74;
	}

	public BigDecimal getDivisionC81() {
		return divisionC81;
	}

	public void setDivisionC81(BigDecimal divisionC81) {
		this.divisionC81 = divisionC81;
	}

	public BigDecimal getDivisionC82() {
		return divisionC82;
	}

	public void setDivisionC82(BigDecimal divisionC82) {
		this.divisionC82 = divisionC82;
	}

	public BigDecimal getDivisionC83() {
		return divisionC83;
	}

	public void setDivisionC83(BigDecimal divisionC83) {
		this.divisionC83 = divisionC83;
	}

	public BigDecimal getDivisionC84() {
		return divisionC84;
	}

	public void setDivisionC84(BigDecimal divisionC84) {
		this.divisionC84 = divisionC84;
	}

	public BigDecimal getDivisionC98() {
		return divisionC98;
	}

	public void setDivisionC98(BigDecimal divisionC98) {
		this.divisionC98 = divisionC98;
	}

	public BigDecimal getDivisionC99() {
		return divisionC99;
	}

	public void setDivisionC99(BigDecimal divisionC99) {
		this.divisionC99 = divisionC99;
	}
}
