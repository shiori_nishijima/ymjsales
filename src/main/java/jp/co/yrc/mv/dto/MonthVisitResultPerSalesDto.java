package jp.co.yrc.mv.dto;

import java.math.BigDecimal;

import org.seasar.doma.Column;
import org.seasar.doma.Entity;
import org.seasar.doma.jdbc.entity.NamingType;
import org.seasar.doma.jdbc.entity.NullEntityListener;

@Entity(listener = NullEntityListener.class, naming = NamingType.SNAKE_LOWER_CASE)
public class MonthVisitResultPerSalesDto {

	String id;
	String idForCustomer;
	String firstName;
	String lastName;
	String eigyoSosikiNm;
	BigDecimal planVisitNum;
	BigDecimal planInfoNum;
	BigDecimal planDemandCoefficient;
	BigDecimal ownCount;
	BigDecimal dealCount;
	BigDecimal demandNumber;
	BigDecimal sales;
	BigDecimal margin;
	BigDecimal number;
	BigDecimal ss;
	BigDecimal rs;
	BigDecimal cd;
	BigDecimal ps;
	BigDecimal cs;
	BigDecimal hc;
	BigDecimal lease;
	BigDecimal indirectOther;
	BigDecimal truck;
	BigDecimal bus;
	BigDecimal hireTaxi;
	BigDecimal directOther;
	BigDecimal dealResult;
	BigDecimal planSales;
	BigDecimal planMargin;
	BigDecimal planNumber;
	BigDecimal plannedCount;
	BigDecimal callCount;
	BigDecimal infoCount;
	BigDecimal lastMonthCallCount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdForCustomer() {
		return idForCustomer;
	}

	public void setIdForCustomer(String idForCustomer) {
		this.idForCustomer = idForCustomer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEigyoSosikiNm() {
		return eigyoSosikiNm;
	}

	public void setEigyoSosikiNm(String eigyoSosikiNm) {
		this.eigyoSosikiNm = eigyoSosikiNm;
	}

	public BigDecimal getPlanVisitNum() {
		return planVisitNum;
	}

	public void setPlanVisitNum(BigDecimal planVisitNum) {
		this.planVisitNum = planVisitNum;
	}

	public BigDecimal getPlanInfoNum() {
		return planInfoNum;
	}

	public void setPlanInfoNum(BigDecimal planInfoNum) {
		this.planInfoNum = planInfoNum;
	}

	public BigDecimal getPlanDemandCoefficient() {
		return planDemandCoefficient;
	}

	public void setPlanDemandCoefficient(BigDecimal planDemandCoefficient) {
		this.planDemandCoefficient = planDemandCoefficient;
	}

	public BigDecimal getOwnCount() {
		return ownCount;
	}

	public void setOwnCount(BigDecimal ownCount) {
		this.ownCount = ownCount;
	}

	public BigDecimal getDealCount() {
		return dealCount;
	}

	public void setDealCount(BigDecimal dealCount) {
		this.dealCount = dealCount;
	}

	public BigDecimal getDemandNumber() {
		return demandNumber;
	}

	public void setDemandNumber(BigDecimal demandNumber) {
		this.demandNumber = demandNumber;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public BigDecimal getSs() {
		return ss;
	}

	public void setSs(BigDecimal ss) {
		this.ss = ss;
	}

	public BigDecimal getRs() {
		return rs;
	}

	public void setRs(BigDecimal rs) {
		this.rs = rs;
	}

	public BigDecimal getCd() {
		return cd;
	}

	public void setCd(BigDecimal cd) {
		this.cd = cd;
	}

	public BigDecimal getPs() {
		return ps;
	}

	public void setPs(BigDecimal ps) {
		this.ps = ps;
	}

	public BigDecimal getCs() {
		return cs;
	}

	public void setCs(BigDecimal cs) {
		this.cs = cs;
	}

	public BigDecimal getHc() {
		return hc;
	}

	public void setHc(BigDecimal hc) {
		this.hc = hc;
	}

	public BigDecimal getLease() {
		return lease;
	}

	public void setLease(BigDecimal lease) {
		this.lease = lease;
	}

	public BigDecimal getIndirectOther() {
		return indirectOther;
	}

	public void setIndirectOther(BigDecimal indirectOther) {
		this.indirectOther = indirectOther;
	}

	public BigDecimal getTruck() {
		return truck;
	}

	public void setTruck(BigDecimal truck) {
		this.truck = truck;
	}

	public BigDecimal getBus() {
		return bus;
	}

	public void setBus(BigDecimal bus) {
		this.bus = bus;
	}

	public BigDecimal getHireTaxi() {
		return hireTaxi;
	}

	public void setHireTaxi(BigDecimal hireTaxi) {
		this.hireTaxi = hireTaxi;
	}

	public BigDecimal getDirectOther() {
		return directOther;
	}

	public void setDirectOther(BigDecimal directOther) {
		this.directOther = directOther;
	}

	public BigDecimal getDealResult() {
		return dealResult;
	}

	public void setDealResult(BigDecimal dealResult) {
		this.dealResult = dealResult;
	}

	public BigDecimal getPlanSales() {
		return planSales;
	}

	public void setPlanSales(BigDecimal planSales) {
		this.planSales = planSales;
	}

	public BigDecimal getPlanMargin() {
		return planMargin;
	}

	public void setPlanMargin(BigDecimal planMargin) {
		this.planMargin = planMargin;
	}

	public BigDecimal getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(BigDecimal planNumber) {
		this.planNumber = planNumber;
	}

	public BigDecimal getPlannedCount() {
		return plannedCount;
	}

	public void setPlannedCount(BigDecimal plannedCount) {
		this.plannedCount = plannedCount;
	}

	public BigDecimal getCallCount() {
		return callCount;
	}

	public void setCallCount(BigDecimal callCount) {
		this.callCount = callCount;
	}

	public BigDecimal getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(BigDecimal infoCount) {
		this.infoCount = infoCount;
	}

	public BigDecimal getLastMonthCallCount() {
		return lastMonthCallCount;
	}

	public void setLastMonthCallCount(BigDecimal lastMonthCallCount) {
		this.lastMonthCallCount = lastMonthCallCount;
	}

	/* DBに訪問理由を変更した場合はここを変更する必要あり  */
	@Column(name = "division_c_01")
	BigDecimal divisionC01;
	@Column(name = "division_c_02")
	BigDecimal divisionC02;
	@Column(name = "division_c_03")
	BigDecimal divisionC03;
	@Column(name = "division_c_04")
	BigDecimal divisionC04;
	@Column(name = "division_c_05")
	BigDecimal divisionC05;
	@Column(name = "division_c_06")
	BigDecimal divisionC06;
	@Column(name = "division_c_07")
	BigDecimal divisionC07;
	@Column(name = "division_c_08")
	BigDecimal divisionC08;
	@Column(name = "division_c_09")
	BigDecimal divisionC09;
	@Column(name = "division_c_10")
	BigDecimal divisionC10;
	@Column(name = "division_c_11")
	BigDecimal divisionC11;
	@Column(name = "division_c_12")
	BigDecimal divisionC12;
	@Column(name = "division_c_21")
	BigDecimal divisionC21;
	@Column(name = "division_c_22")
	BigDecimal divisionC22;
	@Column(name = "division_c_23")
	BigDecimal divisionC23;
	@Column(name = "division_c_24")
	BigDecimal divisionC24;
	@Column(name = "division_c_31")
	BigDecimal divisionC31;
	@Column(name = "division_c_32")
	BigDecimal divisionC32;
	@Column(name = "division_c_33")
	BigDecimal divisionC33;
	@Column(name = "division_c_41")
	BigDecimal divisionC41;
	@Column(name = "division_c_42")
	BigDecimal divisionC42;
	@Column(name = "division_c_43")
	BigDecimal divisionC43;
	@Column(name = "division_c_51")
	BigDecimal divisionC51;
	@Column(name = "division_c_52")
	BigDecimal divisionC52;
	@Column(name = "division_c_53")
	BigDecimal divisionC53;
	@Column(name = "division_c_71")
	BigDecimal divisionC71;
	@Column(name = "division_c_72")
	BigDecimal divisionC72;
	@Column(name = "division_c_73")
	BigDecimal divisionC73;
	@Column(name = "division_c_74")
	BigDecimal divisionC74;
	@Column(name = "division_c_81")
	BigDecimal divisionC81;
	@Column(name = "division_c_82")
	BigDecimal divisionC82;
	@Column(name = "division_c_83")
	BigDecimal divisionC83;
	@Column(name = "division_c_84")
	BigDecimal divisionC84;
	@Column(name = "division_c_98")
	BigDecimal divisionC98;
	@Column(name = "division_c_99")
	BigDecimal divisionC99;

	public BigDecimal getDivisionC01() {
		return divisionC01;
	}

	public void setDivisionC01(BigDecimal divisionC01) {
		this.divisionC01 = divisionC01;
	}

	public BigDecimal getDivisionC02() {
		return divisionC02;
	}

	public void setDivisionC02(BigDecimal divisionC02) {
		this.divisionC02 = divisionC02;
	}

	public BigDecimal getDivisionC03() {
		return divisionC03;
	}

	public void setDivisionC03(BigDecimal divisionC03) {
		this.divisionC03 = divisionC03;
	}

	public BigDecimal getDivisionC04() {
		return divisionC04;
	}

	public void setDivisionC04(BigDecimal divisionC04) {
		this.divisionC04 = divisionC04;
	}

	public BigDecimal getDivisionC05() {
		return divisionC05;
	}

	public void setDivisionC05(BigDecimal divisionC05) {
		this.divisionC05 = divisionC05;
	}

	public BigDecimal getDivisionC06() {
		return divisionC06;
	}

	public void setDivisionC06(BigDecimal divisionC06) {
		this.divisionC06 = divisionC06;
	}

	public BigDecimal getDivisionC07() {
		return divisionC07;
	}

	public void setDivisionC07(BigDecimal divisionC07) {
		this.divisionC07 = divisionC07;
	}

	public BigDecimal getDivisionC08() {
		return divisionC08;
	}

	public void setDivisionC08(BigDecimal divisionC08) {
		this.divisionC08 = divisionC08;
	}

	public BigDecimal getDivisionC09() {
		return divisionC09;
	}

	public void setDivisionC09(BigDecimal divisionC09) {
		this.divisionC09 = divisionC09;
	}

	public BigDecimal getDivisionC10() {
		return divisionC10;
	}

	public void setDivisionC10(BigDecimal divisionC10) {
		this.divisionC10 = divisionC10;
	}

	public BigDecimal getDivisionC11() {
		return divisionC11;
	}

	public void setDivisionC11(BigDecimal divisionC11) {
		this.divisionC11 = divisionC11;
	}

	public BigDecimal getDivisionC12() {
		return divisionC12;
	}

	public void setDivisionC12(BigDecimal divisionC12) {
		this.divisionC12 = divisionC12;
	}

	public BigDecimal getDivisionC21() {
		return divisionC21;
	}

	public void setDivisionC21(BigDecimal divisionC21) {
		this.divisionC21 = divisionC21;
	}

	public BigDecimal getDivisionC22() {
		return divisionC22;
	}

	public void setDivisionC22(BigDecimal divisionC22) {
		this.divisionC22 = divisionC22;
	}

	public BigDecimal getDivisionC23() {
		return divisionC23;
	}

	public void setDivisionC23(BigDecimal divisionC23) {
		this.divisionC23 = divisionC23;
	}

	public BigDecimal getDivisionC24() {
		return divisionC24;
	}

	public void setDivisionC24(BigDecimal divisionC24) {
		this.divisionC24 = divisionC24;
	}

	public BigDecimal getDivisionC31() {
		return divisionC31;
	}

	public void setDivisionC31(BigDecimal divisionC31) {
		this.divisionC31 = divisionC31;
	}

	public BigDecimal getDivisionC32() {
		return divisionC32;
	}

	public void setDivisionC32(BigDecimal divisionC32) {
		this.divisionC32 = divisionC32;
	}

	public BigDecimal getDivisionC33() {
		return divisionC33;
	}

	public void setDivisionC33(BigDecimal divisionC33) {
		this.divisionC33 = divisionC33;
	}

	public BigDecimal getDivisionC41() {
		return divisionC41;
	}

	public void setDivisionC41(BigDecimal divisionC41) {
		this.divisionC41 = divisionC41;
	}

	public BigDecimal getDivisionC42() {
		return divisionC42;
	}

	public void setDivisionC42(BigDecimal divisionC42) {
		this.divisionC42 = divisionC42;
	}

	public BigDecimal getDivisionC43() {
		return divisionC43;
	}

	public void setDivisionC43(BigDecimal divisionC43) {
		this.divisionC43 = divisionC43;
	}

	public BigDecimal getDivisionC51() {
		return divisionC51;
	}

	public void setDivisionC51(BigDecimal divisionC51) {
		this.divisionC51 = divisionC51;
	}

	public BigDecimal getDivisionC52() {
		return divisionC52;
	}

	public void setDivisionC52(BigDecimal divisionC52) {
		this.divisionC52 = divisionC52;
	}

	public BigDecimal getDivisionC53() {
		return divisionC53;
	}

	public void setDivisionC53(BigDecimal divisionC53) {
		this.divisionC53 = divisionC53;
	}

	public BigDecimal getDivisionC71() {
		return divisionC71;
	}

	public void setDivisionC71(BigDecimal divisionC71) {
		this.divisionC71 = divisionC71;
	}

	public BigDecimal getDivisionC72() {
		return divisionC72;
	}

	public void setDivisionC72(BigDecimal divisionC72) {
		this.divisionC72 = divisionC72;
	}

	public BigDecimal getDivisionC73() {
		return divisionC73;
	}

	public void setDivisionC73(BigDecimal divisionC73) {
		this.divisionC73 = divisionC73;
	}

	public BigDecimal getDivisionC74() {
		return divisionC74;
	}

	public void setDivisionC74(BigDecimal divisionC74) {
		this.divisionC74 = divisionC74;
	}

	public BigDecimal getDivisionC81() {
		return divisionC81;
	}

	public void setDivisionC81(BigDecimal divisionC81) {
		this.divisionC81 = divisionC81;
	}

	public BigDecimal getDivisionC82() {
		return divisionC82;
	}

	public void setDivisionC82(BigDecimal divisionC82) {
		this.divisionC82 = divisionC82;
	}

	public BigDecimal getDivisionC83() {
		return divisionC83;
	}

	public void setDivisionC83(BigDecimal divisionC83) {
		this.divisionC83 = divisionC83;
	}

	public BigDecimal getDivisionC84() {
		return divisionC84;
	}

	public void setDivisionC84(BigDecimal divisionC84) {
		this.divisionC84 = divisionC84;
	}

	public BigDecimal getDivisionC98() {
		return divisionC98;
	}

	public void setDivisionC98(BigDecimal divisionC98) {
		this.divisionC98 = divisionC98;
	}

	public BigDecimal getDivisionC99() {
		return divisionC99;
	}

	public void setDivisionC99(BigDecimal divisionC99) {
		this.divisionC99 = divisionC99;
	}

}
