package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.MathUtils.add;
import static jp.co.yrc.mv.common.MathUtils.toDefaultZero;
import static jp.co.yrc.mv.common.MathUtils.toK;
import static jp.co.yrc.mv.common.MathUtils.toPercentage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.dao.MonthlyCommentsDao;
import jp.co.yrc.mv.dao.MonthlyGroupCallsDao;
import jp.co.yrc.mv.dao.MonthlyGroupDealsDao;
import jp.co.yrc.mv.dao.MonthlyGroupDemandEstimationsDao;
import jp.co.yrc.mv.dao.MonthlyGroupResultsDao;
import jp.co.yrc.mv.dao.MonthlyKindPlansDao;
import jp.co.yrc.mv.dao.MonthlyKindResultsDao;
import jp.co.yrc.mv.dao.MonthlyMarketCallsDao;
import jp.co.yrc.mv.dao.MonthlyMarketDealsDao;
import jp.co.yrc.mv.dao.MonthlyMarketDemandEstimationsDao;
import jp.co.yrc.mv.dao.MonthlyMarketPlansDao;
import jp.co.yrc.mv.dao.MonthlyMarketResultsDao;
import jp.co.yrc.mv.dao.ResultsDao;
import jp.co.yrc.mv.dto.CallTotalDto;
import jp.co.yrc.mv.dto.GroupTotalDto;
import jp.co.yrc.mv.dto.KindMarketTotalDto;
import jp.co.yrc.mv.dto.MinMaxDto;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MonthlyGroupCalls;
import jp.co.yrc.mv.entity.MonthlyGroupDeals;
import jp.co.yrc.mv.entity.MonthlyGroupDemandEstimations;
import jp.co.yrc.mv.entity.MonthlyGroupResults;
import jp.co.yrc.mv.entity.MonthlyKindPlans;
import jp.co.yrc.mv.entity.MonthlyKindResults;
import jp.co.yrc.mv.entity.MonthlyMarketCalls;
import jp.co.yrc.mv.entity.MonthlyMarketDeals;
import jp.co.yrc.mv.entity.MonthlyMarketDemandEstimations;
import jp.co.yrc.mv.entity.MonthlyMarketPlans;
import jp.co.yrc.mv.entity.MonthlyMarketResults;
import jp.co.yrc.mv.service.CallsService;
import jp.co.yrc.mv.service.MstEigyoSosikiService;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 日報画面のヘルパー {@link #getResult(Integer, Integer, Integer, UserInfoDto, String, String, String, String, boolean)}
 * で結果を取得する。
 */
@Component
public class SumHelper {

	@Autowired
	ResultsDao resultsDao;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	@Autowired
	CallsService callsService;

	@Autowired
	MarketHelper marketHelper;

	@Autowired
	CallHelper callHelper;

	@Autowired
	MonthlyKindResultsDao monthlyKindResultsDao;

	@Autowired
	MonthlyMarketResultsDao monthlyMarketResultsDao;

	@Autowired
	MonthlyMarketCallsDao monthlyMarketCallsDao;

	@Autowired
	MonthlyMarketDealsDao monthlyMarketDealsDao;

	@Autowired
	MonthlyMarketDemandEstimationsDao monthlyMarketDemandEstimationsDao;

	@Autowired
	MonthlyGroupCallsDao monthlyGroupCallsDao;

	@Autowired
	MonthlyGroupDealsDao monthlyGroupDealsDao;

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	MonthlyGroupDemandEstimationsDao monthlyGroupDemandEstimationsDao;

	@Autowired
	MonthlyMarketPlansDao monthlyMarketPlansDao;

	@Autowired
	MonthlyKindPlansDao monthlyKindPlansDao;

	@Autowired
	MonthlyGroupResultsDao monthlyGroupResultsDao;

	@Autowired
	MonthlyCommentsDao monthlyCommentsDao;

	@Autowired
	SoshikiHelper soshikiHelper;

	public MinMaxDto findMinMaxYear() {
		return resultsDao.findMinMaxYear();
	}

	/**
	 * 当日の品種別・販路別を集計。前月以前の場合は、当日は表示しない
	 * 
	 * @param year
	 * @param month
	 * @param date
	 * @param yearMonth
	 * @param idForCustomer
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	protected KindMarketTotalDto sumDailyKindMarket(Integer year, Integer month, Integer date, Integer yearMonth,
			String idForCustomer, List<String> div1, String div2, String div3, boolean isHistory) {
		KindMarketTotalDto result = new KindMarketTotalDto();
		// 前月以前の場合は、当日は表示しない
		if (!isHistory) {
			MonthlyKindResults dailyResult = resultsDao.sumKindResult(year, month, date, yearMonth, idForCustomer,
					div1, div2, div3, isHistory);
			if (dailyResult != null) {
				dozerBeanMapper.map(dailyResult, result);
			}
			marketHelper.setMarket(resultsDao.sumMarketResult(year, month, date, yearMonth, idForCustomer, div1, div2,
					div3, isHistory), result);
		}
		return result;
	}

	/**
	 * 月の品種・販路の集計
	 * 
	 * @param year
	 * @param month
	 * @param tanto
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	protected KindMarketTotalDto sumMonthlyKindMarket(Integer year, Integer month, String tanto, List<String> div1,
			String div2, String div3, boolean isHistory) {
		KindMarketTotalDto result = new KindMarketTotalDto();

		MonthlyKindResults monthlyKindResults = monthlyKindResultsDao.sum(year, month, tanto, div1, div2, div3,
				isHistory);
		MonthlyMarketResults monthlyMarketResults = monthlyMarketResultsDao.sum(year, month, tanto, div1, div2, div3,
				isHistory);

		if (monthlyKindResults != null) {
			dozerBeanMapper.map(monthlyKindResults, result);
		}
		if (monthlyMarketResults != null) {
			dozerBeanMapper.map(monthlyMarketResults, result);
		}

		return result;
	}

	/**
	 * 当日のグループトータルを集計。前月以前の場合は、当日は表示しない。
	 * 
	 * @param year
	 * @param month
	 * @param date
	 * @param yearMonth
	 * @param tanto
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public GroupTotalDto sumDailyGroupTotal(Integer year, Integer month, Integer date, Integer yearMonth, String tanto,
			List<String> div1, String div2, String div3, boolean isHistory) {
		GroupTotalDto result = new GroupTotalDto();

		// 前月以前の場合は、当日は表示しない
		if (!isHistory) {
			MonthlyGroupResults sumed = resultsDao.sumGroupResult(year, month, date, yearMonth, tanto, div1, div2,
					div3, isHistory);
			if (sumed != null) {
				dozerBeanMapper.map(sumed, result);
			}
		}
		return result;
	}

	/**
	 * 月のグループトータルを集計。組織単位で検索した場合は担当外を足し込む処理有り。
	 * 
	 * @param year
	 * @param month
	 * @param tanto
	 * @param div1
	 * @param div2
	 * @param div3
	 * @param isHistory
	 * @return
	 */
	public GroupTotalDto sumMonthlyGroupTotal(Integer year, Integer month, String tanto, List<String> div1,
			String div2, String div3, boolean isHistory) {
		GroupTotalDto result = new GroupTotalDto();

		MonthlyGroupCalls calls = monthlyGroupCallsDao.sum(year, month, tanto, div1, div2, div3, isHistory);
		MonthlyGroupDeals deals = monthlyGroupDealsDao.sum(year, month, tanto, div1, div2, div3, isHistory);
		MonthlyGroupResults monthlyGroupResults = monthlyGroupResultsDao.sum(year, month, tanto, div1, div2, div3,
				isHistory);
		if (calls != null) {

			// 組織単位で検索した場合は担当外を足し込む
			if (tanto == null) {
				addOutsideGroupCall(calls);
			}
			dozerBeanMapper.map(calls, result);
		}
		if (deals != null) {
			dozerBeanMapper.map(deals, result);
		}
		if (monthlyGroupResults != null) {
			dozerBeanMapper.map(monthlyGroupResults, result);
		}

		return result;
	}

	/**
	 * 担当外の訪問を足し込む。
	 * 
	 * @param calls
	 */
	public void addOutsideGroupCall(MonthlyGroupCalls calls) {
		calls.setOutsideCallCount(BigDecimal.ZERO);
		calls.setOutsidePlanCount(BigDecimal.ZERO);
		calls.setOutsideInfoCount(BigDecimal.ZERO);
		calls.setRankAPlanCount(calls.getRankAPlanCount().add(calls.getOutsideRankAPlanCount()));
		calls.setRankACallCount(calls.getRankACallCount().add(calls.getOutsideRankACallCount()));
		calls.setRankAInfoCount(calls.getRankAInfoCount().add(calls.getOutsideRankAInfoCount()));
		calls.setRankBPlanCount(calls.getRankBPlanCount().add(calls.getOutsideRankBPlanCount()));
		calls.setRankBCallCount(calls.getRankBCallCount().add(calls.getOutsideRankBCallCount()));
		calls.setRankBInfoCount(calls.getRankBInfoCount().add(calls.getOutsideRankBInfoCount()));
		calls.setRankCPlanCount(calls.getRankCPlanCount().add(calls.getOutsideRankCPlanCount()));
		calls.setRankCCallCount(calls.getRankCCallCount().add(calls.getOutsideRankCCallCount()));
		calls.setRankCInfoCount(calls.getRankCInfoCount().add(calls.getOutsideRankCInfoCount()));
		calls.setImportantPlanCount(calls.getImportantPlanCount().add(calls.getOutsideImportantPlanCount()));
		calls.setImportantCallCount(calls.getImportantCallCount().add(calls.getOutsideImportantCallCount()));
		calls.setImportantInfoCount(calls.getImportantInfoCount().add(calls.getOutsideImportantInfoCount()));
		calls.setNewPlanCount(calls.getNewPlanCount().add(calls.getOutsideNewPlanCount()));
		calls.setNewCallCount(calls.getNewCallCount().add(calls.getOutsideNewCallCount()));
		calls.setNewInfoCount(calls.getNewInfoCount().add(calls.getOutsideNewInfoCount()));
		calls.setDeepPlowingPlanCount(calls.getDeepPlowingPlanCount().add(calls.getOutsideDeepPlowingPlanCount()));
		calls.setDeepPlowingCallCount(calls.getDeepPlowingCallCount().add(calls.getOutsideDeepPlowingCallCount()));
		calls.setDeepPlowingInfoCount(calls.getDeepPlowingInfoCount().add(calls.getOutsideDeepPlowingInfoCount()));
		calls.setYCpPlanCount(calls.getYCpPlanCount().add(calls.getOutsideYCpPlanCount()));
		calls.setYCpCallCount(calls.getYCpCallCount().add(calls.getOutsideYCpCallCount()));
		calls.setYCpInfoCount(calls.getYCpInfoCount().add(calls.getOutsideYCpInfoCount()));
		calls.setOtherCpPlanCount(calls.getOtherCpPlanCount().add(calls.getOutsideOtherCpPlanCount()));
		calls.setOtherCpCallCount(calls.getOtherCpCallCount().add(calls.getOutsideOtherCpCallCount()));
		calls.setOtherCpInfoCount(calls.getOtherCpInfoCount().add(calls.getOutsideOtherCpInfoCount()));
		calls.setGroupOnePlanCount(calls.getGroupOnePlanCount().add(calls.getOutsideGroupOnePlanCount()));
		calls.setGroupOneCallCount(calls.getGroupOneCallCount().add(calls.getOutsideGroupOneCallCount()));
		calls.setGroupOneInfoCount(calls.getGroupOneInfoCount().add(calls.getOutsideGroupOneInfoCount()));
		calls.setGroupTwoPlanCount(calls.getGroupTwoPlanCount().add(calls.getOutsideGroupTwoPlanCount()));
		calls.setGroupTwoCallCount(calls.getGroupTwoCallCount().add(calls.getOutsideGroupTwoCallCount()));
		calls.setGroupTwoInfoCount(calls.getGroupTwoInfoCount().add(calls.getOutsideGroupTwoInfoCount()));
		calls.setGroupThreePlanCount(calls.getGroupThreePlanCount().add(calls.getOutsideGroupThreePlanCount()));
		calls.setGroupThreeCallCount(calls.getGroupThreeCallCount().add(calls.getOutsideGroupThreeCallCount()));
		calls.setGroupThreeInfoCount(calls.getGroupThreeInfoCount().add(calls.getOutsideGroupThreeInfoCount()));
		calls.setGroupFourPlanCount(calls.getGroupFourPlanCount().add(calls.getOutsideGroupFourPlanCount()));
		calls.setGroupFourCallCount(calls.getGroupFourCallCount().add(calls.getOutsideGroupFourCallCount()));
		calls.setGroupFourInfoCount(calls.getGroupFourInfoCount().add(calls.getOutsideGroupFourInfoCount()));
		calls.setGroupFivePlanCount(calls.getGroupFivePlanCount().add(calls.getOutsideGroupFivePlanCount()));
		calls.setGroupFiveCallCount(calls.getGroupFiveCallCount().add(calls.getOutsideGroupFiveCallCount()));
		calls.setGroupFiveInfoCount(calls.getGroupFiveInfoCount().add(calls.getOutsideGroupFiveInfoCount()));
	}

	public CallTotalDto sumMonthlyCall(Integer year, Integer month, String tanto, List<String> div1, String div2,
			String div3, boolean isHistory) {
		CallTotalDto result = new CallTotalDto();

		MonthlyMarketCalls calls = monthlyMarketCallsDao.sum(year, month, tanto, div1, div2, div3, isHistory);

		MonthlyMarketDeals deals = monthlyMarketDealsDao.sum(year, month, tanto, div1, div2, div3, isHistory);

		if (calls != null) {
			// 組織単位で検索した場合は担当外を足し込む
			if (tanto == null) {
				addOutsideMarketCall(calls);
			}
			dozerBeanMapper.map(calls, result);
		}
		if (deals != null) {
			dozerBeanMapper.map(deals, result);
			result.setOwnPlanCount(deals.getPlanCount());
		}

		return result;
	}

	/**
	 * 担当外の販路別訪問を足し込む
	 * 
	 * @param calls
	 */
	public void addOutsideMarketCall(MonthlyMarketCalls calls) {
		calls.setSsPlanCount(calls.getSsPlanCount().add(calls.getOutsideSsPlanCount()));
		calls.setSsCallCount(calls.getSsCallCount().add(calls.getOutsideSsCallCount()));
		calls.setSsInfoCount(calls.getSsInfoCount().add(calls.getOutsideSsInfoCount()));
		calls.setRsPlanCount(calls.getRsPlanCount().add(calls.getOutsideRsPlanCount()));
		calls.setRsCallCount(calls.getRsCallCount().add(calls.getOutsideRsCallCount()));
		calls.setRsInfoCount(calls.getRsInfoCount().add(calls.getOutsideRsInfoCount()));
		calls.setCdPlanCount(calls.getCdPlanCount().add(calls.getOutsideCdPlanCount()));
		calls.setCdCallCount(calls.getCdCallCount().add(calls.getOutsideCdCallCount()));
		calls.setCdInfoCount(calls.getCdInfoCount().add(calls.getOutsideCdInfoCount()));
		calls.setPsPlanCount(calls.getPsPlanCount().add(calls.getOutsidePsPlanCount()));
		calls.setPsCallCount(calls.getPsCallCount().add(calls.getOutsidePsCallCount()));
		calls.setPsInfoCount(calls.getPsInfoCount().add(calls.getOutsidePsInfoCount()));
		calls.setCsPlanCount(calls.getCsPlanCount().add(calls.getOutsideCsPlanCount()));
		calls.setCsCallCount(calls.getCsCallCount().add(calls.getOutsideCsCallCount()));
		calls.setCsInfoCount(calls.getCsInfoCount().add(calls.getOutsideCsInfoCount()));
		calls.setHcPlanCount(calls.getHcPlanCount().add(calls.getOutsideHcPlanCount()));
		calls.setHcCallCount(calls.getHcCallCount().add(calls.getOutsideHcCallCount()));
		calls.setHcInfoCount(calls.getHcInfoCount().add(calls.getOutsideHcInfoCount()));
		calls.setLeasePlanCount(calls.getLeasePlanCount().add(calls.getOutsideLeasePlanCount()));
		calls.setLeaseCallCount(calls.getLeaseCallCount().add(calls.getOutsideLeaseCallCount()));
		calls.setLeaseInfoCount(calls.getLeaseInfoCount().add(calls.getOutsideLeaseInfoCount()));
		calls.setIndirectPlanCount(calls.getIndirectPlanCount().add(calls.getOutsideIndirectPlanCount()));
		calls.setIndirectOtherCallCount(calls.getIndirectOtherCallCount().add(calls.getOutsideIndirectOtherCallCount()));
		calls.setIndirectOtherInfoCount(calls.getIndirectOtherInfoCount().add(calls.getOutsideIndirectOtherInfoCount()));
		calls.setTruckPlanCount(calls.getTruckPlanCount().add(calls.getOutsideTruckPlanCount()));
		calls.setTruckCallCount(calls.getTruckCallCount().add(calls.getOutsideTruckCallCount()));
		calls.setTruckInfoCount(calls.getTruckInfoCount().add(calls.getOutsideTruckInfoCount()));
		calls.setBusPlanCount(calls.getBusPlanCount().add(calls.getOutsideBusPlanCount()));
		calls.setBusCallCount(calls.getBusCallCount().add(calls.getOutsideBusCallCount()));
		calls.setBusInfoCount(calls.getBusInfoCount().add(calls.getOutsideBusInfoCount()));
		calls.setHirePlanCount(calls.getHirePlanCount().add(calls.getOutsideHirePlanCount()));
		calls.setHireTaxiCallCount(calls.getHireTaxiCallCount().add(calls.getOutsideHireTaxiCallCount()));
		calls.setHireTaxiInfoCount(calls.getHireTaxiInfoCount().add(calls.getOutsideHireTaxiInfoCount()));
		calls.setDirectPlanCount(calls.getDirectPlanCount().add(calls.getOutsideDirectPlanCount()));
		calls.setDirectOtherCallCount(calls.getDirectOtherCallCount().add(calls.getOutsideDirectOtherCallCount()));
		calls.setDirectOtherInfoCount(calls.getDirectOtherInfoCount().add(calls.getOutsideDirectOtherInfoCount()));
	}

	/**
	 * 日報表示用結果を取得する。
	 * 
	 * @param year
	 * @param month
	 * @param date
	 *            <code>NULL</code>なら月指定モード
	 * @param userInfo
	 *            組織選んでいないときはここからログインユーザーの権限に紐付く組織をリストで取得する
	 * @param tanto
	 *            担当者コード
	 * @param div1
	 * @param div2
	 * @param div3
	 * @return
	 */
	public Result getResult(Integer year, Integer month, Integer date, UserInfoDto userInfo, String tanto, String div1,
			String div2, String div3, boolean isToday) {

		Integer yearMonth = DateUtils.toYearMonth(year, month);

		MonthlyCommentsDto com = monthlyCommentsDao.selectByCondition(year, month, userInfo.getId());
		BigDecimal planDemandCoefficient = com == null ? BigDecimal.ZERO : com.getPlanDemandCoefficient();
		if (planDemandCoefficient == null) {
			planDemandCoefficient = BigDecimal.ZERO;
		} else {
			planDemandCoefficient = planDemandCoefficient.divide(MathUtils.HUNDRED);
		}

		div2 = soshikiHelper.checkSoshikiParamValue(div2);
		div3 = soshikiHelper.checkSoshikiParamValue(div3);
		tanto = soshikiHelper.checkSoshikiParamValue(tanto);

		List<String> companies = soshikiHelper.createSearchCompanies(div1, tanto, userInfo, year, month);

		// 日付の指定が無い場合は過去情報の検索とみなす
		boolean isHistory = date == null;

		// monthly_group_calls(月次グループ訪問実績)
		// ,monthly_group_deals(月次グループ取引軒数)
		// ,monthly_group_demand_estimations(月次グループ推定需要)
		// ,monthly_group_results(月次グループ実績)
		// ,monthly_kind_results(月次品種別実績)
		// ,monthly_market_results(月次販路別実績);
		// ,monthly_market_demand_estimations(月次販路別需要予測)
		// ,monthly_market_calls(月次販路別訪問実績)
		// ,monthly_market_deals(月次販路別取引軒数)
		// ,monthly_market_plans(月次販路別計画)
		// ,monthly_kind_plans(月次品種別計画)

		// 品種別・販路別の計算
		KindMarketTotalDto dayKindMarket = sumDailyKindMarket(year, month, date, yearMonth, tanto, companies, div2,
				div3, isHistory);
		KindMarketTotalDto monthlyKindMarket = sumMonthlyKindMarket(year, month, tanto, companies, div2, div3,
				isHistory);
		MonthlyMarketDemandEstimations monthlyMarketDemandEstimations = monthlyMarketDemandEstimationsDao.sum(year,
				month, tanto, companies, div2, div3, isHistory);

		// 訪問・取引（月次のみ）
		CallTotalDto monthlyCallResults = sumMonthlyCall(year, month, tanto, companies, div2, div3, isHistory);

		// グルーピング
		GroupTotalDto groupTotalDto = sumDailyGroupTotal(year, month, date, yearMonth, tanto, companies, div2, div3,
				isHistory);
		GroupTotalDto monthlyGroupResults = sumMonthlyGroupTotal(year, month, tanto, companies, div2, div3, isHistory);
		MonthlyGroupDemandEstimations groupDemandEstimations = monthlyGroupDemandEstimationsDao.sum(year, month, tanto,
				companies, div2, div3, isHistory);

		// 計画
		MonthlyMarketPlans monthlyMarketPlans = monthlyMarketPlansDao.sum(year, month, tanto, companies, div2, div3,
				isHistory);
		MonthlyKindPlans monthlyKindPlans = monthlyKindPlansDao.sum(year, month, tanto, companies, div2, div3,
				isHistory);

		Result result = new Result();

		// ログイン者の訪問予定・本数予定
		BigDecimal visitPlan = com == null ? BigDecimal.ZERO : com.getPlanVisitNum();
		if (visitPlan == null) {
			visitPlan = BigDecimal.ZERO;
		}
		result.call.total.visit.plan = visitPlan;

		BigDecimal infoPlan = com == null ? BigDecimal.ZERO : com.getPlanInfoNum();
		if (infoPlan == null) {
			infoPlan = BigDecimal.ZERO;
		}
		result.call.total.info.plan = infoPlan;

		setKind(result, dayKindMarket, monthlyKindMarket, monthlyKindPlans, isToday);

		setMarket(result, dayKindMarket, monthlyKindMarket, monthlyMarketDemandEstimations, monthlyMarketPlans,
				planDemandCoefficient, isToday);

		setCall(result, monthlyCallResults, isToday);

		setGroup(result, groupTotalDto, monthlyGroupResults, groupDemandEstimations, planDemandCoefficient, isToday);

		return result;

	}

	/**
	 * 画面表示用に品種別データを設定
	 * 
	 * @param result
	 * @param kindMarketTotalDto
	 * @param monthlyPersonalResults
	 * @param monthlyKindPlans
	 * @param isToday
	 */
	protected void setKind(Result result, KindMarketTotalDto kindMarketTotalDto,
			KindMarketTotalDto monthlyPersonalResults, MonthlyKindPlans monthlyKindPlans, boolean isToday) {

		// 計画データが無い場合は空
		if (monthlyKindPlans == null) {
			monthlyKindPlans = new MonthlyKindPlans();
		}

		setKindEntity(result.total.total, result.total.summer, result.kind.pcrTotal, result.kind.pcrSummer, isToday,
				kindMarketTotalDto.getPcrSummerNumber(), monthlyPersonalResults.getPcrSummerNumber(),
				kindMarketTotalDto.getPcrSummerSales(), monthlyPersonalResults.getPcrSummerSales(),
				kindMarketTotalDto.getPcrSummerMargin(), monthlyPersonalResults.getPcrSummerMargin(),
				kindMarketTotalDto.getPcrSummerStock(), monthlyKindPlans.getPcrSummerNumber(),
				monthlyKindPlans.getPcrSummerSales(), monthlyKindPlans.getPcrSummerMargin());

		setKindEntity(result.total.total, result.total.snow, result.kind.pcrTotal, result.kind.pcrPureSnow, isToday,
				kindMarketTotalDto.getPcrPureSnowNumber(), monthlyPersonalResults.getPcrPureSnowNumber(),
				kindMarketTotalDto.getPcrPureSnowSales(), monthlyPersonalResults.getPcrPureSnowSales(),
				kindMarketTotalDto.getPcrPureSnowMargin(), monthlyPersonalResults.getPcrPureSnowMargin(),
				kindMarketTotalDto.getPcrPureSnowStock(), monthlyKindPlans.getPcrPureSnowNumber(),
				monthlyKindPlans.getPcrPureSnowSales(), monthlyKindPlans.getPcrPureSnowMargin());

		calcKindRate(result.kind.pcrTotal);

		setKindEntity(result.total.total, result.total.summer, result.kind.vanTotal, result.kind.vanSummer, isToday,
				kindMarketTotalDto.getVanSummerNumber(), monthlyPersonalResults.getVanSummerNumber(),
				kindMarketTotalDto.getVanSummerSales(), monthlyPersonalResults.getVanSummerSales(),
				kindMarketTotalDto.getVanSummerMargin(), monthlyPersonalResults.getVanSummerMargin(),
				kindMarketTotalDto.getVanSummerStock(), monthlyKindPlans.getVanSummerNumber(),
				monthlyKindPlans.getVanSummerSales(), monthlyKindPlans.getVanSummerMargin());

		setKindEntity(result.total.total, result.total.snow, result.kind.vanTotal, result.kind.vanPureSnow, isToday,
				kindMarketTotalDto.getVanPureSnowNumber(), monthlyPersonalResults.getVanPureSnowNumber(),
				kindMarketTotalDto.getVanPureSnowSales(), monthlyPersonalResults.getVanPureSnowSales(),
				kindMarketTotalDto.getVanPureSnowMargin(), monthlyPersonalResults.getVanPureSnowMargin(),
				kindMarketTotalDto.getVanPureSnowStock(), monthlyKindPlans.getVanPureSnowNumber(),
				monthlyKindPlans.getVanPureSnowSales(), monthlyKindPlans.getVanPureSnowMargin());

		calcKindRate(result.kind.vanTotal);

		setKindEntity(result.total.total, result.total.summer, result.kind.pcbTotal, result.kind.pcbSummer, isToday,
				kindMarketTotalDto.getPcbSummerNumber(), monthlyPersonalResults.getPcbSummerNumber(),
				kindMarketTotalDto.getPcbSummerSales(), monthlyPersonalResults.getPcbSummerSales(),
				kindMarketTotalDto.getPcbSummerMargin(), monthlyPersonalResults.getPcbSummerMargin(),
				kindMarketTotalDto.getPcbSummerStock(), monthlyKindPlans.getPcbSummerNumber(),
				monthlyKindPlans.getPcbSummerSales(), monthlyKindPlans.getPcbSummerMargin());

		// PCBSNOWは夏扱い
		setKindEntity(result.total.total, result.total.summer, result.kind.pcbTotal, result.kind.pcbPureSnow, isToday,
				kindMarketTotalDto.getPcbPureSnowNumber(), monthlyPersonalResults.getPcbPureSnowNumber(),
				kindMarketTotalDto.getPcbPureSnowSales(), monthlyPersonalResults.getPcbPureSnowSales(),
				kindMarketTotalDto.getPcbPureSnowMargin(), monthlyPersonalResults.getPcbPureSnowMargin(),
				kindMarketTotalDto.getPcbPureSnowStock(), monthlyKindPlans.getPcbPureSnowNumber(),
				monthlyKindPlans.getPcbPureSnowSales(), monthlyKindPlans.getPcbPureSnowMargin());

		calcKindRate(result.kind.pcbTotal);

		setKindEntity(result.total.total, result.total.summer, result.kind.ltTotal, result.kind.ltSummer, isToday,
				kindMarketTotalDto.getLtSummerNumber(), monthlyPersonalResults.getLtSummerNumber(),
				kindMarketTotalDto.getLtSummerSales(), monthlyPersonalResults.getLtSummerSales(),
				kindMarketTotalDto.getLtSummerMargin(), monthlyPersonalResults.getLtSummerMargin(),
				kindMarketTotalDto.getLtSummerStock(), monthlyKindPlans.getLtSummerNumber(),
				monthlyKindPlans.getLtSummerSales(), monthlyKindPlans.getLtSummerMargin());

		setKindEntity(result.total.total, result.total.snow, result.kind.ltTotal, result.kind.ltSnow, isToday,
				kindMarketTotalDto.getLtSnowNumber(), monthlyPersonalResults.getLtSnowNumber(),
				kindMarketTotalDto.getLtSnowSales(), monthlyPersonalResults.getLtSnowSales(),
				kindMarketTotalDto.getLtSnowMargin(), monthlyPersonalResults.getLtSnowMargin(),
				kindMarketTotalDto.getLtSnowStock(), monthlyKindPlans.getLtSnowNumber(),
				monthlyKindPlans.getLtSnowSales(), monthlyKindPlans.getLtSnowMargin());

		setKindEntity(null, null, null, result.kind.ltAs, isToday, kindMarketTotalDto.getLtAsNumber(),
				monthlyPersonalResults.getLtAsNumber(), kindMarketTotalDto.getLtAsSales(),
				monthlyPersonalResults.getLtAsSales(), kindMarketTotalDto.getLtAsMargin(),
				monthlyPersonalResults.getLtAsMargin(), kindMarketTotalDto.getLtAsStock(), BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO);

		setKindEntity(null, null, null, result.kind.ltPureSnow, isToday, kindMarketTotalDto.getLtPureSnowNumber(),
				monthlyPersonalResults.getLtPureSnowNumber(), kindMarketTotalDto.getLtPureSnowSales(),
				monthlyPersonalResults.getLtPureSnowSales(), kindMarketTotalDto.getLtPureSnowMargin(),
				monthlyPersonalResults.getLtPureSnowMargin(), kindMarketTotalDto.getLtPureSnowStock(), BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO);

		calcKindRate(result.kind.ltTotal);

		setKindEntity(result.total.total, result.total.summer, result.kind.tbTotal, result.kind.tbSummer, isToday,
				kindMarketTotalDto.getTbSummerNumber(), monthlyPersonalResults.getTbSummerNumber(),
				kindMarketTotalDto.getTbSummerSales(), monthlyPersonalResults.getTbSummerSales(),
				kindMarketTotalDto.getTbSummerMargin(), monthlyPersonalResults.getTbSummerMargin(),
				kindMarketTotalDto.getTbSummerStock(), monthlyKindPlans.getTbSummerNumber(),
				monthlyKindPlans.getTbSummerSales(), monthlyKindPlans.getTbSummerMargin());

		setKindEntity(result.total.total, result.total.snow, result.kind.tbTotal, result.kind.tbSnow, isToday,
				kindMarketTotalDto.getTbSnowNumber(), monthlyPersonalResults.getTbSnowNumber(),
				kindMarketTotalDto.getTbSnowSales(), monthlyPersonalResults.getTbSnowSales(),
				kindMarketTotalDto.getTbSnowMargin(), monthlyPersonalResults.getTbSnowMargin(),
				kindMarketTotalDto.getTbSnowStock(), monthlyKindPlans.getTbSnowNumber(),
				monthlyKindPlans.getTbSnowSales(), monthlyKindPlans.getTbSnowMargin());

		setKindEntity(null, null, null, result.kind.tbAs, isToday, kindMarketTotalDto.getTbAsNumber(),
				monthlyPersonalResults.getTbAsNumber(), kindMarketTotalDto.getTbAsSales(),
				monthlyPersonalResults.getTbAsSales(), kindMarketTotalDto.getTbAsMargin(),
				monthlyPersonalResults.getTbAsMargin(), kindMarketTotalDto.getTbAsStock(), BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO);

		setKindEntity(null, null, null, result.kind.tbPureSnow, isToday, kindMarketTotalDto.getTbPureSnowNumber(),
				monthlyPersonalResults.getTbPureSnowNumber(), kindMarketTotalDto.getTbPureSnowSales(),
				monthlyPersonalResults.getTbPureSnowSales(), kindMarketTotalDto.getTbPureSnowMargin(),
				monthlyPersonalResults.getTbPureSnowMargin(), kindMarketTotalDto.getTbPureSnowStock(), BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO);

		calcKindRate(result.kind.tbTotal);

		// 計画は、OR/ID共通だがIDで詰めておく
		setKindEntity(result.total.total, result.total.summer, result.kind.orId, result.kind.id, isToday,
				kindMarketTotalDto.getIdNumber(), monthlyPersonalResults.getIdNumber(),
				kindMarketTotalDto.getIdSales(), monthlyPersonalResults.getIdSales(), kindMarketTotalDto.getIdMargin(),
				monthlyPersonalResults.getIdMargin(), kindMarketTotalDto.getIdStock(),
				monthlyKindPlans.getOrIdNumber(), monthlyKindPlans.getOrIdSales(), monthlyKindPlans.getOrIdMargin());

		setKindEntity(result.total.total, result.total.summer, result.kind.orId, result.kind.or, isToday,
				kindMarketTotalDto.getOrNumber(), monthlyPersonalResults.getOrNumber(),
				kindMarketTotalDto.getOrSales(), monthlyPersonalResults.getOrSales(), kindMarketTotalDto.getOrMargin(),
				monthlyPersonalResults.getOrMargin(), kindMarketTotalDto.getOrStock(), BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO);

		calcKindRate(result.kind.orId);

		// チフ・リトレッド・その他・作業の金額・粗利は「本数・金額・粗利」の「その他」に加える。
		// チフ・リトレッドの本数は「品種別実績」には表示するが「本数・金額・粗利」の「その他」には加えない。
		setKindEntity(result.total.total, false, result.total.other, null, result.kind.tifu, isToday,
				kindMarketTotalDto.getTifuNumber(), monthlyPersonalResults.getTifuNumber(), 
				kindMarketTotalDto.getTifuSales(), monthlyPersonalResults.getTifuSales(),
				kindMarketTotalDto.getTifuMargin(), monthlyPersonalResults.getTifuMargin(),
				kindMarketTotalDto.getTifuStock(), 
				monthlyKindPlans.getTifuNumber(), monthlyKindPlans.getTifuSales(), monthlyKindPlans.getTifuMargin());

		setKindEntity(result.total.total, false, result.total.other, null, result.kind.retread, isToday, 
				kindMarketTotalDto.getRetreadNumber(), monthlyPersonalResults.getRetreadNumber(), 
				kindMarketTotalDto.getRetreadSales(), monthlyPersonalResults.getRetreadSales(),
				kindMarketTotalDto.getRetreadMargin(), monthlyPersonalResults.getRetreadMargin(),
				kindMarketTotalDto.getRetreadStock(), 
				monthlyKindPlans.getRetreadNumber(), monthlyKindPlans.getRetreadSales(), monthlyKindPlans.getRetreadMargin());

		setKindEntity(result.total.total, result.total.other, null, result.kind.other, isToday, 
				BigDecimal.ZERO, BigDecimal.ZERO, 
				kindMarketTotalDto.getOtherSales(), monthlyPersonalResults.getOtherSales(),
				kindMarketTotalDto.getOtherMargin(), monthlyPersonalResults.getOtherMargin(),
				kindMarketTotalDto.getOtherStock(), 
				BigDecimal.ZERO, monthlyKindPlans.getOtherSales(), monthlyKindPlans.getOtherMargin());

		setKindEntity(result.total.total, result.total.other, null, result.kind.work, isToday, 
				BigDecimal.ZERO, BigDecimal.ZERO, 
				kindMarketTotalDto.getWorkSales(), monthlyPersonalResults.getWorkSales(),
				kindMarketTotalDto.getWorkMargin(), monthlyPersonalResults.getWorkMargin(),
				kindMarketTotalDto.getWorkStock(), 
				BigDecimal.ZERO, monthlyKindPlans.getWorkSales(), monthlyKindPlans.getWorkMargin());

		calcKindRate(result.total.summer);
		calcKindRate(result.total.snow);
		calcKindRate(result.total.other);
		calcKindRate(result.total.total);

	}

	/**
	 * 品種別のrate関連の計算、足し込んだデータをもとに算出
	 * 
	 * @param total
	 * @param entities
	 */
	public void calcKindRate(KindEntity total, KindEntity... entities) {
		total.number.demandEstimationComponentRate = toPercentage(total.number.demandEstimation, total.number.monthly);

		total.number.custodyRate = toPercentage(total.number.custody, total.number.monthly);

		total.number.achievementRate = toPercentage(total.number.monthly, total.number.plan);

		total.sales.achievementRate = toPercentage(total.sales.monthly, total.sales.plan);

		total.margin.achievementRate = toPercentage(total.margin.monthly, total.margin.plan);

		total.margin.marginRate = toPercentage(total.margin.monthly, total.sales.monthly);

		total.sales.plan = toK(total.sales.plan);
		total.sales.current = toK(total.sales.current);
		total.sales.monthly = toK(total.sales.monthly);
		total.margin.plan = toK(total.margin.plan);
		total.margin.current = toK(total.margin.current);
		total.margin.monthly = toK(total.margin.monthly);
	}

	/**
	 * 品種の1データ作成（本数の足し込みをフラグで判定）
	 * 
	 * @param total
	 * @param isTotalAddNumber
	 *            totalに本数を足し込むか
	 * @param totalBreakdown
	 * @param kindTotal
	 * @param kind
	 * @param isToday
	 * @param dailyNumber
	 * @param monthlyNumber
	 * @param dailySales
	 * @param monthlySales
	 * @param dailyMargin
	 * @param monthlyMargin
	 * @param stock
	 * @param planNumber
	 * @param planSales
	 * @param planMargin
	 */
	protected void setKindEntity(KindEntity total, boolean isTotalAddNumber, KindEntity totalBreakdown,
			KindEntity kindTotal, KindEntity kind, boolean isToday, BigDecimal dailyNumber, BigDecimal monthlyNumber,
			BigDecimal dailySales, BigDecimal monthlySales, BigDecimal dailyMargin, BigDecimal monthlyMargin,
			BigDecimal stock, BigDecimal planNumber, BigDecimal planSales, BigDecimal planMargin) {

		kind.number.plan = toDefaultZero(planNumber);

		kind.number.current = toDefaultZero(dailyNumber);

		kind.number.monthly = calcMonthly(monthlyNumber, dailyNumber, isToday);

		kind.number.achievementRate = toPercentage(kind.number.monthly, kind.number.plan);

		kind.number.custody = toDefaultZero(stock);

		kind.number.custodyRate = toPercentage(kind.number.monthly, kind.number.custody);

		kind.sales.plan = toDefaultZero(planSales);

		kind.sales.current = toDefaultZero(dailySales);

		kind.sales.monthly = calcMonthly(monthlySales, dailySales, isToday);

		kind.sales.achievementRate = toPercentage(kind.sales.monthly, kind.sales.plan);

		kind.margin.plan = toDefaultZero(planMargin);

		kind.margin.current = toDefaultZero(dailyMargin);

		kind.margin.monthly = calcMonthly(monthlyMargin, dailyMargin, isToday);

		kind.margin.marginRate = toPercentage(kind.margin.monthly, kind.sales.monthly);

		kind.margin.achievementRate = toPercentage(kind.margin.monthly, kind.margin.plan);

		if (kindTotal != null) {
			setKindTotal(kindTotal, kind, isTotalAddNumber);
		}

		if (totalBreakdown != null) {
			setKindTotal(totalBreakdown, kind, isTotalAddNumber);
		}

		if (total != null) {
			setKindTotal(total, kind, isTotalAddNumber);
		}

		kind.sales.plan = toK(kind.sales.plan);
		kind.sales.current = toK(kind.sales.current);
		kind.sales.monthly = toK(kind.sales.monthly);
		kind.margin.plan = toK(kind.margin.plan);
		kind.margin.monthly = toK(kind.margin.monthly);
		kind.margin.current = toK(kind.margin.current);

	}

	/**
	 * 品種の1データ作成（本数は足しこむ）
	 * 
	 * @see #setKindEntity(KindEntity, boolean, KindEntity, KindEntity, KindEntity, boolean, BigDecimal, BigDecimal,
	 *      BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal)
	 * @param total 合計
	 * @param totalBreakdown ナツ・ASスノー・その他 
	 * @param kindTotal 品種別合計
	 * @param kind 品種
	 * @param isToday 当日検索かどうか
	 * @param dailyNumber 本数
	 * @param monthlyNumber 月本数
	 * @param dailySales 売上
	 * @param monthlySales 月売上
	 * @param dailyMargin 粗利
	 * @param monthlyMargin 月粗利
	 * @param stock 在庫
	 * @param planNumber 計画本数
	 * @param planSales 計画売上
	 * @param planMargin 計画粗利
	 */
	protected void setKindEntity(KindEntity total, KindEntity totalBreakdown, KindEntity kindTotal, KindEntity kind,
			boolean isToday, BigDecimal dailyNumber, BigDecimal monthlyNumber, BigDecimal dailySales,
			BigDecimal monthlySales, BigDecimal dailyMargin, BigDecimal monthlyMargin, BigDecimal stock,
			BigDecimal planNumber, BigDecimal planSales, BigDecimal planMargin) {

		setKindEntity(total, true, totalBreakdown, kindTotal, kind, isToday, dailyNumber, monthlyNumber, dailySales,
				monthlySales, dailyMargin, monthlyMargin, stock, planNumber, planSales, planMargin);

	}

	/**
	 * 品種別と合計部分を設定
	 * 
	 * @param total
	 * @param kind
	 * @param isTotalAddNumber
	 */
	protected void setKindTotal(KindEntity total, KindEntity kind, boolean isTotalAddNumber) {
		if (isTotalAddNumber) {
			total.number.plan = add(total.number.plan, kind.number.plan);
			total.number.current = add(total.number.current, kind.number.current);
			total.number.monthly = add(total.number.monthly, kind.number.monthly);
			total.number.custody = add(total.number.custody, kind.number.custody);
		}

		total.sales.plan = add(total.sales.plan, kind.sales.plan);
		total.sales.current = add(total.sales.current, kind.sales.current);
		total.sales.monthly = add(total.sales.monthly, kind.sales.monthly);

		total.margin.plan = add(total.margin.plan, kind.margin.plan);
		total.margin.current = add(total.margin.current, kind.margin.current);
		total.margin.monthly = add(total.margin.monthly, kind.margin.monthly);
	}

	/**
	 * 販路別の画面用データを作成
	 * 
	 * @param result
	 * @param kindMarketTotalDto
	 * @param monthlyPersonalResults
	 * @param demand
	 * @param monthlyMarketPlans
	 * @param planDemandCoefficient
	 * @param isToday
	 */
	protected void setMarket(Result result, KindMarketTotalDto kindMarketTotalDto,
			KindMarketTotalDto monthlyPersonalResults, MonthlyMarketDemandEstimations demand,
			MonthlyMarketPlans monthlyMarketPlans, BigDecimal planDemandCoefficient, boolean isToday) {

		// データが無い場合は空を入れる
		if (kindMarketTotalDto == null) {
			kindMarketTotalDto = new KindMarketTotalDto();

		}
		if (monthlyPersonalResults == null) {
			monthlyPersonalResults = new KindMarketTotalDto();
		}

		if (demand == null) {
			demand = new MonthlyMarketDemandEstimations();
		}

		if (monthlyMarketPlans == null) {
			monthlyMarketPlans = new MonthlyMarketPlans();
		}

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.ss, isToday,
				monthlyMarketPlans.getSsNumber(), kindMarketTotalDto.getSsNumber(),
				monthlyPersonalResults.getSsNumber(), monthlyMarketPlans.getSsSales(), kindMarketTotalDto.getSsSales(),
				monthlyPersonalResults.getSsSales(), monthlyMarketPlans.getSsMargin(),
				kindMarketTotalDto.getSsMargin(), monthlyPersonalResults.getSsMargin(),
				demand.getSsYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.rs, isToday,
				monthlyMarketPlans.getRsNumber(), kindMarketTotalDto.getRsNumber(),
				monthlyPersonalResults.getRsNumber(), monthlyMarketPlans.getRsSales(), kindMarketTotalDto.getRsSales(),
				monthlyPersonalResults.getRsSales(), monthlyMarketPlans.getRsMargin(),
				kindMarketTotalDto.getRsMargin(), monthlyPersonalResults.getRsMargin(),
				demand.getRsYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.cd, isToday,
				monthlyMarketPlans.getCdNumber(), kindMarketTotalDto.getCdNumber(),
				monthlyPersonalResults.getCdNumber(), monthlyMarketPlans.getCdSales(), kindMarketTotalDto.getCdSales(),
				monthlyPersonalResults.getCdSales(), monthlyMarketPlans.getCdMargin(),
				kindMarketTotalDto.getCdMargin(), monthlyPersonalResults.getCdMargin(),
				demand.getCdYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.ps, isToday,
				monthlyMarketPlans.getPsNumber(), kindMarketTotalDto.getPsNumber(),
				monthlyPersonalResults.getPsNumber(), monthlyMarketPlans.getPsSales(), kindMarketTotalDto.getPsSales(),
				monthlyPersonalResults.getPsSales(), monthlyMarketPlans.getPsMargin(),
				kindMarketTotalDto.getPsMargin(), monthlyPersonalResults.getPsMargin(),
				demand.getPsYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.cs, isToday,
				monthlyMarketPlans.getCsNumber(), kindMarketTotalDto.getCsNumber(),
				monthlyPersonalResults.getCsNumber(), monthlyMarketPlans.getCsSales(), kindMarketTotalDto.getCsSales(),
				monthlyPersonalResults.getCsSales(), monthlyMarketPlans.getCsMargin(),
				kindMarketTotalDto.getCsMargin(), monthlyPersonalResults.getCsMargin(),
				demand.getCsYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.hc, isToday,
				monthlyMarketPlans.getHcNumber(), kindMarketTotalDto.getHcNumber(),
				monthlyPersonalResults.getHcNumber(), monthlyMarketPlans.getHcSales(), kindMarketTotalDto.getHcSales(),
				monthlyPersonalResults.getHcSales(), monthlyMarketPlans.getHcMargin(),
				kindMarketTotalDto.getHcMargin(), monthlyPersonalResults.getHcMargin(),
				demand.getHcYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.lease, isToday,
				monthlyMarketPlans.getLeaseNumber(), kindMarketTotalDto.getLeaseNumber(),
				monthlyPersonalResults.getLeaseNumber(), monthlyMarketPlans.getLeaseSales(),
				kindMarketTotalDto.getLeaseSales(), monthlyPersonalResults.getLeaseSales(),
				monthlyMarketPlans.getLeaseMargin(), kindMarketTotalDto.getLeaseMargin(),
				monthlyPersonalResults.getLeaseMargin(), demand.getLeaseYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.indirectTotal, result.market.indirectOther, isToday,
				monthlyMarketPlans.getIndirectOtherNumber(), kindMarketTotalDto.getIndirectOtherNumber(),
				monthlyPersonalResults.getIndirectOtherNumber(), monthlyMarketPlans.getIndirectOtherSales(),
				kindMarketTotalDto.getIndirectOtherSales(), monthlyPersonalResults.getIndirectOtherSales(),
				monthlyMarketPlans.getIndirectOtherMargin(), kindMarketTotalDto.getIndirectOtherMargin(),
				monthlyPersonalResults.getIndirectOtherMargin(), demand.getIndirectOtherYearDemandEstimation(),
				planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.directTotal, result.market.truck, isToday,
				monthlyMarketPlans.getTruckNumber(), kindMarketTotalDto.getTruckNumber(),
				monthlyPersonalResults.getTruckNumber(), monthlyMarketPlans.getTruckSales(),
				kindMarketTotalDto.getTruckSales(), monthlyPersonalResults.getTruckSales(),
				monthlyMarketPlans.getTruckMargin(), kindMarketTotalDto.getTruckMargin(),
				monthlyPersonalResults.getTruckMargin(), demand.getTruckYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.directTotal, result.market.bus, isToday,
				monthlyMarketPlans.getBusNumber(), kindMarketTotalDto.getBusNumber(),
				monthlyPersonalResults.getBusNumber(), monthlyMarketPlans.getBusSales(),
				kindMarketTotalDto.getBusSales(), monthlyPersonalResults.getBusSales(),
				monthlyMarketPlans.getBusMargin(), kindMarketTotalDto.getBusMargin(),
				monthlyPersonalResults.getBusMargin(), demand.getBusYearDemandEstimation(), planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.directTotal, result.market.hireTaxi, isToday,
				monthlyMarketPlans.getHireTaxiNumber(), kindMarketTotalDto.getHireTaxiNumber(),
				monthlyPersonalResults.getHireTaxiNumber(), monthlyMarketPlans.getHireTaxiSales(),
				kindMarketTotalDto.getHireTaxiSales(), monthlyPersonalResults.getHireTaxiSales(),
				monthlyMarketPlans.getHireTaxiMargin(), kindMarketTotalDto.getHireTaxiMargin(),
				monthlyPersonalResults.getHireTaxiMargin(), demand.getHireTaxiYearDemandEstimation(),
				planDemandCoefficient);

		setMarketEntity(result.market.total, result.market.directTotal, result.market.directOther, isToday,
				monthlyMarketPlans.getDirectOtherNumber(), kindMarketTotalDto.getDirectOtherNumber(),
				monthlyPersonalResults.getDirectOtherNumber(), monthlyMarketPlans.getDirectOtherSales(),
				kindMarketTotalDto.getDirectOtherSales(), monthlyPersonalResults.getDirectOtherSales(),
				monthlyMarketPlans.getDirectOtherMargin(), kindMarketTotalDto.getDirectOtherMargin(),
				monthlyPersonalResults.getDirectOtherMargin(), demand.getDirectOtherYearDemandEstimation(),
				planDemandCoefficient);

		setMarketEntity(result.market.total, null, result.market.retail, isToday, monthlyMarketPlans.getRetailNumber(),
				kindMarketTotalDto.getRetailNumber(), monthlyPersonalResults.getRetailNumber(),
				monthlyMarketPlans.getRetailSales(), kindMarketTotalDto.getRetailSales(),
				monthlyPersonalResults.getRetailSales(), monthlyMarketPlans.getRetailMargin(),
				kindMarketTotalDto.getRetailMargin(), monthlyPersonalResults.getRetailMargin(),
				demand.getRetailYearDemandEstimation(), planDemandCoefficient);

		result.market.indirectTotal.number.achievementRate = toPercentage(result.market.indirectTotal.number.monthly,
				result.market.indirectTotal.number.plan);
		result.market.indirectTotal.number.issEstimation = toPercentage(result.market.indirectTotal.number.monthly,
				result.market.indirectTotal.number.demandEstimation);
		result.market.indirectTotal.sales.achievementRate = toPercentage(result.market.indirectTotal.sales.monthly,
				result.market.indirectTotal.sales.plan);
		result.market.indirectTotal.margin.achievementRate = toPercentage(result.market.indirectTotal.margin.monthly,
				result.market.indirectTotal.margin.plan);

		result.market.directTotal.number.achievementRate = toPercentage(result.market.directTotal.number.monthly,
				result.market.directTotal.number.plan);
		result.market.directTotal.number.issEstimation = toPercentage(result.market.directTotal.number.monthly,
				result.market.directTotal.number.demandEstimation);
		result.market.directTotal.sales.achievementRate = toPercentage(result.market.directTotal.sales.monthly,
				result.market.directTotal.sales.plan);
		result.market.directTotal.margin.achievementRate = toPercentage(result.market.directTotal.margin.monthly,
				result.market.directTotal.margin.plan);

		// 構成比の計算
		calcMarketRate(result.market.total, result.market.ss, result.market.rs, result.market.cd, result.market.ps,
				result.market.cs, result.market.hc, result.market.lease, result.market.indirectOther,
				result.market.indirectTotal, result.market.truck, result.market.bus, result.market.hireTaxi,
				result.market.directOther, result.market.directTotal, result.market.retail);

		result.market.total.number.achievementRate = toPercentage(result.market.total.number.monthly,
				result.market.total.number.plan);
		result.market.total.number.issEstimation = toPercentage(result.market.total.number.monthly,
				result.market.total.number.demandEstimation);
		result.market.total.sales.achievementRate = toPercentage(result.market.total.sales.monthly,
				result.market.total.sales.plan);
		result.market.total.margin.achievementRate = toPercentage(result.market.total.margin.monthly,
				result.market.total.margin.plan);

		// 1000円単位でないデータで計算をするためここで詰めておく
		result.group.total.sales.plan = result.market.total.sales.plan;
		result.group.total.margin.plan = result.market.total.margin.plan;

		toKMarketResult(result.market.total, result.market.ss, result.market.rs, result.market.cd, result.market.ps,
				result.market.cs, result.market.hc, result.market.lease, result.market.indirectOther,
				result.market.indirectTotal, result.market.truck, result.market.bus, result.market.hireTaxi,
				result.market.directOther, result.market.directTotal, result.market.retail);
	}

	/**
	 * 構成比の計算
	 * 
	 * @param total
	 * @param markets
	 */
	protected void calcMarketRate(MarketEntity total, MarketEntity... markets) {

		for (MarketEntity entity : markets) {
			entity.number.componentRate = toPercentage(entity.number.monthly, total.number.monthly);
			entity.number.demandEstimationComponentRate = toPercentage(entity.number.demandEstimation,
					total.number.demandEstimation);
			entity.sales.componentRate = toPercentage(entity.sales.monthly, total.sales.monthly);
			entity.margin.componentRate = toPercentage(entity.margin.monthly, total.margin.monthly);
		}

	}

	/**
	 * 販路別実績の売上・粗利を1000円単位に
	 * 
	 * @param markets
	 */
	protected void toKMarketResult(MarketEntity... markets) {

		for (MarketEntity entity : markets) {
			entity.sales.plan = toK(entity.sales.plan);
			entity.sales.current = toK(entity.sales.current);
			entity.sales.monthly = toK(entity.sales.monthly);
			entity.margin.plan = toK(entity.margin.plan);
			entity.margin.monthly = toK(entity.margin.monthly);
			entity.margin.current = toK(entity.margin.current);
		}

	}

	/**
	 * 販路別の１データの設定
	 * 
	 * @param total
	 * @param marketTotal
	 * @param market
	 * @param isToday
	 * @param planNumber
	 * @param dailyNumber
	 * @param monthlyNumber
	 * @param planSales
	 * @param dailySales
	 * @param monthlySales
	 * @param planMargin
	 * @param dailyMargin
	 * @param monthlyMargin
	 * @param demandEstimation
	 * @param planDemandCoefficient
	 */
	protected void setMarketEntity(MarketEntity total, MarketEntity marketTotal, MarketEntity market, boolean isToday,
			BigDecimal planNumber, BigDecimal dailyNumber, BigDecimal monthlyNumber, BigDecimal planSales,
			BigDecimal dailySales, BigDecimal monthlySales, BigDecimal planMargin, BigDecimal dailyMargin,
			BigDecimal monthlyMargin, BigDecimal demandEstimation, BigDecimal planDemandCoefficient) {

		market.number.plan = toDefaultZero(planNumber);
		market.number.current = toDefaultZero(dailyNumber);
		market.number.monthly = calcMonthly(monthlyNumber, dailyNumber, isToday);
		market.number.achievementRate = toPercentage(market.number.monthly, market.number.plan);
		market.number.demandEstimation = toDefaultZero(demandEstimation).multiply(planDemandCoefficient).setScale(0,
				RoundingMode.HALF_UP);
		market.number.issEstimation = toPercentage(market.number.monthly, market.number.demandEstimation);
		market.sales.plan = toDefaultZero(planSales);
		market.sales.current = toDefaultZero(dailySales);
		market.sales.monthly = calcMonthly(monthlySales, dailySales, isToday);
		market.sales.achievementRate = toPercentage(market.sales.monthly, market.sales.plan);
		market.margin.plan = toDefaultZero(planMargin);
		market.margin.current = toDefaultZero(dailyMargin);
		market.margin.monthly = calcMonthly(monthlyMargin, dailyMargin, isToday);
		market.margin.achievementRate = toPercentage(market.margin.monthly, market.margin.plan);

		if (marketTotal != null) {

			setMarketTotal(marketTotal, market);
		}

		if (total != null) {

			setMarketTotal(total, market);
		}
	}

	/**
	 * 販路の合計をします
	 * 
	 * @param total
	 * @param market
	 */
	protected void setMarketTotal(MarketEntity total, MarketEntity market) {
		total.number.plan = add(total.number.plan, market.number.plan);
		total.number.current = add(total.number.current, market.number.current);
		total.number.monthly = add(total.number.monthly, market.number.monthly);
		total.number.demandEstimation = add(total.number.demandEstimation, market.number.demandEstimation);
		total.sales.plan = add(total.sales.plan, market.sales.plan);
		total.sales.current = add(total.sales.current, market.sales.current);
		total.sales.monthly = add(total.sales.monthly, market.sales.monthly);
		total.margin.plan = add(total.margin.plan, market.margin.plan);
		total.margin.current = add(total.margin.current, market.margin.current);
		total.margin.monthly = add(total.margin.monthly, market.margin.monthly);
	}

	/**
	 * 訪問の1データを設定
	 * 
	 * @param total
	 * @param callTotal
	 * @param call
	 * @param isToday
	 * @param havingCount
	 * @param deal
	 * @param dealMCount
	 * @param visit
	 * @param info
	 */
	protected void setCallEntity(CallEntity total, CallEntity callTotal, CallEntity call, boolean isToday,
			BigDecimal havingCount, BigDecimal deal, BigDecimal dealMCount, BigDecimal visit, BigDecimal info) {

		call.having.count = toDefaultZero(havingCount);
		call.having.deal = toDefaultZero(deal);
		call.having.dealRate = toPercentage(call.having.deal, call.having.count);

		call.having.dealMCount = toDefaultZero(dealMCount);

		call.visit.monthly = toDefaultZero(visit);

		call.info.count = toDefaultZero(info);

		if (total != null) {
			setCallTotal(total, call);
		}
		if (callTotal != null) {
			setCallTotal(callTotal, call);
		}
	}

	/**
	 * 訪問の合計を設定
	 * 
	 * @param total
	 * @param call
	 */
	protected void setCallTotal(CallEntity total, CallEntity call) {
		total.having.deal = add(total.having.deal, call.having.deal);
		total.having.count = add(total.having.count, call.having.count);
		total.having.dealMCount = add(total.having.dealMCount, call.having.dealMCount);
		total.visit.monthly = add(total.visit.monthly, call.visit.monthly);
		total.info.count = add(total.info.count, call.info.count);
	}

	/**
	 * 訪問を設定
	 * 
	 * @param result
	 * @param monthlyCallResults
	 * @param isToday
	 */
	protected void setCall(Result result, CallTotalDto monthlyCallResults, boolean isToday) {

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.ss, isToday,
				monthlyCallResults.getSsOwnCount(), monthlyCallResults.getSsDealCount(),
				monthlyCallResults.getSsAccountMasterCount(), monthlyCallResults.getSsCallCount(),
				monthlyCallResults.getSsInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.rs, isToday,
				monthlyCallResults.getRsOwnCount(), monthlyCallResults.getRsDealCount(),
				monthlyCallResults.getRsAccountMasterCount(), monthlyCallResults.getRsCallCount(),
				monthlyCallResults.getRsInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.cd, isToday,
				monthlyCallResults.getCdOwnCount(), monthlyCallResults.getCdDealCount(),
				monthlyCallResults.getCdAccountMasterCount(), monthlyCallResults.getCdCallCount(),
				monthlyCallResults.getCdInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.ps, isToday,
				monthlyCallResults.getPsOwnCount(), monthlyCallResults.getPsDealCount(),
				monthlyCallResults.getPsAccountMasterCount(), monthlyCallResults.getPsCallCount(),
				monthlyCallResults.getPsInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.cs, isToday,
				monthlyCallResults.getCsOwnCount(), monthlyCallResults.getCsDealCount(),
				monthlyCallResults.getCsAccountMasterCount(), monthlyCallResults.getCsCallCount(),
				monthlyCallResults.getCsInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.hc, isToday,
				monthlyCallResults.getHcOwnCount(), monthlyCallResults.getHcDealCount(),
				monthlyCallResults.getHcAccountMasterCount(), monthlyCallResults.getHcCallCount(),
				monthlyCallResults.getHcInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.lease, isToday,
				monthlyCallResults.getLeaseOwnCount(), monthlyCallResults.getLeaseDealCount(),
				monthlyCallResults.getLeaseAccountMasterCount(), monthlyCallResults.getLeaseCallCount(),
				monthlyCallResults.getLeaseInfoCount());

		setCallEntity(result.call.total, result.call.indirectTotal, result.call.indirectOther, isToday,
				monthlyCallResults.getIndirectOtherOwnCount(), monthlyCallResults.getIndirectOtherDealCount(),
				monthlyCallResults.getIndirectOtherAccountMasterCount(),
				monthlyCallResults.getIndirectOtherCallCount(), monthlyCallResults.getIndirectOtherInfoCount());

		setCallEntity(result.call.total, result.call.directTotal, result.call.truck, isToday,
				monthlyCallResults.getTruckOwnCount(), monthlyCallResults.getTruckDealCount(),
				monthlyCallResults.getTruckAccountMasterCount(), monthlyCallResults.getTruckCallCount(),
				monthlyCallResults.getTruckInfoCount());

		setCallEntity(result.call.total, result.call.directTotal, result.call.bus, isToday,
				monthlyCallResults.getBusOwnCount(), monthlyCallResults.getBusDealCount(),
				monthlyCallResults.getBusAccountMasterCount(), monthlyCallResults.getBusCallCount(),
				monthlyCallResults.getBusInfoCount());

		setCallEntity(result.call.total, result.call.directTotal, result.call.hireTaxi, isToday,
				monthlyCallResults.getHireTaxiOwnCount(), monthlyCallResults.getHireTaxiDealCount(),
				monthlyCallResults.getHireTaxiAccountMasterCount(), monthlyCallResults.getHireTaxiCallCount(),
				monthlyCallResults.getHireTaxiInfoCount());

		setCallEntity(result.call.total, result.call.directTotal, result.call.directOther, isToday,
				monthlyCallResults.getDirectOtherOwnCount(), monthlyCallResults.getDirectOtherDealCount(),
				monthlyCallResults.getDirectOtherAccountMasterCount(), monthlyCallResults.getDirectOtherCallCount(),
				monthlyCallResults.getDirectOtherInfoCount());

		result.call.total.having.plan = monthlyCallResults.getOwnPlanCount();
		calcCallRate(result.call.total, result.call.ss, result.call.rs, result.call.cd, result.call.ps, result.call.cs,
				result.call.hc, result.call.lease, result.call.indirectOther, result.call.indirectTotal,
				result.call.truck, result.call.bus, result.call.hireTaxi, result.call.directOther,
				result.call.directTotal, result.call.outside);

		result.call.directTotal.having.dealRate = toPercentage(result.call.directTotal.having.deal,
				result.call.directTotal.having.count);
		result.call.indirectTotal.having.dealRate = toPercentage(result.call.indirectTotal.having.deal,
				result.call.indirectTotal.having.count);

	}

	/**
	 * 取引件数・訪問実績の達成率・取引率合計と訪問の構成比を計算し、設定します。
	 * 
	 * @param total
	 * @param calls
	 */
	protected void calcCallRate(CallEntity total, CallEntity... calls) {

		for (CallEntity call : calls) {
			call.visit.componentRate = toPercentage(call.visit.monthly, total.visit.monthly);
		}
		total.having.dealRate = toPercentage(total.having.deal, total.having.count);
		total.visit.achievementRate = toPercentage(total.visit.monthly, total.visit.plan);
		total.having.achievementRate = toPercentage(total.having.deal, total.having.plan);
		total.info.achievementRate = toPercentage(total.info.count, total.info.plan);
	}

	/**
	 * グルーピングを設定
	 * 
	 * @param result
	 * @param groupTotalDto
	 * @param monthlyGroupResults
	 * @param groupDemandEstimations
	 * @param planDemandCoefficient
	 * @param isToday
	 */
	protected void setGroup(Result result, GroupTotalDto groupTotalDto, GroupTotalDto monthlyGroupResults,
			MonthlyGroupDemandEstimations groupDemandEstimations, BigDecimal planDemandCoefficient, boolean isToday) {

		if (groupDemandEstimations == null) {
			groupDemandEstimations = new MonthlyGroupDemandEstimations();
		}

		// totalはランクだけを集計するのでよい＝ 品種別のTotalと同じになる
		setGroupEntity(result.group.total, result.group.rankA, isToday, groupTotalDto.getRankANumber(),
				monthlyGroupResults.getRankANumber(), groupTotalDto.getRankASales(),
				monthlyGroupResults.getRankASales(), groupTotalDto.getRankAMargin(),
				monthlyGroupResults.getRankAMargin(), monthlyGroupResults.getRankAOwnCount(),
				monthlyGroupResults.getRankADealCount(), monthlyGroupResults.getRankAAccountMasterCount(),
				monthlyGroupResults.getRankACallCount(), monthlyGroupResults.getRankAInfoCount(),
				groupDemandEstimations.getRankAYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(result.group.total, result.group.rankB, isToday, groupTotalDto.getRankBNumber(),
				monthlyGroupResults.getRankBNumber(), groupTotalDto.getRankBSales(),
				monthlyGroupResults.getRankBSales(), groupTotalDto.getRankBMargin(),
				monthlyGroupResults.getRankBMargin(), monthlyGroupResults.getRankBOwnCount(),
				monthlyGroupResults.getRankBDealCount(), monthlyGroupResults.getRankBAccountMasterCount(),
				monthlyGroupResults.getRankBCallCount(), monthlyGroupResults.getRankBInfoCount(),
				groupDemandEstimations.getRankBYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(result.group.total, result.group.rankC, isToday, groupTotalDto.getRankCNumber(),
				monthlyGroupResults.getRankCNumber(), groupTotalDto.getRankCSales(),
				monthlyGroupResults.getRankCSales(), groupTotalDto.getRankCMargin(),
				monthlyGroupResults.getRankCMargin(), monthlyGroupResults.getRankCOwnCount(),
				monthlyGroupResults.getRankCDealCount(), monthlyGroupResults.getRankCAccountMasterCount(),
				monthlyGroupResults.getRankCCallCount(), monthlyGroupResults.getRankCInfoCount(),
				groupDemandEstimations.getRankCYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.important, isToday, groupTotalDto.getImportantNumber(),
				monthlyGroupResults.getImportantNumber(), groupTotalDto.getImportantSales(),
				monthlyGroupResults.getImportantSales(), groupTotalDto.getImportantMargin(),
				monthlyGroupResults.getImportantMargin(), monthlyGroupResults.getImportantOwnCount(),
				monthlyGroupResults.getImportantDealCount(), monthlyGroupResults.getImportantAccountMasterCount(),
				monthlyGroupResults.getImportantCallCount(), monthlyGroupResults.getImportantInfoCount(),
				groupDemandEstimations.getImportantYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.newCustomer, isToday, groupTotalDto.getNewNumber(),
				monthlyGroupResults.getNewNumber(), groupTotalDto.getNewSales(), monthlyGroupResults.getNewSales(),
				groupTotalDto.getNewMargin(), monthlyGroupResults.getNewMargin(), monthlyGroupResults.getNewOwnCount(),
				monthlyGroupResults.getNewDealCount(), monthlyGroupResults.getNewAccountMasterCount(),
				monthlyGroupResults.getNewCallCount(), monthlyGroupResults.getNewInfoCount(),
				groupDemandEstimations.getNewYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.deepPlowing, isToday, groupTotalDto.getDeepPlowingNumber(),
				monthlyGroupResults.getDeepPlowingNumber(), groupTotalDto.getDeepPlowingSales(),
				monthlyGroupResults.getDeepPlowingSales(), groupTotalDto.getDeepPlowingMargin(),
				monthlyGroupResults.getDeepPlowingMargin(), monthlyGroupResults.getDeepPlowingOwnCount(),
				monthlyGroupResults.getDeepPlowingDealCount(), monthlyGroupResults.getDeepPlowingAccountMasterCount(),
				monthlyGroupResults.getDeepPlowingCallCount(), monthlyGroupResults.getDeepPlowingInfoCount(),
				groupDemandEstimations.getDeepPlowingYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.yCp, isToday, groupTotalDto.getYCpNumber(),
				monthlyGroupResults.getYCpNumber(), groupTotalDto.getYCpSales(), monthlyGroupResults.getYCpSales(),
				groupTotalDto.getYCpMargin(), monthlyGroupResults.getYCpMargin(), monthlyGroupResults.getYCpOwnCount(),
				monthlyGroupResults.getYCpDealCount(), monthlyGroupResults.getYCpAccountMasterCount(),
				monthlyGroupResults.getYCpCallCount(), monthlyGroupResults.getYCpInfoCount(),
				groupDemandEstimations.getYCpYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.otherCp, isToday, groupTotalDto.getOtherCpNumber(),
				monthlyGroupResults.getOtherCpNumber(), groupTotalDto.getOtherCpSales(),
				monthlyGroupResults.getOtherCpSales(), groupTotalDto.getOtherCpMargin(),
				monthlyGroupResults.getOtherCpMargin(), monthlyGroupResults.getOtherCpOwnCount(),
				monthlyGroupResults.getOtherCpDealCount(), monthlyGroupResults.getOtherCpAccountMasterCount(),
				monthlyGroupResults.getOtherCpCallCount(), monthlyGroupResults.getOtherCpInfoCount(),
				groupDemandEstimations.getOtherCpYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.outside, isToday, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
				BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
				monthlyGroupResults.getOutsideCallCount(), monthlyGroupResults.getOutsideInfoCount(), BigDecimal.ZERO,
				BigDecimal.ZERO);

		setGroupEntity(null, result.group.groupOne, isToday, groupTotalDto.getGroupOneNumber(),
				monthlyGroupResults.getGroupOneNumber(), groupTotalDto.getGroupOneSales(),
				monthlyGroupResults.getGroupOneSales(), groupTotalDto.getGroupOneMargin(),
				monthlyGroupResults.getGroupOneMargin(), monthlyGroupResults.getGroupOneOwnCount(),
				monthlyGroupResults.getGroupOneDealCount(), monthlyGroupResults.getGroupOneAccountMasterCount(),
				monthlyGroupResults.getGroupOneCallCount(), monthlyGroupResults.getGroupOneInfoCount(),
				groupDemandEstimations.getGroupOneYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.groupTwo, isToday, groupTotalDto.getGroupTwoNumber(),
				monthlyGroupResults.getGroupTwoNumber(), groupTotalDto.getGroupTwoSales(),
				monthlyGroupResults.getGroupTwoSales(), groupTotalDto.getGroupTwoMargin(),
				monthlyGroupResults.getGroupTwoMargin(), monthlyGroupResults.getGroupTwoOwnCount(),
				monthlyGroupResults.getGroupTwoDealCount(), monthlyGroupResults.getGroupTwoAccountMasterCount(),
				monthlyGroupResults.getGroupTwoCallCount(), monthlyGroupResults.getGroupTwoInfoCount(),
				groupDemandEstimations.getGroupTwoYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.groupThree, isToday, groupTotalDto.getGroupThreeNumber(),
				monthlyGroupResults.getGroupThreeNumber(), groupTotalDto.getGroupThreeSales(),
				monthlyGroupResults.getGroupThreeSales(), groupTotalDto.getGroupThreeMargin(),
				monthlyGroupResults.getGroupThreeMargin(), monthlyGroupResults.getGroupThreeOwnCount(),
				monthlyGroupResults.getGroupThreeDealCount(), monthlyGroupResults.getGroupThreeAccountMasterCount(),
				monthlyGroupResults.getGroupThreeCallCount(), monthlyGroupResults.getGroupThreeInfoCount(),
				groupDemandEstimations.getGroupThreeYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.groupFour, isToday, groupTotalDto.getGroupFourNumber(),
				monthlyGroupResults.getGroupFourNumber(), groupTotalDto.getGroupFourSales(),
				monthlyGroupResults.getGroupFourSales(), groupTotalDto.getGroupFourMargin(),
				monthlyGroupResults.getGroupFourMargin(), monthlyGroupResults.getGroupFourOwnCount(),
				monthlyGroupResults.getGroupFourDealCount(), monthlyGroupResults.getGroupFourAccountMasterCount(),
				monthlyGroupResults.getGroupFourCallCount(), monthlyGroupResults.getGroupFourInfoCount(),
				groupDemandEstimations.getGroupFourYearDemandEstimation(), planDemandCoefficient);

		setGroupEntity(null, result.group.groupFive, isToday, groupTotalDto.getGroupFiveNumber(),
				monthlyGroupResults.getGroupFiveNumber(), groupTotalDto.getGroupFiveSales(),
				monthlyGroupResults.getGroupFiveSales(), groupTotalDto.getGroupFiveMargin(),
				monthlyGroupResults.getGroupFiveMargin(), monthlyGroupResults.getGroupFiveOwnCount(),
				monthlyGroupResults.getGroupFiveDealCount(), monthlyGroupResults.getGroupFiveAccountMasterCount(),
				monthlyGroupResults.getGroupFiveCallCount(), monthlyGroupResults.getGroupFiveInfoCount(),
				groupDemandEstimations.getGroupFiveYearDemandEstimation(), planDemandCoefficient);

		// Groupの計画は販路のものをいれる
		result.group.total.number.plan = result.market.total.number.plan;
		result.group.total.visit.plan = result.call.total.visit.plan;

		calcGroupRate(result.group.total, result.group.rankA, result.group.rankB, result.group.rankC,
				result.group.important, result.group.newCustomer, result.group.deepPlowing, result.group.yCp,
				result.group.otherCp, result.group.outside, result.group.groupOne, result.group.groupTwo,
				result.group.groupThree, result.group.groupFour, result.group.groupFive);

	}

	/**
	 * グルーピングの1データを設定
	 * 
	 * @param total
	 * @param group
	 * @param isToday
	 * @param dailyNumber
	 * @param monthlyNumber
	 * @param dailySales
	 * @param monthlySales
	 * @param dailyMargin
	 * @param monthlyMargin
	 * @param havingCount
	 * @param deal
	 * @param dealMCount
	 * @param visit
	 * @param info
	 * @param demandEstimation
	 * @param planDemandCoefficient
	 */
	protected void setGroupEntity(GroupEntity total, GroupEntity group, boolean isToday, BigDecimal dailyNumber,
			BigDecimal monthlyNumber, BigDecimal dailySales, BigDecimal monthlySales, BigDecimal dailyMargin,
			BigDecimal monthlyMargin, BigDecimal havingCount, BigDecimal deal, BigDecimal dealMCount, BigDecimal visit,
			BigDecimal info, BigDecimal demandEstimation, BigDecimal planDemandCoefficient) {

		group.number.current = toDefaultZero(dailyNumber);
		group.number.monthly = calcMonthly(monthlyNumber, group.number.current, isToday);

		group.number.demandEstimation = toDefaultZero(demandEstimation).multiply(planDemandCoefficient).setScale(0,
				RoundingMode.HALF_UP);
		group.number.issEstimation = toPercentage(group.number.monthly, group.number.demandEstimation);

		group.sales.current = toDefaultZero(dailySales);
		group.sales.monthly = calcMonthly(monthlySales, group.sales.current, isToday);

		group.margin.current = toDefaultZero(dailyMargin);
		group.margin.monthly = calcMonthly(monthlyMargin, group.margin.current, isToday);

		group.having.count = toDefaultZero(havingCount);
		group.having.deal = toDefaultZero(deal);
		group.having.dealRate = toPercentage(group.having.deal, group.having.count);
		group.having.dealMCount = toDefaultZero(dealMCount);

		group.visit.monthly = toDefaultZero(visit);
		group.visit.info = toDefaultZero(info);

		if (total != null) {
			total.number.current = add(total.number.current, group.number.current);
			total.number.monthly = add(total.number.monthly, group.number.monthly);
			total.number.demandEstimation = add(total.number.demandEstimation, group.number.demandEstimation);
			total.sales.current = add(total.sales.current, group.sales.current);
			total.sales.monthly = add(total.sales.monthly, group.sales.monthly);

			total.margin.current = add(total.margin.current, group.margin.current);
			total.margin.monthly = add(total.margin.monthly, group.margin.monthly);

			total.having.count = add(total.having.count, group.having.count);
			total.having.deal = add(total.having.deal, group.having.deal);
			total.having.dealMCount = add(total.having.dealMCount, group.having.dealMCount);

			total.visit.monthly = add(total.visit.monthly, group.visit.monthly);
			total.visit.info = add(total.visit.info, group.visit.info);

		}
	}

	/**
	 * グルーピングのrate関連を設定
	 * 
	 * @param total
	 * @param groups
	 */
	protected void calcGroupRate(GroupEntity total, GroupEntity... groups) {

		total.number.achievementRate = toPercentage(total.number.monthly, total.number.plan);

		total.sales.achievementRate = toPercentage(total.sales.monthly, total.sales.plan);

		total.margin.achievementRate = toPercentage(total.margin.monthly, total.margin.plan);

		total.having.dealRate = toPercentage(total.having.deal, total.having.count);

		for (GroupEntity group : groups) {
			group.number.componentRate = toPercentage(group.number.monthly, total.number.monthly);
			group.number.demandEstimationComponentRate = toPercentage(group.number.demandEstimation,
					total.number.demandEstimation);
			group.sales.componentRate = toPercentage(group.sales.monthly, total.sales.monthly);
			group.margin.componentRate = toPercentage(group.margin.monthly, total.margin.monthly);
			group.visit.componentRate = toPercentage(group.visit.monthly, total.visit.monthly);

			group.sales.plan = toK(group.sales.plan);
			group.sales.current = toK(group.sales.current);
			group.sales.monthly = toK(group.sales.monthly);
			group.margin.plan = toK(group.margin.plan);
			group.margin.monthly = toK(group.margin.monthly);
			group.margin.current = toK(group.margin.current);

		}

		total.number.achievementRate = toPercentage(total.number.monthly, total.number.plan);
		total.sales.achievementRate = toPercentage(total.sales.monthly, total.sales.plan);
		total.margin.achievementRate = toPercentage(total.margin.monthly, total.margin.plan);
		total.visit.achievementRate = toPercentage(total.visit.monthly, total.visit.plan);

		total.sales.plan = toK(total.sales.plan);
		total.sales.current = toK(total.sales.current);
		total.sales.monthly = toK(total.sales.monthly);
		total.margin.plan = toK(total.margin.plan);
		total.margin.monthly = toK(total.margin.monthly);
		total.margin.current = toK(total.margin.current);
	}

	/**
	 * 当月累計を計算します。
	 * 
	 * 前日のデータを表示する場合は当日に情報を表示するが、その情報を当月累計に足しこまない。
	 * 
	 * @param monthly
	 * @param daily
	 * @param isToday
	 * @return
	 */
	protected static BigDecimal calcMonthly(BigDecimal monthly, BigDecimal daily, boolean isToday) {
		if (monthly == null) {
			monthly = BigDecimal.ZERO;
		}

		if (isToday) {
			return add(monthly, daily);
		}
		return monthly;
	}

	public static class Result {
		public CallTable call = new CallTable();

		public TotalTable total = new TotalTable();

		public KindTable kind = new KindTable();

		public MarketTable market = new MarketTable();

		public GroupTable group = new GroupTable();

		public CallTable getCall() {
			return call;
		}

		public void setCall(CallTable call) {
			this.call = call;
		}

		public TotalTable getTotal() {
			return total;
		}

		public void setTotal(TotalTable total) {
			this.total = total;
		}

		public KindTable getKind() {
			return kind;
		}

		public void setKind(KindTable kind) {
			this.kind = kind;
		}

		public MarketTable getMarket() {
			return market;
		}

		public void setMarket(MarketTable market) {
			this.market = market;
		}

		public GroupTable getGroup() {
			return group;
		}

		public void setGroup(GroupTable group) {
			this.group = group;
		}

	}

	public static class TotalTable {
		public KindEntity total = new KindEntity();
		public KindEntity summer = new KindEntity();
		public KindEntity snow = new KindEntity();
		public KindEntity other = new KindEntity();

		public KindEntity getTotal() {
			return total;
		}

		public void setTotal(KindEntity total) {
			this.total = total;
		}

		public KindEntity getSummer() {
			return summer;
		}

		public void setSummer(KindEntity summer) {
			this.summer = summer;
		}

		public KindEntity getSnow() {
			return snow;
		}

		public void setSnow(KindEntity snow) {
			this.snow = snow;
		}

		public KindEntity getOther() {
			return other;
		}

		public void setOther(KindEntity other) {
			this.other = other;
		}
	}

	public static class Info {
		/** 計画 */
		public BigDecimal plan = BigDecimal.ZERO;
		/** 当日 */
		public BigDecimal count = BigDecimal.ZERO;
		/** 達成率 */
		public BigDecimal achievementRate = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getCount() {
			return count;
		}

		public void setCount(BigDecimal count) {
			this.count = count;
		}

		public BigDecimal getAchievementRate() {
			return achievementRate;
		}

		public void setAchievementRate(BigDecimal achievementRate) {
			this.achievementRate = achievementRate;
		}

	}

	public static class Achievement {
		/** 計画 */
		public BigDecimal plan = BigDecimal.ZERO;
		/** 当日 */
		public BigDecimal current = BigDecimal.ZERO;
		/** 当月累計 */
		public BigDecimal monthly = BigDecimal.ZERO;
		/** 達成率 */
		public BigDecimal achievementRate = BigDecimal.ZERO;
		/** 構成比 */
		public BigDecimal componentRate = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getCurrent() {
			return current;
		}

		public void setCurrent(BigDecimal current) {
			this.current = current;
		}

		public BigDecimal getMonthly() {
			return monthly;
		}

		public void setMonthly(BigDecimal monthly) {
			this.monthly = monthly;
		}

		public BigDecimal getAchievementRate() {
			return achievementRate;
		}

		public void setAchievementRate(BigDecimal achievementRate) {
			this.achievementRate = achievementRate;
		}

		public BigDecimal getComponentRate() {
			return componentRate;
		}

		public void setComponentRate(BigDecimal componentRate) {
			this.componentRate = componentRate;
		}
	}

	public static class KindEntity {
		public NumberResult number = new NumberResult();
		public Achievement sales = new Achievement();
		public Margin margin = new Margin();

		public NumberResult getNumber() {
			return number;
		}

		public void setNumber(NumberResult number) {
			this.number = number;
		}

		public Achievement getSales() {
			return sales;
		}

		public void setSales(Achievement sales) {
			this.sales = sales;
		}

		public Margin getMargin() {
			return margin;
		}

		public void setMargin(Margin margin) {
			this.margin = margin;
		}

	}

	public static class KindTable {
		public KindEntity total = new KindEntity();

		public KindEntity pcrTotal = new KindEntity();
		public KindEntity pcrSummer = new KindEntity();
		public KindEntity pcrPureSnow = new KindEntity();
		public KindEntity vanTotal = new KindEntity();
		public KindEntity vanSummer = new KindEntity();
		public KindEntity vanPureSnow = new KindEntity();
		public KindEntity pcbTotal = new KindEntity();
		public KindEntity pcbSummer = new KindEntity();
		public KindEntity pcbPureSnow = new KindEntity();
		public KindEntity ltTotal = new KindEntity();
		public KindEntity ltSummer = new KindEntity();
		public KindEntity ltSnow = new KindEntity();
		public KindEntity ltAs = new KindEntity();
		public KindEntity ltPureSnow = new KindEntity();
		public KindEntity tbTotal = new KindEntity();
		public KindEntity tbSummer = new KindEntity();
		public KindEntity tbSnow = new KindEntity();
		public KindEntity tbAs = new KindEntity();
		public KindEntity tbPureSnow = new KindEntity();
		public KindEntity orId = new KindEntity();
		public KindEntity or = new KindEntity();
		public KindEntity id = new KindEntity();
		public KindEntity tifu = new KindEntity();
		public KindEntity other = new KindEntity();
		public KindEntity work = new KindEntity();
		public KindEntity retread = new KindEntity();

		public KindEntity getTotal() {
			return total;
		}

		public void setTotal(KindEntity total) {
			this.total = total;
		}

		public KindEntity getPcrTotal() {
			return pcrTotal;
		}

		public void setPcrTotal(KindEntity pcrTotal) {
			this.pcrTotal = pcrTotal;
		}

		public KindEntity getPcrSummer() {
			return pcrSummer;
		}

		public void setPcrSummer(KindEntity pcrSummer) {
			this.pcrSummer = pcrSummer;
		}

		public KindEntity getPcrPureSnow() {
			return pcrPureSnow;
		}

		public void setPcrPureSnow(KindEntity pcrPureSnow) {
			this.pcrPureSnow = pcrPureSnow;
		}

		public KindEntity getVanTotal() {
			return vanTotal;
		}

		public void setVanTotal(KindEntity vanTotal) {
			this.vanTotal = vanTotal;
		}

		public KindEntity getVanSummer() {
			return vanSummer;
		}

		public void setVanSummer(KindEntity vanSummer) {
			this.vanSummer = vanSummer;
		}

		public KindEntity getVanPureSnow() {
			return vanPureSnow;
		}

		public void setVanPureSnow(KindEntity vanPureSnow) {
			this.vanPureSnow = vanPureSnow;
		}

		public KindEntity getLtTotal() {
			return ltTotal;
		}

		public void setLtTotal(KindEntity ltTotal) {
			this.ltTotal = ltTotal;
		}

		public KindEntity getLtSummer() {
			return ltSummer;
		}

		public void setLtSummer(KindEntity ltSummer) {
			this.ltSummer = ltSummer;
		}

		public KindEntity getLtSnow() {
			return ltSnow;
		}

		public void setLtSnow(KindEntity ltSnow) {
			this.ltSnow = ltSnow;
		}

		public KindEntity getLtAs() {
			return ltAs;
		}

		public void setLtAs(KindEntity ltAs) {
			this.ltAs = ltAs;
		}

		public KindEntity getLtPureSnow() {
			return ltPureSnow;
		}

		public void setLtPureSnow(KindEntity ltPureSnow) {
			this.ltPureSnow = ltPureSnow;
		}

		public KindEntity getTbTotal() {
			return tbTotal;
		}

		public void setTbTotal(KindEntity tbTotal) {
			this.tbTotal = tbTotal;
		}

		public KindEntity getTbSummer() {
			return tbSummer;
		}

		public void setTbSummer(KindEntity tbSummer) {
			this.tbSummer = tbSummer;
		}

		public KindEntity getTbSnow() {
			return tbSnow;
		}

		public void setTbSnow(KindEntity tbSnow) {
			this.tbSnow = tbSnow;
		}

		public KindEntity getTbAs() {
			return tbAs;
		}

		public void setTbAs(KindEntity tbAs) {
			this.tbAs = tbAs;
		}

		public KindEntity getTbPureSnow() {
			return tbPureSnow;
		}

		public void setTbPureSnow(KindEntity tbPureSnow) {
			this.tbPureSnow = tbPureSnow;
		}

		public KindEntity getOrId() {
			return orId;
		}

		public void setOrId(KindEntity orId) {
			this.orId = orId;
		}

		public KindEntity getId() {
			return id;
		}

		public void setId(KindEntity id) {
			this.id = id;
		}

		public KindEntity getOr() {
			return or;
		}

		public void setOr(KindEntity or) {
			this.or = or;
		}

		public KindEntity getTifu() {
			return tifu;
		}

		public void setTifu(KindEntity tifu) {
			this.tifu = tifu;
		}

		public KindEntity getOther() {
			return other;
		}

		public void setOther(KindEntity other) {
			this.other = other;
		}

		public KindEntity getWork() {
			return work;
		}

		public void setWork(KindEntity work) {
			this.work = work;
		}

		public KindEntity getPcbTotal() {
			return pcbTotal;
		}

		public void setPcbTotal(KindEntity pcbTotal) {
			this.pcbTotal = pcbTotal;
		}

		public KindEntity getPcbPureSnow() {
			return pcbPureSnow;
		}

		public void setPcbPureSnow(KindEntity pcbPureSnow) {
			this.pcbPureSnow = pcbPureSnow;
		}

		public KindEntity getPcbSummer() {
			return pcbSummer;
		}

		public void setPcbSummer(KindEntity pcbSummer) {
			this.pcbSummer = pcbSummer;
		}

	}

	public static class MarketEntity {
		public NumberResult number = new NumberResult();
		public Achievement sales = new Achievement();
		public Margin margin = new Margin();

		public NumberResult getNumber() {
			return number;
		}

		public void setNumber(NumberResult number) {
			this.number = number;
		}

		public Achievement getSales() {
			return sales;
		}

		public void setSales(Achievement sales) {
			this.sales = sales;
		}

		public Margin getMargin() {
			return margin;
		}

		public void setMargin(Margin margin) {
			this.margin = margin;
		}
	}

	public static class MarketTable {
		public MarketEntity total = new MarketEntity();
		public MarketEntity ss = new MarketEntity();
		public MarketEntity rs = new MarketEntity();
		public MarketEntity cd = new MarketEntity();
		public MarketEntity ps = new MarketEntity();
		public MarketEntity cs = new MarketEntity();
		public MarketEntity hc = new MarketEntity();
		public MarketEntity lease = new MarketEntity();
		public MarketEntity indirectOther = new MarketEntity();
		public MarketEntity indirectTotal = new MarketEntity();
		public MarketEntity truck = new MarketEntity();
		public MarketEntity bus = new MarketEntity();
		public MarketEntity hireTaxi = new MarketEntity();
		public MarketEntity directOther = new MarketEntity();
		public MarketEntity directTotal = new MarketEntity();
		public MarketEntity retail = new MarketEntity();

		public MarketEntity getTotal() {
			return total;
		}

		public void setTotal(MarketEntity total) {
			this.total = total;
		}

		public MarketEntity getSs() {
			return ss;
		}

		public void setSs(MarketEntity ss) {
			this.ss = ss;
		}

		public MarketEntity getRs() {
			return rs;
		}

		public void setRs(MarketEntity rs) {
			this.rs = rs;
		}

		public MarketEntity getCd() {
			return cd;
		}

		public void setCd(MarketEntity cd) {
			this.cd = cd;
		}

		public MarketEntity getPs() {
			return ps;
		}

		public void setPs(MarketEntity ps) {
			this.ps = ps;
		}

		public MarketEntity getCs() {
			return cs;
		}

		public void setCs(MarketEntity cs) {
			this.cs = cs;
		}

		public MarketEntity getHc() {
			return hc;
		}

		public void setHc(MarketEntity hc) {
			this.hc = hc;
		}

		public MarketEntity getLease() {
			return lease;
		}

		public void setLease(MarketEntity lease) {
			this.lease = lease;
		}

		public MarketEntity getIndirectOther() {
			return indirectOther;
		}

		public void setIndirectOther(MarketEntity indirectOther) {
			this.indirectOther = indirectOther;
		}

		public MarketEntity getIndirectTotal() {
			return indirectTotal;
		}

		public void setIndirectTotal(MarketEntity indirectTotal) {
			this.indirectTotal = indirectTotal;
		}

		public MarketEntity getTruck() {
			return truck;
		}

		public void setTruck(MarketEntity truck) {
			this.truck = truck;
		}

		public MarketEntity getBus() {
			return bus;
		}

		public void setBus(MarketEntity bus) {
			this.bus = bus;
		}

		public MarketEntity getHireTaxi() {
			return hireTaxi;
		}

		public void setHireTaxi(MarketEntity hireTaxi) {
			this.hireTaxi = hireTaxi;
		}

		public MarketEntity getDirectOther() {
			return directOther;
		}

		public void setDirectOther(MarketEntity directOther) {
			this.directOther = directOther;
		}

		public MarketEntity getDirectTotal() {
			return directTotal;
		}

		public void setDirectTotal(MarketEntity directTotal) {
			this.directTotal = directTotal;
		}

		public MarketEntity getRetail() {
			return retail;
		}

		public void setRetail(MarketEntity retail) {
			this.retail = retail;
		}

	}

	public static class CallEntity {
		public Having having = new Having();
		public Visit visit = new Visit();
		public Info info = new Info();

		public Having getHaving() {
			return having;
		}

		public void setHaving(Having having) {
			this.having = having;
		}

		public Visit getVisit() {
			return visit;
		}

		public void setVisit(Visit visit) {
			this.visit = visit;
		}

		public Info getInfo() {
			return info;
		}

		public void setInfo(Info info) {
			this.info = info;
		}

	}

	public static class CallTable {
		public CallEntity total = new CallEntity();
		public CallEntity ss = new CallEntity();
		public CallEntity rs = new CallEntity();
		public CallEntity cd = new CallEntity();
		public CallEntity ps = new CallEntity();
		public CallEntity cs = new CallEntity();
		public CallEntity hc = new CallEntity();
		public CallEntity lease = new CallEntity();
		public CallEntity indirectOther = new CallEntity();
		public CallEntity indirectTotal = new CallEntity();
		public CallEntity truck = new CallEntity();
		public CallEntity bus = new CallEntity();
		public CallEntity hireTaxi = new CallEntity();
		public CallEntity directOther = new CallEntity();
		public CallEntity directTotal = new CallEntity();
		public CallEntity outside = new CallEntity();

		public CallEntity getTotal() {
			return total;
		}

		public void setTotal(CallEntity total) {
			this.total = total;
		}

		public CallEntity getSs() {
			return ss;
		}

		public void setSs(CallEntity ss) {
			this.ss = ss;
		}

		public CallEntity getRs() {
			return rs;
		}

		public void setRs(CallEntity rs) {
			this.rs = rs;
		}

		public CallEntity getCd() {
			return cd;
		}

		public void setCd(CallEntity cd) {
			this.cd = cd;
		}

		public CallEntity getPs() {
			return ps;
		}

		public void setPs(CallEntity ps) {
			this.ps = ps;
		}

		public CallEntity getCs() {
			return cs;
		}

		public void setCs(CallEntity cs) {
			this.cs = cs;
		}

		public CallEntity getHc() {
			return hc;
		}

		public void setHc(CallEntity hc) {
			this.hc = hc;
		}

		public CallEntity getLease() {
			return lease;
		}

		public void setLease(CallEntity lease) {
			this.lease = lease;
		}

		public CallEntity getIndirectOther() {
			return indirectOther;
		}

		public void setIndirectOther(CallEntity indirectOther) {
			this.indirectOther = indirectOther;
		}

		public CallEntity getIndirectTotal() {
			return indirectTotal;
		}

		public void setIndirectTotal(CallEntity indirectTotal) {
			this.indirectTotal = indirectTotal;
		}

		public CallEntity getTruck() {
			return truck;
		}

		public void setTruck(CallEntity truck) {
			this.truck = truck;
		}

		public CallEntity getBus() {
			return bus;
		}

		public void setBus(CallEntity bus) {
			this.bus = bus;
		}

		public CallEntity getHireTaxi() {
			return hireTaxi;
		}

		public void setHireTaxi(CallEntity hireTaxi) {
			this.hireTaxi = hireTaxi;
		}

		public CallEntity getDirectOther() {
			return directOther;
		}

		public void setDirectOther(CallEntity directOther) {
			this.directOther = directOther;
		}

		public CallEntity getDirectTotal() {
			return directTotal;
		}

		public void setDirectTotal(CallEntity directTotal) {
			this.directTotal = directTotal;
		}

		public CallEntity getOutside() {
			return outside;
		}

		public void setOutside(CallEntity outside) {
			this.outside = outside;
		}

	}

	public static class GroupEntity {
		public NumberResult number = new NumberResult();
		public Achievement sales = new Achievement();
		public Margin margin = new Margin();
		public Having having = new Having();
		public Visit visit = new Visit();

		public NumberResult getNumber() {
			return number;
		}

		public void setNumber(NumberResult number) {
			this.number = number;
		}

		public Achievement getSales() {
			return sales;
		}

		public void setSales(Achievement sales) {
			this.sales = sales;
		}

		public Margin getMargin() {
			return margin;
		}

		public void setMargin(Margin margin) {
			this.margin = margin;
		}

		public Having getHaving() {
			return having;
		}

		public void setHaving(Having having) {
			this.having = having;
		}

		public Visit getVisit() {
			return visit;
		}

		public void setVisit(Visit visit) {
			this.visit = visit;
		}

	}

	public static class GroupTable {
		public GroupEntity total = new GroupEntity();
		public GroupEntity rankA = new GroupEntity();
		public GroupEntity rankB = new GroupEntity();
		public GroupEntity rankC = new GroupEntity();
		public GroupEntity important = new GroupEntity();
		public GroupEntity newCustomer = new GroupEntity();
		public GroupEntity deepPlowing = new GroupEntity();
		public GroupEntity yCp = new GroupEntity();
		public GroupEntity otherCp = new GroupEntity();
		public GroupEntity outside = new GroupEntity();
		public GroupEntity groupOne = new GroupEntity();
		public GroupEntity groupTwo = new GroupEntity();
		public GroupEntity groupThree = new GroupEntity();
		public GroupEntity groupFour = new GroupEntity();
		public GroupEntity groupFive = new GroupEntity();

		public GroupEntity getTotal() {
			return total;
		}

		public void setTotal(GroupEntity total) {
			this.total = total;
		}

		public GroupEntity getRankA() {
			return rankA;
		}

		public void setRankA(GroupEntity rankA) {
			this.rankA = rankA;
		}

		public GroupEntity getRankB() {
			return rankB;
		}

		public void setRankB(GroupEntity rankB) {
			this.rankB = rankB;
		}

		public GroupEntity getRankC() {
			return rankC;
		}

		public void setRankC(GroupEntity rankC) {
			this.rankC = rankC;
		}

		public GroupEntity getImportant() {
			return important;
		}

		public void setImportant(GroupEntity important) {
			this.important = important;
		}

		public GroupEntity getNewCustomer() {
			return newCustomer;
		}

		public void setNewCustomer(GroupEntity newCustomer) {
			this.newCustomer = newCustomer;
		}

		public GroupEntity getDeepPlowing() {
			return deepPlowing;
		}

		public void setDeepPlowing(GroupEntity deepPlowing) {
			this.deepPlowing = deepPlowing;
		}

		public GroupEntity getYCp() {
			return yCp;
		}

		public void setYCp(GroupEntity yCp) {
			this.yCp = yCp;
		}

		public GroupEntity getOtherCp() {
			return otherCp;
		}

		public void setOtherCp(GroupEntity otherCp) {
			this.otherCp = otherCp;
		}

		public GroupEntity getOutside() {
			return outside;
		}

		public void setOutside(GroupEntity outside) {
			this.outside = outside;
		}

		public GroupEntity getGroupOne() {
			return groupOne;
		}

		public void setGroupOne(GroupEntity groupOne) {
			this.groupOne = groupOne;
		}

		public GroupEntity getGroupTwo() {
			return groupTwo;
		}

		public void setGroupTwo(GroupEntity groupTwo) {
			this.groupTwo = groupTwo;
		}

		public GroupEntity getGroupThree() {
			return groupThree;
		}

		public void setGroupThree(GroupEntity groupThree) {
			this.groupThree = groupThree;
		}

		public GroupEntity getGroupFour() {
			return groupFour;
		}

		public void setGroupFour(GroupEntity groupFour) {
			this.groupFour = groupFour;
		}

		public GroupEntity getGroupFive() {
			return groupFive;
		}

		public void setGroupFive(GroupEntity groupFive) {
			this.groupFive = groupFive;
		}

	}

	public static class Margin extends Achievement {

		public BigDecimal marginRate = BigDecimal.ZERO;

		public BigDecimal getMarginRate() {
			return marginRate;
		}

		public void setMarginRate(BigDecimal marginRate) {
			this.marginRate = marginRate;
		}

	}

	public static class Visit extends Achievement {
		public BigDecimal info = BigDecimal.ZERO;

		public BigDecimal getInfo() {
			return info;
		}

		public void setInfo(BigDecimal info) {
			this.info = info;
		}

	}

	public static class NumberResult extends Achievement {
		/** 推定需要 */
		public BigDecimal demandEstimation = BigDecimal.ZERO;
		/** 推定需要構成比 */
		public BigDecimal demandEstimationComponentRate = BigDecimal.ZERO;
		/** 推定ISS */
		public BigDecimal issEstimation = BigDecimal.ZERO;
		/** 預り */
		public BigDecimal custody = BigDecimal.ZERO;
		/** 預り率 */
		public BigDecimal custodyRate = BigDecimal.ZERO;

		public BigDecimal getDemandEstimation() {
			return demandEstimation;
		}

		public void setDemandEstimation(BigDecimal demandEstimation) {
			this.demandEstimation = demandEstimation;
		}

		public BigDecimal getDemandEstimationComponentRate() {
			return demandEstimationComponentRate;
		}

		public void setDemandEstimationComponentRate(BigDecimal demandEstimationComponentRate) {
			this.demandEstimationComponentRate = demandEstimationComponentRate;
		}

		public BigDecimal getIssEstimation() {
			return issEstimation;
		}

		public void setIssEstimation(BigDecimal issEstimation) {
			this.issEstimation = issEstimation;
		}

		public BigDecimal getCustody() {
			return custody;
		}

		public void setCustody(BigDecimal custody) {
			this.custody = custody;
		}

		public BigDecimal getCustodyRate() {
			return custodyRate;
		}

		public void setCustodyRate(BigDecimal custodyRate) {
			this.custodyRate = custodyRate;
		}

	}

	public static class Having {
		/** 計画 */
		public BigDecimal plan = BigDecimal.ZERO;
		/** 持ち軒数 */
		public BigDecimal count = BigDecimal.ZERO;
		/** 取引件数 */
		public BigDecimal deal = BigDecimal.ZERO;
		/** 達成率 */
		public BigDecimal achievementRate = BigDecimal.ZERO;
		/** 取引率 */
		public BigDecimal dealRate = BigDecimal.ZERO;
		/** 取引得M数 */
		public BigDecimal dealMCount = BigDecimal.ZERO;

		public BigDecimal getPlan() {
			return plan;
		}

		public void setPlan(BigDecimal plan) {
			this.plan = plan;
		}

		public BigDecimal getCount() {
			return count;
		}

		public void setCount(BigDecimal count) {
			this.count = count;
		}

		public BigDecimal getDeal() {
			return deal;
		}

		public void setDeal(BigDecimal deal) {
			this.deal = deal;
		}

		public BigDecimal getAchievementRate() {
			return achievementRate;
		}

		public void setAchievementRate(BigDecimal achievementRate) {
			this.achievementRate = achievementRate;
		}

		public BigDecimal getDealRate() {
			return dealRate;
		}

		public void setDealRate(BigDecimal dealRate) {
			this.dealRate = dealRate;
		}

		public BigDecimal getDealMCount() {
			return dealMCount;
		}

		public void setDealMCount(BigDecimal dealMCount) {
			this.dealMCount = dealMCount;
		}

	}
}
