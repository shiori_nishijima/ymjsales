package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;

/**
 * 検索用年月リストを作成します。
 */
@Component
public class SelectYearsHelper {

	/**
	 * 過去年From
	 */
	@Value("${show.year.range.from}")
	String rangeFrom;

	/**
	 * 未来年To
	 */
	@Value("${show.year.range.to}")
	String rangeTo;

	/**
	 * 年月リストを生成し、モデルに設定します。
	 * 
	 * @param model モデル
	 */
	public void create(Model model) {
		List<Integer> years = new ArrayList<>();
		List<Integer> months = new ArrayList<>();
		int from = Integer.parseInt(rangeFrom);
		int to = Integer.parseInt(rangeTo);
		makeYearsList(years, from, to);
		makeMonthsList(months);
		model.addAttribute("years", years).addAttribute("months", months);
	}

	/**
	 * 年月日リストを生成し、モデルに設定します。
	 * 
	 * @param model モデル
	 */
	public void createYearMonthDate(Model model) {
		List<SelectItem> years = new ArrayList<>();
		List<SelectItemGroup> months = new ArrayList<>();
		List<SelectItemGroup> days = new ArrayList<>();

		int from = Integer.parseInt(rangeFrom);
		int to = Integer.parseInt(rangeTo);

		List<Integer> yearList = new ArrayList<>();
		makeYearsList(yearList, from, to);
		for (int year : yearList) {

			years.add(new SelectItem(String.valueOf(year), String.valueOf(year)));

			SelectItemGroup monthGroup = new SelectItemGroup(String.valueOf(year));
			monthGroup.setItems(new ArrayList<SelectItem>());
			months.add(monthGroup);

			for (int month = 1; month <= 12; month++) {
				monthGroup.getItems().add(
						new SelectItem(String.valueOf(month), String.valueOf(DateUtils.toYearMonth(year, month))));

				Calendar c = Calendar.getInstance();
				c.set(year, month - 1, 1, 0, 0, 0);
				c.set(Calendar.MILLISECOND, 0);
				int maxDay = c.getActualMaximum(Calendar.DATE);
				SelectItemGroup dayGroup = new SelectItemGroup(String.valueOf(DateUtils.toYearMonth(year, month)));
				dayGroup.setItems(new ArrayList<SelectItem>());
				days.add(dayGroup);
				dayGroup.getItems().add(new SelectItem(Constants.SelectItrem.GROUP_LABEL_ALL, String.valueOf(-1)));
				for (int day = 1; day <= maxDay; day++) {
					dayGroup.getItems().add(new SelectItem(String.valueOf(day), String.valueOf(day)));
				}
			}
		}
		model.addAttribute("years", years).addAttribute("months", months).addAttribute("dates", days);
	}

	/**
	 * 年月週リストを生成し、モデルに設定します。
	 * 
	 * @param model モデル
	 */
	public void createYearMonthWeek(Model model) {
		List<SelectItem> years = new ArrayList<>();
		List<SelectItemGroup> months = new ArrayList<>();
		List<SelectItemGroup> weeks = new ArrayList<>();

		int from = Integer.parseInt(rangeFrom);
		int to = Integer.parseInt(rangeTo);

		List<Integer> yearList = new ArrayList<>();
		makeYearsList(yearList, from, to);
		for (int year : yearList) {

			years.add(new SelectItem(String.valueOf(year), String.valueOf(year)));

			SelectItemGroup monthGroup = new SelectItemGroup(String.valueOf(year));
			monthGroup.setItems(new ArrayList<SelectItem>());
			months.add(monthGroup);

			for (int month = 1; month <= 12; month++) {
				monthGroup.getItems().add(
						new SelectItem(String.valueOf(month), String.valueOf(DateUtils.toYearMonth(year, month))));

				Calendar c = DateUtils.getCalendar();
				c.set(year, month - 1, 1, 0, 0, 0);
				c.set(Calendar.MILLISECOND, 0);

				int maxWeek = c.getActualMaximum(Calendar.WEEK_OF_MONTH);
				SelectItemGroup weekGroup = new SelectItemGroup(String.valueOf(DateUtils.toYearMonth(year, month)));
				weekGroup.setItems(new ArrayList<SelectItem>());
				weeks.add(weekGroup);
				for (int week = 1; week <= maxWeek; week++) {
					weekGroup.getItems().add(new SelectItem(String.valueOf(week), String.valueOf(week)));
				}
			}
		}
		model.addAttribute("years", years).addAttribute("months", months).addAttribute("weeks", weeks);
	}

	/**
	 * 年リストの値を作成します。
	 * 
	 * @param years 年リスト
	 * @param from 過去年From
	 * @param to 未来年To
	 */
	public void makeYearsList(List<Integer> years, int from, int to) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(DateUtils.getDBDate());
		int year = cal.get(Calendar.YEAR);

		for (int i = 0; i <= to; i++) {
			years.add(year);
			year++;
		}
		year = cal.get(Calendar.YEAR);
		for (int i = 0; i < from; i++) {
			year--;
			years.add(year);
		}
		Collections.sort(years);
		Collections.reverse(years);
	}

	/**
	 * 月リストの値を作成します。
	 * 
	 * @param months 月リスト
	 */
	public void makeMonthsList(List<Integer> months) {
		for (int i = 1; i <= 12; i++) {
			months.add(i);
		}
	}
}
