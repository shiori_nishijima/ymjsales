package jp.co.yrc.mv.helper;

import java.util.Map;

import jp.co.yrc.mv.common.Constants.MiddleClassification;
import jp.co.yrc.mv.dto.SimpleResult;
import jp.co.yrc.mv.common.MathUtils;
import jp.co.yrc.mv.entity.MonthlyKindPlans;
import jp.co.yrc.mv.entity.MonthlyKindResults;

import org.springframework.stereotype.Component;

@Component
public class KindHelper {

	public void setupResult(Map<String, SimpleResult> r, MonthlyKindResults monthlyKindResults) {
		r.put(MiddleClassification.PCR_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getPcrSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrSummerSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrSummerNumber())));
		r.put(MiddleClassification.PCR_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getPcrPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcrPureSnowNumber())));
		r.put(MiddleClassification.VAN_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getVanSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanSummerSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanSummerNumber())));
		r.put(MiddleClassification.VAN_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getVanPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getVanPureSnowNumber())));
		r.put(MiddleClassification.LT_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getLtSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtSummerSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtSummerNumber())));
		r.put(MiddleClassification.LT_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getLtSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtSnowSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getLtSnowNumber())));
		r.put(MiddleClassification.LT_AS,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getLtAsMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtAsHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtAsSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getLtAsNumber())));
		r.put(MiddleClassification.LT_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getLtPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getLtPureSnowNumber())));
		r.put(MiddleClassification.TB_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getTbSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbSummerSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbSummerNumber())));
		r.put(MiddleClassification.TB_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getTbSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbSnowSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getTbSnowNumber())));
		r.put(MiddleClassification.TB_AS,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getTbAsMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbAsHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbAsSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getTbAsNumber())));
		r.put(MiddleClassification.TB_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getTbPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getTbPureSnowNumber())));
		r.put(MiddleClassification.ORID,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getOrMargin()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getIdMargin())), MathUtils.toDefaultZero(
						monthlyKindResults.getOrHqMargin()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getIdHqMargin())), MathUtils.toDefaultZero(
						monthlyKindResults.getOrSales()).add(MathUtils.toDefaultZero(monthlyKindResults.getIdSales())),
						MathUtils.toDefaultZero(monthlyKindResults.getOrNumber()).add(
								MathUtils.toDefaultZero(monthlyKindResults.getIdNumber()))));
		r.put(MiddleClassification.ID,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getIdMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getIdHqMargin()), MathUtils.toDefaultZero(monthlyKindResults
						.getIdSales()), MathUtils.toDefaultZero(monthlyKindResults.getIdNumber())));
		r.put(MiddleClassification.OR,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getOrMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getOrHqMargin()), MathUtils.toDefaultZero(monthlyKindResults
						.getOrSales()), MathUtils.toDefaultZero(monthlyKindResults.getOrNumber())));
		r.put(MiddleClassification.TIFU,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getTifuMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTifuHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getTifuSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getTifuNumber())));
		r.put(MiddleClassification.OTHER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getOtherMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getOtherHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getOtherSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getOtherNumber())));
		r.put(MiddleClassification.WORK,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getWorkMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getWorkHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getWorkSales()), MathUtils.toDefaultZero(monthlyKindResults
						.getWorkNumber())));
		r.put(MiddleClassification.PCB_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getPcbSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbSummerSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbSummerNumber())));
		r.put(MiddleClassification.PCB_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getPcbPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindResults.getPcbPureSnowNumber())));
		r.put(MiddleClassification.PCB,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getPcbSummerMargin()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getPcbPureSnowMargin())), MathUtils.toDefaultZero(
						monthlyKindResults.getPcbSummerHqMargin()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getPcbPureSnowHqMargin())), MathUtils.toDefaultZero(
						monthlyKindResults.getPcbSummerSales()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getPcbPureSnowSales())), MathUtils.toDefaultZero(
						monthlyKindResults.getPcbSummerNumber()).add(
						MathUtils.toDefaultZero(monthlyKindResults.getPcbPureSnowNumber()))));
		r.put(MiddleClassification.RETREAD,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadNumber())));
		r.put(MiddleClassification.LT_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSummerSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSummerNumber())));
		r.put(MiddleClassification.TB_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSummerSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSummerNumber())));
		r.put(MiddleClassification.TIRE_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadSpecialTireSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadSpecialTireSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadSpecialTireSummerSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadSpecialTireSummerNumber())));
		r.put(MiddleClassification.LT_RETREAD_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSnowMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSnowHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSnowSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadLtSnowNumber())));
		r.put(MiddleClassification.TB_RETREAD_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSnowMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSnowHqMargin()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSnowSales()),
						MathUtils.toDefaultZero(monthlyKindResults.getRetreadTbSnowNumber())));
	}

	public void setupPlan(Map<String, SimpleResult> r, MonthlyKindPlans monthlyKindPlans) {

		r.put(MiddleClassification.PCR_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getPcrSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcrSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcrSummerSales()), MathUtils.toDefaultZero(monthlyKindPlans
						.getPcrSummerNumber())));
		r.put(MiddleClassification.PCR_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getPcrPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcrPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcrPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcrPureSnowNumber())));
		r.put(MiddleClassification.VAN_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getVanSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getVanSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getVanSummerSales()), MathUtils.toDefaultZero(monthlyKindPlans
						.getVanSummerNumber())));
		r.put(MiddleClassification.VAN_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getVanPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getVanPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getVanPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindPlans.getVanPureSnowNumber())));
		r.put(MiddleClassification.LT_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getLtSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getLtSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getLtSummerSales()), MathUtils.toDefaultZero(monthlyKindPlans
						.getLtSummerNumber())));
		r.put(MiddleClassification.LT_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getLtSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getLtSnowHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getLtSnowSales()), MathUtils.toDefaultZero(monthlyKindPlans.getLtSnowNumber())));
		r.put(MiddleClassification.TB_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getTbSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getTbSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getTbSummerSales()), MathUtils.toDefaultZero(monthlyKindPlans
						.getTbSummerNumber())));
		r.put(MiddleClassification.TB_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getTbSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getTbSnowHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getTbSnowSales()), MathUtils.toDefaultZero(monthlyKindPlans.getTbSnowNumber())));
		r.put(MiddleClassification.ORID,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getOrIdMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getOrIdHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getOrIdSales()), MathUtils.toDefaultZero(monthlyKindPlans.getOrIdNumber())));
		r.put(MiddleClassification.TIFU,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getTifuMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getTifuHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getTifuSales()), MathUtils.toDefaultZero(monthlyKindPlans.getTifuNumber())));
		r.put(MiddleClassification.OTHER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getOtherMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getOtherHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getOtherSales()), MathUtils.toDefaultZero(monthlyKindPlans.getOtherNumber())));
		r.put(MiddleClassification.WORK,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getWorkMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getWorkHqMargin()), MathUtils.toDefaultZero(monthlyKindPlans
						.getWorkSales()), MathUtils.toDefaultZero(monthlyKindPlans.getWorkNumber())));
		r.put(MiddleClassification.PCB_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getPcbSummerMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcbSummerHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcbSummerSales()), MathUtils.toDefaultZero(monthlyKindPlans
						.getPcbSummerNumber())));
		r.put(MiddleClassification.PCB_PURE_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getPcbPureSnowMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcbPureSnowHqMargin()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcbPureSnowSales()), MathUtils
						.toDefaultZero(monthlyKindPlans.getPcbPureSnowNumber())));
		r.put(MiddleClassification.PCB,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getPcbSummerMargin()).add(
						MathUtils.toDefaultZero(monthlyKindPlans.getPcbPureSnowMargin())), MathUtils.toDefaultZero(
						monthlyKindPlans.getPcbSummerHqMargin()).add(
						MathUtils.toDefaultZero(monthlyKindPlans.getPcbPureSnowHqMargin())), MathUtils.toDefaultZero(
						monthlyKindPlans.getPcbSummerSales()).add(
						MathUtils.toDefaultZero(monthlyKindPlans.getPcbPureSnowSales())), MathUtils.toDefaultZero(
						monthlyKindPlans.getPcbSummerNumber()).add(
						MathUtils.toDefaultZero(monthlyKindPlans.getPcbPureSnowNumber()))));
		r.put(MiddleClassification.RETREAD,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadNumber())));
		r.put(MiddleClassification.LT_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSummerSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSummerNumber())));
		r.put(MiddleClassification.TB_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSummerSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSummerNumber())));
		r.put(MiddleClassification.TIRE_RETREAD_SUMMER,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadSpecialTireSummerMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadSpecialTireSummerHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadSpecialTireSummerSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadSpecialTireSummerNumber())));
		r.put(MiddleClassification.LT_RETREAD_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSnowMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSnowHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSnowSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadLtSnowNumber())));
		r.put(MiddleClassification.TB_RETREAD_SNOW,
				new SimpleResult(MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSnowMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSnowHqMargin()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSnowSales()),
						MathUtils.toDefaultZero(monthlyKindPlans.getRetreadTbSnowNumber())));

	}

}
