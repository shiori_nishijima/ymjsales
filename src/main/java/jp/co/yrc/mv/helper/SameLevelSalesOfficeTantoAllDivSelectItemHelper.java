package jp.co.yrc.mv.helper;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.html.DivSelectItem;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;

/**
 * 同一レベルの他部署は営業所の担当者「全て」まで閲覧可能とする組織ドロップダウン項目を作成するヘルパー。 
 */
@Component
public class SameLevelSalesOfficeTantoAllDivSelectItemHelper extends DivSelectItemHelper {

	/**
	 * 販社ドロップダウンに全てを設定できる権限Set
	 */
	protected static final Set<Role> DIV1_ALL_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(
		Arrays.asList(Role.All)));

	/**
	 * デフォルト表示時の「全て」を設定します。
	 * その他の販社を閲覧可能とするので、複数販社の場合でも「全て」を出さない。
	 * 
	 * @param auth
	 * @param tanto
	 * @param div3
	 * @param div2
	 * @param div1
	 */
	protected void setDefaultOptionAll(Role auth, List<SelectItemGroup> tanto, List<SelectItemGroup> div3,
			List<SelectItemGroup> div2, List<SelectItem> div1) {
		setDefaultOptionAllToGroupList(tanto, DIV3_CONTROL, auth);
		setDefaultOptionAllToGroupList(div3, DIV3_CONTROL, auth);
		setDefaultOptionAllToGroupList(div2, DIV2_CONTROL, auth);
		setDefaultOptionAllToList(div1, DIV1_ALL_CONTROL, auth); // 複数販社で「全て」を出さない
	}
	
	/**
	 * 販社、部門、営業所、担当者リスト用に情報を設定します。
	 * 
	 * @param param　パラメータ
	 * @param user　ログインユーザ情報
	 * @param allList　全営業組織リスト
	 * @param myList　 ユーザに紐づく営業組織リスト
	 */
	protected void setSosikiList(DivSelectItem param, Role auth, List<EigyoSosikiDto> allList, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : allList) {
			String div1Cd = e.getDiv1Cd();
			String div2Cd = e.getDiv2Cd();

			if (Role.Department.equals(auth)) {
				if (containsMyDiv1(div1Cd, myList)) {
					setDiv1(param, e, auth, myList);
				}
			} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
				if (containsMyDiv1(div1Cd, myList) 
						&& containsMyDiv2(div2Cd, myList)) {
					setDiv1(param, e, auth, myList);
				}
			} else {
				setDiv1(param, e, auth, myList);
			}
		}
	}

	/**
	 * div1に紐づく担当者ドロップダウンに「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth
	 * @param myList
	 * @return 追加できる場合はtrue
	 */
	@Override
	protected boolean canAddDiv1TantoAll(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV2_CONTROL.contains(auth);
	}

	/**
	 * div2に紐づく担当者ドロップダウンに「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth
	 * @param myList
	 * @return 追加できる場合はtrue
	 */
	@Override
	protected boolean canAddDiv2TantoAll(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV3_CONTROL.contains(auth);
	}

	/**
	 * div1に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth
	 * @param myList
	 * @return 追加できる場合はtrue
	 */
	@Override
	protected boolean canAddDiv1TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		boolean canAdd = false;
		if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList);
		}
		return canAdd;
	}

	/**
	 * div2に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth
	 * @param myList
	 * @return 追加できる場合はtrue
	 */
	@Override
	protected boolean canAddDiv2TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		boolean canAdd = false;
		if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList);
		} else if (Role.Department.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList)
					&& containsMyDiv2(e.getDiv2Cd(), myList);
		}
		return canAdd;
	}

	/**
	 * div3に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth
	 * @param myList
	 * @return 追加できる場合はtrue
	 */
	@Override
	protected boolean canAddDiv3TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		boolean canAdd = true;
		if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList);
		} else if (Role.Department.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList)
					&& containsMyDiv2(e.getDiv2Cd(), myList);
		} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
			canAdd = containsMyDiv1(e.getDiv1Cd(), myList)
				&& containsMyDiv2(e.getDiv2Cd(), myList) 
				&& containsMyDiv3(e.getDiv3Cd(), myList);
		}
		return canAdd;
	}
}