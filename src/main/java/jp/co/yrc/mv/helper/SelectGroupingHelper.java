package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.Group;
import jp.co.yrc.mv.html.SelectItem;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * グルーピングリストを作成します。
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectGroupingHelper {

	private static final SelectItem ALL;
	static {
		SelectItem allItem = new SelectItem(Constants.SelectItrem.OPTION_ALL);
		allItem.setLabel("グルーピング");
		ALL = allItem;
	}

	public String allToNull(String group) {
		if (StringUtils.isBlank(group) || ALL.getValue().equals(group)) {
			return null;
		}
		return group;
	}

	/**
	 * {@link #createGroup(Model, boolean, boolean)}<br>
	 * <code>all</code>を<code>true</code>で作成。<br>
	 * <code>outside</code>を<code>true</code>で作成。
	 * 
	 * @param model
	 * @return
	 */
	public List<SelectItem> createGroup(Model model) {
		return createGroup(model, true, false);
	}

	/**
	 */
	public List<SelectItem> createGroup(Model model, boolean all, boolean outside) {
		List<SelectItem> items = new ArrayList<>();

		if (all) {
			items.add(ALL);
		}

		Group[] groups = Group.values();

		for (Group g : groups) {
			if (!outside && g == Group.GROUP_OUTSIDE) {
				continue;
			}
			items.add(new SelectItem(g.getName(), g.getCode()));
		}

		model.addAttribute("groups", items);

		return items;
	}
}
