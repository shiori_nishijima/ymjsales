package jp.co.yrc.mv.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.yrc.mv.common.Constants.Group;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.DemandDto;
import jp.co.yrc.mv.dto.GroupTotalDto;
import jp.co.yrc.mv.entity.Results;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * グルーピングの変換をするヘルパー
 */
@Component
public class GroupHelper {

	private static final Logger logger = LoggerFactory.getLogger(GroupHelper.class);

	private static final Map<Group, DemandSetupper> DEMAND_SETUPPER_MAP;
	static {
		HashMap<Group, DemandSetupper> map = new HashMap<>();
		map.put(Group.RANK_A, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setRankADemandEstimation(results.getDemandNumber());
			}

		});

		map.put(Group.RANK_B, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setRankBDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.RANK_C, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setRankCDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_NEW, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setNewDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_DEEP_PLOWING, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setDeepPlowingDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_Y_CP, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setYCpDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_OTHER_CP, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setOtherCpDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_IMPORTANT, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setImportantDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_ONE, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setGroupOneDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_TWO, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setGroupTwoDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_THREE, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setGroupThreeDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_FOUR, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setGroupFourDemandEstimation(results.getDemandNumber());
			}
		});

		map.put(Group.GROUP_FIVE, new DemandSetupper() {
			@Override
			public void setup(GroupTotalDto dto, DemandDto results) {
				dto.setGroupFiveDemandEstimation(results.getDemandNumber());
			}
		});

		DEMAND_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	private static final Map<Group, ResultsSetupper> RESULTS_SETUPPER_MAP;
	static {
		HashMap<Group, ResultsSetupper> map = new HashMap<>();
		map.put(Group.RANK_A, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setRankAMargin(results.getMargin());
				dto.setRankASales(results.getSales());
				dto.setRankANumber(results.getNumber());
			}
		});

		map.put(Group.RANK_B, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setRankBMargin(results.getMargin());
				dto.setRankBSales(results.getSales());
				dto.setRankBNumber(results.getNumber());
			}
		});

		map.put(Group.RANK_C, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setRankCMargin(results.getMargin());
				dto.setRankCSales(results.getSales());
				dto.setRankCNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_NEW, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setNewMargin(results.getMargin());
				dto.setNewSales(results.getSales());
				dto.setNewNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_DEEP_PLOWING, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setDeepPlowingMargin(results.getMargin());
				dto.setDeepPlowingSales(results.getSales());
				dto.setDeepPlowingNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_Y_CP, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setYCpMargin(results.getMargin());
				dto.setYCpSales(results.getSales());
				dto.setYCpNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_OTHER_CP, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setOtherCpMargin(results.getMargin());
				dto.setOtherCpSales(results.getSales());
				dto.setOtherCpNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_IMPORTANT, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setImportantMargin(results.getMargin());
				dto.setImportantSales(results.getSales());
				dto.setImportantNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_ONE, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setGroupOneMargin(results.getMargin());
				dto.setGroupOneSales(results.getSales());
				dto.setGroupOneNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_TWO, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setGroupTwoMargin(results.getMargin());
				dto.setGroupTwoSales(results.getSales());
				dto.setGroupTwoNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_THREE, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setGroupThreeMargin(results.getMargin());
				dto.setGroupThreeSales(results.getSales());
				dto.setGroupThreeNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_FOUR, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setGroupFourMargin(results.getMargin());
				dto.setGroupFourSales(results.getSales());
				dto.setGroupFourNumber(results.getNumber());
			}
		});

		map.put(Group.GROUP_FIVE, new ResultsSetupper() {
			@Override
			public void setup(GroupTotalDto dto, Results results) {
				dto.setGroupFiveMargin(results.getMargin());
				dto.setGroupFiveSales(results.getSales());
				dto.setGroupFiveNumber(results.getNumber());
			}
		});

		RESULTS_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	private static final Map<Group, CallSetupper> CALL_SETUPPER_MAP;
	static {
		HashMap<Group, CallSetupper> map = new HashMap<>();
		map.put(Group.RANK_A, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setRankACallCount(o.getCallCount());
				dto.setRankAPlanCount(o.getPlannedCount());
				dto.setRankAInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.RANK_B, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setRankBCallCount(o.getCallCount());
				dto.setRankBPlanCount(o.getPlannedCount());
				dto.setRankBInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.RANK_C, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setRankCCallCount(o.getCallCount());
				dto.setRankCPlanCount(o.getPlannedCount());
				dto.setRankCInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_NEW, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setNewCallCount(o.getCallCount());
				dto.setNewPlanCount(o.getPlannedCount());
				dto.setNewInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_DEEP_PLOWING, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setDeepPlowingCallCount(o.getCallCount());
				dto.setDeepPlowingPlanCount(o.getPlannedCount());
				dto.setDeepPlowingInfoCount(o.getInfoCount());
				;
			}
		});

		map.put(Group.GROUP_Y_CP, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setYCpCallCount(o.getCallCount());
				dto.setYCpPlanCount(o.getPlannedCount());
				dto.setYCpInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_OTHER_CP, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setOtherCpCallCount(o.getCallCount());
				dto.setOtherCpPlanCount(o.getPlannedCount());
				dto.setOtherCpInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_IMPORTANT, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setImportantCallCount(o.getCallCount());
				dto.setImportantPlanCount(o.getPlannedCount());
				dto.setImportantInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_ONE, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setGroupOneCallCount(o.getCallCount());
				dto.setGroupOnePlanCount(o.getPlannedCount());
				dto.setGroupOneInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_TWO, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setGroupTwoCallCount(o.getCallCount());
				dto.setGroupTwoPlanCount(o.getPlannedCount());
				dto.setGroupTwoInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_THREE, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setGroupThreeCallCount(o.getCallCount());
				dto.setGroupThreePlanCount(o.getPlannedCount());
				dto.setGroupThreeInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_FOUR, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setGroupFourCallCount(o.getCallCount());
				dto.setGroupFourPlanCount(o.getPlannedCount());
				dto.setGroupFourInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_FIVE, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setGroupFiveCallCount(o.getCallCount());
				dto.setGroupFivePlanCount(o.getPlannedCount());
				dto.setGroupFiveInfoCount(o.getInfoCount());
			}
		});

		map.put(Group.GROUP_OUTSIDE, new CallSetupper() {
			@Override
			public void setup(GroupTotalDto dto, CallCountInfoDto o) {
				dto.setOutsideCallCount(o.getCallCount());
				dto.setOutsidePlanCount(o.getPlannedCount());
				dto.setOutsideInfoCount(o.getInfoCount());
			}
		});

		CALL_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	private static interface DemandSetupper {
		void setup(GroupTotalDto dto, DemandDto results);
	}

	private static interface ResultsSetupper {
		void setup(GroupTotalDto dto, Results results);
	}

	private static interface CallSetupper {
		void setup(GroupTotalDto dto, CallCountInfoDto o);
	}

	/**
	 * グルーピング毎の訪問を設定する。
	 * 
	 * @param list
	 * @param result
	 */
	public void setCall(List<CallCountInfoDto> list, GroupTotalDto result) {
		for (CallCountInfoDto o : list) {
			CallSetupper setupper = CALL_SETUPPER_MAP.get(Group.getByCode(o.getGroupType()));
			if (setupper != null) {
				setupper.setup(result, o);
			} else {
				logger.warn(String.format("グルーピング訪問作成時に予期しないグループ[%s]があります。", o.getGroupType()));
			}
		}
	}

	/**
	 * グルーピング毎の需要を設定する。
	 * 
	 * @param list
	 * @param result
	 */
	public void setDemand(List<DemandDto> list, GroupTotalDto result) {
		for (DemandDto o : list) {
			DemandSetupper setupper = DEMAND_SETUPPER_MAP.get(Group.getByCode(o.getGroupType()));
			if (setupper != null) {
				setupper.setup(result, o);
			} else {
				logger.warn(String.format("グルーピング需要作成時に予期しないグループ[%s]があります。", o.getGroupType()));
			}
		}
	}

	/**
	 * グルーピング毎の実績を設定する。
	 * 
	 * @param list
	 * @param result
	 */
	public void setResult(List<Results> list, GroupTotalDto result) {
		for (Results o : list) {
			ResultsSetupper setupper = RESULTS_SETUPPER_MAP.get(Group.getByCode(o.getCompassKind()));
			if (setupper != null) {
				setupper.setup(result, o);
			} else {
				logger.warn(String.format("グルーピング実績作成時に予期しないグループ[%s]があります。", o.getCompassKind()));
			}
		}
	}

}
