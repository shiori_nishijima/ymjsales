package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.Constants.SelectItrem;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.service.MstEigyoSosikiService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SoshikiHelper {

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	/**
	 * パラメータをチェックします。全て（{@link SelectItrem#GROUP_VAL_ALL}）の場合はNULLを返します。
	 * 
	 * @param param
	 * @return
	 */
	public String checkSoshikiParamValue(String param) {
		return GROUP_VAL_ALL.equals(param) || StringUtils.isEmpty(param) ? null : param;
	}

	/**
	 * 検索用に販社コードのリストを以下の条件で作成します。
	 * <ul>
	 * <li>何も選択されていない場合は、ログイン者の権限に応じたcompanyリストを返却。※過去の検索は過去の所属も含める。</li>
	 * <li>div1が設定されている場合は、div1のみのリストを返却。</li>
	 * <li>div1が未設定で担当者コードが設定されている場合は空のリストを返却し、検索条件としない。</li>
	 * </ul>
	 * 
	 * @param div1
	 * @param tant
	 * @param userInfo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> createSearchCompanies(String div1, String tant, UserInfoDto userInfo, int year, int month) {
		div1 = checkSoshikiParamValue(div1);
		if (div1 == null) {
			if (tant != null) {
				return Collections.EMPTY_LIST;
			}

			List<MstEigyoSosiki> history = mstEigyoSosikiService.findByUserId(userInfo.getCorpCodes(),
					userInfo.getId(), year, month);
			Set<String> companies = new LinkedHashSet<>();
			for (MstEigyoSosiki o : history) {
				companies.add(o.getDiv1Cd());
			}
			companies.addAll(userInfo.getCompanies());
			return new ArrayList<>(companies);
		} else {
			return Collections.singletonList(div1);
		}
	}

	/**
	 * 検索用に販社コード条件用のリストを作成します。
	 * 複数販社権限が販社コードに「全て」を選択した場合、自身の所属する販社のみを作成します。
	 * 過去検索の場合は過去の所属と現在の所属をマージして作成します。
	 * 
	 * @param div1
	 * @param userInfo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<String> createSearchSalesCompanyCodeList(String div1, UserInfoDto userInfo, int year, int month) {
		String checkedDiv1 = checkSoshikiParamValue(div1);
		if (checkedDiv1 == null) {
			Role role = Role.getByCode(userInfo.getAuthorization()).getParent();
			// 全社と複数販社権限でありうる
			if (Role.All == role) {
				return Collections.EMPTY_LIST;
			} else {
				List<MstEigyoSosiki> history = mstEigyoSosikiService.findByUserId(userInfo.getCorpCodes(),
						userInfo.getId(), year, month);
				Set<String> companies = new LinkedHashSet<>();
				for (MstEigyoSosiki o : history) {
					companies.add(o.getDiv1Cd());
				}
				// 複数販社権限の現在の所属での過去の閲覧は可。過去の所属の現在の閲覧は不可。
				companies.addAll(userInfo.getCompanies());
				return new ArrayList<>(companies);
			}
		}
		// 画面上で販社を選択した場合
		return Collections.singletonList(checkedDiv1);
	}
}
