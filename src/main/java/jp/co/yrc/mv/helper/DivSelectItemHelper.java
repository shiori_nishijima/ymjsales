package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;
import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_NOT_BELONG;
import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.DivSelectItem;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;
import jp.co.yrc.mv.service.MstEigyoSosikiService;

/**
 * 検索条件用の組織ドロップダウン項目を作成するヘルパー。
 * 
 */
@Component
public class DivSelectItemHelper {

	/**
	 * 販社ドロップダウンを選択できる権限Set
	 */
	protected static final Set<Role> DIV1_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(
		Arrays.asList(Role.All, Role.Multiple)));

	/**
	 * 部門ドロップダウンを選択できる権限Set
	 */
	protected static final Set<Role> DIV2_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(
		Arrays.asList(Role.All, Role.Multiple, Role.Company)));

	/**
	 * 営業所ドロップダウンを選択できる権限Set
	 */
	protected static final Set<Role> DIV3_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(
		Arrays.asList(Role.All, Role.Multiple, Role.Company, Role.Department)));

	/**
	 * 担当者ドロップダウンを選択できる権限Set
	 */
	protected static final Set<Role> TANTO_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(
		Arrays.asList(Role.All, Role.Multiple, Role.Company, Role.Department, Role.SalesOffice, Role.Sales)));

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	/**
	 * 組織ドロップダウン項目を作成します。
	 * 
	 * @param model モデル
	 * @param principal プリンシパル
	 * @param year 年
	 * @param month 月
	 * @return ドロップダウン項目
	 */
	public DivSelectItem create(Model model, Principal principal, int year, int month) {
		UserInfoDto user = userDetailsHelper.getUserInfo(principal);
		Role auth = Role.getByCode(user.getAuthorization()).getParent();
		return createItem(year, month, user, auth, model);
	}

	/**
	 * 営業組織ドロップダウン用情報を生成します。 権限に応じて表示を制御します。
	 * 
	 * @param year 年
	 * @param month 月
	 * @param user ユーザ情報
	 * @param auth 権限
	 * @param model モデル
	 * @return ドロップダウン項目
	 */
	protected DivSelectItem createItem(int year, int month, UserInfoDto user, Role auth, Model model) {

		// 各ドロップダウン用リスト作成
		List<SelectItemGroup> tanto = new ArrayList<>();
		List<SelectItemGroup> div3 = new ArrayList<>();
		List<SelectItemGroup> div2 = new ArrayList<>();
		List<SelectItem> div1 = new ArrayList<>();
		
		setDefaultOptionAll(auth, tanto, div3, div2, div1);
				
		List<String> corpCodes = user.getCorpCodes();
		String userId = user.getId();
		int inputYearMonth = DateUtils.toYearMonth(year, month);
		Calendar now = DateUtils.getCalendar();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		boolean isHistory = inputYearMonth < nowYearMonth;

		// 営業組織とそれに紐づく所属・ユーザ情報を全て取得
		List<EigyoSosikiDto> allList = mstEigyoSosikiService.listSosikiTree(corpCodes, year, month);
		// 現在の自身の所属情報を取得
		List<EigyoSosikiDto> myList = mstEigyoSosikiService.listMySosiki(corpCodes, userId,
				now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);		
		// 過去の自身の所属情報を取得
		if (isHistory) {
			List<EigyoSosikiDto> historyMyList = mstEigyoSosikiService.listMySosiki(corpCodes, userId, year, month);
			myList.addAll(historyMyList);
		}
		DivSelectItem item = new DivSelectItem();
		item.div1 = div1;
		item.div2 = div2;
		item.div3 = div3;
		item.tanto = tanto;

		// ログインユーザ自身を担当者ドロップダウンに設定。
		setUserToTantoList(tanto, user, auth, myList);

		// ドロップダウンリストの生成。
		setSosikiList(item, auth, allList, myList);

		model.addAttribute("div", item);
		return item;
	}

	/**
	 * デフォルト表示時の「全て」を設定します。
	 * 
	 * @param auth
	 * @param tanto
	 * @param div3
	 * @param div2
	 * @param div1
	 */
	protected void setDefaultOptionAll(Role auth, List<SelectItemGroup> tanto, List<SelectItemGroup> div3,
			List<SelectItemGroup> div2, List<SelectItem> div1) {
		// TANTO_CONTROLを使うと先頭に不要なALLが入る
		setDefaultOptionAllToGroupList(tanto, DIV3_CONTROL, auth);
		setDefaultOptionAllToGroupList(div3, DIV3_CONTROL, auth);
		setDefaultOptionAllToGroupList(div2, DIV2_CONTROL, auth);
		setDefaultOptionAllToList(div1, DIV1_CONTROL, auth);
	}

	/**
	 * div1の「全て」を削除します。
	 * 
	 * @param model
	 */
	public void removeAllDiv1(DivSelectItem divSelectItem) {
		List<SelectItem> div1List = divSelectItem.div1;
		if (div1List.size() > 0) {
			SelectItem item = div1List.get(0);
			if (GROUP_VAL_ALL.equals(item.getValue())) {
				div1List.remove(0);
			}
		}
	}

	/**
	 * 初期表示時用の「全て」を設定します。
	 * 
	 * @param groupList ドロップダウン用リスト
	 * @param authorizations 「全て」を許可する権限Set
	 * @param auth ユーザ権限
	 */
	protected void setDefaultOptionAllToGroupList(List<SelectItemGroup> groupList, Set<Role> authorizations, Role auth) {
		if (authorizations.contains(auth)) {
			SelectItemGroup group = new SelectItemGroup(GROUP_VAL_ALL);
			setOptionAll(group);
			groupList.add(group);
		}
	}
	
	/**
	 * 初期表示時用の「全て」を設定します。
	 * 
	 * @param groupList ドロップダウン用リスト
	 * @param authorizations 「全て」を許可する権限Set
	 * @param auth ユーザ権限
	 */
	protected void setDefaultOptionAllToList(List<SelectItem> item, Set<Role> authorizations, Role auth) {
		if (authorizations.contains(auth)) {
			setOptionAll(item);
		}		
	}

	/**
	 * 担当者リストにログインユーザを追加します。 権限に応じで未選択または販社、部門、営業所選択時に表示できるようにします。
	 * 
	 * @param tanto 担当者ドロップダウン用リスト
	 * @param user ログインユーザ情報
	 * @param myList ユーザに紐づく営業組織リスト（担当者情報込み）
	 * @param isMonthlySum 月報用に作成するかどうか
	 */
	protected void setUserToTantoList(List<SelectItemGroup> tanto, UserInfoDto user, Role auth,	List<EigyoSosikiDto> myList) {
		if (Role.All.equals(auth)) {
			// 権限01は所属に紐づく権限ではないので一つだけ追加
			EigyoSosikiDto myInfo = new EigyoSosikiDto();
			myInfo.setUserId(user.getId());
			myInfo.setFirstName(user.getFirstName());
			myInfo.setLastName(user.getLastName());
			setTantoAll(tanto, GROUP_VAL_NOT_BELONG);
			setTantoName(tanto, myInfo, GROUP_VAL_NOT_BELONG);
		} else {
			// 権限に応じで自身を担当者に追加
			for (EigyoSosikiDto myInfo : myList) {
				if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
					setTantoAll(tanto, myInfo.getDiv1Cd());
					setTantoName(tanto, myInfo, myInfo.getDiv1Cd());
				} else if (Role.Department.equals(auth)) {
					setTantoAll(tanto, myInfo.getDiv2Cd());
					setTantoName(tanto, myInfo, myInfo.getDiv2Cd());
				} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
					setDiv3TantoAll(tanto, myInfo.getDiv3Cd());
					setTantoName(tanto, myInfo, myInfo.getDiv3Cd());
				}
			}
		}
	}

	/**
	 * 販社、部門、営業所、担当者ドロップダウン用に情報を設定します。
	 * 
	 * ログインユーザ自身を担当者リストに設定する処理はsetUserToTantoListで行う。
	 * 
	 * @param param パラメータ
	 * @param user ログインユーザ情報
	 * @param allList 全営業組織リスト
	 * @param myList ユーザに紐づく営業組織リスト
	 */
	protected void setSosikiList(DivSelectItem param, Role auth, List<EigyoSosikiDto> allList, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : allList) {
			String div1Cd = e.getDiv1Cd();
			String div2Cd = e.getDiv2Cd();
			String div3Cd = e.getDiv3Cd();

			if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
				if (containsMyDiv1(div1Cd, myList)) {
					setDiv1(param, e, auth, myList);
				}
			} else if (Role.Department.equals(auth)) {
				if (containsMyDiv1(div1Cd, myList) 
						&& containsMyDiv2(div2Cd, myList)) {
					setDiv1(param, e, auth, myList);
				}
			} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
				if (containsMyDiv1(div1Cd, myList) 
						&& containsMyDiv2(div2Cd, myList) 
						&& containsMyDiv3(div3Cd, myList)) {
					setDiv1(param, e, auth, myList);
				}
			} else {
				setDiv1(param, e, auth, myList);
			}
		}
	}

	/**
	 * 全てを選択項目グループに追加します。
	 * 
	 * @param group
	 */
	protected void setOptionAll(SelectItemGroup group) {
		group.getItems().add(OPTION_ALL);
	}

	/**
	 * 全てを選択項目に追加します。
	 * 
	 * @param itemList
	 */
	protected void setOptionAll(List<SelectItem> itemList) {
		itemList.add(OPTION_ALL);
	}
	
	/**
	 * チェック対象の販社コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div1 チェック対象コード
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean containsMyDiv1(String div1, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv1Cd()) && e.getDiv1Cd().equals(div1)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * チェック対象の部門コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div2  チェック対象コード
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean containsMyDiv2(String div2, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv2Cd()) && e.getDiv2Cd().equals(div2)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * チェック対象の営業所コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div3  チェック対象コード
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean containsMyDiv3(String div3, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv3Cd()) && e.getDiv3Cd().equals(div3)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 販社ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param パラメータ
	 * @param e 追加対象組織情報
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return div1コードが存在したら{@code true}
	 */
	protected boolean setDiv1(DivSelectItem param, EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		if (StringUtils.isNotEmpty(e.getDiv1Cd())) {
			List<SelectItem> div1 = param.div1;
			SelectItem item = new SelectItem(e.getDiv1Nm(), e.getDiv1Cd());
			if (!div1.contains(item)) {
				div1.add(item);
			}
			boolean result = setDiv2(param, e, auth, myList);
			// div1に紐づくユーザを閲覧できる権限の場合のみ設定。
			if (!result) {
				if (canAddDiv1TantoAll(e, auth, myList)) {
					setTantoAll(param.tanto, e.getDiv1Cd());
				}
				if (canAddDiv1TantoName(e, auth, myList)) {
					setTantoName(param.tanto, e, e.getDiv1Cd());
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * 部門ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param パラメータ
	 * @param e 追加対象組織情報
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return div2コードが存在したら{@code true}
	 */
	protected boolean setDiv2(DivSelectItem param, EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		if (StringUtils.isNotEmpty(e.getDiv2Cd())) {
			List<SelectItemGroup> div2 = param.div2;
			SelectItemGroup group = new SelectItemGroup(e.getDiv1Cd());
			if (!div2.contains(group)) {
				div2.add(group);
				if (canAddDiv2All(e, auth, myList)) {
					setOptionAll(group);
				}
			} else {
				group = div2.get(div2.indexOf(group));
			}
			SelectItem item = new SelectItem(e.getDiv2Nm(), e.getDiv2Cd());
			if (!group.getItems().contains(item)) {
				group.getItems().add(item);
			}
			boolean result = setDiv3(param, e, auth, myList);
			if (!result) {
				if (canAddDiv2TantoAll(e, auth, myList)) {
					setTantoAll(param.tanto, e.getDiv2Cd());
				}
				if (canAddDiv2TantoName(e, auth, myList)) {
					setTantoName(param.tanto, e, e.getDiv2Cd());
				}
			}			
			return true;
		}
		return false;
	}

	/**
	 * 営業所ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param パラメータ
	 * @param e 追加対象組織情報
	 * @param auth 権限
	 * @param myList ログインユーザに紐づく営業組織リスト
	 * @return div3コードが存在したら{@code true}
	 */
	protected boolean setDiv3(DivSelectItem param, EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		if (StringUtils.isNotEmpty(e.getDiv3Cd())) {
			List<SelectItemGroup> div3 = param.div3;
			SelectItemGroup group = new SelectItemGroup(e.getDiv2Cd());
			if (!div3.contains(group)) {
				div3.add(group);
				if (canAddDiv3All(e, auth, myList)) {
					setOptionAll(group);
				}
			} else {
				group = div3.get(div3.indexOf(group));
			}
			SelectItem item = new SelectItem(e.getDiv3Nm(), e.getDiv3Cd());
			if (!group.getItems().contains(item)) {
				group.getItems().add(item);
			}
			if (canAddDiv3TantoAll(e, auth, myList)) {
				setDiv3TantoAll(param.tanto, e.getDiv3Cd());
			}
			if (canAddDiv3TantoName(e, auth, myList)) {
				setTantoName(param.tanto, e, e.getDiv3Cd());
			}
			return true;
		}
		return false;
	}

	/**
	 * 担当者ドロップダウンリスト用項目を設定します。
	 * 
	 * @param tanto 担当者リスト
	 * @param e 追加対象ユーザ
	 * @param cd 追加対象となる親のコード
	 */
	protected void setTantoName(List<SelectItemGroup> tanto, EigyoSosikiDto e, String cd) {
		if (StringUtils.isNotEmpty(e.getUserId()) && StringUtils.isNotEmpty(cd)) {
			SelectItemGroup tantoGroup = new SelectItemGroup(cd);
			if (tanto.contains(tantoGroup)) {
				tantoGroup = tanto.get(tanto.indexOf(tantoGroup));
			} else {
				tanto.add(tantoGroup);
			}
			// 同レベルに同じ名称を複数設定しないようにする
			SelectItem tantoItem = new SelectItem(e.getLastName() + " " + e.getFirstName(), e.getUserId());
			if (!tantoGroup.getItems().contains(tantoItem)) {
				tantoGroup.getItems().add(tantoItem);
			}
		}
	}

	/**
	 * 担当者ドロップダウンリストに「全て」を設定します。
	 * 
	 * @param tanto 担当者リスト
	 * @param cd 追加対象となる親のコード
	 */
	protected void setTantoAll(List<SelectItemGroup> tanto, String cd) {
		// 先に呼ばれるのでcontainsでない場合のみ設定する
		SelectItemGroup tantoGroup = new SelectItemGroup(cd);
		if (!tanto.contains(tantoGroup)) {
			setOptionAll(tantoGroup);
			tanto.add(tantoGroup);
		}
	}

	/**
	 * div3に紐づく担当者ドロップダウンリストに「全て」を設定します。
	 * 
	 * @param tanto 担当者リスト
	 * @param cd 追加対象となる親のコード
	 */
	protected void setDiv3TantoAll(List<SelectItemGroup> tanto, String cd) {
		setTantoAll(tanto, cd);
	}

	/**
	 * div1に紐づく担当者ドロップダウンに「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv1TantoAll(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV1_CONTROL.contains(auth);
	}

	/**
	 * div2に紐づく担当者ドロップダウンに「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv2TantoAll(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV2_CONTROL.contains(auth);
	}

	/**
	 * div3に紐づく担当者ドロップダウンに「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv3TantoAll(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return true;
	}
	
	/**
	 * div1に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv1TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV1_CONTROL.contains(auth);
	}

	/**
	 * div2に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv2TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV2_CONTROL.contains(auth);
	}

	/**
	 * div3に紐づく担当者ドロップダウンに氏名を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv3TantoName(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return true;
	}
	
	/**
	 * div2に「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv2All(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV2_CONTROL.contains(auth);
	}

	/**
	 * div3に「全て」を追加できるかどうかを判定します。
	 * 
	 * @param auth 権限
	 * @param myList ユーザに紐づく営業組織リスト
	 * @return 追加できる場合はtrue
	 */
	protected boolean canAddDiv3All(EigyoSosikiDto e, Role auth, List<EigyoSosikiDto> myList) {
		return DIV3_CONTROL.contains(auth);
	}
}
