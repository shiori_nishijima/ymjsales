package jp.co.yrc.mv.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.yrc.mv.common.Constants.Market;
import jp.co.yrc.mv.dto.KindMarketTotalDto;
import jp.co.yrc.mv.entity.Results;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 販路別の情報を設定する。
 */
@Component
public class MarketHelper {

	private static final Logger logger = LoggerFactory.getLogger(MarketHelper.class);

	private static final Map<String, MarketSetupper> MARKET_ENTITY_SETUPPER_MAP;
	static {
		HashMap<String, MarketSetupper> map = new HashMap<String, MarketSetupper>();
		map.put(Market.SS, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setSsMargin(results.getMargin());
				dto.setSsSales(results.getSales());
				dto.setSsNumber(results.getNumber());
			}
		});
		map.put(Market.RS, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setRsMargin(results.getMargin());
				dto.setRsSales(results.getSales());
				dto.setRsNumber(results.getNumber());
			}
		});
		map.put(Market.CD, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setCdMargin(results.getMargin());
				dto.setCdSales(results.getSales());
				dto.setCdNumber(results.getNumber());
			}
		});
		map.put(Market.PS, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setPsMargin(results.getMargin());
				dto.setPsSales(results.getSales());
				dto.setPsNumber(results.getNumber());
			}
		});
		map.put(Market.CS, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setCsMargin(results.getMargin());
				dto.setCsSales(results.getSales());
				dto.setCsNumber(results.getNumber());
			}
		});
		map.put(Market.HC, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setHcMargin(results.getMargin());
				dto.setHcSales(results.getSales());
				dto.setHcNumber(results.getNumber());
			}
		});
		map.put(Market.LEASE, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setLeaseMargin(results.getMargin());
				dto.setLeaseSales(results.getSales());
				dto.setLeaseNumber(results.getNumber());
			}
		});

		map.put(Market.INDIRECT_OTHER, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setIndirectOtherMargin(results.getMargin());
				dto.setIndirectOtherSales(results.getSales());
				dto.setIndirectOtherNumber(results.getNumber());
			}
		});
		map.put(Market.TRUCK, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setTruckMargin(results.getMargin());
				dto.setTruckSales(results.getSales());
				dto.setTruckNumber(results.getNumber());
			}
		});
		map.put(Market.BUS, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setBusMargin(results.getMargin());
				dto.setBusSales(results.getSales());
				dto.setBusNumber(results.getNumber());
			}
		});

		map.put(Market.HIRE_TAXI, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setHireTaxiMargin(results.getMargin());
				dto.setHireTaxiSales(results.getSales());
				dto.setHireTaxiNumber(results.getNumber());
			}
		});

		map.put(Market.DIRECT_OTHER, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setDirectOtherMargin(results.getMargin());
				dto.setDirectOtherSales(results.getSales());
				dto.setDirectOtherNumber(results.getNumber());
			}
		});

		map.put(Market.RETAIL, new MarketSetupper() {
			@Override
			public void setup(KindMarketTotalDto dto, Results results) {
				dto.setRetailMargin(results.getMargin());
				dto.setRetailSales(results.getSales());
				dto.setRetailNumber(results.getNumber());
			}
		});
		MARKET_ENTITY_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	/**
	 * 販路別の実績を作成する。
	 * @param list
	 * @param result
	 */
	public void setMarket(List<Results> list, KindMarketTotalDto result) {
		for (Results o : list) {
			MarketSetupper setupper = MARKET_ENTITY_SETUPPER_MAP.get(o.getCompassKind());
			if (setupper != null) {
				setupper.setup(result, o);
			} else {
				logger.warn(String.format("品種別実績作成時に予期しない中分類[%s]があります。", o.getCompassKind()));
			}
		}
	}

	private static interface MarketSetupper {
		void setup(KindMarketTotalDto dto, Results results);
	}

}
