package jp.co.yrc.mv.helper;

import java.security.Principal;

import jp.co.yrc.mv.dto.UserInfoDto;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * SpringSecurityからユーザー情報を取得するためのヘルパー。
 */
@Component
public class UserDetailsHelper {

	public String getUserId(Principal principal) {
		Authentication authentication = (Authentication) principal;
		UserInfoDto userInfo = (UserInfoDto) authentication.getPrincipal();
		return userInfo.getId();
	}

	public UserInfoDto getUserInfo(Principal principal) {
		Authentication authentication = (Authentication) principal;
		return (UserInfoDto) authentication.getPrincipal();
	}

}
