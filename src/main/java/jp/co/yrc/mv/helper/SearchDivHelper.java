package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;
import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_NOT_BELONG;
import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL;
import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL_BLANK;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.EigyoSosikiDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;
import jp.co.yrc.mv.service.MstEigyoSosikiService;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * 検索用の組織リストを作成します。 div1、div2、div3、tantoで取得できます。
 */
@Component
public class SearchDivHelper {

	/**
	 * 販社ドロップダウンを選択できる権限Set
	 */
	private static final Set<Role> DIV1_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(Arrays.asList(Role.All,
			Role.Multiple)));

	/**
	 * 部門ドロップダウンを選択できる権限Set
	 */
	private static final Set<Role> DIV2_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(Arrays.asList(Role.All,
			Role.Multiple, Role.Company)));

	/**
	 * 営業所ドロップダウンを選択できる権限Set
	 */
	private static final Set<Role> DIV3_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(Arrays.asList(Role.All,
			Role.Multiple, Role.Company, Role.Department)));

	/**
	 * 担当者ドロップダウンを選択できる権限Set
	 */
	private static final Set<Role> TANTO_CONTROL = Collections.unmodifiableSet(new HashSet<Role>(Arrays.asList(
			Role.All, Role.Multiple, Role.Company, Role.Department, Role.SalesOffice, Role.Sales)));

	@Autowired
	MstEigyoSosikiService mstEigyoSosikiService;

	@Autowired
	UserDetailsHelper userDetailsHelper;

	@Autowired
	DozerBeanMapper dozerBeanMapper;

	/**
	 * @deprecated 訪問実績画面に表示する営業組織リストを作成します。<br>
	 *             {@link #createBasicSosiki(Model, Principal, boolean)}に<code>false</code>を渡したものと同じ。
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 */
	public DivSelectItem createVisitListSosiki(Model model, Principal principal, int year, int month) {
		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		Role rawAuth = Role.getByCode(userInfo.getAuthorization());
		Role auth = rawAuth.getParent();
		return create(year, month, userInfo, auth, rawAuth, model, false);
	}

	/**
	 * 画面に表示する営業組織リストを作成します。
	 * 
	 * @param model
	 *            モデル
	 * @param principal
	 *            プリンシパル
	 */
	public DivSelectItem createBasicSosiki(Model model, Principal principal, int year, int month) {
		return createBasicSosiki(model, principal, false, year, month);
	}

	public DivSelectItem createBasicSosiki(Model model, Principal principal, boolean hideAllLabel, int year, int month) {
		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		Role rawAuth = Role.getByCode(userInfo.getAuthorization());
		Role auth = rawAuth.getParent();
		return create(year, month, userInfo, auth, rawAuth, model, hideAllLabel);
	}

	public DivSelectItem createVisitListSosiki(Model model, Principal principal, boolean hideAllLabel, int year,
			int month) {
		UserInfoDto userInfo = userDetailsHelper.getUserInfo(principal);
		Role rawAuth = Role.getByCode(userInfo.getAuthorization());
		Role auth = Role.getVisitListRole(rawAuth);
		return create(year, month, userInfo, auth, rawAuth, model, hideAllLabel);
	}

	/**
	 * 営業組織ドロップダウン用情報を生成します。 権限に応じて表示を制御します。
	 * 
	 * @param allList
	 *            全営業組織リスト
	 * @param myList
	 *            ユーザに紐づく営業組織リスト（担当者情報込み）
	 * @param user
	 *            ログインユーザ情報
	 * @param model
	 *            モデル
	 * @param hideAllLabel
	 *            全てフラグを隠すかどうか
	 */
	protected DivSelectItem create(int year, int month, UserInfoDto user, Role auth, Role rawAuth, Model model,
			boolean hideAllLabel) {

		List<String> corpCodes = user.getCorpCodes();
		String userId = user.getId();

		int lastYearMonth = DateUtils.toYearMonth(year, month);

		Calendar now = Calendar.getInstance();
		now.setTime(DateUtils.getDBDate());
		int nowYearMonth = DateUtils.toYearMonth(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);

		boolean isHistory = lastYearMonth < nowYearMonth;

		List<EigyoSosikiDto> allList = mstEigyoSosikiService.listSosikiTree(corpCodes, year, month);

		if (allList == null) {
			allList = new ArrayList<>();
		}

		List<EigyoSosikiDto> myList = new ArrayList<>();
		if (isHistory) {

			List<EigyoSosikiDto> historyMyList = mstEigyoSosikiService.listMySosiki(corpCodes, userId, year, month);

			if (historyMyList != null) {
				myList.addAll(historyMyList);
			}
		}
		List<EigyoSosikiDto> currentMyList = mstEigyoSosikiService.listMySosiki(corpCodes, userId,
				now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
		myList.addAll(currentMyList);

		List<SelectItemGroup> tanto = new ArrayList<>();
		// tantoのselectに全体のall[全て]を選択できるControlセットはDIV3_CONTROLと同じになるのでDIV3_CONTROLを設定
		// TANTO_CONTROLを使うと、 all[全て]，111[1太郎，2太郎]，112[3太郎，4太郎]の様に先頭に不要なALLが入る
		setDefaultOptionAll(tanto, DIV3_CONTROL, auth, hideAllLabel);

		List<SelectItemGroup> div3 = new ArrayList<>();
		setDefaultOptionAll(div3, DIV3_CONTROL, auth, hideAllLabel);

		List<SelectItemGroup> div2 = new ArrayList<>();
		setDefaultOptionAll(div2, DIV2_CONTROL, auth, hideAllLabel);

		List<SelectItem> div1 = new ArrayList<>();
		if (DIV1_CONTROL.contains(auth)) {
			if (hideAllLabel) {
				div1.add(OPTION_ALL_BLANK);
			} else {
				div1.add(OPTION_ALL);
			}
		}

		DivSelectItem item = new DivSelectItem();
		item.div1 = div1;
		item.div2 = div2;
		item.div3 = div3;
		item.tanto = tanto;

		// ログインユーザ自身を担当者ドロップダウンに設定。
		setUserToTantoList(tanto, user, auth, rawAuth, myList, hideAllLabel);

		// ドロップダウンリストの生成。
		setSosikiList(item, auth, allList, myList, hideAllLabel);

		model.addAttribute("div", item);
		return item;
	}

	/**
	 * div1の「全て」を削除します。
	 * 
	 * @deprecated
	 * @param model
	 */
	public void removeAllDiv1(Model model) {
		List<SelectItem> div1List = ((DivSelectItem) model.asMap().get("div")).div1;
		if (div1List.size() > 0) {
			SelectItem item = div1List.get(0);
			if (GROUP_VAL_ALL.equals(item.getValue())) {
				div1List.remove(0);
			}
		}
	}

	/**
	 * div1の「全て」を削除します。
	 * 
	 * @param model
	 */
	public void removeAllDiv1(DivSelectItem divSelectItem) {
		List<SelectItem> div1List = divSelectItem.div1;
		if (div1List.size() > 0) {
			SelectItem item = div1List.get(0);
			if (GROUP_VAL_ALL.equals(item.getValue())) {
				div1List.remove(0);
			}
		}
	}

	/**
	 * 初期表示時用の「全て」を設定します。
	 * 
	 * 月報の場合は全てを選べないのでブランクを設定。
	 * 
	 * @param groupList
	 *            ドロップダウン用リスト
	 * @param authorizations
	 *            「全て」を許可する権限Set
	 * @param user
	 *            ログインユーザ情報
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 */
	protected void setDefaultOptionAll(List<SelectItemGroup> groupList, Set<Role> authorizations, Role auth,
			boolean isMonthlySum) {
		if (authorizations.contains(auth)) {
			SelectItemGroup group = new SelectItemGroup(GROUP_VAL_ALL);
			setOptionAll(group, isMonthlySum);
			groupList.add(group);
		}
	}

	/**
	 * 担当者リストにログインユーザを追加します。 権限に応じで未選択または販社、部門、営業所選択時に表示できるようにします。
	 * 
	 * @param tanto
	 *            担当者ドロップダウン用リスト
	 * @param user
	 *            ログインユーザ情報
	 * @param myList
	 *            ユーザに紐づく営業組織リスト（担当者情報込み）
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 */
	protected void setUserToTantoList(List<SelectItemGroup> tanto, UserInfoDto user, Role auth, Role rawAuth,
			List<EigyoSosikiDto> myList, boolean isMonthlySum) {

		// 自身を設定するときに、拡張したロールに行かないように設定する。
		auth = rawAuth.getParent() == auth ? auth : rawAuth.getParent();

		if (Role.All.equals(auth)) {
			// 権限01は所属に紐づく権限ではないので一つだけ追加
			SelectItemGroup tantoGroup = new SelectItemGroup(GROUP_VAL_NOT_BELONG);
			setOptionAll(tantoGroup, isMonthlySum);
			tantoGroup.getItems().add(new SelectItem(user.getLastName() + " " + user.getFirstName(), user.getId()));
			tanto.add(tantoGroup);
		} else {
			// 権限に応じで自身を担当者に追加する。
			for (EigyoSosikiDto mine : myList) {

				if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
					setTanto(tanto, mine, mine.getDiv1Cd(), auth, isMonthlySum);
				} else if (Role.Department.equals(auth)) {
					setTanto(tanto, mine, mine.getDiv2Cd(), auth, isMonthlySum);
				} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
					setTanto(tanto, mine, mine.getDiv3Cd(), auth, isMonthlySum);
				}
			}
		}
	}

	/**
	 * 販社、部門、営業所、担当者（ログインユーザ以外）リスト用に情報を設定します。
	 * 
	 * ログインユーザ自身を担当者リストに設定する処理はsetUserToTantoListで行う。
	 * 
	 * @param param
	 *            パラメータ
	 * @param user
	 *            ログインユーザ情報
	 * @param allList
	 *            全営業組織リスト
	 * @param myList
	 *            ユーザに紐づく営業組織リスト
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 */
	protected void setSosikiList(DivSelectItem param, Role auth, List<EigyoSosikiDto> allList,
			List<EigyoSosikiDto> myList, boolean isMonthlySum) {
		for (EigyoSosikiDto e : allList) {
			String div1Cd = e.getDiv1Cd();
			String div2Cd = e.getDiv2Cd();
			String div3Cd = e.getDiv3Cd();

			if (Role.Multiple.equals(auth) || Role.Company.equals(auth)) {
				if (checkMyDiv1(div1Cd, myList)) {
					setDiv1(param, e, auth, isMonthlySum);
				}
			} else if (Role.Department.equals(auth)) {
				// div1とdiv2が自分の所属するdiv1とdiv2と一致するものだけ
				if (checkMyDiv1(div1Cd, myList) && checkMyDiv2(div2Cd, myList)) {
					setDiv1(param, e, auth, isMonthlySum);
				}
			} else if (Role.SalesOffice.equals(auth) || Role.Sales.equals(auth)) {
				// div1とdiv2とdiv3が自分の所属するdiv1とdiv2とdiv3と一致するものだけ
				if (checkMyDiv1(div1Cd, myList) && checkMyDiv2(div2Cd, myList) && checkMyDiv3(div3Cd, myList)) {
					setDiv1(param, e, auth, isMonthlySum);
				}
			} else {
				setDiv1(param, e, auth, isMonthlySum);
			}
		}
	}

	/**
	 * 全てを選択項目に追加します。
	 * 
	 * 月報の場合は全てを選べないのでブランクを設定。
	 * 
	 * @param group
	 * @param isMonthlySum
	 */
	protected void setOptionAll(SelectItemGroup group, boolean isMonthlySum) {
		if (isMonthlySum) {
			group.getItems().add(OPTION_ALL_BLANK);
		} else {
			group.getItems().add(OPTION_ALL);
		}
	}

	/**
	 * チェック対象の販社コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div1
	 *            チェック対象コード
	 * @param myList
	 *            ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean checkMyDiv1(String div1, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv1Cd()) && e.getDiv1Cd().equals(div1)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * チェック対象の部門コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div2
	 *            チェック対象コード
	 * @param myList
	 *            ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean checkMyDiv2(String div2, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv2Cd()) && e.getDiv2Cd().equals(div2)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * チェック対象の営業所コードがユーザに紐づく営業組織リストに含まれているか確認します。
	 * 
	 * @param div3
	 *            チェック対象コード
	 * @param myList
	 *            ユーザに紐づく営業組織リスト
	 * @return チェック対象コードがユーザに紐づく営業組織リストに含まれていれば{@code true}
	 */
	protected boolean checkMyDiv3(String div3, List<EigyoSosikiDto> myList) {
		for (EigyoSosikiDto e : myList) {
			if (StringUtils.isNotEmpty(e.getDiv3Cd()) && e.getDiv3Cd().equals(div3)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 販社ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param e
	 *            追加対象組織情報
	 * @param user
	 *            ログインユーザ情報
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 * @return 販社ドロップダウンリスト項目を追加したら{@code true}
	 */
	protected boolean setDiv1(DivSelectItem param, EigyoSosikiDto e, Role auth, boolean isMonthlySum) {
		if (StringUtils.isNotEmpty(e.getDiv1Cd())) {
			List<SelectItem> div1 = param.div1;
			SelectItem item = new SelectItem(e.getDiv1Nm(), e.getDiv1Cd());
			if (!div1.contains(item)) {
				div1.add(item);
			}
			boolean result = setDiv2(param, e, auth, isMonthlySum);
			// div1に紐づくユーザを閲覧できる権限の場合のみ設定。
			if (DIV1_CONTROL.contains(auth) && !result) {
				setTanto(param.tanto, e, e.getDiv1Cd(), auth, isMonthlySum);
			}
			return true;
		}
		return false;
	}

	/**
	 * 部門ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param e
	 *            追加対象組織情報
	 * @param user
	 *            ログインユーザ情報
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 * @return 販社ドロップダウンリスト項目を追加したら{@code true}
	 */
	protected boolean setDiv2(DivSelectItem param, EigyoSosikiDto e, Role auth, boolean isMonthlySum) {
		if (StringUtils.isNotEmpty(e.getDiv2Cd())) {
			List<SelectItemGroup> div2 = param.div2;
			SelectItemGroup group = new SelectItemGroup(e.getDiv1Cd());
			if (!div2.contains(group)) {
				div2.add(group);
				if (DIV2_CONTROL.contains(auth)) {
					setOptionAll(group, isMonthlySum);
				}
			} else {
				group = div2.get(div2.indexOf(group));
			}
			SelectItem item = new SelectItem(e.getDiv2Nm(), e.getDiv2Cd());
			if (!group.getItems().contains(item)) {
				group.getItems().add(item);
			}
			boolean result = setDiv3(param, e, auth, isMonthlySum);
			if (DIV2_CONTROL.contains(auth) && !result) {
				setTanto(param.tanto, e, e.getDiv2Cd(), auth, isMonthlySum);
			}
			return true;
		}
		return false;
	}

	/**
	 * 営業所ドロップダウンリスト用項目を設定します。
	 * 
	 * @param param
	 *            パラメータ
	 * @param e
	 *            追加対象組織情報
	 * @param user
	 *            ログインユーザ情報
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 * @return 販社ドロップダウンリスト項目を追加したら{@code true}
	 */
	protected boolean setDiv3(DivSelectItem param, EigyoSosikiDto e, Role auth, boolean isMonthlySum) {
		if (StringUtils.isNotEmpty(e.getDiv3Cd())) {
			List<SelectItemGroup> div3 = param.div3;
			SelectItemGroup group = new SelectItemGroup(e.getDiv2Cd());
			if (!div3.contains(group)) {
				div3.add(group);
				if (DIV3_CONTROL.contains(auth)) {
					setOptionAll(group, isMonthlySum);
				}
			} else {
				group = div3.get(div3.indexOf(group));
			}
			SelectItem item = new SelectItem(e.getDiv3Nm(), e.getDiv3Cd());
			if (!group.getItems().contains(item)) {
				group.getItems().add(item);
			}
			setTanto(param.tanto, e, e.getDiv3Cd(), auth, isMonthlySum);
			return true;
		}
		return false;
	}

	/**
	 * 担当者ドロップダウンリスト用項目を設定します。
	 * 
	 * @param tanto
	 *            担当者リスト
	 * @param e
	 *            追加対象ユーザ
	 * @param cd
	 *            追加対象となる親のコード
	 * @param auth
	 *            権限
	 * @param isMonthlySum
	 *            月報用に作成するかどうか
	 */
	protected void setTanto(List<SelectItemGroup> tanto, EigyoSosikiDto e, String cd, Role auth, boolean isMonthlySum) {
		if (StringUtils.isNotEmpty(e.getUserId()) && StringUtils.isNotEmpty(cd)) {
			SelectItemGroup tantoGroup = new SelectItemGroup(cd);
			if (tanto.contains(tantoGroup)) {
				tantoGroup = tanto.get(tanto.indexOf(tantoGroup));
			} else {
				if (TANTO_CONTROL.contains(auth)) {
					setOptionAll(tantoGroup, isMonthlySum);
				}
				tanto.add(tantoGroup);
			}

			// 同レベルに同じ名称を複数設定しないようにする
			SelectItem tantoItem = new SelectItem(e.getLastName() + " " + e.getFirstName(), e.getUserId());
			if (!tantoGroup.getItems().contains(tantoItem)) {
				tantoGroup.getItems().add(tantoItem);
			}
		}
	}

	/**
	 * 営業組織リスト生成用パラメータ。
	 */
	public static class DivSelectItem {
		List<SelectItem> div1;
		List<SelectItemGroup> div2;
		List<SelectItemGroup> div3;
		List<SelectItemGroup> tanto;

		public List<SelectItem> getDiv1() {
			return div1;
		}

		public void setDiv1(List<SelectItem> div1) {
			this.div1 = div1;
		}

		public List<SelectItemGroup> getDiv2() {
			return div2;
		}

		public void setDiv2(List<SelectItemGroup> div2) {
			this.div2 = div2;
		}

		public List<SelectItemGroup> getDiv3() {
			return div3;
		}

		public void setDiv3(List<SelectItemGroup> div3) {
			this.div3 = div3;
		}

		public List<SelectItemGroup> getTanto() {
			return tanto;
		}

		public void setTanto(List<SelectItemGroup> tanto) {
			this.tanto = tanto;
		}

	}

}
