package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.dao.SelectionItemsDao;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.html.SelectItem;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * 訪問情報区分
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectCallsInfoDivHelper {

	@Autowired
	SelectionItemsDao selectionItemsDao;

	private static final SelectItem ALL;
	static {
		SelectItem allItem = new SelectItem(Constants.SelectItrem.OPTION_ALL);
		allItem.setLabel("情報区分");
		ALL = allItem;
	}

	public String allToNull(String market) {
		if (StringUtils.isBlank(market) || ALL.getValue().equals(market)) {
			return null;
		}
		return market;
	}

	/**
	 * {@link #createMarket(Model, boolean)}<br>
	 * <code>all</code>を<code>true</code>で作成。
	 * 
	 * @param model
	 * @return
	 */
	public List<SelectItem> createInfoDiv(Model model) {
		return createInfoDiv(model, true);
	}

	/**
	 */
	public List<SelectItem> createInfoDiv(Model model, boolean all) {
		List<SelectItem> items = new ArrayList<>();

		if (all) {
			items.add(ALL);
		}

		List<SelectionItems> list = selectionItemsDao.listByEntityNameAndPropertyName("Calls", "infoDiv", LocaleContextHolder.getLocale().getLanguage());

		for (SelectionItems si : list) {
			items.add(new SelectItem(si.getLabel(), si.getValue()));
		}

		model.addAttribute("callsInfoDivs", items);

		return items;
	}

}
