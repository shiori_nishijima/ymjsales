package jp.co.yrc.mv.helper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.yrc.mv.common.Constants.Market;
import jp.co.yrc.mv.dto.CallCountInfoDto;
import jp.co.yrc.mv.dto.CallTotalDto;
import jp.co.yrc.mv.dto.DealDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 訪問データを変換するヘルパー
 */
@Component
public class CallHelper {

	private static final Logger logger = LoggerFactory.getLogger(CallHelper.class);

	private static final Map<String, CallSetupper> CALL_SETUPPER_MAP;
	static {
		HashMap<String, CallSetupper> map = new HashMap<String, CallSetupper>();
		map.put(Market.SS, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setSsCallCount(callCountInfoDto.getCallCount());
				dto.setSsInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.RS, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setRsCallCount(callCountInfoDto.getCallCount());
				dto.setRsInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.CD, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setCdCallCount(callCountInfoDto.getCallCount());
				dto.setCdInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.PS, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setPsCallCount(callCountInfoDto.getCallCount());
				dto.setPsInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.CS, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setCsCallCount(callCountInfoDto.getCallCount());
				dto.setCsInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.HC, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setHcCallCount(callCountInfoDto.getCallCount());
				dto.setHcInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.LEASE, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setLeaseCallCount(callCountInfoDto.getCallCount());
				dto.setLeaseInfoCount(callCountInfoDto.getInfoCount());
			}
		});

		map.put(Market.INDIRECT_OTHER, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setIndirectOtherCallCount(callCountInfoDto.getCallCount());
				dto.setIndirectOtherInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.TRUCK, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setTruckCallCount(callCountInfoDto.getCallCount());
				dto.setTruckInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		map.put(Market.BUS, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setBusCallCount(callCountInfoDto.getCallCount());
				dto.setBusInfoCount(callCountInfoDto.getInfoCount());
			}
		});

		map.put(Market.HIRE_TAXI, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setHireTaxiCallCount(callCountInfoDto.getCallCount());
				dto.setHireTaxiInfoCount(callCountInfoDto.getInfoCount());
			}
		});

		map.put(Market.DIRECT_OTHER, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setDirectOtherCallCount(callCountInfoDto.getCallCount());
				dto.setDirectOtherInfoCount(callCountInfoDto.getInfoCount());
			}
		});

		map.put(Market.RETAIL, new CallSetupper() {
			@Override
			public void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto) {
				dto.setRetailCallCount(callCountInfoDto.getCallCount());
				dto.setRetailInfoCount(callCountInfoDto.getInfoCount());
			}
		});
		CALL_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	private static final Map<String, DealSetupper> DEAL_SETUPPER_MAP;
	static {
		HashMap<String, DealSetupper> map = new HashMap<String, DealSetupper>();
		map.put(Market.SS, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setSsAccountMasterCount(dealDto.getMasterCount());
				dto.setSsDealCount(dealDto.getDealCount());
				dto.setSsOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.RS, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setRsAccountMasterCount(dealDto.getMasterCount());
				dto.setRsDealCount(dealDto.getDealCount());
				dto.setRsOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.CD, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setCdAccountMasterCount(dealDto.getMasterCount());
				dto.setCdDealCount(dealDto.getDealCount());
				dto.setCdOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.PS, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setPsAccountMasterCount(dealDto.getMasterCount());
				dto.setPsDealCount(dealDto.getDealCount());
				dto.setPsOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.CS, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setCsAccountMasterCount(dealDto.getMasterCount());
				dto.setCsDealCount(dealDto.getDealCount());
				dto.setCsOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.HC, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setHcAccountMasterCount(dealDto.getMasterCount());
				dto.setHcDealCount(dealDto.getDealCount());
				dto.setHcOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.LEASE, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setLeaseAccountMasterCount(dealDto.getMasterCount());
				dto.setLeaseDealCount(dealDto.getDealCount());
				dto.setLeaseOwnCount(dealDto.getOwnCount());
			}
		});

		map.put(Market.INDIRECT_OTHER, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setIndirectOtherAccountMasterCount(dealDto.getMasterCount());
				dto.setIndirectOtherDealCount(dealDto.getDealCount());
				dto.setIndirectOtherOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.TRUCK, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setTruckAccountMasterCount(dealDto.getMasterCount());
				dto.setTruckDealCount(dealDto.getDealCount());
				dto.setTruckOwnCount(dealDto.getOwnCount());
			}
		});
		map.put(Market.BUS, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setBusAccountMasterCount(dealDto.getMasterCount());
				dto.setBusDealCount(dealDto.getDealCount());
				dto.setBusOwnCount(dealDto.getOwnCount());
			}
		});

		map.put(Market.HIRE_TAXI, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setHireTaxiAccountMasterCount(dealDto.getMasterCount());
				dto.setHireTaxiDealCount(dealDto.getDealCount());
				dto.setHireTaxiOwnCount(dealDto.getOwnCount());
			}
		});

		map.put(Market.DIRECT_OTHER, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setDirectOtherAccountMasterCount(dealDto.getMasterCount());
				dto.setDirectOtherDealCount(dealDto.getDealCount());
				dto.setDirectOtherOwnCount(dealDto.getOwnCount());
			}
		});

		map.put(Market.RETAIL, new DealSetupper() {
			@Override
			public void setup(CallTotalDto dto, DealDto dealDto) {
				dto.setRetailAccountMasterCount(dealDto.getMasterCount());
				dto.setRetailDealCount(dealDto.getDealCount());
				dto.setRetailOwnCount(dealDto.getOwnCount());
			}
		});
		DEAL_SETUPPER_MAP = Collections.unmodifiableMap(map);
	}

	/**
	 * 販路別の訪問を設定します。
	 * @param list
	 * @param callTotalDto
	 */
	public void setMarket(List<CallCountInfoDto> list, CallTotalDto callTotalDto) {
		for (CallCountInfoDto o : list) {
			CallSetupper setupper = CALL_SETUPPER_MAP.get(o.getGroupType());
			if (setupper != null) {
				setupper.setup(callTotalDto, o);
			} else {
				logger.warn(String.format("訪問績作成時に予期しない販路[%s]があります。", o.getGroupType()));
			}
		}
	}

	/**
	 * 販路別の持軒数を設定します
	 * @param list
	 * @param callTotalDto
	 */
	public void setDeal(List<DealDto> list, CallTotalDto callTotalDto) {
		for (DealDto o : list) {
			DealSetupper setupper = DEAL_SETUPPER_MAP.get(o.getGroupType());
			if (setupper != null) {
				setupper.setup(callTotalDto, o);
			} else {
				logger.warn(String.format("取引軒数作成時に予期しない販路[%s]があります。", o.getGroupType()));
			}
		}
	}

	private static interface CallSetupper {
		void setup(CallTotalDto dto, CallCountInfoDto callCountInfoDto);
	}

	private static interface DealSetupper {
		void setup(CallTotalDto dto, DealDto dealDto);
	}

}
