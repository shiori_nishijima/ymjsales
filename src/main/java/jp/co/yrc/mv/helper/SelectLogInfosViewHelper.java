package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.SelectItrem;
import jp.co.yrc.mv.entity.ViewInfos;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.service.ViewInfosService;

/**
 * 訪問情報区分
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectLogInfosViewHelper {

	@Autowired
	ViewInfosService viewInfosService;

	private static final SelectItem ALL;
	static {
		SelectItem allItem = new SelectItem(Constants.SelectItrem.OPTION_ALL);
		allItem.setLabel("画面名");
		ALL = allItem;
	}

	/**
	 * パラメータをチェックします。全て（{@link SelectItrem#GROUP_VAL_ALL}）の場合はNULLを返します。
	 * 
	 * @param param
	 * @return
	 */
	public String checkSoshikiParamValue(String param) {
		return GROUP_VAL_ALL.equals(param) || StringUtils.isEmpty(param) ? null : param;
	}

	public String allToNull(String view) {
		if (StringUtils.isBlank(view) || ALL.getValue().equals(view)) {
			return null;
		}
		return view;
	}

	public List<SelectItem> createView(Model mode) {

		return createView(mode, true);
	}

	/**
	*/
	public List<SelectItem> createView(Model model, boolean all) {
		List<SelectItem> result = new ArrayList<>();

		if (all) {
			result.add(ALL);
		}

		List<ViewInfos> list = viewInfosService.listLogInfosView(null);

		for (ViewInfos o : list) {
			result.add(new SelectItem(o.getName(), o.getUrl()));
		}

		model.addAttribute("views", result);

		return result;
	}

}
