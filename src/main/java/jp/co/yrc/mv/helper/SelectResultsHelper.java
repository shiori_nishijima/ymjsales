package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants.ResultsNet;
import jp.co.yrc.mv.common.Constants.ResultsType;
import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.service.SelectionItemsService;

/**
 * 表示用リストを作成します。
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectResultsHelper {

	@Autowired
	SelectionItemsService selectionItemsService;

	/**
	 * 表示用タイプ（本数・金額・粗利）リストを生成します。
     *
	 * @param model モデル
	 * @return 表示用リスト
	 */
	public List<SelectItem> createType(Model model) {
		List<SelectItem> typeItems = new ArrayList<>();
		ResultsType[] types = ResultsType.values();
		for (ResultsType type : types) {
			typeItems.add(new SelectItem(type.getLabel(), type.getValue()));
		}
		model.addAttribute("resultTypes", typeItems);
		return typeItems;
	}

	/**
	 * 表示用NETリストを生成します。
	 * 権限により取得できる値を変えます。
     *
	 * @param model モデル
	 * @param userInfo ユーザ情報
	 * @return 表示用リスト
	 */
	public List<SelectItem> createNet(Model model, UserInfoDto userInfo) {
		List<SelectItem> typeItems = new ArrayList<>();
		Role parentRole = Role.getByCode(userInfo.getAuthorization()).getParent();
		ResultsNet[] types = ResultsNet.values();
		for (ResultsNet type : types) {
			if (!type.containsIgnoreRoles(parentRole)) {
				typeItems.add(new SelectItem(type.getLabel(), type.getValue()));
			}
		}
		model.addAttribute("nets", typeItems);
		return typeItems;
	}
}
