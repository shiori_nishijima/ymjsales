package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.entity.SelectionItemsSecondLevel;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;
import jp.co.yrc.mv.service.SelectionItemsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * 検索用年月リストを作成します。
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectCompassKindHelper {

	@Autowired
	SelectionItemsService selectionItemsService;

	/**
	 */
	public void create(Model model) {
		List<SelectItem> largeClass2Items = new ArrayList<>();
		List<SelectItemGroup> middleClassItems = new ArrayList<>();

		List<SelectionItems> largeClass2s = selectionItemsService.listCompassKindLargeClass2();
		for (SelectionItems largeClass2 : largeClass2s) {
			largeClass2Items.add(new SelectItem(largeClass2.getLabel(), largeClass2.getValue()));
			List<SelectionItemsSecondLevel> seconds = selectionItemsService
					.listSecondLevelBySelectionItems(largeClass2);
			SelectItemGroup group = new SelectItemGroup(largeClass2.getValue());
			middleClassItems.add(group);
			for (SelectionItemsSecondLevel second : seconds) {
				group.getItems().add(new SelectItem(second.getLabel(), second.getValue()));
			}
		}

		model.addAttribute("largeClass2s", largeClass2Items).addAttribute("middleClasses", middleClassItems);
	}

	/**
	 */
	public void createMap(Model model, boolean excludeAll) {
		Map<String, String> largeClass2Items = new LinkedHashMap<>();
		Map<String, Map<String, String>> middleClassItems = new LinkedHashMap<>();

		List<SelectionItems> largeClass2s = selectionItemsService.listCompassKindLargeClass2();
		for (SelectionItems largeClass2 : largeClass2s) {
			if (excludeAll && Constants.SelectItrem.GROUP_VAL_ALL.equals(largeClass2.getValue())) {
				continue;
			}
			largeClass2Items.put(largeClass2.getValue(), largeClass2.getLabel());
			List<SelectionItemsSecondLevel> seconds = selectionItemsService
					.listSecondLevelBySelectionItems(largeClass2);

			Map<String, String> map = new LinkedHashMap<>();
			middleClassItems.put(largeClass2.getValue(), map);
			for (SelectionItemsSecondLevel second : seconds) {
				if (excludeAll && Constants.SelectItrem.GROUP_VAL_ALL.equals(second.getValue())) {
					continue;
				}
				map.put(second.getValue(), second.getLabel());
			}
		}

		model.addAttribute("largeClass2Map", largeClass2Items).addAttribute("middleClassMap", middleClassItems);
	}

}
