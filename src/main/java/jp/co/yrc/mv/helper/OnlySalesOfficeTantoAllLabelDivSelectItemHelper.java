package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL_BLANK;

import java.util.List;

import org.springframework.stereotype.Component;

import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;

/**
 * 営業所の担当者のみ「全て」を表示する組織ドロップダウン項目を作成するヘルパー。 
 */
@Component
public class OnlySalesOfficeTantoAllLabelDivSelectItemHelper extends DivSelectItemHelper {

	/**
	 * ブランクをを選択項目に追加します。
	 * 
	 * @param itemList 項目リスト
	 */
	@Override
	protected void setOptionAll(List<SelectItem> itemList) {
		itemList.add(OPTION_ALL_BLANK);
	}
	
	/**
	 * ブランクを選択項目グループに追加します。
	 * 
	 * @param group 項目グループ
	 */
	@Override
	protected void setOptionAll(SelectItemGroup group) {
		group.getItems().add(OPTION_ALL_BLANK);
	}

	/**
	 * div3設定後の担当者ドロップダウンリスト用項目を設定します。
	 * 
	 * @param tanto 担当者リスト
	 * @param cd 追加対象となる親のコード
	 */
	@Override
	protected void setDiv3TantoAll(List<SelectItemGroup> tanto, String cd) {
		SelectItemGroup tantoGroup = new SelectItemGroup(cd);
		if (!tanto.contains(tantoGroup)) {
			super.setOptionAll(tantoGroup);
			tanto.add(tantoGroup);
		}
	}
}
