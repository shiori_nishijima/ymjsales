package jp.co.yrc.mv.helper;

import java.security.Principal;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants.Role;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.DivSelectItem;

/**
 * 訪問実績検索条件用の組織ドロップダウン項目を作成するヘルパー。
 * 技サ訪問実績でも利用。
 */
@Component
public class VisitListDivSelectItemHelper extends DivSelectItemHelper {

	/**
	 * 組織ドロップダウン項目を作成します。
	 * 
	 * @param model モデル
	 * @param principal プリンシパル
	 * @param year 年
	 * @param month 月
	 * @return ドロップダウン項目
	 */
	@Override
	public DivSelectItem create(Model model, Principal principal, int year, int month) {
		// 権限の持つ訪問実績権限をもとに項目を作成
		UserInfoDto user = userDetailsHelper.getUserInfo(principal);
		Role rawAuth = Role.getByCode(user.getAuthorization());
		Role auth = Role.getVisitListRole(rawAuth);
		return createItem(year, month, user, auth, model);
	}
}
