package jp.co.yrc.mv.helper;

import java.util.ArrayList;
import java.util.List;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.entity.Markets;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.service.MarketsService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

/**
 * 検索用年月リストを作成します。
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectMarketHelper {

	@Autowired
	MarketsService marketsService;

	private static final SelectItem ALL;
	static {
		SelectItem allItem = new SelectItem(Constants.SelectItrem.OPTION_ALL);
		allItem.setLabel("販路");
		ALL = allItem;
	}

	public String allToNull(String market) {
		if (StringUtils.isBlank(market) || ALL.getValue().equals(market)) {
			return null;
		}
		return market;
	}

	/**
	 * {@link #createMarket(Model, boolean)}<br>
	 * <code>all</code>を<code>true</code>で作成。
	 * 
	 * @param model
	 * @return
	 */
	public List<SelectItem> createMarket(Model model) {
		return createMarket(model, true);
	}

	/**
	 */
	public List<SelectItem> createMarket(Model model, boolean all) {
		List<SelectItem> items = new ArrayList<>();

		if (all) {
			items.add(ALL);
		}

		List<Markets> list = marketsService.listAll();

		for (Markets market : list) {
			items.add(new SelectItem(market.getName(), market.getId()));
		}

		model.addAttribute("markets", items);

		return items;
	}

}
