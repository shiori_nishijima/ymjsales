package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.Constants.SelectItrem;
import jp.co.yrc.mv.common.Constants.LogInfos.UserAgent;
import jp.co.yrc.mv.html.SelectItem;

/**
 * 訪問情報区分
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SelectLogInfosUserAgentHelper {

	private static final SelectItem ALL;
	static {
		SelectItem allItem = new SelectItem(Constants.SelectItrem.OPTION_ALL);
		allItem.setLabel("端末");
		ALL = allItem;
	}

	/**
	 * パラメータをチェックします。全て（{@link SelectItrem#GROUP_VAL_ALL}）の場合はNULLを返します。
	 * 
	 * @param param
	 * @return
	 */
	public String checkSoshikiParamValue(String param) {
		return GROUP_VAL_ALL.equals(param) || StringUtils.isEmpty(param) ? null : param;
	}

	public String allToNull(String userAgent) {
		if (StringUtils.isBlank(userAgent) || ALL.getValue().equals(userAgent)) {
			return null;
		}
		return userAgent;
	}

	public List<SelectItem> createUserAgent(Model model) {
		return createUserAgent(model, true);
	}

	/**
	 */
	public List<SelectItem> createUserAgent(Model model, boolean all) {
		List<SelectItem> result = new ArrayList<>();

		if (all) {
			result.add(ALL);
		}

		UserAgent[] arr = UserAgent.values();
		for (UserAgent o : arr) {
			result.add(new SelectItem(o.getLabel(), o.getValue()));
		}

		model.addAttribute("userAgents", result);

		return result;
	}

}
