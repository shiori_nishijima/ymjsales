package jp.co.yrc.mv.validator;

import java.nio.charset.Charset;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jp.co.yrc.mv.validator.annotation.ByteLength;

/**
 * バイト長判定用バリデータ。
 *
 */
public class ByteLengthValidator implements ConstraintValidator<ByteLength, String> {
	private int min;
	private int max;
	private Charset charset;

	@Override
	public void initialize(ByteLength ano) {
		min = ano.min();
		max = ano.max();
		charset = Charset.forName(ano.charset());
		validateParameters();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		byte[] bytes = value.getBytes(charset);
		return bytes.length >= min && bytes.length <= max;
	}

	private void validateParameters() {
		if (min < 0) {
			throw new IllegalArgumentException("The min parameter cannot be negative.");
		}
		if (max < 0) {
			throw new IllegalArgumentException("The max parameter cannot be negative.");
		}
		if (max < min) {
			throw new IllegalArgumentException("The length cannot be negative.");
		}
	}

}
