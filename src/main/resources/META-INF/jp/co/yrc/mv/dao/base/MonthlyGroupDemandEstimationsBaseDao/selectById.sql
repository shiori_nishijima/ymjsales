select
  year,
  month,
  user_id,
  eigyo_sosiki_cd,
  rank_a_year_demand_estimation,
  rank_b_year_demand_estimation,
  rank_c_year_demand_estimation,
  important_year_demand_estimation,
  new_year_demand_estimation,
  deep_plowing_year_demand_estimation,
  y_cp_year_demand_estimation,
  other_cp_year_demand_estimation,
  group_one_year_demand_estimation,
  group_two_year_demand_estimation,
  group_three_year_demand_estimation,
  group_four_year_demand_estimation,
  group_five_year_demand_estimation,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  monthly_group_demand_estimations
where
  year = /* year */1
  and
  month = /* month */1
  and
  user_id = /* userId */'a'
  and
  eigyo_sosiki_cd = /* eigyoSosikiCd */'a'
