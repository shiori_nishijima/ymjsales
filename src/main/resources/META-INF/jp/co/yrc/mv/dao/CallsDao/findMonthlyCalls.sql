SELECT
  calls.parent_id
  , division_c
  , companion_names
  , calls.date
  , held_count 
FROM
  ( 
    SELECT
      calls.parent_id
      , GROUP_CONCAT(REPLACE (division_c, '^', '') separator ',') AS division_c
      , SUM(calls.held_count) AS held_count
      , DATE_FORMAT(date_start, '%d') AS date 
    FROM
      calls
      INNER JOIN users 
        ON users.id = calls.assigned_user_id 
      INNER JOIN 
      /*%if isHistory */
      ( 
        SELECT
          * 
        FROM
          accounts_history 
          INNER JOIN ( 
            SELECT
              id AS current_id
              , MAX(yearmonth) AS current_yearmonth 
            FROM
              accounts_history 
            WHERE
              yearmonth <= /* yearMonth */2000
              /*%if !div1.isEmpty() */
              AND sales_company_code IN /* div1 */('a', 'aa') /**一つの得意先は販社をまたいだ部署変更はあり得ない*/
              /*%end*/
            GROUP BY
              current_id
          ) current 
            ON current.current_id = accounts_history.id 
            AND current.current_yearmonth = accounts_history.yearmonth
      ) accounts 
      /*%else*/
      accounts 
      /*%end*/
        ON calls.parent_id = accounts.id 
        AND calls.parent_type = 'Accounts' 
        AND users.id_for_customer = accounts.assigned_user_id /**担当内訪問のみ検索のため、この条件を入れる */
    WHERE
      calls.date_start >= /* from */2000
      AND calls.date_start < /* to */2000
      AND calls.status = 'Held'
      /*%if !div1.isEmpty() */
      AND accounts.sales_company_code IN /* div1 */('a', 'aa') 
      /*%end*/
      /*%if div2 != null */
      AND accounts.department_code = /* div2 */'a'
      /*%end*/
      /*%if div3 != null */
      AND accounts.sales_office_code = /* div3 */'a'
      /*%end*/
      /*%if tanto != null */
      AND accounts.assigned_user_id = /* idForCustomer */'a'
      /*%end*/
      /*%if "GROUP_IMPORTANT".equals(group) */
      AND accounts.group_important = 1 
      /*%end*/
      /*%if "GROUP_NEW".equals(group) */
      AND accounts.group_new = 1 
      /*%end*/
      /*%if "GROUP_DEEP_PLOWING".equals(group) */
      AND accounts.group_deep_plowing = 1 
      /*%end*/
      /*%if "GROUP_Y_CP".equals(group) */
      AND accounts.group_y_cp = 1 
      /*%end*/
      /*%if "GROUP_OTHER_CP".equals(group) */
      AND accounts.group_other_cp = 1 
      /*%end*/
      /*%if "GROUP_ONE".equals(group) */
      AND accounts.group_one = 1 
      /*%end*/
      /*%if "GROUP_TWO".equals(group) */
      AND accounts.group_two = 1 
      /*%end*/
      /*%if "GROUP_THREE".equals(group) */
      AND accounts.group_three = 1 
      /*%end*/
      /*%if "GROUP_FOUR".equals(group) */
      AND accounts.group_four = 1 
      /*%end*/
      /*%if "GROUP_FIVE".equals(group) */
      AND accounts.group_five = 1 
      /*%end*/
      /*%if "A".equals(group) */
      AND accounts.sales_rank = 'A' 
      /*%end*/
      /*%if "B".equals(group) */
      AND accounts.sales_rank = 'B' 
      /*%end*/
      /*%if "C".equals(group) */
      AND accounts.sales_rank = 'C' 
      /*%end*/
    GROUP BY
      calls.parent_id
      , DATE_FORMAT(date_start, '%d')
  ) calls 
    LEFT OUTER JOIN ( 
      SELECT
        calls.parent_id
        , GROUP_CONCAT(CONCAT(companion_users.last_name, companion_users.first_name) separator ',') AS companion_names
        , DATE_FORMAT(date_start, '%d') AS date 
      FROM
        calls
        INNER JOIN users 
          ON users.id = calls.assigned_user_id 
        INNER JOIN 
        /*%if isHistory */
        ( 
          SELECT
            * 
          FROM
            accounts_history 
            INNER JOIN ( 
              SELECT
                id AS current_id
                , MAX(yearmonth) AS current_yearmonth 
              FROM
                accounts_history 
              WHERE
              yearmonth <= /* yearMonth */2000
              /*%if !div1.isEmpty() */
              AND sales_company_code IN /* div1 */('a', 'aa') /**一つの得意先は販社をまたいだ部署変更はあり得ない*/
              /*%end*/
              GROUP BY
                current_id
            ) current 
              ON current.current_id = accounts_history.id 
              AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts 
        /*%else*/
        accounts 
        /*%end*/
          ON calls.parent_id = accounts.id 
          AND calls.parent_type = 'Accounts' 
          AND users.id_for_customer = accounts.assigned_user_id /**担当内訪問のみ検索のため、この条件を入れる */
        INNER JOIN calls_users 
          ON calls.id = calls_users.call_id 
        INNER JOIN users AS companion_users 
          ON calls_users.user_id = companion_users.id 
      WHERE
      calls.date_start >= /* from */2000
      AND calls.date_start < /* to */2000
      AND calls.status = 'Held'
      /*%if !div1.isEmpty() */
      AND accounts.sales_company_code IN /* div1 */('a', 'aa') 
      /*%end*/
      /*%if div2 != null */
      AND accounts.department_code = /* div2 */'a'
      /*%end*/
      /*%if div3 != null */
      AND accounts.sales_office_code = /* div3 */'a'
      /*%end*/
      /*%if tanto != null */
      AND accounts.assigned_user_id = /* idForCustomer */'a'
      /*%end*/
      /*%if "GROUP_IMPORTANT".equals(group) */
      AND accounts.group_important = 1 
      /*%end*/
      /*%if "GROUP_NEW".equals(group) */
      AND accounts.group_new = 1 
      /*%end*/
      /*%if "GROUP_DEEP_PLOWING".equals(group) */
      AND accounts.group_deep_plowing = 1 
      /*%end*/
      /*%if "GROUP_Y_CP".equals(group) */
      AND accounts.group_y_cp = 1 
      /*%end*/
      /*%if "GROUP_OTHER_CP".equals(group) */
      AND accounts.group_other_cp = 1 
      /*%end*/
      /*%if "GROUP_ONE".equals(group) */
      AND accounts.group_one = 1 
      /*%end*/
      /*%if "GROUP_TWO".equals(group) */
      AND accounts.group_two = 1 
      /*%end*/
      /*%if "GROUP_THREE".equals(group) */
      AND accounts.group_three = 1 
      /*%end*/
      /*%if "GROUP_FOUR".equals(group) */
      AND accounts.group_four = 1 
      /*%end*/
      /*%if "GROUP_FIVE".equals(group) */
      AND accounts.group_five = 1 
      /*%end*/
      /*%if "A".equals(group) */
      AND accounts.sales_rank = 'A' 
      /*%end*/
      /*%if "B".equals(group) */
      AND accounts.sales_rank = 'B' 
      /*%end*/
      /*%if "C".equals(group) */
      AND accounts.sales_rank = 'C' 
      /*%end*/
    GROUP BY
      calls.parent_id
      , DATE_FORMAT(date_start, '%d')
    ) companion_calls 
      ON calls.parent_id = companion_calls.parent_id
      AND calls.date = companion_calls.date
