SELECT
    users.id
    ,calls_read_users.date_modified
  FROM
    /*%if tech */tech_service_calls_read_users/*%else*/calls_read_users/*%end*/ calls_read_users
    ,users
  WHERE
    calls_read_users.call_id = /*callId*/''
    AND users.id = calls_read_users.user_id
  ORDER BY
    calls_read_users.date_modified DESC
    ,users.id