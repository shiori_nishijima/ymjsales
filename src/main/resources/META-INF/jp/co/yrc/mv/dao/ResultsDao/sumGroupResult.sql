SELECT
        results.year
        ,results.month
        ,SUM(CASE WHEN accounts.sales_rank = 'A' THEN results.sales ELSE 0 END) AS rank_a_sales
        ,SUM(CASE WHEN accounts.sales_rank = 'A' THEN results.margin ELSE 0 END) AS rank_a_margin
        ,SUM(CASE WHEN accounts.sales_rank = 'A' THEN results.number ELSE 0 END) AS rank_a_number
        ,SUM(CASE WHEN accounts.sales_rank = 'B' THEN results.sales ELSE 0 END) AS rank_b_sales
        ,SUM(CASE WHEN accounts.sales_rank = 'B' THEN results.margin ELSE 0 END) AS rank_b_margin
        ,SUM(CASE WHEN accounts.sales_rank = 'B' THEN results.number ELSE 0 END) AS rank_b_number
        ,SUM(CASE WHEN accounts.sales_rank = 'C' THEN results.sales ELSE 0 END) AS rank_c_sales
        ,SUM(CASE WHEN accounts.sales_rank = 'C' THEN results.margin ELSE 0 END) AS rank_c_margin
        ,SUM(CASE WHEN accounts.sales_rank = 'C' THEN results.number ELSE 0 END) AS rank_c_number
        ,SUM(CASE WHEN accounts.group_important = '1' THEN results.sales ELSE 0 END) AS important_sales
        ,SUM(CASE WHEN accounts.group_important = '1' THEN results.margin ELSE 0 END) AS important_margin
        ,SUM(CASE WHEN accounts.group_important = '1' THEN results.number ELSE 0 END) AS important_number
        ,SUM(CASE WHEN accounts.group_new = '1' THEN results.sales ELSE 0 END) AS new_sales
        ,SUM(CASE WHEN accounts.group_new = '1' THEN results.margin ELSE 0 END) AS new_margin
        ,SUM(CASE WHEN accounts.group_new = '1' THEN results.number ELSE 0 END) AS new_number
        ,SUM(CASE WHEN accounts.group_deep_plowing = '1' THEN results.sales ELSE 0 END) AS deep_plowing_sales
        ,SUM(CASE WHEN accounts.group_deep_plowing = '1' THEN results.margin ELSE 0 END) AS deep_plowing_margin
        ,SUM(CASE WHEN accounts.group_deep_plowing = '1' THEN results.number ELSE 0 END) AS deep_plowing_number
        ,SUM(CASE WHEN accounts.group_y_cp = '1' THEN results.sales ELSE 0 END) AS y_cp_sales
        ,SUM(CASE WHEN accounts.group_y_cp = '1' THEN results.margin ELSE 0 END) AS y_cp_margin
        ,SUM(CASE WHEN accounts.group_y_cp = '1' THEN results.number ELSE 0 END) AS y_cp_number
        ,SUM(CASE WHEN accounts.group_other_cp = '1' THEN results.sales ELSE 0 END) AS other_cp_sales
        ,SUM(CASE WHEN accounts.group_other_cp = '1' THEN results.margin ELSE 0 END) AS other_cp_margin
        ,SUM(CASE WHEN accounts.group_other_cp = '1' THEN results.number ELSE 0 END) AS other_cp_number
        ,SUM(CASE WHEN accounts.group_one = '1' THEN results.sales ELSE 0 END) AS group_one_sales
        ,SUM(CASE WHEN accounts.group_one = '1' THEN results.margin ELSE 0 END) AS group_one_margin
        ,SUM(CASE WHEN accounts.group_one = '1' THEN results.number ELSE 0 END) AS group_one_number
        ,SUM(CASE WHEN accounts.group_two = '1' THEN results.sales ELSE 0 END) AS group_two_sales
        ,SUM(CASE WHEN accounts.group_two = '1' THEN results.margin ELSE 0 END) AS group_two_margin
        ,SUM(CASE WHEN accounts.group_two = '1' THEN results.number ELSE 0 END) AS group_two_number
        ,SUM(CASE WHEN accounts.group_three = '1' THEN results.sales ELSE 0 END) AS group_three_sales
        ,SUM(CASE WHEN accounts.group_three = '1' THEN results.margin ELSE 0 END) AS group_three_margin
        ,SUM(CASE WHEN accounts.group_three = '1' THEN results.number ELSE 0 END) AS group_three_number
        ,SUM(CASE WHEN accounts.group_four = '1' THEN results.sales ELSE 0 END) AS group_four_sales
        ,SUM(CASE WHEN accounts.group_four = '1' THEN results.margin ELSE 0 END) AS group_four_margin
        ,SUM(CASE WHEN accounts.group_four = '1' THEN results.number ELSE 0 END) AS group_four_number
        ,SUM(CASE WHEN accounts.group_five = '1' THEN results.sales ELSE 0 END) AS group_five_sales
        ,SUM(CASE WHEN accounts.group_five = '1' THEN results.margin ELSE 0 END) AS group_five_margin
        ,SUM(CASE WHEN accounts.group_five = '1' THEN results.number ELSE 0 END) AS group_five_number
    FROM
		(
			SELECT
			        results.year
			        ,results.month
			        ,results.date
			        ,results.account_id
			        ,SUM(results.sales) AS sales
			        ,SUM(results.margin) AS margin
					,SUM(
						CASE
							WHEN
								/** TIFU */
								compass_kinds.middle_class = '91'
								/** OTHER */
								OR compass_kinds.middle_class = '92'
								OR compass_kinds.middle_class = '93'
								OR compass_kinds.middle_class = '94'
								OR compass_kinds.middle_class = '95'
								/** WORK */
								OR compass_kinds.middle_class = '96'
								OR compass_kinds.middle_class = '97'
								/** RETREAD */
								OR compass_kinds.middle_class = 'A1'
								OR compass_kinds.middle_class = 'A2'
								OR compass_kinds.middle_class = 'A3'
								OR compass_kinds.middle_class = 'B1'
								OR compass_kinds.middle_class = 'B2'							
							THEN 0 
						ELSE results.number END
					) AS number
			    FROM
					(
						SELECT
						        results.year
						        ,results.month
						        ,results.date
						        ,results.account_id
						        ,results.compass_kind
						        ,SUM(results.sales) AS sales
						        ,SUM(results.margin) AS margin
						        ,SUM(results.number) AS number
						    FROM
						        results
						    	,
						/*%if isHistory */
						        (
						            SELECT
											*
						                FROM
						                    accounts_history
						                        INNER JOIN (
						                        	SELECT
						                        			id AS current_id
						                        			,MAX(yearmonth) AS current_yearmonth
						                        		FROM
						                        			accounts_history
						                        		WHERE
						                        			yearmonth <= /* yearMonth */2000
                                                            /*%if !div1.isEmpty() */
                                                            AND sales_company_code IN /* div1 */('a', 'aa')
                                                            /*%end*/
								                        GROUP BY current_id
						                        ) current
						                            ON current.current_id = accounts_history.id
						                            AND current.current_yearmonth = accounts_history.yearmonth
						        ) accounts
						/*%else*/
						    	accounts
						/*%end*/
						    	,users
						    WHERE
						        results.year = /* year */2000
						        AND results.month = /* month */1
							    /*%if date != null */
							    AND results.date = /* date */1
								/*%end*/
						        AND results.account_id = accounts.id
						        AND users.id_for_customer = accounts.assigned_user_id
						/*%if !div1.isEmpty() */
						        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
						    /*%if div1.size() == 1 */
						        /*%if div2 != null */
						        AND accounts.department_code = /* div2 */'a'
						        /*%end*/
						        /*%if div3 != null */
						        AND accounts.sales_office_code = /* div3 */'a'
						        /*%end*/
						        /*%if userId != null */
						        AND users.id = /* userId */'sfa'
						        /*%end*/
						    /*%end*/
						/*%end*/
						/*%if div1.isEmpty() */
						        /*%if userId != null */
						        AND users.id = /* userId */'sfa'
						        /*%end*/
						/*%end*/
						    GROUP BY
								results.year
								,results.month
						        ,results.date
								,results.account_id
								,results.compass_kind
					) results
					,compass_kinds
			    WHERE
					compass_kinds.id = results.compass_kind
					AND (
						compass_kinds.middle_class = '11'
						OR compass_kinds.middle_class = '12'
						OR compass_kinds.middle_class = '13'
						OR compass_kinds.middle_class = '21'
						OR compass_kinds.middle_class = '22'
						OR compass_kinds.middle_class = '51'
						OR compass_kinds.middle_class = '61'
						OR compass_kinds.middle_class = '62'
						OR compass_kinds.middle_class = '71'
						OR compass_kinds.middle_class = '72'
						/** OR */
						OR (
							compass_kinds.middle_class = '41'
							AND compass_kinds.small_class = '50'
						)
						/** ID */
						OR (
							compass_kinds.middle_class = '41'
							AND compass_kinds.small_class = '55'
						)
						/** TIFU */
						OR compass_kinds.middle_class = '91'
						/** OTHER */
						OR compass_kinds.middle_class = '92'
						OR compass_kinds.middle_class = '93'
						OR compass_kinds.middle_class = '94'
						OR compass_kinds.middle_class = '95'
						/** WORK */
						OR compass_kinds.middle_class = '96'
						OR compass_kinds.middle_class = '97'
						/** RETREAD */
						OR compass_kinds.middle_class = 'A1'
						OR compass_kinds.middle_class = 'A2'
						OR compass_kinds.middle_class = 'A3'
						OR compass_kinds.middle_class = 'B1'
						OR compass_kinds.middle_class = 'B2'
					)
			    GROUP BY
					results.year
					,results.month
			        ,results.date
					,results.account_id
		) results
    	,
/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
                                    /*%if !div1.isEmpty() */
                                    AND sales_company_code IN /* div1 */('a', 'aa')
                                    /*%end*/
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
/*%else*/
    	accounts
/*%end*/
    	,users
    WHERE
        results.account_id = accounts.id
        AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
    GROUP BY
        results.year
        ,results.month