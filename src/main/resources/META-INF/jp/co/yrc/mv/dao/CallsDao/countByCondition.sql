SELECT
		COUNT(calls_merged.id)
	FROM
(
SELECT
/*%if o.tech */
		CASE WHEN tech_calls_info.id IS NULL THEN calls_info.id ELSE tech_calls_info.id END AS id
/*%else*/
		*
/*%end*/
	FROM

(
SELECT
		calls_info_base.id
		,null AS call_id
	FROM
	   (
	       SELECT
	           calls.*
	       FROM
	           calls
	       WHERE
               calls.deleted <> 1
		   /*%if o.assignedUserId != null */
		       AND calls.assigned_user_id = /* o.assignedUserId */'sfa'
		   /*%end*/
		   /*%if o.from != null*/
		       AND calls.date_start >= /* o.from */'2000/01/01'
		   /*%end*/
		   /*%if o.to != null*/
		       AND calls.date_start < /* o.to */'2000/01/01'
		   /*%end*/
		   UNION
		   /** 敢えてUNIONにして重複を排除して担当者を指定した場合のみ社内同行分を別行で出せるようにする。 */
           SELECT
               calls.*
           FROM
               calls
           INNER JOIN
               calls_users
           ON
               calls.id = calls_users.call_id
           WHERE
               calls.deleted <> 1
           /*%if o.assignedUserId != null */
               AND calls_users.user_id = /* o.assignedUserId */'sfa'
           /*%end*/
           /*%if  o.from != null*/
               AND calls.date_start >= /* o.from */'2000/01/01'
           /*%end*/
           /*%if  o.to != null*/
               AND calls.date_start < /* o.to */'2000/01/01'
           /*%end*/
	   ) calls_info_base
			INNER JOIN 
           /*%if o.history */
                    (
                        SELECT
                                *
                            FROM
                                accounts_history
                                    INNER JOIN (
                                        SELECT
                                                id AS current_id
                                                ,MAX(yearmonth) AS current_yearmonth
                                            FROM
                                                accounts_history
                                            WHERE
                                                yearmonth <= /* o.yearMonth */2000
                                                /*%if !o.div1.isEmpty() */
                                                AND sales_company_code IN /* o.div1 */('a', 'aa')
                                                /*%end*/
                                            GROUP BY current_id
                                    ) current
                                        ON current.current_id = accounts_history.id
                                        AND current.current_yearmonth = accounts_history.yearmonth
                    ) accounts
            /*%else*/
                    accounts
            /*%end*/
                ON accounts.id = calls_info_base.parent_id
				AND calls_info_base.parent_type = 'Accounts'
			LEFT OUTER JOIN market_infos
				ON calls_info_base.id = market_infos.call_id
	WHERE
		calls_info_base.deleted <> 1
    /*%if o.id != null */
        AND calls_info_base.id = /* o.id */'a'
    /*%end*/

    /*%if o.heldOnly */
        AND calls_info_base.status = 'held'
    /*%end*/
    /*%if o.hasInfoOnly */
        AND (
            calls_info_base.com_free_c <> '' AND calls_info_base.com_free_c IS NOT NULL
            OR market_infos.description <> '' AND market_infos.description IS NOT NULL 
        )
    /*%end*/

    /*%if !o.div1.isEmpty() */
        AND accounts.sales_company_code IN /* o.div1 */('a', 'aa')
    /*%end*/
	/*%if o.div2 != null */
		AND accounts.department_code = /* o.div2 */'a'
	/*%end*/
	/*%if o.div3 != null */
		AND accounts.sales_office_code = /* o.div3 */'a'
	/*%end*/

	/*%if o.marketInfoInfoDiv != null */
		AND market_infos.info_div = /* o.marketInfoInfoDiv */'a'
	/*%end*/
) calls_info


/*%if o.tech */
LEFT OUTER JOIN 
				
				
(	
SELECT
		tech_calls_info_base.id
		,tech_calls_info_base.call_id

	FROM
	   (
	       SELECT
	           calls.*
	       FROM
	           tech_service_calls calls
	       WHERE
               calls.deleted <> 1
		   /*%if o.assignedUserId != null */
		       AND calls.assigned_user_id = /* o.assignedUserId */'sfa'
		   /*%end*/
		   /*%if o.from != null*/
		       AND calls.date_start >= /* o.from */'2000/01/01'
		   /*%end*/
		   /*%if o.to != null*/
		       AND calls.date_start < /* o.to */'2000/01/01'
		   /*%end*/
		   UNION
		   /** 敢えてUNIONにして重複を排除して担当者を指定した場合のみ社内同行分を別行で出せるようにする。 */
           SELECT
               calls.*
           FROM
               tech_service_calls calls
           INNER JOIN
               calls_users
           ON
               calls.id = calls_users.call_id
           WHERE
               calls.deleted <> 1
           /*%if o.assignedUserId != null */
               AND calls_users.user_id = /* o.assignedUserId */'sfa'
           /*%end*/
           /*%if  o.from != null*/
               AND calls.date_start >= /* o.from */'2000/01/01'
           /*%end*/
           /*%if  o.to != null*/
               AND calls.date_start < /* o.to */'2000/01/01'
           /*%end*/
	   ) tech_calls_info_base
			INNER JOIN 
           /*%if o.history */
                    (
                        SELECT
                                *
                            FROM
                                accounts_history
                                    INNER JOIN (
                                        SELECT
                                                id AS current_id
                                                ,MAX(yearmonth) AS current_yearmonth
                                            FROM
                                                accounts_history
                                            WHERE
                                                yearmonth <= /* o.yearMonth */2000
                                                /*%if !o.div1.isEmpty() */
                                                AND sales_company_code IN /* o.div1 */('a', 'aa')
                                                /*%end*/
                                            GROUP BY current_id
                                    ) current
                                        ON current.current_id = accounts_history.id
                                        AND current.current_yearmonth = accounts_history.yearmonth
                    ) accounts
            /*%else*/
                    accounts
            /*%end*/
                ON accounts.id = tech_calls_info_base.parent_id
				AND tech_calls_info_base.parent_type = 'Accounts'
			LEFT OUTER JOIN tech_service_market_infos market_infos
				ON tech_calls_info_base.id = market_infos.call_id
	WHERE
		tech_calls_info_base.deleted <> 1
    /*%if o.id != null */
        AND tech_calls_info_base.id = /* o.id */'a'
    /*%end*/

    /*%if o.heldOnly */
        AND tech_calls_info_base.status = 'held'
    /*%end*/
    /*%if o.hasInfoOnly */
        AND (
            tech_calls_info_base.com_free_c <> '' AND tech_calls_info_base.com_free_c IS NOT NULL
            OR market_infos.description <> '' AND market_infos.description IS NOT NULL 
        )
    /*%end*/

    /*%if !o.div1.isEmpty() */
        AND accounts.sales_company_code IN /* o.div1 */('a', 'aa')
    /*%end*/
	/*%if o.div2 != null */
		AND accounts.department_code = /* o.div2 */'a'
	/*%end*/
	/*%if o.div3 != null */
		AND accounts.sales_office_code = /* o.div3 */'a'
	/*%end*/

	/*%if o.marketInfoInfoDiv != null */
		AND market_infos.info_div = /* o.marketInfoInfoDiv */'a'
	/*%end*/
) tech_calls_info

	ON calls_info.id = tech_calls_info.call_id
/*%end*/
) calls_merged


		