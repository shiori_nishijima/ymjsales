SELECT
		COUNT(comments.id)
	FROM
		/*%if tech */tech_service_comments/*%else*/comments/*%end*/ comments
	WHERE
		parent_id = /* callId */'a'
		AND parent_type = /*%if tech */'TechServiceCalls'/*%else*/'Calls'/*%end*/
		AND is_confirmation = 0
