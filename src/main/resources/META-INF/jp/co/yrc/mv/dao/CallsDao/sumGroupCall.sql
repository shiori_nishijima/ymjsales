SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    accounts.sales_rank AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    accounts.sales_rank AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_IMPORTANT' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_important = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_IMPORTANT' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_important = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_NEW' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_new = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_NEW' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_new = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_DEEP_PLOWING' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_deep_plowing = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_DEEP_PLOWING' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_deep_plowing = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_Y_CP' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_y_cp = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_Y_CP' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_y_cp = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_OTHER_CP' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_other_cp = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_OTHER_CP' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_other_cp = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_ONE' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_one = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_ONE' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_one = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_TWO' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_two = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_TWO' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_two = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_THREE' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_three = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_THREE' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_three = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_FOUR' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_four = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_FOUR' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_four = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_FIVE' AS group_type
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_five = 1
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_FIVE' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND accounts.group_five = 1
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
UNION ALL
SELECT
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null as date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    'GROUP_OUTSIDE' AS group_type
                    ,0 AS info_count
                    ,calls.planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN users 
                            ON users.id_for_customer = accounts.assigned_user_id 
                            AND users.id <> calls.assigned_user_id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    'GROUP_OUTSIDE' AS group_type
                    ,0 AS info_count
                    ,0 AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,calls.date_start
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
            WHERE
                calls.date_start >= /*from*/2000
                AND calls.date_start < /*to*/2000
        /*%if !div1.isEmpty() */
                AND accounts.sales_company_code IN /* div1 */('a', 'aa')
            /*%if div1.size() == 1 */
                /*%if div2 != null */
                AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null */
                AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
            /*%end*/
        /*%end*/
        /*%if div1.isEmpty() */
                /*%if userId != null */
                AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
        /*%end*/
                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/