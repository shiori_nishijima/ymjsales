SELECT
        accounts.id
        ,accounts.name
        ,accounts.name_kana
        ,accounts.assigned_user_id
        ,accounts.sales_company_code
        ,accounts.department_code
        ,accounts.sales_office_code
        ,accounts.market_id
        ,markets.name AS market_name
        ,accounts.own_count
        ,accounts.deal_count
        ,accounts.group_important
        ,accounts.group_new
        ,accounts.group_deep_plowing
        ,accounts.group_y_cp
        ,accounts.group_other_cp
        ,accounts.group_one
        ,accounts.group_two
        ,accounts.group_three
        ,accounts.group_four
        ,accounts.group_five
        ,accounts.sales_rank
        ,accounts.demand_number
        ,accounts.iss_yh
        ,accounts.iss_bs
        ,accounts.iss_dl
        ,accounts.iss_ty
        ,accounts.iss_gy
        ,accounts.iss_mi
        ,accounts.iss_pb
        ,accounts.iss_other
    FROM
/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
                                    AND sales_company_code = /* div1 */'a'
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
/*%else*/
        accounts
/*%end*/
        	INNER JOIN users
        		ON users.id_for_customer = accounts.assigned_user_id
        	INNER JOIN markets
        		ON accounts.market_id = markets.id
        	LEFT OUTER JOIN (
        		SELECT
        				user_id
        			FROM
        				monthly_comments
        			WHERE
        				year = /*year*/2000
        				AND month = /*month*/1
        	) monthly_comments
        		ON users.id = monthly_comments.user_id
	WHERE
        accounts.sales_company_code = /* div1 */'a'
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
        AND accounts.deleted <> 1
	ORDER BY
		accounts.id ASC