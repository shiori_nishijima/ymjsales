SELECT
		mst_eigyo_sosiki.eigyo_sosiki_cd
		,mst_eigyo_sosiki.eigyo_sosiki_nm
		,mst_eigyo_sosiki.eigyo_sosiki_short_nm
		,mst_eigyo_sosiki.corp_cd
		,mst_eigyo_sosiki.div1_cd
		,mst_eigyo_sosiki.div2_cd
		,mst_eigyo_sosiki.div3_cd
		,mst_eigyo_sosiki.div4_cd
		,mst_eigyo_sosiki.div5_cd
		,mst_eigyo_sosiki.div6_cd
		,mst_eigyo_sosiki.div1_nm
		,mst_eigyo_sosiki.div2_nm
		,mst_eigyo_sosiki.div3_nm
		,mst_eigyo_sosiki.div4_nm
		,mst_eigyo_sosiki.div5_nm
		,mst_eigyo_sosiki.div6_nm
FROM 
	/*%if isHistory */
	(
	  SELECT
	      *
	    FROM
	      mst_eigyo_sosiki_history
	    WHERE
	       year = /*year*/2015
	       AND month = /*month*/01
	) mst_eigyo_sosiki
	/*%else*/
	mst_eigyo_sosiki
	/*%end*/ 
WHERE 

	/*%if !div1.isEmpty()*/
		div1_cd IN /* div1 */('a')
		/*%if div2 != null */
			AND div2_cd = /* div2 */'a'
		/*%else*/
			AND (div2_cd IS NULL OR div2_cd = '')
			AND (div3_cd IS NULL OR div3_cd = '')
		/*%end*/
	/*%else*/
		(div1_cd IS NOT NULL OR div1_cd <> '')
		AND (div2_cd IS NULL OR div2_cd = '')
		AND (div3_cd IS NULL OR div3_cd = '')
	/*%end*/


	AND deleted = 0
	AND corp_cd in /* corpCodes */('a', 'b', 'c')
ORDER BY 
	div1_cd
	,div2_cd
	,div3_cd
