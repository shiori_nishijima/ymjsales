select
  eigyo_sosiki_cd,
  year,
  month,
  eigyo_sosiki_nm,
  eigyo_sosiki_short_nm,
  corp_cd,
  div1_cd,
  div2_cd,
  div3_cd,
  div4_cd,
  div5_cd,
  div6_cd,
  div1_nm,
  div2_nm,
  div3_nm,
  div4_nm,
  div5_nm,
  div6_nm,
  is_sfa,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  mst_eigyo_sosiki_history
where
  eigyo_sosiki_cd = /* eigyoSosikiCd */'a'
  and
  year = /* year */1
  and
  month = /* month */1
