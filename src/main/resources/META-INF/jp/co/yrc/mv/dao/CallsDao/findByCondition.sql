SELECT
  calls_info.id
  , calls_info.name
  , calls_info.assigned_user_id
  , calls_info.date_start
  , calls_info.date_end
  , calls_info.parent_type
  , calls_info.status
  , calls_info.parent_id
  , calls_info.info_div
  , calls_info.maker
  , calls_info.kind
  , calls_info.category
  , calls_info.sensitivity
  , calls_info.time_end_c
  , calls_info.com_free_c
  , calls_info.division_c
  , calls_info.is_plan
  , calls_info.planned_count
  , calls_info.held_count
  , calls_info.deleted
  , calls_info.date_entered
  , calls_info.date_modified
  , calls_info.modified_user_id
  , calls_info.created_by
  , calls_info.accounts_name
  , calls_info.calls_like_count
  , calls_info.calls_read_count
  , calls_info.file1
  , calls_info.file2
  , calls_info.file3
  , calls_info.market_info 
  /*%if o.userId != null */
  , calls_info.calls_read
  , calls_info.comment_read_count
  , calls_info.comment_entered 
  /*%end*/
  , calls_info.comment_count
  , calls_info.exsists_tech
  , parent_type.label AS parent_type
  , status.label AS status
  , info_div.label AS info_div
  , maker.label AS maker
  , kind.label AS kind
  , category.label AS category
  , sensitivity.label AS sensitivity
  , calls_info.first_name AS assigned_user_first_name
  , calls_info.last_name AS assigned_user_last_name
  /*%if o.tech */
  , calls_info.created_user_first_name
  , calls_info.created_user_last_name
  /*%end*/
  , calls_info.call_id
  , accounts_sales_company_code
  , accounts_department_code
  , accounts_sales_office_code
  , market_id
  , markets.name AS market_name 
FROM
  ( 
    SELECT
      calls_info_base.id
      , /*%if o.tech */calls_info_base.call_id/*%else*/null AS call_id/*%end*/
      , calls_info_base.name
      , calls_info_base.assigned_user_id
      , calls_info_base.date_start
      , calls_info_base.date_end
      , calls_info_base.parent_type
      , calls_info_base.status
      , calls_info_base.parent_id
      , calls_info_base.info_div
      , calls_info_base.maker
      , calls_info_base.kind
      , calls_info_base.category
      , calls_info_base.sensitivity
      , calls_info_base.time_end_c 
      /** 情報区分が04（性能・技術・ラインナップ・損傷現状）の場合は市場情報の詳細、それ以外は内容を表示する */
      , ( 
        CASE 
          WHEN calls_info_base.info_div = '04' 
          THEN market_infos.description 
          ELSE calls_info_base.com_free_c 
          END
      ) AS com_free_c
      , calls_info_base.division_c
      , calls_info_base.is_plan
      , calls_info_base.planned_count
      , calls_info_base.held_count
      , calls_info_base.deleted
      , calls_info_base.date_entered
      , calls_info_base.date_modified
      , calls_info_base.modified_user_id
      , calls_info_base.created_by
      , calls_like_count.calls_like_count
      , calls_read_count.calls_read_count
      , ( 
        CASE 
          WHEN calls_files.file1 IS NOT NULL 
          AND length(calls_files.file1) > 0 
          THEN 1 
          ELSE 0 
          END
      ) AS file1
      , ( 
        CASE 
          WHEN calls_files.file2 IS NOT NULL 
          AND length(calls_files.file2) > 0 
          THEN 1 
          ELSE 0 
          END
      ) AS file2
      , ( 
        CASE 
          WHEN calls_files.file3 IS NOT NULL 
          AND length(calls_files.file3) > 0 
          THEN 1 
          ELSE 0 
          END
      ) AS file3
      , ( 
        CASE 
          WHEN market_infos.id IS NOT NULL 
          THEN 1 
          ELSE 0 
          END
      ) AS market_info 
      /*%if o.userId != null */
      , calls_read.calls_read
      , comment_read_count.comment_read_count
      , comment_entered.comment_entered 
      /*%end*/
      , comment_count.comment_count
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.sales_company_code ELSE opportunity_accounts.sales_company_code END AS accounts_sales_company_code
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.department_code ELSE opportunity_accounts.department_code END AS accounts_department_code
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.sales_office_code ELSE opportunity_accounts.sales_office_code END AS accounts_sales_office_code
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.market_id ELSE opportunity_accounts.market_id END AS market_id
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.id ELSE opportunity_accounts.id END AS accounts_id
      , CASE WHEN opportunity_accounts.id IS NULL THEN accounts.name ELSE opportunity_accounts.name END AS accounts_name
  	  , users.first_name
  	  , users.last_name
      /*%if o.tech */
	    , created_user.first_name AS created_user_first_name
	    , created_user.last_name AS created_user_last_name
	    /*%end*/
      , /*%if o.checkExistsTech */
      ( 
        CASE 
          WHEN tech_service_calls.id IS NULL 
          THEN 0 
          ELSE 1 
          END
      )/*%else*/0/*%end*/ AS exsists_tech 
    FROM
/*%if o.id != null || o.tantoOnly */
      (
        SELECT
          calls.* 
        FROM
          /*%if o.tech */tech_service_calls/*%else*/calls/*%end*/ calls 
        WHERE
          calls.deleted <> 1 
        /*%if o.assignedUserId != null */
		  AND calls.assigned_user_id = /* o.assignedUserId */'sfa'
		/*%end*/
		/*%if o.from != null*/
		  AND calls.date_start >= /* o.from */'2000/01/01'
		/*%end*/
		/*%if o.to != null*/
		  AND calls.date_start < /* o.to */'2000/01/01'
		/*%end*/
		/*%if o.accountId != null*/
		  AND calls.parent_id = /* o.accountId */'2000000001'
		/*%end*/
		/*%if o.id != null*/
		  AND calls.id = /* o.id */'1111'
		/*%end*/
      ) calls_info_base
/*%else*/
      (
        SELECT
          calls.* 
        FROM
          /*%if o.tech */tech_service_calls/*%else*/calls/*%end*/ calls 
        WHERE
          calls.deleted <> 1 
	    /*%if o.assignedUserId != null */
		  AND calls.assigned_user_id = /* o.assignedUserId */'sfa'
		/*%end*/
		/*%if o.from != null*/
		  AND calls.date_start >= /* o.from */'2000/01/01'
		/*%end*/
		/*%if o.to != null*/
		  AND calls.date_start < /* o.to */'2000/01/01'
		/*%end*/
		/*%if o.accountId != null*/
		  AND calls.parent_id = /* o.accountId */'2000000001'
		/*%end*/
        UNION
        /** 敢えてUNIONにして重複を排除して担当者を指定した場合のみ社内同行分を別行で出せるようにする。 */
        SELECT
          calls.* 
        FROM
          /*%if o.tech */tech_service_calls/*%else*/calls/*%end*/ calls
        INNER JOIN
          /*%if o.tech */tech_service_calls_users/*%else*/calls_users/*%end*/ calls_users
          ON calls.id = calls_users.call_id
        WHERE
          calls.deleted <> 1
        /*%if o.assignedUserId != null */
          AND calls_users.user_id = /* o.assignedUserId */'sfa'
        /*%end*/
        /*%if  o.from != null*/
          AND calls.date_start >= /* o.from */'2000/01/01'
        /*%end*/
        /*%if  o.to != null*/
          AND calls.date_start < /* o.to */'2000/01/01'
        /*%end*/
		/*%if o.accountId != null*/
		  AND calls.parent_id = /* o.accountId */'2000000001'
		/*%end*/
      ) calls_info_base
/*%end*/
      INNER JOIN users
      	ON calls_info_base.assigned_user_id = users.id
      LEFT OUTER JOIN 
      /*%if o.history */
      ( 
        SELECT
          * 
        FROM
          accounts_history 
          INNER JOIN ( 
            SELECT
              id AS current_id
              , MAX(yearmonth) AS current_yearmonth 
            FROM
              accounts_history 
            WHERE
              yearmonth <= /* o.yearMonth */2000
              /*%if !o.div1.isEmpty() */
              AND sales_company_code IN /* o.div1 */('a', 'aa')
              /*%end*/
		      /*%if o.accountId != null*/
		      AND id = /* o.accountId */'2000000001'
		      /*%end*/
            GROUP BY 
              current_id
          ) current 
            ON current.current_id = accounts_history.id 
            AND current.current_yearmonth = accounts_history.yearmonth
      ) accounts 
      /*%else*/
      accounts 
      /*%end*/
        ON accounts.id = calls_info_base.parent_id
        AND calls_info_base.parent_type = 'Accounts'
        /*%if o.tantoOnly */
        AND users.id_for_customer = accounts.assigned_user_id
        /*%end*/
      LEFT OUTER JOIN opportunities
        ON calls_info_base.parent_id = opportunities.id
        AND calls_info_base.parent_type = 'Opportunities'
      LEFT OUTER JOIN 
      /*%if o.history */
      ( 
        SELECT
          * 
        FROM
          accounts_history 
          INNER JOIN ( 
            SELECT
              id AS current_id
              , MAX(yearmonth) AS current_yearmonth 
            FROM
              accounts_history 
            WHERE
              yearmonth <= /* o.yearMonth */2000
              /*%if !o.div1.isEmpty() */
              AND sales_company_code IN /* o.div1 */('a', 'aa')
              /*%end*/
		      /*%if o.accountId != null*/
		      AND id = /* o.accountId */'2000000001'
		      /*%end*/
            GROUP BY 
              current_id
          ) current 
            ON current.current_id = accounts_history.id 
            AND current.current_yearmonth = accounts_history.yearmonth
      ) opportunity_accounts
      /*%else*/
      accounts AS opportunity_accounts
      /*%end*/
        ON account_id = opportunity_accounts.id 
        /*%if o.tantoOnly */
        AND users.id_for_customer = opportunity_accounts.assigned_user_id
        /*%end*/        
      /*%if o.checkExistsTech */
      LEFT OUTER JOIN tech_service_calls 
        ON calls_info_base.id = tech_service_calls.call_id 
      /*%end*/
      LEFT OUTER JOIN ( 
        SELECT
          count(calls_like_users.id) AS calls_like_count
          , calls_like_users.call_id 
        FROM
          /*%if o.tech */tech_service_calls/*%else*/calls/*%end*/ calls
         ,/*%if o.tech */tech_service_calls_like_users/*%else*/calls_like_users/*%end*/ calls_like_users
        WHERE
          calls.id = calls_like_users.call_id 
        GROUP BY
          call_id
      ) calls_like_count 
        ON calls_info_base.id = calls_like_count.call_id 
      LEFT OUTER JOIN ( 
        SELECT
          count(id) AS calls_read_count
          , calls_read_users.call_id 
        FROM
          /*%if o.tech */tech_service_calls_read_users/*%else*/calls_read_users/*%end*/ calls_read_users
        GROUP BY
          call_id
      ) calls_read_count 
        ON calls_info_base.id = calls_read_count.call_id 
      /*%if o.userId != null */
      LEFT OUTER JOIN ( 
        SELECT
          (CASE WHEN COUNT(id) > 0 THEN 1 ELSE 0 END) AS calls_read
          , calls_read_users.call_id 
        FROM
          /*%if o.tech */tech_service_calls_read_users/*%else*/calls_read_users/*%end*/ calls_read_users
        WHERE
          calls_read_users.user_id = /* o.userId */'sfa' 
        GROUP BY
          call_id
      ) calls_read 
        ON calls_info_base.id = calls_read.call_id 
      LEFT OUTER JOIN ( 
        SELECT
          count(id) AS comment_read_count
          , comments_read_users.call_id 
        FROM
          /*%if o.tech */tech_service_comments_read_users/*%else*/comments_read_users/*%end*/ comments_read_users
        WHERE
          comments_read_users.user_id = /* o.userId */'sfa' 
        GROUP BY
          call_id
      ) comment_read_count 
        ON calls_info_base.id = comment_read_count.call_id 
      LEFT OUTER JOIN ( 
        SELECT
          (CASE WHEN COUNT(id) > 0 THEN 1 ELSE 0 END) AS comment_entered
          , parent_id 
        FROM
          /*%if o.tech */tech_service_comments/*%else*/comments/*%end*/ comments
        WHERE
          modified_user_id = /* o.userId */'sfa' 
          AND parent_type = /*%if o.tech */'TechServiceCalls'/*%else*/'calls'/*%end*/
          AND is_confirmation = 0 
        GROUP BY
          parent_id
      ) comment_entered 
        ON calls_info_base.id = comment_entered.parent_id 
      /*%end*/
      LEFT OUTER JOIN ( 
        SELECT
          count(id) AS comment_count
          , parent_id 
        FROM
          /*%if o.tech */tech_service_comments/*%else*/comments/*%end*/ comments
        WHERE
          parent_type = /*%if o.tech */'TechServiceCalls'/*%else*/'calls'/*%end*/
          AND is_confirmation = 0 
        GROUP BY
          parent_id
      ) comment_count 
        ON calls_info_base.id = comment_count.parent_id
      LEFT OUTER JOIN /*%if o.tech */tech_service_calls_files/*%else*/calls_files/*%end*/ calls_files
        ON calls_info_base.id = calls_files.id
      LEFT OUTER JOIN /*%if o.tech */tech_service_market_infos/*%else*/market_infos/*%end*/ market_infos
        ON calls_info_base.id = market_infos.call_id
      /*%if o.tech */
      LEFT OUTER JOIN users as created_user
      	ON calls_info_base.created_by = created_user.id
      /*%end*/
    WHERE
      calls_info_base.deleted <> 1 
      /*%if o.id != null */
      AND calls_info_base.id = /* o.id */'a' 
      /*%end*/
      /*%if o.heldOnly */
      AND calls_info_base.status = 'held' 
      /*%end*/
      /*%if o.hasInfoOnly */
      AND (
        calls_info_base.com_free_c <> '' 
        AND calls_info_base.com_free_c IS NOT NULL 
        OR market_infos.description <> '' 
        AND market_infos.description IS NOT NULL
      )
      /*%end*/
      /*%if o.callsInfoDiv != null */
      AND calls_info_base.info_div = /* o.callsInfoDiv */'a'
      /*%end*/
      /*%if o.marketInfoInfoDiv != null */
      AND market_infos.info_div = /* o.marketInfoInfoDiv */'a' 
      /*%end*/
      /*%if o.userId != null && o.unreadOnly */
      AND (calls_read.calls_read = 0 OR calls_read.calls_read IS NULL)
      /*%end*/
  ) calls_info
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'parentType'
      AND locale = /*o.locale*/'ja'
  ) AS parent_type 
    ON calls_info.parent_type = parent_type.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'status'
      AND locale = /*o.locale*/'ja'
  ) AS status 
    ON calls_info.status = status.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'infoDiv'
      AND locale = /*o.locale*/'ja'
  ) AS info_div 
    ON calls_info.info_div = info_div.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items_by_company 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'maker'
      AND locale = /*o.locale*/'ja'
      AND company = /*o.company*/'YTST'
  ) AS maker 
    ON calls_info.maker = maker.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'kind'
      AND locale = /*o.locale*/'ja'
  ) AS kind 
    ON calls_info.kind = kind.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'category'
      AND locale = /*o.locale*/'ja'
  ) AS category 
    ON calls_info.category = category.value 
  LEFT OUTER JOIN ( 
    SELECT
      value
      , label
      , deleted 
    FROM
      selection_items 
    WHERE
      entity_name = 'Calls' 
      AND property_name = 'sensitivity'
      AND locale = /*o.locale*/'ja'
  ) AS sensitivity 
    ON calls_info.sensitivity = sensitivity.value 
  LEFT OUTER JOIN markets 
    ON calls_info.market_id = markets.id
WHERE
    1 = 1 /**LIMITがあると自動削除されないのでダミーを入れる */
  /*%if !o.canIgnoreSosikiCondition() */
    /*%if !o.div1.isEmpty() */
    AND accounts_sales_company_code IN /* o.div1 */('a', 'aa') 
    /*%end*/
    /*%if o.div2 != null */
    AND accounts_department_code = /* o.div2 */'a' 
    /*%end*/
    /*%if o.div3 != null */
    AND accounts_sales_office_code = /* o.div3 */'a' 
    /*%end*/
  /*%end*/
    /*%if o.market != null */
    AND market_id =  /* o.market */'a'
    /*%end*/
    /*%if o.accountId != null*/
    AND accounts_id = /* o.accountId */'2000000001'
    /*%end*/
    /*%if o.parentId != null */
    AND parent_id = /* o.parentId */'14b4c8ea-0201-2393-058a-22d809b19aa5'
    /*%end*/
LIMIT
  /*o.maxCount*/100
