SELECT
  calls_accounts.account_id
  , accounts.name AS account_name
  , users.last_name
  , users.first_name
  , mst_eigyo_sosiki.div2_nm
  , mst_eigyo_sosiki.div3_nm
FROM
  calls_accounts 
  INNER JOIN users
    ON calls_accounts.user_id = users.id
  INNER JOIN 
  /*%if isHistory */
  ( 
    SELECT
      * 
    FROM
      accounts_history 
      INNER JOIN ( 
        SELECT
          id AS current_id
          , MAX(yearmonth) AS current_yearmonth 
        FROM
          accounts_history 
        WHERE
          yearmonth <= /* yearMonth */2000
          /*%if !div1.isEmpty() */
          AND sales_company_code IN /* div1 */('a', 'aa') /**一つの得意先は販社をまたいだ部署変更はあり得ない*/
          /*%end*/
        GROUP BY
          current_id
      ) current 
        ON current.current_id = accounts_history.id 
        AND current.current_yearmonth = accounts_history.yearmonth
  ) accounts 
  /*%else*/
  accounts 
  /*%end*/
    ON calls_accounts.account_id = accounts.id
    AND users.id_for_customer = accounts.assigned_user_id /**担当内訪問のみ検索のため、この条件を入れる */
  INNER JOIN
  /*%if isHistory */
  ( 
    SELECT
      * 
    FROM
      mst_eigyo_sosiki_history 
    WHERE
      year = /*year*/2015 
      AND month = /*month*/01
  ) mst_eigyo_sosiki
  /*%else*/
  mst_eigyo_sosiki 
  /*%end*/
    ON accounts.sales_company_code = mst_eigyo_sosiki.div1_cd
    AND accounts.department_code = mst_eigyo_sosiki.div2_cd
    AND accounts.sales_office_code = mst_eigyo_sosiki.div3_cd
  WHERE
    calls_accounts.year = /* year */2015
    AND calls_accounts.month = /* month */1
    /*%if !div1.isEmpty() */
    AND accounts.sales_company_code IN /* div1 */('a', 'aa') 
    /*%end*/
    /*%if div2 != null */
    AND accounts.department_code = /* div2 */'a'
    /*%end*/
    /*%if div3 != null */
    AND accounts.sales_office_code = /* div3 */'a'
    /*%end*/
    /*%if tanto != null */
    AND accounts.assigned_user_id = /* idForCustomer */'a'
    /*%end*/
    /*%if "GROUP_IMPORTANT".equals(group) */
    AND accounts.group_important = 1 
    /*%end*/
    /*%if "GROUP_NEW".equals(group) */
    AND accounts.group_new = 1 
    /*%end*/
    /*%if "GROUP_DEEP_PLOWING".equals(group) */
    AND accounts.group_deep_plowing = 1 
    /*%end*/
    /*%if "GROUP_Y_CP".equals(group) */
    AND accounts.group_y_cp = 1 
    /*%end*/
    /*%if "GROUP_OTHER_CP".equals(group) */
    AND accounts.group_other_cp = 1 
    /*%end*/
    /*%if "GROUP_ONE".equals(group) */
    AND accounts.group_one = 1 
    /*%end*/
    /*%if "GROUP_TWO".equals(group) */
    AND accounts.group_two = 1 
    /*%end*/
    /*%if "GROUP_THREE".equals(group) */
    AND accounts.group_three = 1 
    /*%end*/
    /*%if "GROUP_FOUR".equals(group) */
    AND accounts.group_four = 1 
    /*%end*/
    /*%if "GROUP_FIVE".equals(group) */
    AND accounts.group_five = 1 
    /*%end*/
    /*%if "A".equals(group) */
    AND accounts.sales_rank = 'A' 
    /*%end*/
    /*%if "B".equals(group) */
    AND accounts.sales_rank = 'B' 
    /*%end*/
    /*%if "C".equals(group) */
    AND accounts.sales_rank = 'C' 
    /*%end*/
  ORDER BY
    mst_eigyo_sosiki.eigyo_sosiki_cd
    , accounts.assigned_user_id
    , accounts.id