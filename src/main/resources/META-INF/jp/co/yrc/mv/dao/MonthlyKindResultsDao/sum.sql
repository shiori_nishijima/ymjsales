SELECT
        monthly_kind_results.year
        ,monthly_kind_results.month
        ,SUM(monthly_kind_results.pcr_summer_sales) AS pcr_summer_sales
        ,SUM(monthly_kind_results.pcr_summer_margin) AS pcr_summer_margin
        ,SUM(monthly_kind_results.pcr_summer_number) AS pcr_summer_number
        ,SUM(monthly_kind_results.pcr_pure_snow_sales) AS pcr_pure_snow_sales
        ,SUM(monthly_kind_results.pcr_pure_snow_margin) AS pcr_pure_snow_margin
        ,SUM(monthly_kind_results.pcr_pure_snow_number) AS pcr_pure_snow_number
        ,SUM(monthly_kind_results.van_summer_sales) AS van_summer_sales
        ,SUM(monthly_kind_results.van_summer_margin) AS van_summer_margin
        ,SUM(monthly_kind_results.van_summer_number) AS van_summer_number
        ,SUM(monthly_kind_results.van_pure_snow_sales) AS van_pure_snow_sales
        ,SUM(monthly_kind_results.van_pure_snow_margin) AS van_pure_snow_margin
        ,SUM(monthly_kind_results.van_pure_snow_number) AS van_pure_snow_number
        ,SUM(monthly_kind_results.lt_summer_sales) AS lt_summer_sales
        ,SUM(monthly_kind_results.lt_summer_margin) AS lt_summer_margin
        ,SUM(monthly_kind_results.lt_summer_number) AS lt_summer_number
        ,SUM(monthly_kind_results.lt_snow_sales) AS lt_snow_sales
        ,SUM(monthly_kind_results.lt_snow_margin) AS lt_snow_margin
        ,SUM(monthly_kind_results.lt_snow_number) AS lt_snow_number
        ,SUM(monthly_kind_results.lt_as_sales) AS lt_as_sales
        ,SUM(monthly_kind_results.lt_as_margin) AS lt_as_margin
        ,SUM(monthly_kind_results.lt_as_number) AS lt_as_number
        ,SUM(monthly_kind_results.lt_pure_snow_sales) AS lt_pure_snow_sales
        ,SUM(monthly_kind_results.lt_pure_snow_margin) AS lt_pure_snow_margin
        ,SUM(monthly_kind_results.lt_pure_snow_number) AS lt_pure_snow_number
        ,SUM(monthly_kind_results.tb_summer_sales) AS tb_summer_sales
        ,SUM(monthly_kind_results.tb_summer_margin) AS tb_summer_margin
        ,SUM(monthly_kind_results.tb_summer_number) AS tb_summer_number
        ,SUM(monthly_kind_results.tb_snow_sales) AS tb_snow_sales
        ,SUM(monthly_kind_results.tb_snow_margin) AS tb_snow_margin
        ,SUM(monthly_kind_results.tb_snow_number) AS tb_snow_number
        ,SUM(monthly_kind_results.tb_as_sales) AS tb_as_sales
        ,SUM(monthly_kind_results.tb_as_margin) AS tb_as_margin
        ,SUM(monthly_kind_results.tb_as_number) AS tb_as_number
        ,SUM(monthly_kind_results.tb_pure_snow_sales) AS tb_pure_snow_sales
        ,SUM(monthly_kind_results.tb_pure_snow_margin) AS tb_pure_snow_margin
        ,SUM(monthly_kind_results.tb_pure_snow_number) AS tb_pure_snow_number
        ,SUM(monthly_kind_results.id_sales) AS id_sales
        ,SUM(monthly_kind_results.id_margin) AS id_margin
        ,SUM(monthly_kind_results.id_number) AS id_number
        ,SUM(monthly_kind_results.or_sales) AS or_sales
        ,SUM(monthly_kind_results.or_margin) AS or_margin
        ,SUM(monthly_kind_results.or_number) AS or_number
        ,SUM(monthly_kind_results.tifu_sales) AS tifu_sales
        ,SUM(monthly_kind_results.tifu_margin) AS tifu_margin
        ,SUM(monthly_kind_results.tifu_number) AS tifu_number
        ,SUM(monthly_kind_results.other_sales) AS other_sales
        ,SUM(monthly_kind_results.other_margin) AS other_margin
        ,SUM(monthly_kind_results.work_sales) AS work_sales
        ,SUM(monthly_kind_results.work_margin) AS work_margin
        ,SUM(monthly_kind_results.retread_sales) AS retread_sales
        ,SUM(monthly_kind_results.retread_margin) AS retread_margin
        ,SUM(monthly_kind_results.retread_number) AS retread_number
        ,SUM(monthly_kind_results.retread_lt_summer_sales) AS retread_lt_summer_sales
        ,SUM(monthly_kind_results.retread_lt_summer_margin) AS retread_lt_summer_margin
        ,SUM(monthly_kind_results.retread_lt_summer_number) AS retread_lt_summer_number
        ,SUM(monthly_kind_results.retread_tb_summer_sales) AS retread_tb_summer_sales
        ,SUM(monthly_kind_results.retread_tb_summer_margin) AS retread_tb_summer_margin
        ,SUM(monthly_kind_results.retread_tb_summer_number) AS retread_tb_summer_number
        ,SUM(monthly_kind_results.retread_special_tire_summer_sales) AS retread_special_tire_summer_sales
        ,SUM(monthly_kind_results.retread_special_tire_summer_margin) AS retread_special_tire_summer_margin
        ,SUM(monthly_kind_results.retread_special_tire_summer_number) AS retread_special_tire_summer_number
        ,SUM(monthly_kind_results.retread_lt_snow_sales) AS retread_lt_snow_sales
        ,SUM(monthly_kind_results.retread_lt_snow_margin) AS retread_lt_snow_margin
        ,SUM(monthly_kind_results.retread_lt_snow_number) AS retread_lt_snow_number
        ,SUM(monthly_kind_results.retread_tb_snow_sales) AS retread_tb_snow_sales
        ,SUM(monthly_kind_results.retread_tb_snow_margin) AS retread_tb_snow_margin
        ,SUM(monthly_kind_results.retread_tb_snow_number) AS retread_tb_snow_number
        ,SUM(monthly_kind_results.pcb_summer_sales) AS pcb_summer_sales
        ,SUM(monthly_kind_results.pcb_summer_margin) AS pcb_summer_margin
        ,SUM(monthly_kind_results.pcb_summer_number) AS pcb_summer_number
        ,SUM(monthly_kind_results.pcb_pure_snow_sales) AS pcb_pure_snow_sales
        ,SUM(monthly_kind_results.pcb_pure_snow_margin) AS pcb_pure_snow_margin
        ,SUM(monthly_kind_results.pcb_pure_snow_number) AS pcb_pure_snow_number
        ,SUM(monthly_kind_results.pcr_summer_hq_margin) AS pcr_summer_hq_margin
        ,SUM(monthly_kind_results.pcr_pure_snow_hq_margin) AS pcr_pure_snow_hq_margin
        ,SUM(monthly_kind_results.van_summer_hq_margin) AS van_summer_hq_margin
        ,SUM(monthly_kind_results.van_pure_snow_hq_margin) AS van_pure_snow_hq_margin
        ,SUM(monthly_kind_results.pcb_summer_hq_margin) AS pcb_summer_hq_margin
        ,SUM(monthly_kind_results.pcb_pure_snow_hq_margin) AS pcb_pure_snow_hq_margin
        ,SUM(monthly_kind_results.lt_summer_hq_margin) AS lt_summer_hq_margin
        ,SUM(monthly_kind_results.lt_snow_hq_margin) AS lt_snow_hq_margin
        ,SUM(monthly_kind_results.lt_as_hq_margin) AS lt_as_hq_margin
        ,SUM(monthly_kind_results.lt_pure_snow_hq_margin) AS lt_pure_snow_hq_margin
        ,SUM(monthly_kind_results.tb_summer_hq_margin) AS tb_summer_hq_margin
        ,SUM(monthly_kind_results.tb_snow_hq_margin) AS tb_snow_hq_margin
        ,SUM(monthly_kind_results.tb_as_hq_margin) AS tb_as_hq_margin
        ,SUM(monthly_kind_results.tb_pure_snow_hq_margin) AS tb_pure_snow_hq_margin
        ,SUM(monthly_kind_results.id_hq_margin) AS id_hq_margin
        ,SUM(monthly_kind_results.or_hq_margin) AS or_hq_margin
        ,SUM(monthly_kind_results.tifu_hq_margin) AS tifu_hq_margin
        ,SUM(monthly_kind_results.other_hq_margin) AS other_hq_margin
        ,SUM(monthly_kind_results.work_hq_margin) AS work_hq_margin
        ,SUM(monthly_kind_results.retread_hq_margin) AS retread_hq_margin
        ,SUM(monthly_kind_results.retread_lt_summer_hq_margin) AS retread_lt_summer_hq_margin
        ,SUM(monthly_kind_results.retread_tb_summer_hq_margin) AS retread_tb_summer_hq_margin
        ,SUM(monthly_kind_results.retread_special_tire_summer_hq_margin) AS retread_special_tire_summer_hq_margin
        ,SUM(monthly_kind_results.retread_lt_snow_hq_margin) AS retread_lt_snow_hq_margin
        ,SUM(monthly_kind_results.retread_tb_snow_hq_margin) AS retread_tb_snow_hq_margin
    FROM
        monthly_kind_results
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_kind_results.year = /* year */1
        AND monthly_kind_results.month = /* month */1
        AND monthly_kind_results.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_kind_results.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_kind_results.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_kind_results.year
		,monthly_kind_results.month
