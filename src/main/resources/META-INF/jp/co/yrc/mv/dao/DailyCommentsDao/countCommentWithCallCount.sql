SELECT
  count(id) 
FROM
  ( 
    /**日報コメントありの情報*/
    SELECT
      users.id
    FROM
      users 
      INNER JOIN 
      /*%if isHistory*/
      ( 
        SELECT
          * 
        FROM
          mst_tanto_s_sosiki_history 
        WHERE
          year = /*year*/2015 
          AND month = /*month*/01
      ) mst_tanto_s_sosiki
      /*%else*/
      mst_tanto_s_sosiki
      /*%end*/
        ON users.id = mst_tanto_s_sosiki.tanto_cd 
      INNER JOIN 
      /*%if isHistory*/
      ( 
        SELECT
          * 
        FROM
          mst_eigyo_sosiki_history 
        WHERE
          year = /*year*/2015 
          AND month = /*month*/01
      ) mst_eigyo_sosiki 
      /*%else*/
      mst_eigyo_sosiki
      /*%end*/
        ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
		/*%if !div1.isEmpty()*/
        AND mst_eigyo_sosiki.div1_cd IN /*div1*/('a', 'aa')
        /*%end*/
        /*%if div2 != null*/
        AND mst_eigyo_sosiki.div2_cd = /*div2*/'a'
        /*%end*/
        /*%if div3 != null*/
        AND mst_eigyo_sosiki.div3_cd = /*div3*/'a'
        /*%end*/
        /*%if userId != null*/
        AND users.id = /*userId*/'a'
        /*%end*/
      INNER JOIN ( 
        SELECT
          * 
        from
          daily_comments 
        WHERE
          daily_comments.year = /*year*/0 
          AND daily_comments.month = /*month*/0
          /*%if date > 0*/
          AND daily_comments.date = /*date*/0
          /*%end*/
      ) daily_comments 
        ON users.id = daily_comments.user_id
    /**日報コメントに存在せず訪問のみ存在する部分の抽出*/
    UNION ALL 
    SELECT
      users.id
    FROM
      users 
      INNER JOIN 
      /*%if isHistory*/
      ( 
        SELECT
          * 
        FROM
          mst_tanto_s_sosiki_history 
        WHERE
          year = /*year*/2015 
          AND month = /*month*/01
      ) mst_tanto_s_sosiki
      /*%else*/
      mst_tanto_s_sosiki
      /*%end*/
        ON users.id = mst_tanto_s_sosiki.tanto_cd 
      INNER JOIN 
      /*%if isHistory*/
      ( 
        SELECT
          * 
        FROM
          mst_eigyo_sosiki_history 
        WHERE
          year = /*year*/2015 
          AND month = /*month*/01
      ) mst_eigyo_sosiki 
      /*%else*/
      mst_eigyo_sosiki
      /*%end*/
        ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
		/*%if !div1.isEmpty()*/
        AND mst_eigyo_sosiki.div1_cd IN /*div1*/('a', 'aa')
        /*%end*/
        /*%if div2 != null*/
        AND mst_eigyo_sosiki.div2_cd = /*div2*/'a'
        /*%end*/
        /*%if div3 != null*/
        AND mst_eigyo_sosiki.div3_cd = /*div3*/'a'
        /*%end*/
        /*%if userId != null*/
        AND users.id = /*userId*/'a'
        /*%end*/
      INNER JOIN ( 
        SELECT
          SUM(calls.held_count) AS call_count
          , DATE(calls.date_start) AS date_start
          , assigned_user_id 
        FROM
          ( 
            SELECT
              calls.assigned_user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
            FROM
              calls 
            WHERE
              calls.date_start >= /*from*/2000 
              AND calls.date_start < /*to*/2000 
              AND calls.deleted <> 1 
            UNION ALL 
            SELECT
              calls_users.user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
            FROM
              calls 
              INNER JOIN calls_users 
                ON calls.id = calls_users.call_id 
            WHERE
              calls.date_start >= /*from*/2000 
              AND calls.date_start < /*to*/2000 
              AND calls.deleted <> 1 
          ) calls 
        GROUP BY
          assigned_user_id
          , DATE(calls.date_start)
      ) calls
        ON users.id = calls.assigned_user_id 
      LEFT OUTER JOIN daily_comments 
        ON daily_comments.user_id = calls.assigned_user_id 
        AND calls.date_start = DATE ( 
          concat( 
            daily_comments.year
            , '-'
            , daily_comments.month
            , '-'
            , daily_comments.date
          )
        ) 
    WHERE
      daily_comments.id IS NULL
  ) daily_comments
