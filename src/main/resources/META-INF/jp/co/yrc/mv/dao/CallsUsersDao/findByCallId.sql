SELECT
		calls_users.id
		,calls_users.call_id
		,calls_users.user_id
		,calls_users.date_modified
		,calls_users.deleted
		,users.first_name
		,users.last_name
	FROM
		/*%if tech */tech_service_calls_users/*%else*/calls_users/*%end*/ calls_users
		,users
	WHERE
		users.id = calls_users.user_id
		AND call_id = /* callId */'a'
