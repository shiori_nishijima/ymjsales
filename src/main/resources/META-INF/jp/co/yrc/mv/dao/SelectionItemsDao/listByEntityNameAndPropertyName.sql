SELECT
		entity_name
		,property_name
		,value
		,label
		,sort_order
		,deleted
		,date_entered
		,date_modified
		,modified_user_id
		,created_by
	FROM
		selection_items
	WHERE
		entity_name = /*entityName*/''
		AND property_name = /*propertyName*/''
		AND locale = /* locale */'ja'
	ORDER BY
		sort_order ASC
		,value ASC