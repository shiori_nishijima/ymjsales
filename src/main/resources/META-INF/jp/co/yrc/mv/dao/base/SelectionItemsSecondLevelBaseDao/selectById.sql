select
  key_entity_name,
  key_property_name,
  key_value,
  property_name,
  value,
  label,
  sort_order,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  selection_items_second_level
where
  key_entity_name = /* keyEntityName */'a'
  and
  key_property_name = /* keyPropertyName */'a'
  and
  key_value = /* keyValue */'a'
  and
  property_name = /* propertyName */'a'
  and
  value = /* value */'a'
  and
  key_locale = /* keyLocale */'ja'
