select
  id,
  name,
  name_native,
  billing_address_postalcode,
  billing_address,
  billing_address_native,
  parent_id,
  assigned_user_id,
  sales_company_code,
  department_code,
  sales_office_code,
  name_for_search,
  market_id,
  brand_id,
  own_count,
  deal_count,
  group_important,
  group_new,
  group_deep_plowing,
  group_y_cp,
  group_other_cp,
  group_one,
  group_two,
  group_three,
  group_four,
  group_five,
  sales_rank,
  demand_number,
  iss_yh,
  iss01,
  iss02,
  iss03,
  iss04,
  iss05,
  iss06,
  iss07,
  iss08,
  iss09,
  iss10,
  undeal,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
/*%if isHistory */
  (
      SELECT
        *
          FROM
              accounts_history
                  INNER JOIN (
                    SELECT
                        id AS current_id
                        ,MAX(yearmonth) AS current_yearmonth
                      FROM
                        accounts_history
                      WHERE
                        id = /* id */'a'	
                        AND yearmonth <= /* yearMonth */2000
                      GROUP BY current_id
                  ) current
                      ON accounts_history.id = /* id */'a'
                      AND current.current_id = accounts_history.id
                      AND current.current_yearmonth = accounts_history.yearmonth
  ) accounts
  /*%else*/
  accounts
/*%end*/
where
  id = /* id */'a'
