SELECT
        monthly_group_deals.year
        ,monthly_group_deals.month
		,SUM(monthly_group_deals.rank_a_own_count) AS rank_a_own_count
		,SUM(monthly_group_deals.rank_a_deal_count) AS rank_a_deal_count
		,SUM(monthly_group_deals.rank_a_account_master_count) AS rank_a_account_master_count
		,SUM(monthly_group_deals.rank_b_own_count) AS rank_b_own_count
		,SUM(monthly_group_deals.rank_b_deal_count) AS rank_b_deal_count
		,SUM(monthly_group_deals.rank_b_account_master_count) AS rank_b_account_master_count
		,SUM(monthly_group_deals.rank_c_own_count) AS rank_c_own_count
		,SUM(monthly_group_deals.rank_c_deal_count) AS rank_c_deal_count
		,SUM(monthly_group_deals.rank_c_account_master_count) AS rank_c_account_master_count
		,SUM(monthly_group_deals.important_own_count) AS important_own_count
		,SUM(monthly_group_deals.important_deal_count) AS important_deal_count
		,SUM(monthly_group_deals.important_account_master_count) AS important_account_master_count
		,SUM(monthly_group_deals.new_own_count) AS new_own_count
		,SUM(monthly_group_deals.new_deal_count) AS new_deal_count
		,SUM(monthly_group_deals.new_account_master_count) AS new_account_master_count
		,SUM(monthly_group_deals.deep_plowing_own_count) AS deep_plowing_own_count
		,SUM(monthly_group_deals.deep_plowing_deal_count) AS deep_plowing_deal_count
		,SUM(monthly_group_deals.deep_plowing_account_master_count) AS deep_plowing_account_master_count
		,SUM(monthly_group_deals.y_cp_own_count) AS y_cp_own_count
		,SUM(monthly_group_deals.y_cp_deal_count) AS y_cp_deal_count
		,SUM(monthly_group_deals.y_cp_account_master_count) AS y_cp_account_master_count
		,SUM(monthly_group_deals.other_cp_own_count) AS other_cp_own_count
		,SUM(monthly_group_deals.other_cp_deal_count) AS other_cp_deal_count
		,SUM(monthly_group_deals.other_cp_account_master_count) AS other_cp_account_master_count
		,SUM(monthly_group_deals.group_one_own_count) AS group_one_own_count
		,SUM(monthly_group_deals.group_one_deal_count) AS group_one_deal_count
		,SUM(monthly_group_deals.group_one_account_master_count) AS group_one_account_master_count
		,SUM(monthly_group_deals.group_two_own_count) AS group_two_own_count
		,SUM(monthly_group_deals.group_two_deal_count) AS group_two_deal_count
		,SUM(monthly_group_deals.group_two_account_master_count) AS group_two_account_master_count
		,SUM(monthly_group_deals.group_three_own_count) AS group_three_own_count
		,SUM(monthly_group_deals.group_three_deal_count) AS group_three_deal_count
		,SUM(monthly_group_deals.group_three_account_master_count) AS group_three_account_master_count
		,SUM(monthly_group_deals.group_four_own_count) AS group_four_own_count
		,SUM(monthly_group_deals.group_four_deal_count) AS group_four_deal_count
		,SUM(monthly_group_deals.group_four_account_master_count) AS group_four_account_master_count
		,SUM(monthly_group_deals.group_five_own_count) AS group_five_own_count
		,SUM(monthly_group_deals.group_five_deal_count) AS group_five_deal_count
		,SUM(monthly_group_deals.group_five_account_master_count) AS group_five_account_master_count
    FROM
        monthly_group_deals
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_group_deals.year = /* year */1
        AND monthly_group_deals.month = /* month */1
        AND monthly_group_deals.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_group_deals.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_group_deals.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_group_deals.year
		,monthly_group_deals.month
