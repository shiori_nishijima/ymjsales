SELECT
        monthly_market_plans.year
        ,monthly_market_plans.month
		,SUM(monthly_market_plans.ss_sales)  AS ss_sales
		,SUM(monthly_market_plans.ss_margin)  AS ss_margin
		,SUM(monthly_market_plans.ss_number)  AS ss_number
		,SUM(monthly_market_plans.rs_sales)  AS rs_sales
		,SUM(monthly_market_plans.rs_margin)  AS rs_margin
		,SUM(monthly_market_plans.rs_number)  AS rs_number
		,SUM(monthly_market_plans.cd_sales)  AS cd_sales
		,SUM(monthly_market_plans.cd_margin)  AS cd_margin
		,SUM(monthly_market_plans.cd_number)  AS cd_number
		,SUM(monthly_market_plans.ps_sales)  AS ps_sales
		,SUM(monthly_market_plans.ps_margin)  AS ps_margin
		,SUM(monthly_market_plans.ps_number)  AS ps_number
		,SUM(monthly_market_plans.cs_sales)  AS cs_sales
		,SUM(monthly_market_plans.cs_margin)  AS cs_margin
		,SUM(monthly_market_plans.cs_number)  AS cs_number
		,SUM(monthly_market_plans.hc_sales)  AS hc_sales
		,SUM(monthly_market_plans.hc_margin)  AS hc_margin
		,SUM(monthly_market_plans.hc_number)  AS hc_number
		,SUM(monthly_market_plans.lease_sales)  AS lease_sales
		,SUM(monthly_market_plans.lease_margin)  AS lease_margin
		,SUM(monthly_market_plans.lease_number)  AS lease_number
		,SUM(monthly_market_plans.indirect_other_sales)  AS indirect_other_sales
		,SUM(monthly_market_plans.indirect_other_margin)  AS indirect_other_margin
		,SUM(monthly_market_plans.indirect_other_number)  AS indirect_other_number
		,SUM(monthly_market_plans.truck_sales)  AS truck_sales
		,SUM(monthly_market_plans.truck_margin)  AS truck_margin
		,SUM(monthly_market_plans.truck_number)  AS truck_number
		,SUM(monthly_market_plans.bus_sales)  AS bus_sales
		,SUM(monthly_market_plans.bus_margin)  AS bus_margin
		,SUM(monthly_market_plans.bus_number)  AS bus_number
		,SUM(monthly_market_plans.hire_taxi_sales)  AS hire_taxi_sales
		,SUM(monthly_market_plans.hire_taxi_margin)  AS hire_taxi_margin
		,SUM(monthly_market_plans.hire_taxi_number)  AS hire_taxi_number
		,SUM(monthly_market_plans.direct_other_sales)  AS direct_other_sales
		,SUM(monthly_market_plans.direct_other_margin)  AS direct_other_margin
		,SUM(monthly_market_plans.direct_other_number)  AS direct_other_number
		,SUM(monthly_market_plans.retail_sales)  AS retail_sales
		,SUM(monthly_market_plans.retail_margin)  AS retail_margin
		,SUM(monthly_market_plans.retail_number)  AS retail_number
    FROM
        monthly_market_plans
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_market_plans.year = /* year */1
        AND monthly_market_plans.month = /* month */1
        AND monthly_market_plans.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_market_plans.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_market_plans.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_market_plans.year
		,monthly_market_plans.month
