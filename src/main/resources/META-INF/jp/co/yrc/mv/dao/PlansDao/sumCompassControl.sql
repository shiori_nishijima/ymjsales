SELECT
    plan.sales
    ,plan.margin
    ,plan.number
    ,plan_control.report_display_seq
    ,plan_control.report_display_name
    ,plan_control.plan_product_kind
    ,plan_control.plan_product_kind_name
  FROM
    (
      SELECT
          SUM(plans.sales) AS sales
          ,SUM(plans.margin) AS margin
          ,SUM(plans.number) AS number
          ,product_kind_cd
        FROM
          plans
          ,users
        WHERE
          plans.year = /* year */2000
          AND plans.month = /* month */1
          AND users.id_for_customer = plans.assigned_user_id
      /*%if !div1.isEmpty() */
          AND plans.sales_company_code IN /* div1 */('a', 'aa')
        /*%if div1.size() == 1 */
          /*%if div2 != null */
          AND plans.department_code = /* div2 */'a'
          /*%end*/
          /*%if div3 != null */
          AND plans.sales_office_code = /* div3 */'a'
          /*%end*/
          /*%if userId != null */
          AND users.id = /* userId */'sfa'
          /*%end*/
        /*%end*/
      /*%end*/
        /*%if div1.isEmpty() */
          /*%if userId != null */
          AND users.id = /* userId */'sfa'
          /*%end*/
        /*%end*/
        GROUP BY product_kind_cd
    ) plan
    ,(
      SELECT
          DISTINCT year
          ,month
          ,report_display_seq
          ,report_display_name
          ,plan_product_kind
          ,plan_product_kind_name
        FROM
          compass_controls
        WHERE
          year = /* year */2000
          AND month = /* month */1
    ) plan_control
  WHERE
      plan.product_kind_cd = plan_control.plan_product_kind
  ORDER BY
    plan_control.report_display_seq
    ,plan_control.plan_product_kind