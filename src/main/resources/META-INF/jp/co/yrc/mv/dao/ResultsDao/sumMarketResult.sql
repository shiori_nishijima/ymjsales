SELECT
        results.year
        ,results.month
        ,null AS date
        ,null AS account_id
        ,results.market_id AS compass_kind
        ,SUM(results.sales) AS sales
        ,SUM(results.margin) AS margin
		,SUM(
			CASE
				WHEN
					/** TIFU */
					compass_kinds.middle_class = '91'
					/** OTHER */
					OR compass_kinds.middle_class = '92'
					OR compass_kinds.middle_class = '93'
					OR compass_kinds.middle_class = '94'
					OR compass_kinds.middle_class = '95'
					/** WORK */
					OR compass_kinds.middle_class = '96'
					OR compass_kinds.middle_class = '97'
					/** RETREAD */
					OR compass_kinds.middle_class = 'A1'
					OR compass_kinds.middle_class = 'A2'
					OR compass_kinds.middle_class = 'A3'
					OR compass_kinds.middle_class = 'B1'
					OR compass_kinds.middle_class = 'B2'
				THEN 0 
			ELSE results.number END
		) AS number
    FROM
		(
			SELECT
			        results.year
			        ,results.month
			        ,results.compass_kind
			        ,accounts.market_id
			        ,SUM(results.sales) AS sales
			        ,SUM(results.margin) AS margin
			        ,SUM(results.number) AS number
			    FROM
			        results
			    	,
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
                                                /*%if !div1.isEmpty() */
                                                AND sales_company_code IN /* div1 */('a', 'aa')
                                                /*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    	,users
			    WHERE
			        results.year = /* year */2000
			        AND results.month = /* month */1
			    /*%if date != null */
			        AND results.date = /* date */1
			    /*%end*/
			        AND results.account_id = accounts.id
			        AND users.id_for_customer = accounts.assigned_user_id
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			        /*%if userId != null */
			        AND users.id = /* userId */'sfa'
			        /*%end*/
			    /*%end*/
			/*%end*/
			/*%if div1.isEmpty() */
			        /*%if userId != null */
			        AND users.id = /* userId */'sfa'
			        /*%end*/
			/*%end*/
			    GROUP BY
					results.year
					,results.month
					,results.compass_kind
					,accounts.market_id
		) results
		,compass_kinds
    WHERE
		compass_kinds.id = results.compass_kind
		AND (
			compass_kinds.middle_class = '11'
			OR compass_kinds.middle_class = '12'
			OR compass_kinds.middle_class = '13'
			OR compass_kinds.middle_class = '21'
			OR compass_kinds.middle_class = '22'
			OR compass_kinds.middle_class = '51'
			OR compass_kinds.middle_class = '61'
			OR compass_kinds.middle_class = '62'
			OR compass_kinds.middle_class = '71'
			OR compass_kinds.middle_class = '72'
			/** OR */
			OR (
				compass_kinds.middle_class = '41'
				AND compass_kinds.small_class = '50'
			)
			/** ID */
			OR (
				compass_kinds.middle_class = '41'
				AND compass_kinds.small_class = '55'
			)
			/** TIFU */
			OR compass_kinds.middle_class = '91'
			/** OTHER */
			OR compass_kinds.middle_class = '92'
			OR compass_kinds.middle_class = '93'
			OR compass_kinds.middle_class = '94'
			OR compass_kinds.middle_class = '95'
			/** WORK */
			OR compass_kinds.middle_class = '96'
			OR compass_kinds.middle_class = '97'
			/** RETREAD */
			OR compass_kinds.middle_class = 'A1'
			OR compass_kinds.middle_class = 'A2'
			OR compass_kinds.middle_class = 'A3'
			OR compass_kinds.middle_class = 'B1'
			OR compass_kinds.middle_class = 'B2'
		)
    GROUP BY
		results.year
		,results.month
        ,results.market_id