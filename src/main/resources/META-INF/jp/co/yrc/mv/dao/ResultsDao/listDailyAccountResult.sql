SELECT
        results.year
        ,results.month
        ,results.date
        ,results.account_id
        ,null AS compass_kind
        ,SUM(results.sales) AS sales
        ,SUM(results.margin) AS margin
		,SUM(
			CASE
				WHEN
					/** TIFU */
					compass_kinds.middle_class = '91'
					/** OTHER */
					OR compass_kinds.middle_class = '92'
					OR compass_kinds.middle_class = '93'
					OR compass_kinds.middle_class = '94'
					OR compass_kinds.middle_class = '95'
					/** WORK */
					OR compass_kinds.middle_class = '96'
					OR compass_kinds.middle_class = '97'
					/** RETREAD */
					OR compass_kinds.middle_class = 'A1'
					OR compass_kinds.middle_class = 'A2'
					OR compass_kinds.middle_class = 'A3'
					OR compass_kinds.middle_class = 'B1'
					OR compass_kinds.middle_class = 'B2'
				THEN 0 
			ELSE results.number END
		) AS number
    FROM
		(
			SELECT
			        results.year
			        ,results.month
			        ,results.date
			        ,results.account_id
			        ,results.compass_kind
			        ,SUM(results.sales) AS sales
			        ,SUM(results.margin) AS margin
			        ,SUM(results.number) AS number
			    FROM
			        results
			    WHERE
			        results.year = /* year */2000
			        AND results.month = /* month */1
			        AND results.account_id = /* accountId  */'acounts'
			    GROUP BY
					results.year
					,results.month
			        ,results.date
					,results.account_id
					,results.compass_kind
		) results
		,compass_kinds
    WHERE
		compass_kinds.id = results.compass_kind
		AND (
			compass_kinds.middle_class = '11'
			OR compass_kinds.middle_class = '12'
			OR compass_kinds.middle_class = '13'
			OR compass_kinds.middle_class = '21'
			OR compass_kinds.middle_class = '22'
			OR compass_kinds.middle_class = '51'
			OR compass_kinds.middle_class = '61'
			OR compass_kinds.middle_class = '62'
			OR compass_kinds.middle_class = '71'
			OR compass_kinds.middle_class = '72'
			/** OR */
			OR (
				compass_kinds.middle_class = '41'
				AND compass_kinds.small_class = '50'
			)
			/** ID */
			OR (
				compass_kinds.middle_class = '41'
				AND compass_kinds.small_class = '55'
			)
			/** TIFU */
			OR compass_kinds.middle_class = '91'
			/** OTHER */
			OR compass_kinds.middle_class = '92'
			OR compass_kinds.middle_class = '93'
			OR compass_kinds.middle_class = '94'
			OR compass_kinds.middle_class = '95'
			/** WORK */
			OR compass_kinds.middle_class = '96'
			OR compass_kinds.middle_class = '97'
			/** RETREAD */
			OR compass_kinds.middle_class = 'A1'
			OR compass_kinds.middle_class = 'A2'
			OR compass_kinds.middle_class = 'A3'
			OR compass_kinds.middle_class = 'B1'
			OR compass_kinds.middle_class = 'B2'
		)
    GROUP BY
		results.year
		,results.month
		,results.account_id
        ,results.date

