SELECT
  calls_accounts.account_id
  , accounts.name AS account_name
FROM
  calls_accounts 
  INNER JOIN 
  accounts 
    ON calls_accounts.account_id = accounts.id
  WHERE
    calls_accounts.year = /* year */2015
    AND calls_accounts.month = /* month */1
    AND calls_accounts.user_id = /* tanto */'a'
  ORDER BY
  	calls_accounts.user_id
    , accounts.id