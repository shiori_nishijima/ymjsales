SELECT
		comments.id
		,comments.name
		,comments.date_entered
		,comments.date_modified
		,comments.modified_user_id
		,comments.created_by
		,comments.description
		,comments.deleted
		,comments.assigned_user_id
		,comments.parent_id
		,comments.parent_type
		,comments.is_confirmation
	FROM
		/*%if tech */tech_service_comments/*%else*/comments/*%end*/ comments
	WHERE
		parent_id = /* callId */'a'
		AND parent_type = /*%if tech */'TechServiceCalls'/*%else*/'Calls'/*%end*/
	ORDER BY
		date_modified DESC
