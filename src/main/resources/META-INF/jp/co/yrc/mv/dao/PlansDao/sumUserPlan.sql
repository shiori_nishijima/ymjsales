SELECT
        plans.year
        ,plans.month
        ,SUM(plans.sales) AS sales
        ,SUM(plans.margin) AS margin
        ,SUM(
            CASE
                WHEN
					/** TIFU */
					plans.middle_class = '91'
					/** OTHER */
					OR plans.middle_class = '92'
					OR plans.middle_class = '93'
					OR plans.middle_class = '94'
					OR plans.middle_class = '95'
					/** WORK */
					OR plans.middle_class = '96'
					OR plans.middle_class = '97'
					/** RETREAD */
					OR plans.middle_class = 'A1'
					OR plans.middle_class = 'A2'
					OR plans.middle_class = 'A3'
					OR plans.middle_class = 'B1'
					OR plans.middle_class = 'B2'
	            THEN 0 
            ELSE plans.number END
        ) AS number
    FROM
        plans
        ,users
    WHERE
        plans.year = /* year */2000
        AND plans.month = /* month */1
        AND users.id_for_customer = plans.assigned_user_id
/*%if !div1.isEmpty() */
        AND plans.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND plans.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND plans.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
		AND (
			plans.middle_class = '11'
			OR plans.middle_class = '12' 
			OR plans.middle_class = '13' 
			OR plans.middle_class = '21' 
			OR plans.middle_class = '22' 
			OR plans.middle_class = '51' 
			OR plans.middle_class = '61' 
			OR plans.middle_class = '62' 
			OR plans.middle_class = '71' 
			OR plans.middle_class = '72' 
			OR plans.middle_class = '41' 
			OR plans.middle_class = '91' 
			/** OTHER */
			OR plans.middle_class = '92'
			OR plans.middle_class = '93'
			OR plans.middle_class = '94'
			OR plans.middle_class = '95'
			/** WORK */
			OR plans.middle_class = '96'
			OR plans.middle_class = '97'
			/** RETREAD */
			OR plans.middle_class = 'A1'
			OR plans.middle_class = 'A2'
			OR plans.middle_class = 'A3'
			OR plans.middle_class = 'B1'
			OR plans.middle_class = 'B2'
		)
/*%end*/
    GROUP BY
        plans.year
        ,plans.month
