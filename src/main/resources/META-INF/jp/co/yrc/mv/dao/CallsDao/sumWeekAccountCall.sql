SELECT
  id AS group_type
  , calls.name AS account_name
  , calls.date_start
  , calls.ampm
  , SUM(calls.planned_count) AS planned_count
  , SUM(calls.held_count) AS call_count
FROM
  ( 
    SELECT
      accounts.id
      , accounts.name
      , DATE(calls.date_start) AS date_start
	  , DATE_FORMAT(calls.date_start, '%p') AS ampm
      , IF (calls.is_plan, calls.planned_count, 0) AS planned_count
      , IF (calls.status = 'Held', calls.held_count, 0) AS held_count
    FROM
      calls 
      INNER JOIN accounts 
        ON calls.parent_type = 'Accounts' 
        AND calls.parent_id = accounts.id 
    WHERE
      calls.date_start >= /*from*/'2016-01-01 00:00:00.000' 
      AND calls.date_start < /*to*/'2016-01-08 00:00:00.000' 
      AND calls.assigned_user_id = /* userId */'sfa'
      AND calls.deleted <> 1 
    UNION ALL
    SELECT
      opportunities.account_id AS id
      , accounts.name
      , DATE(calls.date_start) AS date_start
	  , DATE_FORMAT(calls.date_start, '%p') AS ampm
      , IF (calls.is_plan, calls.planned_count, 0) AS planned_count
      , IF (calls.status = 'Held', calls.held_count, 0) AS held_count
    FROM
      calls 
      INNER JOIN opportunities 
        on calls.parent_type = 'Opportunities' 
        AND calls.parent_id = opportunities.id 
      INNER JOIN accounts 
        ON opportunities.account_id = accounts.id 
    WHERE
      calls.date_start >= /*from*/'2016-01-01 00:00:00.000' 
      AND calls.date_start < /*to*/'2016-01-08 00:00:00.000' 
      AND calls.assigned_user_id = /* userId */'sfa'
      AND calls.deleted <> 1 
  ) calls 
GROUP BY
  group_type
  , calls.date_start 
ORDER BY
  group_type ASC
