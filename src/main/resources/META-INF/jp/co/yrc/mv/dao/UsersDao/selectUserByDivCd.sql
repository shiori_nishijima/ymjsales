SELECT DISTINCT
  users.id,
  users.first_name,
  users.last_name,
  mst_eigyo_sosiki.eigyo_sosiki_nm
FROM
  users
  ,
	/*%if isHistory */
	(
	  SELECT
	      *
	    FROM
	      mst_tanto_s_sosiki_history
	    WHERE
	        year = /*year*/2015
	        AND month = /*month*/01
	) mst_tanto_s_sosiki
	/*%else*/
	mst_tanto_s_sosiki
	/*%end*/
  ,
	/*%if isHistory */
	(
	  SELECT
	      *
	    FROM
	      mst_eigyo_sosiki_history
	    WHERE
	       year = /*year*/2015
	       AND month = /*month*/01
	) mst_eigyo_sosiki
	/*%else*/
	mst_eigyo_sosiki
	/*%end*/
WHERE
  id = /* id */'a'
  AND users.deleted = 0
  AND mst_eigyo_sosiki.deleted = 0
  AND mst_tanto_s_sosiki.deleted = 0
  AND mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
  AND users.id = mst_tanto_s_sosiki.tanto_cd
  AND mst_eigyo_sosiki.corp_cd in /* corpCodes */('a', 'b', 'c')
  /*%if div1 != null */
  AND mst_eigyo_sosiki.div1_cd = /* div1 */'a'
  /*%end*/
  /*%if div2 != null */
  AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
  /*%end*/
  /*%if div3 != null */
  AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
  /*%end*/