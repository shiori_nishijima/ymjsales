SELECT
        monthly_group_results.year
        ,monthly_group_results.month
        ,SUM(monthly_group_results.rank_a_sales) AS rank_a_sales
        ,SUM(monthly_group_results.rank_a_margin) AS rank_a_margin
        ,SUM(monthly_group_results.rank_a_number) AS rank_a_number
        ,SUM(monthly_group_results.rank_b_sales) AS rank_b_sales
        ,SUM(monthly_group_results.rank_b_margin) AS rank_b_margin
        ,SUM(monthly_group_results.rank_b_number) AS rank_b_number
        ,SUM(monthly_group_results.rank_c_sales) AS rank_c_sales
        ,SUM(monthly_group_results.rank_c_margin) AS rank_c_margin
        ,SUM(monthly_group_results.rank_c_number) AS rank_c_number
        ,SUM(monthly_group_results.important_sales) AS important_sales
        ,SUM(monthly_group_results.important_margin) AS important_margin
        ,SUM(monthly_group_results.important_number) AS important_number
        ,SUM(monthly_group_results.new_sales) AS new_sales
        ,SUM(monthly_group_results.new_margin) AS new_margin
        ,SUM(monthly_group_results.new_number) AS new_number
        ,SUM(monthly_group_results.deep_plowing_sales) AS deep_plowing_sales
        ,SUM(monthly_group_results.deep_plowing_margin) AS deep_plowing_margin
        ,SUM(monthly_group_results.deep_plowing_number) AS deep_plowing_number
        ,SUM(monthly_group_results.y_cp_sales) AS y_cp_sales
        ,SUM(monthly_group_results.y_cp_margin) AS y_cp_margin
        ,SUM(monthly_group_results.y_cp_number) AS y_cp_number
        ,SUM(monthly_group_results.other_cp_sales) AS other_cp_sales
        ,SUM(monthly_group_results.other_cp_margin) AS other_cp_margin
        ,SUM(monthly_group_results.other_cp_number) AS other_cp_number
        ,SUM(monthly_group_results.group_one_sales) AS group_one_sales
        ,SUM(monthly_group_results.group_one_margin) AS group_one_margin
        ,SUM(monthly_group_results.group_one_number) AS group_one_number
        ,SUM(monthly_group_results.group_two_sales) AS group_two_sales
        ,SUM(monthly_group_results.group_two_margin) AS group_two_margin
        ,SUM(monthly_group_results.group_two_number) AS group_two_number
        ,SUM(monthly_group_results.group_three_sales) AS group_three_sales
        ,SUM(monthly_group_results.group_three_margin) AS group_three_margin
        ,SUM(monthly_group_results.group_three_number) AS group_three_number
        ,SUM(monthly_group_results.group_four_sales) AS group_four_sales
        ,SUM(monthly_group_results.group_four_margin) AS group_four_margin
        ,SUM(monthly_group_results.group_four_number) AS group_four_number
        ,SUM(monthly_group_results.group_five_sales) AS group_five_sales
        ,SUM(monthly_group_results.group_five_margin) AS group_five_margin
        ,SUM(monthly_group_results.group_five_number) AS group_five_number
    FROM
        monthly_group_results
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_group_results.year = /* year */1
        AND monthly_group_results.month = /* month */1
        AND monthly_group_results.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_group_results.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_group_results.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_group_results.year
		,monthly_group_results.month
