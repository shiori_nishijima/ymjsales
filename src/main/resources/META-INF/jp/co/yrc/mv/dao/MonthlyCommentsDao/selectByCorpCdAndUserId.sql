SELECT
		monthly_comments.id
		,monthly_comments.year
		,monthly_comments.month
		,monthly_comments.user_id
		,monthly_comments.plan_visit_num
		,monthly_comments.plan_info_num
		,monthly_comments.plan_demand_coefficient
		,monthly_comments.user_comment
		,monthly_comments.boss_comment1
		,monthly_comments.boss_comment2
		,monthly_comments.boss_comment3
		,monthly_comments.boss_comment4
		,monthly_comments.boss_comment5
		,monthly_comments.date_entered
		,monthly_comments.date_modified
		,monthly_comments.modified_user_id
		,monthly_comments.created_by
		,monthly_comments.deleted
		,usersMergedEigyo.first_name
		,usersMergedEigyo.last_name
		,usersMergedEigyo.eigyo_sosiki_nm
	FROM
		monthly_comments INNER JOIN (
			SELECT
					mst_eigyo_sosiki.eigyo_sosiki_nm
					,users.first_name
					,users.last_name
					,users.id AS user_id
				FROM
					/*%if isHistory */
					(
					  SELECT
					      *
					    FROM
					      mst_eigyo_sosiki_history
					    WHERE
					       year = /*year*/2015
					       AND month = /*month*/01
					) mst_eigyo_sosiki
					/*%else*/
					mst_eigyo_sosiki
					/*%end*/
						LEFT OUTER JOIN
							/*%if isHistory */
							(
							  SELECT
							      *
							    FROM
							      mst_tanto_s_sosiki_history
							    WHERE
							        year = /*year*/2015
							        AND month = /*month*/01
							) mst_tanto_s_sosiki
							/*%else*/
							mst_tanto_s_sosiki
							/*%end*/
							ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
							AND mst_tanto_s_sosiki.deleted = 0
						LEFT OUTER JOIN users
							ON users.id = mst_tanto_s_sosiki.tanto_cd
							AND users.deleted = 0
				WHERE
					mst_eigyo_sosiki.corp_cd in /* corpCodes */('a', 'b', 'c')
					AND mst_tanto_s_sosiki.tanto_cd = /* userId */'a'
					AND mst_eigyo_sosiki.deleted = 0
		) usersMergedEigyo
			ON monthly_comments.user_id = usersMergedEigyo.user_id
WHERE
	monthly_comments.year = /* year */'2014'
	AND monthly_comments.month = /* month */'1'
	AND monthly_comments.user_id = /* userId */'a'
limit 1;
