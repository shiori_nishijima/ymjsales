SELECT
	mst_eigyo_sosiki.div2_nm
	,mst_eigyo_sosiki.div3_nm
	,users.first_name
	,users.last_name
	,SUM(calls.held_count) AS call_count
	,SUM(CASE WHEN accounts.group_important = 1 THEN calls.held_count ELSE 0 END) AS group_important
	,SUM(CASE WHEN accounts.group_new = 1 THEN calls.held_count ELSE 0 END) AS group_new
	,SUM(CASE WHEN accounts.group_deep_plowing = 1 THEN calls.held_count ELSE 0 END) AS group_deep_plowing
	,SUM(CASE WHEN accounts.group_y_cp = 1 THEN calls.held_count ELSE 0 END) AS group_y_cp
	,SUM(CASE WHEN accounts.group_other_cp = 1 THEN calls.held_count ELSE 0 END) AS group_other_cp
	,SUM(CASE WHEN accounts.group_one = 1 THEN calls.held_count ELSE 0 END) AS group_one
	,SUM(CASE WHEN accounts.group_two = 1 THEN calls.held_count ELSE 0 END) AS group_two
	,SUM(CASE WHEN accounts.group_three = 1 THEN calls.held_count ELSE 0 END) AS group_three
	,SUM(CASE WHEN accounts.group_four = 1 THEN calls.held_count ELSE 0 END) AS group_four
	,SUM(CASE WHEN accounts.group_five = 1 THEN calls.held_count ELSE 0 END) AS group_five
  FROM
	calls
	,calls_users
	,users
	,
	/*%if isHistory */
	mst_tanto_s_sosiki_history
	/*%end*/
	mst_tanto_s_sosiki
	,
	/*%if isHistory */
	mst_eigyo_sosiki_history
	/*%end*/
	mst_eigyo_sosiki
	,
	/*%if isHistory */
	(
		SELECT
				*
			FROM
				accounts_history
					INNER JOIN (
						SELECT
								id AS current_id
								,MAX(yearmonth) AS current_yearmonth
							FROM
								accounts_history
								,(
									SELECT
											calls.parent_id AS account_id
										FROM
											calls
											,calls_users
											,users
											,
											/*%if isHistory */
											mst_tanto_s_sosiki_history
											/*%end*/
											mst_tanto_s_sosiki
											,
											/*%if isHistory */
											mst_eigyo_sosiki_history
											/*%end*/
											mst_eigyo_sosiki
										WHERE
											calls.id = calls_users.call_id
											AND calls.date_start >= /* from */'2000/01/01'
											AND calls.date_start < /* to */'2000/01/01'
											AND users.id = calls_users.user_id
											/*%if isHistory */
											AND mst_tanto_s_sosiki.year = /*year*/2015
											AND mst_tanto_s_sosiki.month = /*month*/01
											/*%end*/
											AND users.id = mst_tanto_s_sosiki.tanto_cd
											AND mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
											AND calls.status = 'Held'
											AND calls.parent_type = 'Accounts'
											AND calls.held_count > 0
											/*%if isHistory */
											AND mst_eigyo_sosiki.year = /*year*/2015
											AND mst_eigyo_sosiki.month = /*month*/01
											/*%end*/
											/** div1は必須前提 */
											AND mst_eigyo_sosiki.div1_cd = /* div1 */'a'
											/*%if div2 != null */
											AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
											/*%end*/
											/*%if div3 != null */
											AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
											/*%end*/
											/*%if userId != null*/
											AND users.id = /* userId */'a'
											/*%end*/
										GROUP BY
											calls.parent_id
								) calls_account
							WHERE
								yearmonth <= /* yearMonth */2000
								AND calls_account.account_id = accounts_history.id
							 GROUP BY current_id
					) current
						ON current.current_id = accounts_history.id
							AND current.current_yearmonth = accounts_history.yearmonth
						) accounts
			/*%else*/
						accounts
			/*%end*/
	WHERE
		calls.id = calls_users.call_id
		AND calls.date_start >= /* from */'2000/01/01'
		AND calls.date_start < /* to */'2000/01/01'
		AND users.id = calls_users.user_id
		/*%if isHistory */
		AND mst_tanto_s_sosiki.year = /*year*/2015
		AND mst_tanto_s_sosiki.month = /*month*/01
		/*%end*/
		AND users.id = mst_tanto_s_sosiki.tanto_cd
		AND mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
		AND calls.status = 'Held'
		AND calls.parent_type = 'Accounts'
		AND calls.parent_id = accounts.id
		AND calls.held_count > 0
		/*%if isHistory */
		AND mst_eigyo_sosiki.year = /*year*/2015
		AND mst_eigyo_sosiki.month = /*month*/01
		/*%end*/
		/** div1は必須前提 */
		AND mst_eigyo_sosiki.div1_cd = /* div1 */'a'
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
		AND users.id = /* userId */'a'
		/*%end*/
	GROUP BY
		mst_eigyo_sosiki.div2_nm
		,mst_eigyo_sosiki.div3_nm
		,users.first_name
		,users.last_name
	ORDER BY
		mst_eigyo_sosiki.div2_cd ASC
		,mst_eigyo_sosiki.div3_cd ASC
		,users.id ASC