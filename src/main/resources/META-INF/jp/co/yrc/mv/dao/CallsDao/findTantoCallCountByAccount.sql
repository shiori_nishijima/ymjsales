SELECT
  calls.parent_id
  , SUM(IF(calls.is_plan, calls.planned_count, 0)) AS planned_count
  , SUM(IF(calls.status = 'Held', calls.held_count, 0)) AS held_count
FROM
  /*%if isHistory */
  ( 
    SELECT
      * 
    FROM
      accounts_history 
      INNER JOIN ( 
        SELECT
          id AS current_id
          , MAX(yearmonth) AS current_yearmonth 
        FROM
          accounts_history 
        WHERE
          yearmonth <= /* yearMonth */2000
          /*%if !div1.isEmpty() */
          AND sales_company_code IN /* div1 */('a', 'aa') /**一つの得意先は販社をまたいだ部署変更はあり得ない*/
          /*%end*/
        GROUP BY
          current_id
      ) current 
        ON current.current_id = accounts_history.id 
        AND current.current_yearmonth = accounts_history.yearmonth
  ) accounts 
  /*%else*/
  accounts 
  /*%end*/
  INNER JOIN users
    ON accounts.assigned_user_id = users.id_for_customer
  INNER JOIN calls
    ON calls.parent_id = accounts.id
    AND calls.parent_type = 'Accounts'
    AND users.id = calls.assigned_user_id /**担当内訪問のみ検索のため、この条件を入れる */
WHERE
  calls.date_start >= /* from */2000
  AND calls.date_start < /* to */2000
  /*%if !div1.isEmpty() */
  AND accounts.sales_company_code IN /* div1 */('a', 'aa') 
  /*%end*/
  /*%if div2 != null */
  AND accounts.department_code = /* div2 */'a'
  /*%end*/
  /*%if div3 != null */
  AND accounts.sales_office_code = /* div3 */'a'
  /*%end*/
  /*%if tanto != null */
  AND users.id = /* tanto */'a'
  /*%end*/
  /*%if "GROUP_IMPORTANT".equals(group) */
  AND accounts.group_important = 1 
  /*%end*/
  /*%if "GROUP_NEW".equals(group) */
  AND accounts.group_new = 1 
  /*%end*/
  /*%if "GROUP_DEEP_PLOWING".equals(group) */
  AND accounts.group_deep_plowing = 1 
  /*%end*/
  /*%if "GROUP_Y_CP".equals(group) */
  AND accounts.group_y_cp = 1 
  /*%end*/
  /*%if "GROUP_OTHER_CP".equals(group) */
  AND accounts.group_other_cp = 1 
  /*%end*/
  /*%if "GROUP_ONE".equals(group) */
  AND accounts.group_one = 1 
  /*%end*/
  /*%if "GROUP_TWO".equals(group) */
  AND accounts.group_two = 1 
  /*%end*/
  /*%if "GROUP_THREE".equals(group) */
  AND accounts.group_three = 1 
  /*%end*/
  /*%if "GROUP_FOUR".equals(group) */
  AND accounts.group_four = 1 
  /*%end*/
  /*%if "GROUP_FIVE".equals(group) */
  AND accounts.group_five = 1 
  /*%end*/
  /*%if "A".equals(group) */
  AND accounts.sales_rank = 'A' 
  /*%end*/
  /*%if "B".equals(group) */
  AND accounts.sales_rank = 'B' 
  /*%end*/
  /*%if "C".equals(group) */
  AND accounts.sales_rank = 'C' 
  /*%end*/
GROUP BY
  calls.parent_id