SELECT
        eigyo_sosiki_cd
        ,eigyo_sosiki_nm
        ,eigyo_sosiki_short_nm
        ,corp_cd
        ,div1_cd
        ,div2_cd
        ,div3_cd
        ,div4_cd
        ,div5_cd
        ,div6_cd
        ,div1_nm
        ,div2_nm
        ,div3_nm
        ,div4_nm
        ,div5_nm
        ,div6_nm
        ,deleted
        ,date_entered
        ,date_modified
        ,modified_user_id
        ,created_by
    FROM
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        div1_cd = /* div1 */'a'

	/*%if div2 != null */
        AND div2_cd = /* div2 */'a'
	/*%else*/
        AND (div2_cd IS NULL OR div2_cd = '')
	/*%end*/

	/*%if div3 != null */
        AND div3_cd = /* div3 */'a'
	/*%else*/
        AND (div3_cd IS NULL OR div3_cd = '')
	/*%end*/