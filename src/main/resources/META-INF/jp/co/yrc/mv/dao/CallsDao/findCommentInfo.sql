SELECT
		calls.id
		,calls.name
		,calls.date_entered
		,calls.date_modified
		,calls.modified_user_id
		,calls.created_by
		,calls.deleted
		,calls.assigned_user_id
		,calls.date_start
		,calls.date_end
		,calls.parent_type
		,calls.status
		,calls.parent_id
		,calls.info_div
		,calls.maker
		,calls.kind
		,calls.category
		,calls.sensitivity
		,calls.time_end_c
		,calls.com_free_c
		,calls.division_c
		,calls.is_plan
		,calls.planned_count
		,calls.held_count
		,calls_like_users.id AS calls_like_id
		,calls_like_count.calls_like_count
	FROM
		/*%if tech */tech_service_calls/*%else*/calls/*%end*/ calls
			LEFT OUTER JOIN /*%if tech */tech_service_calls_like_users/*%else*/calls_like_users/*%end*/ calls_like_users
				ON calls.id = calls_like_users.call_id
				AND calls_like_users.user_id = /* userId */'sfa'
			LEFT OUTER JOIN (
				SELECT
						count(calls_like_users.id) AS calls_like_count
						,calls_like_users.call_id
					FROM
						/*%if tech */tech_service_calls/*%else*/calls/*%end*/ calls
						,/*%if tech */tech_service_calls_like_users/*%else*/calls_like_users/*%end*/ calls_like_users
					WHERE
						calls.id = calls_like_users.call_id
					GROUP BY
						call_id
			) calls_like_count
				ON calls.id = calls_like_count.call_id
	WHERE
		calls.id = /* callId */'a'
		AND calls.deleted = 0