SELECT
        COUNT(accounts.id)
    FROM
/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
                                    /*%if !div1.isEmpty() */
                                    AND sales_company_code IN /* div1 */('a', 'aa') /**一つの得意先は販社をまたいだ部署変更はあり得ない*/
                                    /*%end*/
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
/*%else*/
    	accounts
/*%end*/
            INNER JOIN users
            	ON users.id_for_customer = accounts.assigned_user_id
    WHERE
/*%if !div1.isEmpty() */
        accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
    	AND accounts.own_count = 1
    	AND deleted <> 1