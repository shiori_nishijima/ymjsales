SELECT
  * 
FROM
  ( 
    /**日報コメントありの情報をユーザID、年月日ごとに抽出。訪問はありなし両方。*/
    SELECT
      daily_comments.id
      , daily_comments.year
      , daily_comments.month
      , daily_comments.date
      , daily_comments.user_comment
      , daily_comments.boss_comment
      , daily_comments.completed
      , daily_comments.date_entered
      , daily_comments.date_modified
      , daily_comments.modified_user_id
      , daily_comments.created_by
      , daily_comments.deleted
      , users.id AS user_id
      , users.first_name
      , users.last_name
      , mst_eigyo_sosiki.div1_cd AS div1
      , mst_eigyo_sosiki.div2_cd AS div2
      , mst_eigyo_sosiki.div3_cd AS div3
      , CASE 
        WHEN CONCAT( 
          TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, ''))
          , TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))
        ) = '' 
        THEN mst_eigyo_sosiki.eigyo_sosiki_nm 
        ELSE CONCAT( 
          TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, ''))
          , TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))
        ) 
        END AS eigyo_sosiki_nm
      , calls.call_count
	  , calls.phone_count
      , DATE(concat(daily_comments.year, '-', daily_comments.month, '-', daily_comments.date)) AS visit_date
      , newest_read_boss_info.first_name AS boss_first_name
      , newest_read_boss_info.last_name AS boss_last_name
      , newest_read_boss_info.confirm_date
      , CASE 
        WHEN login_user_read_info.daily_comment_id IS NULL 
        THEN 0 
        ELSE 1 
        END AS read_login_user 
    FROM
      users 
      INNER JOIN 
      /*%if isHistory */
      ( 
        SELECT
          * 
        FROM
          mst_tanto_s_sosiki_history 
        WHERE
          year = /* year */2015 
          AND month = /* month */01
      ) mst_tanto_s_sosiki
      /*%else*/
      mst_tanto_s_sosiki
      /*%end*/
        ON users.id = mst_tanto_s_sosiki.tanto_cd 
      INNER JOIN 
      /*%if isHistory */
      ( 
        SELECT
          * 
        FROM
          mst_eigyo_sosiki_history 
        WHERE
          year = /* year */2015 
          AND month = /* month */01
      ) mst_eigyo_sosiki 
      /*%else*/
      mst_eigyo_sosiki
      /*%end*/
        ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
        /*%if !div1.isEmpty() */
        AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
        /*%end*/
        /*%if div2 != null */
        AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'a'
        /*%end*/
      INNER JOIN ( 
        SELECT
          * 
        from
          daily_comments 
        WHERE
          daily_comments.year = /* year */0 
          AND daily_comments.month = /* month */0
          /*%if date > 0 */
          AND daily_comments.date = /* date */0
          /*%end*/
      ) daily_comments 
        ON users.id = daily_comments.user_id 
      /**日報コメントに紐づく最新の上司情報の取得*/
      LEFT OUTER JOIN ( 
        SELECT
          daily_comments_read_users.daily_comment_id
          , daily_comments_read_users.date_modified AS confirm_date
          , users.last_name
          , users.first_name 
        FROM
          ( 
            SELECT
              daily_comments_read_users.daily_comment_id
              , daily_comments_read_users.date_modified
              , MIN(daily_comments_read_users.user_id) AS user_id 
            FROM
              daily_comments_read_users 
              INNER JOIN ( 
                SELECT
                  daily_comment_id
                  , MAX(date_modified) AS date_modified 
                FROM
                  daily_comments_read_users 
                GROUP BY
                  daily_comment_id
              ) max_daily_comment_id 
                ON daily_comments_read_users.daily_comment_id = max_daily_comment_id.daily_comment_id 
                AND daily_comments_read_users.date_modified = max_daily_comment_id.date_modified 
            GROUP BY
              daily_comment_id
              , date_modified
          ) daily_comments_read_users 
          INNER JOIN users 
            ON users.id = daily_comments_read_users.user_id
      ) newest_read_boss_info 
        ON daily_comments.id = newest_read_boss_info.daily_comment_id 
      /**ログインユーザの既読情報の取得*/
      LEFT OUTER JOIN ( 
        SELECT
          daily_comment_id 
        FROM
          daily_comments_read_users 
        WHERE
          user_id = /* loginUserId */'' 
        GROUP BY
          daily_comment_id
      ) login_user_read_info 
        ON daily_comments.id = login_user_read_info.daily_comment_id 
      LEFT OUTER JOIN ( 
        SELECT
          SUM(calls.held_count) AS call_count
		  , SUM(calls.phone_count) AS phone_count
          , DATE(calls.date_start) AS date_start
          , assigned_user_id 
        FROM
          ( 
            SELECT
              calls.assigned_user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
              , CASE calls.division_c 
                WHEN '14' THEN 1 
                ELSE 0 
                END AS phone_count
            FROM
              calls 
            WHERE
              calls.date_start >= /* from */2000 
              AND calls.date_start < /* to */2000 
              AND calls.deleted <> 1 
            UNION ALL 
            SELECT
              calls_users.user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
              , CASE calls.division_c 
                WHEN '14' THEN 1 
                ELSE 0 
                END AS phone_count
            FROM
              calls 
              INNER JOIN calls_users 
                ON calls.id = calls_users.call_id 
            WHERE
              calls.date_start >= /* from */2000 
              AND calls.date_start < /* to */2000 
              AND calls.deleted <> 1 
          ) calls
        GROUP BY
          assigned_user_id
          , DATE(calls.date_start)
      ) calls 
        ON daily_comments.user_id = calls.assigned_user_id 
        AND calls.date_start = DATE( 
          concat( 
            daily_comments.year
            , '-'
            , daily_comments.month
            , '-'
            , daily_comments.date
          )
        ) 
    /**訪問あり日報コメントなしの情報をユーザID、年月日ごとに抽出*/
    UNION ALL 
    SELECT
      daily_comments.id
      , daily_comments.year
      , daily_comments.month
      , daily_comments.date
      , daily_comments.user_comment
      , daily_comments.boss_comment
      , daily_comments.completed
      , daily_comments.date_entered
      , daily_comments.date_modified
      , daily_comments.modified_user_id
      , daily_comments.created_by
      , daily_comments.deleted
      , users.id AS user_id
      , users.first_name
      , users.last_name
      , mst_eigyo_sosiki.div1_cd AS div1
      , mst_eigyo_sosiki.div2_cd AS div2
      , mst_eigyo_sosiki.div3_cd AS div3
      , CASE 
        WHEN CONCAT( 
          TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, ''))
          , TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))
        ) = '' 
        THEN mst_eigyo_sosiki.eigyo_sosiki_nm 
        ELSE CONCAT( 
          TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, ''))
          , TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))
        ) 
        END AS eigyo_sosiki_nm
      , calls.call_count
	  , calls.phone_count
      , calls.date_start AS visit_date
      , '' AS boss_first_name
      , '' AS boss_last_name
      , NULL AS confirm_date
      , 0 AS read_login_user 
    FROM
      users 
      INNER JOIN 
      /*%if isHistory */
      ( 
        SELECT
          * 
        FROM
          mst_tanto_s_sosiki_history 
        WHERE
          year = /* year */2015 
          AND month = /* month */01
      ) mst_tanto_s_sosiki
      /*%else*/
      mst_tanto_s_sosiki
      /*%end*/
        ON users.id = mst_tanto_s_sosiki.tanto_cd 
      INNER JOIN 
      /*%if isHistory */
      ( 
        SELECT
          * 
        FROM
          mst_eigyo_sosiki_history 
        WHERE
          year = /*year*/2015 
          AND month = /*month*/01
      ) mst_eigyo_sosiki 
      /*%else*/
      mst_eigyo_sosiki
      /*%end*/
        ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
        /*%if !div1.isEmpty() */
        AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
        /*%end*/
        /*%if div2 != null */
        AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'a'
        /*%end*/
      INNER JOIN ( 
        SELECT
          SUM(calls.held_count) AS call_count
          , SUM(calls.phone_count) AS phone_count
          , DATE(calls.date_start) AS date_start
          , assigned_user_id 
        FROM
          ( 
            SELECT
              calls.assigned_user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
              , CASE calls.division_c 
                WHEN '14' THEN 1 
                ELSE 0 
                END AS phone_count
            FROM
              calls 
            WHERE
              calls.date_start >= /* from */2000 
              AND calls.date_start < /* to */2000 
              AND calls.deleted <> 1 
            UNION ALL 
            SELECT
              calls_users.user_id AS assigned_user_id
              , CASE calls.status 
                WHEN 'Held' THEN calls.held_count 
                ELSE 0 
                END AS held_count
              , calls.date_start 
              , CASE calls.division_c 
                WHEN '14' THEN 1 
                ELSE 0 
                END AS phone_count
            FROM
              calls 
              INNER JOIN calls_users 
                ON calls.id = calls_users.call_id 
            WHERE
              calls.date_start >= /* from */2000 
              AND calls.date_start < /* to */2000 
              AND calls.deleted <> 1 
          ) calls 
        GROUP BY
          assigned_user_id
          , DATE(calls.date_start)
      ) calls
        ON users.id = calls.assigned_user_id 
      LEFT OUTER JOIN daily_comments 
        ON daily_comments.user_id = calls.assigned_user_id 
        AND calls.date_start = DATE( 
          concat( 
            daily_comments.year
            , '-'
            , daily_comments.month
            , '-'
            , daily_comments.date
          )
        ) 
    WHERE
      daily_comments.id IS NULL
  ) daily_comments
ORDER BY
  visit_date ASC
  , div1 ASC
  , div2 ASC
  , div3 ASC
  , user_id ASC
