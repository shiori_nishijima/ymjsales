SELECT
		COUNT(user_id)
	FROM
(
	SELECT
	        calls.assigned_user_id AS user_id
			,calls.parent_id AS account_id
	    FROM
	        calls
	    WHERE
			calls.date_start >= /*from*/2000
			AND calls.date_start < /*to*/2000
			AND calls.planned_count > 0
			AND calls.status = 'Planned'
		/*%if userId != null*/
			AND calls.assigned_user_id = /* userId */'sfa'
		/*%end*/
            AND calls.parent_type = 'Accounts'
	        AND calls.deleted <> 1
	    GROUP BY
	        assigned_user_id
			,parent_id
	UNION ALL
	SELECT
	        calls_users.user_id AS user_id
            ,calls.parent_id AS account_id
	    FROM
	        calls
        INNER JOIN calls_users
        ON calls.id = calls_users.call_id
        WHERE
	        calls.date_start >= /*from*/2000
	        AND calls.date_start < /*to*/2000
	        AND calls.planned_count > 0
	        AND calls.status = 'Planned' 
	    /*%if userId != null*/
	        AND calls_users.user_id = /* userId */'sfa'
	    /*%end*/
	        AND calls.parent_type = 'Accounts'
	        AND calls.deleted <> 1
	    GROUP BY
	        calls_users.user_id
            ,parent_id
) unvisit_info
                INNER JOIN
        /*%if isHistory */
                    (
                        SELECT
                                *
                            FROM
                                accounts_history
                                    INNER JOIN (
                                        SELECT
                                                id AS current_id
                                                ,MAX(yearmonth) AS current_yearmonth
                                            FROM
                                                accounts_history
                                            WHERE
                                                yearmonth <= /* yearMonth */2000
                                                /*%if !div1.isEmpty() */
                                                AND sales_company_code IN /* div1 */('a', 'aa')
                                                /*%end*/
                                            GROUP BY current_id
                                    ) current
                                        ON current.current_id = accounts_history.id
                                        AND current.current_yearmonth = accounts_history.yearmonth
                    ) accounts
        /*%else*/
                accounts
        /*%end*/
                ON unvisit_info.account_id = accounts.id
                INNER JOIN
					/*%if isHistory */
					(
					  SELECT
					      *
					    FROM
					      mst_tanto_s_sosiki_history
					    WHERE
					        year = /*year*/2015
					        AND month = /*month*/01
					) mst_tanto_s_sosiki
					/*%else*/
					mst_tanto_s_sosiki
					/*%end*/
                ON unvisit_info.user_id = mst_tanto_s_sosiki.tanto_cd
                INNER JOIN
					/*%if isHistory */
					(
					  SELECT
					      *
					    FROM
					      mst_eigyo_sosiki_history
					    WHERE
					       year = /*year*/2015
					       AND month = /*month*/01
					) mst_eigyo_sosiki
					/*%else*/
					mst_eigyo_sosiki
					/*%end*/
		        /** 営業所所属のみ表示 */
		        ON mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
		        AND mst_eigyo_sosiki.div1_cd = accounts.sales_company_code
		        AND mst_eigyo_sosiki.div2_cd = accounts.department_code
		        AND mst_eigyo_sosiki.div3_cd = accounts.sales_office_code
    WHERE
    /*%if !div1.isEmpty() */
        accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%end*/
    /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
    /*%end*/
    /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
    /*%end*/
