SELECT
        accounts.id
        ,accounts.name
        ,accounts.name_kana
        ,accounts.assigned_user_id
        ,accounts.sales_company_code
        ,accounts.department_code
        ,accounts.sales_office_code
        ,accounts.market_id
        ,markets.name AS market_name
        ,accounts.own_count
        ,accounts.deal_count
        ,accounts.group_important
        ,accounts.group_new
        ,accounts.group_deep_plowing
        ,accounts.group_y_cp
        ,accounts.group_other_cp
        ,accounts.group_one
        ,accounts.group_two
        ,accounts.group_three
        ,accounts.group_four
        ,accounts.group_five
        ,accounts.sales_rank
        ,accounts.demand_number
        ,accounts.iss_yh
        ,accounts.iss_bs
        ,accounts.iss_dl
        ,accounts.iss_ty
        ,accounts.iss_gy
        ,accounts.iss_mi
        ,accounts.iss_pb
        ,accounts.iss_other
        ,users.first_name
        ,users.last_name
        
        ,kind_results.pcr_summer_sales
        ,kind_results.pcr_summer_margin
        ,kind_results.pcr_summer_hq_margin
        ,kind_results.pcr_summer_number
        
        ,kind_results.pcr_pure_snow_sales
        ,kind_results.pcr_pure_snow_margin
        ,kind_results.pcr_pure_snow_hq_margin
        ,kind_results.pcr_pure_snow_number
        
        ,kind_results.van_summer_sales
        ,kind_results.van_summer_margin
        ,kind_results.van_summer_hq_margin
        ,kind_results.van_summer_number
        
        ,kind_results.van_pure_snow_sales
        ,kind_results.van_pure_snow_margin
        ,kind_results.van_pure_snow_hq_margin
        ,kind_results.van_pure_snow_number
        
        ,kind_results.lt_summer_sales
        ,kind_results.lt_summer_margin
        ,kind_results.lt_summer_hq_margin
        ,kind_results.lt_summer_number
        
        ,kind_results.lt_snow_sales
        ,kind_results.lt_snow_margin
        ,kind_results.lt_snow_hq_margin
        ,kind_results.lt_snow_number
        
        ,kind_results.lt_as_sales
        ,kind_results.lt_as_margin
        ,kind_results.lt_as_hq_margin
        ,kind_results.lt_as_number
        
        ,kind_results.lt_pure_snow_sales
        ,kind_results.lt_pure_snow_margin
        ,kind_results.lt_pure_snow_hq_margin
        ,kind_results.lt_pure_snow_number
        
        ,kind_results.tb_summer_sales
        ,kind_results.tb_summer_margin
        ,kind_results.tb_summer_hq_margin
        ,kind_results.tb_summer_number
        
        ,kind_results.tb_snow_sales
        ,kind_results.tb_snow_margin
        ,kind_results.tb_snow_hq_margin
        ,kind_results.tb_snow_number
        
        ,kind_results.tb_as_sales
        ,kind_results.tb_as_margin
        ,kind_results.tb_as_hq_margin
        ,kind_results.tb_as_number
        
        ,kind_results.tb_pure_snow_sales
        ,kind_results.tb_pure_snow_margin
        ,kind_results.tb_pure_snow_hq_margin
        ,kind_results.tb_pure_snow_number
        
        ,kind_results.id_sales
        ,kind_results.id_margin
        ,kind_results.id_hq_margin
        ,kind_results.id_number
        
        ,kind_results.or_sales
        ,kind_results.or_margin
        ,kind_results.or_hq_margin
        ,kind_results.or_number
        
        ,kind_results.tifu_sales
        ,kind_results.tifu_margin
        ,kind_results.tifu_hq_margin
        ,kind_results.tifu_number
        
        ,kind_results.other_sales
        ,kind_results.other_margin
        ,kind_results.other_hq_margin
        
        ,kind_results.work_sales
        ,kind_results.work_margin
        ,kind_results.work_hq_margin
        ,kind_results.work_number
        
        ,kind_results.pcb_summer_sales
        ,kind_results.pcb_summer_margin
        ,kind_results.pcb_summer_hq_margin
        ,kind_results.pcb_summer_number
        
        ,kind_results.pcb_pure_snow_sales
        ,kind_results.pcb_pure_snow_margin
        ,kind_results.pcb_pure_snow_hq_margin
        ,kind_results.pcb_pure_snow_number

        ,kind_results.ltr_summer_sales
        ,kind_results.ltr_summer_margin
        ,kind_results.ltr_summer_hq_margin
        ,kind_results.ltr_summer_number

        ,kind_results.ltr_snow_sales
        ,kind_results.ltr_snow_margin
        ,kind_results.ltr_snow_hq_margin
        ,kind_results.ltr_snow_number
        
        ,kind_results.retread_sales
        ,kind_results.retread_margin
        ,kind_results.retread_hq_margin
        ,kind_results.retread_number
        
    FROM
		(
		SELECT
				results.year
				,results.month
				,results.account_id
		        ,SUM(CASE WHEN  results.middle_class = '11' THEN results.sales ELSE 0 END) AS pcr_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '11' THEN results.margin ELSE 0 END) AS pcr_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '11' THEN results.hq_margin ELSE 0 END) AS pcr_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '11' THEN results.number ELSE 0 END) AS pcr_summer_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '51' THEN results.sales ELSE 0 END) AS pcr_pure_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '51' THEN results.margin ELSE 0 END) AS pcr_pure_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '51' THEN results.hq_margin ELSE 0 END) AS pcr_pure_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '51' THEN results.number ELSE 0 END) AS pcr_pure_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '12' THEN results.sales ELSE 0 END) AS van_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '12' THEN results.margin ELSE 0 END) AS van_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '12' THEN results.hq_margin ELSE 0 END) AS van_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '12' THEN results.number ELSE 0 END) AS van_summer_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '61' THEN results.sales ELSE 0 END) AS van_pure_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '61' THEN results.margin ELSE 0 END) AS van_pure_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '61' THEN results.hq_margin ELSE 0 END) AS van_pure_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '61' THEN results.number ELSE 0 END) AS van_pure_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '21' THEN results.sales ELSE 0 END) AS lt_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '21' THEN results.margin ELSE 0 END) AS lt_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '21' THEN results.hq_margin ELSE 0 END) AS lt_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '21' THEN results.number ELSE 0 END) AS lt_summer_number

		        ,SUM(CASE WHEN  results.middle_class = '21' AND results.small_class = '4F' THEN results.sales ELSE 0 END) AS ltr_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '21' AND results.small_class = '4F' THEN results.margin ELSE 0 END) AS ltr_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '21' AND results.small_class = '4F' THEN results.hq_margin ELSE 0 END) AS ltr_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '21' AND results.small_class = '4F' THEN results.number ELSE 0 END) AS ltr_summer_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '71' THEN results.sales ELSE 0 END) AS lt_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '71' THEN results.margin ELSE 0 END) AS lt_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' THEN results.hq_margin ELSE 0 END) AS lt_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' THEN results.number ELSE 0 END) AS lt_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div = 'Z' THEN results.sales ELSE 0 END) AS lt_as_sales
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div = 'Z' THEN results.margin ELSE 0 END) AS lt_as_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div = 'Z' THEN results.hq_margin ELSE 0 END) AS lt_as_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div = 'Z' THEN results.number ELSE 0 END) AS lt_as_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div <> 'Z' THEN results.sales ELSE 0 END) AS lt_pure_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div <> 'Z' THEN results.margin ELSE 0 END) AS lt_pure_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div <> 'Z' THEN results.hq_margin ELSE 0 END) AS lt_pure_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.snow_div <> 'Z' THEN results.number ELSE 0 END) AS lt_pure_snow_number

		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.small_class = 'BF' THEN results.sales ELSE 0 END) AS ltr_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.small_class = 'BF' THEN results.margin ELSE 0 END) AS ltr_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.small_class = 'BF' THEN results.hq_margin ELSE 0 END) AS ltr_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '71' AND results.small_class = 'BF' THEN results.number ELSE 0 END) AS ltr_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '22'  THEN results.sales ELSE 0 END) AS tb_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '22'  THEN results.margin ELSE 0 END) AS tb_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '22'  THEN results.hq_margin ELSE 0 END) AS tb_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '22'  THEN results.number ELSE 0 END) AS tb_summer_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '72'  THEN results.sales ELSE 0 END) AS tb_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '72'  THEN results.margin ELSE 0 END) AS tb_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '72'  THEN results.hq_margin ELSE 0 END) AS tb_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '72'  THEN results.number ELSE 0 END) AS tb_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div = 'Z' THEN results.sales ELSE 0 END) AS tb_as_sales
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div = 'Z' THEN results.margin ELSE 0 END) AS tb_as_margin
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div = 'Z' THEN results.hq_margin ELSE 0 END) AS tb_as_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div = 'Z' THEN results.number ELSE 0 END) AS tb_as_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div <> 'Z' THEN results.sales ELSE 0 END) AS tb_pure_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div <> 'Z' THEN results.margin ELSE 0 END) AS tb_pure_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div <> 'Z' THEN results.hq_margin ELSE 0 END) AS tb_pure_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '72' AND results.snow_div <> 'Z' THEN results.number ELSE 0 END) AS tb_pure_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '55' THEN results.sales ELSE 0 END) AS id_sales
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '55' THEN results.margin ELSE 0 END) AS id_margin
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '55' THEN results.hq_margin ELSE 0 END) AS id_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '55' THEN results.number ELSE 0 END) AS id_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '50' THEN results.sales ELSE 0 END) AS or_sales
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '50' THEN results.margin ELSE 0 END) AS or_margin
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '50' THEN results.hq_margin ELSE 0 END) AS or_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '41' AND results.small_class = '50' THEN results.number ELSE 0 END) AS or_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '91'  THEN results.sales ELSE 0 END) AS tifu_sales
		        ,SUM(CASE WHEN  results.middle_class = '91'  THEN results.margin ELSE 0 END) AS tifu_margin
		        ,SUM(CASE WHEN  results.middle_class = '91'  THEN results.hq_margin ELSE 0 END) AS tifu_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '91'  THEN results.number ELSE 0 END) AS tifu_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '92' OR results.middle_class = '93' OR results.middle_class = '94' OR results.middle_class = '95' THEN results.sales ELSE 0 END) AS other_sales
		        ,SUM(CASE WHEN  results.middle_class = '92' OR results.middle_class = '93' OR results.middle_class = '94' OR results.middle_class = '95' THEN results.margin ELSE 0 END) AS other_margin
		        ,SUM(CASE WHEN  results.middle_class = '92' OR results.middle_class = '93' OR results.middle_class = '94' OR results.middle_class = '95' THEN results.hq_margin ELSE 0 END) AS other_hq_margin
		        
		        ,SUM(CASE WHEN  results.middle_class = '96' OR results.middle_class = '97' THEN results.sales ELSE 0 END) AS work_sales
		        ,SUM(CASE WHEN  results.middle_class = '96' OR results.middle_class = '97' THEN results.margin ELSE 0 END) AS work_margin
		        ,SUM(CASE WHEN  results.middle_class = '96' OR results.middle_class = '97' THEN results.hq_margin ELSE 0 END) AS work_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '96' OR results.middle_class = '97' THEN results.number ELSE 0 END) AS work_number
       		
		        ,SUM(CASE WHEN  results.middle_class = '13'  THEN results.sales ELSE 0 END) AS pcb_summer_sales
		        ,SUM(CASE WHEN  results.middle_class = '13'  THEN results.margin ELSE 0 END) AS pcb_summer_margin
		        ,SUM(CASE WHEN  results.middle_class = '13'  THEN results.hq_margin ELSE 0 END) AS pcb_summer_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '13'  THEN results.number ELSE 0 END) AS pcb_summer_number
		        
		        ,SUM(CASE WHEN  results.middle_class = '62'  THEN results.sales ELSE 0 END) AS pcb_pure_snow_sales
		        ,SUM(CASE WHEN  results.middle_class = '62'  THEN results.margin ELSE 0 END) AS pcb_pure_snow_margin
		        ,SUM(CASE WHEN  results.middle_class = '62'  THEN results.hq_margin ELSE 0 END) AS pcb_pure_snow_hq_margin
		        ,SUM(CASE WHEN  results.middle_class = '62'  THEN results.number ELSE 0 END) AS pcb_pure_snow_number
		        
		        ,SUM(CASE WHEN  results.middle_class = 'A1' 
		        		OR results.middle_class = 'A2' 
		        		OR results.middle_class = 'A3'
		        		OR results.middle_class = 'B1' 
		        		OR results.middle_class = 'B2' 
		        		THEN results.sales ELSE 0 END) AS retread_sales
		        ,SUM(CASE WHEN  
		        		results.middle_class = 'A1' 
		        		OR results.middle_class = 'A2' 
		        		OR results.middle_class = 'A3'
		        		OR results.middle_class = 'B1' 
		        		OR results.middle_class = 'B2' 
				        THEN results.margin ELSE 0 END) AS retread_margin
		        ,SUM(CASE WHEN  
		        		results.middle_class = 'A1' 
		        		OR results.middle_class = 'A2' 
		        		OR results.middle_class = 'A3'
		        		OR results.middle_class = 'B1' 
		        		OR results.middle_class = 'B2'
		        		THEN results.hq_margin ELSE 0 END) AS retread_hq_margin
		        ,SUM(CASE WHEN  
		        		results.middle_class = 'A1' 
		        		OR results.middle_class = 'A2' 
		        		OR results.middle_class = 'A3'
		        		OR results.middle_class = 'B1' 
		        		OR results.middle_class = 'B2'   
		        		THEN results.number ELSE 0 END) AS retread_number
			FROM
				(
				SELECT
						results.year
						,results.month
						,accounts.id as account_id
						,sum(results.sales) as sales
						,sum(results.margin) as margin
						,sum(results.hq_margin) as hq_margin
						,sum(results.number) as number
						,compass_kinds.middle_class
						,compass_kinds.small_class
						,compass_kinds.snow_div
					FROM
						results
				    	,
				/*%if isHistory */
				        (
				            SELECT
									*
				                FROM
				                    accounts_history
				                        INNER JOIN (
				                        	SELECT
				                        			id AS current_id
				                        			,MAX(yearmonth) AS current_yearmonth
				                        		FROM
				                        			accounts_history
				                        		WHERE
				                        			yearmonth <= /* yearMonth */2000
                                                    /*%if !div1.isEmpty() */
                                                    AND sales_company_code IN /* div1 */('a', 'aa')
                                                    /*%end*/
						                        GROUP BY current_id
				                        ) current
				                            ON current.current_id = accounts_history.id
				                            AND current.current_yearmonth = accounts_history.yearmonth
				        ) accounts
				/*%else*/
				    	accounts
				/*%end*/
				    	,users
						,compass_kinds
					WHERE
						results.year = /* year */2000
						AND results.month = /* month */1
					/*%if date != null */
						AND results.date = /* date */1
					/*%end*/
						AND results.account_id = accounts.id
				        AND users.id_for_customer = accounts.assigned_user_id
				/*%if !div1.isEmpty() */
						AND accounts.sales_company_code IN /* div1 */('a', 'aa')
					/*%if div1.size() == 1 */
						/*%if div2 != null */
						AND accounts.department_code = /* div2 */'a'
						/*%end*/
						/*%if div3 != null */
						AND accounts.sales_office_code = /* div3 */'a'
						/*%end*/
						/*%if userId != null */
						AND users.id = /* userId */'sfa'
						/*%end*/
					/*%end*/
				/*%end*/
				/*%if div1.isEmpty() */
				        /*%if userId != null */
				        AND users.id = /* userId */'sfa'
				        /*%end*/
				/*%end*/
			            /*%if group == "GROUP_IMPORTANT"*/
			            AND accounts.group_important = 1
			            /*%end*/
			            /*%if group == "GROUP_NEW"*/
			            AND accounts.group_new = 1
			            /*%end*/
			            /*%if group == "GROUP_DEEP_PLOWING"*/
			            AND accounts.group_deep_plowing = 1
			            /*%end*/
			            /*%if group == "GROUP_Y_CP"*/
			            AND accounts.group_y_cp = 1
			            /*%end*/
			            /*%if group == "GROUP_OTHER_CP"*/
			            AND accounts.group_other_cp = 1
			            /*%end*/
			            /*%if group == "GROUP_ONE"*/
			            AND accounts.group_one = 1
			            /*%end*/
			            /*%if group == "GROUP_TWO"*/
			            AND accounts.group_two = 1
			            /*%end*/
			            /*%if group == "GROUP_THREE"*/
			            AND accounts.group_three = 1
			            /*%end*/
			            /*%if group == "GROUP_FOUR"*/
			            AND accounts.group_four = 1
			            /*%end*/
			            /*%if group == "GROUP_FIVE"*/
			            AND accounts.group_five = 1
			            /*%end*/
			            /*%if group == "A"*/
			            AND accounts.sales_rank = 'A'
			            /*%end*/
			            /*%if group == "B"*/
			            AND accounts.sales_rank = 'B'
			            /*%end*/
			            /*%if group == "C"*/
			            AND accounts.sales_rank = 'C'
			            /*%end*/

			            AND compass_kinds.id = results.compass_kind
			    GROUP BY
					results.year
					,results.month
					,accounts.id
					,compass_kinds.middle_class
					,compass_kinds.small_class
					,compass_kinds.snow_div
				) results
		    GROUP BY
				results.year
				,results.month
				,results.account_id
		) kind_results,
		/*%if isHistory */
		        (
		            SELECT
							*
		                FROM
		                    accounts_history
		                        INNER JOIN (
		                        	SELECT
		                        			id AS current_id
		                        			,MAX(yearmonth) AS current_yearmonth
		                        		FROM
		                        			accounts_history
		                        		WHERE
		                        			yearmonth <= /* yearMonth */2000
                                            /*%if !div1.isEmpty() */
                                            AND sales_company_code IN /* div1 */('a', 'aa')
                                            /*%end*/
				                        GROUP BY current_id
		                        ) current
		                            ON current.current_id = accounts_history.id
		                            AND current.current_yearmonth = accounts_history.yearmonth
		        ) accounts
		/*%else*/
		    	accounts
		/*%end*/
			INNER JOIN 	markets
				ON markets.id = accounts.market_id
			INNER JOIN users
				ON users.id_for_customer = accounts.assigned_user_id
	WHERE
			kind_results.account_id = accounts.id
	ORDER BY
			accounts.id