SELECT
  opportunities.id AS parentId
  , opportunities.name AS opportunityName
  , opportunities.op_step_c AS step
  , opportunities.op_order_poss_c AS rank
  , opportunities.demand_amount AS demand
  , opportunities.target_amount AS target
  , accounts.name AS customer
  , concat(users.last_name, ' ', users.first_name) AS name
FROM
  opportunities
  LEFT OUTER JOIN accounts
    ON opportunities.account_id = accounts.id
  LEFT OUTER JOIN users
    ON opportunities.assigned_user_id = users.id
WHERE
  opportunities.deleted <> 1
  AND ( DATE_FORMAT(opportunities.date_modified, '%Y%m') = /* o.yearMonth */'201701' )
  /*%if o.div1 != null*/
  AND accounts.sales_company_code = /* o.div1 */'0001110103'
  /*%end*/
  /*%if o.div2 != null */
  AND accounts.department_code = /* o.div2 */'J310'
  /*%end*/
  /*%if o.div3 != null */
  AND accounts.sales_office_code = /* o.div3 */'1100'
  /*%end*/
  /*%if o.tanto != null*/
  AND opportunities.assigned_user_id = /* o.tanto */'ytj01'
  /*%end*/
ORDER BY
  accounts.name
