SELECT
		results.year
		,results.month
        ,SUM(CASE WHEN  compass_kinds.middle_class = '11'  THEN results.sales ELSE 0 END) AS pcr_summer_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '11'  THEN results.margin ELSE 0 END) AS pcr_summer_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '11'  THEN results.hq_margin ELSE 0 END) AS pcr_summer_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '11'  THEN results.number ELSE 0 END) AS pcr_summer_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '51'  THEN results.sales ELSE 0 END) AS pcr_pure_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '51'  THEN results.margin ELSE 0 END) AS pcr_pure_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '51'  THEN results.hq_margin ELSE 0 END) AS pcr_pure_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '51'  THEN results.number ELSE 0 END) AS pcr_pure_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '12'  THEN results.sales ELSE 0 END) AS van_summer_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '12'  THEN results.margin ELSE 0 END) AS van_summer_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '12'  THEN results.hq_margin ELSE 0 END) AS van_summer_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '12'  THEN results.number ELSE 0 END) AS van_summer_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '61'  THEN results.sales ELSE 0 END) AS van_pure_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '61'  THEN results.margin ELSE 0 END) AS van_pure_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '61'  THEN results.hq_margin ELSE 0 END) AS van_pure_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '61'  THEN results.number ELSE 0 END) AS van_pure_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '21'  THEN results.sales ELSE 0 END) AS lt_summer_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '21'  THEN results.margin ELSE 0 END) AS lt_summer_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '21'  THEN results.hq_margin ELSE 0 END) AS lt_summer_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '21'  THEN results.number ELSE 0 END) AS lt_summer_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71'  THEN results.sales ELSE 0 END) AS lt_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71'  THEN results.margin ELSE 0 END) AS lt_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71'  THEN results.hq_margin ELSE 0 END) AS lt_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71'  THEN results.number ELSE 0 END) AS lt_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div = 'Z' THEN results.sales ELSE 0 END) AS lt_as_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div = 'Z' THEN results.margin ELSE 0 END) AS lt_as_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div = 'Z' THEN results.hq_margin ELSE 0 END) AS lt_as_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div = 'Z' THEN results.number ELSE 0 END) AS lt_as_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div <> 'Z' THEN results.sales ELSE 0 END) AS lt_pure_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div <> 'Z' THEN results.margin ELSE 0 END) AS lt_pure_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div <> 'Z' THEN results.hq_margin ELSE 0 END) AS lt_pure_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '71' AND compass_kinds.snow_div <> 'Z' THEN results.number ELSE 0 END) AS lt_pure_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '22'  THEN results.sales ELSE 0 END) AS tb_summer_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '22'  THEN results.margin ELSE 0 END) AS tb_summer_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '22'  THEN results.hq_margin ELSE 0 END) AS tb_summer_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '22'  THEN results.number ELSE 0 END) AS tb_summer_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72'  THEN results.sales ELSE 0 END) AS tb_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72'  THEN results.margin ELSE 0 END) AS tb_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72'  THEN results.hq_margin ELSE 0 END) AS tb_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72'  THEN results.number ELSE 0 END) AS tb_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div = 'Z' THEN results.sales ELSE 0 END) AS tb_as_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div = 'Z' THEN results.margin ELSE 0 END) AS tb_as_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div = 'Z' THEN results.hq_margin ELSE 0 END) AS tb_as_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div = 'Z' THEN results.number ELSE 0 END) AS tb_as_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div <> 'Z' THEN results.sales ELSE 0 END) AS tb_pure_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div <> 'Z' THEN results.margin ELSE 0 END) AS tb_pure_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div <> 'Z' THEN results.hq_margin ELSE 0 END) AS tb_pure_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '72' AND compass_kinds.snow_div <> 'Z' THEN results.number ELSE 0 END) AS tb_pure_snow_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '55' THEN results.sales ELSE 0 END) AS id_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '55' THEN results.margin ELSE 0 END) AS id_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '55' THEN results.hq_margin ELSE 0 END) AS id_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '55' THEN results.number ELSE 0 END) AS id_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '50' THEN results.sales ELSE 0 END) AS or_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '50' THEN results.margin ELSE 0 END) AS or_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '50' THEN results.hq_margin ELSE 0 END) AS or_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '41' AND compass_kinds.small_class = '50' THEN results.number ELSE 0 END) AS or_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '91'  THEN results.sales ELSE 0 END) AS tifu_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '91'  THEN results.margin ELSE 0 END) AS tifu_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '91'  THEN results.hq_margin ELSE 0 END) AS tifu_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '91'  THEN results.number ELSE 0 END) AS tifu_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '92' OR compass_kinds.middle_class = '93' OR compass_kinds.middle_class = '94' OR compass_kinds.middle_class = '95' THEN results.sales ELSE 0 END) AS other_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '92' OR compass_kinds.middle_class = '93' OR compass_kinds.middle_class = '94' OR compass_kinds.middle_class = '95' THEN results.margin ELSE 0 END) AS other_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '92' OR compass_kinds.middle_class = '93' OR compass_kinds.middle_class = '94' OR compass_kinds.middle_class = '95' THEN results.hq_margin ELSE 0 END) AS other_hq_margin
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '96' OR compass_kinds.middle_class = '97' THEN results.sales ELSE 0 END) AS work_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '96' OR compass_kinds.middle_class = '97' THEN results.margin ELSE 0 END) AS work_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '96' OR compass_kinds.middle_class = '97' THEN results.hq_margin ELSE 0 END) AS work_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '96' OR compass_kinds.middle_class = '97' THEN results.number ELSE 0 END) AS work_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '13'  THEN results.sales ELSE 0 END) AS pcb_summer_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '13'  THEN results.margin ELSE 0 END) AS pcb_summer_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '13'  THEN results.hq_margin ELSE 0 END) AS pcb_summer_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '13'  THEN results.number ELSE 0 END) AS pcb_summer_number
        
        ,SUM(CASE WHEN  compass_kinds.middle_class = '62'  THEN results.sales ELSE 0 END) AS pcb_pure_snow_sales
        ,SUM(CASE WHEN  compass_kinds.middle_class = '62'  THEN results.margin ELSE 0 END) AS pcb_pure_snow_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '62'  THEN results.hq_margin ELSE 0 END) AS pcb_pure_snow_hq_margin
        ,SUM(CASE WHEN  compass_kinds.middle_class = '62'  THEN results.number ELSE 0 END) AS pcb_pure_snow_number

        ,SUM(CASE WHEN  
        		compass_kinds.middle_class = 'A1' 
        		OR compass_kinds.middle_class = 'A2' 
        		OR compass_kinds.middle_class = 'A3'
        		OR compass_kinds.middle_class = 'B1' 
        		OR compass_kinds.middle_class = 'B2' 
        		THEN results.sales ELSE 0 END) AS retread_sales
        ,SUM(CASE WHEN  
        		compass_kinds.middle_class = 'A1' 
        		OR compass_kinds.middle_class = 'A2' 
        		OR compass_kinds.middle_class = 'A3'
        		OR compass_kinds.middle_class = 'B1' 
        		OR compass_kinds.middle_class = 'B2' 
		        THEN results.margin ELSE 0 END) AS retread_margin
        ,SUM(CASE WHEN  
        		compass_kinds.middle_class = 'A1' 
        		OR compass_kinds.middle_class = 'A2' 
        		OR compass_kinds.middle_class = 'A3'
        		OR compass_kinds.middle_class = 'B1' 
        		OR compass_kinds.middle_class = 'B2'
        		THEN results.hq_margin ELSE 0 END) AS retread_hq_margin
        ,SUM(CASE WHEN  
        		compass_kinds.middle_class = 'A1' 
        		OR compass_kinds.middle_class = 'A2' 
        		OR compass_kinds.middle_class = 'A3'
        		OR compass_kinds.middle_class = 'B1' 
        		OR compass_kinds.middle_class = 'B2'   
        		THEN results.number ELSE 0 END) AS retread_number
        		
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A1' THEN results.sales ELSE 0 END) AS retread_lt_summer_sales
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A1' THEN results.margin ELSE 0 END) AS retread_lt_summer_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A1' THEN results.hq_margin ELSE 0 END) AS retread_lt_summer_hq_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A1' THEN results.number ELSE 0 END) AS retread_lt_summer_number
        		
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A2' THEN results.sales ELSE 0 END) AS retread_tb_summer_sales
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A2' THEN results.margin ELSE 0 END) AS retread_tb_summer_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A2' THEN results.hq_margin ELSE 0 END) AS retread_tb_summer_hq_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A2' THEN results.number ELSE 0 END) AS retread_tb_summer_number
        		
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A3' THEN results.sales ELSE 0 END) AS retread_special_tire_summer_sales
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A3' THEN results.margin ELSE 0 END) AS retread_special_tire_summer_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A3' THEN results.hq_margin ELSE 0 END) AS retread_special_tire_summer_hq_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'A3' THEN results.number ELSE 0 END) AS retread_special_tire_summer_number
        		
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B1' THEN results.sales ELSE 0 END) AS retread_lt_snow_sales
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B1' THEN results.margin ELSE 0 END) AS retread_lt_snow_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B1' THEN results.hq_margin ELSE 0 END) AS retread_lt_snow_hq_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B1' THEN results.number ELSE 0 END) AS retread_lt_snow_number
        		
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B2' THEN results.sales ELSE 0 END) AS retread_tb_snow_sales
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B2' THEN results.margin ELSE 0 END) AS retread_tb_snow_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B2' THEN results.hq_margin ELSE 0 END) AS retread_tb_snow_hq_margin
        ,SUM(CASE WHEN compass_kinds.middle_class = 'B2' THEN results.number ELSE 0 END) AS retread_tb_snow_number
    FROM
		(
		SELECT
				results.year
				,results.month
				, sum(results.sales) as sales
				,sum(results.margin) as margin
				,sum(results.hq_margin) as hq_margin
				,sum(results.number) as number
				,results.compass_kind
			FROM
				results
		    	,
		/*%if isHistory */
		        (
		            SELECT
							*
		                FROM
		                    accounts_history
		                        INNER JOIN (
		                        	SELECT
		                        			id AS current_id
		                        			,MAX(yearmonth) AS current_yearmonth
		                        		FROM
		                        			accounts_history
		                        		WHERE
		                        			yearmonth <= /* yearMonth */2000
                                            /*%if !div1.isEmpty() */
                                            AND sales_company_code IN /* div1 */('a', 'aa')
                                            /*%end*/
				                        GROUP BY current_id
		                        ) current
		                            ON current.current_id = accounts_history.id
		                            AND current.current_yearmonth = accounts_history.yearmonth
		        ) accounts
		/*%else*/
		    	accounts
		/*%end*/
		    	,users
			WHERE
				results.year = /* year */2000
				AND results.month = /* month */1
			/*%if date != null */
				AND results.date = /* date */1
			/*%end*/
				AND results.account_id = accounts.id
		        AND users.id_for_customer = accounts.assigned_user_id
		/*%if !div1.isEmpty() */
				AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			/*%if div1.size() == 1 */
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				/*%if userId != null */
				AND users.id = /* userId */'sfa'
				/*%end*/
			/*%end*/
		/*%end*/
		/*%if div1.isEmpty() */
		        /*%if userId != null */
		        AND users.id = /* userId */'sfa'
		        /*%end*/
		/*%end*/
	    GROUP BY
			results.year
			,results.month
			,results.compass_kind
		) results
		,compass_kinds
	WHERE
		compass_kinds.id = results.compass_kind
    GROUP BY
		results.year
		,results.month

		
		