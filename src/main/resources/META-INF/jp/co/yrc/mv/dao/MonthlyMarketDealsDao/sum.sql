SELECT
        monthly_market_deals.year
        ,monthly_market_deals.month
		,SUM(monthly_market_deals.ss_own_count) AS ss_own_count
		,SUM(monthly_market_deals.ss_deal_count) AS ss_deal_count
		,SUM(monthly_market_deals.ss_account_master_count) AS ss_account_master_count
		,SUM(monthly_market_deals.rs_own_count) AS rs_own_count
		,SUM(monthly_market_deals.rs_deal_count) AS rs_deal_count
		,SUM(monthly_market_deals.rs_account_master_count) AS rs_account_master_count
		,SUM(monthly_market_deals.cd_own_count) AS cd_own_count
		,SUM(monthly_market_deals.cd_deal_count) AS cd_deal_count
		,SUM(monthly_market_deals.cd_account_master_count) AS cd_account_master_count
		,SUM(monthly_market_deals.ps_own_count) AS ps_own_count
		,SUM(monthly_market_deals.ps_deal_count) AS ps_deal_count
		,SUM(monthly_market_deals.ps_account_master_count) AS ps_account_master_count
		,SUM(monthly_market_deals.cs_own_count) AS cs_own_count
		,SUM(monthly_market_deals.cs_deal_count) AS cs_deal_count
		,SUM(monthly_market_deals.cs_account_master_count) AS cs_account_master_count
		,SUM(monthly_market_deals.hc_own_count) AS hc_own_count
		,SUM(monthly_market_deals.hc_deal_count) AS hc_deal_count
		,SUM(monthly_market_deals.hc_account_master_count) AS hc_account_master_count
		,SUM(monthly_market_deals.lease_own_count) AS lease_own_count
		,SUM(monthly_market_deals.lease_deal_count) AS lease_deal_count
		,SUM(monthly_market_deals.lease_account_master_count) AS lease_account_master_count
		,SUM(monthly_market_deals.indirect_other_own_count) AS indirect_other_own_count
		,SUM(monthly_market_deals.indirect_other_deal_count) AS indirect_other_deal_count
		,SUM(monthly_market_deals.indirect_other_account_master_count) AS indirect_other_account_master_count
		,SUM(monthly_market_deals.truck_own_count) AS truck_own_count
		,SUM(monthly_market_deals.truck_deal_count) AS truck_deal_count
		,SUM(monthly_market_deals.truck_account_master_count) AS truck_account_master_count
		,SUM(monthly_market_deals.bus_own_count) AS bus_own_count
		,SUM(monthly_market_deals.bus_deal_count) AS bus_deal_count
		,SUM(monthly_market_deals.bus_account_master_count) AS bus_account_master_count
		,SUM(monthly_market_deals.hire_taxi_own_count) AS hire_taxi_own_count
		,SUM(monthly_market_deals.hire_taxi_deal_count) AS hire_taxi_deal_count
		,SUM(monthly_market_deals.hire_taxi_account_master_count) AS hire_taxi_account_master_count
		,SUM(monthly_market_deals.direct_other_own_count) AS direct_other_own_count
		,SUM(monthly_market_deals.direct_other_deal_count) AS direct_other_deal_count
		,SUM(monthly_market_deals.direct_other_account_master_count) AS direct_other_account_master_count
		,SUM(monthly_market_deals.retail_own_count) AS retail_own_count
		,SUM(monthly_market_deals.retail_deal_count) AS retail_deal_count
		,SUM(monthly_market_deals.retail_account_master_count) AS retail_account_master_count
		,SUM(monthly_market_deals.plan_count) AS plan_count
    FROM
        monthly_market_deals
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_market_deals.year = /* year */1
        AND monthly_market_deals.month = /* month */1
        AND monthly_market_deals.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_market_deals.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_market_deals.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_market_deals.year
		,monthly_market_deals.month
