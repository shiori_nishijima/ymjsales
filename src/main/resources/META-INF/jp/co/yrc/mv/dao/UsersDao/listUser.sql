SELECT
  users.id,
  users.first_name,
  users.last_name,
  mst_eigyo_sosiki.eigyo_sosiki_nm,
  mst_eigyo_sosiki.div1_cd AS div1,
  mst_eigyo_sosiki.div2_cd AS div2,
  mst_eigyo_sosiki.div3_cd AS div3
FROM
  users
  ,
	/*%if isHistory */
	(
	  SELECT
	      *
	    FROM
	      mst_tanto_s_sosiki_history
	    WHERE
	        year = /*year*/2015
	        AND month = /*month*/01
	) mst_tanto_s_sosiki
	/*%else*/
	mst_tanto_s_sosiki
	/*%end*/
  ,
	/*%if isHistory */
	(
	  SELECT
	      *
	    FROM
	      mst_eigyo_sosiki_history
	    WHERE
	       year = /*year*/2015
	       AND month = /*month*/01
	) mst_eigyo_sosiki
	/*%else*/
	mst_eigyo_sosiki
	/*%end*/
WHERE
  mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
  AND users.id = mst_tanto_s_sosiki.tanto_cd
/*%if !div1.isEmpty() */
  AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
  /*%if div1.size() == 1 */
    /*%if div2 != null */
  AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
    /*%end*/
    /*%if div3 != null */
  AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
    /*%end*/
    /*%if userId != null*/
  AND users.id = /* userId */'a'
    /*%end*/
    /*%if idForCustomer != null*/
  AND users.id_for_customer = /* idForCustomer */'a'
    /*%end*/
  /*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND users.id = /* userId */'a'
		/*%end*/
	    /*%if idForCustomer != null*/
	    AND users.id_for_customer = /* idForCustomer */'a'
	    /*%end*/
/*%end*/
ORDER BY
  mst_eigyo_sosiki.div1_cd
  ,mst_eigyo_sosiki.div2_cd
  ,mst_eigyo_sosiki.div3_cd
  ,users.id