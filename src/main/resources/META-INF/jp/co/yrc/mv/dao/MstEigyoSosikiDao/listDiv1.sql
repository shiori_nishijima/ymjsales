SELECT
    DISTINCT div1_cd 
FROM 
	mst_eigyo_sosiki
WHERE 
	div1_cd IS NOT NULL
	AND div1_cd <> ''
	AND deleted = 0
