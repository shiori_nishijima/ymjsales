SELECT
	DISTINCT mst_eigyo_sosiki.div1_cd
FROM
	users
	,mst_eigyo_sosiki
	,mst_tanto_s_sosiki
WHERE
	mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
	AND mst_tanto_s_sosiki.tanto_cd = users.id
	AND users.id = /* userId */'sfa'
ORDER BY
	mst_eigyo_sosiki.div1_cd