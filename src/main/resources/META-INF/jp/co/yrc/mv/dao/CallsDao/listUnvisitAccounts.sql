SELECT
        call_info.user_id
        ,users.first_name
        ,users.last_name
        ,CASE WHEN 
	    	CONCAT(TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, '')), TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))) = '' 
	    	THEN mst_eigyo_sosiki.eigyo_sosiki_nm 
	    	ELSE CONCAT(TRIM(IFNULL(mst_eigyo_sosiki.div2_nm, '')), TRIM(IFNULL(mst_eigyo_sosiki.div3_nm, ''))) 
	    	END AS eigyo_sosiki_nm 
        ,accounts.name AS account_name
        ,accounts.group_important
        ,accounts.group_new
        ,accounts.group_deep_plowing
        ,accounts.group_y_cp
        ,accounts.group_other_cp
        ,call_info.this_month_plan AS this_month_plan
        ,call_info.yesterday_plan AS yesterday_plan
    FROM
        (
            SELECT
                    calls.assigned_user_id AS user_id
                    ,accounts.id AS account_id
                    ,SUM(calls.planned_count) AS this_month_plan
                    ,SUM(
                        CASE WHEN calls.date_start < /*today*/2000
                            THEN calls.planned_count
                            ELSE 0
                        END
                    ) AS yesterday_plan
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                            accounts
                /*%end*/
                            ON calls.parent_type = 'Accounts'
                            AND calls.parent_id = accounts.id
                WHERE
                    calls.date_start >= /*from*/2000
                    AND calls.date_start < /*to*/2000
                    AND calls.planned_count > 0
                    AND calls.status = 'Planned' 
                /*%if !div1.isEmpty() */
                    AND accounts.sales_company_code IN /* div1 */('a', 'aa')
                /*%end*/
                /*%if div2 != null */
                    AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                    AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null*/
                    AND calls.assigned_user_id = /* userId */'sfa'
                /*%end*/
                    AND calls.deleted <> 1
                GROUP BY
                    user_id
                    ,accounts.id
            UNION ALL
            SELECT
                    calls_users.user_id AS user_id
                    ,accounts.id AS account_id
                    ,SUM(calls.planned_count) AS this_month_plan
                    ,SUM(
                        CASE WHEN calls.date_start < /*today*/2000
                            THEN calls.planned_count
                            ELSE 0
                        END
                    ) AS yesterday_plan
                FROM
                    calls
                        INNER JOIN
                /*%if isHistory */
                            (
                                SELECT
                                        *
                                    FROM
                                        accounts_history
                                            INNER JOIN (
                                                SELECT
                                                        id AS current_id
                                                        ,MAX(yearmonth) AS current_yearmonth
                                                    FROM
                                                        accounts_history
                                                    WHERE
                                                        yearmonth <= /* yearMonth */2000
                                                        /*%if !div1.isEmpty() */
                                                        AND sales_company_code IN /* div1 */('a', 'aa')
                                                        /*%end*/
                                                    GROUP BY current_id
                                            ) current
                                                ON current.current_id = accounts_history.id
                                                AND current.current_yearmonth = accounts_history.yearmonth
                            ) accounts
                /*%else*/
                        accounts
                /*%end*/
                        ON calls.parent_type = 'Accounts'
                        AND calls.parent_id = accounts.id
                INNER JOIN calls_users
                ON calls.id = calls_users.call_id
                WHERE
                    calls.date_start >= /*from*/2000
                    AND calls.date_start < /*to*/2000
                    AND calls.planned_count > 0
                    AND calls.status = 'Planned'
                /*%if !div1.isEmpty() */
                    AND accounts.sales_company_code IN /* div1 */('a', 'aa')
                /*%end*/
                /*%if div2 != null */
                    AND accounts.department_code = /* div2 */'a'
                /*%end*/
                /*%if div3 != null */
                    AND accounts.sales_office_code = /* div3 */'a'
                /*%end*/
                /*%if userId != null*/
                    AND calls_users.user_id = /* userId */'sfa'
                /*%end*/
                    AND calls.deleted <> 1
                GROUP BY
                    user_id
                    ,accounts.id
        ) call_info
        ,users
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_tanto_s_sosiki_history
		    WHERE
		        year = /*year*/2015
		        AND month = /*month*/01
		) mst_tanto_s_sosiki
		/*%else*/
		mst_tanto_s_sosiki
		/*%end*/
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
        ,
/*%if isHistory */
        (
            SELECT
                    *
                FROM
                    accounts_history
                        INNER JOIN (
                            SELECT
                                    id AS current_id
                                    ,MAX(yearmonth) AS current_yearmonth
                                FROM
                                    accounts_history
                                WHERE
                                    yearmonth <= /* yearMonth */2000
                                    /*%if !div1.isEmpty() */
                                    AND sales_company_code IN /* div1 */('a', 'aa')
                                    /*%end*/
                                GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
/*%else*/
        accounts
/*%end*/
    WHERE
        call_info.user_id = users.id
        AND call_info.account_id = accounts.id
        AND users.id = mst_tanto_s_sosiki.tanto_cd
        /** 営業所所属のみ表示 */
        AND mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
		AND mst_eigyo_sosiki.div1_cd = accounts.sales_company_code
		AND mst_eigyo_sosiki.div2_cd = accounts.department_code
		AND mst_eigyo_sosiki.div3_cd = accounts.sales_office_code
    ORDER BY
        sales_company_code
        ,department_code
        ,sales_office_code
        ,user_id