SELECT
  calls.id
  , calls.info_div
  , calls.sensitivity
  , calls.com_free_c 
  , comment_read_count.comment_read_count 
  , comment_count.comment_count
  , comment_entered.comment_entered
FROM
  ( 
    SELECT
      DATE (calls.date_start) AS date_start
      , calls.id
      , REPLACE(calls.info_div, '0', '') AS info_div
      , calls.sensitivity
      , calls.com_free_c 
    FROM
      calls 
      INNER JOIN accounts 
        ON calls.parent_type = 'Accounts' 
        AND calls.parent_id = accounts.id 
    WHERE
      calls.date_start >= /*from*/'2016-01-16 00:00:00.000' 
      AND calls.date_start < /*to*/'2016-01-22 00:00:00.000' 
      AND calls.assigned_user_id = /* userId */'sfa'
	  AND calls.info_div IS NOT NULL 
      AND calls.sensitivity IS NOT NULL 
      AND calls.deleted <> 1 
    UNION ALL 
    SELECT
      DATE (calls.date_start) AS date_start
      , calls.id
      , REPLACE(calls.info_div, '0', '') AS info_div
      , calls.sensitivity
      , calls.com_free_c 
    FROM
      calls 
      INNER JOIN opportunities 
        on calls.parent_type = 'Opportunities' 
        AND calls.parent_id = opportunities.id 
      INNER JOIN accounts 
        ON opportunities.account_id = accounts.id 
    WHERE
      calls.date_start >= /*from*/'2016-01-16 00:00:00.000' 
      AND calls.date_start < /*to*/'2016-01-22 00:00:00.000' 
      AND calls.assigned_user_id = /* userId */'sfa'
	  AND calls.info_div IS NOT NULL 
      AND calls.sensitivity IS NOT NULL 
      AND calls.deleted <> 1 
  ) calls 
  LEFT OUTER JOIN ( 
    SELECT
      count(id) AS comment_read_count
      , comments_read_users.call_id 
    FROM
      comments_read_users 
    WHERE
      comments_read_users.user_id = /* userId */'sfa'
    GROUP BY
      call_id
  ) comment_read_count 
    ON calls.id = comment_read_count.call_id 
  LEFT OUTER JOIN ( 
    SELECT
      (CASE WHEN COUNT(id) > 0 THEN 1 ELSE 0 END) AS comment_entered
      , parent_id 
    FROM
      comments comments 
    WHERE
      modified_user_id = /* userId */'sfa'
      AND parent_type = 'calls' 
      AND is_confirmation = 0 
    GROUP BY
      parent_id
  ) comment_entered
    ON calls.id = comment_entered.parent_id 
  LEFT OUTER JOIN ( 
    SELECT
      count(id) AS comment_count
      , parent_id 
    FROM
      comments comments 
    WHERE
      parent_type = 'calls' 
      AND is_confirmation = 0 
    GROUP BY
      parent_id
  ) comment_count 
    ON calls.id = comment_count.parent_id 
ORDER BY
  calls.date_start ASC
