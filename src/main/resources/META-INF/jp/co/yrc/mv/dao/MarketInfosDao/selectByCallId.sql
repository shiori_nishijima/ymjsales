select
  market_infos.id,
  market_infos.info_div,
  info_div.label AS info_div_name,
  market_infos.art_no,
  CASE WHEN market_infos.art_no IS NULL OR market_infos.art_no = '' THEN market_infos.size ELSE products.size END AS size,
  market_infos.serial,
  market_infos.pattern,
  market_infos.damaged_part,
  damaged_part.label AS damaged_part_name,
  market_infos.damaged_type,
  damaged_type.label AS damaged_type_name,
  market_infos.damaged_name,
  damaged_name.label AS damaged_name_name,
  market_infos.performance,
  performance.label AS performance_name,
  market_infos.usages,
  usages.label AS usages_name,
  market_infos.road_surface,
  road_surface.label AS road_surface_name,
  market_infos.car_type,
  car_type.label AS car_type_name,
  market_infos.car_body_shape,
  car_body_shape.label AS car_body_shape_name,
  market_infos.car_maker,
  car_maker.label AS car_maker_name,
  market_infos.mounting_term,
  market_infos.mileage,
  market_infos.mileage_unit,
  mileage_unit.label AS mileage_unit_name,
  market_infos.description,
  market_infos.mark,
  market_infos.call_id,
  market_infos.deleted,
  market_infos.date_entered,
  market_infos.date_modified,
  market_infos.modified_user_id,
  market_infos.created_by

from
  /*%if tech */ tech_service_market_infos /*%else*/ market_infos /*%end*/ market_infos
			INNER JOIN /*%if tech */tech_service_calls/*%else*/calls/*%end*/ calls
				ON calls.id = market_infos.call_id
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'carBodyShape' AND locale = /* locale */'ja') AS car_body_shape
				ON market_infos.car_body_shape = car_body_shape.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'carType' AND locale = /* locale */'ja') AS car_type
				ON market_infos.car_type = car_type.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'damagedPart' AND locale = /* locale */'ja') AS damaged_part
				ON market_infos.damaged_part = damaged_part.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'damagedType' AND locale = /* locale */'ja') AS damaged_type
				ON market_infos.damaged_type = damaged_type.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'infoDiv' AND locale = /* locale */'ja') AS info_div
				ON market_infos.info_div = info_div.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'mileageUnit' AND locale = /* locale */'ja') AS mileage_unit
				ON market_infos.mileage_unit = mileage_unit.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'performance' AND locale = /* locale */'ja') AS performance
				ON market_infos.performance = performance.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'roadSurface' AND locale = /* locale */'ja') AS road_surface
				ON market_infos.road_surface = road_surface.value
			LEFT OUTER JOIN
			(SELECT value, label, deleted FROM selection_items WHERE entity_name = 'MarketInfos' AND property_name = 'usages' AND locale = /* locale */'ja') AS usages
				ON market_infos.usages = usages.value
			LEFT OUTER JOIN
			(SELECT key_value, value, label, deleted FROM selection_items_second_level WHERE key_entity_name = 'MarketInfos' AND key_property_name = 'damagedType' AND property_name = 'damagedName' AND key_locale = /* locale */'ja') AS damaged_name
				ON market_infos.damaged_type = damaged_name.key_value
				AND  market_infos.damaged_name = damaged_name.value
			LEFT OUTER JOIN
			(SELECT key_value, value, label, deleted FROM selection_items_second_level WHERE key_entity_name = 'MarketInfos' AND key_property_name = 'kind' AND property_name = 'carMaker' AND key_locale = /* locale */'ja') AS car_maker
				ON calls.kind = car_maker.key_value
				AND  market_infos.car_maker = car_maker.value
			LEFT OUTER JOIN products
				ON market_infos.art_no = products.id
where
  market_infos.call_id = /* callId */'a'
