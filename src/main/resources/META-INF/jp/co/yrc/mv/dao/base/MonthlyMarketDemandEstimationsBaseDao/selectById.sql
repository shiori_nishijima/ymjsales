select
  year,
  month,
  user_id,
  eigyo_sosiki_cd,
  ss_year_demand_estimation,
  rs_year_demand_estimation,
  cd_year_demand_estimation,
  ps_year_demand_estimation,
  cs_year_demand_estimation,
  hc_year_demand_estimation,
  lease_year_demand_estimation,
  indirect_other_year_demand_estimation,
  truck_year_demand_estimation,
  bus_year_demand_estimation,
  hire_taxi_year_demand_estimation,
  direct_other_year_demand_estimation,
  retail_year_demand_estimation,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  monthly_market_demand_estimations
where
  year = /* year */1
  and
  month = /* month */1
  and
  user_id = /* userId */'a'
  and
  eigyo_sosiki_cd = /* eigyoSosikiCd */'a'
