SELECT
        id AS group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%else*/
        ,null AS date_start
        /*%end*/
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
        ,SUM(calls.call_user_count) AS call_user_count
    FROM
        (
            SELECT
			        CASE WHEN opportunity_accounts.id IS NULL THEN accounts.id ELSE opportunity_accounts.id END AS id
                    ,DATE(calls.date_start) AS date_start
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,IF (calls.is_plan, calls.planned_count, 0) AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,0 AS call_user_count
                FROM
                    calls
                    	LEFT OUTER JOIN
				/*%if isHistory && !canIgnoreSosikiCondition */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) accounts
				/*%else*/
							accounts
				/*%end*/
                        	ON calls.parent_type = 'Accounts'
                        	AND calls.parent_id = accounts.id
                    	LEFT OUTER JOIN opportunities
                        	ON calls.parent_type = 'Opportunities'
                        	AND calls.parent_id = opportunities.id
                        LEFT OUTER JOIN
				/*%if isHistory && !canIgnoreSosikiCondition */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) opportunity_accounts
				/*%else*/
							accounts AS opportunity_accounts
				/*%end*/
                        	ON opportunities.account_id = opportunity_accounts.id
	            WHERE
					calls.date_start >= /*from*/2000
					AND calls.date_start < /*to*/2000
				/*%if !canIgnoreSosikiCondition */
					/*%if div1 != null */
					AND accounts.sales_company_code = /* div1 */'a'
					/*%end*/
					/*%if div2 != null */
					AND accounts.department_code = /* div2 */'a'
					/*%end*/
					/*%if div3 != null */
					AND accounts.sales_office_code = /* div3 */'a'
					/*%end*/
	                /*%if marketId != null*/
	                AND accounts.market_id = /* marketId */'marketId'
	                /*%end*/
	                /*%if group == "GROUP_IMPORTANT"*/
	                AND accounts.group_important = 1
	                /*%end*/
	                /*%if group == "GROUP_NEW"*/
	                AND accounts.group_new = 1
	                /*%end*/
	                /*%if group == "GROUP_DEEP_PLOWING"*/
	                AND accounts.group_deep_plowing = 1
	                /*%end*/
	                /*%if group == "GROUP_Y_CP"*/
	                AND accounts.group_y_cp = 1
	                /*%end*/
	                /*%if group == "GROUP_OTHER_CP"*/
	                AND accounts.group_other_cp = 1
	                /*%end*/
	                /*%if group == "GROUP_ONE"*/
	                AND accounts.group_one = 1
	                /*%end*/
	                /*%if group == "GROUP_TWO"*/
	                AND accounts.group_two = 1
	                /*%end*/
	                /*%if group == "GROUP_THREE"*/
	                AND accounts.group_three = 1
	                /*%end*/
	                /*%if group == "GROUP_FOUR"*/
	                AND accounts.group_four = 1
	                /*%end*/
	                /*%if group == "GROUP_FIVE"*/
	                AND accounts.group_five = 1
	                /*%end*/
	                /*%if group == "A"*/
	                AND accounts.sales_rank = 'A'
	                /*%end*/
	                /*%if group == "B"*/
	                AND accounts.sales_rank = 'B'
	                /*%end*/
	                /*%if group == "C"*/
	                AND accounts.sales_rank = 'C'
	                /*%end*/
				/*%end*/
					/*%if userId != null */
					AND calls.assigned_user_id = /* userId */'sfa'
					/*%end*/
	                AND calls.deleted <> 1
		UNION ALL
            SELECT
			        CASE WHEN opportunity_accounts.id IS NULL THEN accounts.id ELSE opportunity_accounts.id END AS id
                    ,DATE(calls.date_start) AS date_start
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,IF (calls.is_plan, calls.planned_count, 0) AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                    ,1 AS call_user_count
                FROM
                    calls
                        INNER JOIN calls_users
                            ON calls_users.call_id = calls.id
                    	LEFT OUTER JOIN
				/*%if isHistory && !canIgnoreSosikiCondition */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) accounts
				/*%else*/
							accounts
				/*%end*/
                        	ON calls.parent_type = 'Accounts'
                        	AND calls.parent_id = accounts.id
                    	LEFT OUTER JOIN opportunities
                        	ON calls.parent_type = 'Opportunities'
                        	AND calls.parent_id = opportunities.id
                        LEFT OUTER JOIN
				/*%if isHistory && !canIgnoreSosikiCondition */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) opportunity_accounts
				/*%else*/
							accounts AS opportunity_accounts
				/*%end*/
                        	ON opportunities.account_id = opportunity_accounts.id
	            WHERE
					calls.date_start >= /*from*/2000
					AND calls.date_start < /*to*/2000
				/*%if !canIgnoreSosikiCondition */
					/*%if div1 != null */
					AND accounts.sales_company_code = /* div1 */'a'
					/*%end*/
					/*%if div2 != null */
					AND accounts.department_code = /* div2 */'a'
					/*%end*/
					/*%if div3 != null */
					AND accounts.sales_office_code = /* div3 */'a'
					/*%end*/
	                /*%if marketId != null*/
	                AND accounts.market_id = /* marketId */'marketId'
	                /*%end*/
	                /*%if group == "GROUP_IMPORTANT"*/
	                AND accounts.group_important = 1
	                /*%end*/
	                /*%if group == "GROUP_NEW"*/
	                AND accounts.group_new = 1
	                /*%end*/
	                /*%if group == "GROUP_DEEP_PLOWING"*/
	                AND accounts.group_deep_plowing = 1
	                /*%end*/
	                /*%if group == "GROUP_Y_CP"*/
	                AND accounts.group_y_cp = 1
	                /*%end*/
	                /*%if group == "GROUP_OTHER_CP"*/
	                AND accounts.group_other_cp = 1
	                /*%end*/
	                /*%if group == "GROUP_ONE"*/
	                AND accounts.group_one = 1
	                /*%end*/
	                /*%if group == "GROUP_TWO"*/
	                AND accounts.group_two = 1
	                /*%end*/
	                /*%if group == "GROUP_THREE"*/
	                AND accounts.group_three = 1
	                /*%end*/
	                /*%if group == "GROUP_FOUR"*/
	                AND accounts.group_four = 1
	                /*%end*/
	                /*%if group == "GROUP_FIVE"*/
	                AND accounts.group_five = 1
	                /*%end*/
	                /*%if group == "A"*/
	                AND accounts.sales_rank = 'A'
	                /*%end*/
	                /*%if group == "B"*/
	                AND accounts.sales_rank = 'B'
	                /*%end*/
	                /*%if group == "C"*/
	                AND accounts.sales_rank = 'C'
	                /*%end*/
				/*%end*/
					/*%if userId != null */
					AND calls_users.user_id = /* userId */'sfa'
					/*%end*/
	                AND calls.deleted <> 1
        ) calls
    GROUP BY
        group_type
        /*%if isDaily*/
        ,calls.date_start
        /*%end*/
	ORDER BY
		group_type ASC