SELECT
		account_info.id
		,account_info.parent_id
		,account_info.name
		,account_info.first_name
		,account_info.last_name
		,account_info.eigyo_sosiki_nm
		,account_info.assigned_user_id
		,account_info.group_important
		,account_info.group_new
		,account_info.group_deep_plowing
		,account_info.group_y_cp
		,account_info.group_other_cp
		,account_info.group_one
		,account_info.group_two
		,account_info.group_three
		,account_info.group_four
		,account_info.group_five
		,account_info.market_name
		,IFNULL(result_info.sales, 0) AS sales
		,IFNULL(result_info.margin, 0) AS margin
		,IFNULL(result_info.number, 0) AS number
		,IFNULL(call_info.planned_count, 0) AS planned_count
		,IFNULL(call_info.call_count, 0) AS call_count
		,IFNULL(call_info.info_count, 0) AS info_count
		/*%for item : items */
		,IFNULL(call_info.division_c_/*# item.value */, 0) AS division_c_/*# item.value */
		/*%end*/
		,IFNULL(last_month_call_info.call_count, 0) AS last_month_call_count
	FROM

	/** アカウント */
(
SELECT
		accounts.id
		,accounts.parent_id
		,accounts.name
		,users.first_name
		,users.last_name
		,mst_eigyo_sosiki.eigyo_sosiki_nm
		,accounts.assigned_user_id
		,users.id AS user_id
		,accounts.sales_company_code
		,accounts.department_code
		,accounts.sales_office_code
		,accounts.group_important
		,accounts.group_new
		,accounts.group_deep_plowing
		,accounts.group_y_cp
		,accounts.group_other_cp
		,accounts.group_one
		,accounts.group_two
		,accounts.group_three
		,accounts.group_four
		,accounts.group_five
		,markets.name AS market_name
	FROM
/*%if isHistory */
		(
			SELECT
					*
				FROM
					accounts_history
						INNER JOIN (
							SELECT
									id AS current_id
									,MAX(yearmonth) AS current_yearmonth
								FROM
									accounts_history
								WHERE
									yearmonth <= /* yearMonth */2000
                                    /*%if div1 != null */
                                    AND sales_company_code = /* div1 */'a'
                                    /*%end*/
								GROUP BY current_id
						) current
							ON current.current_id = accounts_history.id
							AND current.current_yearmonth = accounts_history.yearmonth
		) accounts
/*%else*/
		accounts
/*%end*/
		,users
		,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_tanto_s_sosiki_history
		    WHERE
		        year = /*year*/2015
		        AND month = /*month*/01
		) mst_tanto_s_sosiki
		/*%else*/
		mst_tanto_s_sosiki
		/*%end*/
		,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
		,markets
		WHERE
			/*%if div1 != null */
			accounts.sales_company_code = /* div1 */'a'
			/*%end*/
			/*%if div2 != null */
			AND accounts.department_code = /* div2 */'a'
			/*%end*/
			/*%if div3 != null */
			AND accounts.sales_office_code = /* div3 */'a'
			/*%end*/
            /*%if marketId != null*/
            AND accounts.market_id = /* marketId */'marketId'
            /*%end*/
            /*%if group == "GROUP_IMPORTANT"*/
            AND accounts.group_important = 1
            /*%end*/
            /*%if group == "GROUP_NEW"*/
            AND accounts.group_new = 1
            /*%end*/
            /*%if group == "GROUP_DEEP_PLOWING"*/
            AND accounts.group_deep_plowing = 1
            /*%end*/
            /*%if group == "GROUP_Y_CP"*/
            AND accounts.group_y_cp = 1
            /*%end*/
            /*%if group == "GROUP_OTHER_CP"*/
            AND accounts.group_other_cp = 1
            /*%end*/
            /*%if group == "GROUP_ONE"*/
            AND accounts.group_one = 1
            /*%end*/
            /*%if group == "GROUP_TWO"*/
            AND accounts.group_two = 1
            /*%end*/
            /*%if group == "GROUP_THREE"*/
            AND accounts.group_three = 1
            /*%end*/
            /*%if group == "GROUP_FOUR"*/
            AND accounts.group_four = 1
            /*%end*/
            /*%if group == "GROUP_FIVE"*/
            AND accounts.group_five = 1
            /*%end*/
            /*%if group == "A"*/
            AND accounts.sales_rank = 'A'
            /*%end*/
            /*%if group == "B"*/
            AND accounts.sales_rank = 'B'
            /*%end*/
            /*%if group == "C"*/
            AND accounts.sales_rank = 'C'
            /*%end*/
			AND users.id_for_customer = accounts.assigned_user_id
			AND users.id =mst_tanto_s_sosiki.tanto_cd
			AND mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
			AND mst_eigyo_sosiki.div1_cd = accounts.sales_company_code
			AND mst_eigyo_sosiki.div2_cd = accounts.department_code
			AND mst_eigyo_sosiki.div3_cd = accounts.sales_office_code
			AND markets.id = accounts.market_id
) account_info

LEFT OUTER JOIN
/** 実績の集計 */
(
SELECT
		SUM(sales) AS sales
		,SUM(margin) AS margin
		,SUM(number) AS number
		,result.account_id
	FROM (
		SELECT
				SUM(results.sales) AS sales
				,SUM(results.margin) AS margin
				,SUM(
					CASE
						WHEN
							/** TIFU */
							compass_kinds.middle_class = '91'
							/** OTHER */
							OR compass_kinds.middle_class = '92'
							OR compass_kinds.middle_class = '93'
							OR compass_kinds.middle_class = '94'
							OR compass_kinds.middle_class = '95'
							/** WORK */
							OR compass_kinds.middle_class = '96'
							OR compass_kinds.middle_class = '97'
							/** RETREAD */
							OR compass_kinds.middle_class = 'A1'
							OR compass_kinds.middle_class = 'A2'
							OR compass_kinds.middle_class = 'A3'
							OR compass_kinds.middle_class = 'B1'
							OR compass_kinds.middle_class = 'B2'
						THEN 0 
					ELSE results.number END
				) AS number
				,results.account_id
			FROM
				(
					SELECT
							SUM(results.sales) AS sales
							,SUM(results.margin) AS margin
							,SUM(results.number) AS number
							,results.account_id
							,results.compass_kind
						FROM
							results
						WHERE
							results.year = /* year */2000
							AND results.month = /* month */1
							/*%if div1 != null */
							AND results.sales_company_code = /* div1 */'a'
							/*%end*/
							/*%if div2 != null */
							AND results.department_code = /* div2 */'a'
							/*%end*/
							/*%if div3 != null */
							AND results.sales_office_code = /* div3 */'a'
							/*%end*/
						GROUP BY
							results.account_id
							,results.compass_kind
				) results
				,compass_kinds
				,
		/*%if isHistory */
				(
					SELECT
							*
						FROM
							accounts_history
								INNER JOIN (
									SELECT
											id AS current_id
											,MAX(yearmonth) AS current_yearmonth
										FROM
											accounts_history
										WHERE
											yearmonth <= /* yearMonth */2000
                                            /*%if div1 != null */
                                            AND sales_company_code = /* div1 */'a'
                                            /*%end*/
										GROUP BY current_id
								) current
									ON current.current_id = accounts_history.id
									AND current.current_yearmonth = accounts_history.yearmonth
				) accounts
		/*%else*/
				accounts
		/*%end*/
			WHERE
				results.account_id = accounts.id
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND compass_kinds.id = results.compass_kind
				AND (
					compass_kinds.middle_class = '11'
					OR compass_kinds.middle_class = '12'
					OR compass_kinds.middle_class = '13'
					OR compass_kinds.middle_class = '21'
					OR compass_kinds.middle_class = '22'
					OR compass_kinds.middle_class = '51'
					OR compass_kinds.middle_class = '61'
					OR compass_kinds.middle_class = '62'
					OR compass_kinds.middle_class = '71'
					OR compass_kinds.middle_class = '72'
					/** OR */
					OR (
						compass_kinds.middle_class = '41'
						AND compass_kinds.small_class = '50'
					)
					/** ID */
					OR (
						compass_kinds.middle_class = '41'
						AND compass_kinds.small_class = '55'
					)
					/** TIFU */
					OR compass_kinds.middle_class = '91'
					/** OTHER */
					OR compass_kinds.middle_class = '92'
					OR compass_kinds.middle_class = '93'
					OR compass_kinds.middle_class = '94'
					OR compass_kinds.middle_class = '95'
					/** WORK */
					OR compass_kinds.middle_class = '96'
					OR compass_kinds.middle_class = '97'
					/** RETREAD */
					OR compass_kinds.middle_class = 'A1'
					OR compass_kinds.middle_class = 'A2'
					OR compass_kinds.middle_class = 'A3'
					OR compass_kinds.middle_class = 'B1'
					OR compass_kinds.middle_class = 'B2'
				)
			GROUP BY
				results.account_id
		) result
	GROUP BY
		result.account_id
) result_info
		ON account_info.id = result_info.account_id

LEFT OUTER JOIN
/** 訪問の集計 */
(
SELECT
		calls.account_id
		,SUM(calls.planned_count) AS planned_count
		,SUM(calls.held_count) AS call_count
		,SUM(calls.info_count) AS info_count
		/*%for item : items */
		,SUM(division_c_/*# item.value */) AS division_c_/*# item.value */
		/*%end*/
	FROM
		(
			SELECT
					calls.parent_id AS account_id
					,CASE WHEN
						calls.com_free_c IS NOT NULL AND calls.com_free_c <> ''
						OR market_infos.description IS NOT NULL AND market_infos.description <> '' THEN 1 
						ELSE 0 
					END AS info_count
					,calls.planned_count
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				/*%for item : items */
					,CASE WHEN calls.status = 'Held' AND calls.division_c LIKE /* @infix(item.value) */'01' THEN 1 ELSE 0 END AS division_c_/*# item.value */
				/*%end*/
				FROM
					calls
						INNER JOIN
				/*%if isHistory */
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
				/*%else*/
							accounts
				/*%end*/
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
					LEFT OUTER JOIN market_infos
						ON calls.id = market_infos.call_id
			WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		UNION ALL
			SELECT
					calls.parent_id AS account_id
					,CASE WHEN
						calls.com_free_c IS NOT NULL AND calls.com_free_c <> ''
						OR market_infos.description IS NOT NULL AND market_infos.description <> '' THEN 1 
						ELSE 0 
					END AS info_count
					,calls.planned_count
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				/*%for item : items */
					,CASE WHEN calls.status = 'Held' AND calls.division_c LIKE /* @infix(item.value) */'01' THEN 1 ELSE 0 END AS division_c_/*# item.value */
				/*%end*/
				FROM
					calls
						INNER JOIN
				/*%if isHistory */
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
				/*%else*/
							accounts
				/*%end*/
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
						INNER JOIN calls_users
							ON calls.id = calls_users.call_id
					LEFT OUTER JOIN market_infos
						ON calls.id = market_infos.call_id
			WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		) calls
	GROUP BY
		account_id
) call_info
		ON account_info.id = call_info.account_id



LEFT OUTER JOIN
/** 前月訪問の集計 */
(
SELECT
		calls.account_id
		,SUM(calls.held_count) AS call_count
	FROM
		(
			SELECT
					calls.parent_id AS account_id
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				FROM
					calls
						INNER JOIN
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearLastMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
			WHERE
				calls.date_start >= /*lastMonthFrom*/2000
				AND calls.date_start < /*lastMonthTo*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		UNION ALL
			SELECT
					calls.parent_id AS account_id
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				FROM
					calls
						INNER JOIN
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearLastMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
						INNER JOIN calls_users
							ON calls.id = calls_users.call_id
			WHERE
				calls.date_start >= /*lastMonthFrom*/2000
				AND calls.date_start < /*lastMonthTo*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		) calls
	GROUP BY
		account_id
) last_month_call_info
		ON account_info.id = last_month_call_info.account_id
	ORDER BY
		account_info.sales_company_code ASC
		,account_info.department_code ASC
		,account_info.sales_office_code ASC
		,account_info.user_id ASC
		,account_info.id ASC