SELECT
        sales_rank AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_NEW' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_new = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_DEEP_PLOWING' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_deep_plowing = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_Y_CP' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_y_cp = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_OTHER_CP' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_other_cp = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_IMPORTANT' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_important = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_ONE' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_one = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_TWO' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_two = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_THREE' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_three = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_FOUR' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_four = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_FIVE' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_five = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
UNION ALL
SELECT
        'GROUP_OUTSIDE' AS group_type
        ,SUM(demand_number) AS demand_number
    FROM
		(
            SELECT
            		*
            	FROM
			/*%if isHistory */
			        (
			            SELECT
								*
			                FROM
			                    accounts_history
			                        INNER JOIN (
			                        	SELECT
			                        			id AS current_id
			                        			,MAX(yearmonth) AS current_yearmonth
			                        		FROM
			                        			accounts_history
			                        		WHERE
			                        			yearmonth <= /* yearMonth */2000
												/*%if !div1.isEmpty() */
										        AND sales_company_code IN /* div1 */('a', 'aa')
												/*%end*/
					                        GROUP BY current_id
			                        ) current
			                            ON current.current_id = accounts_history.id
			                            AND current.current_yearmonth = accounts_history.yearmonth
			        ) accounts
			/*%else*/
			    	accounts
			/*%end*/
			    WHERE
			/*%if !div1.isEmpty() */
			        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
			    /*%if div1.size() == 1 */
			        /*%if div2 != null */
			        AND accounts.department_code = /* div2 */'a'
			        /*%end*/
			        /*%if div3 != null */
			        AND accounts.sales_office_code = /* div3 */'a'
			        /*%end*/
			    /*%end*/
			/*%end*/
		) accounts
        ,users
    WHERE
        accounts.assigned_user_id = users.id_for_customer
        AND accounts.group_important = 1
/*%if !div1.isEmpty() */
        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1
    GROUP BY
        group_type
