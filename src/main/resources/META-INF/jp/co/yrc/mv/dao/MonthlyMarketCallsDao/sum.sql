SELECT
        monthly_market_calls.year
        ,monthly_market_calls.month
		,SUM(monthly_market_calls.ss_plan_count) AS ss_plan_count
		,SUM(monthly_market_calls.ss_call_count) AS ss_call_count
		,SUM(monthly_market_calls.ss_info_count) AS ss_info_count
		,SUM(monthly_market_calls.rs_plan_count) AS rs_plan_count
		,SUM(monthly_market_calls.rs_call_count) AS rs_call_count
		,SUM(monthly_market_calls.rs_info_count) AS rs_info_count
		,SUM(monthly_market_calls.cd_plan_count) AS cd_plan_count
		,SUM(monthly_market_calls.cd_call_count) AS cd_call_count
		,SUM(monthly_market_calls.cd_info_count) AS cd_info_count
		,SUM(monthly_market_calls.ps_plan_count) AS ps_plan_count
		,SUM(monthly_market_calls.ps_call_count) AS ps_call_count
		,SUM(monthly_market_calls.ps_info_count) AS ps_info_count
		,SUM(monthly_market_calls.cs_plan_count) AS cs_plan_count
		,SUM(monthly_market_calls.cs_call_count) AS cs_call_count
		,SUM(monthly_market_calls.cs_info_count) AS cs_info_count
		,SUM(monthly_market_calls.hc_plan_count) AS hc_plan_count
		,SUM(monthly_market_calls.hc_call_count) AS hc_call_count
		,SUM(monthly_market_calls.hc_info_count) AS hc_info_count
		,SUM(monthly_market_calls.lease_plan_count) AS lease_plan_count
		,SUM(monthly_market_calls.lease_call_count) AS lease_call_count
		,SUM(monthly_market_calls.lease_info_count) AS lease_info_count
		,SUM(monthly_market_calls.indirect_plan_count) AS indirect_plan_count
		,SUM(monthly_market_calls.indirect_other_call_count) AS indirect_other_call_count
		,SUM(monthly_market_calls.indirect_other_info_count) AS indirect_other_info_count
		,SUM(monthly_market_calls.truck_plan_count) AS truck_plan_count
		,SUM(monthly_market_calls.truck_call_count) AS truck_call_count
		,SUM(monthly_market_calls.truck_info_count) AS truck_info_count
		,SUM(monthly_market_calls.bus_plan_count) AS bus_plan_count
		,SUM(monthly_market_calls.bus_call_count) AS bus_call_count
		,SUM(monthly_market_calls.bus_info_count) AS bus_info_count
		,SUM(monthly_market_calls.hire_plan_count) AS hire_plan_count
		,SUM(monthly_market_calls.hire_taxi_call_count) AS hire_taxi_call_count
		,SUM(monthly_market_calls.hire_taxi_info_count) AS hire_taxi_info_count
		,SUM(monthly_market_calls.direct_plan_count) AS direct_plan_count
		,SUM(monthly_market_calls.direct_other_call_count) AS direct_other_call_count
		,SUM(monthly_market_calls.direct_other_info_count) AS direct_other_info_count
		,SUM(monthly_market_calls.retail_plan_count) AS retail_plan_count
		,SUM(monthly_market_calls.retail_call_count) AS retail_call_count
		,SUM(monthly_market_calls.retail_info_count) AS retail_info_count
		,SUM(monthly_market_calls.outside_ss_plan_count) AS outside_ss_plan_count
		,SUM(monthly_market_calls.outside_ss_call_count) AS outside_ss_call_count
		,SUM(monthly_market_calls.outside_ss_info_count) AS outside_ss_info_count
		,SUM(monthly_market_calls.outside_rs_plan_count) AS outside_rs_plan_count
		,SUM(monthly_market_calls.outside_rs_call_count) AS outside_rs_call_count
		,SUM(monthly_market_calls.outside_rs_info_count) AS outside_rs_info_count
		,SUM(monthly_market_calls.outside_cd_plan_count) AS outside_cd_plan_count
		,SUM(monthly_market_calls.outside_cd_call_count) AS outside_cd_call_count
		,SUM(monthly_market_calls.outside_cd_info_count) AS outside_cd_info_count
		,SUM(monthly_market_calls.outside_ps_plan_count) AS outside_ps_plan_count
		,SUM(monthly_market_calls.outside_ps_call_count) AS outside_ps_call_count
		,SUM(monthly_market_calls.outside_ps_info_count) AS outside_ps_info_count
		,SUM(monthly_market_calls.outside_cs_plan_count) AS outside_cs_plan_count
		,SUM(monthly_market_calls.outside_cs_call_count) AS outside_cs_call_count
		,SUM(monthly_market_calls.outside_cs_info_count) AS outside_cs_info_count
		,SUM(monthly_market_calls.outside_hc_plan_count) AS outside_hc_plan_count
		,SUM(monthly_market_calls.outside_hc_call_count) AS outside_hc_call_count
		,SUM(monthly_market_calls.outside_hc_info_count) AS outside_hc_info_count
		,SUM(monthly_market_calls.outside_lease_plan_count) AS outside_lease_plan_count
		,SUM(monthly_market_calls.outside_lease_call_count) AS outside_lease_call_count
		,SUM(monthly_market_calls.outside_lease_info_count) AS outside_lease_info_count
		,SUM(monthly_market_calls.outside_indirect_plan_count) AS outside_indirect_plan_count
		,SUM(monthly_market_calls.outside_indirect_other_call_count) AS outside_indirect_other_call_count
		,SUM(monthly_market_calls.outside_indirect_other_info_count) AS outside_indirect_other_info_count
		,SUM(monthly_market_calls.outside_truck_plan_count) AS outside_truck_plan_count
		,SUM(monthly_market_calls.outside_truck_call_count) AS outside_truck_call_count
		,SUM(monthly_market_calls.outside_truck_info_count) AS outside_truck_info_count
		,SUM(monthly_market_calls.outside_bus_plan_count) AS outside_bus_plan_count
		,SUM(monthly_market_calls.outside_bus_call_count) AS outside_bus_call_count
		,SUM(monthly_market_calls.outside_bus_info_count) AS outside_bus_info_count
		,SUM(monthly_market_calls.outside_hire_plan_count) AS outside_hire_plan_count
		,SUM(monthly_market_calls.outside_hire_taxi_call_count) AS outside_hire_taxi_call_count
		,SUM(monthly_market_calls.outside_hire_taxi_info_count) AS outside_hire_taxi_info_count
		,SUM(monthly_market_calls.outside_direct_plan_count) AS outside_direct_plan_count
		,SUM(monthly_market_calls.outside_direct_other_call_count) AS outside_direct_other_call_count
		,SUM(monthly_market_calls.outside_direct_other_info_count) AS outside_direct_other_info_count
		,SUM(monthly_market_calls.outside_retail_plan_count) AS outside_retail_plan_count
		,SUM(monthly_market_calls.outside_retail_call_count) AS outside_retail_call_count
		,SUM(monthly_market_calls.outside_retail_info_count) AS outside_retail_info_count
    FROM
        monthly_market_calls
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_market_calls.year = /* year */1
        AND monthly_market_calls.month = /* month */1
        AND monthly_market_calls.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_market_calls.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_market_calls.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_market_calls.year
		,monthly_market_calls.month
