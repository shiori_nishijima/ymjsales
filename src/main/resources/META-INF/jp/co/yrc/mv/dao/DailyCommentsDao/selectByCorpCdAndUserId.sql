SELECT
		daily_comments.id
		,daily_comments.year
		,daily_comments.month
		,daily_comments.date
		,daily_comments.user_id
		,daily_comments.user_comment
		,daily_comments.boss_comment
		,daily_comments.completed
		,daily_comments.date_entered
		,daily_comments.date_modified
		,daily_comments.modified_user_id
		,daily_comments.created_by
		,daily_comments.deleted
		,usersMergedEigyo.first_name
		,usersMergedEigyo.last_name
		,usersMergedEigyo.eigyo_sosiki_nm
	FROM
		daily_comments INNER JOIN (
			SELECT
					mst_eigyo_sosiki.eigyo_sosiki_nm
					,users.first_name
					,users.last_name
					,users.id AS user_id
				FROM
					/*%if isHistory */
					(
					  SELECT
					      *
					    FROM
					      mst_eigyo_sosiki_history
					    WHERE
					       year = /*year*/2015
					       AND month = /*month*/01
					) mst_eigyo_sosiki
					/*%else*/
					mst_eigyo_sosiki
					/*%end*/
						LEFT OUTER JOIN
							/*%if isHistory */
							(
							  SELECT
							      *
							    FROM
							      mst_tanto_s_sosiki_history
							    WHERE
							       year = /*year*/2015
							       AND month = /*month*/01
							) mst_tanto_s_sosiki
							/*%else*/
							mst_tanto_s_sosiki
							/*%end*/
							ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
							AND mst_tanto_s_sosiki.deleted = 0
						LEFT OUTER JOIN users
							ON users.id = mst_tanto_s_sosiki.tanto_cd
							AND users.deleted = 0
				WHERE
					mst_eigyo_sosiki.corp_cd in /* corpCodes */('a', 'b', 'c')
					AND mst_tanto_s_sosiki.tanto_cd = /* userId */'a'
					AND mst_eigyo_sosiki.deleted = 0
		) usersMergedEigyo
			ON daily_comments.user_id = usersMergedEigyo.user_id
WHERE
	daily_comments.year = /* year */'2014'
	AND daily_comments.month = /* month */'1'
	AND daily_comments.date = /* date */'1'
	AND daily_comments.user_id = /* userId */'a'
limit 1
