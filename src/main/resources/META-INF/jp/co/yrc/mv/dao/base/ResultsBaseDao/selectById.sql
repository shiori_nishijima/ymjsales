select
  year,
  month,
  date,
  sales_company_code,
  account_id,
  sga_year_month,
  compass_kind,
  department_code,
  sales_office_code,
  assigned_user_id,
  accoount_category,
  sga_year,
  sga_period_div,
  product_class_yrc_div,
  large_class1,
  large_class2,
  middle_class,
  small_class,
  product_kind_cd,
  number,
  sales,
  base_amount,
  margin,
  monthly_number,
  monthly_sales,
  monthly_base_amount,
  monthly_margin,
  date_updated,
  hq_margin,
  monthly_hq_margin,
  modified_div,
  jatma_cd,
  kind_div,
  closing_div,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  results
where
  year = /* year */1
  and
  month = /* month */1
  and
  date = /* date */1
  and
  sales_company_code = /* salesCompanyCode */'a'
  and
  account_id = /* accountId */'a'
  and
  compass_kind = /* compassKind */'a'
