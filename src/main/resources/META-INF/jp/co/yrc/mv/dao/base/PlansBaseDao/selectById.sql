select
  year,
  month,
  sales_company_code,
  department_code,
  sales_office_code,
  assigned_user_id,
  sga_plan_year,
  sga_plan_term_div,
  sga_plan_year_month,
  product_class_yrc_div,
  large_class1,
  large_class2,
  middle_class,
  small_class,
  product_kind_cd,
  number,
  sales,
  margin,
  hq_margin,
  base_amount,
  updated_user_id,
  updated,
  date_updated,
  kind_other_cp_div,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  plans
where
  year = /* year */1
  and
  month = /* month */1
  and
  sales_company_code = /* salesCompanyCode */'a'
  and
  department_code = /* departmentCode */'a'
  and
  sales_office_code = /* salesOfficeCode */'a'
  and
  assigned_user_id = /* assignedUserId */'a'
  and
  sga_plan_term_div = /* sgaPlanTermDiv */'a'
  and
  product_class_yrc_div = /* productClassYrcDiv */'a'
  and
  large_class1 = /* largeClass1 */'a'
  and
  large_class2 = /* largeClass2 */'a'
  and
  middle_class = /* middleClass */'a'
  and
  small_class = /* smallClass */'a'
  and
  product_kind_cd = /* productKindCd */'a'
