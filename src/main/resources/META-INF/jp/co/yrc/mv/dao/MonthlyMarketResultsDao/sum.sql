SELECT
        monthly_market_results.year
        ,monthly_market_results.month
		,SUM(monthly_market_results.ss_sales) AS ss_sales
		,SUM(monthly_market_results.ss_margin) AS ss_margin
		,SUM(monthly_market_results.ss_number) AS ss_number
		,SUM(monthly_market_results.rs_sales) AS rs_sales
		,SUM(monthly_market_results.rs_margin) AS rs_margin
		,SUM(monthly_market_results.rs_number) AS rs_number
		,SUM(monthly_market_results.cd_sales) AS cd_sales
		,SUM(monthly_market_results.cd_margin) AS cd_margin
		,SUM(monthly_market_results.cd_number) AS cd_number
		,SUM(monthly_market_results.ps_sales) AS ps_sales
		,SUM(monthly_market_results.ps_margin) AS ps_margin
		,SUM(monthly_market_results.ps_number) AS ps_number
		,SUM(monthly_market_results.cs_sales) AS cs_sales
		,SUM(monthly_market_results.cs_margin) AS cs_margin
		,SUM(monthly_market_results.cs_number) AS cs_number
		,SUM(monthly_market_results.hc_sales) AS hc_sales
		,SUM(monthly_market_results.hc_margin) AS hc_margin
		,SUM(monthly_market_results.hc_number) AS hc_number
		,SUM(monthly_market_results.lease_sales) AS lease_sales
		,SUM(monthly_market_results.lease_margin) AS lease_margin
		,SUM(monthly_market_results.lease_number) AS lease_number
		,SUM(monthly_market_results.indirect_other_sales) AS indirect_other_sales
		,SUM(monthly_market_results.indirect_other_margin) AS indirect_other_margin
		,SUM(monthly_market_results.indirect_other_number) AS indirect_other_number
		,SUM(monthly_market_results.truck_sales) AS truck_sales
		,SUM(monthly_market_results.truck_margin) AS truck_margin
		,SUM(monthly_market_results.truck_number) AS truck_number
		,SUM(monthly_market_results.bus_sales) AS bus_sales
		,SUM(monthly_market_results.bus_margin) AS bus_margin
		,SUM(monthly_market_results.bus_number) AS bus_number
		,SUM(monthly_market_results.hire_taxi_sales) AS hire_taxi_sales
		,SUM(monthly_market_results.hire_taxi_margin) AS hire_taxi_margin
		,SUM(monthly_market_results.hire_taxi_number) AS hire_taxi_number
		,SUM(monthly_market_results.direct_other_sales) AS direct_other_sales
		,SUM(monthly_market_results.direct_other_margin) AS direct_other_margin
		,SUM(monthly_market_results.direct_other_number) AS direct_other_number
		,SUM(monthly_market_results.retail_sales) AS retail_sales
		,SUM(monthly_market_results.retail_margin) AS retail_margin
		,SUM(monthly_market_results.retail_number) AS retail_number
    FROM
        monthly_market_results
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_market_results.year = /* year */1
        AND monthly_market_results.month = /* month */1
		/*%if userId != null*/
        AND monthly_market_results.user_id = /* userId */'a'
		/*%end*/
        AND monthly_market_results.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_market_results.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_market_results.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_market_results.year
		,monthly_market_results.month
