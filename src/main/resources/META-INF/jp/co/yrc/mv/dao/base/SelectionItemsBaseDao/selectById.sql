select
  entity_name,
  property_name,
  value,
  label,
  sort_order,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  selection_items
where
  entity_name = /* entityName */'a'
  and
  property_name = /* propertyName */'a'
  and
  value = /* value */'a'
  and
  locale = /* locale */'ja'
