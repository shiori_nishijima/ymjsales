SELECT
		monthly_comments.id
		,monthly_comments.year
		,monthly_comments.month
		,monthly_comments.user_id
		,monthly_comments.plan_visit_num
		,monthly_comments.plan_info_num
		,monthly_comments.plan_demand_coefficient
		,monthly_comments.user_comment
		,monthly_comments.boss_comment1
		,monthly_comments.boss_comment2
		,monthly_comments.boss_comment3
		,monthly_comments.boss_comment4
		,monthly_comments.boss_comment5
		,monthly_comments.date_entered
		,monthly_comments.date_modified
		,monthly_comments.modified_user_id
		,monthly_comments.created_by
		,monthly_comments.deleted
		,users.first_name
		,users.last_name
	FROM
		monthly_comments
			INNER JOIN users
				ON	monthly_comments.user_id = users.id
	WHERE
		monthly_comments.year = /* year */'2014'
	AND
		monthly_comments.month = /* month */'1'
	AND
		monthly_comments.user_id = /* userId */'a'
