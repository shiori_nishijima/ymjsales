SELECT
  id,
  call_id,
  user_id,
  date_modified,
  deleted
FROM
  /*%if tech */tech_service_comments_read_users/*%else*/comments_read_users/*%end*/ comments_read_users
WHERE
  call_id = /* callId */'a'
  AND user_id = /* userId */'a'
