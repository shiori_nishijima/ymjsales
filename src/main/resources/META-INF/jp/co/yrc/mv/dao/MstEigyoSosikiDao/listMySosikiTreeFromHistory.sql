SELECT
		mst_eigyo_sosiki.eigyo_sosiki_cd
		,mst_eigyo_sosiki.eigyo_sosiki_nm
		,mst_eigyo_sosiki.eigyo_sosiki_short_nm
		,mst_eigyo_sosiki.corp_cd
		,mst_eigyo_sosiki.div1_cd
		,mst_eigyo_sosiki.div2_cd
		,mst_eigyo_sosiki.div3_cd
		,mst_eigyo_sosiki.div4_cd
		,mst_eigyo_sosiki.div5_cd
		,mst_eigyo_sosiki.div6_cd
		,mst_eigyo_sosiki.div1_nm
		,mst_eigyo_sosiki.div2_nm
		,mst_eigyo_sosiki.div3_nm
		,mst_eigyo_sosiki.div4_nm
		,mst_eigyo_sosiki.div5_nm
		,mst_eigyo_sosiki.div6_nm
		,users.first_name
		,users.last_name
		,users.id AS user_id
	FROM
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
			LEFT OUTER JOIN 
				mst_tanto_s_sosiki
				ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
				AND mst_tanto_s_sosiki.deleted = 0
			LEFT OUTER JOIN users
				ON users.id = mst_tanto_s_sosiki.tanto_cd
				AND users.deleted = 0
	WHERE
		mst_eigyo_sosiki.corp_cd in /* corpCodes */('a', 'b', 'c')
		AND mst_tanto_s_sosiki.tanto_cd = /* userName */'sfa'
		AND mst_eigyo_sosiki.deleted = 0
	ORDER BY 
		div1_cd
		,div2_cd
		,div3_cd
		,user_id