SELECT DISTINCT
  users.id,
  users.id_for_customer,
  users.user_hash,
  users.first_name,
  users.last_name,
  users.authorization
FROM
  users
  ,mst_tanto_s_sosiki
  ,mst_eigyo_sosiki
WHERE
  id = /* id */'a'
  AND users.deleted = 0
  AND mst_eigyo_sosiki.deleted = 0
  AND mst_tanto_s_sosiki.deleted = 0
  AND mst_eigyo_sosiki.eigyo_sosiki_cd = mst_tanto_s_sosiki.eigyo_sosiki_cd
  AND users.id = mst_tanto_s_sosiki.tanto_cd
