SELECT
    url
    ,name
    ,sort_order
    ,deleted
    ,date_entered
    ,date_modified
    ,modified_user_id
    ,created_by
  FROM
    view_infos
  WHERE
    /*%if url != null */
    url = /* url */'url'
    /*%end*/
  ORDER BY
    sort_order ASC
