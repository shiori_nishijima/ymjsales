SELECT
        monthly_group_calls.year
        ,monthly_group_calls.month
		,SUM(monthly_group_calls.rank_a_plan_count) AS rank_a_plan_count
		,SUM(monthly_group_calls.rank_a_call_count) AS rank_a_call_count
		,SUM(monthly_group_calls.rank_a_info_count) AS rank_a_info_count
		,SUM(monthly_group_calls.rank_b_plan_count) AS rank_b_plan_count
		,SUM(monthly_group_calls.rank_b_call_count) AS rank_b_call_count
		,SUM(monthly_group_calls.rank_b_info_count) AS rank_b_info_count
		,SUM(monthly_group_calls.rank_c_plan_count) AS rank_c_plan_count
		,SUM(monthly_group_calls.rank_c_call_count) AS rank_c_call_count
		,SUM(monthly_group_calls.rank_c_info_count) AS rank_c_info_count
		,SUM(monthly_group_calls.important_plan_count) AS important_plan_count
		,SUM(monthly_group_calls.important_call_count) AS important_call_count
		,SUM(monthly_group_calls.important_info_count) AS important_info_count
		,SUM(monthly_group_calls.new_plan_count) AS new_plan_count
		,SUM(monthly_group_calls.new_call_count) AS new_call_count
		,SUM(monthly_group_calls.new_info_count) AS new_info_count
		,SUM(monthly_group_calls.deep_plowing_plan_count) AS deep_plowing_plan_count
		,SUM(monthly_group_calls.deep_plowing_call_count) AS deep_plowing_call_count
		,SUM(monthly_group_calls.deep_plowing_info_count) AS deep_plowing_info_count
		,SUM(monthly_group_calls.y_cp_plan_count) AS y_cp_plan_count
		,SUM(monthly_group_calls.y_cp_call_count) AS y_cp_call_count
		,SUM(monthly_group_calls.y_cp_info_count) AS y_cp_info_count
		,SUM(monthly_group_calls.other_cp_plan_count) AS other_cp_plan_count
		,SUM(monthly_group_calls.other_cp_call_count) AS other_cp_call_count
		,SUM(monthly_group_calls.other_cp_info_count) AS other_cp_info_count
		,SUM(monthly_group_calls.group_one_plan_count) AS group_one_plan_count
		,SUM(monthly_group_calls.group_one_call_count) AS group_one_call_count
		,SUM(monthly_group_calls.group_one_info_count) AS group_one_info_count
		,SUM(monthly_group_calls.group_two_plan_count) AS group_two_plan_count
		,SUM(monthly_group_calls.group_two_call_count) AS group_two_call_count
		,SUM(monthly_group_calls.group_two_info_count) AS group_two_info_count
		,SUM(monthly_group_calls.group_three_plan_count) AS group_three_plan_count
		,SUM(monthly_group_calls.group_three_call_count) AS group_three_call_count
		,SUM(monthly_group_calls.group_three_info_count) AS group_three_info_count
		,SUM(monthly_group_calls.group_four_plan_count) AS group_four_plan_count
		,SUM(monthly_group_calls.group_four_call_count) AS group_four_call_count
		,SUM(monthly_group_calls.group_four_info_count) AS group_four_info_count
		,SUM(monthly_group_calls.group_five_plan_count) AS group_five_plan_count
		,SUM(monthly_group_calls.group_five_call_count) AS group_five_call_count
		,SUM(monthly_group_calls.group_five_info_count) AS group_five_info_count
		,SUM(monthly_group_calls.outside_plan_count) AS outside_plan_count
		,SUM(monthly_group_calls.outside_call_count) AS outside_call_count
		,SUM(monthly_group_calls.outside_info_count) AS outside_info_count
		,SUM(monthly_group_calls.outside_rank_a_plan_count) AS outside_rank_a_plan_count
		,SUM(monthly_group_calls.outside_rank_a_call_count) AS outside_rank_a_call_count
		,SUM(monthly_group_calls.outside_rank_a_info_count) AS outside_rank_a_info_count
		,SUM(monthly_group_calls.outside_rank_b_plan_count) AS outside_rank_b_plan_count
		,SUM(monthly_group_calls.outside_rank_b_call_count) AS outside_rank_b_call_count
		,SUM(monthly_group_calls.outside_rank_b_info_count) AS outside_rank_b_info_count
		,SUM(monthly_group_calls.outside_rank_c_plan_count) AS outside_rank_c_plan_count
		,SUM(monthly_group_calls.outside_rank_c_call_count) AS outside_rank_c_call_count
		,SUM(monthly_group_calls.outside_rank_c_info_count) AS outside_rank_c_info_count
		,SUM(monthly_group_calls.outside_important_plan_count) AS outside_important_plan_count
		,SUM(monthly_group_calls.outside_important_call_count) AS outside_important_call_count
		,SUM(monthly_group_calls.outside_important_info_count) AS outside_important_info_count
		,SUM(monthly_group_calls.outside_new_plan_count) AS outside_new_plan_count
		,SUM(monthly_group_calls.outside_new_call_count) AS outside_new_call_count
		,SUM(monthly_group_calls.outside_new_info_count) AS outside_new_info_count
		,SUM(monthly_group_calls.outside_deep_plowing_plan_count) AS outside_deep_plowing_plan_count
		,SUM(monthly_group_calls.outside_deep_plowing_call_count) AS outside_deep_plowing_call_count
		,SUM(monthly_group_calls.outside_deep_plowing_info_count) AS outside_deep_plowing_info_count
		,SUM(monthly_group_calls.outside_y_cp_plan_count) AS outside_y_cp_plan_count
		,SUM(monthly_group_calls.outside_y_cp_call_count) AS outside_y_cp_call_count
		,SUM(monthly_group_calls.outside_y_cp_info_count) AS outside_y_cp_info_count
		,SUM(monthly_group_calls.outside_other_cp_plan_count) AS outside_other_cp_plan_count
		,SUM(monthly_group_calls.outside_other_cp_call_count) AS outside_other_cp_call_count
		,SUM(monthly_group_calls.outside_other_cp_info_count) AS outside_other_cp_info_count
		,SUM(monthly_group_calls.outside_group_one_plan_count) AS outside_group_one_plan_count
		,SUM(monthly_group_calls.outside_group_one_call_count) AS outside_group_one_call_count
		,SUM(monthly_group_calls.outside_group_one_info_count) AS outside_group_one_info_count
		,SUM(monthly_group_calls.outside_group_two_plan_count) AS outside_group_two_plan_count
		,SUM(monthly_group_calls.outside_group_two_call_count) AS outside_group_two_call_count
		,SUM(monthly_group_calls.outside_group_two_info_count) AS outside_group_two_info_count
		,SUM(monthly_group_calls.outside_group_three_plan_count) AS outside_group_three_plan_count
		,SUM(monthly_group_calls.outside_group_three_call_count) AS outside_group_three_call_count
		,SUM(monthly_group_calls.outside_group_three_info_count) AS outside_group_three_info_count
		,SUM(monthly_group_calls.outside_group_four_plan_count) AS outside_group_four_plan_count
		,SUM(monthly_group_calls.outside_group_four_call_count) AS outside_group_four_call_count
		,SUM(monthly_group_calls.outside_group_four_info_count) AS outside_group_four_info_count
		,SUM(monthly_group_calls.outside_group_five_plan_count) AS outside_group_five_plan_count
		,SUM(monthly_group_calls.outside_group_five_call_count) AS outside_group_five_call_count
		,SUM(monthly_group_calls.outside_group_five_info_count) AS outside_group_five_info_count
    FROM
        monthly_group_calls
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_group_calls.year = /* year */1
        AND monthly_group_calls.month = /* month */1
        AND monthly_group_calls.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_group_calls.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_group_calls.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_group_calls.year
		,monthly_group_calls.month
