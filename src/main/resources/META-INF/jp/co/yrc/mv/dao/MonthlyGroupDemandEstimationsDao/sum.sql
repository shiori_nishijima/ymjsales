SELECT
        monthly_group_demand_estimations.year
        ,monthly_group_demand_estimations.month
		,SUM(monthly_group_demand_estimations.rank_a_year_demand_estimation)  AS rank_a_year_demand_estimation
		,SUM(monthly_group_demand_estimations.rank_b_year_demand_estimation)  AS rank_b_year_demand_estimation
		,SUM(monthly_group_demand_estimations.rank_c_year_demand_estimation)  AS rank_c_year_demand_estimation
		,SUM(monthly_group_demand_estimations.important_year_demand_estimation)  AS important_year_demand_estimation
		,SUM(monthly_group_demand_estimations.new_year_demand_estimation)  AS new_year_demand_estimation
		,SUM(monthly_group_demand_estimations.deep_plowing_year_demand_estimation)  AS deep_plowing_year_demand_estimation
		,SUM(monthly_group_demand_estimations.y_cp_year_demand_estimation)  AS y_cp_year_demand_estimation
		,SUM(monthly_group_demand_estimations.other_cp_year_demand_estimation)  AS other_cp_year_demand_estimation
		,SUM(monthly_group_demand_estimations.group_one_year_demand_estimation)  AS group_one_year_demand_estimation
		,SUM(monthly_group_demand_estimations.group_two_year_demand_estimation)  AS group_two_year_demand_estimation
		,SUM(monthly_group_demand_estimations.group_three_year_demand_estimation)  AS group_three_year_demand_estimation
		,SUM(monthly_group_demand_estimations.group_four_year_demand_estimation)  AS group_four_year_demand_estimation
		,SUM(monthly_group_demand_estimations.group_five_year_demand_estimation)  AS group_five_year_demand_estimation

    FROM
        monthly_group_demand_estimations
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_group_demand_estimations.year = /* year */1
        AND monthly_group_demand_estimations.month = /* month */1
        AND monthly_group_demand_estimations.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_group_demand_estimations.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_group_demand_estimations.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_group_demand_estimations.year
		,monthly_group_demand_estimations.month
