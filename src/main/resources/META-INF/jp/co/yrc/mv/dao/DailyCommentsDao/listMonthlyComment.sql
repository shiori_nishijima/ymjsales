SELECT
		daily_comments.id
		,daily_comments.year
		,daily_comments.month
		,daily_comments.date
		,daily_comments.user_id
		,daily_comments.user_comment
		,daily_comments.boss_comment
		,daily_comments.completed
		,daily_comments.date_entered
		,daily_comments.date_modified
		,daily_comments.modified_user_id
		,daily_comments.created_by
		,daily_comments.deleted
	FROM
		daily_comments
	WHERE
		daily_comments.year = /* year */'2014'
	AND
		daily_comments.month = /* month */'1'
	AND
		daily_comments.user_id = /* userId */'a'
	ORDER BY
		daily_comments.year
		,daily_comments.month
