select
  id,
  name,
  date_entered,
  date_modified,
  modified_user_id,
  created_by,
  description,
  deleted,
  assigned_user_id,
  class,
  cate,
  month_num,
  temp_num,
  line_num,
  yyyy,
  mm,
  billiard_form,
  billiard_model_code,
  billiard_maker,
  billiard_model,
  billiard_color,
  month_num_variable_profits,
  temp_num_variable_profits,
  posted_month_age,
  billiard_choice_day,
  billiard_memo,
  calling_agent,
  month_num_local_currency,
  month_num_unit,
  temp_num_local_currency,
  temp_num_unit,
  month_age
from
  sv_cancel
where
  id = /* id */'a'
