SELECT
  id,
  call_id,
  user_id,
  date_modified,
  deleted
FROM
  /*%if tech */tech_service_calls_like_users/*%else*/calls_like_users/*%end*/ calls_like_users
WHERE
  call_id = /* callId */'a'
  AND user_id = /*userId*/'a'
