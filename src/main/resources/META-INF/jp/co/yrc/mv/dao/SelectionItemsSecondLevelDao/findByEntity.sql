SELECT
    key_entity_name
    ,key_property_name
    ,key_value
    ,property_name
    ,value
    ,label
    ,sort_order
    ,deleted
    ,date_entered
    ,date_modified
    ,modified_user_id
    ,created_by
  FROM
    selection_items_second_level
  WHERE
/*%if entity.keyEntityName != null */
    key_entity_name =/* entity.keyEntityName */'a'
/*%end*/
/*%if entity.keyPropertyName != null */
    AND key_property_name = /* entity.keyPropertyName */'a'
/*%end*/
/*%if entity.keyValue != null */
    AND key_value = /* entity.keyValue */'a'
/*%end*/
/*%if entity.propertyName != null */
    AND property_name = /* entity.propertyName */'a'
/*%end*/
/*%if entity.value != null */
    AND value = /* entity.value */'a'
/*%end*/
    AND key_locale = /* keyLocale */'ja'
    ORDER BY
    sort_order ASC
    ,value ASC
  

