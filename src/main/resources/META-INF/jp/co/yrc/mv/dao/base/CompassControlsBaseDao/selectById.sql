select
  year,
  month,
  report_display_seq,
  report_display_name,
  plan_product_kind,
  plan_product_kind_name,
  compass_kind,
  compass_kind_name,
  deleted,
  date_entered,
  date_modified,
  modified_user_id,
  created_by
from
  compass_controls
where
  year = /* year */1
  and
  month = /* month */1
  and
  report_display_seq = /* reportDisplaySeq */1
  and
  compass_kind = /* compassKind */'a'
