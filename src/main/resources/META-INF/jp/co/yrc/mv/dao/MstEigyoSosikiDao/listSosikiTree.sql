SELECT
		mst_eigyo_sosiki.eigyo_sosiki_cd
		,mst_eigyo_sosiki.eigyo_sosiki_nm
		,mst_eigyo_sosiki.eigyo_sosiki_short_nm
		,mst_eigyo_sosiki.corp_cd
		,mst_eigyo_sosiki.div1_cd
		,mst_eigyo_sosiki.div2_cd
		,mst_eigyo_sosiki.div3_cd
		,mst_eigyo_sosiki.div4_cd
		,mst_eigyo_sosiki.div5_cd
		,mst_eigyo_sosiki.div6_cd
		,mst_eigyo_sosiki.div1_nm
		,mst_eigyo_sosiki.div2_nm
		,mst_eigyo_sosiki.div3_nm
		,mst_eigyo_sosiki.div4_nm
		,mst_eigyo_sosiki.div5_nm
		,mst_eigyo_sosiki.div6_nm
		,mst_eigyo_sosiki.deleted
		,mst_eigyo_sosiki.date_entered
		,mst_eigyo_sosiki.date_modified
		,mst_eigyo_sosiki.modified_user_id
		,mst_eigyo_sosiki.created_by
		,users.first_name
		,users.last_name
		,users.id AS user_id
	FROM
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
			LEFT OUTER JOIN 
				/*%if isHistory */
				(
				  SELECT
				      *
				    FROM
				      mst_tanto_s_sosiki_history
				    WHERE
				        year = /*year*/2015
				        AND month = /*month*/01
				) mst_tanto_s_sosiki
				/*%else*/
				mst_tanto_s_sosiki
				/*%end*/
				ON mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
			LEFT OUTER JOIN users
				ON users.id = mst_tanto_s_sosiki.tanto_cd
				/*%if !isHistory */
				AND users.deleted = 0
				/*%end*/
	WHERE
		mst_eigyo_sosiki.corp_cd in /* corpCodes */('a', 'b', 'c')
	ORDER BY 
		div1_cd
		,div2_cd
		,div3_cd
		,user_id