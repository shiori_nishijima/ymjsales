SELECT
        monthly_market_demand_estimations.year
        ,monthly_market_demand_estimations.month
		,SUM(monthly_market_demand_estimations.ss_year_demand_estimation) AS ss_year_demand_estimation
		,SUM(monthly_market_demand_estimations.rs_year_demand_estimation) AS rs_year_demand_estimation
		,SUM(monthly_market_demand_estimations.cd_year_demand_estimation) AS cd_year_demand_estimation
		,SUM(monthly_market_demand_estimations.ps_year_demand_estimation) AS ps_year_demand_estimation
		,SUM(monthly_market_demand_estimations.cs_year_demand_estimation) AS cs_year_demand_estimation
		,SUM(monthly_market_demand_estimations.hc_year_demand_estimation) AS hc_year_demand_estimation
		,SUM(monthly_market_demand_estimations.lease_year_demand_estimation) AS lease_year_demand_estimation
		,SUM(monthly_market_demand_estimations.indirect_other_year_demand_estimation) AS indirect_other_year_demand_estimation
		,SUM(monthly_market_demand_estimations.truck_year_demand_estimation) AS truck_year_demand_estimation
		,SUM(monthly_market_demand_estimations.bus_year_demand_estimation) AS bus_year_demand_estimation
		,SUM(monthly_market_demand_estimations.hire_taxi_year_demand_estimation) AS hire_taxi_year_demand_estimation
		,SUM(monthly_market_demand_estimations.direct_other_year_demand_estimation) AS direct_other_year_demand_estimation
		,SUM(monthly_market_demand_estimations.retail_year_demand_estimation) AS retail_year_demand_estimation
    FROM
        monthly_market_demand_estimations
        ,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
    WHERE
        monthly_market_demand_estimations.year = /* year */1
        AND monthly_market_demand_estimations.month = /* month */1
        AND monthly_market_demand_estimations.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
/*%if !div1.isEmpty() */
		AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
		/*%if userId != null*/
        AND monthly_market_demand_estimations.user_id = /* userId */'a'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
		/*%if userId != null*/
        AND monthly_market_demand_estimations.user_id = /* userId */'a'
		/*%end*/
/*%end*/
	GROUP BY
		monthly_market_demand_estimations.year
		,monthly_market_demand_estimations.month
