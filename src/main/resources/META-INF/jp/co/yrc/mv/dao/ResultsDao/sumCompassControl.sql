SELECT
    SUM(result.sales) AS sales
    ,SUM(result.margin) AS margin
    ,SUM(result.number) AS number
    ,result_control.report_display_seq
    ,result_control.report_display_name
    ,result_control.plan_product_kind
    ,result_control.plan_product_kind_name
  FROM
    (
      SELECT
          SUM(results.sales) AS sales
          ,SUM(results.margin) AS margin
          ,SUM(results.number) AS number
          ,compass_kind
        FROM
          results
          ,users
        WHERE
          results.year = /* year */2000
          AND results.month = /* month */1
          /*%if day != null*/
          AND results.date = /* day*/1
          /*%end*/
          AND users.id_for_customer = results.assigned_user_id
      /*%if !div1.isEmpty() */
          AND results.sales_company_code IN /* div1 */('a', 'aa')
        /*%if div1.size() == 1 */
          /*%if div2 != null */
          AND results.department_code = /* div2 */'a'
          /*%end*/
          /*%if div3 != null */
          AND results.sales_office_code = /* div3 */'a'
          /*%end*/
          /*%if userId != null */
          AND users.id = /* userId */'sfa'
          /*%end*/
        /*%end*/
      /*%end*/
        /*%if div1.isEmpty() */
          /*%if userId != null */
          AND users.id = /* userId */'sfa'
          /*%end*/
        /*%end*/
        GROUP BY compass_kind
    ) result
    ,(
      SELECT
          DISTINCT year
          ,month
          ,report_display_seq
          ,report_display_name
          ,plan_product_kind
          ,plan_product_kind_name
          ,compass_kind
        FROM
          compass_controls
        WHERE
          year = /* year */2000
          AND month = /* month */1
    ) result_control
  WHERE
      result.compass_kind = result_control.compass_kind
  GROUP BY
    result_control.report_display_seq
    ,result_control.report_display_name
    ,result_control.plan_product_kind
    ,result_control.plan_product_kind_name
  ORDER BY
    result_control.report_display_seq
    ,result_control.plan_product_kind