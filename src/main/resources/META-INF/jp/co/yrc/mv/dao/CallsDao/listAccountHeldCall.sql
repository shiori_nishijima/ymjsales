SELECT
		calls.id
		,calls.name
		,calls.assigned_user_id
		,calls.date_start
		,calls.date_end
		,calls.parent_type
		,calls.status
		,calls.parent_id
		,calls.info_div
		,calls.maker
		,calls.kind
		,calls.category
		,calls.sensitivity
		,calls.time_end_c
		,calls.com_free_c
		,calls.division_c
		,calls.is_plan
		,calls.planned_count
		,calls.held_count
		,calls.deleted
		,calls.date_entered
		,calls.date_modified
		,calls.modified_user_id
		,calls.created_by
    FROM
		calls
	WHERE
		calls.date_start >= /*from*/2000
		AND calls.date_start < /*to*/2000
		AND calls.parent_type = 'Accounts'
		AND calls.parent_id = /* accountId */'a'
		AND calls.assigned_user_id =  /* userId */'a'
		AND calls.status = 'Held'
		AND calls.deleted <> 1
UNION ALL
SELECT
		calls.id
		,calls.name
		,calls.assigned_user_id
		,calls.date_start
		,calls.date_end
		,calls.parent_type
		,calls.status
		,calls.parent_id
		,calls.info_div
		,calls.maker
		,calls.kind
		,calls.category
		,calls.sensitivity
		,calls.time_end_c
		,calls.com_free_c
		,calls.division_c
		,calls.is_plan
		,0 AS planned_count
		,calls.held_count
		,calls.deleted
		,calls.date_entered
		,calls.date_modified
		,calls.modified_user_id
		,calls.created_by
    FROM
		calls
		,calls_users
	WHERE
		calls.date_start >= /*from*/2000
		AND calls.date_start < /*to*/2000
		AND calls.parent_type = 'Accounts'
		AND calls.parent_id = /* accountId */'a'
		AND calls.status = 'Held'
		AND calls.id = calls_users.call_id
		AND calls_users.user_id =  /* userId */'a'
		AND calls.deleted <> 1