SELECT
	id
	,year
	,month
	,date
	,user_id
	,user_comment
	,boss_comment
	,completed
	,date_entered
	,date_modified
	,modified_user_id
	,created_by
	,deleted
FROM
	daily_comments
WHERE
	id = /* id */'a'
	AND deleted <> 1
