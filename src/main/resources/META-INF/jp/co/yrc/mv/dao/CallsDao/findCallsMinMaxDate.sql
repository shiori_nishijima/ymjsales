SELECT
        MAX(date_start) AS maxDate,
        MIN(date_start) AS minDate
    FROM
        calls
    WHERE
        deleted = 0
