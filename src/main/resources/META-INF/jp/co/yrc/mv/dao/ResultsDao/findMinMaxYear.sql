SELECT
        MAX(max) AS max
        ,MIN(min) AS min
FROM (
SELECT
        MAX(year) AS max
        ,MIN(year) AS min
    FROM
        results
    GROUP BY ''
UNION ALL
SELECT
        MAX(year) AS max
        ,MIN(year) AS min
    FROM
        plans
    GROUP BY ''
UNION ALL
SELECT
        DATE_FORMAT(MAX(date_start), '%Y') AS max
        ,DATE_FORMAT(MIN(date_start), '%Y') AS min
    FROM
        calls
    GROUP BY ''
) min_max