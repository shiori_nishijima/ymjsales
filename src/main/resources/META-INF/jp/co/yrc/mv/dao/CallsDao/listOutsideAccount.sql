SELECT
		*
	FROM
		(
SELECT DISTINCT
		accounts.id
		,accounts.name
		,accounts.name_kana
		,accounts.billing_address_postalcode
		,accounts.billing_address_state
		,accounts.billing_address_city
		,accounts.billing_address_street
		,accounts.parent_id
		,accounts.assigned_user_id
		,accounts.sales_company_code
		,accounts.department_code
		,accounts.sales_office_code
		,accounts.name_for_search
		,accounts.market_id
		,markets.name AS market_name
		,accounts.brand_id
		,accounts.own_count
		,accounts.deal_count
		,accounts.group_important
		,accounts.group_new
		,accounts.group_deep_plowing
		,accounts.group_y_cp
		,accounts.group_other_cp
		,accounts.group_one
		,accounts.group_two
		,accounts.group_three
		,accounts.group_four
		,accounts.group_five
		,accounts.sales_rank
		,accounts.demand_number
		,accounts.iss_yh
		,accounts.iss_bs
		,accounts.iss_dl
		,accounts.iss_ty
		,accounts.iss_gy
		,accounts.iss_mi
		,accounts.iss_pb
		,accounts.iss_other
		,accounts.undeal
		,accounts.deleted
    FROM
        calls
        	INNER JOIN
	/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
                                    /*%if div1 != null */
                                    AND sales_company_code = /* div1 */'a'
                                    /*%end*/
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
	/*%else*/
				accounts
	/*%end*/
            	ON calls.parent_type = 'Accounts'
            	AND calls.parent_id = accounts.id
            INNER JOIN users 
                ON users.id_for_customer = accounts.assigned_user_id 
                AND users.id <> calls.assigned_user_id
        	INNER JOIN markets
        		ON accounts.market_id = markets.id
WHERE
	calls.date_start >= /*from*/2000
	AND calls.date_start < /*to*/2000
	/*%if div1 != null */
	AND accounts.sales_company_code = /* div1 */'a'
	/*%end*/
	/*%if div2 != null */
	AND accounts.department_code = /* div2 */'a'
	/*%end*/
	/*%if div3 != null */
	AND accounts.sales_office_code = /* div3 */'a'
	/*%end*/
	/*%if userId != null */
	AND calls.assigned_user_id = /* userId */'sfa'
	/*%end*/
	AND calls.deleted <> 1
UNION
SELECT DISTINCT
		accounts.id
		,accounts.name
		,accounts.name_kana
		,accounts.billing_address_postalcode
		,accounts.billing_address_state
		,accounts.billing_address_city
		,accounts.billing_address_street
		,accounts.parent_id
		,accounts.assigned_user_id
		,accounts.sales_company_code
		,accounts.department_code
		,accounts.sales_office_code
		,accounts.name_for_search
		,accounts.market_id
		,markets.name AS market_name
		,accounts.brand_id
		,accounts.own_count
		,accounts.deal_count
		,accounts.group_important
		,accounts.group_new
		,accounts.group_deep_plowing
		,accounts.group_y_cp
		,accounts.group_other_cp
		,accounts.group_one
		,accounts.group_two
		,accounts.group_three
		,accounts.group_four
		,accounts.group_five
		,accounts.sales_rank
		,accounts.demand_number
		,accounts.iss_yh
		,accounts.iss_bs
		,accounts.iss_dl
		,accounts.iss_ty
		,accounts.iss_gy
		,accounts.iss_mi
		,accounts.iss_pb
		,accounts.iss_other
		,accounts.undeal
		,accounts.deleted
    FROM
        calls
        	INNER JOIN
	/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
                                    /*%if div1 != null */
                                    AND sales_company_code = /* div1 */'a'
                                    /*%end*/
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
	/*%else*/
	        	accounts
	/*%end*/
            	ON calls.parent_type = 'Accounts'
            	AND calls.parent_id = accounts.id
            INNER JOIN calls_users
            	ON calls.id = calls_users.call_id
        	INNER JOIN markets
        		ON accounts.market_id = markets.id
WHERE
	calls.date_start >= /*from*/2000
	AND calls.date_start < /*to*/2000
	/*%if div1 != null */
	AND accounts.sales_company_code = /* div1 */'a'
	/*%end*/
	/*%if div2 != null */
	AND accounts.department_code = /* div2 */'a'
	/*%end*/
	/*%if div3 != null */
	AND accounts.sales_office_code = /* div3 */'a'
	/*%end*/
	/*%if userId != null */
	AND calls_users.user_id = /* userId */'sfa'
	/*%end*/
	AND calls.deleted <> 1
) outside
ORDER BY
id ASC