SELECT
		user_info.id
		,user_info.id_for_customer
		,user_info.first_name
		,user_info.last_name
		,user_info.eigyo_sosiki_nm
		,IFNULL(user_info.plan_visit_num, 0) AS plan_visit_num
		,IFNULL(user_info.plan_info_num, 0) AS plan_info_num
		,IFNULL(user_info.plan_demand_coefficient, 0) AS plan_demand_coefficient
		,IFNULL(account_info.own_count, 0) AS own_count
		,IFNULL(account_info.deal_count, 0) AS deal_count
		,IFNULL(account_info.demand_number, 0) AS demand_number
		,IFNULL(result_info.sales, 0) AS sales
		,IFNULL(result_info.margin, 0) AS margin
		,IFNULL(result_info.number, 0) AS number
		,IFNULL(result_info.ss, 0) AS ss
		,IFNULL(result_info.rs, 0) AS rs
		,IFNULL(result_info.cd, 0) AS cd
		,IFNULL(result_info.ps, 0) AS ps
		,IFNULL(result_info.cs, 0) AS cs
		,IFNULL(result_info.hc, 0) AS hc
		,IFNULL(result_info.lease, 0) AS lease
		,IFNULL(result_info.indirect_other, 0) AS indirect_other
		,IFNULL(result_info.truck, 0) AS truck
		,IFNULL(result_info.bus, 0) AS bus
		,IFNULL(result_info.hire_taxi, 0) AS hire_taxi
		,IFNULL(result_info.direct_other, 0) AS direct_other
		,IFNULL(result_info.deal_result, 0) AS deal_result
		,IFNULL(plan_info.sales, 0) AS plan_sales
		,IFNULL(plan_info.margin, 0) AS plan_margin
		,IFNULL(plan_info.number, 0) AS plan_number
		,IFNULL(call_info.planned_count, 0) AS planned_count
		,IFNULL(call_info.call_count, 0) AS call_count
		,IFNULL(call_info.info_count, 0) AS info_count
		/*%for item : items */
		,IFNULL(division_c_/*# item.value */, 0) AS division_c_/*# item.value */
		/*%end*/
		,IFNULL(last_month_call_info.call_count, 0) AS last_month_call_count
	FROM

/** ユーザー情報絞り込み */
(
SELECT
		users.id
		,users.id_for_customer
		,users.first_name
		,users.last_name
		,mst_eigyo_sosiki.eigyo_sosiki_nm
		,mst_eigyo_sosiki.div1_cd
		,mst_eigyo_sosiki.div2_cd
		,mst_eigyo_sosiki.div3_cd 
		,monthly_comments.plan_visit_num
		,monthly_comments.plan_info_num
		,monthly_comments.plan_demand_coefficient
	FROM
		users
			LEFT OUTER JOIN monthly_comments
				ON monthly_comments.user_id = users.id
				AND monthly_comments.year = /* year */0
				AND monthly_comments.month = /* month */0
		,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_tanto_s_sosiki_history
		    WHERE
		        year = /*year*/2015
		        AND month = /*month*/01
		) mst_tanto_s_sosiki
		/*%else*/
		mst_tanto_s_sosiki
		/*%end*/
		,
		/*%if isHistory */
		(
		  SELECT
		      *
		    FROM
		      mst_eigyo_sosiki_history
		    WHERE
		       year = /*year*/2015
		       AND month = /*month*/01
		) mst_eigyo_sosiki
		/*%else*/
		mst_eigyo_sosiki
		/*%end*/
	WHERE
		users.id = mst_tanto_s_sosiki.tanto_cd
		AND mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
		/*%if div1 != null */
		AND mst_eigyo_sosiki.div1_cd = /* div1 */'a'
		/*%end*/
		/*%if div2 != null */
		AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
		/*%end*/
) user_info


LEFT OUTER JOIN
/** アカウントの情報集計 */
(
SELECT
		SUM(accounts.own_count) AS own_count
		,SUM(accounts.deal_count) AS deal_count
		,SUM(accounts.demand_number) AS demand_number
		,accounts.assigned_user_id
		,accounts.sales_company_code
		,accounts.department_code
		,accounts.sales_office_code
	FROM
/*%if isHistory */
		(
			SELECT
					*
				FROM
					accounts_history
						INNER JOIN (
							SELECT
									id AS current_id
									,MAX(yearmonth) AS current_yearmonth
								FROM
									accounts_history
								WHERE
									yearmonth <= /* yearMonth */2000
                                    /*%if div1 != null */
                                    AND sales_company_code = /* div1 */'a'
                                    /*%end*/
								GROUP BY current_id
						) current
							ON current.current_id = accounts_history.id
							AND current.current_yearmonth = accounts_history.yearmonth
		) accounts
/*%else*/
		accounts
/*%end*/
		WHERE
			/*%if div1 != null */
			accounts.sales_company_code = /* div1 */'a'
			/*%end*/
			/*%if div2 != null */
			AND accounts.department_code = /* div2 */'a'
			/*%end*/
			/*%if div3 != null */
			AND accounts.sales_office_code = /* div3 */'a'
			/*%end*/
	GROUP BY
		accounts.assigned_user_id
		,accounts.sales_company_code
		,accounts.department_code
		,accounts.sales_office_code
) account_info
		ON user_info.id_for_customer = account_info.assigned_user_id
		AND user_info.div1_cd = account_info.sales_company_code
		AND user_info.div2_cd = account_info.department_code
		AND user_info.div3_cd = account_info.sales_office_code

LEFT OUTER JOIN
/** 実績の集計 */
(
SELECT
		SUM(sales) AS sales
		,SUM(margin) AS margin
		,SUM(number) AS number
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511H'
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS ss
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511L' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS rs
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511N' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS cd
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511J' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS ps
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511P' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS cs
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511S' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS hc
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511X' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS lease
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511R' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS indirect_other
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511C' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS truck
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511E' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS bus
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511G' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS hire_taxi
        ,SUM(CASE
            WHEN sales > 0
            AND market_id = 'Z2511F' 
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS direct_other
        ,SUM(CASE
            WHEN sales > 0
            AND deal_count = 1 THEN 1
            ELSE 0
        END) AS deal_result
		,assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
	FROM (
		SELECT
				SUM(results.sales) AS sales
				,SUM(results.margin) AS margin
				,SUM(
					CASE
						WHEN
							/** TIFU */
							compass_kinds.middle_class = '91'
							/** OTHER */
							OR compass_kinds.middle_class = '92'
							OR compass_kinds.middle_class = '93'
							OR compass_kinds.middle_class = '94'
							OR compass_kinds.middle_class = '95'
							/** WORK */
							OR compass_kinds.middle_class = '96'
							OR compass_kinds.middle_class = '97'
							/** RETREAD */
							OR compass_kinds.middle_class = 'A1'
							OR compass_kinds.middle_class = 'A2'
							OR compass_kinds.middle_class = 'A3'
							OR compass_kinds.middle_class = 'B1'
							OR compass_kinds.middle_class = 'B2'
						THEN 0 
					ELSE results.number END
				) AS number
				,accounts.id
				,accounts.deal_count
				,accounts.market_id 
				,accounts.assigned_user_id
				,accounts.sales_company_code
				,accounts.department_code
				,accounts.sales_office_code
			FROM
				(
					SELECT
							SUM(results.sales) AS sales
							,SUM(results.margin) AS margin
							,SUM(results.number) AS number
							,results.account_id 
							,results.compass_kind
						FROM
							results
							,
					/*%if isHistory */
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
					/*%else*/
							accounts
					/*%end*/
						WHERE
							results.year = /* year */2000
							AND results.month = /* month */1
							AND results.account_id = accounts.id
							/*%if div1 != null */
							AND accounts.sales_company_code = /* div1 */'a'
							/*%end*/
							/*%if div2 != null */
							AND accounts.department_code = /* div2 */'a'
							/*%end*/
							/*%if div3 != null */
							AND accounts.sales_office_code = /* div3 */'a'
							/*%end*/
						GROUP BY
							results.account_id 
							,results.compass_kind
				) results
				,compass_kinds
				,
		/*%if isHistory */
				(
					SELECT
							*
						FROM
							accounts_history
								INNER JOIN (
									SELECT
											id AS current_id
											,MAX(yearmonth) AS current_yearmonth
										FROM
											accounts_history
										WHERE
											yearmonth <= /* yearMonth */2000
                                            /*%if div1 != null */
                                            AND sales_company_code = /* div1 */'a'
                                            /*%end*/
										GROUP BY current_id
								) current
									ON current.current_id = accounts_history.id
									AND current.current_yearmonth = accounts_history.yearmonth
				) accounts
		/*%else*/
				accounts
		/*%end*/
			WHERE
				results.account_id = accounts.id
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND compass_kinds.id = results.compass_kind
				AND (
					compass_kinds.middle_class = '11'
					OR compass_kinds.middle_class = '12' 
					OR compass_kinds.middle_class = '13' 
					OR compass_kinds.middle_class = '21' 
					OR compass_kinds.middle_class = '22' 
					OR compass_kinds.middle_class = '51' 
					OR compass_kinds.middle_class = '61' 
					OR compass_kinds.middle_class = '62' 
					OR compass_kinds.middle_class = '71' 
					OR compass_kinds.middle_class = '72' 
					/** OR */
					OR (
						compass_kinds.middle_class = '41'
						AND compass_kinds.small_class = '50'
					)
					/** ID */
					OR (
						compass_kinds.middle_class = '41'
						AND compass_kinds.small_class = '55'
					)
					/** TIFU */
					OR compass_kinds.middle_class = '91'
					/** OTHER */
					OR compass_kinds.middle_class = '92'
					OR compass_kinds.middle_class = '93'
					OR compass_kinds.middle_class = '94'
					OR compass_kinds.middle_class = '95'
					/** WORK */
					OR compass_kinds.middle_class = '96'
					OR compass_kinds.middle_class = '97'
					/** RETREAD */
					OR compass_kinds.middle_class = 'A1'
					OR compass_kinds.middle_class = 'A2'
					OR compass_kinds.middle_class = 'A3'
					OR compass_kinds.middle_class = 'B1'
					OR compass_kinds.middle_class = 'B2'
				)
			GROUP BY
				accounts.id
				,accounts.market_id 
				,accounts.deal_count
				,accounts.assigned_user_id
				,accounts.sales_company_code
				,accounts.department_code
				,accounts.sales_office_code
		) result
	GROUP BY
		assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
) result_info
		ON user_info.id_for_customer = result_info.assigned_user_id
		AND user_info.div1_cd = result_info.sales_company_code
		AND user_info.div2_cd = result_info.department_code
		AND user_info.div3_cd = result_info.sales_office_code


LEFT OUTER JOIN
/** 計画の集計 */
(
SELECT
		SUM(plans.sales) AS sales
		,SUM(plans.margin) AS margin
		,SUM(
			CASE
				WHEN 				
					/** TIFU */
					plans.middle_class = '91'
					/** OTHER */
					OR plans.middle_class = '92'
					OR plans.middle_class = '93'
					OR plans.middle_class = '94'
					OR plans.middle_class = '95'
					/** WORK */
					OR plans.middle_class = '96'
					OR plans.middle_class = '97'
					/** RETREAD */
					OR plans.middle_class = 'A1'
					OR plans.middle_class = 'A2'
					OR plans.middle_class = 'A3'
					OR plans.middle_class = 'B1'
					OR plans.middle_class = 'B2'
				THEN 0 
			ELSE plans.number END
		) AS number
		,sales_company_code
		,department_code
		,sales_office_code
		,assigned_user_id
	FROM
		plans
	WHERE
		plans.year = /* year */2000
		AND plans.month = /* month */1
		/*%if div1 != null */
		AND plans.sales_company_code = /* div1 */'a'
		/*%end*/
		/*%if div2 != null */
		AND plans.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND plans.sales_office_code = /* div3 */'a'
		/*%end*/
		AND (
			plans.middle_class = '11'
			OR plans.middle_class = '12' 
			OR plans.middle_class = '13' 
			OR plans.middle_class = '21' 
			OR plans.middle_class = '22' 
			OR plans.middle_class = '51' 
			OR plans.middle_class = '61' 
			OR plans.middle_class = '62' 
			OR plans.middle_class = '71' 
			OR plans.middle_class = '72' 
			OR plans.middle_class = '41' 
			OR plans.middle_class = '91' 
			/** OTHER */
			OR plans.middle_class = '92'
			OR plans.middle_class = '93'
			OR plans.middle_class = '94'
			OR plans.middle_class = '95'
			/** WORK */
			OR plans.middle_class = '96'
			OR plans.middle_class = '97'
            /** RETREAD */
            OR plans.middle_class = 'A1'
            OR plans.middle_class = 'A2'
            OR plans.middle_class = 'A3'
            OR plans.middle_class = 'B1'
            OR plans.middle_class = 'B2'
		)
	GROUP BY
		sales_company_code
		,department_code
		,sales_office_code
		,assigned_user_id
) plan_info
		ON user_info.id_for_customer = plan_info.assigned_user_id
		AND user_info.div1_cd = plan_info.sales_company_code
		AND user_info.div2_cd = plan_info.department_code
		AND user_info.div3_cd = plan_info.sales_office_code


LEFT OUTER JOIN
/** 訪問の集計 */
(
SELECT
		SUM(calls.planned_count) AS planned_count
		,SUM(calls.held_count) AS call_count
		,SUM(calls.info_count) AS info_count
		/*%for item : items */
		,SUM(division_c_/*# item.value */) AS division_c_/*# item.value */
		/*%end*/
		,assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
	FROM
		(
			SELECT
					calls.assigned_user_id AS assigned_user_id
					,CASE WHEN
						calls.com_free_c IS NOT NULL AND calls.com_free_c <> ''
						OR market_infos.description IS NOT NULL AND market_infos.description <> '' THEN 1 
						ELSE 0 
					END AS info_count
					,calls.planned_count
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				/*%for item : items */
					,CASE WHEN calls.status = 'Held' AND calls.division_c LIKE /* @infix(item.value) */'01' THEN 1 ELSE 0 END AS division_c_/*# item.value */
				/*%end*/
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
				FROM
					calls
						INNER JOIN
				/*%if isHistory */
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
				/*%else*/
							accounts
				/*%end*/
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
					LEFT OUTER JOIN market_infos
						ON calls.id = market_infos.call_id
			WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		UNION ALL
			SELECT
					calls_users.user_id AS assigned_user_id
					,CASE WHEN
						calls.com_free_c IS NOT NULL AND calls.com_free_c <> ''
						OR market_infos.description IS NOT NULL AND market_infos.description <> '' THEN 1 
						ELSE 0 
					END AS info_count
					,calls.planned_count
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
				/*%for item : items */
					,CASE WHEN calls.status = 'Held' AND calls.division_c LIKE /* @infix(item.value) */'01' THEN 1 ELSE 0 END AS division_c_/*# item.value */
				/*%end*/
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
				FROM
					calls
						INNER JOIN
				/*%if isHistory */
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
				/*%else*/
							accounts
				/*%end*/
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
						INNER JOIN calls_users
							ON calls.id = calls_users.call_id
					LEFT OUTER JOIN market_infos
						ON calls.id = market_infos.call_id
			WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		) calls
	GROUP BY
		assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
) call_info
		ON user_info.id = call_info.assigned_user_id
		AND user_info.div1_cd = call_info.sales_company_code
		AND user_info.div2_cd = call_info.department_code
		AND user_info.div3_cd = call_info.sales_office_code



LEFT OUTER JOIN
/** 前月訪問の集計 */
(
SELECT
		SUM(calls.held_count) AS call_count
		,assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
	FROM
		(
			SELECT
					calls.assigned_user_id AS assigned_user_id
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
				FROM
					calls
						INNER JOIN
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearLastMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
			WHERE
				calls.date_start >= /*lastMonthFrom*/2000
				AND calls.date_start < /*lastMonthTo*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		UNION ALL
			SELECT
					calls_users.user_id AS assigned_user_id
					,CASE calls.status WHEN 'Held' THEN calls.held_count ELSE 0 END AS held_count
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
				FROM
					calls
						INNER JOIN
							(
								SELECT
										*
									FROM
										accounts_history
											INNER JOIN (
												SELECT
														id AS current_id
														,MAX(yearmonth) AS current_yearmonth
													FROM
														accounts_history
													WHERE
														yearmonth <= /* yearLastMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
													GROUP BY current_id
											) current
												ON current.current_id = accounts_history.id
												AND current.current_yearmonth = accounts_history.yearmonth
							) accounts
							ON calls.parent_type = 'Accounts'
							AND calls.parent_id = accounts.id
						INNER JOIN calls_users
							ON calls.id = calls_users.call_id
			WHERE
				calls.date_start >= /*lastMonthFrom*/2000
				AND calls.date_start < /*lastMonthTo*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				AND calls.deleted <> 1
		) calls
	GROUP BY
		assigned_user_id
		,sales_company_code
		,department_code
		,sales_office_code
) last_month_call_info
		ON user_info.id = last_month_call_info.assigned_user_id
		AND user_info.div1_cd = last_month_call_info.sales_company_code
		AND user_info.div2_cd = last_month_call_info.department_code
		AND user_info.div3_cd = last_month_call_info.sales_office_code
	ORDER BY
		user_info.div1_cd ASC
		,user_info.div2_cd ASC
		,user_info.div3_cd ASC
		,user_info.id ASC