SELECT
        COUNT(CASE WHEN accounts.sales_rank = 'A' THEN own_accounts.id ELSE NULL END) AS rank_a_own_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'B' THEN own_accounts.id ELSE NULL END) AS rank_b_own_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'C' THEN own_accounts.id ELSE NULL END) AS rank_c_own_count
        ,COUNT(CASE WHEN accounts.group_important = '1' THEN own_accounts.id ELSE NULL END) AS important_own_count
        ,COUNT(CASE WHEN accounts.group_new = '1' THEN own_accounts.id ELSE NULL END) AS new_own_count
        ,COUNT(CASE WHEN accounts.group_deep_plowing = '1' THEN own_accounts.id ELSE NULL END) AS deep_plowing_own_count
        ,COUNT(CASE WHEN accounts.group_y_cp = '1' THEN own_accounts.id ELSE NULL END) AS y_cp_own_count
        ,COUNT(CASE WHEN accounts.group_other_cp = '1' THEN own_accounts.id ELSE NULL END) AS other_cp_own_count
        ,COUNT(CASE WHEN accounts.group_one = '1' THEN own_accounts.id ELSE NULL END) AS group_one_own_count
        ,COUNT(CASE WHEN accounts.group_two = '1' THEN own_accounts.id ELSE NULL END) AS group_two_own_count
        ,COUNT(CASE WHEN accounts.group_three = '1' THEN own_accounts.id ELSE NULL END) AS group_three_own_count
        ,COUNT(CASE WHEN accounts.group_four = '1' THEN own_accounts.id ELSE NULL END) AS group_four_own_count
        ,COUNT(CASE WHEN accounts.group_five = '1' THEN own_accounts.id ELSE NULL END) AS group_five_own_count

        ,COUNT(CASE WHEN accounts.sales_rank = 'A' THEN deal_accounts_plan.id ELSE NULL END) AS rank_a_deal_plan_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'B' THEN deal_accounts_plan.id ELSE NULL END) AS rank_b_deal_plan_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'C' THEN deal_accounts_plan.id ELSE NULL END) AS rank_c_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_important = '1' THEN deal_accounts_plan.id ELSE NULL END) AS important_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_new = '1' THEN deal_accounts_plan.id ELSE NULL END) AS new_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_deep_plowing = '1' THEN deal_accounts_plan.id ELSE NULL END) AS deep_plowing_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_y_cp = '1' THEN deal_accounts_plan.id ELSE NULL END) AS y_cp_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_other_cp = '1' THEN deal_accounts_plan.id ELSE NULL END) AS other_cp_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_one = '1' THEN deal_accounts_plan.id ELSE NULL END) AS group_one_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_two = '1' THEN deal_accounts_plan.id ELSE NULL END) AS group_two_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_three = '1' THEN deal_accounts_plan.id ELSE NULL END) AS group_three_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_four = '1' THEN deal_accounts_plan.id ELSE NULL END) AS group_four_deal_plan_count
        ,COUNT(CASE WHEN accounts.group_five = '1' THEN deal_accounts_plan.id ELSE NULL END) AS group_five_deal_plan_count
        
        ,COUNT(CASE WHEN accounts.sales_rank = 'A' THEN deal_accounts_result.id ELSE NULL END) AS rank_a_deal_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'B' THEN deal_accounts_result.id ELSE NULL END) AS rank_b_deal_count
        ,COUNT(CASE WHEN accounts.sales_rank = 'C' THEN deal_accounts_result.id ELSE NULL END) AS rank_c_deal_count
        ,COUNT(CASE WHEN accounts.group_important = '1' THEN deal_accounts_result.id ELSE NULL END) AS important_deal_count
        ,COUNT(CASE WHEN accounts.group_new = '1' THEN deal_accounts_result.id ELSE NULL END) AS new_deal_count
        ,COUNT(CASE WHEN accounts.group_deep_plowing = '1' THEN deal_accounts_result.id ELSE NULL END) AS deep_plowing_deal_count
        ,COUNT(CASE WHEN accounts.group_y_cp = '1' THEN deal_accounts_result.id ELSE NULL END) AS y_cp_deal_count
        ,COUNT(CASE WHEN accounts.group_other_cp = '1' THEN deal_accounts_result.id ELSE NULL END) AS other_cp_deal_count
        ,COUNT(CASE WHEN accounts.group_one = '1' THEN deal_accounts_result.id ELSE NULL END) AS group_one_deal_count
        ,COUNT(CASE WHEN accounts.group_two = '1' THEN deal_accounts_result.id ELSE NULL END) AS group_two_deal_count
        ,COUNT(CASE WHEN accounts.group_three = '1' THEN deal_accounts_result.id ELSE NULL END) AS group_three_deal_count
        ,COUNT(CASE WHEN accounts.group_four = '1' THEN deal_accounts_result.id ELSE NULL END) AS group_four_deal_count
        ,COUNT(CASE WHEN accounts.group_five = '1' THEN deal_accounts_result.id ELSE NULL END) AS group_five_deal_count
        FROM
/*%if isHistory */
        (
            SELECT
					*
                FROM
                    accounts_history
                        INNER JOIN (
                        	SELECT
                        			id AS current_id
                        			,MAX(yearmonth) AS current_yearmonth
                        		FROM
                        			accounts_history
                        		WHERE
                        			yearmonth <= /* yearMonth */2000
									/*%if !div1.isEmpty() */
							        AND sales_company_code IN /* div1 */('a', 'aa')
									/*%end*/
		                        GROUP BY current_id
                        ) current
                            ON current.current_id = accounts_history.id
                            AND current.current_yearmonth = accounts_history.yearmonth
        ) accounts
/*%else*/
    	accounts
/*%end*/
            LEFT OUTER JOIN (
                SELECT
                        accounts.id
                    FROM
						(
				            SELECT
				            		*
				            	FROM
							/*%if isHistory */
							        (
							            SELECT
												*
							                FROM
							                    accounts_history
							                        INNER JOIN (
							                        	SELECT
							                        			id AS current_id
							                        			,MAX(yearmonth) AS current_yearmonth
							                        		FROM
							                        			accounts_history
							                        		WHERE
							                        			yearmonth <= /* yearMonth */2000
																/*%if !div1.isEmpty() */
														        AND sales_company_code IN /* div1 */('a', 'aa')
																/*%end*/
									                        GROUP BY current_id
							                        ) current
							                            ON current.current_id = accounts_history.id
							                            AND current.current_yearmonth = accounts_history.yearmonth
							        ) accounts
							/*%else*/
							    	accounts
							/*%end*/
							    WHERE
							/*%if !div1.isEmpty() */
							        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
							    /*%if div1.size() == 1 */
							        /*%if div2 != null */
							        AND accounts.department_code = /* div2 */'a'
							        /*%end*/
							        /*%if div3 != null */
							        AND accounts.sales_office_code = /* div3 */'a'
							        /*%end*/
							    /*%end*/
							/*%end*/
						) accounts
                    WHERE
                        accounts.own_count = 1
                        AND accounts.deleted <> 1
            ) own_accounts
                ON accounts.id = own_accounts.id
            LEFT OUTER JOIN (
                SELECT
                        DISTINCT id
                    FROM
						(
				            SELECT
				            		*
				            	FROM
							/*%if isHistory */
							        (
							            SELECT
												*
							                FROM
							                    accounts_history
							                        INNER JOIN (
							                        	SELECT
							                        			id AS current_id
							                        			,MAX(yearmonth) AS current_yearmonth
							                        		FROM
							                        			accounts_history
							                        		WHERE
							                        			yearmonth <= /* yearMonth */2000
																/*%if !div1.isEmpty() */
														        AND sales_company_code IN /* div1 */('a', 'aa')
																/*%end*/
									                        GROUP BY current_id
							                        ) current
							                            ON current.current_id = accounts_history.id
							                            AND current.current_yearmonth = accounts_history.yearmonth
							        ) accounts
							/*%else*/
							    	accounts
							/*%end*/
							    WHERE
							/*%if !div1.isEmpty() */
							        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
							    /*%if div1.size() == 1 */
							        /*%if div2 != null */
							        AND accounts.department_code = /* div2 */'a'
							        /*%end*/
							        /*%if div3 != null */
							        AND accounts.sales_office_code = /* div3 */'a'
							        /*%end*/
							    /*%end*/
							/*%end*/
						) accounts
                    WHERE
                        accounts.deal_count = 1
                        AND accounts.deleted <> 1
            ) deal_accounts_plan
                ON accounts.id = deal_accounts_plan.id
            LEFT OUTER JOIN (
                SELECT
                        DISTINCT id
                    FROM
						(
				            SELECT
				            		*
				            	FROM
							/*%if isHistory */
							        (
							            SELECT
												*
							                FROM
							                    accounts_history
							                        INNER JOIN (
							                        	SELECT
							                        			id AS current_id
							                        			,MAX(yearmonth) AS current_yearmonth
							                        		FROM
							                        			accounts_history
							                        		WHERE
							                        			yearmonth <= /* yearMonth */2000
																/*%if !div1.isEmpty() */
														        AND sales_company_code IN /* div1 */('a', 'aa')
																/*%end*/
									                        GROUP BY current_id
							                        ) current
							                            ON current.current_id = accounts_history.id
							                            AND current.current_yearmonth = accounts_history.yearmonth
							        ) accounts
							/*%else*/
							    	accounts
							/*%end*/
							    WHERE
							/*%if !div1.isEmpty() */
							        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
							    /*%if div1.size() == 1 */
							        /*%if div2 != null */
							        AND accounts.department_code = /* div2 */'a'
							        /*%end*/
							        /*%if div3 != null */
							        AND accounts.sales_office_code = /* div3 */'a'
							        /*%end*/
							    /*%end*/
							/*%end*/
						) accounts
						INNER JOIN (
						SELECT	
								DISTINCT results.account_id
							FROM
								(
									SELECT
											results.account_id
											,results.compass_kind
											,SUM(results.sales) AS sales
										FROM
											results
									    	,
									/*%if isHistory */
									        (
									            SELECT
														*
									                FROM
									                    accounts_history
									                        INNER JOIN (
									                        	SELECT
									                        			id AS current_id
									                        			,MAX(yearmonth) AS current_yearmonth
									                        		FROM
									                        			accounts_history
									                        		WHERE
									                        			yearmonth <= /* yearMonth */2000
																		/*%if !div1.isEmpty() */
																        AND sales_company_code IN /* div1 */('a', 'aa')
																		/*%end*/
											                        GROUP BY current_id
									                        ) current
									                            ON current.current_id = accounts_history.id
									                            AND current.current_yearmonth = accounts_history.yearmonth
									        ) accounts
									/*%else*/
									    	accounts
									/*%end*/
									    	,users
										WHERE
											results.year = /* year */2015
											AND results.month = /* month */10
									        AND results.account_id = accounts.id
									        AND users.id_for_customer = accounts.assigned_user_id
									/*%if !div1.isEmpty() */
									        AND accounts.sales_company_code IN /* div1 */('a', 'aa')
									    /*%if div1.size() == 1 */
									        /*%if div2 != null */
									        AND accounts.department_code = /* div2 */'a'
									        /*%end*/
									        /*%if div3 != null */
									        AND accounts.sales_office_code = /* div3 */'a'
									        /*%end*/
									        /*%if userId != null */
									        AND users.id = /* userId */'sfa'
									        /*%end*/
									    /*%end*/
									/*%end*/
									/*%if div1.isEmpty() */
									        /*%if userId != null */
									        AND users.id = /* userId */'sfa'
									        /*%end*/
									/*%end*/
										GROUP BY
											results.account_id
											,results.compass_kind
								) results
								,compass_kinds
							WHERE
								compass_kinds.id = results.compass_kind
								AND results.sales > 0
								AND (
									compass_kinds.middle_class = '11'
									OR compass_kinds.middle_class = '12'
									OR compass_kinds.middle_class = '13'
									OR compass_kinds.middle_class = '21'
									OR compass_kinds.middle_class = '22'
									OR compass_kinds.middle_class = '51'
									OR compass_kinds.middle_class = '61'
									OR compass_kinds.middle_class = '62'
									OR compass_kinds.middle_class = '71'
									OR compass_kinds.middle_class = '72'
									/** OR */
									OR (
										compass_kinds.middle_class = '41'
										AND compass_kinds.small_class = '50'
									)
									/** ID */
									OR (
										compass_kinds.middle_class = '41'
										AND compass_kinds.small_class = '55'
									)
									/** TIFU */
									OR compass_kinds.middle_class = '91'
									/** OTHER */
									OR compass_kinds.middle_class = '92'
									OR compass_kinds.middle_class = '93'
									OR compass_kinds.middle_class = '94'
									OR compass_kinds.middle_class = '95'
									/** WORK */
									OR compass_kinds.middle_class = '96'
									OR compass_kinds.middle_class = '97'
									/** RETREAD */
									OR compass_kinds.middle_class = 'A1'
									OR compass_kinds.middle_class = 'A2'
									OR compass_kinds.middle_class = 'A3'
									OR compass_kinds.middle_class = 'B1'
									OR compass_kinds.middle_class = 'B2'
								)
						) results
							ON accounts.id = results.account_id
                    WHERE
                        accounts.deal_count = 1
                        AND accounts.deleted <> 1
            ) deal_accounts_result
                ON accounts.id = deal_accounts_result.id
            INNER JOIN users
            	ON users.id_for_customer = accounts.assigned_user_id
    WHERE
/*%if !div1.isEmpty() */
        accounts.sales_company_code IN /* div1 */('a', 'aa')
    /*%if div1.size() == 1 */
        /*%if div2 != null */
        AND accounts.department_code = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
        AND accounts.sales_office_code = /* div3 */'a'
        /*%end*/
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
    /*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
        AND accounts.deleted <> 1