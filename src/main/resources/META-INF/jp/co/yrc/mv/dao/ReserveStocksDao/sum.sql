SELECT
		'PCR_SUMMER' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '11'
    GROUP BY
		type
UNION ALL
SELECT
		'VAN_SUMMER' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '12'
    GROUP BY
		type
UNION ALL
SELECT
		'LT_SUMMER' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '21'
    GROUP BY
		type
UNION ALL
SELECT
		'TB_SUMMER' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '22'
    GROUP BY
		type
UNION ALL
SELECT
		'PCR_PURE_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '51'
    GROUP BY
		type
UNION ALL
SELECT
		'VAN_PURE_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '61'
    GROUP BY
		type
UNION ALL
SELECT
		'LT_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '71'
    GROUP BY
		type
UNION ALL
SELECT
		'LT_AS' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '71'
		AND products.snow_div = 'Z'
    GROUP BY
		type
UNION ALL
SELECT
		'LT_PURE_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '71'
		AND products.snow_div <> 'Z'
    GROUP BY
		type
UNION ALL
SELECT
		'TB_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '72'
    GROUP BY
		type
UNION ALL
SELECT
		'TB_AS' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.deleted = '0'
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '72'
		AND products.snow_div = 'Z'
    GROUP BY
		type
UNION ALL
SELECT
		'TB_PURE_SNOW' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.deleted = '0'
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '72'
		AND products.snow_div <> 'Z'
    GROUP BY
		type
UNION ALL
SELECT
		'ORID' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '41'
    GROUP BY
		type
UNION ALL
SELECT
		'OR' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.deleted = '0'
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '41'
		AND products.small_class = '50'
    GROUP BY
		type
UNION ALL
SELECT
		'ID' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '41'
		AND products.small_class = '55'
    GROUP BY
		type
UNION ALL
SELECT
		'TIFU' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.deleted = '0'
		AND reserve_stocks.art_no = products.id
		AND products.deleted = '0'
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND products.middle_class = '91'
    GROUP BY
		type
UNION ALL
SELECT
		'OTHER' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND (
			products.middle_class = '94'
			OR products.middle_class = '95'
		)
    GROUP BY
		type
UNION ALL
SELECT
		'WORK' AS type
		,SUM(reserve_stocks.number) AS number
	FROM
        reserve_stocks
        ,accounts
        ,products
        ,users
	WHERE
		reserve_stocks.account_id = accounts.id
		AND reserve_stocks.art_no = products.id
		AND users.id_for_customer = accounts.assigned_user_id
/*%if !div1.isEmpty() */
		AND accounts.sales_company_code IN /* div1 */('a', 'aa')
	/*%if div1.size() == 1 */
		/*%if div2 != null */
		AND accounts.department_code = /* div2 */'a'
		/*%end*/
		/*%if div3 != null */
		AND accounts.sales_office_code = /* div3 */'a'
		/*%end*/
		/*%if userId != null */
		AND users.id = /* userId */'sfa'
		/*%end*/
	/*%end*/
/*%end*/
/*%if div1.isEmpty() */
        /*%if userId != null */
        AND users.id = /* userId */'sfa'
        /*%end*/
/*%end*/
		AND (
			products.middle_class = '96'
			OR products.middle_class = '97'
		)
    GROUP BY
		type
