SELECT
    log_infos.id
    ,log_infos.level
    ,log_infos.user_id
    ,login_timestamp
    ,log_infos.event_timestamp
    ,log_infos.description
    ,log_infos.user_agent
    ,log_infos.date_entered
    ,user_info.eigyo_sosiki_cd
    ,CASE WHEN 
        CONCAT(TRIM(IFNULL(user_info.div2_nm, '')), TRIM(IFNULL(user_info.div3_nm, ''))) = '' 
        THEN user_info.eigyo_sosiki_nm 
        ELSE CONCAT(TRIM(IFNULL(user_info.div2_nm, '')), TRIM(IFNULL(user_info.div3_nm, ''))) 
    END AS eigyo_sosiki_nm
    ,user_info.div1
    ,user_info.div2
    ,user_info.div3
    ,user_info.div1_nm
    ,user_info.div2_nm
    ,user_info.div3_nm
    ,user_info.first_name
    ,user_info.last_name
  FROM
    log_infos
    ,( 
      SELECT
          users.id
          ,users.first_name
          ,users.last_name
          ,mst_eigyo_sosiki.eigyo_sosiki_cd
          ,mst_eigyo_sosiki.eigyo_sosiki_nm
          ,mst_eigyo_sosiki.div1_cd AS div1
          ,mst_eigyo_sosiki.div2_cd AS div2
          ,mst_eigyo_sosiki.div3_cd AS div3
          ,mst_eigyo_sosiki.div1_nm
          ,mst_eigyo_sosiki.div2_nm
          ,mst_eigyo_sosiki.div3_nm
          ,/* todayYear */2016 AS year
          ,/* todayMonth */1 AS month
	      ,DATE_FORMAT(concat(/* todayYear */2016, '-', /* todayMonth */4, '-1'), '%Y%m') AS yearmonth
        FROM
          users
          ,mst_tanto_s_sosiki
          ,mst_eigyo_sosiki
        WHERE
          users.id = mst_tanto_s_sosiki.tanto_cd
          AND mst_tanto_s_sosiki.eigyo_sosiki_cd = mst_eigyo_sosiki.eigyo_sosiki_cd
        /*%if !div1.isEmpty() */
          AND mst_eigyo_sosiki.div1_cd IN /* div1 */('a', 'aa')
        /*%end*/
        /*%if div2 != null */
          AND mst_eigyo_sosiki.div2_cd = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
          AND mst_eigyo_sosiki.div3_cd = /* div3 */'a'
        /*%end*/
        /*%if userId != null*/
         AND users.id = /* userId */'a'
        /*%end*/
      UNION
      SELECT
          users.id
          ,users.first_name
          ,users.last_name
          ,mst_eigyo_sosiki_history.eigyo_sosiki_cd
          ,mst_eigyo_sosiki_history.eigyo_sosiki_nm
          ,mst_eigyo_sosiki_history.div1_cd AS div1
          ,mst_eigyo_sosiki_history.div2_cd AS div2
          ,mst_eigyo_sosiki_history.div3_cd AS div3
          ,mst_eigyo_sosiki_history.div1_nm
          ,mst_eigyo_sosiki_history.div2_nm
          ,mst_eigyo_sosiki_history.div3_nm
          ,mst_eigyo_sosiki_history.year
          ,mst_eigyo_sosiki_history.month
          ,DATE_FORMAT(concat(mst_eigyo_sosiki_history.year, '-', mst_eigyo_sosiki_history.month, '-1'), '%Y%m') AS yearmonth
        FROM
          users
          ,mst_tanto_s_sosiki_history
          ,mst_eigyo_sosiki_history
        WHERE
          mst_tanto_s_sosiki_history.year = mst_eigyo_sosiki_history.year
          AND mst_tanto_s_sosiki_history.month = mst_eigyo_sosiki_history.month
          AND (
       /*%for term : terms */
            OR (mst_tanto_s_sosiki_history.year = /* term.year */2016
         /*%if term.month != 0*/
             AND mst_tanto_s_sosiki_history.month = /* term.month */1
         /*%end */
             )  
       /*%end */
           )
          AND mst_tanto_s_sosiki_history.tanto_cd = users.id
          AND mst_eigyo_sosiki_history.eigyo_sosiki_cd = mst_tanto_s_sosiki_history.eigyo_sosiki_cd
        /*%if !div1.isEmpty() */
          AND mst_eigyo_sosiki_history.div1_cd IN /* div1 */('a', 'aa')
        /*%end*/
        /*%if div2 != null */
          AND mst_eigyo_sosiki_history.div2_cd = /* div2 */'a'
        /*%end*/
        /*%if div3 != null */
          AND mst_eigyo_sosiki_history.div3_cd = /* div3 */'a'
        /*%end*/
        /*%if userId != null*/
         AND users.id = /* userId */'a'
        /*%end*/
    ) user_info

  WHERE
    ( level = 'ACCESS' OR level = 'ACCESS_MV' )
    AND user_info.id = log_infos.user_id 
    AND user_info.yearmonth = log_infos.yearmonth 
  /*%if start != null */
    AND /* start */'2000/01/01' <= log_infos.event_timestamp
  /*%end*/
  /*%if end != null */
    AND log_infos.event_timestamp <= /* end */'2000/01/01'
  /*%end*/