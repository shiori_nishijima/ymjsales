SELECT
        calls.group_type
        ,calls.date_start
		,calls.sales_company_code
		,calls.department_code
		,calls.sales_office_code
        ,SUM(calls.planned_count) AS planned_count
        ,SUM(calls.held_count) AS call_count
        ,SUM(calls.info_count) AS info_count
    FROM
        (
            SELECT
                    calls.assigned_user_id AS group_type
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
                    ,DATE(calls.date_start) AS date_start
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,IF (calls.is_plan, calls.planned_count, 0) AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                FROM
                    calls
                    	INNER JOIN
				/*%if isHistory */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) accounts
				/*%else*/
							accounts
				/*%end*/
                        	ON calls.parent_type = 'Accounts'
                        	AND calls.parent_id = accounts.id
            WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				/*%if userId != null */
				AND calls.assigned_user_id = /* userId */'sfa'
				/*%end*/
                AND calls.deleted <> 1
            UNION ALL
            SELECT
                    calls_users.user_id AS group_type
					,accounts.sales_company_code
					,accounts.department_code
					,accounts.sales_office_code
                    ,DATE(calls.date_start) AS date_start
                    ,IF (calls.com_free_c IS NOT NULL AND calls.com_free_c <> '', 1, 0) AS info_count
                    ,IF (calls.is_plan, calls.planned_count, 0) AS planned_count
                    ,IF (calls.status = 'Held', calls.held_count, 0) AS held_count
                FROM
                    calls
                    	INNER JOIN
				/*%if isHistory */
					        (
					            SELECT
										*
					                FROM
					                    accounts_history
					                        INNER JOIN (
					                        	SELECT
					                        			id AS current_id
					                        			,MAX(yearmonth) AS current_yearmonth
					                        		FROM
					                        			accounts_history
					                        		WHERE
					                        			yearmonth <= /* yearMonth */2000
                                                        /*%if div1 != null */
                                                        AND sales_company_code = /* div1 */'a'
                                                        /*%end*/
							                        GROUP BY current_id
					                        ) current
					                            ON current.current_id = accounts_history.id
					                            AND current.current_yearmonth = accounts_history.yearmonth
					        ) accounts
				/*%else*/
				        	accounts
				/*%end*/
                        	ON calls.parent_type = 'Accounts'
                        	AND calls.parent_id = accounts.id
                        INNER JOIN calls_users
                        	ON calls.id = calls_users.call_id
            WHERE
				calls.date_start >= /*from*/2000
				AND calls.date_start < /*to*/2000
				/*%if div1 != null */
				AND accounts.sales_company_code = /* div1 */'a'
				/*%end*/
				/*%if div2 != null */
				AND accounts.department_code = /* div2 */'a'
				/*%end*/
				/*%if div3 != null */
				AND accounts.sales_office_code = /* div3 */'a'
				/*%end*/
				/*%if userId != null */
				AND calls_users.user_id = /* userId */'sfa'
				/*%end*/
				AND calls.deleted <> 1
        ) calls
    GROUP BY
        calls.group_type
		,calls.sales_company_code
		,calls.department_code
		,calls.sales_office_code
        ,calls.date_start
    ORDER BY
		calls.sales_company_code
		,calls.department_code
		,calls.sales_office_code
        ,calls.group_type
