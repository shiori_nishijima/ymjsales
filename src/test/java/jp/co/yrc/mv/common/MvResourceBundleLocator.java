package jp.co.yrc.mv.common;

import java.util.Locale;
import java.util.ResourceBundle;

import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;

/**
 * リソースバンドルロケータ。
 * Validatorのプロパティファイルを指定する際に利用する。
 *
 */
public class MvResourceBundleLocator extends PlatformResourceBundleLocator {
	
	protected MvResourceBundleControl control = new MvResourceBundleControl();

	private String bundleName;

	public MvResourceBundleLocator(String bundleName) {
		super(bundleName);
		this.bundleName = bundleName;
	}

	@Override
	public ResourceBundle getResourceBundle(Locale locale) {
		return ResourceBundle.getBundle(bundleName, LocaleContext.instance().getLocale(), control);
	}
}
