package jp.co.yrc.mv.common;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

public class TestUtils {
	/**
	 * 日付フォーマット
	 */
	public static final String DATE_FORMAT_DATE = "yyyy/MM/dd";

	/**
	 * 日付フォーマット
	 */
	public static final String DATE_FORMAT_DATETIME = "yyyy/MM/dd HH:mm:ss";

	/**
	 * 日付フォーマット
	 */
	public static final String DATE_FORMAT_TIME = "HH:mm";
	
	/**
	 * 文字列をDate型に変換
	 * 
	 * @param dateString
	 * @param format
	 * @return
	 */
	public static Date convertStringToDate(String dateString, String format) {
		Date date = null;
		try {
			date = new Date(new SimpleDateFormat(format).parse(dateString).getTime());
		} catch (NullPointerException e) {
		} catch (ParseException e) {
		}
		return date;
	}
	
	/**
	 * 文字列形式の日時をTimestamp型に変換します。 引数がNULLの場合、変換に失敗した場合、NULLを返却します。
	 *
	 * @param dateTimeString
	 * @return
	 */
	public static Timestamp convertStringToTimestamp(
			String dateTimeString) {
		return convertDateTimeStringToTimestamp(dateTimeString,
			DATE_FORMAT_DATETIME);
	}
	
	public static Timestamp convertDateTimeStringToTimestamp(
			String dateTimeString, String format) {
		Timestamp timestamp = null;
		try {
			timestamp = new Timestamp(new SimpleDateFormat(format).parse(
				dateTimeString).getTime());
		} catch (NullPointerException e) {
		} catch (ParseException e) {
		}
		return timestamp;
	}
	
	/**
	 * 文字列形式の時間をTime型に変換します。 引数がNULLの場合、変換に失敗した場合、NULLを返却します。
	 *
	 * @param time
	 * @return
	 */
	public static Time convertStringToTime(String timeString) {
		Time time = null;
		try {
			time = new Time(new SimpleDateFormat(DATE_FORMAT_TIME).parse(
				timeString).getTime());
		} catch (NullPointerException e) {
		} catch (ParseException e) {
		}
		return time;
	}
	
	/**
	 * キャメルケースをスネークケースに変換します。
	 * 
	 * @param camel インプット文字列
	 * @return 変換後の文字列
	 */
    public static final String camelToSnake(final String camel) {
        if (StringUtils.isEmpty(camel)) {
            return camel;
        }
        final StringBuilder sb = new StringBuilder(camel.length() + camel.length());
        for (int i = 0; i < camel.length(); i++) {
            final char c = camel.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append(sb.length() != 0 ? '_' : "").append(Character.toLowerCase(c));
            } else {
                sb.append(Character.toLowerCase(c));
            }
        }
        return sb.toString();
    }

}
