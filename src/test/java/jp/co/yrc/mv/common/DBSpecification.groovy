package jp.co.yrc.mv.common;

import java.security.Principal
import java.sql.SQLException

import jp.co.yrc.mv.dto.UserInfoDto

import org.dbunit.Assertion
import org.dbunit.DatabaseUnitException
import org.dbunit.assertion.FailureHandler
import org.dbunit.database.DatabaseDataSourceConnection
import org.dbunit.database.IDatabaseConnection
import org.dbunit.database.QueryDataSet
import org.dbunit.dataset.Column
import org.dbunit.dataset.IDataSet
import org.dbunit.dataset.ITable
import org.dbunit.dataset.excel.XlsDataSet
import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.Scope
import org.springframework.context.support.SimpleThreadScope
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.support.BindingAwareModelMap

import spock.lang.Specification

/**
 * <table border="1">
 * <thead><tr>
 * <th>ラベル </th><th>説明  </th>
 * </tr></thead>
 * <tbody>
 * <tr><td>setup</td><td>feature で利用するオブジェクトなどの宣言と初期化を行うブロック。別名として given も同じ。</td></tr>
 * <tr><td>when</td><td>then にて示される予想結果の誘因となる任意のコードを含めることができる。 when-thenで繰り返して書くことができる。</td></tr>
 * <tr><td>then</td><td>when に応答する予想結果。条件、例外条件、相互作用、および変数の定義を含めることができる。</td></tr>
 * <tr><td>expect</td><td>予想結果を定義。条件、変数の定義を含めることができる。when-thenと比べ関数的な記述となる。</td></tr>
 * <tr><td>cleanup</td><td>where ブロックに続けて定義し、feature メソッドのリソース解放などを書く。</td></tr>
 * <tr><td>where</td><td>データドリブンな feature の条件を書く。常に feature メソッドの最後に置く。</td></tr>
 * </tbody><table>
 *
 */
@ContextConfiguration(locations =  "classpath:test-application-config.xml" )
@TransactionConfiguration
@Transactional
@WebAppConfiguration
@DBUnitSetup
class DBSpecification extends Specification {

	/**
	 * Excelファイルの基底パス
	 */
	String basePath
	/**
	 * テストクラス名
	 */
	String testClassName

	/**
	 * テストメソッド名
	 */
	String testMethodName

	/**
	 * SpringMVCのモデル
	 */
	BindingAwareModelMap model;

	/**
	 * SpringMVCのプリンシパル
	 */
	Principal principal;


	/**
	 * DataSource
	 */
	@Autowired
	TransactionAwareDataSourceProxy dataSourceProxy

	@Autowired
	BeanFactory beanFactory

	@Value('${master.excel.name}')
	String masterExcelName;

	void setup() {
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		userInfoDto.setLastName("lastName");
		userInfoDto.setFirstName("firstName");
		principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		model = new BindingAwareModelMap();

		// スコープを設定する
		Scope sessionScope = new SimpleThreadScope();
		beanFactory.registerScope("session", sessionScope);
		Scope requestScope = new SimpleThreadScope();
		beanFactory.registerScope("request", requestScope);
	}

	/**
	 * 期待値を記述したDataSetを取得します。
	 *
	 * @return IDataSet
	 * @throws Exception
	 */
	IDataSet getExpectedDataSet() throws Exception {
		return new XlsDataSet(new File(basePath + testClassName + "_" + testMethodName + "_expected.xls"));
	}

	IDataSet getActualDataSet(String ...tableName) throws Exception {
		IDatabaseConnection con = new DatabaseDataSourceConnection(dataSourceProxy);
		QueryDataSet result = new QueryDataSet(con);
		tableName.each({table -> result.addTable(table)});
		return result;
	}

	/**
	 * @see Assertion#assertEqualsIgnoreColss(IDataSet, IDataSet, String, String[])
	 */
	public static void assertEqualsIgnoreCols(final IDataSet expectedDataset,
			final IDataSet actualDataset, final String tableName,
			final String[] ignoreCols) throws DatabaseUnitException {
		Assertion.assertEqualsIgnoreCols(expectedDataset, actualDataset, tableName, ignoreCols);
	}

	/**
	 * @see Assertion#assertEqualsIgnoreCols(ITable, ITable, String[])
	 */
	public static void assertEqualsIgnoreCols(final ITable expectedTable,
			final ITable actualTable, final String[] ignoreCols)
	throws DatabaseUnitException {
		Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignoreCols);
	}

	/**
	 * @see Assertion#assertEqualsByQuery(IDataSet, IDatabaseConnection, String, String, String[])
	 */
	public static void assertEqualsByQuery(final IDataSet expectedDataset,
			final IDatabaseConnection connection, final String sqlQuery,
			final String tableName, final String[] ignoreCols)
	throws DatabaseUnitException, SQLException {
		Assertion.assertEqualsByQuery(expectedDataset, connection, sqlQuery,
				tableName, ignoreCols);
	}

	/**
	 * @see Assertion#assertEqualsByQuery(ITable, IDatabaseConnection, String, String, String[])
	 */
	public static void assertEqualsByQuery(final ITable expectedTable,
			final IDatabaseConnection connection, final String tableName,
			final String sqlQuery, final String[] ignoreCols)
	throws DatabaseUnitException, SQLException {
		Assertion.assertEqualsByQuery(expectedTable, connection, tableName,
				sqlQuery, ignoreCols);
	}

	/**
	 * @see Assertion#assertEquals(IDataSet, IDataSet)
	 */
	public static void assertEquals(IDataSet expectedDataSet,
			IDataSet actualDataSet) throws DatabaseUnitException {
		Assertion.assertEquals(expectedDataSet, actualDataSet);
	}

	/**
	 * @see Assertion#assertEquals(IDataSet, IDataSet, FailureHandler)
	 * @since 2.4
	 */
	public static void assertEquals(IDataSet expectedDataSet,
			IDataSet actualDataSet, FailureHandler failureHandler)
	throws DatabaseUnitException {
		Assertion.assertEquals(expectedDataSet, actualDataSet, failureHandler);
	}

	/**
	 * @see Assertion#assertEquals(ITable, ITable)
	 */
	public static void assertEquals(ITable expectedTable, ITable actualTable)
	throws DatabaseUnitException {
		Assertion.assertEquals(expectedTable, actualTable);
	}

	/**
	 * @see Assertion#assertEquals(ITable, ITable, Column[])
	 */
	public static void assertEquals(ITable expectedTable, ITable actualTable,
			Column[] additionalColumnInfo) throws DatabaseUnitException {
		Assertion.assertEquals(expectedTable, actualTable, additionalColumnInfo);
	}

	/**
	 * @see Assertion#assertEquals(ITable, ITable, FailureHandler)
	 */
	public static void assertEquals(ITable expectedTable, ITable actualTable,
			FailureHandler failureHandler) throws DatabaseUnitException {
		Assertion.assertEquals(expectedTable, actualTable, failureHandler);
	}
}

