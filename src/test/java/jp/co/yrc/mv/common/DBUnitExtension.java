
package jp.co.yrc.mv.common;

import org.spockframework.runtime.extension.AbstractAnnotationDrivenExtension;
import org.spockframework.runtime.model.FeatureInfo;
import org.spockframework.runtime.model.FieldInfo;
import org.spockframework.runtime.model.MethodInfo;
import org.spockframework.runtime.model.SpecInfo;

public class DBUnitExtension extends AbstractAnnotationDrivenExtension<DBUnitSetup> {

	private DBUnitInterceptor interceptor = new DBUnitInterceptor();

	/**
	 * テストメソッドに{@link DBUnit}があったら、実行前に{@link DBUnit}のあるメソッド分呼ばれるもよう
	 * 
	 * @param annotation
	 * @param feature
	 */
	@Override
	public void visitFeatureAnnotation(DBUnitSetup annotation, FeatureInfo feature) {
	}

	@Override
	public void visitFixtureAnnotation(DBUnitSetup annotation, MethodInfo fixtureMethod) {
	}

	/**
	 * テストメソッドに{@link GebDBUnit}があったら、実行前に{@link GebDBUnit}のあるメソッド分呼ばれる。
	 * 
	 * @param annotation
	 * @param feature
	 */
	@Override
	public void visitSpec(SpecInfo spec) {
		spec.addCleanupInterceptor(interceptor);
		spec.addCleanupSpecInterceptor(interceptor);
		spec.addInitializerInterceptor(interceptor);
		spec.addInterceptor(interceptor);
		spec.addSetupInterceptor(interceptor);
		spec.addSetupSpecInterceptor(interceptor);
		spec.addSharedInitializerInterceptor(interceptor);

		// spec.addInterceptor(interceptor);
		super.visitSpec(spec);
	}

	@Override
	public void visitFieldAnnotation(DBUnitSetup annotation, FieldInfo field) {
	}

	/**
	 * テストクラスに{@link DBUnit}があったら、実行前に1回呼ばれるもよう
	 * 
	 * @param annotation
	 * @param feature
	 */
	@Override
	public void visitSpecAnnotation(DBUnitSetup annotation, SpecInfo spec) {
	}

}
