package jp.co.yrc.mv.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * リソースバンドルコントロールクラス。
 * キャッシュ無効化、UTF-8のプロパティファイルに対応させる。
 * 
 */
public class MvResourceBundleControl extends ResourceBundle.Control {

		private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

		private static final List<String> FORMAT = Collections.unmodifiableList(Arrays.asList("properties"));

		/**
		 * 10秒間キャッシュする
		 */
		private static final long DEFAULT_CACHE_TIME = 10000L;
		private Charset charset;

		public MvResourceBundleControl(Charset charset) {
			this.charset = charset;
		}

		public MvResourceBundleControl() {
			this(DEFAULT_CHARSET);
		}

		@Override
		public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader,
				boolean reload) throws IllegalAccessException, InstantiationException, IOException {

			ResourceBundle result = null;
			String resourceName = getResourceName(baseName, locale, format, loader);
			InputStream stream = null;
			if (reload) {
				URL url = loader.getResource(resourceName);
				if (url != null) {
					URLConnection connection = url.openConnection();
					if (connection != null) {
						connection.setUseCaches(false);
						stream = connection.getInputStream();
					}
				}
			} else {
				stream = loader.getResourceAsStream(resourceName);
			}
			
			if (stream != null) {
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset))) {
					result = new PropertyResourceBundle(reader);
					reader.close();
					stream.close();
				}
			}

			return result;
		}

		/**
		 * ロケールに合わせたリソースファイル名を取得します。
		 * 
		 * @param baseName ベース名
		 * @param locale ロケール
		 * @param format フォーマット
		 * @param loader クラスローダー
		 * @return リソースファイル名
		 */
		protected String getResourceName(String baseName, Locale locale, String format, ClassLoader loader) {
			String resourceName = toResourceName(toBundleName(baseName, locale), format);
			URL url = loader.getResource(resourceName);
			if (url != null) {
				return resourceName;
			}

			return toResourceName(toBundleName(baseName, locale), format);
		}

		@Override
		public List<String> getFormats(String baseName) {
			return FORMAT;
		}

		@Override
		public long getTimeToLive(String baseName, Locale locale) {
			return DEFAULT_CACHE_TIME;
	}
}