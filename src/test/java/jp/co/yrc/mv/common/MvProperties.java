package jp.co.yrc.mv.common;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

/**
 * ResourceBundleをラッピングしたクラス。
 *
 */
public class MvProperties {

	private static final Logger logger = Logger.getLogger(MvProperties.class);

	/**
	 * リソースバンドルコントロール。
	 */
	protected MvResourceBundleControl control = new MvResourceBundleControl();

	/**
	 * プロパティファイルベース名。
	 */
	protected String baseName;

	/**
	 * コンストラクタ。
	 * 
	 * @param baseName　プロパティファイルベース名
	 */
	public MvProperties(String baseName) {
		this.baseName = baseName;
	}

	/**
	 * キーに対応する値を取得します。
	 * 
	 * @param key キー
	 * @return 値
	 */
	public String getValue(String key) {
		ResourceBundle bundle = ResourceBundle.getBundle(baseName, LocaleContext.instance().getLocale(), control);
		try {
			return bundle.getString(key);
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("", e);
			}
			return null;
		}
	}

	/**
	 * キーに対応する値を取得します。
	 * 
	 * @param key キー
	 * @return 値
	 */
	public Integer getIntegerValue(String key) {
		String str = getValue(key);
		return str == null ? null : Integer.valueOf(str);
	}
}
