package jp.co.yrc.mv.common;

import jp.co.yrc.mv.common.MvProperties;

/**
 * application_XX.propertiesファイルから値を取得するためのユーティリティクラス。
 */
public class TestPropertiesUtils {

	public static final MvProperties APPLICATION = new MvProperties("test");

	/**
	 * キーに対応する値を取得します。
	 * 
	 * @param key キー
	 * @return 値
	 */
	public static String getValue(String key) {
		return APPLICATION.getValue(key);
	}
}
