package jp.co.yrc.mv.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.spockframework.runtime.extension.ExtensionAnnotation;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
@ExtensionAnnotation(GebDBUnitExtension.class)
public @interface GebDBUnitSetup {

}
