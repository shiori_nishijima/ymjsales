package jp.co.yrc.mv.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.Principal;

import javax.sql.DataSource;

import org.dbunit.DataSourceBasedDBTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.support.BindingAwareModelMap;

import jp.co.yrc.mv.dto.UserInfoDto;

/**
 * ユニットテスト実施用の基底クラス。
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public abstract class DataSourceBasedDBTestCaseBase extends DataSourceBasedDBTestCase {
	
	/**
	 * Excelファイルの基底パス
	 */
	private String BASE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath() 
			+ (getClass().getPackage().getName().replace(".", "/")) + "/";
	
	/**
	 * テストクラス名
	 */
	private String testClassName = this.getClass().getSimpleName();
	
	/**
	 * テストメソッド名
	 */
	private String testMethodName;
	
	/**
	 * SpringMVCのモデル
	 */
	public BindingAwareModelMap model;
	
	/**
	 * SpringMVCのプリンシパル
	 */
	public Principal principal;

	/**
	 * テストメソッド名を設定します。
	 */
	@Rule
	public TestRule testName = new TestWatcher() {
		protected void starting(Description d) {
			testMethodName = d.getMethodName();
		};
	};
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		principal = createPrincipal();
		model = new BindingAwareModelMap();
	}

	/**
	 * プリンシパルの情報を作成します。
	 * 
	 * @return Principal
	 */
	protected Principal createPrincipal() {
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		userInfoDto.setLastName("lastName");
		userInfoDto.setFirstName("firstName");
		return new UsernamePasswordAuthenticationToken(userInfoDto, "password");
	}

	@Autowired
	TransactionAwareDataSourceProxy dataSourceProxy;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceProxy;
	}

	/**
	 * セットアップ用Excelファイルを取得します。
	 * デフォルトはテストクラス名_メソッド名に対応するファイルを取得します。
	 * 存在しない場合はテストクラス名に対応するファイルを取得します。
	 * 
	 * @return IDataSet
	 * @throws Exception
	 */
	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet;
		try {
			dataSet = getDataSetEachMethod();
		} catch (FileNotFoundException e) {
			dataSet = new XlsDataSet(new File(BASE_PATH + testClassName + ".xls")); 
		}
		return dataSet;
	}
	
	/**
	 * テストクラス名_メソッド名に対応するファイルを取得します。
	 * 
	 * @return IDataSet
	 * @throws Exception
	 */
	protected IDataSet getDataSetEachMethod() throws Exception {
		return new XlsDataSet(new File(BASE_PATH + testClassName + "_" + testMethodName + ".xls"));
	}

	/**
	 * 期待値を記述したDataSetを取得します。
	 * 
	 * @return IDataSet
	 * @throws Exception
	 */
	protected IDataSet getExpectedDataSet() throws Exception {
		return new XlsDataSet(new File(BASE_PATH + testClassName + "_" + testMethodName + "_expected.xls"));
	}	
}
