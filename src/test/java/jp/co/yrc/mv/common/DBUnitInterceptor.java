package jp.co.yrc.mv.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.dbunit.DataSourceDatabaseTester;
import org.dbunit.DefaultOperationListener;
import org.dbunit.IOperationListener;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.spockframework.runtime.InvalidSpecException;
import org.spockframework.runtime.extension.AbstractMethodInterceptor;
import org.spockframework.runtime.extension.IMethodInvocation;

/**
 * Interceptor for setup and cleanup methods for DBUnit
 */
public class DBUnitInterceptor extends AbstractMethodInterceptor {

	DataSourceDatabaseTester tester;

	IOperationListener operationListener;

	/**
	 * セットアップメソッドをインターセプトします。
	 * 
	 * @param invocation
	 */
	@Override
	public void interceptSetupMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
		setupTestInfo(invocation);

		DBUnit sepcAno = invocation.getSpec().getAnnotation(DBUnit.class);
		DBUnit featureAno = invocation.getFeature().getFeatureMethod().getAnnotation(DBUnit.class);
		tester = new DataSourceDatabaseTester(getDataSource(invocation));
		if (featureAno != null || sepcAno != null) {
			tester.setDataSet(getDataSet(invocation));
		} else {
			tester.setDataSet(getMasterDataSet(invocation));
		}
		tester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
		tester.setOperationListener(getOperationListener());

		TimeZone t = TimeZone.getDefault();
		// dbunit modifies time incorrectly when timezone is not GMT.
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		tester.onSetup();
		TimeZone.setDefault(t);
	}

	/**
	 * クリーンアップメソッドをインターセプトします。
	 * 
	 * @param invocation
	 */
	@Override
	public void interceptCleanupMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	/**
	 * テスト情報を設定します。
	 * 
	 * @param invocation
	 */
	protected void setupTestInfo(IMethodInvocation invocation) {
		Object o = invocation.getInstance();
		if (!(o instanceof DBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + DBSpecification.class.getName());
		}
		((DBSpecification) o).setTestClassName(getClassName(invocation));
		((DBSpecification) o).setBasePath(getBasePath(invocation));
		((DBSpecification) o).setTestMethodName(getMethodName(invocation));
	}

	/**
	 * データソースを取得します。
	 * 
	 * @param invocation
	 * @return DataSource
	 */
	protected DataSource getDataSource(IMethodInvocation invocation) {
		Object o = invocation.getInstance();
		if (!(o instanceof DBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + DBSpecification.class.getName());
		}
		return ((DBSpecification) o).getDataSourceProxy();
	}

	/**
	 * オペレーションリスナを取得します。
	 * 
	 * @return IOperationListener
	 */
	protected IOperationListener getOperationListener() {
		if (this.operationListener == null) {
			this.operationListener = new DefaultOperationListener() {
				public void connectionRetrieved(IDatabaseConnection connection) {
					super.connectionRetrieved(connection);
				}
			};
		}
		return this.operationListener;
	}

	/**
	 * Excelのデータセットを取得します。 テストクラス名に対応するExcelとメソッド名に対応するExcelが存在する場合は後者を優先する。
	 * 全体用のマスタExcelは必ず読み込みます。ファイル名はtext.propertiesに設定します。
	 * 
	 * @param invocation
	 * @return
	 * @throws Exception
	 */
	protected IDataSet getDataSet(IMethodInvocation invocation) throws Exception {
		Object o = invocation.getInstance();
		if (!(o instanceof DBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + DBSpecification.class.getName());
		}
		String masterExcelName = ((DBSpecification) o).getMasterExcelName();
		String basePath = getBasePath();
		String basePackagePath = getBasePath(invocation);
		String className = getClassName(invocation);
		IDataSet dataSet;
		try {
			dataSet = new MvXlsDataSet(new File(basePath + masterExcelName),
					new File(basePackagePath + className + "_" + getMethodName(invocation) + ".xls"));
		} catch (FileNotFoundException e) {
			dataSet = new MvXlsDataSet(new File(basePath + masterExcelName),
					new File(basePackagePath + className + ".xls"));
		}
		return dataSet;
	}

	/**
	 * 全体用のマスタExcelを取得します。
	 * 
	 * @param invocation
	 * @return
	 * @throws Exception
	 */
	protected IDataSet getMasterDataSet(IMethodInvocation invocation) throws Exception {
		Object o = invocation.getInstance();
		if (!(o instanceof DBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + DBSpecification.class.getName());
		}
		String masterExcelName = ((DBSpecification) o).getMasterExcelName();
		String basePath = getBasePath();
		IDataSet dataSet = new MvXlsDataSet(new File(basePath + masterExcelName));
		return dataSet;
	}

	/**
	 * ベースのパスを取得します。 該当クラスのパッケージは含まれません。
	 * 
	 * @return ベースパス
	 */
	protected String getBasePath() {
		return Thread.currentThread().getContextClassLoader().getResource("").getPath();
	}

	/**
	 * 該当クラスのパッケージを含めたベースのパスを取得します。
	 * 
	 * @param invocation
	 * @return ベースパス
	 */
	protected String getBasePath(IMethodInvocation invocation) {
		return Thread.currentThread().getContextClassLoader().getResource("").getPath()
				+ (invocation.getInstance().getClass().getPackage().getName().replace(".", "/")) + "/";
	}

	/**
	 * クラス名を取得します。
	 * 
	 * @param invocation
	 * @return クラス名
	 */
	protected String getClassName(IMethodInvocation invocation) {
		return invocation.getInstance().getClass().getSimpleName();
	}

	/**
	 * メソッド名を取得します。
	 * 
	 * @param invocation
	 * @return メソッド名
	 */
	protected String getMethodName(IMethodInvocation invocation) {
		return invocation.getFeature().getFeatureMethod().getName();
	}

	public void interceptInitializerMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptSharedInitializerMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptSetupSpecMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptCleanupSpecMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptFeatureMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptDataProviderMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptDataProcessorMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptIterationExecution(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptSpecExecution(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}

	public void interceptFeatureExecution(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
	}
}
