package jp.co.yrc.mv.common;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.BeforeClass;
import org.junit.Test;

import mockit.Mock;
import mockit.MockUp;

public class DateUtilsTest {
	
	@BeforeClass
	public static void setUpClass() {
		new MockUp<DateUtils>() {
			@Mock
			public Calendar getCalendar() {
				Calendar c = Calendar.getInstance();
				c.set(2016, 3, 10, 0, 0, 0); // 2016/4/10を当日とする。
				c.set(Calendar.MILLISECOND, 0);
				return c;
			}
		};
	}
	
	@Test
	public void getYearMonthCalendar_引数なし() {
		Calendar c = DateUtils.getYearMonthCalendar();
		assertThat(c.get(Calendar.YEAR), is(2016));
		assertThat(c.get(Calendar.MONTH), is(3));
		assertThat(c.get(Calendar.DATE), is(1));
		assertThat(c.get(Calendar.HOUR_OF_DAY), is(0));
		assertThat(c.get(Calendar.MINUTE), is(0));
		assertThat(c.get(Calendar.SECOND), is(0));
		assertThat(c.get(Calendar.MILLISECOND), is(0));
	}

	@Test
	public void getYearMonthCalendar_引数あり() {
		Calendar c = DateUtils.getYearMonthCalendar(2016, 4);
		assertThat(c.get(Calendar.YEAR), is(2016));
		assertThat(c.get(Calendar.MONTH), is(3));
		assertThat(c.get(Calendar.DATE), is(1));
		assertThat(c.get(Calendar.HOUR_OF_DAY), is(0));
		assertThat(c.get(Calendar.MINUTE), is(0));
		assertThat(c.get(Calendar.SECOND), is(0));
		assertThat(c.get(Calendar.MILLISECOND), is(0));
	}

	@Test
	public void getYearMonthCalendar_引数あり_月数追加() {
		Calendar c = DateUtils.getYearMonthCalendar();
		Calendar additionCal = DateUtils.getYearMonthCalendar(c, 9);
		assertThat(additionCal.get(Calendar.YEAR), is(2017));
		assertThat(additionCal.get(Calendar.MONTH), is(0));
		assertThat(additionCal.get(Calendar.DATE), is(1));
		assertThat(additionCal.get(Calendar.HOUR_OF_DAY), is(0));
		assertThat(additionCal.get(Calendar.MINUTE), is(0));
		assertThat(additionCal.get(Calendar.SECOND), is(0));
		assertThat(additionCal.get(Calendar.MILLISECOND), is(0));
	}

	@Test
	public void isPastYearMonth_過去年月でない() {
		assertThat(DateUtils.isPastYearMonth(2016, 4), is(false));
		assertThat(DateUtils.isPastYearMonth(2016, 5), is(false));		
		assertThat(DateUtils.isPastYearMonth(2017, 4), is(false));
	}
	
	@Test
	public void isPastYearMonth_過去年月() {
		assertThat(DateUtils.isPastYearMonth(2016, 3), is(true));
		assertThat(DateUtils.isPastYearMonth(2015, 12), is(true));		
	}
	
	@Test
	public void isPastYearMonth_引数カレンダー_過去年月でない() {
		assertThat(DateUtils.isPastYearMonth(DateUtils.getYearMonthCalendar(2016, 4)), is(false));
		assertThat(DateUtils.isPastYearMonth(DateUtils.getYearMonthCalendar(2016, 5)), is(false));
		assertThat(DateUtils.isPastYearMonth(DateUtils.getYearMonthCalendar(2017, 4)), is(false));
	}

	@Test
	public void isPastYearMonth_引数カレンダー_過去年月() {
		assertThat(DateUtils.isPastYearMonth(DateUtils.getYearMonthCalendar(2016, 3)), is(true));
		assertThat(DateUtils.isPastYearMonth(DateUtils.getYearMonthCalendar(2015, 12)), is(true));
	}

	@Test
	public void isCurrentYearMonth_現在年月() {
		assertThat(DateUtils.isCurrentYearMonth(2016, 4), is(true));
	}

	@Test
	public void isCurrentYearMonth_現在年月でない() {
		assertThat(DateUtils.isCurrentYearMonth(2015, 3), is(false));
		assertThat(DateUtils.isCurrentYearMonth(2016, 3), is(false));
		assertThat(DateUtils.isCurrentYearMonth(2016, 5), is(false));
		assertThat(DateUtils.isCurrentYearMonth(2017, 5), is(false));
	}

	@Test
	public void getDay() {
		Calendar c = DateUtils.getCalendar();
		assertThat(DateUtils.getDay(c.getTime()), is(10));
		c.set(Calendar.DATE, 20);
		assertThat(DateUtils.getDay(c.getTime()), is(20));
	}

	@Test
	public void getNowYearMonth() {
		assertThat(DateUtils.getNowYearMonth(), is(201604));
	}

	@Test
	public void toYearMonth_年月引数() {
		assertThat(DateUtils.toYearMonth(2016, 5), is(201605));
	}

	@Test
	public void toYearMonth_カレンダー引数() {
		Calendar c = DateUtils.getCalendar();
		assertThat(DateUtils.toYearMonth(c), is(201604));
	}

	@Test
	public void toMonth() {
		assertThat(DateUtils.toMonth(201604), is(4));
		assertThat(DateUtils.toMonth(201612), is(12));
	}
}

