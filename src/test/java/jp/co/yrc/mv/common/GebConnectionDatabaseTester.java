package jp.co.yrc.mv.common;

import java.sql.Connection;

import javax.sql.DataSource;

import org.dbunit.AbstractDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Gebテスト用DatabaseTester
 * uses a {@link DataSource} to create connections.
 *
 */
public class GebConnectionDatabaseTester extends AbstractDatabaseTester {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GebConnectionDatabaseTester.class);

	private Connection connection;

	/**
	 * Creates a new DataSourceDatabaseTester with the specified DataSource.
	 *
	 * @param dataSource
	 *            the DataSource to pull connections from
	 */
	public GebConnectionDatabaseTester(Connection connection) {
		super();

		if (connection == null) {
			throw new NullPointerException("The parameter 'dataSource' must not be null");
		}
		this.connection = connection;
	}

	/**
	 * Creates a new DataSourceDatabaseTester with the specified DataSource and schema name.
	 * 
	 * @param dataSource
	 *            the DataSource to pull connections from
	 * @param schema
	 *            The schema name to be used for new dbunit connections
	 * @since 2.4.5
	 */
	public GebConnectionDatabaseTester(Connection connection, String schema) {
		super(schema);

		if (connection == null) {
			throw new NullPointerException("The parameter 'dataSource' must not be null");
		}
		this.connection = connection;
	}

	public IDatabaseConnection getConnection() throws Exception {
		logger.debug("getConnection() - start");

		assertTrue("DataSource is not set", connection != null);
		IDatabaseConnection conn = new DatabaseConnection(connection, getSchema());
		conn.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);
		
		return conn;
	}
}
