package jp.co.yrc.mv.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

import org.dbunit.AbstractDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;

/**
 * Gebテスト用DatabaseTester
 * uses a {@link DataSource} to create connections.
 *
 */
public class GebDataSourceDatabaseTester extends AbstractDatabaseTester {

	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(GebDataSourceDatabaseTester.class);

	private DataSource dataSource;

	/**
	 * Creates a new DataSourceDatabaseTester with the specified DataSource.
	 *
	 * @param dataSource
	 *            the DataSource to pull connections from
	 */
	public GebDataSourceDatabaseTester(DataSource dataSource) {
		super();

		if (dataSource == null) {
			throw new NullPointerException("The parameter 'dataSource' must not be null");
		}
		this.dataSource = dataSource;
	}

	/**
	 * Creates a new DataSourceDatabaseTester with the specified DataSource and schema name.
	 * 
	 * @param dataSource
	 *            the DataSource to pull connections from
	 * @param schema
	 *            The schema name to be used for new dbunit connections
	 * @since 2.4.5
	 */
	public GebDataSourceDatabaseTester(DataSource dataSource, String schema) {
		super(schema);

		if (dataSource == null) {
			throw new NullPointerException("The parameter 'dataSource' must not be null");
		}
		this.dataSource = dataSource;
	}

	public IDatabaseConnection getConnection() throws Exception {
		logger.debug("getConnection() - start");

		assertTrue("DataSource is not set", dataSource != null);
		IDatabaseConnection conn = new DatabaseConnection(dataSource.getConnection(), getSchema());
		conn.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);
		conn.getConnection().setAutoCommit(false);

		return conn;
	}
}
