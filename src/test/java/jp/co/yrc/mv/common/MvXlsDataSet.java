
package jp.co.yrc.mv.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.dbunit.dataset.AbstractDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.DefaultTableIterator;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * MV用XlsDataSetクラス。
 * 
 * シート名文字数制限にかかるテーブル名を変換する処理を追加。
 * 複数のExcelシートを引き渡せる処理を追加。
 */
public class MvXlsDataSet extends AbstractDataSet {

	private static final Logger logger = LoggerFactory.getLogger(MvXlsDataSet.class);

	private final MvOrderedTableNameMap _tables;

	/**
	 * コンストラクタ。
	 * 
	 * @param files
	 * @throws IOException
	 * @throws DataSetException
	 */
	public MvXlsDataSet(File... files) throws IOException, DataSetException {
		_tables = createMvTableNameMap();
		addTable(files);
	}

	/**
	 * テーブル名のイテレータを作成します。
	 * 
	 * @param reversed
	 */
	@Override
	protected ITableIterator createIterator(boolean reversed) throws DataSetException {
		if (logger.isDebugEnabled())
			logger.debug("createIterator(reversed={}) - start", String.valueOf(reversed));

		ITable[] tables = (ITable[]) _tables.orderedValues().toArray(new ITable[0]);
		return new DefaultTableIterator(tables, reversed);
	}

	/**
	 * Excelからシートを読み取り、テーブルデータを追加します。
	 * 
	 * @param files
	 * @throws EncryptedDocumentException
	 * @throws IOException
	 * @throws DataSetException
	 */
	protected void addTable(File... files) throws EncryptedDocumentException, IOException, DataSetException {
		for (File file : files) {
			Workbook workbook;
			try {
				workbook = WorkbookFactory.create(new FileInputStream(file));
			} catch (InvalidFormatException e) {
				throw new IOException(e);
			}

			int sheetCount = workbook.getNumberOfSheets();
			for (int i = 0; i < sheetCount; i++) {
				String sheetName = workbook.getSheetName(i);
				ITable table = new MvXlsTable(TestUtils.camelToSnake(sheetName), workbook.getSheetAt(i));
				_tables.add(table.getTableMetaData().getTableName(), table);
			}
		}
	}

	/**
	 * テーブル名マップを作成します。
	 * 
	 * @return
	 */
	protected MvOrderedTableNameMap createMvTableNameMap() {
		return new MvOrderedTableNameMap(isCaseSensitiveTableNames());
	}
}
