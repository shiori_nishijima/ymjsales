package jp.co.yrc.mv.common;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

public class MD5UtilTest {
	@Test
	public void getMD5HexStringTest() {
		String actual = MD5Util.getMD5HexString("passw0rd");
		String expected = "bed128365216c019988915ed3add75fb";

		assertThat(actual, is(expected));
	}

	@Test(expected = RuntimeException.class)
	public void MD5インスタンス生成エラー() throws NoSuchAlgorithmException {
		PowerMockito.mockStatic(MessageDigest.class);
		PowerMockito.when(MessageDigest.getInstance((String) any())).thenThrow(new Exception());

		MD5Util.getMD5HexString("passw0rd");
	}

	@Test
	public void toHexStringTest() throws UnsupportedEncodingException {
		String actual = MD5Util.toHexString("passw0rd".getBytes("UTF-8"));
		String expected = "7061737377307264";

		assertThat(actual, is(expected));
	}
}