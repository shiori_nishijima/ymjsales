
package jp.co.yrc.mv.common;

import org.spockframework.runtime.extension.AbstractAnnotationDrivenExtension;
import org.spockframework.runtime.model.FeatureInfo;
import org.spockframework.runtime.model.FieldInfo;
import org.spockframework.runtime.model.MethodInfo;
import org.spockframework.runtime.model.SpecInfo;

/**
 * テストクラスに{@link GebDBUnitSetup}が付与された場合に起動し、インターセプタを設定します。
 *
 */
public class GebDBUnitExtension extends AbstractAnnotationDrivenExtension<GebDBUnitSetup> {

	private GebDBUnitInterceptor interceptor = new GebDBUnitInterceptor();

	@Override
	public void visitFeatureAnnotation(GebDBUnitSetup annotation, FeatureInfo feature) {
	}

	@Override
	public void visitFixtureAnnotation(GebDBUnitSetup annotation, MethodInfo fixtureMethod) {
	}

	@Override
	public void visitSpec(SpecInfo spec) {
		spec.addCleanupInterceptor(interceptor);
		spec.addCleanupSpecInterceptor(interceptor);
		spec.addInitializerInterceptor(interceptor);
		spec.addInterceptor(interceptor);
		spec.addSetupInterceptor(interceptor);
		spec.addSetupSpecInterceptor(interceptor);
		spec.addSharedInitializerInterceptor(interceptor);

		// spec.addInterceptor(interceptor);
		super.visitSpec(spec);
	}

	@Override
	public void visitFieldAnnotation(GebDBUnitSetup annotation, FieldInfo field) {
	}

	@Override
	public void visitSpecAnnotation(GebDBUnitSetup annotation, SpecInfo spec) {
	}

}
