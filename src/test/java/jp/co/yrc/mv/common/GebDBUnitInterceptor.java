package jp.co.yrc.mv.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.dbunit.DefaultOperationListener;
import org.dbunit.IOperationListener;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.spockframework.runtime.InvalidSpecException;
import org.spockframework.runtime.extension.IMethodInvocation;

/**
 * Interceptor for setup and cleanup methods for DBUnit
 */
public class GebDBUnitInterceptor extends DBUnitInterceptor {

	IOperationListener operationListener;

	List<String> tableList = new ArrayList<>();

	/** バックアップ */
	File file;

	static final String STRING_NULL = "[NULL]";
	static final String STRING_EMPTY = "[EMPTY]";

	@Override
	public void interceptSetupSpecMethod(IMethodInvocation invocation) throws Throwable {

		// データの全バックアップ
		Connection conn = getConnection(invocation); // setUpSpec段階ではAutoWiredフィールドに値が設定されていない
		conn.setAutoCommit(true);
		IDatabaseConnection dbConn = new DatabaseConnection(conn);
		QueryDataSet partialDataSet = new QueryDataSet(dbConn);
		
		ResultSet rs = conn.getMetaData().getTables(null, "", null, null);
		while (rs.next()) {
			String tableName = rs.getString(3);
			if (tableName != null && !"".equals(tableName)) {
				tableList.add(tableName);
				partialDataSet.addTable(tableName);
			}
		}

		ReplacementDataSet rd = new ReplacementDataSet(partialDataSet);
		rd.addReplacementObject(null, STRING_NULL);
		rd.addReplacementObject("", STRING_EMPTY);
		file = File.createTempFile("backup", ".xml");
		FlatXmlDataSet.write(rd, new FileOutputStream(file));
		
		// マスタデータを最初にセットアップ
		GebConnectionDatabaseTester gebTester = new GebConnectionDatabaseTester(conn);
		IDataSet dataset = getMasterDataSet(invocation);

		gebTester.setDataSet(dataset);
		gebTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
		gebTester.setOperationListener(getOperationListener());
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		gebTester.onSetup();
		TimeZone.setDefault(TimeZone.getTimeZone("JST"));
		conn.close();
		// セットアップしてから実行
		invocation.proceed();
	}

	/**
	 * セットアップメソッド呼び出しをインターセプトする。
	 */
	@Override
	public void interceptSetupMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
		setupTestInfo(invocation);

		GebDBUnit gebSepc = invocation.getSpec().getAnnotation(GebDBUnit.class);
		GebDBUnit gebFeature = invocation.getFeature().getFeatureMethod().getAnnotation(GebDBUnit.class);
		if (gebSepc != null || gebFeature != null) {

			GebDataSourceDatabaseTester gebTester = new GebDataSourceDatabaseTester(getDataSource(invocation));
			IDataSet dataset = getDataSet(invocation);

			gebTester.setDataSet(dataset);
			gebTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
			gebTester.setOperationListener(getOperationListener());

			TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
			gebTester.onSetup();
			TimeZone.setDefault(TimeZone.getTimeZone("JST"));

			gebTester.getConnection().getConnection().commit();
		}
	}

	/**
	 * クリーンアップメソッドをインターセプトします。
	 * 
	 * @param invocation
	 */
	@Override
	public void interceptCleanupSpecMethod(IMethodInvocation invocation) throws Throwable {
		invocation.proceed();
		// テーブルの全データをクリーンにする
		Connection conn = getConnection(invocation);
		conn.setAutoCommit(true);
		Statement stmt = conn.createStatement();
		for (String tableName : tableList) {
			stmt.executeUpdate("DELETE FROM " + tableName);
		}
		// バックアップファイルからデータを復元
		GebConnectionDatabaseTester gebTester = new GebConnectionDatabaseTester(conn);
		FileInputStream in = new FileInputStream(file);
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(in);
		ReplacementDataSet rd = new ReplacementDataSet(dataSet);
		rd.addReplacementObject(STRING_NULL, null);
		rd.addReplacementObject(STRING_EMPTY, "");

		gebTester.setDataSet(rd);
		gebTester.setTearDownOperation(DatabaseOperation.REFRESH);
		gebTester.setOperationListener(getOperationListener());
		gebTester.onTearDown(); // tearDown処理後に自動クローズされる
		gebTester = null;
		// バックアップファイルを削除
		file.delete();
	}

	/**
	 * オペレーションリスナを取得します。
	 * 
	 * @return IOperationListener
	 */
	@Override
	protected IOperationListener getOperationListener() {
		if (this.operationListener == null) {
			this.operationListener = new DefaultOperationListener() {
				public void connectionRetrieved(IDatabaseConnection connection) {
					super.connectionRetrieved(connection);
				}
			};
		}
		return this.operationListener;
	}
	
	/**
	 * Excelのデータセットを取得します。 テストクラス名に対応するExcelとメソッド名に対応するExcelが存在する場合は後者を優先します。
	 * ログイン処理をsetUpClassで処理するため、マスタExcelはここでは取得しません。
	 * 
	 * @param invocation
	 * @return
	 * @throws Exception
	 */
	@Override
	protected IDataSet getDataSet(IMethodInvocation invocation) throws Exception {
		Object o = invocation.getInstance();
		if (!(o instanceof GebDBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + GebDBSpecification.class.getName());
		}
		String basePackagePath = getBasePath(invocation);
		String className = getClassName(invocation);
		IDataSet dataSet;
		try {
			dataSet = new MvXlsDataSet(new File(basePackagePath + className + "_" + getMethodName(invocation) + ".xls"));
		} catch (FileNotFoundException e) {
			dataSet = new MvXlsDataSet(new File(basePackagePath + className + ".xls"));
		}
		return dataSet;
	}
	
	/**
	 * 全体用のマスタExcelを取得します。
	 * 
	 * @param invocation
	 * @return
	 * @throws Exception
	 */
	@Override
	protected IDataSet getMasterDataSet(IMethodInvocation invocation) throws Exception {
		Object o = invocation.getInstance();
		if (!(o instanceof GebDBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + GebDBSpecification.class.getName());
		}
		String masterExcelName = TestPropertiesUtils.getValue("master.excel.name");
		String basePath = getBasePath();
		IDataSet dataSet = new MvXlsDataSet(new File(basePath + masterExcelName));
		return dataSet;
	}
	
	/**
	 * コネクションを取得します。
	 * 
	 * @param invocation
	 * @return Connection
	 * @throws SQLException
	 */
	protected Connection getConnection(IMethodInvocation invocation) throws SQLException {
		Object o = invocation.getInstance();
		if (!(o instanceof GebDBSpecification)) {
			throw new InvalidSpecException(o.getClass().getName() + "is Not " + GebDBSpecification.class.getName());
		}
		// コネクションを生成
		Connection conn = null;
		String url = TestPropertiesUtils.getValue("db.url");
		String user = TestPropertiesUtils.getValue("db.user");
		String password = TestPropertiesUtils.getValue("db.password");
		conn = DriverManager.getConnection(url, user, password);
		return conn;
	}
}
