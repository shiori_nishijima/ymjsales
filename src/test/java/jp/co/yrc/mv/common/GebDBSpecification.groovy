package jp.co.yrc.mv.common

import geb.Browser
import geb.Configuration
import geb.ConfigurationLoader
import groovy.lang.GroovyClassLoader;
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

/**
 * Gebテスト用基底クラス。
 *
 * テストクラス実行時に1回データのバックアップを取り、テストクラス終了時に1回バックアップデータを元に戻す。
 * 各テストごとにデータをDBに投入したい場合はメソッドに{@link @GebDbUnit}を付与する。
 *
 */
@GebDBUnitSetup
class GebDBSpecification extends DBSpecification {

	String gebConfEnv = null
	String gebConfScript = null

	@Shared
	Browser _browser

	@Override
	void setup() {
		dataSourceProxy.getConnection().commit();
	}

	Configuration createConf() {
		new ConfigurationLoader(gebConfEnv, System.properties, new GroovyClassLoader(getClass().classLoader)).getConf(gebConfScript)
	}

	Browser createBrowser() {
		new Browser(createConf())
	}

	Browser getBrowser() {
		if (_browser == null) {
			_browser = createBrowser()
		}
		_browser
	}

	void resetBrowser() {
		if (_browser?.config?.autoClearCookies) {
			_browser.clearCookiesQuietly()
		}
		_browser = null
	}

	def methodMissing(String name, args) {
		getBrowser()."$name"(*args)
	}

	def propertyMissing(String name) {
		getBrowser()."$name"
	}

	def propertyMissing(String name, value) {
		getBrowser()."$name" = value
	}

	private isSpecStepwise() {
		this.class.getAnnotation(Stepwise) != null
	}

	def cleanup() {
		if (!isSpecStepwise()) {
			resetBrowser()
		}
	}

	def cleanupSpec() {
		if (isSpecStepwise()) {
			resetBrowser()
		}
	}
}
