package jp.co.yrc.mv.aop;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
public class RestErrorHandlerTest {
	@Autowired
	RestErrorHandler sut;

	@Test
	public void handleMethodArgumentNotValidTest() {
		MethodArgumentNotValidException e = mock(MethodArgumentNotValidException.class);
		HttpHeaders headers = mock(HttpHeaders.class);
		HttpStatus status = HttpStatus.BAD_REQUEST;
		WebRequest request = mock(WebRequest.class);

		BindingResult bindingResult = mock(BindingResult.class);
		when(e.getBindingResult()).thenReturn(bindingResult);
		List<FieldError> fieldErrors = new ArrayList<FieldError>();
		FieldError fieldError = mock(FieldError.class);
		fieldErrors.add(fieldError);
		when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

		List<ObjectError> objectErrors = new ArrayList<ObjectError>();
		ObjectError objectError = mock(ObjectError.class);
		objectErrors.add(objectError);
		when(bindingResult.getGlobalErrors()).thenReturn(objectErrors);

		when(fieldError.getDefaultMessage()).thenReturn("FieldDefaultMessage");
		when(fieldError.getField()).thenReturn("fieldName");

		when(objectError.getDefaultMessage()).thenReturn("ObjectDefaultMessage");
		when(objectError.getObjectName()).thenReturn("objectName");

		ResponseEntity<Object> actual = sut.handleMethodArgumentNotValid(e, headers, status, request);

		List<String> errors = new ArrayList<String>();
		errors.add("FieldDefaultMessage");
		errors.add("ObjectDefaultMessage");

		Map<String, String> header = new LinkedHashMap<String, String>();

		assertThat(actual.getStatusCode(), is(HttpStatus.BAD_REQUEST));
		assertThat(actual.getBody().toString(), is(errors.toString()));
		assertThat(actual.getHeaders().toString(), is(header.toString()));
	}
}
