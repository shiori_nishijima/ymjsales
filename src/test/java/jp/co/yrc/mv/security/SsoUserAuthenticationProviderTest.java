package jp.co.yrc.mv.security;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.util.Iterator;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class SsoUserAuthenticationProviderTest extends DataSourceBasedDBTestCaseBase {

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/"+ getClass().getSimpleName();

	@Autowired
	SsoUserAuthenticationProvider sut;

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XlsDataSet(new File(RESOURCE_PATH + testClassName + ".xls"));
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void authenticateTest() {
		Authentication authentication = new SsoUserAuthenticationToken("test", "passw0rd", "linkId", "kaishaCd", "ts",
				"OjQ16xYFyEseM4XQj6qbJBg1Vrw=", "ja");
		Authentication actual = sut.authenticate(authentication);

		Iterator<? extends GrantedAuthority> ite = actual.getAuthorities().iterator();
		GrantedAuthority g = ite.next();

		assertThat(actual.getName(), is("test"));
		assertThat((String) actual.getPrincipal(), is("test"));
		assertThat((String) actual.getCredentials(), is("passw0rd"));
		assertThat(g.getAuthority(), is("ROLE_USER"));
		assertNull(actual.getDetails());
	}

	@Test
	public void authenticateTest_idに対応するuserが存在しない場合() {
		Authentication authentication = new SsoUserAuthenticationToken("test12345", "passw0rd", "linkId", "kaishaCd",
				"ts", "h5PJoMZ33fCk/lpMEdMy7M19iTI=", "ja");
		Authentication actual = sut.authenticate(authentication);

		assertNull(actual);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void authenticateTest_シグネチャが不正な場合はUsernameNotFoundExceptionをスロー() {
		Authentication authentication = new SsoUserAuthenticationToken("test", "passw0rd", "linkId", "kaishaCd", "ts",
				"sg", "ja");
		sut.authenticate(authentication);
	}
}
