package jp.co.yrc.mv.security;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.Iterator;

import javax.sql.DataSource;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.dto.UserInfoDto;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class UserAuthenticationProviderTest extends DataSourceBasedDBTestCaseBase {

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/" + getClass().getSimpleName();

	@Autowired
	UserAuthenticationProvider sut;

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XlsDataSet(new File(RESOURCE_PATH + testClassName + ".xls"));
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void additionalAuthenticationChecksTest() {
		UserInfoDto user = new UserInfoDto();
		user.setId("test");
		user.setUserHash("bed128365216c019988915ed3add75fb");
		UserDetails userDetails = user;
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);

		when(authentication.getCredentials()).thenReturn("passw0rd");
		sut.additionalAuthenticationChecks(userDetails, authentication);
	}

	@Test(expected = BadCredentialsException.class)
	public void additionalAuthenticationChecksTest_exception() {
		UserInfoDto user = new UserInfoDto();
		user.setId("test");
		user.setUserHash("bed128365216c019988915ed3add75");
		UserDetails userDetails = user;
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);

		when(authentication.getCredentials()).thenReturn("passw0rd");
		sut.additionalAuthenticationChecks(userDetails, authentication);
	}

	// MvUserDetailsServiceのloadUserByUsernameメソッドのテスト兼用
	@Test
	public void retrieveUserTest() {
		String username = "test";
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);
		when(authentication.getCredentials()).thenReturn("passw0rd");
		UserInfoDto actual = (UserInfoDto) sut.retrieveUser(username, authentication);

		Iterator<? extends GrantedAuthority> ite = actual.getAuthorities().iterator();
		GrantedAuthority g = ite.next();

		assertThat(actual.getId(), is("test"));
		assertThat(actual.getUserHash(), is("bed128365216c019988915ed3add75fb"));
		assertThat(actual.getIdForCustomer(), is("test"));
		assertThat(actual.getFirstName(), is("太郎"));
		assertThat(actual.getLastName(), is("SFA"));
		assertNull(actual.getAuthorization());
		assertTrue(actual.isLogin());
		assertTrue(actual.isEnabled());
		assertThat(g.getAuthority(), is("ROLE_USER"));
	}

	@Test(expected = BothNotEnterException.class)
	public void retrieveUserTest_BothNotEnterException() {
		String username = "";
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);
		when(authentication.getCredentials()).thenReturn("");
		sut.retrieveUser(username, authentication);
	}

	@Test(expected = UserNameNotEnterException.class)
	public void retrieveUserTest_UserNameNotEnterException() {
		String username = "";
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);
		when(authentication.getCredentials()).thenReturn("passw0rd");
		sut.retrieveUser(username, authentication);
	}

	@Test(expected = PasswordNotEnterException.class)
	public void retrieveUserTest_PasswordNotEnterException() {
		String username = "test";
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);
		when(authentication.getCredentials()).thenReturn("");
		sut.retrieveUser(username, authentication);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void retrieveUserTest_UsernameNotFoundException() {
		String username = "dummy";
		UsernamePasswordAuthenticationToken authentication = mock(UsernamePasswordAuthenticationToken.class);
		when(authentication.getCredentials()).thenReturn("passw0rd");
		sut.retrieveUser(username, authentication);
	}
}
