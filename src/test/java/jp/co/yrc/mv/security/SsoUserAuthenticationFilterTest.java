package jp.co.yrc.mv.security;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mockit.Mock;
import mockit.MockUp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class SsoUserAuthenticationFilterTest {

	SsoUserAuthenticationFilter sut;

	@Before
	public void setUp() {
		sut = spy(new SsoUserAuthenticationFilter() {
		});
		doReturn("userId").when(sut).obtainUserId((HttpServletRequest) any());
		doReturn("linkId").when(sut).obtainLinkId((HttpServletRequest) any());
		doReturn("kaishaCd").when(sut).obtainKaishaCd((HttpServletRequest) any());
		doReturn("ts").when(sut).obtainTs((HttpServletRequest) any());
		doReturn("sg").when(sut).obtainSignature((HttpServletRequest) any());
	}

	@Test
	public void attemptAuthenticationTest() {
		new MockUp<UsernamePasswordAuthenticationFilter>() {
			@Mock
			public String obtainPassword(HttpServletRequest request) {
				return "password";
			}

			@Mock
			public AuthenticationManager getAuthenticationManager() {
				AuthenticationManager manager = mock(AuthenticationManager.class);
				when(manager.authenticate((Authentication) any())).thenReturn(null);
				return manager;
			}
		};
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		sut.attemptAuthentication(request, response);

		ArgumentCaptor<HttpServletRequest> argRequest = ArgumentCaptor.forClass(HttpServletRequest.class);
		ArgumentCaptor<SsoUserAuthenticationToken> argToken = ArgumentCaptor.forClass(SsoUserAuthenticationToken.class);

		verify(sut, times(1)).setDetails(argRequest.capture(), argToken.capture());
		SsoUserAuthenticationToken token = argToken.getValue();

		assertThat((String) token.getPrincipal(), is("userId"));
		assertThat((String) token.getCredentials(), is("password"));
		assertThat(token.getKaishaCd(), is("kaishaCd"));
		assertThat(token.getLinkId(), is("linkId"));
		assertThat(token.getSg(), is("sg"));
		assertThat(token.getTs(), is("ts"));
	}
}
