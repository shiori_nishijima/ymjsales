package jp.co.yrc.mv.rest


import java.sql.Timestamp

import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.common.IDUtils
import mockit.MockUp

import org.dbunit.dataset.IDataSet
import org.dbunit.dataset.SortedTable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration

@DBUnit
class MenuFavoriteControllerSpec extends DBSpecification {
	@Autowired
	MenuFavoriteController sut;


	def "指定ユーザーのお気に入り一覧を取得する"() {

		setup:
		def expected = new ArrayList<>();
		expected << 'test.html'
		expected << 'test2.html'


		when:
		def actual = sut.list(principal);

		then:
		actual == expected
	}


	def "指定ユーザーのお気に入り一覧更新する"() {

		setup:
		def newIds = new ArrayList<>();
		newIds << 'test.html'
		newIds << 'test3.html'

		new MockUp<DateUtils>() {
					@mockit.Mock
					Timestamp getDBTimestamp() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/5/01 00:00:00"))
						return 	new Timestamp(cal.getTimeInMillis());
					}
				}

		new MockUp<IDUtils>() {
					int i = 100;
					@mockit.Mock
					String generateGuid() {
						return i++;
					}
				}


		when:
		sut.update(newIds, principal);

		then:
		IDataSet expected = getExpectedDataSet();
		IDataSet actual = getActualDataSet('menu_favorites');

		assertEquals(new SortedTable(expected.getTable('menu_favorites'), 'id'), new SortedTable(actual.getTable('menu_favorites'), 'id'))
		
	}
}

