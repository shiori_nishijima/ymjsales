package jp.co.yrc.mv.helper;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class SelectYearsHelperTest {
	SelectYearsHelper sut;

	@Before
	public void setUp() {
		sut = new SelectYearsHelper();
	}

	@Test
	public void 年リストの作成() {
		List<Integer> years = new ArrayList<>();
		sut.makeYearsList(years, 5, 5);

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		List<Integer> expected = new ArrayList<Integer>();
		for (int i = 0; i <= 5; i++) {
			expected.add(year);
			year++;
		}
		year = cal.get(Calendar.YEAR);
		for (int i = 0; i < 5; i++) {
			year--;
			expected.add(year);
		}
		Collections.sort(expected);
		Collections.reverse(expected);
		assertThat(years, is(expected));
	}

	@Test
	public void 月リストの作成() {
		List<Integer> months = new ArrayList<>();
		sut.makeMonthsList(months);

		List<Integer> expected = new ArrayList<Integer>();
		for (int i = 1; i <= 12; i++) {
			expected.add(i);
		}
		assertThat(months, is(expected));
	}

}
