package jp.co.yrc.mv.helper;


public class SoshikiHelperTest {
//	SoshikiHelper sut;
//
//	@Before
//	public void setUp() {
//		sut = spy(new SoshikiHelper());
//		sut.init();
//	}
//
//	@Test
//	public void createTest() {
//		List<EigyoSosikiDto> list = null;
//		List<EigyoSosikiDto> myList = null;
//		String auth = Role.All.getCode();
//		sut.create(list, myList, auth);
//
//		assertThat(sut.div1.get(0).getLabel(), is(""));
//		assertThat(sut.div1.get(0).getValue(), is(""));
//	}
//
//	@Test
//	public void createTest_RoleがAllでリストがnullでない場合() {
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv1Nm("div1");
//		e.setDiv1Cd("1");
//		list.add(e);
//		List<EigyoSosikiDto> myList = new ArrayList<EigyoSosikiDto>();
//		String auth = Role.All.getCode();
//
//		sut.create(list, myList, auth);
//
//		assertThat(sut.div1.get(0).getLabel(), is(""));
//		assertThat(sut.div1.get(0).getValue(), is(""));
//		assertThat(sut.div1.get(1).getLabel(), is("div1"));
//		assertThat(sut.div1.get(1).getValue(), is("1"));
//	}
//
//	@Test
//	public void createTest_RoleがMultipleで全リストと自組織リストがnullでない場合() {
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv1Nm("div1");
//		e.setDiv1Cd("1");
//		EigyoSosikiDto e2 = new EigyoSosikiDto();
//		e2.setDiv1Nm("myDiv1");
//		e2.setDiv1Cd("2");
//		list.add(e);
//		list.add(e2);
//		
//		List<EigyoSosikiDto> myList = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto mySosiki = new EigyoSosikiDto();
//		mySosiki.setDiv1Cd("2");
//		mySosiki.setDiv1Nm("myDiv1");
//		myList.add(mySosiki);
//
//		String auth = Role.All.getCode();
//
//		sut.create(list, myList, auth);
//
//		assertThat(sut.div1.get(0).getLabel(), is(""));
//		assertThat(sut.div1.get(0).getValue(), is(""));
//		assertThat(sut.div1.get(1).getLabel(), is("myDiv1"));
//		assertThat(sut.div1.get(1).getValue(), is("2"));
//	}
//
//	@Test
//	public void setDiv1Test_true() {
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv1Nm("div1");
//		e.setDiv1Cd("1");
//		list.add(e);
//
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv1(e, auth);
//		assertTrue(actual);
//	}
//
//	@Test
//	public void setDiv1Test_false() {
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv1Nm(null);
//		e.setDiv1Cd("0");
//
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv1(e, auth);
//
//		assertFalse(actual);
//	}
//
//	@Test
//	public void setDiv2Test_true() {
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv2Nm("div2");
//		e.setDiv2Cd("2");
//		list.add(e);
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv2(e, auth);
//		assertTrue(actual);
//
//		assertThat(sut.div2.get(0).getItems().get(0).getLabel(), is(""));
//		assertThat(sut.div2.get(0).getItems().get(0).getValue(), is(""));
//		assertThat(sut.div2.get(0).getItems().get(1).getLabel(), is("div2"));
//		assertThat(sut.div2.get(0).getItems().get(1).getValue(), is("2"));
//	}
//
//	@Test
//	public void setDiv2Test_true_else() {
//		SelectItemGroup selectItemGroup = new SelectItemGroup("1");
//		sut.div2.add(selectItemGroup);
//
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv2Nm("div2");
//		e.setDiv2Cd("2");
//		e.setDiv1Cd("1");
//		list.add(e);
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv2(e, auth);
//		assertTrue(actual);
//		assertThat(sut.div2.get(0).getItems().get(0).getLabel(), is("div2"));
//		assertThat(sut.div2.get(0).getItems().get(0).getValue(), is("2"));
//	}
//
//	@Test
//	public void setDiv2Test_false() {
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv2Nm(null);
//		e.setDiv2Cd("0");
//		String auth = Role.All.getCode();
//		boolean actual = sut.setDiv2(e, auth);
//
//		assertFalse(actual);
//	}
//
//	@Test
//	public void setDiv3Test_true() {
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv3Nm("div3");
//		e.setDiv3Cd("3");
//		list.add(e);
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv3(e, auth);
//		assertTrue(actual);
//
//		assertThat(sut.div3.get(0).getItems().get(0).getLabel(), is(""));
//		assertThat(sut.div3.get(0).getItems().get(0).getValue(), is(""));
//		assertThat(sut.div3.get(0).getItems().get(1).getLabel(), is("div3"));
//		assertThat(sut.div3.get(0).getItems().get(1).getValue(), is("3"));
//	}
//
//	@Test
//	public void setDiv3Test_true_else() {
//		SelectItemGroup selectItemGroup = new SelectItemGroup("2");
//		sut.div3.add(selectItemGroup);
//
//		List<EigyoSosikiDto> list = new ArrayList<EigyoSosikiDto>();
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv3Nm("div3");
//		e.setDiv3Cd("3");
//		e.setDiv2Cd("2");
//		list.add(e);
//		String auth = Role.All.getCode();
//
//		boolean actual = sut.setDiv3(e, auth);
//		assertTrue(actual);
//		assertThat(sut.div3.get(0).getItems().get(0).getLabel(), is("div3"));
//		assertThat(sut.div3.get(0).getItems().get(0).getValue(), is("3"));
//	}
//
//	@Test
//	public void setDiv3Test_false() {
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setDiv3Nm(null);
//		e.setDiv3Cd("0");
//		String auth = Role.All.getCode();
//		boolean actual = sut.setDiv3(e, auth);
//
//		assertFalse(actual);
//	}
//
//	@Test
//	public void setTantoTest() {
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setUserId("userId");
//		e.setLastName("lastName");
//		e.setFirstName("firstName");
//
//		List<SelectItemGroup> list = new ArrayList<SelectItemGroup>();
//		SelectItemGroup selectItemGroup = new SelectItemGroup("label");
//		list.add(selectItemGroup);
//		sut.tanto = list;
//		String auth = Role.All.getCode();
//
//		sut.setTanto(e, "label", auth);
//
//		assertThat(sut.tanto.get(0).getItems().get(0).getLabel(), is("lastName firstName"));
//		assertThat(sut.tanto.get(0).getItems().get(0).getValue(), is("userId"));
//	}
//
//	@Test
//	public void setTantoTest_else() {
//		EigyoSosikiDto e = new EigyoSosikiDto();
//		e.setUserId("userId");
//		e.setLastName("lastName");
//		e.setFirstName("firstName");
//		String auth = Role.All.getCode();
//
//		sut.setTanto(e, "label", auth);
//
//		assertThat(sut.tanto.get(0).getItems().get(0).getLabel(), is(""));
//		assertThat(sut.tanto.get(0).getItems().get(0).getValue(), is(""));
//		assertThat(sut.tanto.get(0).getItems().get(1).getLabel(), is("lastName firstName"));
//		assertThat(sut.tanto.get(0).getItems().get(1).getValue(), is("userId"));
//	}
}
