package jp.co.yrc.mv.helper;

import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_ALL;
import static jp.co.yrc.mv.common.Constants.SelectItrem.GROUP_VAL_NOT_BELONG;
import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL;
import static jp.co.yrc.mv.common.Constants.SelectItrem.OPTION_ALL_BLANK;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class SearchDivHelperTest extends DataSourceBasedDBTestCaseBase {

	@Autowired
	SearchDivHelper sut;

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/" + getClass().getSimpleName();

	@Rule
	public TestName testName = new TestName();

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	Authentication principal;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {

		File xls = new File(RESOURCE_PATH + testClassName + "/" + testName.getMethodName() + "_setup.xlsx");
		if (!xls.exists()) {
			xls = new File(RESOURCE_PATH + testClassName + "/setup.xlsx");
		}

		return new XlsDataSet(xls);
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
		UserInfoDto userInfoDto = new UserInfoDto();
		Authentication authentication = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		principal = authentication;
	}

	@Test
	public void 権限01で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj01");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.All.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL, Expected.DIV1_001, Expected.DIV1_002,
				Expected.DIV1_003, Expected.DIV1_010000, Expected.DIV1_020000);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002, Expected.All.DIV2_010000, Expected.All.DIV2_020000);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014, Expected.All.DIV3_010100,
				Expected.All.DIV3_010200, Expected.All.DIV3_020100, Expected.All.DIV3_020200);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_ALL,
				Expected.All.TANTO_001, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_002, Expected.All.TANTO_013,
				Expected.All.TANTO_115, Expected.All.TANTO_010000, Expected.All.TANTO_010100,
				Expected.All.TANTO_010101, Expected.All.TANTO_010102, Expected.All.TANTO_010200,
				Expected.All.TANTO_010201, Expected.All.TANTO_010202, Expected.All.TANTO_020000,
				Expected.All.TANTO_020100, Expected.All.TANTO_020101, Expected.All.TANTO_020102,
				Expected.All.TANTO_020200, Expected.All.TANTO_020201, Expected.All.TANTO_020202);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限02で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj02");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Multiple.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL, Expected.DIV1_001, Expected.DIV1_002);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_002, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_013, Expected.All.TANTO_115);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限03で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj031");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Company.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112, Expected.All.TANTO_113,
				Expected.All.TANTO_114);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限04で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj041");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Department.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_011,
				Expected.All.TANTO_111, Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限05で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj051");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.SalesOffice.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj061");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06の複数所属で全件表示のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj067");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111,
				Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限01で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj01");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.All.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL_BLANK, Expected.DIV1_001, Expected.DIV1_002,
				Expected.DIV1_003, Expected.DIV1_010000, Expected.DIV1_020000);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV2_001, Expected.AllBlank.DIV2_002, Expected.AllBlank.DIV2_010000,
				Expected.AllBlank.DIV2_020000);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV3_011, Expected.AllBlank.DIV3_012, Expected.AllBlank.DIV3_013,
				Expected.AllBlank.DIV3_014, Expected.AllBlank.DIV3_010100, Expected.AllBlank.DIV3_010200,
				Expected.AllBlank.DIV3_020100, Expected.AllBlank.DIV3_020200);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_ALL, Expected.AllBlank.TANTO_001, Expected.AllBlank.TANTO_011,
				Expected.AllBlank.TANTO_111, Expected.AllBlank.TANTO_112, Expected.AllBlank.TANTO_113,
				Expected.AllBlank.TANTO_114, Expected.AllBlank.TANTO_002, Expected.AllBlank.TANTO_013,
				Expected.AllBlank.TANTO_115, Expected.AllBlank.TANTO_010000, Expected.AllBlank.TANTO_010100,
				Expected.AllBlank.TANTO_010101, Expected.AllBlank.TANTO_010102, Expected.AllBlank.TANTO_010200,
				Expected.AllBlank.TANTO_010201, Expected.AllBlank.TANTO_010202, Expected.AllBlank.TANTO_020000,
				Expected.AllBlank.TANTO_020100, Expected.AllBlank.TANTO_020101, Expected.AllBlank.TANTO_020102,
				Expected.AllBlank.TANTO_020200, Expected.AllBlank.TANTO_020201, Expected.AllBlank.TANTO_020202);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限02で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj02");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Multiple.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL_BLANK, Expected.DIV1_001, Expected.DIV1_002);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV2_001, Expected.AllBlank.DIV2_002);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV3_011, Expected.AllBlank.DIV3_012, Expected.AllBlank.DIV3_013,
				Expected.AllBlank.DIV3_014);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_001, Expected.AllBlank.TANTO_002, Expected.AllBlank.TANTO_011,
				Expected.AllBlank.TANTO_111, Expected.AllBlank.TANTO_112, Expected.AllBlank.TANTO_113,
				Expected.AllBlank.TANTO_114, Expected.AllBlank.TANTO_013, Expected.AllBlank.TANTO_115);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限03で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj031");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Company.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV3_011, Expected.AllBlank.DIV3_012);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_001, Expected.AllBlank.TANTO_011, Expected.AllBlank.TANTO_111,
				Expected.AllBlank.TANTO_112, Expected.AllBlank.TANTO_113, Expected.AllBlank.TANTO_114);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限04で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj041");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Department.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.AllBlank.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_011, Expected.AllBlank.TANTO_111, Expected.AllBlank.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限05で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj051");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.SalesOffice.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.AllBlank.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.AllBlank.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj061");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.AllBlank.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.AllBlank.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06で複数所属で全件表示のないプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj067");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, true, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.AllBlank.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.AllBlank.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL),
				Expected.AllBlank.TANTO_111, Expected.AllBlank.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限01で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj01");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.All.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001, Expected.DIV1_002, Expected.DIV1_003,
				Expected.DIV1_010000, Expected.DIV1_020000);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002, Expected.All.DIV2_010000, Expected.All.DIV2_020000);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014, Expected.All.DIV3_010100,
				Expected.All.DIV3_010200, Expected.All.DIV3_020100, Expected.All.DIV3_020200);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_ALL,
				Expected.All.TANTO_001, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_002, Expected.All.TANTO_013,
				Expected.All.TANTO_115, Expected.All.TANTO_010000, Expected.All.TANTO_010100,
				Expected.All.TANTO_010101, Expected.All.TANTO_010102, Expected.All.TANTO_010200,
				Expected.All.TANTO_010201, Expected.All.TANTO_010202, Expected.All.TANTO_020000,
				Expected.All.TANTO_020100, Expected.All.TANTO_020101, Expected.All.TANTO_020102,
				Expected.All.TANTO_020200, Expected.All.TANTO_020201, Expected.All.TANTO_020202);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限02で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj02");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Multiple.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001, Expected.DIV1_002);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_002, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_013, Expected.All.TANTO_115);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限03で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj031");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Company.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112, Expected.All.TANTO_113,
				Expected.All.TANTO_114);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限04で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj041");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Department.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_011,
				Expected.All.TANTO_111, Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限05で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj051");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.SalesOffice.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj061");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限06の複数所属で全件表示のプルダウンからDiv1のALLを抜く() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj067");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
		sut.removeAllDiv1(model);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111,
				Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限01で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj01");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.All.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL, Expected.DIV1_001, Expected.DIV1_002,
				Expected.DIV1_003, Expected.DIV1_010000, Expected.DIV1_020000);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002, Expected.All.DIV2_010000, Expected.All.DIV2_020000);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014, Expected.All.DIV3_010100,
				Expected.All.DIV3_010200, Expected.All.DIV3_020100, Expected.All.DIV3_020200);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_ALL,
				Expected.All.TANTO_001, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_002, Expected.All.TANTO_013,
				Expected.All.TANTO_115, Expected.All.TANTO_010000, Expected.All.TANTO_010100,
				Expected.All.TANTO_010101, Expected.All.TANTO_010102, Expected.All.TANTO_010200,
				Expected.All.TANTO_010201, Expected.All.TANTO_010202, Expected.All.TANTO_020000,
				Expected.All.TANTO_020100, Expected.All.TANTO_020101, Expected.All.TANTO_020102,
				Expected.All.TANTO_020200, Expected.All.TANTO_020201, Expected.All.TANTO_020202);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限02で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj02");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Multiple.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(OPTION_ALL, Expected.DIV1_001, Expected.DIV1_002);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001,
				Expected.All.DIV2_002);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012, Expected.All.DIV3_013, Expected.All.DIV3_014);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_002, Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112,
				Expected.All.TANTO_113, Expected.All.TANTO_114, Expected.All.TANTO_013, Expected.All.TANTO_115);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限03で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj031");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Company.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112, Expected.All.TANTO_113,
				Expected.All.TANTO_114);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限04で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj041");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Department.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_011,
				Expected.All.TANTO_111, Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限05で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj051");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.SalesOffice.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限06で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj061");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void 権限06の複数所属で訪問一覧用のプルダウン生成() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj067");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Sales.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createVisitListSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(Expected.All.DIV3_011);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_111,
				Expected.All.TANTO_112);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	@Test
	public void 権限03で複数所属がある場合一つ分の所属しかプルダウン生成しない() {
		ExtendedModelMap model = new ExtendedModelMap();
		UserInfoDto userInfoDto = (UserInfoDto) principal.getPrincipal();
		userInfoDto.setId("ytj031");
		userInfoDto.setCorpCodes(Collections.singletonList("A040AZ"));
		userInfoDto.setAuthorization(Constants.Role.Company.getCode());

		Calendar c = Calendar.getInstance();
		c.setTime(DateUtils.getDBDate());
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		sut.createBasicSosiki(model, principal, c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);

		@SuppressWarnings("unchecked")
		List<SelectItem> actualDiv1 = (List<SelectItem>) model.get("div1");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv2 = (List<SelectItemGroup>) model.get("div2");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualDiv3 = (List<SelectItemGroup>) model.get("div3");
		@SuppressWarnings("unchecked")
		List<SelectItemGroup> actualTanto = (List<SelectItemGroup>) model.get("tanto");

		List<SelectItem> expectedDiv1 = Arrays.asList(Expected.DIV1_001);

		List<SelectItemGroup> expectedDiv2 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV2_001);

		List<SelectItemGroup> expectedDiv3 = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.DIV3_011,
				Expected.All.DIV3_012);

		List<SelectItemGroup> expectedTanto = Arrays.asList(new SelectItemGroup(GROUP_VAL_ALL), Expected.All.TANTO_001,
				Expected.All.TANTO_011, Expected.All.TANTO_111, Expected.All.TANTO_112, Expected.All.TANTO_113,
				Expected.All.TANTO_114);

		assertThat(actualDiv1, is(expectedDiv1));
		assertSelectItemGroup(actualDiv2, expectedDiv2);
		assertSelectItemGroup(actualDiv3, expectedDiv3);
		assertSelectItemGroup(actualTanto, expectedTanto);
	}

	static void assertSelectItemGroup(List<SelectItemGroup> actual, List<SelectItemGroup> expected) {

		assertThat(actual, is(expected));

		for (int i = 0; actual.size() < i; i++) {
			SelectItemGroup a = actual.get(i);
			SelectItemGroup e = expected.get(i);
			assertThat(a.getLabel(), is(e.getLabel()));
			assertThat(a.getValue(), is(e.getValue()));
			assertThat(a.getItems(), is(e.getItems()));
		}
	}

	static class Expected {

		static final SelectItem DIV1_001 = new SelectItem("（株）ヨコハマタイヤジャパン首都圏営業本部", "001");
		static final SelectItem DIV1_002 = new SelectItem("（株）ヨコハマタイヤジャパン関東甲信営業本部", "002");
		static final SelectItem DIV1_003 = new SelectItem("（株）ヨコハマタイヤジャパン近畿四国営業本部", "003");
		static final SelectItem DIV1_010000 = new SelectItem("010000営業本部", "010000");
		static final SelectItem DIV1_020000 = new SelectItem("020000営業本部", "020000");

		static class AllBlank {

			static final SelectItemGroup DIV2_001 = new SelectItemGroup("001");
			static {
				DIV2_001.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("東京カンパニー", "011"), new SelectItem(
						"神奈川カンパニー", "012")));
			}
			static final SelectItemGroup DIV2_002 = new SelectItemGroup("002");
			static {
				DIV2_002.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("山梨カンパニー", "013"), new SelectItem(
						"長野カンパニー", "014")));
			}
			static final SelectItemGroup DIV2_010000 = new SelectItemGroup("010000");
			static {
				DIV2_010000.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("010100カンパニー", "010100"),
						new SelectItem("010200カンパニー", "010200")));
			}
			static final SelectItemGroup DIV2_020000 = new SelectItemGroup("020000");
			static {
				DIV2_020000.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("020100カンパニー", "020100"),
						new SelectItem("020200カンパニー", "020200")));
			}

			static final SelectItemGroup DIV3_011 = new SelectItemGroup("011");
			static {
				DIV3_011.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("東京本社", "111"), new SelectItem(
						"品川営業所", "112")));
			}
			static final SelectItemGroup DIV3_012 = new SelectItemGroup("012");
			static {
				DIV3_012.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("神奈川本社", "113"), new SelectItem(
						"藤沢営業所", "114")));
			}

			static final SelectItemGroup DIV3_013 = new SelectItemGroup("013");
			static {
				DIV3_013.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("甲府営業所", "115"), new SelectItem(
						"甲府西営業所", "116")));
			}

			static final SelectItemGroup DIV3_014 = new SelectItemGroup("014");
			static {
				DIV3_014.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("長野中央営業所", "117"), new SelectItem(
						"長野営業所", "118")));
			}

			static final SelectItemGroup DIV3_010100 = new SelectItemGroup("010100");
			static {
				DIV3_014.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("010101営業所", "010101"),
						new SelectItem("010102営業所", "010102")));
			}

			static final SelectItemGroup DIV3_010200 = new SelectItemGroup("010200");
			static {
				DIV3_010200.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("010201営業所", "010201"),
						new SelectItem("010202営業所", "010202")));
			}

			static final SelectItemGroup DIV3_020100 = new SelectItemGroup("020100");
			static {
				DIV3_020100.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("020101営業所", "020101"),
						new SelectItem("020102営業所", "020102")));
			}

			static final SelectItemGroup DIV3_020200 = new SelectItemGroup("020200");
			static {
				DIV3_020200.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("020201営業所", "020201"),
						new SelectItem("020202営業所", "020202")));
			}

			static final SelectItemGroup TANTO_ALL = new SelectItemGroup(GROUP_VAL_NOT_BELONG);
			static {
				TANTO_ALL.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ全社 太郎", "ytj01")));
			}

			static final SelectItemGroup TANTO_001 = new SelectItemGroup("001");
			static {
				TANTO_001.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ複数販社 太郎", "ytj02"),
						new SelectItem("YTJ販社 太郎1", "ytj031")));
			}

			static final SelectItemGroup TANTO_011 = new SelectItemGroup("011");
			static {
				TANTO_011.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎1", "ytj041")));
			}

			static final SelectItemGroup TANTO_111 = new SelectItemGroup("111");
			static {
				TANTO_111.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ営,業所 太郎1", "ytj051"),
						new SelectItem("YTJセールス 太郎1", "ytj061"), new SelectItem("YTJセールス 太郎2", "ytj062"),
						new SelectItem("YTJセールス 太郎7", "ytj067"), new SelectItem("YTJセールス 太郎9", "ytj069")));
			}

			static final SelectItemGroup TANTO_112 = new SelectItemGroup("112");
			static {
				TANTO_112.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ営業所 太郎2", "ytj052"),
						new SelectItem("YTJセールス 太郎3", "ytj063"), new SelectItem("YTJセールス 太郎4", "ytj064"),
						new SelectItem("YTJセールス 太郎7", "ytj067")));
			}

			static final SelectItemGroup TANTO_012 = new SelectItemGroup("012");
			static {
				TANTO_012.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎1", "ytj041")));
			}

			static final SelectItemGroup TANTO_113 = new SelectItemGroup("113");
			static {
				TANTO_113.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("藤沢 太郎", "ytj088")));
			}

			static final SelectItemGroup TANTO_114 = new SelectItemGroup("114");
			static {
				TANTO_114.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("藤沢 太郎", "ytj088")));
			}

			static final SelectItemGroup TANTO_002 = new SelectItemGroup("002");
			static {
				TANTO_002.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ複数販社 太郎", "ytj02"),
						new SelectItem("YTJ販社 太郎2", "ytj032")));
			}

			static final SelectItemGroup TANTO_013 = new SelectItemGroup("013");
			static {
				TANTO_013.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎2", "ytj042")));
			}

			static final SelectItemGroup TANTO_115 = new SelectItemGroup("115");
			static {
				TANTO_115.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ営業所 太郎3", "ytj053"),
						new SelectItem("YTJセールス 太郎5", "ytj065"), new SelectItem("YTJセールス 太郎6", "ytj066")));
			}

			static final SelectItemGroup TANTO_010000 = new SelectItemGroup("010000");
			static {
				TANTO_010000.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ販社 太郎010000_01",
						"ytj010000_01")));
			}

			static final SelectItemGroup TANTO_010100 = new SelectItemGroup("010100");
			static {
				TANTO_010100.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎010100_01",
						"ytj010100_01")));
			}

			static final SelectItemGroup TANTO_010101 = new SelectItemGroup("010101");
			static {
				TANTO_010101.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎010101_00", "ytj010101_00"), new SelectItem("セールス 太郎010101_01",
								"ytj010101_01")));
			}

			static final SelectItemGroup TANTO_010102 = new SelectItemGroup("010102");
			static {
				TANTO_010102.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎010102_00", "ytj010102_00"), new SelectItem("セールス 太郎010102_01",
								"ytj010102_01")));
			}

			static final SelectItemGroup TANTO_010200 = new SelectItemGroup("010200");
			static {
				TANTO_010200.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎010200_01",
						"ytj010200_01")));
			}
			static final SelectItemGroup TANTO_010201 = new SelectItemGroup("010201");
			static {
				TANTO_010201.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎010201_00", "ytj010201_00"), new SelectItem("セールス 太郎010201_01",
								"ytj010201_01")));
			}
			static final SelectItemGroup TANTO_010202 = new SelectItemGroup("010202");
			static {
				TANTO_010202.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎010202_00", "ytj010202_00"), new SelectItem("セールス 太郎010202_01",
								"ytj010202_01")));
			}

			static final SelectItemGroup TANTO_020000 = new SelectItemGroup("020000");
			static {
				TANTO_020000.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ販社 太郎020000_01",
						"ytj020000_01")));
			}

			static final SelectItemGroup TANTO_020100 = new SelectItemGroup("020100");
			static {
				TANTO_020100.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎020100_01",
						"ytj020100_01")));
			}

			static final SelectItemGroup TANTO_020101 = new SelectItemGroup("020101");
			static {
				TANTO_020101.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎020101_00", "ytj020101_00"), new SelectItem("セールス 太郎020101_01",
								"ytj020101_01")));
			}

			static final SelectItemGroup TANTO_020102 = new SelectItemGroup("020102");
			static {
				TANTO_020102.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎020102_00", "ytj020102_00"), new SelectItem("セールス 太郎020102_01",
								"ytj020102_01")));
			}

			static final SelectItemGroup TANTO_020200 = new SelectItemGroup("020200");
			static {
				TANTO_020200.setItems(Arrays.asList(OPTION_ALL_BLANK, new SelectItem("YTJ部門 太郎020200_01",
						"ytj020200_01")));
			}
			static final SelectItemGroup TANTO_020201 = new SelectItemGroup("020201");
			static {
				TANTO_020201.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎020201_00", "ytj020201_00"), new SelectItem("セールス 太郎020201_01",
								"ytj020201_01")));
			}
			static final SelectItemGroup TANTO_020202 = new SelectItemGroup("020202");
			static {
				TANTO_020202.setItems(Arrays.asList(OPTION_ALL_BLANK,
						new SelectItem("セールス 太郎020202_00", "ytj020202_00"), new SelectItem("セールス 太郎020202_01",
								"ytj020202_01")));
			}
		}

		static class All {
			static final SelectItemGroup DIV2_001 = new SelectItemGroup("001");
			static {
				DIV2_001.setItems(Arrays.asList(OPTION_ALL, new SelectItem("東京カンパニー", "011"), new SelectItem(
						"神奈川カンパニー", "012")));
			}
			static final SelectItemGroup DIV2_002 = new SelectItemGroup("002");
			static {
				DIV2_002.setItems(Arrays.asList(OPTION_ALL, new SelectItem("山梨カンパニー", "013"), new SelectItem("長野カンパニー",
						"014")));
			}
			static final SelectItemGroup DIV2_010000 = new SelectItemGroup("010000");
			static {
				DIV2_010000.setItems(Arrays.asList(OPTION_ALL, new SelectItem("010100カンパニー", "010100"), new SelectItem(
						"010200カンパニー", "010200")));
			}
			static final SelectItemGroup DIV2_020000 = new SelectItemGroup("020000");
			static {
				DIV2_020000.setItems(Arrays.asList(OPTION_ALL, new SelectItem("020100カンパニー", "020100"), new SelectItem(
						"020200カンパニー", "020200")));
			}

			static final SelectItemGroup DIV3_011 = new SelectItemGroup("011");
			static {
				DIV3_011.setItems(Arrays.asList(OPTION_ALL, new SelectItem("東京本社", "111"), new SelectItem("品川営業所",
						"112")));
			}
			static final SelectItemGroup DIV3_012 = new SelectItemGroup("012");
			static {
				DIV3_012.setItems(Arrays.asList(OPTION_ALL, new SelectItem("神奈川本社", "113"), new SelectItem("藤沢営業所",
						"114")));
			}

			static final SelectItemGroup DIV3_013 = new SelectItemGroup("013");
			static {
				DIV3_013.setItems(Arrays.asList(OPTION_ALL, new SelectItem("甲府営業所", "115"), new SelectItem("甲府西営業所",
						"116")));
			}

			static final SelectItemGroup DIV3_014 = new SelectItemGroup("014");
			static {
				DIV3_014.setItems(Arrays.asList(OPTION_ALL, new SelectItem("長野中央営業所", "117"), new SelectItem("長野営業所",
						"118")));
			}

			static final SelectItemGroup DIV3_010100 = new SelectItemGroup("010100");
			static {
				DIV3_014.setItems(Arrays.asList(OPTION_ALL, new SelectItem("010101営業所", "010101"), new SelectItem(
						"010102営業所", "010102")));
			}

			static final SelectItemGroup DIV3_010200 = new SelectItemGroup("010200");
			static {
				DIV3_010200.setItems(Arrays.asList(OPTION_ALL, new SelectItem("010201営業所", "010201"), new SelectItem(
						"010202営業所", "010202")));
			}

			static final SelectItemGroup DIV3_020100 = new SelectItemGroup("020100");
			static {
				DIV3_020100.setItems(Arrays.asList(OPTION_ALL, new SelectItem("020101営業所", "020101"), new SelectItem(
						"020102営業所", "020102")));
			}

			static final SelectItemGroup DIV3_020200 = new SelectItemGroup("020200");
			static {
				DIV3_020200.setItems(Arrays.asList(OPTION_ALL, new SelectItem("020201営業所", "020201"), new SelectItem(
						"020202営業所", "020202")));
			}

			static final SelectItemGroup TANTO_ALL = new SelectItemGroup(GROUP_VAL_NOT_BELONG);
			static {
				TANTO_ALL.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ全社 太郎", "ytj01")));
			}

			static final SelectItemGroup TANTO_001 = new SelectItemGroup("001");
			static {
				TANTO_001.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ複数販社 太郎", "ytj02"), new SelectItem(
						"YTJ販社 太郎1", "ytj031")));
			}

			static final SelectItemGroup TANTO_011 = new SelectItemGroup("011");
			static {
				TANTO_011.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎1", "ytj041")));
			}

			static final SelectItemGroup TANTO_111 = new SelectItemGroup("111");
			static {
				TANTO_111.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ営,業所 太郎1", "ytj051"), new SelectItem(
						"YTJセールス 太郎1", "ytj061"), new SelectItem("YTJセールス 太郎2", "ytj062"), new SelectItem(
						"YTJセールス 太郎7", "ytj067"), new SelectItem("YTJセールス 太郎9", "ytj069")));
			}

			static final SelectItemGroup TANTO_112 = new SelectItemGroup("112");
			static {
				TANTO_112.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ営業所 太郎2", "ytj052"), new SelectItem(
						"YTJセールス 太郎3", "ytj063"), new SelectItem("YTJセールス 太郎4", "ytj064"), new SelectItem(
						"YTJセールス 太郎7", "ytj067")));
			}

			static final SelectItemGroup TANTO_012 = new SelectItemGroup("012");
			static {
				TANTO_012.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎1", "ytj041")));
			}

			static final SelectItemGroup TANTO_113 = new SelectItemGroup("113");
			static {
				TANTO_113.setItems(Arrays.asList(OPTION_ALL, new SelectItem("藤沢 太郎", "ytj088")));
			}

			static final SelectItemGroup TANTO_114 = new SelectItemGroup("114");
			static {
				TANTO_114.setItems(Arrays.asList(OPTION_ALL, new SelectItem("藤沢 太郎", "ytj088")));
			}

			static final SelectItemGroup TANTO_002 = new SelectItemGroup("002");
			static {
				TANTO_002.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ複数販社 太郎", "ytj02"), new SelectItem(
						"YTJ販社 太郎2", "ytj032")));
			}

			static final SelectItemGroup TANTO_013 = new SelectItemGroup("013");
			static {
				TANTO_013.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎2", "ytj042")));
			}

			static final SelectItemGroup TANTO_115 = new SelectItemGroup("115");
			static {
				TANTO_115.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ営業所 太郎3", "ytj053"), new SelectItem(
						"YTJセールス 太郎5", "ytj065"), new SelectItem("YTJセールス 太郎6", "ytj066")));
			}

			static final SelectItemGroup TANTO_010000 = new SelectItemGroup("010000");
			static {
				TANTO_010000.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ販社 太郎010000_01", "ytj010000_01")));
			}

			static final SelectItemGroup TANTO_010100 = new SelectItemGroup("010100");
			static {
				TANTO_010100.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎010100_01", "ytj010100_01")));
			}

			static final SelectItemGroup TANTO_010101 = new SelectItemGroup("010101");
			static {
				TANTO_010101.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎010101_00", "ytj010101_00"),
						new SelectItem("セールス 太郎010101_01", "ytj010101_01")));
			}

			static final SelectItemGroup TANTO_010102 = new SelectItemGroup("010102");
			static {
				TANTO_010102.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎010102_00", "ytj010102_00"),
						new SelectItem("セールス 太郎010102_01", "ytj010102_01")));
			}

			static final SelectItemGroup TANTO_010200 = new SelectItemGroup("010200");
			static {
				TANTO_010200.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎010200_01", "ytj010200_01")));
			}
			static final SelectItemGroup TANTO_010201 = new SelectItemGroup("010201");
			static {
				TANTO_010201.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎010201_00", "ytj010201_00"),
						new SelectItem("セールス 太郎010201_01", "ytj010201_01")));
			}
			static final SelectItemGroup TANTO_010202 = new SelectItemGroup("010202");
			static {
				TANTO_010202.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎010202_00", "ytj010202_00"),
						new SelectItem("セールス 太郎010202_01", "ytj010202_01")));
			}

			static final SelectItemGroup TANTO_020000 = new SelectItemGroup("020000");
			static {
				TANTO_020000.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ販社 太郎020000_01", "ytj020000_01")));
			}

			static final SelectItemGroup TANTO_020100 = new SelectItemGroup("020100");
			static {
				TANTO_020100.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎020100_01", "ytj020100_01")));
			}

			static final SelectItemGroup TANTO_020101 = new SelectItemGroup("020101");
			static {
				TANTO_020101.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎020101_00", "ytj020101_00"),
						new SelectItem("セールス 太郎020101_01", "ytj020101_01")));
			}

			static final SelectItemGroup TANTO_020102 = new SelectItemGroup("020102");
			static {
				TANTO_020102.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎020102_00", "ytj020102_00"),
						new SelectItem("セールス 太郎020102_01", "ytj020102_01")));
			}

			static final SelectItemGroup TANTO_020200 = new SelectItemGroup("020200");
			static {
				TANTO_020200.setItems(Arrays.asList(OPTION_ALL, new SelectItem("YTJ部門 太郎020200_01", "ytj020200_01")));
			}
			static final SelectItemGroup TANTO_020201 = new SelectItemGroup("020201");
			static {
				TANTO_020201.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎020201_00", "ytj020201_00"),
						new SelectItem("セールス 太郎020201_01", "ytj020201_01")));
			}
			static final SelectItemGroup TANTO_020202 = new SelectItemGroup("020202");
			static {
				TANTO_020202.setItems(Arrays.asList(OPTION_ALL, new SelectItem("セールス 太郎020202_00", "ytj020202_00"),
						new SelectItem("セールス 太郎020202_01", "ytj020202_01")));
			}
		}
	}
}
