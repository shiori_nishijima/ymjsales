package jp.co.yrc.mv.web

import jp.co.yrc.mv.web.AccountGroupingVisitListController;
import jp.co.yrc.mv.web.AccountGroupingVisitListController.SearchCondition;
import jp.co.yrc.mv.web.AccountGroupingVisitListController.DayInfo;
import jp.co.yrc.mv.web.AccountGroupingVisitListController.VisitInfo
import jp.co.yrc.mv.web.VisitPlanListController.DailyCallInfo;
import jp.co.yrc.mv.web.AccountGroupingVisitListController.AccountVisit;

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit;
import jp.co.yrc.mv.html.SelectItem

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.ImportDocument.Import
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ContextConfiguration

import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CallsDto
import jp.co.yrc.mv.dto.CallsUsersDto
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.SelectionItems;

import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import spock.lang.Ignore;
import mockit.MockUp


/**
 * 得意先グルーピング別訪問実績画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class AccountGroupingVisitListControllerSpec {
	/**
	 * 得意先グルーピング別訪問実績画面の初期表示時のテスト
	 * 対象メソッド：init
	 * */
	static class AccountGroupingVisitListControllerSpec_初期表示 extends DBSpecification {
		@Autowired
		AccountGroupingVisitListController sut;

		void setupSpec() {
			// 現在日時を 2016/09/03 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/09/03 00:00:00"))
							return cal
						}
					}
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();

			when:
			String actual = sut.init(param, model, principal);

			then:
			actual == "accountGroupingVisitList";
		}

		def "引数として渡したパラメータに現在年月が設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.init(param, model, principal);

			then:
			def p = model.get("searchCondition");
			p == param; // モデルから取得したparamが引数として渡したものと同じものであること
			p.year == 2016;
			p.month == 9;
		}

		def "modelに空の得意先リストが設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.init(param, model, principal);

			then:
			model.get("accountVisitList").size() == 0;
		}

		def "modelにグルーピングリストが設定される"() {
			setup:
			def param = new SearchCondition();

			def List<SelectItem> expected = new ArrayList<>();

			def s = new SelectItem("重点", "GROUP_IMPORTANT");
			expected << s;
			s = new SelectItem("新規", "GROUP_NEW");
			expected << s;
			s = new SelectItem("深耕", "GROUP_DEEP_PLOWING");
			expected << s;
			s = new SelectItem("Y-CP", "GROUP_Y_CP");
			expected << s;
			s = new SelectItem("他社CP", "GROUP_OTHER_CP");
			expected << s;
			s = new SelectItem("G1 ", "GROUP_ONE");
			expected << s;
			s = new SelectItem("G2", "GROUP_TWO");
			expected << s;
			s = new SelectItem("G3", "GROUP_THREE");
			expected << s;
			s = new SelectItem("G4", "GROUP_FOUR");
			expected << s;
			s = new SelectItem("G5", "GROUP_FIVE");
			expected << s;
			s = new SelectItem("A", "A");
			expected << s;
			s = new SelectItem("B", "B");
			expected << s;
			s = new SelectItem("C", "C");
			expected << s;

			when:
			sut.init(param, model, principal);

			then:
			model.get("groups").eachWithIndex { it, i ->
				def e = expected.get(i)
				assert it.label == e.label;
				assert it.value == e.value;
			}
		}

		def "modelに現在年月に紐づく日付リストが設定される"() {
			setup:
			def param = new SearchCondition();
			def expected = createDayInfoList();

			when:
			sut.init(param, model, principal);

			then:
			model.get("dayInfoList").size() == 30; // 9月は30日まで
			model.get("dayInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.date)
				assert it.date == e.date;
				assert it.dayOfWeek == e.dayOfWeek;
				assert it.dayOfWeekLabel == e.dayOfWeekLabel;
			}
		}

		def "modelに年月リストが設定される"() {
			setup:
			def param = new SearchCondition();
			def years = [2017, 2016, 2015, 2014]; // 現在年+1年-2年
			def months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

			when:
			sut.init(param, model, principal);

			then:
			model.get("years").eachWithIndex { it, i ->
				def e = years.get(i)
				assert it == e;
			}
			model.get("months").eachWithIndex { it, i ->
				def e = months.get(i)
				assert it == e;
			}
		}
	}

	/**
	 * 得意先グルーピング別実績訪問画面の初期表示時のテスト
	 * 検索対象の得意先：訪問得意先かつ担当得意先に該当する得意先
	 * 検索対象の訪問：得意先に紐づく担当者の訪問
	 * 対象メソッド：find
	 * */
	static class AccountGroupingVisitListControllerSpec_検索押下 extends DBSpecification {
		@Autowired
		AccountGroupingVisitListController sut;

		void setupSpec() {
			setSalesUser();
			// 現在日時を 2016/09/03 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/09/03 00:00:00"))
							return cal
						}
					}
		}

		/** 営業ユーザ */
		void setSalesUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj061");
			userInfoDto.setLastName("YTJセールス");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("06"); // 営業権限
			userInfoDto.setCorpCodes(["A040AZ", "Y00300"]);
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT";

			when:
			String actual = sut.find(param, model, principal);

			then:
			actual == "accountGroupingVisitList";
		}

		def "modelに引数として渡したパラメータが設定される"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT";

			when:
			sut.find(param, model, principal);

			then:
			def p = model.get("searchCondition");
			p == param; // モデルから取得したparamが引数として渡したものと同じものであること
			p.div1 == param.div1;
			p.div2 == param.div2;
			p.div3 == param.div3;
			p.tanto == param.tanto;
			p.year == param.year;
			p.month == param.month;
			p.group == param.group;
		}

		def "modelにグルーピングリストが設定される"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT";

			def List<SelectItem> expected = new ArrayList<>();

			def s = new SelectItem("重点", "GROUP_IMPORTANT");
			expected << s;
			s = new SelectItem("新規", "GROUP_NEW");
			expected << s;
			s = new SelectItem("深耕", "GROUP_DEEP_PLOWING");
			expected << s;
			s = new SelectItem("Y-CP", "GROUP_Y_CP");
			expected << s;
			s = new SelectItem("他社CP", "GROUP_OTHER_CP");
			expected << s;
			s = new SelectItem("G1 ", "GROUP_ONE");
			expected << s;
			s = new SelectItem("G2", "GROUP_TWO");
			expected << s;
			s = new SelectItem("G3", "GROUP_THREE");
			expected << s;
			s = new SelectItem("G4", "GROUP_FOUR");
			expected << s;
			s = new SelectItem("G5", "GROUP_FIVE");
			expected << s;
			s = new SelectItem("A", "A");
			expected << s;
			s = new SelectItem("B", "B");
			expected << s;
			s = new SelectItem("C", "C");
			expected << s;

			when:
			sut.find(param, model, principal);

			then:
			model.get("groups").eachWithIndex { it, i ->
				def e = expected.get(i)
				assert it.label == e.label;
				assert it.value == e.value;
			}
		}

		def "modelに現在年月に紐づく日付リストが設定される"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT";

			def expected = createDayInfoList();

			when:
			sut.find(param, model, principal);

			then:
			model.get("dayInfoList").size() == 30; // 9月は30日まで
			model.get("dayInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.date)
				assert it.date == e.date;
				assert it.dayOfWeek == e.dayOfWeek;
				assert it.dayOfWeekLabel == e.dayOfWeekLabel;
			}
		}

		def "modelに年月リストが設定される"() {
			setup:
			def param = new SearchCondition();
			def years = [2017, 2016, 2015, 2014]; // 現在年+1年-2年
			def months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT";

			when:
			sut.find(param, model, principal);

			then:
			model.get("years").eachWithIndex { it, i ->
				def e = years.get(i)
				assert it == e;
			}
			model.get("months").eachWithIndex { it, i ->
				def e = months.get(i)
				assert it == e;
			}
		}

		/**
		 * 検索条件：営業所→東京本社、担当者→セールス太郎１
		 * 全ての日付に訪問と同行者が設定されていることを確認する
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合_担当者固定_東京本社"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "得意先_計画あり・実績あり";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 30; // 実績のある訪問のみを取得する
			r.visitInfoList = createVisitInfoList(30, "YTJ部門太郎1"); // 社内同行者

			expected[r.accountId] = r;

			r = new AccountVisit();
			r.accountId = "2000000003";
			r.accountName = "得意先_計画なし・実績あり";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 30;
			r.visitInfoList = createVisitInfoList(30, "YTJ販社太郎1"); // 社内同行者

			expected[r.accountId] = r;

			r = new AccountVisit();
			r.accountId = "2000000005";
			r.accountName = "得意先_計画あり・実績なし";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 0; // 得意先に紐づく訪問が計画のみの場合は0が設定される
			r.visitInfoList = [];

			expected[r.accountId] = r;

			r = new AccountVisit();
			r.accountId = "9000000001";
			r.accountName = "得意先_計画なし・実績なし";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 0; // 得意先に紐づく訪問が存在しない場合は0が設定される
			r.visitInfoList = [];

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 4;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				if (e.visitInfoList.size != 0) {
					it.visitInfoList.eachWithIndex {info, j ->
						def d = e.visitInfoList.get(j);
						assert info.date == d.date;
						assert info.divisionC == d.divisionC;
						assert info.companionNames == d.companionNames;
					}
				}
			}
		}

		/**
		 * 検索条件：営業所→東京官庁、担当者→営業所太郎１
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合_担当者固定_東京官庁"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1101";
			param.tanto = "ytj051";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "営業所太郎の得意先（訪問得意先○）";
			r.lastName = "YTJ営業所";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京官庁";
			r.rowHeldCount = 3; // 実績のある訪問のみを取得する
			r.visitInfoList = createVisitInfoList(3, "YTJ部門太郎1");

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 検索条件：営業所→東京本社、担当者→全て
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 5;
			r.visitInfoList = createVisitInfoList(5, "");

			expected[r.accountId] = r;

			r = new AccountVisit();
			r.accountId = "2000000002";
			r.accountName = "営業所太郎の得意先";
			r.lastName = "YTJ営業所";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 3;
			r.visitInfoList = createVisitInfoList(3, "");

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 2;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 検索条件：グルーピング→G1（デフォルトの重点以外）
		 * */
		@DBUnit
		def "グルーピングを指定して検索した場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_ONE"; // G1

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先（訪問得意先○）";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 5;
			r.visitInfoList = createVisitInfoList(5, ""); // 社内同行者

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 検索条件：２０１６年８月（現在月-1）
		 * */
		@DBUnit
		def "前月を指定して検索した場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 8;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先（訪問得意先○）";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 5;
			r.visitInfoList = createVisitInfoList(5, ""); // 社内同行者

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 期待結果：
		 * 訪問が正常にカウントされること
		 * 複数の訪問の訪問理由が同じ場合は重複して設定されないこと
		 * 複数の訪問の社内同行者が同じ場合は重複して設定されないこと
		 */
		@DBUnit
		def "１日に複数の訪問が存在する場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先（訪問得意先○）";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 5; // 訪問件数は５件
			r.visitInfoList = createVisitInfoList(2, "YTJ部門太郎1"); // 訪問の存在する日数は２日

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 全ての訪問理由が設定されること
		 * */
		@DBUnit
		def "訪問理由が複数存在する場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先（訪問得意先○）";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 4; // １日の訪問３件＋３日の訪問１件

			def v = new VisitInfo();
			v.date = 1;
			v.divisionC = "訪問：新規開拓<br/> 訪問：定常<br/> 訪問：商談あり"; // 異なる訪問理由の訪問が３つ
			v.companionNames = "";
			r.visitInfoList << v;

			v = new VisitInfo();
			v.date = 3;
			v.divisionC = "イベント：担当得意先<br/> イベント：担当外得意先<br/> イベント：試乗会等"; // １つの訪問に対して複数の訪問理由
			v.companionNames = "";
			r.visitInfoList << v;

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 複数の社内同行者が設定されること
		 * */
		@DBUnit
		def "社内同行者が複数いる場合"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			def expected = [:];

			def r = new AccountVisit();
			r.accountId = "2000000001";
			r.accountName = "セールス太郎の得意先（訪問得意先○）";
			r.lastName = "YTJセールス";
			r.firstName = "太郎1";
			r.div2Nm = "東京Co";
			r.div3Nm = "東京本社";
			r.rowHeldCount = 4; // １日の訪問３件＋３日の訪問１件

			def v = new VisitInfo();
			v.date = 1;
			v.divisionC = "訪問：定常";
			// 異なる社内同行者を持つ訪問が３件
			v.companionNames = "YTJ複数販社太郎<br/> YTJ全社太郎<br/> YTJ販社太郎1";
			r.visitInfoList << v;

			v = new VisitInfo();
			v.date = 3;
			v.divisionC = "訪問：定常";
			// １つの訪問に対して複数の社内同行者
			v.companionNames = "YTJ部門太郎1<br/> YTJ営業所太郎1<br/> YTJセールス太郎2";
			r.visitInfoList << v;

			expected[r.accountId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 1;
			model.get("accountVisitList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountId == e.accountId;
				assert it.accountName == e.accountName;
				assert it.lastName == e.lastName;
				assert it.firstName == e.firstName;
				assert it.div2Nm == e.div2Nm;
				assert it.div3Nm == e.div3Nm;
				assert it.rowHeldCount == e.rowHeldCount;
				it.visitInfoList.eachWithIndex {info, j ->
					def d = e.visitInfoList.get(j);
					assert info.date == d.date;
					assert info.divisionC == d.divisionC;
					assert info.companionNames == d.companionNames;
				}
			}
		}

		/**
		 * 検索条件：ytj061（セールス太郎）
		 * 期待結果：
		 * セールス太郎の担当得意先１件（訪問得意先×）が得意先リストに設定されないこと
		 * 得意先が取得できない場合は空の得意先リストが設定されること
		 * */
		@DBUnit
		def "担当得意先で訪問得意先に登録していない得意先は設定されない"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 0;
		}

		/**
		 * 検索条件：ytj061（セールス太郎）
		 * 期待結果：
		 * セールス太郎の担当外得意先１件（訪問得意先○）が得意先リストに設定されないこと
		 * 得意先が取得できない場合は空の得意先リストが設定されること
		 * */
		@DBUnit
		def "担当外得意先で訪問得意先に登録している得意先は設定されない"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 0;
		}

		/**
		 * 検索結果：得意先の取得件数が3000件より多い
		 * */
		@DBUnit
		def "検索結果が3000件を超えた場合はlimitOverにtrueが設定される"() {
			setup:
			def param = new SearchCondition();
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.year = 2016;
			param.month = 9;
			param.group = "GROUP_IMPORTANT"; // 重点

			when:
			sut.find(param, model, principal);

			then:
			model.get("accountVisitList").size() == 0;
			model.get("limitOver") == true;
		}
	}

	/**
	 * dayInfoListのexpectedを生成する
	 * */
	static Map<Integer, String> createDayInfoList() {
		Map<Integer, DayInfo> expected = new TreeMap<>();

		DayInfo info = new DayInfo();
		info.date = 0;
		info.dayOfWeek = 0;
		info.dayOfWeekLabel = null;

		expected.put(0, info);

		int dayOfWeek = 5;
		Map<Integer, String> map = new TreeMap<>();
		map.put(1, "日");
		map.put(2, "月");
		map.put(3, "火");
		map.put(4, "水");
		map.put(5, "木");
		map.put(6, "金");
		map.put(7, "土");

		for (int i = 1; i <= 31; i++) {
			info = new DayInfo();
			info.date = i;
			info.dayOfWeek = dayOfWeek;
			info.dayOfWeekLabel = map.get(dayOfWeek);
			expected.put(i, info);

			if (dayOfWeek != 7) {
				dayOfWeek++;
			} else {
				dayOfWeek = 1;
			}
		}
		return expected;
	}

	/**
	 * 
	 * */
	static List<VisitInfo> createVisitInfoList(int count, String companionNames) {
		List<VisitInfo> list = new ArrayList<>();
		for (int i = 1; i <= count; i++) {
			VisitInfo info = new VisitInfo();
			info.date = i;
			info.divisionC = "訪問：定常";
			info.companionNames = companionNames;
			list.add(info);
		}
		return list;
	}
}
