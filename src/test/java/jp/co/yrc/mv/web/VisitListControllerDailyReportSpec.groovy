package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit;
import jp.co.yrc.mv.html.SelectItem
import jp.co.yrc.mv.web.VisitListController.DailyReportParam;
import jp.co.yrc.mv.web.VisitListController.SearchResult;
import jp.co.yrc.mv.web.VisitListController.VisitListSearchCondition

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.ImportDocument.Import
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ContextConfiguration

import mockit.MockUp
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CallsDto
import jp.co.yrc.mv.dto.CallsUsersDto
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.SelectionItems;

import org.junit.runner.RunWith;
import org.junit.experimental.runners.Enclosed;
import spock.lang.Ignore;


/**
 * 日報詳細（管理者）、日報詳細（セールス）画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class VisitListControllerDailyReportSpec {

	/**
	 * 日報詳細（管理者）画面の検索処理のテスト
	 * 対象メソッド：findDailyReportAdmin
	 * */
	static class VisitListControllerDailyReportSpec_日報詳細（管理者）検索処理 extends DBSpecification {

		@Autowired
		VisitListController sut;

		void setupSpec() {
			// 現在日時を 2016/07/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/07/09 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");

			when:
			String actual = sut.findDailyReportAdmin(param, model, principal);

			then:
			actual == "visitList";
		}

		def "モードがdailyReportAdminに設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			model.get("mode") == "dailyReportAdmin";
		}

		/**
		 * loginUser：署名用ログインユーザ情報
		 * */
		def "modelにloginUserが設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			def loginUser = model.get("loginUser");
			loginUser.firstName == "太郎";
			loginUser.lastName == "YTJ全社";
			loginUser.eigyoSosikiNm == "YTST";
		}

		def "modelにuserが設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			def user = model.get("user");
			user.firstName == "太郎1";
			user.lastName == "YTJセールス";
			user.eigyoSosikiNm == "Bangkok";
		}

		/**
		 * comment：セールス、上司日報コメント
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索結果：セールスコメントあり、上司コメントあり
		 * */
		@DBUnit
		def "modelにcommentが設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			def comment = model.get("comment");
			comment.id == "168f537a-edb2-76a4-cd0e-2c14604ac917";
			comment.year == 2016;
			comment.month == 8;
			comment.date == 17;
			comment.userComment == "8月17日の日報コメントを入力しました。";
			comment.bossComment == "全社太郎が見ましたよっと\n2016/08/17　YTJ全社太郎　ヨコハマタイヤジャパン\n\n販社太郎も見ましたよっと\n2016/08/17　YTJ販社太郎1　Ｊ首都圏";
			comment.userId == "ytj061";
			comment.createdBy == "ytj061";
			comment.completed == false;
			comment.deleted == false;
			comment.firstName == "太郎1";
			comment.lastName == "YTJセールス";
			comment.eigyoSosikiNm == "Bangkok";
		}

		/**
		 * comment：セールス、上司日報コメント
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索結果：セールスコメントなし、上司コメントなし
		 * */
		@DBUnit
		def "modelにcommentが設定される_commentが存在しない場合はuserIdのみ設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			def comment = model.get("comment");
			comment.id == null;
			comment.year == null;
			comment.month == null;
			comment.date == null;
			comment.userComment == null;
			comment.bossComment == null;
			comment.userId == "ytj061"; // userIdのみ設定される
			comment.createdBy == null;
			comment.completed == false;
			comment.deleted == false;
			comment.firstName == null;
			comment.lastName == null;
			comment.eigyoSosikiNm == null;
		}

		/**
		 * 担当得意先
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索結果：１件
		 * */
		@DBUnit
		def "自分が担当している得意先の訪問が取得できていること"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "17037845-5291-d3ac-85c4-20e03da7ae09";
			r.callId = null;
			r.name = "実績_0817";
			r.accountsName = "得意先1"; // 担当得意先
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容";
			r.read = false;
			r.commentReadCount = -1;
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['実績報告'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "商品関連";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "YTST";
			r.accountSalesOfficeName = "Bangkok";

			expected[r.id] = r;

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 担当外得意先
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索対象：8月17日　YTJ部門太郎1の日報（同行者としてYTJセールス太郎1）
		 * 検索結果：１件
		 * */
		@DBUnit
		def "社内同行者として登録されている訪問が取得できていること"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "2962010b-d148-d3b5-0386-2431c62050b4";
			r.callId = null;
			r.name = "部門太郎の実績_セールス太郎が同行";
			r.accountsName = "得意先_部門太郎"; // 部門太郎の得意先
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容";
			r.read = false;
			r.commentReadCount = -1;
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ部門";
			r.divisionC = ['実績報告'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "商品関連";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "YTST";
			r.accountSalesOfficeName = "Bangkok";

			expected[r.id] = r;

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 担当外得意先
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索対象：8月17日　YTJセールス太郎1の日報（担当外得意先への訪問）
		 * 検索結果：１件
		 * */
		@DBUnit
		def "担当者として登録されている訪問が取得できていること_営業組織が同じかつ担当していない得意先の場合"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "2a510aa7-c347-19a5-7454-1a46a2902d8b";
			r.callId = null;
			r.name = "セールス太郎の実績_部門太郎の得意先";
			r.accountsName = "得意先_部門太郎"; // 部門太郎の得意先
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容";
			r.read = false;
			r.commentReadCount = -1;
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['実績報告'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "商品関連";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "YTST";
			r.accountSalesOfficeName = "Bangkok";

			expected[r.id] = r;

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 担当外得意先
		 * 検索条件：8月17日　YTJセールス太郎1の日報
		 * 検索対象：8月17日　YTJセールス太郎1の日報（所属外かつ担当外得意先への訪問）
		 * 検索結果：１件
		 * */
		@DBUnit
		def "担当者として登録されている訪問が取得できていること_自身の所属していない営業組織の得意先の場合"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/17 00:00:00");
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.userId = "ytj061"; // YTJセールス太郎１

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "2ae207a7-b4bc-916d-d199-8311bc700c43";
			r.callId = null;
			r.name = "セールス太郎の実績_所属先が異なる得意先";
			r.accountsName = "得意先_神奈川Co神奈川本社"; // 全社太郎の得意先
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容";
			r.read = false;
			r.commentReadCount = -1;
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['実績報告'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "商品関連";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "YTST"; // 所属していない営業組織
			r.accountSalesOfficeName = "Southern"; // 所属していない営業組織

			expected[r.id] = r;

			when:
			sut.findDailyReportAdmin(param, model, principal);

			then:
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}
	}

	/**
	 * 日報詳細（セールス）画面の検索処理のテスト
	 * 対象メソッド：findDailyReportSales
	 * */
	static class VisitListControllerDailyReportSpec_日報詳細（セールス）検索処理 extends DBSpecification {

		@Autowired
		VisitListController sut;

		void setupSpec() {
			// 現在日時を 2016/07/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/07/09 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");

			when:
			String actual = sut.findDailyReportSales(param, model, principal);

			then:
			actual == "visitList";
		}

		def "モードがdailyReportSalesに設定される"() {
			setup:
			def param = new DailyReportParam();
			param.date = new Date("2016/08/01 00:00:00");

			when:
			sut.findDailyReportSales(param, model, principal);

			then:
			model.get("mode") == "dailyReportSales";
		}
	}
}
