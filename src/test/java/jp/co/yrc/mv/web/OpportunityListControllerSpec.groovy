package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.UserInfoDto
import jp.co.yrc.mv.web.OpportunityListController.SearchCondition
import jp.co.yrc.mv.web.OpportunityListController.SearchResult
import jp.co.yrc.mv.web.VisitListController.VisitListSearchCondition
import mockit.MockUp

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken

/**
 * 案件状況画面のテスト
 * @author komatsu
 *
 */
@RunWith(Enclosed)
class OpportunityListControllerSpec {

	/**
	 * 案件状況画面の初期表示時のテスト
	 * 対象メソッド：initOpportunitiesList
	 */
	static class OpportunityListControllerSpec_案件状況初期表示 extends DBSpecification {
		@Autowired
		OpportunityListController sut;

		void setupSpec() {
			// 現在日時を 2017/01/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2017/01/01 00:00:00"))
							return cal
						}
					}
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.initOpportunitiesList(new SearchCondition(), model, principal);

			then:
			actual == "opportunityList";
		}

		def "modelにパラメーターが設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initOpportunitiesList(param, model, principal);

			then:
			model.get("searchCondition") == param;
		}

		def "modelに現在年月が設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initOpportunitiesList(param, model, principal);

			then:
			def c  = model.get("searchCondition");
			c.getYear() == 2017;
			c.getMonth() == 1;
		}

		def "yearsの最小最大範囲が設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initOpportunitiesList(param, model, principal);

			then:
			def c  = model.get("years");
			c.max() == 2018;
			c.min() == 2015;
		}
	}

	/**
	 * 案件状況画面の検索ボタン押下時のテスト
	 * 対象メソッド：find
	 */
	static class OpportunityListControllerSpec_案件状況検索押下時 extends DBSpecification {
		@Autowired
		OpportunityListController sut;

		void setupSpec() {
			// 現在日時を 2017/02/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2017/02/01 00:00:00"))
							return cal
						}
					}
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();
			param.setTanto("ytj01");
			param.setDiv1("0001110004");
			param.setYear(2016);
			param.setMonth(01);

			when:
			def actual = sut.find(param, model, principal)

			then:
			actual == "opportunityList";
		}

		@DBUnit
		def "検索結果が上限を超えた場合はlimitOverにtrueが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "all";

			when:
			sut.find(param, model, principal);

			then:
			assert model.get("limitOver") == true;
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→YTJ営業所太郎１
		 * 検索結果：担当案件（YTJセールス太郎１）
		 */
		@DBUnit
		def "営業所に紐づく担当者の案件状況を取得する場合_担当者固定"() {
			setup:

			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj051";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "営業所太郎1の案件";
			r.name = "YTJ営業所太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52300";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→全て
		 * 検索結果：２件 担当案件（YTJ営業所太郎１、YTJ全社太郎）
		 * 
		 */
		@DBUnit
		def "営業所に紐づく担当者の案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "営業所太郎1の案件";
			r.name = "YTJ営業所太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52300";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52697";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：初期表示の設定、部門→東京CO、担当者→YTJ部門太郎1
		 * 検索結果：１件 担当案件（YTJ部門太郎）
		 */
		@DBUnit
		def "部門に紐づく担当者の案件状況を取得する場合_担当者固定"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "all";
			param.tanto = "ytj041";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "部門太郎1の案件";
			r.name = "YTJ部門太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52200";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索対象：初期表示の設定、部門→東京CO、担当者→全て
		 * 検索結果：２件 担当案件（YTJ部門太郎１、YTJ全社太郎）
		 */
		@DBUnit
		def "部門に紐づく担当者の案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "部門太郎1の案件";
			r.name = "YTJ部門太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52300";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52697";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：初期表示の設定、販社→J首都圏、担当者→YTJ販社太郎1
		 * 検索結果：１件 担当案件（YTJ販社太郎１）
		 */
		@DBUnit
		def "販社に紐づく担当者の案件状況を取得する場合_担当者固定"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj031";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "販社太郎1の案件";
			r.name = "YTJ販社太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52100";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索対象：初期表示の設定、販社→J首都圏、担当者→全て
		 * 検索結果：２件 担当案件（YTJ販社太郎１、YTJ全社太郎１）
		 */
		@DBUnit
		def "販社に紐づく担当者の案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "0001110103";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "all";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "販社太郎1の案件";
			r.name = "YTJ販社太郎1";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52300";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52697";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：１件 担当案件（YTJ全社太郎）
		 */
		@DBUnit
		def "現在月に紐づく案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj01";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52697";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：１件 担当案件（YTJ全社太郎）
		 */
		@DBUnit
		def "前月に紐づく案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 1;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj01";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52695";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：１件 担当案件（YTJ全社太郎）
		 */
		@DBUnit
		def "次月に紐づく案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 3;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj01";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52694";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：１件 担当案件（YTJ全社太郎）
		 */
		@DBUnit
		def "前年に紐づく案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 1;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj01";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52698";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：１件 担当案件（YTJ全社太郎）
		 */
		@DBUnit
		def "次年に紐づく案件状況を取得する場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2018;
			param.month = 1;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj01";
			def expected = [:];

			def r = new SearchResult();
			r.opportunityName = "全社太郎の案件";
			r.name = "YTJ全社太郎";
			r.customer = "得意先1";
			r.step = 10;
			r.parentId = "28eb05d4-3c0e-011d-0300-bc008da52699";
			r.demand = "100";
			r.target = "100";
			r.rank = 10;
			expected[r.parentId] = r;

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.parentId)
				assert it.opportunityName == e.opportunityName;
				assert it.name == e.name;
				assert it.customer == e.customer;
				assert it.parentId == e.parentId;
				assert it.demand == e.demand;
				assert it.target == e.target;
				assert it.rank == e.rank;
			}
		}

		/**
		 * 検索条件：販社、部門、営業所、担当者→全て
		 * 検索結果：0件
		 */
		@DBUnit
		def "検索結果が取得できなかった場合は空のリストが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 8;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "ytj02";

			when:
			sut.find(param, model, principal);

			then:
			model.get("result").size() == 0;
		}

		@DBUnit
		def "DateConditionResultに検索対象年月が設定されていること"() {
			setup:
			def param = new SearchCondition();
			param.year = 2017;
			param.month = 2;
			param.div1 = "all";
			param.div2 = "all";
			param.div3 = "all";

			when:
			sut.find(param, model, principal);
			def dateConditionResult = model.get("dateConditionResult");

			then:
			dateConditionResult.dateFrom == "2017/02/01";
			dateConditionResult.dateTo == "2017/02/28";
		}
	}

	/**
	 * 案件状況画面の訪問情報ボタン押下時のテスト
	 * 対象メソッド：findVisitList
	 */
	static class OpportunityListControllerSpec_訪問情報押下時 extends DBSpecification {
		@Autowired
		VisitListController sut;

		void setupSpec() {
			// 現在日時を 2017/02/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2017/02/01 00:00:00"))
							return cal
						}
					}
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		@DBUnit
		def 案件に紐付いた訪問を取得すること() {
			setup:
			setAllUser();

			def param = new VisitListSearchCondition();
			param.parentId = "8d400b0b-6d29-583b-0038-1a2100484cd5";
			param.dateFrom = new Date("2017/02/01");
			param.dateTo = new Date("2017/02/28");
			param.mode = "visitListView";
			param.heldOnly = false;
			param.tantoOnly = false;

			def expected = [:];

			def r = new VisitListController.SearchResult();
			r.id = "b46304ac-7b99-613a-797b-1a1b28506183";
			r.callId = null;
			r.name = "訪問名";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2017/02/03 00:00";
			r.visitDateEnd = "17:45";
			r.description = "内容";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 1;
			r.assignedUserFirstName = "太郎";
			r.assignedUserLastName = "YTJ全社";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1; // 実績あり
			r.isPlan = true;
			r.infoDiv = "施策・イベント・キャンペーン・展示";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);
			def result = model.get("result");

			then:
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}
	}
}