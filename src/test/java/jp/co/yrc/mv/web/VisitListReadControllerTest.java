package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.security.Principal;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.sql.DataSource;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.TechServiceCalls;
import jp.co.yrc.mv.rest.VisitListReadController;
import jp.co.yrc.mv.rest.VisitListReadController.RegisterParam;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class VisitListReadControllerTest extends DataSourceBasedDBTestCaseBase {

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/" + getClass().getSimpleName();

	private String testMethodName = null;

	@Autowired
	VisitListReadController sut;

	static Principal principal;

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_" + testMethodName + ".xls"));
	}

	@Rule
	public TestRule testName = new TestWatcher() {
		protected void starting(Description d) {
			testMethodName = d.getMethodName();
		};
	};

	@BeforeClass
	public static void setUpClass() {
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		Authentication authentication = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		principal = authentication;
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void 既読にする() throws SQLException, Exception {
		Set<TechServiceCalls> set = new TreeSet<>();
		TechServiceCalls o = new TechServiceCalls();
		o.setId("callId");
		set.add(o);

		RegisterParam param = new RegisterParam();
		param.calls = set;
		sut.toRead(param, principal);

		IDataSet actualDataSet = getConnection().createDataSet();
		ITable actual = actualDataSet.getTable("calls_read_users");

		IDataSet expectedDataSet = new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_" + testMethodName
				+ "_expected.xls"));
		ITable expected = expectedDataSet.getTable("calls_read_users");

		// 2件あったところに1件インサート
		assertThat(actual.getRowCount(), is(3));
		assertCallsReadUsersTable(actual, expected);
	}

	public void assertCallsReadUsersTable(ITable actual, ITable expected) throws DataSetException {
		Map<String, Object> actualMap = new LinkedHashMap<String, Object>();
		for (Column column : actual.getTableMetaData().getColumns()) {
			actualMap.put(column.getColumnName(), actual.getValue(0, column.getColumnName()));
		}

		Map<String, Object> expectedMap = new LinkedHashMap<String, Object>();
		for (Column column : expected.getTableMetaData().getColumns()) {
			Object value = expected.getValue(0, column.getColumnName());
			expectedMap.put(column.getColumnName(), value);
		}

		// DB状態確認
		for (String key : actualMap.keySet()) {
			if (!("id".equals(key) || "date_modified".equals(key))) {
				assertThat(actualMap.get(key), is(expectedMap.get(key)));
			}
		}
	}
}
