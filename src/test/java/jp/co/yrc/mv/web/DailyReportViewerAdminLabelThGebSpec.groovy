package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification


/**
 * 日報確認画面(管理者)の多言語化対応(タイ語)テスト
 * @author komatsu
 * 
 */
class DailyReportViewerAdminLabelThGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{$("div[data-role='page'].ui-page-active").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
		waitFor{$("#container").isDisplayed()}

		$("#headerMenu-report").click()
		waitFor{$("#container").isDisplayed()}
		// 日報確認画面(管理者)に遷移
		$("img[src='/ymjmv/images/dashboard/dailyReportViewerAdmin.png']").click()
		waitFor{$("#contentsWrapper").isDisplayed()}
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "รายงานประจำวันที่ได้รับการยืนยัน (ผู้จัดการ)"
	}

	def "ボタンの文言が正しいこと"() {
		expect:
		$("#selectAll").value() == "เลือกทั้งหมด" // 全選択
		$("#confirmComment").value() == "อัพเดตข้อมูลเพื่อยืนยัน" // 確認済みへ変更
	}

	def "テーブルヘッダの文言が正しいこと"() {
		when:
		def column = $("#table_head").find("th")

		then:
		// 確認
		column.eq(1).text() == "ยืนยัน"
		// セールスコメント
		column.eq(7).text() == "บันทึกข้อมูลผู้แทนขาย"
	}

	def "テーブル下部の文言が正しいこと"() {
		expect:
		// 確認ボタン
		$(".comment-button-narrow").value() == "ยืนยัน"
		// セールスコメント
		$(".dataTables_scrollFootInner").find(".comment input").attr("placeholder") == "บันทึกข้อมูลผู้แทนขาย"
	}
}
