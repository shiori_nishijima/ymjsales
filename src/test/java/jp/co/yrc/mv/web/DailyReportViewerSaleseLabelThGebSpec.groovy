package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification


/**
 * 日報確認画面(セールス)の多言語化対応(タイ語)テスト
 * @author komatsu
 * 
 */
class DailyReportViewerSaleseLabelThGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{$("div[data-role='page'].ui-page-active").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
		waitFor{$("#container").isDisplayed()}

		$("#headerMenu-report").click()
		waitFor{$("#container").isDisplayed()}
		// 日報確認画面(管理者)に遷移
		$("img[src='/ymjmv/images/dashboard/dailyReportViewerSales.png']").click()
		waitFor{$("#contentsWrapper").isDisplayed()}
	}

	def "ページタイトルが正しいこと"() {
		when:
		def actual = driver.getTitle()

		then:
		actual == "รายงานประจำวันที่ได้รับการยืนยัน (ผู้แทนขาย)"
	}

	def "テーブルヘッダの文言が正しいこと"() {
		when:
		def column = $("#table_head").find("th")

		then:
		column.eq(5).text() == "หมายเหตุโดยผู้จัดการ" // 上司コメント
	}

	def "テーブル下部の文言が正しいこと"() {
		expect:
		// 上司コメント
		$(".dataTables_scrollFootInner").find(".comment input").attr("placeholder") == "หมายเหตุโดยผู้จัดการ"
	}
}
