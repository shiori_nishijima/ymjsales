package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit

/**
 * 案件状況一覧画面のテスト
 * @author komatsu
 *
 */
class OpportunityListGebSpec extends GebDBSpecification{

	void setupSpec() {
		to TopPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click()
		$("[data-menu-id='opportunityList.html']").children().children("div").click()
	}

	@GebDBUnit
	def "DBに登録されている案件のデータが表示されていること"() {
		setup:
		//検索範囲を2017年2月に設定する。
		setupDatePicker()
		setupMarketAll()

		$("#tanto").click()
		$('#tanto').find("option").find{ it.value() == "ytj01" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		then:
		tr.children(".rank").text() == "10"
		tr.children(".assignedUserName").text() == "YTJ全社 太郎"
		tr.children(".customer").text() == "得意先1"
		tr.children(".opportunityName").text() == "全社太郎の案件"
		tr.children(".demand").text() == "100"
		tr.children(".target").text() == "100"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step3").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"
	}

	@GebDBUnit
	def "販社を指定した場合は選んだ販社に紐づく案件のみが取得できること"() {
		setup:
		setupDatePicker()
		// 販社：J首都圏に設定
		setupMarket()

		$("#tanto").click()
		$('#tanto').find("option").find{ it.value() == "ytj031" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		then:
		tr.children(".rank").text() == "10"
		tr.children(".assignedUserName").text() == "YTJ販社 太郎1"
		tr.children(".customer").text() == "得意先1"
		tr.children(".opportunityName").text() == "販社太郎1の案件"
		tr.children(".demand").text() == "100"
		tr.children(".target").text() == "100"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step3").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"
	}

	@GebDBUnit
	def "部門を指定した場合は選んだ部門に紐づく案件のみが取得できること"() {
		setup:
		setupDatePicker()
		setupMarket()
		// 部門：東京COに設定
		setupDepartment()

		$("#tanto").click()
		$('#tanto').find("option").find{ it.value() == "ytj041" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		then:
		tr.children(".rank").text() == "10"
		tr.children(".assignedUserName").text() == "YTJ部門 太郎1"
		tr.children(".customer").text() == "得意先1"
		tr.children(".opportunityName").text() == "部門太郎1の案件"
		tr.children(".demand").text() == "100"
		tr.children(".target").text() == "100"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step3").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"

		setupMarketAll()
	}

	@GebDBUnit
	def "営業所を指定した場合は指定した営業所に紐づく案件のみが取得できること"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		// 営業所：東京本社に設定
		setupSalesOffice()

		$("#tanto").click()
		$('#tanto').find("option").find{ it.value() == "ytj051" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		then:
		tr.children(".rank").text() == "10"
		tr.children(".assignedUserName").text() == "YTJ営業所 太郎1"
		tr.children(".customer").text() == "得意先1"
		tr.children(".opportunityName").text() == "営業所太郎1の案件"
		tr.children(".demand").text() == "100"
		tr.children(".target").text() == "100"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step3").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"

		setupMarketAll()
	}

	@GebDBUnit
	def "担当者を指定しない場合は営業所に紐づく複数の担当者の案件が取得できること"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		setupSalesOffice()
		//担当者を全てに設定
		setupTanto()

		when:
		$("#searchButton").click()
		def trs = $(".dataTables_scroll .dataTables_scrollBody tbody tr")

		then:
		trs.size() == 5

		def tr1 = trs.children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da52695']").parent().parent()
		tr1.children(".assignedUserName").text() == "YTJ販社 太郎1"
		tr1.children(".customer").text() == "得意先1"
		tr1.children(".opportunityName").text() == "販社太郎1の案件"

		def tr2 = trs.children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da52698']").parent().parent()
		tr2.children(".assignedUserName").text() == "YTJ営業所 太郎1"
		tr2.children(".customer").text() == "得意先1"
		tr2.children(".opportunityName").text() == "営業所太郎1の案件"

		def tr3 = trs.children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da52694']").parent().parent()
		tr3.children(".assignedUserName").text() == "YTJ部門 太郎1"
		tr3.children(".customer").text() == "得意先1"
		tr3.children(".opportunityName").text() == "部門太郎1の案件"

		def tr4 = trs.children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da52300']").parent().parent()
		tr4.children(".assignedUserName").text() == "YTJ全社 太郎"
		tr4.children(".customer").text() == "得意先1"
		tr4.children(".opportunityName").text() == "全社太郎の案件"

		def tr5 = trs.children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da52696']").parent().parent()
		tr5.children(".assignedUserName").text() == "YTJセールス 太郎1"
		tr5.children(".customer").text() == "得意先1"
		tr5.children(".opportunityName").text() == "セールス太郎1の案件"

		setupMarketAll()
	}

	@GebDBUnit
	def "ステータスが40の場合、案件発掘-受注間のセルに背景色が設定される"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		setupTanto()

		$("#div3").click()
		$('#div3').find("option").find{ it.value() == "1101" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da50000']").parent().parent()

		then:
		tr.children(".opportunityName").text() == "全社太郎の案件(受注)"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step3").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step4").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"

		setupMarketAll()
	}

	@GebDBUnit
	def "ステータスが50の場合、案件発掘-完了間のセルに背景色が設定される"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		setupTanto()

		$("#div3").click()
		$('#div3').find("option").find{ it.value() == "1101" }.click()


		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da60000']").parent().parent()

		then:
		tr.children(".opportunityName").text() == "全社太郎の案件(完了)"
		tr.children(".step1").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step2").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step3").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step4").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step5").children().css("background-color") == "rgba(105, 149, 201, 1)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"

		setupMarketAll()
	}

	@GebDBUnit
	def "ステータスが60の場合、失注のセルのみ背景色が設定される"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		setupTanto()

		$("#div3").click()
		$('#div3').find("option").find{ it.value() == "1101" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da70000']").parent().parent()

		then:
		tr.children(".opportunityName").text() == "全社太郎の案件(失注)"
		tr.children(".step1").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step2").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step3").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(201, 105, 105, 1)"
		tr.children(".step7").children().css("background-color") == "rgba(0, 0, 0, 0)"

		setupMarketAll()
	}

	@GebDBUnit
	def "ステータスが70の場合、案件消滅のセルのみ背景色が設定される"() {
		setup:
		setupDatePicker()
		setupMarket()
		setupDepartment()
		setupTanto()

		$("#div3").click()
		$('#div3').find("option").find{ it.value() == "1101" }.click()

		when:
		$("#searchButton").click()
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").children("td").find("input[data-parent_id='28eb05d4-3c0e-011d-0300-bc008da80000']").parent().parent()

		then:
		tr.children(".opportunityName").text() == "全社太郎の案件(案件消滅)"
		tr.children(".step1").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step2").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step3").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step4").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step5").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step6").children().css("background-color") == "rgba(0, 0, 0, 0)"
		tr.children(".step7").children().css("background-color") == "rgba(201, 105, 105, 1)"

		setupMarketAll()
	}


	//検索範囲を2017年2月に設定する。
	void setupDatePicker() {
		$("#year").click()
		$("#year").find("option").find{ it.value() == "2017" }.click()

		$("#month").click()
		$("#month").find("option").find{ it.value() == "2" }.click()
	}

	//販社を初期状態（全て）に設定する
	void setupMarketAll() {
		$("#div1").click()
		$('#div1').find("option").find{ it.value() == "all" }.click()
	}

	//販社をJ首都圏に設定する
	void setupMarket() {
		$("#div1").click()
		$('#div1').find("option").find{ it.value() == "0001110103" }.click()
	}

	//部門を東京COに設定
	void setupDepartment() {
		$("#div2").click()
		$('#div2').find("option").find{ it.value() == "J310" }.click()
	}

	//営業所を東京本社に設定
	void setupSalesOffice() {
		$("#div3").click()
		$('#div3').find("option").find{ it.value() == "1100" }.click()
	}

	//担当を初期状態（全て）に設定する。
	void setupTanto() {
		$("#tanto").click()
		$('#tanto').find("option").find{ it.value() == "all" }.click()
	}
}
