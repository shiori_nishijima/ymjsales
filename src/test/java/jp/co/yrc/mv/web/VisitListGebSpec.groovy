package jp.co.yrc.mv.web

import geb.Page
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit
import mockit.MockUp


/**
 * 訪問実績画面のテスト
 * @author SHIORIN
 *
 */
class VisitListGebSpec extends GebDBSpecification {

	void setupSpec() {
		// 現在日時を 2016/09/09 に固定
		new MockUp<DateUtils>() {
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return true
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/09/09 00:00:00"))
						return cal
					}
				}

		to TopPage
		$('#userId').value('ytj061')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click()
		$("[data-menu-id='visitList.html']").children().children("div").click()
	}

	/**
	 * 検索条件：実績のみ表示○、情報ありのみ表示○、未読のみ表示×（初期設定）
	 * 担当者→セールス太郎
	 * ※実績のない訪問、情報のない訪問は表示されないこと
	 * */
	@GebDBUnit
	def "DBに登録されている訪問のデータが表示されていること"() {
		setup:

		when:
		// 検索日付を９月１日～３０日に固定
		setupDatePicker()
		// 担当者固定
		setupTanto()
		// チェックボックスの初期化
		setupChecked()

		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.size() == 1

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == "1"; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	@GebDBUnit
	def "ログインユーザが訪問既読の場合は「訪問」が既読であること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "既読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "1"; // 閲覧履歴
	}

	@GebDBUnit
	def "ログインユーザが訪問未読の場合は「訪問」が未読であること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "1"; // 閲覧履歴 ログインユーザーでないユーザーが既読になっているため
	}

	@GebDBUnit
	def "訪問に対してコメントが存在しない場合は「コメント未読」がコメント無しになっていること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	/**
	 * コメント表示押下時のアサートもおこなう
	 * */
	@GebDBUnit
	def "訪問に対して未読のコメントが存在しない場合は「コメント未読」がなしになっていること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "既読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "なし"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "済"; // コメント入力
		tr.children(".show-view-history").text() == "1"; // 閲覧履歴

		// コメント表示ボタン押下（非同期）
		$("input[value='コメント表示']").click()

		waitFor{$("#eigyoSosikiNm").text() == "東京本社"}
		waitFor{$("#visitUserLastName").text() == "YTJセールス"}
		waitFor{$("#visitUserFirstName").text() == "太郎1"}
		//		waitFor{$("#likeCount").text() == "0"}
		waitFor{$("#commentTable tbody").children("tr").eq(5).text() == "セールス太郎が入力\n2016/09/26 15:47　YTJセールス 太郎1　東京本社"}

		waitFor{$("#cboxClose").click()}
	}

	@GebDBUnit
	def "訪問に対して未読のコメントが存在する場合は「コメント未読」がありになっていること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "既読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "あり"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "済"; // コメント入力
		tr.children(".show-view-history").text() == "2"; // 閲覧履歴
	}

	@GebDBUnit
	def "注目マーク数が０の場合は「注目マーク数」に何も表示されないこと"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	@GebDBUnit
	def "ログインユーザが訪問に対してコメントを入力している場合は「コメント入力」が済であること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "既読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "なし"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "済"; // コメント入力
		tr.children(".show-view-history").text() == "1"; // 閲覧履歴
	}

	@GebDBUnit
	def "ログインユーザが訪問に対してコメントを入力していない場合は「コメント入力」が未であること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def tr = $(".dataTables_scroll .dataTables_scrollBody tbody tr").first()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == "1"; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	@GebDBUnit
	def "実績のみ表示にチェックを入れない場合は実績のない訪問も検索結果に含まれること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()

		// 実績のみ表示のチェックを外す
		if ($('#heldOnly').attr('checked')) {
			$("#heldOnly").click()
		}

		// 情報ありのみ表示のチェックを外す
		if ($('#hasInfoOnly').attr('checked')) {
			$("#hasInfoOnly").click()
		}

		// 未読のみ表示のチェックを外す
		if ($('#unreadOnly').attr('checked')) {
			$("#unreadOnly").click()
		}

		$("input[value='検索']").click()

		then:
		// 訪問が２件取得できること
		def trs = $(".dataTables_scroll .dataTables_scrollBody tbody tr")

		trs.size() == 2

		// 実績のない行を取得
		def tr = trs.children("td").find("input[value='17f0e06d-b75e-c00b-3190-15152a920a98']").parent().parent()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == ""; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/27 12:00"; // 訪問日時
		tr.children(".description").text() == ""; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "予定"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ販社太郎1[ytj031]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == ""; // 実績数
		tr.children(".info-div").text() == ""; // 情報区分
		tr.children(".maker").text() == ""; // メーカー
		tr.children(".kind").text() == ""; // 品種
		tr.children(".category").text() == ""; // カテゴリ
		tr.children(".sensitivity").text() == ""; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == ""; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	@GebDBUnit
	def "未読のみ表示にチェックを入れた場合はログインユーザが未読の訪問のみ取得できること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()

		// 未読のみ表示にチェックをつける
		if (!$('#unreadOnly').attr('checked')) {
			$("#unreadOnly").click()
		}

		// 実績のみ表示にチェック
		if (!$('#heldOnly').attr('checked')) {
			$("#heldOnly").click()
		}

		// 情報ありのみ表示のチェック
		if (!$('#hasInfoOnly').attr('checked')) {
			$("#hasInfoOnly").click()
		}

		$("input[value='検索']").click()

		then:
		def trs = $(".dataTables_scroll .dataTables_scrollBody tbody tr")

		// 訪問が１件取得できること
		trs.size() == 1

		// 実績のない行を取得
		def tr = trs.children("td").find("input[value='177b3101-9509-76ce-ac12-532d6990a309']").parent().parent()

		tr.children(".readflg").text() == "未読" // 訪問
		tr.children(".visit-name").text() == "セールス太郎の訪問_未読"; // 件名
		tr.children(".customer-name").text() == "得意先1"; // 得意先名
		tr.children(".market").text() == "ＳＳ"; // 販路
		tr.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr.children(".description").text() == "テスト"; // 内容
		tr.children(".parent-type").text() == "得意先"; // 関連先
		tr.children(".sutatus").text() == "実績"; // ステータス
		tr.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr.children(".account-department").text() == "東京Co"; // 部門
		tr.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr.children(".planned-count").text() == "1"; // 予定数
		tr.children(".held-count").text() == "1"; // 実績数
		tr.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr.children(".maker").text() == "YH"; // メーカー
		tr.children(".kind").text() == "PC"; // 品種
		tr.children(".category").text() == "ナツ"; // カテゴリ
		tr.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr.children(".important-mark").text() == "1"; // 注目マーク数
		tr.children(".comment-entered").text() == "未"; // コメント入力
		tr.children(".show-view-history").text() == "1"; // 閲覧履歴 自分以外のユーザが既読のため
	}

	/**
	 * 検索条件：営業所→東京本社、担当者→セールス太郎
	 * ※営業所→東京本社に紐づく他の担当者の訪問が取得できていないこと
	 * */
	@GebDBUnit
	def "担当者を指定して検索した場合は担当者に紐づく訪問のみ取得できること"() {
		setup:

		when:
		setupDatePicker()
		setupTanto()
		setupChecked()
		$("input[value='検索']").click()

		then:
		def trs = $(".dataTables_scroll .dataTables_scrollBody tbody tr")

		trs.size() == 2

		// セールス太郎1の実績、関連先が得意先
		def tr1 = trs.children("td").find("input[value='177b3101-9509-76ce-ac12-532d6990a308']").parent().parent()

		tr1.children(".readflg").text() == "未読" // 訪問
		tr1.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr1.children(".customer-name").text() == "得意先1"; // 得意先名
		tr1.children(".market").text() == "ＳＳ"; // 販路
		tr1.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr1.children(".description").text() == "テスト"; // 内容
		tr1.children(".parent-type").text() == "得意先"; // 関連先
		tr1.children(".sutatus").text() == "実績"; // ステータス
		tr1.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr1.children(".account-department").text() == "東京Co"; // 部門
		tr1.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr1.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr1.children(".calls-users").text() == "YTJ部門太郎1[ytj041]"; // 社内同行者
		tr1.children(".planned-count").text() == "1"; // 予定数
		tr1.children(".held-count").text() == "1"; // 実績数
		tr1.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr1.children(".maker").text() == "YH"; // メーカー
		tr1.children(".kind").text() == "PC"; // 品種
		tr1.children(".category").text() == "ナツ"; // カテゴリ
		tr1.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr1.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr1.children(".important-mark").text() == "1"; // 注目マーク数
		tr1.children(".comment-entered").text() == "未"; // コメント入力
		tr1.children(".show-view-history").text() == "0"; // 閲覧履歴

		// セールス太郎1の実績、関連先が案件
		def tr2 = trs.children("td").find("input[value='00000000-4ce9-69be-3889-25eba653c385']").parent().parent()

		tr2.children(".readflg").text() == "未読" // 訪問
		tr2.children(".visit-name").text() == "案件に紐付いてる訪問"; // 件名
		tr2.children(".customer-name").text() == "得意先7"; // 得意先名
		tr2.children(".market").text() == "ＳＳ"; // 販路
		tr2.children(".datetime").text() == "2016/09/26 12:00"; // 訪問日時
		tr2.children(".description").text() == "テスト"; // 内容
		tr2.children(".parent-type").text() == "案件"; // 関連先
		tr2.children(".sutatus").text() == "実績"; // ステータス
		tr2.children(".division-c").text() == "訪問：新規開拓\n訪問：商談あり\n訪問：定常"; // 訪問理由
		tr2.children(".account-department").text() == "東京Co"; // 部門
		tr2.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr2.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr2.children(".calls-users").text() == "YTJ販社太郎1[ytj031]"; // 社内同行者
		tr2.children(".planned-count").text() == "1"; // 予定数
		tr2.children(".held-count").text() == "1"; // 実績数
		tr2.children(".info-div").text() == "価格・販売状況・チラシ"; // 情報区分
		tr2.children(".maker").text() == "YH"; // メーカー
		tr2.children(".kind").text() == "PC"; // 品種
		tr2.children(".category").text() == "ナツ"; // カテゴリ
		tr2.children(".sensitivity").text() == "ポジティブな情報"; // 感度
		tr2.children(".comment-read").text() == "コメント無し"; // コメンド未読
		tr2.children(".important-mark").text() == "1"; // 注目マーク数
		tr2.children(".comment-entered").text() == "未"; // コメント入力
		tr2.children(".show-view-history").text() == "0"; // 閲覧履歴
	}

	/**
	 * 検索条件：営業所→東京本社、担当者→全て
	 * ※営業所→東京本社に紐付いていない担当者の訪問が取得できていないこと
	 * ※指定した日付以外の訪問が取得できていないこと
	 * */
	@GebDBUnit
	def "担当者全てで検索した場合は営業所に紐づく複数の担当者の訪問が取得できること"() {
		setup:

		when:
		// 検索日付を２６日～２７日に固定
		$('#dateFrom').click()
		def pickerFrom =  $('#ui-datepicker-div')

		pickerFrom.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerFrom.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerFrom.find("td a").find{ it.text() == "26" }.click()

		$('#dateTo').click()
		def pickerTo = $('#ui-datepicker-div')

		pickerTo.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerTo.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerTo.find("td a").find{ it.text() == "27" }.click()

		// 担当者を全てに変更
		$('#tanto').click()
		$('#tanto').find("option").find{ it.value() == "all" }.click()

		setupChecked()
		$("input[value='検索']").click()

		then:
		def trs = $(".dataTables_scroll .dataTables_scrollBody tbody tr")

		// 訪問が5件取得できること
		trs.size() == 5

		// セールス太郎1の実績
		def tr1 = trs.children("td").find("input[value='177b3101-9509-76ce-ac12-532d6990a308']").parent().parent()

		tr1.children(".visit-name").text() == "セールス太郎の訪問"; // 件名
		tr1.children(".customer-name").text() == "得意先1"; // 得意先名
		tr1.children(".account-department").text() == "東京Co"; // 部門
		tr1.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr1.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr1.children(".parent-type").text() == "得意先"; // 関連先

		// セールス太郎4の実績
		def tr2 = trs.children("td").find("input[value='28e30deb-520e-959b-dc90-c3f8d384ad5a']").parent().parent()

		tr2.children(".visit-name").text() == "セールス太郎4の実績"; // 件名
		tr2.children(".customer-name").text() == "得意先7"; // 得意先名
		tr2.children(".account-department").text() == "東京Co"; // 部門
		tr2.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr2.children(".assigned-user-name").text() == "YTJセールス 太郎4"; // 担当者

		// 営業所太郎の実績
		def tr3 = trs.children("td").find("input[value='28a10ae7-2488-7a0a-a836-6c5f1046c7a3']").parent().parent()

		tr3.children(".visit-name").text() == "営業所太郎の実績_得意先1"; // 件名
		tr3.children(".customer-name").text() == "得意先1"; // 得意先名
		tr3.children(".account-department").text() == "東京Co"; // 部門
		tr3.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr3.children(".assigned-user-name").text() == "YTJ営業所 太郎1"; // 担当者

		// 販社太郎の実績（東京本社に紐づく訪問）
		def tr4 = trs.children("td").find("input[value='28e90062-1bba-c192-6c0d-12b030a02272']").parent().parent()

		tr4.children(".visit-name").text() == "販社太郎の実績_東京本社"; // 件名
		tr4.children(".customer-name").text() == "得意先3"; // 得意先名
		tr4.children(".account-department").text() == "東京Co"; // 部門
		tr4.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr4.children(".assigned-user-name").text() == "YTJ販社 太郎1"; // 担当者

		// セールス太郎の実績（案件に紐づく訪問）
		def tr5 = trs.children("td").find("input[value='00000000-4ce9-69be-3889-25eba653c385']").parent().parent()

		tr5.children(".visit-name").text() == "案件に紐付いてる訪問"; // 件名
		tr5.children(".customer-name").text() == "得意先7"; // 得意先名
		tr5.children(".account-department").text() == "東京Co"; // 部門
		tr5.children(".account-sales-office").text() == "東京本社"; // 営業所
		tr5.children(".assigned-user-name").text() == "YTJセールス 太郎1"; // 担当者
		tr5.children(".parent-type").text() == "案件"; // 関連先
	}

	/**
	 * 検索範囲を９月１日～３０日に設定する
	 * */
	void setupDatePicker() {
		$('#dateFrom').click()
		def pickerFrom =  $('#ui-datepicker-div')

		pickerFrom.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerFrom.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerFrom.find("td a").find{ it.text() == "1" }.click()

		$('#dateTo').click()
		def pickerTo = $('#ui-datepicker-div')

		pickerTo.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerTo.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerTo.find("td a").find{ it.text() == "30" }.click()
	}

	/**
	 * 担当者固定
	 * */
	void setupTanto() {
		$('#tanto').click()
		$('#tanto').find("option").find{ it.value() == "ytj061" }.click()
	}

	/**
	 * チェックボックスを初期状態に設定する
	 * 実績のみ表示：○
	 * 情報ありのみ表示：○
	 * 未読のみ表示：×
	 * */
	void setupChecked() {
		// 実績のみ表示にチェック
		if (!$('#heldOnly').attr('checked')) {
			$("#heldOnly").click()
		}
		// 情報ありのみ表示にチェック
		if (!$('#hasInfoOnly').attr('checked')) {
			$("#hasInfoOnly").click()
		}
		// 未読のみ表示のチェックを外す
		if ($('#unreadOnly').attr('checked')) {
			$("#unreadOnly").click()
		}
	}

	/**
	 * 販路を初期状態（全て）に設定する
	 * */
	void setupMarket() {
		// 販路を全てに設定する
		$("#market").click()
		$('#market').find("option").find{ it.value() == "all" }.click()
	}
}

class TopPage extends Page {
	static url = "http://localhost:8080/ymjmv/"

	static content = {
	}
}

