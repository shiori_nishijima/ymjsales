package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit

import org.openqa.selenium.JavascriptExecutor


/**
 * 日報詳細画面(管理者)の多言語化対応(タイ語)テスト
 * @author komatsu
 *
 */
class DailyReportAdminLabelThGebSpec extends GebDBSpecification {

	JavascriptExecutor exe = (JavascriptExecutor)driver

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{!$("#loadingView").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])

		waitFor{$("#headerMenu-report").click()}
		// 日報確認画面(管理者)に遷移
		waitFor{$("[data-menu-id='dailyReportViewerAdmin.html'] img.dashboardImg").isDisplayed()}
		$("[data-menu-id='dailyReportViewerAdmin.html'] img.dashboardImg").click()
	}

	@GebDBUnit
	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "รายระเอียดรายงานประจำวัน(ผู้จัดการ)"
	}

	@GebDBUnit
	def "'ヘッダーの各文言が正しいこと"() {
		setup:

		when:
		def head = $("span.comment-user")

		then:
		head.eq(0).text() == "แผนก：" // 所属部署:
		head.eq(1).text() == "ผู้แทนขาย：" // 担当:
	}

	@GebDBUnit
	def "コメント入力フォームの各文言が正しいこと"() {
		when:
		def commentDiv = $(".comment div")

		then:
		commentDiv.eq(0).text() == "ผู้แทนขาย" // セールス
		commentDiv.eq(1).text() == "บันทึกรายงานประจำวัน" // 日報コメント
		commentDiv.eq(2).text() == "ผู้จัดการ" // 上司
		commentDiv.eq(3).text() == "บันทึกรายงานประจำวัน" // 日報コメント
		$("#bossSign").value() == "ลายเซ็น" // 署名
	}

	@GebDBUnit
	def "'コメント保存'の文言が正しいこと"() {
		expect:
		$("#commentSave").value() == "บันทึกหมายเหตุ"
	}

	/**
	 * 日報詳細画面へ遷移
	 */
	void setup() {
		if(exe.executeScript("return location.pathname;") != "/ymjmv/dailyReportAdmin.html") {
			$("#year").click()
			$("#year").children().eq(1).click()

			$("#month").click()
			$("#month").children().eq(8).click()

			$("#date").click()
			$("#date").children().eq(0).click()

			$(".search").click()
			$("tbody").children().first().click()

			def windows = driver.getWindowHandles();
			driver.switchTo().window(windows[2])
			
			waitFor{$(".next").isDisplayed()}
		}
	}
}