package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit


/**
 * 日報確認画面(共通)の多言語化対応(英語)テスト
 * @author komatsu
 * 
 */
class DailyReportViewerCommonLabelEnGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleEn').click()
		$('#btnLogin').click()
		waitFor{$("div[data-role='page'].ui-page-active").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
		waitFor{$("#container").isDisplayed()}

		$("#headerMenu-report").click()
		waitFor{$("img.dashboardImg").size() == 3}
		// 日報確認画面(管理者)に遷移
		$("img[src='/ymjmv/images/dashboard/dailyReportViewerAdmin.png']").click()
		waitFor{$("#contentsWrapper").isDisplayed()}
	}

	def "Datatablesの文言が正しいこと"() {
		expect:
		// _MENU_ 件表示
		$("#DataTables_Table_0_length").text() == "Display\n10\n25\n50\n100\nrecords per page"
		// 検索：
		$("#DataTables_Table_0_filter").text() == "search:"
		$(".previous").text() == "Prev" // 前
		$(".next").text() == "Next" // 次
	}

	def "検索ボタンの文言が正しいこと"() {
		expect:
		$(".search").value() == "Search"
	}

	def "テーブルヘッダの文言が正しいこと"() {
		when:
		def column = $("#table_head").find("th")

		then:
		column.eq(2).text() == "Department" // 所属名
		column.eq(3).text() == "Sales Rep." // セールスマン名
		column.eq(4).text() == "Visit date" // 訪問日
		column.eq(5).text() == "Visit" // 訪問件数
		column.eq(6).text() == "Number of call" // 電話件数
		try {
			js."document.getElementsByClassName('dataTables_scrollHeadInner')[0].getElementsByTagName('th')[9].scrollIntoView(true)"
		} catch(Error e) {}
		column.eq(8).text() == "Confirmation by manager" // 上司確認
		column.eq(9).text() == "Confirmed date" // 確認日
	}

	def "テーブル下部の文言が正しいこと"() {
		when:
		def column = $(".dataTables_scrollFootInner").find("input")

		then:
		column.eq(1).attr("placeholder") == "Department" // 所属名
		column.eq(2).attr("placeholder") == "Sales Rep." // セールスマン名
		column.eq(3).attr("placeholder") == "Visit date" // 訪問日
		column.eq(4).attr("placeholder") == "Visit" // 訪問件数
		column.eq(5).attr("placeholder") == "Number of call" // 電話件数
		column.eq(7).attr("placeholder") == "Confirmation by manager" // 上司確認
		column.eq(8).attr("placeholder") == "Confirmed date" // 確認日
	}

	@GebDBUnit
	def "確認「未」の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$(".search").click()

		when:
		def label = $("span[data-roll-confirm='00000000-0000-0000-0000-000000000001']").text()

		then:
		label == "Not yet (uncompleted)"
		// _TOTAL_ 件中 _START_ から _END_ まで表示（０件でない場合）
		$("#DataTables_Table_0_info").text() == "Showing page 1 of 1"
	}

	@GebDBUnit
	def "確認「済」の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$(".search").click()

		when:
		def label = $("span[data-roll-confirm='00000000-0000-0000-0000-000000000001']").text()

		then:
		label == "Done"
	}

	@GebDBUnit
	def "Datatablesの文言が正しいこと_データが存在しない場合"() {
		setup:
		$(".search").click()

		expect:
		// データはありません。
		$(".dataTables_empty").text() == "Nothing found - sorry"
		// _TOTAL_ 件中 _START_ から _END_ まで表示（０件の場合）
		$("#DataTables_Table_0_info").text() == "No records available"
	}

	/**
	 * 検索日付を2016年9月に設定
	 */
	void setupDatePicker() {
		$("#year").click()
		$("#year").find("option[value='2016']").click()

		$("#month").click()
		$("#month").find("option[value='201609']").click()

		$("#date").click()
		$("#date").find("option[value='-1']").click()
	}
}
