package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.yrc.mv.web.LoginController.Fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@SuppressWarnings("unchecked")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
public class LoginControllerTest {

	@Autowired
	LoginController sut;

	@Test
	public void ユーザIDパスワード共にブランクの場合はログイン失敗() {
		Fail fail = mock(Fail.class);
		when(fail.getCode()).thenReturn("both");
		RedirectAttributes attributes = mock(RedirectAttributes.class);

		String actual = sut.fail(fail, attributes);
		String expected = "redirect:/";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(attributes, times(1)).addFlashAttribute(argString.capture(), argObj.capture());
		Iterator<String> ite = (Iterator<String>) argObj.getValue();

		List<String> list = new ArrayList<String>();
		while (ite.hasNext()) {
			list.add(ite.next());
		}
		assertThat(list.get(0), is("ユーザIDを入力してください。"));
		assertThat(list.get(1), is("パスワードを入力してください。"));
	}

	@Test
	public void ユーザIDがブランクの場合はログイン失敗() {
		Fail fail = mock(Fail.class);
		when(fail.getCode()).thenReturn("user");
		RedirectAttributes attributes = mock(RedirectAttributes.class);

		String actual = sut.fail(fail, attributes);
		String expected = "redirect:/";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(attributes, times(1)).addFlashAttribute(argString.capture(), argObj.capture());
		Iterator<String> ite = (Iterator<String>) argObj.getValue();

		List<String> list = new ArrayList<String>();
		while (ite.hasNext()) {
			list.add(ite.next());
		}
		assertThat(list.get(0), is("ユーザIDを入力してください。"));
	}

	@Test
	public void パスワードがブランクの場合はログイン失敗() {
		Fail fail = mock(Fail.class);
		when(fail.getCode()).thenReturn("password");
		RedirectAttributes attributes = mock(RedirectAttributes.class);

		String actual = sut.fail(fail, attributes);
		String expected = "redirect:/";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(attributes, times(1)).addFlashAttribute(argString.capture(), argObj.capture());
		Iterator<String> ite = (Iterator<String>) argObj.getValue();

		List<String> list = new ArrayList<String>();
		while (ite.hasNext()) {
			list.add(ite.next());
		}
		assertThat(list.get(0), is("パスワードを入力してください。"));
	}

	@Test
	public void 入力したログインIDとパスワードで認証出来なかった場合はログイン失敗() {
		Fail fail = mock(Fail.class);
		when(fail.getCode()).thenReturn("badCredentials");
		RedirectAttributes attributes = mock(RedirectAttributes.class);

		String actual = sut.fail(fail, attributes);
		String expected = "redirect:/";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(attributes, times(1)).addFlashAttribute(argString.capture(), argObj.capture());
		Iterator<String> ite = (Iterator<String>) argObj.getValue();

		List<String> list = new ArrayList<String>();
		while (ite.hasNext()) {
			list.add(ite.next());
		}
		assertThat(list.get(0), is("ユーザIDまたはパスワードに誤りがあります。"));
	}
}
