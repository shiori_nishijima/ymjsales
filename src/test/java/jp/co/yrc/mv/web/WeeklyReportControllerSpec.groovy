package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit;
import jp.co.yrc.mv.web.WeeklyReportController.SearchCondition;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.ImportDocument.Import
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken

import mockit.MockUp
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.UserInfoDto;

import org.junit.runner.RunWith;
import org.junit.experimental.runners.Enclosed;
import spock.lang.Ignore;


/**
 * 週報画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class WeeklyReportControllerSpec {

	/**
	 * 週報画面の初期表示時のテスト
	 * 対象メソッド：init
	 * */
	static class WeeklyReportControllerSpec_週報初期表示 extends DBSpecification {
		@Autowired
		WeeklyReportController sut;

		void setupSpec() {
			// 現在日時を 2017/01/08 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2017/01/08 00:00:00"))
							return cal
						}
					}
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.init(new SearchCondition(), model, principal);

			then:
			assert actual == "weeklyReport";
		}

		def "パラメータがmodelに設定される"() {
			setup:
			def param = new SearchCondition()

			when:
			sut.init(param, model, principal);

			then:
			def actual = model.get("searchCondition");
			assert actual == param;
			assert actual.year == 2017;
			assert actual.month == 201701;
			assert actual.week == 2;
		}

		def "画面表示用DTOがmodelに設定される"() {
			when:
			sut.init(new SearchCondition(), model, principal);

			then:
			assert model.get("weeklyReport").any();
		}

		def "前週と今週の日付一覧が表示用DTOに設定される"() {
			when:
			sut.init(new SearchCondition(), model, principal);

			then:
			def weeklyReport = model.get("weeklyReport")
			def lastWeekCalls = weeklyReport.lastWeekCalls
			def thisWeekCalls = weeklyReport.thisWeekCalls

			def exLast = getExpectedDateList(1)
			def exThis = getExpectedDateList(2)

			// 前週
			lastWeekCalls.eachWithIndex { it, i ->
				def el = exLast.get(i)
				assert it.date == el;
			}
			// 今週
			thisWeekCalls.eachWithIndex { it, i ->
				def et = exThis.get(i)
				assert it.date == et;
			}
		}

		List getExpectedDateList(weekOfMonth) {
			def list = []
			Calendar cal = getCalendar(weekOfMonth);
			for (int i = 0; i < 6; i++) {
				list.push(cal.getTime())
				cal = DateUtils.getYearMonthDateCalendar(cal, 1)
			}
			return list
		}

		Calendar getCalendar(weekOfMonth) {
			Calendar cal = DateUtils.getCalendar();
			cal.set(Calendar.YEAR, 2017);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.WEEK_OF_MONTH, weekOfMonth);
			cal.set(Calendar.DAY_OF_WEEK, 2);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal;
		}
	}

	/**
	 * 週報画面の検索ボタン押下時のテスト
	 * 対象メソッド：find
	 * */
	static class WeeklyReportControllerSpec_週報検索押下 extends DBSpecification {
		@Autowired
		WeeklyReportController sut

		void setupSpec() {
			// 現在日時を 2017/01/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2017/01/01 00:00:00"))
							return cal
						}
					}
		}

		/** 営業ユーザ */
		void setSalesUser() {
			UserInfoDto userInfoDto = new UserInfoDto()
			userInfoDto.setId("ytj061")
			userInfoDto.setLastName("YTJセールス")
			userInfoDto.setFirstName("太郎1")
			userInfoDto.setAuthorization("06") // 営業権限
			userInfoDto.setCompany("YMJ")
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password")
		}

		SearchCondition setupParam() {
			// 2017年1月の4週目を指定（前週が３週目、今週が４週目になる）
			def param = new SearchCondition()
			param.year = 2017
			param.month = 201701
			param.week = 4
			param.tanto = "ytj061"
			return param
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.find(setupParam(), model, principal);

			then:
			assert actual == "weeklyReport";
		}

		def "パラメータがmodelに設定される"() {
			when:
			sut.find(setupParam(), model, principal);

			then:
			def condition = model.get("searchCondition");
			assert condition.year == 2017
			assert condition.month == 201701
			assert condition.week == 4
			assert condition.tanto == "ytj061"
		}

		def "画面表示用DTOがmodelに設定される"() {
			when:
			sut.find(setupParam(), model, principal);

			then:
			assert model.get("weeklyReport").any();
		}

		def "Paramで指定した週の前週とその次の週の日付一覧が表示用DTOに設定される"() {
			when:
			sut.find(setupParam(), model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def lastWeekCalls = weeklyReport.lastWeekCalls
			def thisWeekCalls = weeklyReport.thisWeekCalls

			def exLast = getExpectedDateList(3)
			def exThis = getExpectedDateList(4)

			// 指定した週
			lastWeekCalls.eachWithIndex { it, i ->
				def el = exLast.get(i)
				assert it.date == el;
			}
			// その次の週
			thisWeekCalls.eachWithIndex { it, i ->
				def et = exThis.get(i)
				assert it.date == et;
			}
		}

		@DBUnit
		def "Paramで指定した週の前週の行動が表示用DTOに設定される"() {
			setup:
			// 訪問計画・実績
			def exlastPlans = [6, 3, 5, 7, 9, 5]
			def exlastResults = [8, 4, 6, 8, 0, 15]

			when:
			sut.find(setupParam(), model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def lastWeekCalls = weeklyReport.lastWeekCalls

			lastWeekCalls.eachWithIndex { it, i ->
				// 訪問計画
				def ep = exlastPlans.get(i)
				assert it.plan == ep;
				// 訪問実績
				def er = exlastResults.get(i)
				assert it.result == er;
				// 得意先
				if (i == 0) {
					assert it.accountsList.join(",") == "得意先1,得意先3" // 複数得意先が設定されることを確認
				} else if (i == 1 || i == 2) {
					assert it.accountsList.join(",") == "得意先4"
				} else if (i == 4) {
					assert it.accountsList.size() == 0 // 訪問実績がない場合は得意先が設定されない
				} else {
					assert it.accountsList.join(",") == "得意先1"
				}
			}
		}

		@DBUnit
		def "Paramで指定した週の行動が表示用DTOに設定される"() {
			setup:
			// 訪問計画・実績
			def exThisPlans = [2, 6, 10, 7, 9, 0]

			when:
			sut.find(setupParam(), model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def thisWeekCalls = weeklyReport.thisWeekCalls

			thisWeekCalls.eachWithIndex { it, i ->
				// 訪問計画
				def ep = exThisPlans.get(i)
				assert it.plan == ep;
				// 得意先
				if (i == 0) {
					assert it.accountsList.join(",") == "得意先1,得意先3" // 複数得意先が設定されることを確認
				} else if (i == 1 || i == 2) {
					assert it.accountsList.join(",") == "得意先4"
				} else if (i == 5)  {
					assert it.accountsList.size() == 0 // 訪問計画がない場合は得意先が設定されない
				} else {
					assert it.accountsList.join(",") == "得意先1"
				}
			}
		}

		@DBUnit
		def "Paramで指定した週の前週と今週の訪問計画と訪問実績の合計が表示用DTOに設定される"() {
			when:
			sut.find(setupParam(), model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")

			assert weeklyReport.thisPlanTotal == 34;
			assert weeklyReport.lastPlanTotal == 35;
			assert weeklyReport.lastResultTotal == 41;
		}

		@DBUnit
		def "Paramで指定した週の前週の報告（GOOD）が表示用DTOに設定される"() {
			setup:
			def param = setupParam()
			param.week = 5

			when:
			sut.find(param, model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def goodReport = weeklyReport.goodReport
			assert goodReport.size() == 2

			def repo1 = goodReport.get(0)
			assert repo1.id == "00000000-0000-0000-0000-000000000014"
			assert repo1.infoDiv == "3"
			assert repo1.comFreeC == "得意先、感度ポジティブ、情報区分３" // 親が得意先

			def repo2 = goodReport.get(1)
			assert repo2.id == "00000000-0000-0000-0000-000000000015"
			assert repo2.infoDiv == "4"
			assert repo2.comFreeC == "案件、感度ポジティブ、情報区分４" // 親が案件
		}

		@DBUnit
		def "Paramで指定した週の前週の報告（BAD）が表示用DTOに設定される"() {
			setup:
			def param = setupParam()
			param.week = 5

			when:
			sut.find(param, model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def negativeReport = weeklyReport.negativeReport
			assert negativeReport.size() == 2

			def repo1 = negativeReport.get(0)
			assert repo1.id == "00000000-0000-0000-0000-000000000012"
			assert repo1.infoDiv == "1"
			assert repo1.comFreeC == "得意先、感度ネガティブ、情報区分１" // 親が得意先

			def repo2 = negativeReport.get(1)
			assert repo2.id == "00000000-0000-0000-0000-000000000017"
			assert repo2.infoDiv == "1"
			assert repo2.comFreeC == "案件、感度ネガティブ、情報区分１" // 親が案件
		}

		@DBUnit
		def "Paramで指定した週の前週の報告（その他）が表示用DTOに設定される"() {
			setup:
			def param = setupParam()
			param.week = 5

			when:
			sut.find(param, model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def otherReport = weeklyReport.otherReport
			assert otherReport.size() == 2

			def repo1 = otherReport.get(0)
			assert repo1.id == "00000000-0000-0000-0000-000000000013"
			assert repo1.infoDiv == "2"
			assert repo1.comFreeC == "得意先、感度その他、情報区分２" // 親が得意先

			def repo2 = otherReport.get(1)
			assert repo2.id == "00000000-0000-0000-0000-000000000016"
			assert repo2.infoDiv == "5"
			assert repo2.comFreeC == "案件、感度その他、情報区分５" // 親が案件
		}

		@DBUnit
		def "報告ごとにコメント未読が設定される"() {
			setup:
			def param = setupParam()
			param.week = 2

			when:
			sut.find(param, model, principal)

			then:
			def weeklyReport = model.get("weeklyReport")
			def goodReport = weeklyReport.goodReport.get(0)
			def negativeReport = weeklyReport.negativeReport.get(0)
			def otherReport = weeklyReport.otherReport.get(0)

			assert goodReport.commentReadCount == -1 // コメント無し
			assert negativeReport.commentReadCount == 1 // あり
			assert otherReport.commentReadCount == 0 // なし
		}

		List getExpectedDateList(weekOfMonth) {
			def list = []
			Calendar cal = getCalendar(weekOfMonth);
			for (int i = 0; i < 6; i++) {
				list.push(cal.getTime())
				cal = DateUtils.getYearMonthDateCalendar(cal, 1)
			}
			return list
		}

		Calendar getCalendar(weekOfMonth) {
			Calendar cal = DateUtils.getCalendar();
			cal.set(Calendar.YEAR, 2017);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.WEEK_OF_MONTH, weekOfMonth);
			cal.set(Calendar.DAY_OF_WEEK, 2);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return cal;
		}
	}
}
