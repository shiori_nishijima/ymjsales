package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification


/**
 * 日報確認画面(管理者)の多言語化対応(英語)テスト
 * @author komatsu
 * 
 */
class DailyReportViewerAdminLabelEnGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleEn').click()
		$('#btnLogin').click()
		waitFor{$("div[data-role='page'].ui-page-active").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
		waitFor{$("#container").isDisplayed()}

		$("#headerMenu-report").click()
		waitFor{$("#container").isDisplayed()}
		// 日報確認画面(管理者)に遷移
		$("img[src='/ymjmv/images/dashboard/dailyReportViewerAdmin.png']").click()
		waitFor{$("#contentsWrapper").isDisplayed()}
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "Daily report confirmation (Manager)"
	}

	def "ボタンの文言が正しいこと"() {
		expect:
		$("#selectAll").value() == "Select all" // 全選択
		$("#confirmComment").value() == "Update to be confirmed" // 確認済みへ変更
	}

	def "テーブルヘッダの文言が正しいこと"() {
		when:
		def column = $("#table_head").find("th")

		then:
		// 確認
		column.eq(1).text() == "Confirmed"
		// セールスコメント
		column.eq(7).text() == "Sales representative notes"
	}

	def "テーブル下部の文言が正しいこと"() {
		expect:
		// 確認ボタン
		$(".comment-button-narrow").value() == "Confirmed"
		// セールスコメント
		$(".dataTables_scrollFootInner").find(".comment input").attr("placeholder") == "Sales representative notes"
	}
}
