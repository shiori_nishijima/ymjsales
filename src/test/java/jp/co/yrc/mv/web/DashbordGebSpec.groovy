package jp.co.yrc.mv.web

import geb.spock.GebSpec
import groovy.sql.Sql
import spock.lang.Shared

class DashbordGebSpec extends GebSpec {
	
	// TODO 初期データ投入をDBUnitでやるか？

	@Shared
	def  sql;
	def setupSpec(){
		sql = Sql.newInstance("jdbc:mysql://localhost:3306/yrcsfa", "root", "passw0rd", "com.mysql.jdbc.Driver")

		sql.execute ''' DELETE FROM users WHERE id LIKE 'dashbord%' '''
		sql.execute ''' DELETE FROM mst_eigyo_sosiki WHERE eigyo_sosiki_cd LIKE 'dashbord%' '''
		sql.execute ''' DELETE FROM mst_tanto_s_sosiki WHERE tanto_cd LIKE 'dashbord%' '''

		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord1','1','bed128365216c019988915ed3add75fb','1','1','01','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord2','2','bed128365216c019988915ed3add75fb','1','1','02','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord3','3','bed128365216c019988915ed3add75fb','1','1','03','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord4','4','bed128365216c019988915ed3add75fb','1','1','04','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord5','5','bed128365216c019988915ed3add75fb','1','1','05','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord6','6','bed128365216c019988915ed3add75fb','1','1','06','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord7','7','bed128365216c019988915ed3add75fb','1','1','07','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord8','8','bed128365216c019988915ed3add75fb','1','1','08','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord9','9','bed128365216c019988915ed3add75fb','1','1','09','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord10','10','bed128365216c019988915ed3add75fb','1','1','10','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord11','11','bed128365216c019988915ed3add75fb','1','1','11','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''
		sql.execute ''' INSERT INTO users ( id, id_for_customer, user_hash, first_name, last_name, authorization, permit_master_edit, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord12','12','bed128365216c019988915ed3add75fb','1','1','12','1',false,'2000-01-01 00:00:00','2000-01-01 00:00:00','system','system'); '''


		sql.execute ''' INSERT INTO mst_eigyo_sosiki ( eigyo_sosiki_cd, eigyo_sosiki_nm, eigyo_sosiki_short_nm, corp_cd, div1_cd, div2_cd, div3_cd, div4_cd, div5_cd, div6_cd, div1_nm, div2_nm, div3_nm, div4_nm, div5_nm, div6_nm, is_sfa, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord000','1','1','A040AZ','','','','','','','','','','','','',false,false,null,null,'','');'''
		sql.execute ''' INSERT INTO mst_eigyo_sosiki ( eigyo_sosiki_cd, eigyo_sosiki_nm, eigyo_sosiki_short_nm, corp_cd, div1_cd, div2_cd, div3_cd, div4_cd, div5_cd, div6_cd, div1_nm, div2_nm, div3_nm, div4_nm, div5_nm, div6_nm, is_sfa, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord001','1','','A040AZ','dashbord001','','','','','','','','','','','',false,false,null,null,'','');'''
		sql.execute ''' INSERT INTO mst_eigyo_sosiki ( eigyo_sosiki_cd, eigyo_sosiki_nm, eigyo_sosiki_short_nm, corp_cd, div1_cd, div2_cd, div3_cd, div4_cd, div5_cd, div6_cd, div1_nm, div2_nm, div3_nm, div4_nm, div5_nm, div6_nm, is_sfa, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord011','1','','A040AZ','dashbord001','dashbord011','','','','','','','','','','',false,false,null,null,'','');'''
		sql.execute ''' INSERT INTO mst_eigyo_sosiki ( eigyo_sosiki_cd, eigyo_sosiki_nm, eigyo_sosiki_short_nm, corp_cd, div1_cd, div2_cd, div3_cd, div4_cd, div5_cd, div6_cd, div1_nm, div2_nm, div3_nm, div4_nm, div5_nm, div6_nm, is_sfa, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord111','1','','A040AZ','dashbord001','dashbord011','dashbord111','','','','','','','','','',false,false,null,null,'','');'''

		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord1','dashbord000',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord2','dashbord001',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord3','dashbord001',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord4','dashbord011',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord5','dashbord111',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord6','dashbord111',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord7','dashbord011',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord8','dashbord111',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord9','dashbord111',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord10','dashbord011',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord11','dashbord111',false,null,null,null,null); '''
		sql.execute ''' INSERT INTO mst_tanto_s_sosiki ( tanto_cd, eigyo_sosiki_cd, deleted, date_entered, date_modified, modified_user_id, created_by ) VALUES ('dashbord12','dashbord111',false,null,null,null,null); '''
	}

	def cleanupSpec(){
		if (sql != null) {
			sql.execute ''' DELETE FROM users WHERE id LIKE 'dashbord%' '''
			sql.execute ''' DELETE FROM mst_eigyo_sosiki WHERE eigyo_sosiki_cd LIKE 'dashbord%' '''
			sql.execute ''' DELETE FROM mst_tanto_s_sosiki WHERE tanto_cd LIKE 'dashbord%' '''
			sql.close()
		}
	}

	def final ALL =  	[
		'visitList.html',
		'sum.html',
		'monthlySum.html',
		'csvDownload.html',
		'dailyReportViewerAdmin.html',
		'dailyReportViewerSales.html',
		'unvisitAccountList.html',
		'visitPlanList.html',
		'chartBudgetPerformanceComparison.html',
		'chartKindAchievementRate.html',
		'chartSosikiAchievementRate.html',
		'tech/visitList.html',
		'accountActualReference.html',
		'logInfo.html',
		'accountGroupingVisitList.html',
		'salesAndKindActualReference.html',
		'callsUsersCount.html'
	]

	def final COMPANY_DEPARTMENT_OFFICE =  	[
		'visitList.html',
		'sum.html',
		'monthlySum.html',
		'csvDownload.html',
		'dailyReportViewerAdmin.html',
		'dailyReportViewerSales.html',
		'unvisitAccountList.html',
		'visitPlanList.html',
		'chartBudgetPerformanceComparison.html',
		'chartKindAchievementRate.html',
		'chartSosikiAchievementRate.html',
		'accountActualReference.html',
		'logInfo.html',
		'accountGroupingVisitList.html',
		'salesAndKindActualReference.html',
		'callsUsersCount.html'
	]


	def final SALES = [
		'visitList.html',
		'sum.html',
		'monthlySum.html',
		'dailyReportViewerSales.html',
		'unvisitAccountList.html',
		'visitPlanList.html',
		'chartBudgetPerformanceComparison.html',
		'chartKindAchievementRate.html',
		'chartSosikiAchievementRate.html',
		'accountActualReference.html',
		'logInfo.html',
		'accountGroupingVisitList.html',
		'salesAndKindActualReference.html',
		'callsUsersCount.html'
	]

	def final TECH =  	['tech/visitList.html',]

	def "権限01"() {
		setup:
		def expected = ALL

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord1')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}

	def "権限02"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord2')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}

	def "権限03"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord3')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}

	def "権限04"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord4')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}



	def "権限05"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord5')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限06"() {
		setup:
		def expected = 	SALES

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord6')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限07"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord7')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限08"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord8')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限09"() {
		setup:
		def expected = SALES

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord9')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限10"() {
		setup:
		def expected = COMPANY_DEPARTMENT_OFFICE

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord10')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限11"() {
		setup:
		def expected = 	SALES

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord11')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}


	def "権限12"() {
		setup:
		def expected = TECH

		when:
		go "http://localhost:8080/ymjmv/"
		$('#userId').value('dashbord12')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click();


		then:
		def actual = []
		def menus = $("[data-menu-id]")
		menus.each ({
			actual << it.attr('data-menu-id')
		})
		actual == expected
	}
}
