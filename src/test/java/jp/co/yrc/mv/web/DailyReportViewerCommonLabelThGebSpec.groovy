package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit


/**
 * 日報確認画面(共通)の多言語化対応(タイ語)テスト
 * @author komatsu
 * 
 */
class DailyReportViewerCommonLabelThGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{$("div[data-role='page'].ui-page-active").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
		waitFor{$("#container").isDisplayed()}

		$("#headerMenu-report").click()
		waitFor{$("img.dashboardImg").size() == 3}
		// 日報確認画面(管理者)に遷移
		$("img[src='/ymjmv/images/dashboard/dailyReportViewerAdmin.png']").click()
		waitFor{$("#contentsWrapper").isDisplayed()}
	}

	def "Datatablesの文言が正しいこと"() {
		expect:
		// _MENU_ 件表示
		$("#DataTables_Table_0_length").text() == "แสดง\n10\n25\n50\n100\nแถว"
		// 検索：
		$("#DataTables_Table_0_filter").text() == "ค้นหา:"
		$(".previous").text() == "ก่อนหน้า" // 前
		$(".next").text() == "ถัดไป" // 次
	}

	def "検索ボタンの文言が正しいこと"() {
		expect:
		$(".search").value() == "ค้นหา"
	}

	def "テーブルヘッダの文言が正しいこと"() {
		when:
		def column = $("#table_head").find("th")

		then:
		column.eq(2).text() == "แผนก" // 所属名
		column.eq(3).text() == "ผู้แทนขาย" // セールスマン名
		column.eq(4).text() == "วันที่เยี่ยมชม" // 訪問日
		column.eq(5).text() == "เยี่ยมชม" // 訪問件数
		column.eq(6).text() == "เบอร์โทร" // 電話件数
		try {
			js."document.getElementsByClassName('dataTables_scrollHeadInner')[0].getElementsByTagName('th')[9].scrollIntoView(true)"
		} catch(Error e) {}
		column.eq(8).text() == "ได้รับการยืนยันโดยผู้จัดการ" // 上司確認
		column.eq(9).text() == "วันที่ยืนยันข้อมูล" // 確認日
	}

	def "テーブル下部の文言が正しいこと"() {
		when:
		def column = $(".dataTables_scrollFootInner").find("input")

		then:
		column.eq(1).attr("placeholder") == "แผนก" // 所属名
		column.eq(2).attr("placeholder") == "ผู้แทนขาย" // セールスマン名
		column.eq(3).attr("placeholder") == "วันที่เยี่ยมชม" // 訪問日
		column.eq(4).attr("placeholder") == "เยี่ยมชม" // 訪問件数
		column.eq(5).attr("placeholder") == "เบอร์โทร" // 電話件数
		column.eq(7).attr("placeholder") == "ได้รับการยืนยันโดยผู้จัดการ" // 上司確認
		column.eq(8).attr("placeholder") == "วันที่ยืนยันข้อมูล" // 確認日
	}

	@GebDBUnit
	def "確認「未」の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$(".search").click()

		when:
		def label = $("span[data-roll-confirm='00000000-0000-0000-0000-000000000001']").text()

		then:
		label == "ไม่สำเร็จ"
		// _TOTAL_ 件中 _START_ から _END_ まで表示（０件でない場合）
		$("#DataTables_Table_0_info").text() == "แสดง 1 ถึง 1 จาก 1 แถว"
	}

	@GebDBUnit
	def "確認「済」の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$(".search").click()

		when:
		def label = $("span[data-roll-confirm='00000000-0000-0000-0000-000000000001']").text()

		then:
		label == "สำเร็จ"
	}

	@GebDBUnit
	def "Datatablesの文言が正しいこと_データが存在しない場合"() {
		setup:
		$(".search").click()

		expect:
		// データはありません。
		$(".dataTables_empty").text() == "ไม่พบข้อมูล"
		// _TOTAL_ 件中 _START_ から _END_ まで表示（０件の場合）
		$("#DataTables_Table_0_info").text() == "แสดง 0 ถึง 0 จาก 0 แถว"
	}

	/**
	 * 検索日付を2016年9月に設定
	 */
	void setupDatePicker() {
		$("#year").click()
		$("#year").find("option[value='2016']").click()

		$("#month").click()
		$("#month").find("option[value='201609']").click()

		$("#date").click()
		$("#date").find("option[value='-1']").click()
	}
}
