package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.web.AccountActualReferenceController.Result;
import jp.co.yrc.mv.web.AccountActualReferenceController.SearchCondition;
import mockit.Mock;
import mockit.MockUp;

@SuppressWarnings("unchecked")
public class AccountActualReferenceControllerTest extends DataSourceBasedDBTestCaseBase {

	@Autowired
	AccountActualReferenceController sut;

	@BeforeClass
	public static void setUpClass() {
		// このモック記述では記述したところだけ置き換わる
		new MockUp<DateUtils>() {
			@Mock
			public Calendar getYearMonthCalendar() {
				// 現在を2016/4/10とするため、現在年月201604とする。
				Calendar c = Calendar.getInstance();
				c.set(Calendar.YEAR, 2016);
				c.set(Calendar.MONTH, 3);
				c.set(Calendar.DATE, 1);
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				return c;
			}

			@Mock
			public Calendar getCalendar() {
				// 現在を2016/4/10とする。
				Calendar c = Calendar.getInstance();
				c.set(Calendar.YEAR, 2016);
				c.set(Calendar.MONTH, 3);
				c.set(Calendar.DATE, 10);
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				return c;
			}
		};
	}

	/**
	 * 得意先・年月・担当者に紐づく訪問は存在するが、resultが存在しない場合のリアル担当者検索
	 */
	@Test
	public void データが存在しない場合の検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");

		sut.find(param, model, principal);
		List<Result> list = (List<Result>) model.get("list");

		assertThat(list.size(), is(0)); // 得意先が取得できないこと
	}

	@Test
	public void 現在月でデータが存在する場合のリアル担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertTanto(list, true); // 10日までのデータを集計
	}

	@Test
	public void 現在月でデータが存在する場合のリアル営業所検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertThat(list.size(), is(2));
		assertTanto(list, true);
		assertSalesOffice(list); // 10日までのデータを集計
	}

	@Test
	public void 現在月でデータが存在する場合の前日担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");
		param.setSearchMode("yesterday");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertThat(list.size(), is(1));
		assertTantoYesterday(list, true); // 9日までのデータを集計、訪問計画・実績数はリアルデータを集計
	}

	@Test
	public void 現在月でデータが存在する場合の前日営業所検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setSearchMode("yesterday");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertThat(list.size(), is(2));
		assertTantoYesterday(list, true);
		assertSalesOfficeYesterday(list); // 9日までのデータを集計、訪問計画・実績数はリアルデータを集計
	}

	@Test
	public void 過去月でデータが存在する場合の担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(3);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertThat(list.size(), is(1));
		assertTantoPast(list, true); // ３月末までのデータを集計
	}

	@Test
	public void 得意先に紐づく訪問計画と実績が存在しない場合のリアル担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertTanto(list, false);
	}

	@Test
	public void 得意先に紐づく訪問計画と実績が存在しない場合の過去月の担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(3);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertTantoYesterday(list, false);
	}

	@Test
	public void 得意先に紐づく訪問計画と実績が存在しない場合の前日担当者検索() {
		SearchCondition param = new SearchCondition();
		param.setYear(2016);
		param.setMonth(4);
		param.setDiv1("0001110103");
		param.setDiv2("J310");
		param.setDiv3("1100");
		param.setTanto("ytj061");
		param.setSearchMode("yesterday");

		sut.find(param, model, principal);

		List<Result> list = (List<Result>) model.get("list");
		assertTantoYesterday(list, false);
	}

	/**
	 * 担当者に紐づくデータの確認
	 * 
	 * @param list
	 */
	private void assertTanto(List<Result> list, boolean calls) {
		Result row0 = list.get(0);
		assertThat(row0.getId(), is("2000000001"));
		assertThat(row0.getAccountName(), is("得意先1"));
		assertThat(row0.getFirstName(), is("セールス太郎1")); // 担当者
		assertThat(row0.getLastName(), is("YTJ")); // 担当者
		if (calls) {
			assertThat(row0.getPlannedCount(), is(4)); // 計画数
			assertThat(row0.getHeldCount(), is(5)); // 実績数
		} else {
			// 得意先に紐づく訪問計画・実績が存在しない場合
			assertThat(row0.getPlannedCount(), is(0));
			assertThat(row0.getHeldCount(), is(0));
		}
		assertThat(row0.getMarketName(), is("ＳＳ")); // 販路
		assertThat(row0.getRank(), is("A")); // ランク
		assertThat(row0.getDemandNumber().toString(), is("0")); // 需要
		assertThat(row0.isGroupImportant(), is(true)); // 重点

		// 合計
		assertThat(row0.getNumber().tire.intValue(), is(820));
		assertThat(row0.getSales().tire.intValue(), is(8200)); // 1/1000した値
		assertThat(row0.getNumber().pcr.intValue(), is(60));
		assertThat(row0.getSales().pcr.intValue(), is(600));
		assertThat(row0.getNumber().tbs.intValue(), is(60));
		assertThat(row0.getSales().tbs.intValue(), is(600));
		assertThat(row0.getNumber().van.intValue(), is(140));
		assertThat(row0.getSales().van.intValue(), is(1400));
		assertThat(row0.getNumber().ltr.intValue(), is(220));
		assertThat(row0.getSales().ltr.intValue(), is(2200));
		assertThat(row0.getSales().total.intValue(), is(11600));
		// 伸長率
		assertThat(row0.getNumber().getTireElongation().doubleValue(), is(828.3)); // 四捨五入テスト
		assertThat(row0.getSales().getTireElongation().doubleValue(), is(941.8));
		assertThat(row0.getNumber().getPcrElongation().doubleValue(), is(461.5));
		assertThat(row0.getSales().getPcrElongation().doubleValue(), is(666.4));
		assertThat(row0.getNumber().getTbsElongation().doubleValue(), is(1000.0));
		assertThat(row0.getSales().getTbsElongation().doubleValue(), is(998.9));
		assertThat(row0.getNumber().getVanElongation().doubleValue(), is(736.8));
		assertThat(row0.getSales().getVanElongation().doubleValue(), is(932.9)); // 四捨五入テスト
		assertThat(row0.getNumber().getLtrElongation().doubleValue(), is(814.8));
		assertThat(row0.getSales().getLtrElongation().doubleValue(), is(956.0));
		assertThat(row0.getSales().getTotalElongation().doubleValue(), is(1332.3));
		// 当日
		assertThat(row0.getNumber().todayTire.intValue(), is(410));
		assertThat(row0.getSales().todayTire.intValue(), is(4100)); // 1/1000した値
		assertThat(row0.getNumber().todayPcr.intValue(), is(30));
		assertThat(row0.getSales().todayPcr.intValue(), is(300));
		assertThat(row0.getNumber().todayTbs.intValue(), is(30));
		assertThat(row0.getSales().todayTbs.intValue(), is(300));
		assertThat(row0.getNumber().todayVan.intValue(), is(70));
		assertThat(row0.getSales().todayVan.intValue(), is(700));
		assertThat(row0.getNumber().todayLtr.intValue(), is(110));
		assertThat(row0.getSales().todayLtr.intValue(), is(1100));
		assertThat(row0.getSales().todayTotal.intValue(), is(5900));
	}

	/**
	 * 営業所に紐づくデータの確認
	 * 
	 * @param list
	 */
	private void assertSalesOffice(List<Result> list) {
		Result row1 = list.get(1);
		assertThat(row1.getId(), is("2000000002"));
		assertThat(row1.getAccountName(), is("得意先2"));
		assertThat(row1.getFirstName(), is("セールス太郎2")); // 担当者
		assertThat(row1.getLastName(), is("YTJ")); // 担当者
		assertThat(row1.getPlannedCount(), is(4)); // 計画数
		assertThat(row1.getHeldCount(), is(5)); // 実績数
		assertThat(row1.getMarketName(), is("ＳＳ")); // 販路
		assertThat(row1.getRank(), is("A")); // ランク
		assertThat(row1.getDemandNumber().toString(), is("0")); // 需要
		assertThat(row1.isGroupImportant(), is(true)); // 重点

		// 合計
		assertThat(row1.getNumber().tire.intValue(), is(110));
		assertThat(row1.getSales().tire.intValue(), is(1100)); // 1/1000した値
		assertThat(row1.getNumber().pcr.intValue(), is(33));
		assertThat(row1.getSales().pcr.intValue(), is(330));
		assertThat(row1.getNumber().tbs.intValue(), is(0));
		assertThat(row1.getSales().tbs.intValue(), is(0));
		assertThat(row1.getNumber().van.intValue(), is(77));
		assertThat(row1.getSales().van.intValue(), is(770));
		assertThat(row1.getNumber().ltr.intValue(), is(0));
		assertThat(row1.getSales().ltr.intValue(), is(0));
		// 伸長率
		assertThat(row1.getNumber().getTireElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getTireElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getPcrElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getPcrElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getTbsElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getTbsElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getVanElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getVanElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getLtrElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getLtrElongation().doubleValue(), is(0.0));
		// 当日
		assertThat(row1.getNumber().todayTire.intValue(), is(10));
		assertThat(row1.getSales().todayTire.intValue(), is(100));
		assertThat(row1.getNumber().todayPcr.intValue(), is(3));
		assertThat(row1.getSales().todayPcr.intValue(), is(30));
		assertThat(row1.getNumber().todayTbs.intValue(), is(0));
		assertThat(row1.getSales().todayTbs.intValue(), is(0));
		assertThat(row1.getNumber().todayVan.intValue(), is(7));
		assertThat(row1.getSales().todayVan.intValue(), is(70));
		assertThat(row1.getNumber().todayLtr.intValue(), is(0));
		assertThat(row1.getSales().todayLtr.intValue(), is(0));
	}

	/**
	 * 営業所に紐づくデータの確認
	 * 
	 * @param list
	 */
	private void assertSalesOfficeYesterday(List<Result> list) {
		Result row1 = list.get(1);
		assertThat(row1.getId(), is("2000000002"));
		assertThat(row1.getAccountName(), is("得意先2"));
		assertThat(row1.getFirstName(), is("セールス太郎2")); // 担当者
		assertThat(row1.getLastName(), is("YTJ")); // 担当者
		assertThat(row1.getPlannedCount(), is(4)); // 計画数
		assertThat(row1.getHeldCount(), is(5)); // 実績数
		assertThat(row1.getMarketName(), is("ＳＳ")); // 販路
		assertThat(row1.getRank(), is("A")); // ランク
		assertThat(row1.getDemandNumber().toString(), is("0")); // 需要
		assertThat(row1.isGroupImportant(), is(true)); // 重点

		// 合計
		assertThat(row1.getNumber().tire.intValue(), is(100));
		assertThat(row1.getSales().tire.intValue(), is(1000)); // 1/1000した値
		assertThat(row1.getNumber().pcr.intValue(), is(30));
		assertThat(row1.getSales().pcr.intValue(), is(300));
		assertThat(row1.getNumber().tbs.intValue(), is(0));
		assertThat(row1.getSales().tbs.intValue(), is(0));
		assertThat(row1.getNumber().van.intValue(), is(70));
		assertThat(row1.getSales().van.intValue(), is(700));
		assertThat(row1.getNumber().ltr.intValue(), is(0));
		assertThat(row1.getSales().ltr.intValue(), is(0));
		// 伸長率
		assertThat(row1.getNumber().getTireElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getTireElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getPcrElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getPcrElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getTbsElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getTbsElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getVanElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getVanElongation().doubleValue(), is(0.0));
		assertThat(row1.getNumber().getLtrElongation().doubleValue(), is(0.0));
		assertThat(row1.getSales().getLtrElongation().doubleValue(), is(0.0));
		// 当日
		assertThat(row1.getNumber().todayTire.intValue(), is(0));
		assertThat(row1.getSales().todayTire.intValue(), is(0));
		assertThat(row1.getNumber().todayPcr.intValue(), is(0));
		assertThat(row1.getSales().todayPcr.intValue(), is(0));
		assertThat(row1.getNumber().todayTbs.intValue(), is(0));
		assertThat(row1.getSales().todayTbs.intValue(), is(0));
		assertThat(row1.getNumber().todayVan.intValue(), is(0));
		assertThat(row1.getSales().todayVan.intValue(), is(0));
		assertThat(row1.getNumber().todayLtr.intValue(), is(0));
		assertThat(row1.getSales().todayLtr.intValue(), is(0));
	}

	/**
	 * 担当者に紐づくデータの確認
	 * 
	 * @param list
	 */
	private void assertTantoYesterday(List<Result> list, boolean calls) {
		Result row0 = list.get(0);
		assertThat(row0.getId(), is("2000000001"));
		assertThat(row0.getAccountName(), is("得意先1"));
		assertThat(row0.getFirstName(), is("セールス太郎1")); // 担当者
		assertThat(row0.getLastName(), is("YTJ")); // 担当者
		if (calls) {
			assertThat(row0.getPlannedCount(), is(4)); // 計画数
			assertThat(row0.getHeldCount(), is(5)); // 実績数
		} else {
			// 得意先に紐づく訪問計画・実績が存在しない場合
			assertThat(row0.getPlannedCount(), is(0));
			assertThat(row0.getHeldCount(), is(0));
		}
		assertThat(row0.getMarketName(), is("ＳＳ")); // 販路
		assertThat(row0.getRank(), is("A")); // ランク
		assertThat(row0.getDemandNumber().toString(), is("0")); // 需要
		assertThat(row0.isGroupImportant(), is(true)); // 重点

		// 合計
		assertThat(row0.getNumber().tire.intValue(), is(410));
		assertThat(row0.getSales().tire.intValue(), is(4100));
		assertThat(row0.getNumber().pcr.intValue(), is(30));
		assertThat(row0.getSales().pcr.intValue(), is(300));
		assertThat(row0.getNumber().tbs.intValue(), is(30));
		assertThat(row0.getSales().tbs.intValue(), is(300));
		assertThat(row0.getNumber().van.intValue(), is(70));
		assertThat(row0.getSales().van.intValue(), is(700));
		assertThat(row0.getNumber().ltr.intValue(), is(110));
		assertThat(row0.getSales().ltr.intValue(), is(1100));
		assertThat(row0.getSales().total.intValue(), is(5700));

		// 伸長率
		assertThat(row0.getNumber().getTireElongation().doubleValue(), is(414.1));
		assertThat(row0.getSales().getTireElongation().doubleValue(), is(470.9));
		assertThat(row0.getNumber().getPcrElongation().doubleValue(), is(230.8));
		assertThat(row0.getSales().getPcrElongation().doubleValue(), is(333.2));
		assertThat(row0.getNumber().getTbsElongation().doubleValue(), is(500.0));
		assertThat(row0.getSales().getTbsElongation().doubleValue(), is(499.5));
		assertThat(row0.getNumber().getVanElongation().doubleValue(), is(368.4));
		assertThat(row0.getSales().getVanElongation().doubleValue(), is(466.4));
		assertThat(row0.getNumber().getLtrElongation().doubleValue(), is(407.4));
		assertThat(row0.getSales().getLtrElongation().doubleValue(), is(478.0));
		assertThat(row0.getSales().getTotalElongation().doubleValue(), is(654.7));
	}

	/**
	 * 担当者に紐づくデータの確認（過去検索時）
	 * 
	 * @param list
	 */
	private void assertTantoPast(List<Result> list, boolean calls) {
		Result row0 = list.get(0);
		assertThat(row0.getId(), is("2000000001"));
		assertThat(row0.getAccountName(), is("得意先1"));
		assertThat(row0.getFirstName(), is("セールス太郎1")); // 担当者
		assertThat(row0.getLastName(), is("YTJ")); // 担当者
		if (calls) {
			assertThat(row0.getPlannedCount(), is(3)); // 計画数
			assertThat(row0.getHeldCount(), is(3)); // 実績数
		} else {
			// 得意先に紐づく訪問計画・実績が存在しない場合
			assertThat(row0.getPlannedCount(), is(0));
			assertThat(row0.getHeldCount(), is(0));
		}
		assertThat(row0.getMarketName(), is("ＳＳ")); // 販路
		assertThat(row0.getRank(), is("A")); // ランク
		assertThat(row0.getDemandNumber().toString(), is("0")); // 需要
		assertThat(row0.isGroupImportant(), is(true)); // 重点

		// 合計
		assertThat(row0.getNumber().tire.intValue(), is(410));
		assertThat(row0.getSales().tire.intValue(), is(4100));
		assertThat(row0.getNumber().pcr.intValue(), is(30));
		assertThat(row0.getSales().pcr.intValue(), is(300));
		assertThat(row0.getNumber().tbs.intValue(), is(30));
		assertThat(row0.getSales().tbs.intValue(), is(300));
		assertThat(row0.getNumber().van.intValue(), is(70));
		assertThat(row0.getSales().van.intValue(), is(700));
		assertThat(row0.getNumber().ltr.intValue(), is(110));
		assertThat(row0.getSales().ltr.intValue(), is(1100));
		assertThat(row0.getSales().total.intValue(), is(5700));

		// 伸長率
		assertThat(row0.getNumber().getTireElongation().doubleValue(), is(414.1));
		assertThat(row0.getSales().getTireElongation().doubleValue(), is(470.9));
		assertThat(row0.getNumber().getPcrElongation().doubleValue(), is(230.8));
		assertThat(row0.getSales().getPcrElongation().doubleValue(), is(333.2));
		assertThat(row0.getNumber().getTbsElongation().doubleValue(), is(500.0));
		assertThat(row0.getSales().getTbsElongation().doubleValue(), is(499.5));
		assertThat(row0.getNumber().getVanElongation().doubleValue(), is(368.4));
		assertThat(row0.getSales().getVanElongation().doubleValue(), is(466.4));
		assertThat(row0.getNumber().getLtrElongation().doubleValue(), is(407.4));
		assertThat(row0.getSales().getLtrElongation().doubleValue(), is(478.0));
		assertThat(row0.getSales().getTotalElongation().doubleValue(), is(654.7));
	}
}
