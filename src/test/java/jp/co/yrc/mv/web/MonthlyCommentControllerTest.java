package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.security.Principal;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.sql.DataSource;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.dto.MonthlyCommentsDto;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.rest.MonthlyCommentController;
import jp.co.yrc.mv.rest.MonthlyCommentController.MonthlyCommentParam;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class MonthlyCommentControllerTest extends DataSourceBasedDBTestCaseBase {

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/" + getClass().getSimpleName();

	@Autowired
	MonthlyCommentController sut;

	static Principal principal;

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XlsDataSet(new File(RESOURCE_PATH + testClassName + ".xls"));
	}

	@BeforeClass
	public static void setUpClass() {
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		userInfoDto.setLastName("lastName");
		userInfoDto.setFirstName("firstName");
		Authentication authentication = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		principal = authentication;
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();

	}

	@Test
	public void 月別コメントを表示する() {
		MonthlyCommentsDto dto = new MonthlyCommentsDto();
		dto.setYear(2014);
		dto.setMonth(5);
		dto.setUserId("userId");

		MonthlyCommentsDto actual = sut.showMonthlyComments(dto, null);

		MonthlyCommentsDto expected = new MonthlyCommentsDto();
		expected.setYear(2014);
		expected.setMonth(5);
		expected.setBossComment1("bossComment1");
		expected.setBossComment2("bossComment2");
		expected.setBossComment3("bossComment3");
		expected.setBossComment4("bossComment4");
		expected.setBossComment5("bossComment5");
		expected.setUserComment("userComment");
		expected.setLastName("lastName");
		expected.setFirstName("firstName");
		expected.setId("comId");

		assertThat(actual.getYear(), is(expected.getYear()));
		assertThat(actual.getMonth(), is(expected.getMonth()));
		assertThat(actual.getBossComment1(), is(expected.getBossComment1()));
		assertThat(actual.getBossComment2(), is(expected.getBossComment2()));
		assertThat(actual.getBossComment3(), is(expected.getBossComment3()));
		assertThat(actual.getBossComment4(), is(expected.getBossComment4()));
		assertThat(actual.getBossComment5(), is(expected.getBossComment5()));
		assertThat(actual.getUserComment(), is(expected.getUserComment()));
		assertThat(actual.getLastName(), is(expected.getLastName()));
		assertThat(actual.getFirstName(), is(expected.getFirstName()));
		assertThat(actual.getId(), is(expected.getId()));
	}

	@Test
	public void 月別コメントを保存する() throws SQLException, Exception {
		MonthlyCommentParam dto = new MonthlyCommentParam();
		dto.setBossComment1("bossComment1");
		dto.setBossComment2("bossComment2");
		dto.setBossComment3("bossComment3");
		dto.setBossComment4("bossComment4");
		dto.setBossComment5("bossComment5");
		dto.setUserComment("userComment");
		dto.setId("comId");

		sut.saveMonthlyComment(dto, principal);

		IDataSet actualDataSet = getConnection().createDataSet();
		ITable actual = actualDataSet.getTable("monthly_comments");

		IDataSet expectedDataSet = new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_expected.xls"));
		ITable expected = expectedDataSet.getTable("monthly_comments");

		assertTable(actual, expected);
	}

	public void assertTable(ITable actual, ITable expected) throws DataSetException {
		// カラムのデータをすべて取得する
		Map<String, Object> actualMap = new LinkedHashMap<String, Object>();
		for (Column column : actual.getTableMetaData().getColumns()) {
			actualMap.put(column.getColumnName(), actual.getValue(0, column.getColumnName()));
		}

		Map<String, Object> expectedMap = new LinkedHashMap<String, Object>();
		for (Column column : expected.getTableMetaData().getColumns()) {
			expectedMap.put(column.getColumnName(), expected.getValue(0, column.getColumnName()));
		}

		// DB状態確認
		for (String key : actualMap.keySet()) {
			if (!("date_entered".equals(key) || "date_modified".equals(key))) {
				if ("year".equals(key) || "month".equals(key)) {
					assertThat(String.valueOf(actualMap.get(key)), is(String.valueOf(expectedMap.get(key))));
				} else {
					assertThat(actualMap.get(key), is(expectedMap.get(key)));
				}
			}
		}
	}
}
