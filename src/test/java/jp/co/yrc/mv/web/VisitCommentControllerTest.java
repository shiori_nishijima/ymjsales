package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.security.Principal;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.sql.DataSource;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.rest.VisitCommentController;
import jp.co.yrc.mv.rest.VisitCommentController.CallsResult;
import jp.co.yrc.mv.rest.VisitCommentController.CommentsResult;
import jp.co.yrc.mv.rest.VisitCommentController.RegisterParam;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class VisitCommentControllerTest extends DataSourceBasedDBTestCaseBase {

	private final String RESOURCE_PATH = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	private String testClassName = "xls/" + getClass().getSimpleName();

	private String testMethodName = null;

	@Autowired
	VisitCommentController sut;

	static Principal principal;

	@Autowired
	private TransactionAwareDataSourceProxy dataSourceTest;

	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
	}

	@Override
	protected DataSource getDataSource() {
		return dataSourceTest;
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_" + testMethodName + ".xls"));
	}

	@BeforeClass
	public static void setUpClass() {
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		userInfoDto.setLastName("lastName");
		userInfoDto.setFirstName("firstName");
		Authentication authentication = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		principal = authentication;
	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Rule
	public TestRule testName = new TestWatcher() {
		protected void starting(Description d) {
			testMethodName = d.getMethodName();
		};
	};

	@Test
	public void コメント情報取得と同時にコメントを既読に更新() throws SQLException, Exception {
		String callId = "callId";
		CallsResult actual = sut.getComments(callId, null, principal);

		assertThat(actual.id, is("callId"));
		assertThat(actual.dateFrom, is("2014/05/05 00:00"));
		assertThat(actual.dateTo, is("00:00"));
		assertThat(actual.name, is("callName"));
		assertThat(actual.firstName, is("firstName"));
		assertThat(actual.lastName, is("lastName"));
		assertThat(actual.detail, is("comFreeC"));
		assertThat(actual.likeCount, is(1));
		assertTrue(actual.isLike);

		assertThat(actual.comments.get(0).id, is("comId"));
		assertThat(actual.comments.get(0).name, is("comName"));
		assertThat(actual.comments.get(0).firstName, is("firstName"));
		assertThat(actual.comments.get(0).lastName, is("lastName"));
		assertThat(actual.comments.get(0).userName, is("userName"));
		assertThat(actual.comments.get(0).description, is("description"));
		assertThat(actual.comments.get(0).date, is("2014/05/05 00:00"));

		// commentsReadUsersテーブルに一件インサートされているかの検証
		IDataSet actualDataSet = getConnection().createDataSet();
		ITable actualTable = actualDataSet.getTable("comments_read_users");

		assertThat(actualTable.getRowCount(), is(1));
		assertThat((String) actualTable.getValue(0, "call_id"), is("callId"));
		assertThat((String) actualTable.getValue(0, "user_id"), is("userId"));
		assertFalse((boolean) actualTable.getValue(0, "deleted"));
	}

	@Test
	public void コメント情報取得() {
		String callId = "callId";
		CallsResult actual = sut.getComments(callId, null, principal);

		assertThat(actual.id, is("callId"));
		assertThat(actual.dateFrom, is("2014/05/05 00:00"));
		assertThat(actual.dateTo, is("00:00"));
		assertThat(actual.name, is("callName"));
		assertThat(actual.firstName, is("firstName"));
		assertThat(actual.lastName, is("lastName"));
		assertThat(actual.detail, is("comFreeC"));
		assertThat(actual.likeCount, is(1));
		assertTrue(actual.isLike);

		assertThat(actual.comments.get(0).id, is("comId"));
		assertThat(actual.comments.get(0).name, is("comName"));
		assertThat(actual.comments.get(0).firstName, is("firstName"));
		assertThat(actual.comments.get(0).lastName, is("lastName"));
		assertThat(actual.comments.get(0).userName, is("userName"));
		assertThat(actual.comments.get(0).description, is("description"));
		assertThat(actual.comments.get(0).date, is("2014/05/05 00:00"));
	}

	@Test
	public void コメントを登録した場合は登録結果を返却() throws SQLException, Exception {
		RegisterParam param = new RegisterParam();
		param.callId = "callId";
		param.comment = "comment";

		CommentsResult actual = sut.register(param, principal);

		IDataSet actualDataSet = getConnection().createDataSet();
		ITable commentsAc = actualDataSet.getTable("comments");
		ITable commentsReadUsers = actualDataSet.getTable("comments_read_users");

		IDataSet expectedDataSet = new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_" + testMethodName
				+ "_expected.xls"));
		ITable commentsEx = expectedDataSet.getTable("comments");

		// 初期テストデータ5件のうち4件は自分以外のユーザのため未読にする
		assertThat(commentsReadUsers.getRowCount(), is(1));

		// コメントが登録されているかの確認
		assertCommentsTable(commentsAc, commentsEx);

		// 戻り値の確認
		assertNull(actual.name);
		assertThat(actual.firstName, is("firstName"));
		assertThat(actual.lastName, is("lastName"));
		assertThat(actual.userName, is("userName"));
		assertThat(actual.description, is("comment"));
	}

	@Test
	public void likeTest() throws SQLException, Exception {
		sut.like("likeId", null, principal);

		IDataSet actualDataSet = getConnection().createDataSet();
		ITable likeAc = actualDataSet.getTable("calls_like_users");

		IDataSet expectedDataSet = new XlsDataSet(new File(RESOURCE_PATH + testClassName + "_" + testMethodName
				+ "_expected.xls"));
		ITable likeEx = expectedDataSet.getTable("calls_like_users");

		assertCallsLikeUsersTable(likeAc, likeEx);
	}

	@Test
	public void unlikeTest() throws SQLException, Exception {
		sut.unlike("callId", false, principal);

		IDataSet actualDataSet = getConnection().createDataSet();
		ITable actual = actualDataSet.getTable("calls_like_users");

		assertThat(actual.getRowCount(), is(0));
	}

	public void assertCommentsTable(ITable actual, ITable expected) throws DataSetException {
		// カラムのデータをすべて取得する
		Map<String, Object> actualMap = new LinkedHashMap<String, Object>();
		for (Column column : actual.getTableMetaData().getColumns()) {
			actualMap.put(column.getColumnName(), actual.getValue(1, column.getColumnName()));
		}

		Map<String, Object> expectedMap = new LinkedHashMap<String, Object>();
		for (Column column : expected.getTableMetaData().getColumns()) {
			Object value = expected.getValue(1, column.getColumnName());
			expectedMap.put(column.getColumnName(), value);
		}

		// DB状態確認
		for (String key : actualMap.keySet()) {
			if (!("id".equals(key) || "date_entered".equals(key) || "date_modified".equals(key))) {
				assertThat(actualMap.get(key), is(expectedMap.get(key)));
			}
		}
	}

	public void assertCallsLikeUsersTable(ITable actual, ITable expected) throws DataSetException {
		Map<String, Object> actualMap = new LinkedHashMap<String, Object>();
		for (Column column : actual.getTableMetaData().getColumns()) {
			actualMap.put(column.getColumnName(), actual.getValue(1, column.getColumnName()));
		}

		Map<String, Object> expectedMap = new LinkedHashMap<String, Object>();
		for (Column column : expected.getTableMetaData().getColumns()) {
			Object value = expected.getValue(1, column.getColumnName());
			expectedMap.put(column.getColumnName(), value);
		}

		// DB状態確認
		for (String key : actualMap.keySet()) {
			if (!("id".equals(key) || "date_modified".equals(key))) {
				assertThat(actualMap.get(key), is(expectedMap.get(key)));
			}
		}
	}
}
