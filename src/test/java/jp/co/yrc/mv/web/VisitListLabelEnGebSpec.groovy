package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit

import org.openqa.selenium.JavascriptExecutor


/**
 * 訪問実績画面の多言語化(英語)のテスト
 * @author komatsu
 *
 */
class VisitListLabelEnGebSpec extends GebDBSpecification {

	JavascriptExecutor exe = (JavascriptExecutor)driver

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleEn').click()
		$('#btnLogin').click()
		waitFor{!$("#loadingView").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])

		waitFor{$("#headerMenu-report").isDisplayed()}
		$("#headerMenu-report").click()
		waitFor{$("[data-menu-id='visitList.html'] img.dashboardImg").isDisplayed()}
		$("[data-menu-id='visitList.html'] img.dashboardImg").click()
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "List of actual visit"
	}

	def "検索フォームの各文言が正しいこと"() {
		expect:
		$("#dateFrom").attr("placeholder") == "Date(From)" // 日付(From)
		$("#dateTo").attr("placeholder") == "Date(To)" // 日付(To)
		$("#heldOnly").parent().text() == "Display only actual" // 実績のみ表示
		$("#unreadOnly").parent().text() == "Display only unread" //未読のみ表示
		$("input.search").eq(1).value() == "Search" // 検索
		$("#csvDownload").value() == "CSV Download" // CSVダウンロード
	}

	def "訪問実績一覧操作ボタンの各文言が正しいこと"() {
		expect:
		$("#selectAll").value() == "Select all" // 全選択
		$("#toRead").value() == "Update to already read" // 既読へ更新
		$(".modeChange-sort").text() == "Sort" // ソート
		$(".modeChange-colReorder").text() == "Sort item" // 項目並び替え
		$(".ColVis_MasterButton").text() == "Line display setting" // 列表示切替
	}

	def "'_MENU_'件表示の文言が正しいこと"() {
		expect:
		$("select[name='DataTables_Table_0_length']").parent().text() == "Display\n10\n25\n50\n100\nrecords per page"
	}

	def "列表示切替の各文言が正しいこと"() {
		setup:
		$(".ColVis button").click()
		waitFor{$(".ColVis_collection").isDisplayed()}

		when:
		def colVis = $(".ColVis_collection li")

		then:
		colVis.eq(0).text() == "Display only record of visit" // 訪問記録のみ表示
		colVis.eq(1).text() == "Display only record of information" // 情報記録のみを表示
		colVis.eq(2).text() == "Already checked (Read)" // 既読チェック
		colVis.eq(3).text() == "Visit" // 訪問
		colVis.eq(4).text() == "Title" // 件名
		colVis.eq(5).text() == "Contents" // 内容
		colVis.eq(6).text() == "Associated" // 関連先
		colVis.eq(7).text() == "Status" // ステータス
		colVis.eq(8).text() == "Purpose of visit" // 訪問理由
		colVis.eq(9).text() == "Department" // 部門
		colVis.eq(10).text() == "Area" // 営業所
		colVis.eq(11).text() == "Sales representative" // 担当者
		colVis.eq(12).text() == "Accompany" // 社内同行者
		colVis.eq(13).text() == "Planned" // 予定数
		colVis.eq(14).text() == "Acutual" // 実績数
		colVis.eq(15).text() == "Type of information" // 情報区分
		colVis.eq(16).text() == "Munufacturer" // メーカー
		colVis.eq(17).text() == "Product type" // 品種
		colVis.eq(18).text() == "Summer / Winter / All season" // カテゴリ
		colVis.eq(19).text() == "Evaluation" // 感度
		colVis.eq(20).text() == "Attached" // 添付ファイル
		colVis.eq(21).text() == "Unread comment" // コメント未読
		colVis.eq(22).text() == "Number of check mark" // 注目マーク数
		colVis.eq(23).text() == "Display comment" // コメント表示
		colVis.eq(24).text() == "Input comment" // コメント入力
		colVis.eq(25).text() == "Display all" // 全て表示

		$(".ColVis_catcher").click()
		waitFor{!$("ColVis_collectionBackground").isDisplayed()}
	}

	def "訪問実績一覧列名の各文言が正しいこと"() {
		expect:
		getRowName("readflg") == "Visit" // 訪問
		getRowName("visit-name") == "Title" // 件名
		getRowName("customer-name") == "Customer name" // 得意先名
		getRowName("market") == "Sales channel" // 販路
		getRowName("datetime") == "Date of visit" // 訪問日時
		getRowName("description") == "Contents" // 内容
		getRowName("parent-type") == "Associated" // 関連先
		getRowName("status") == "Status" // ステータス
		getRowName("division-c") == "Purpose of visit" // 訪問理由
		getRowName("account-department") == "Department" // 部門
		getRowName("account-sales-office") == "Area" // 営業所
		getRowName("assigned-user-name") == "Sales representative" // 担当者
		getRowName("calls-users") == "Accompany" // 社内同行者
		getRowName("planned-count") == "Planned" // 予定数
		getRowName("held-count") == "Acutual" // 実績数
		getRowName("info-div") == "Type of information" // 情報区分
		getRowName("maker") == "Munufacturer" // メーカー
		getRowName("kind") == "Product type" // 品種
		getRowName("category") == "Summer / Winter / All season"
		getRowName("sensitivity") == "Evaluation" // 感度
		getRowName("file-download") == "Attached" // 添付ファイル
		getRowName("comment-read") == "Unread comment" // コメント未読
		getRowName("important-mark") == "Number of check mark" // 注目マーク数
		getRowName("show-comment") == "Display comment" // コメント表示
		getRowName("comment-entered") == "Input comment" // コメント入力
		getRowName("show-view-history") == "Browsing history" // 閲覧履歴
	}

	def "訪問実績一覧下部の各文言が正しいこと"() {
		when:
		def foot = $(".dataTables_scrollFoot tfoot")

		then:
		foot.find(".readflg input").value() == "Visit" // 訪問
		foot.find(".visit-name input").attr("placeholder") == "Title" // 件名
		foot.find(".customer-name input").attr("placeholder") == "Customer name" // 得意先名
		foot.find(".market input").value() == "Sales channel" // 販路
		foot.find(".datetime input").attr("placeholder") == "Date of visit" // 訪問日時
		foot.find(".description input").eq(0).attr("placeholder") == "Contents" // 内容
		foot.find(".description input").eq(1).value() == "Contents" // 内容
		foot.find(".parent-type input").value() == "Associated" // 関連先
		foot.find(".status input").value() == "Status" // ステータス
		foot.find(".division-c input").attr("placeholder") == "Purpose of visit" // 訪問理由
		foot.find(".account-department input").attr("placeholder") == "Department" // 部門
		foot.find(".account-sales-office input").attr("placeholder") == "Area" // 営業所
		foot.find(".assigned-user-name input").attr("placeholder") == "Sales representative" // 担当者
		foot.find(".calls-users input").attr("placeholder") == "Accompany" // 社内同行者
		foot.find(".planned-count input").attr("placeholder") == "Planned" // 予定数
		foot.find(".held-count input").attr("placeholder") == "Acutual" // 実績数
		foot.find(".info-div input").value() == "Type of information" // 情報区分
		foot.find(".maker input").value() == "Munufacturer" // メーカー
		foot.find(".kind input").value() == "Product type" // 品種
		foot.find(".category input").value() == "Summer / Winter / All season" // カテゴリ
		foot.find(".comment-read input").value() == "Unread comment" // コメント未読
		foot.find(".important-mark input").attr("placeholder") == "Number of check mark" // 注目マーク数
		foot.find(".comment-entered input").value() == "Input comment" // コメント入力
		foot.find(".show-view-history input").attr("placeholder") == "Browsing history"// 閲覧履歴
	}

	def "'0 件中 0 から 0 まで表示'の文言が正しいこと"() {
		expect:
		$("#DataTables_Table_0_info").text() == "No records available"
	}

	def "ページ移動ボタンの各文言が正しいこと"() {
		expect:
		$(".previous").text() == "Prev" // 前
		$(".next").text() == "Next" // 次
	}

	@GebDBUnit
	def "訪問実績一覧の各文言が正しいこと"() {
		setup:
		setupDatePicker()

		when:
		def readflg = $("td.readflg.center")
		def commentRead = $("td.comment-read.center")
		def commentEntered = $("td.comment-entered.center")

		then:
		readflg.eq(0).text() == "Read" // 既読
		readflg.eq(1).text() == "Unread" // 未読
		commentRead.eq(0).text() == "None" //なし
		commentRead.eq(1).text() == "Existe" // あり
		commentRead.eq(2).text() == "Not commented" // コメントなし
		commentEntered.eq(0).text() == "Done" // 済
		commentEntered.eq(1).text() == "Not yet (uncompleted)" // 未
		$("td.show-comment.center input").eq(0).value() == "Display comment" // コメント表示
	}

	@GebDBUnit
	def "'未読'を'既読'に更新した場合'既読'の文言が正しいこと"() {
		setup:
		setupDatePicker()
		// 既読に更新
		$("input.readCallId").eq(2).click()

		when:
		$("#toRead").click()

		then:
		$("tbody").find(".readflg").eq(2).text() == "Read"
	}

	@GebDBUnit
	def "'コメント無し'にコメントを入力した場合'なし'と'済'の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$("tbody").find("input[data-roll='displayComment']").eq(2).click()

		when:
		waitFor{$("#commentTable").isDisplayed()}
		$("#commentEditor").value("input")
		$("#registerComment").click()
		$("#cboxClose").click()

		then:
		def tr = $("tbody tr").eq(2)
		tr.children(".comment-read.center").text() == "None"
		tr.children(".comment-entered").text() == "Done"
	}

	@GebDBUnit
	def "'コメント一覧'の各文言が正しいこと"() {
		setup:
		setupDatePicker()
		$("tbody").find("input[data-roll='displayComment']").eq(1).click()
		waitFor{$("#commentTable").isDisplayed()}

		expect:
		$("#eigyoSosikiNm").parent().parent().children("th").text() == "Department" // 所属部署
		$("#visitUserLastName").parent().parent().children("th").text() == "Visiter" // 訪問者
		$("#likeCount").parent().children().eq(0).text() == "Number of check mark：" // 注目マーク数:
		$("#like").value() == "Stamp" // 注目マークを付ける
		$("#unlike").value() == "deselect check mark" // 注目マークを解除する
		$("#readComment").value() == "Mark to checked comment" //コメント確認済みにマーク
		$("#commentEditor").parent().parent().children("th").text() == "Input" // 入力
		$("#gmailLink").text() == "Gmail" // Gmailを開く
		$("#registerComment").value() == "Save notes" // コメント保存
		$("#cboxClose").click()
		waitFor{!$("#cboxOverlay").isDisplayed()}
	}

	@GebDBUnit
	def "'データはありません'の文言が正しいこと"() {
		setup:
		setupDatePicker()

		expect:
		$(".dataTables_empty").text() == "Nothing found - sorry"
	}

	/**
	 * 検索範囲を９月１日～３０日に設定する
	 * */
	void setupDatePicker() {
		exe.executeScript("document.getElementById('dateFrom').scrollIntoView(false);")
		waitFor{$('#dateFrom').isDisplayed()}
		$('#dateFrom').click()
		def pickerFrom =  $('#ui-datepicker-div')

		pickerFrom.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerFrom.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerFrom.find("td a").find{ it.text() == "1" }.click()

		waitFor{$('#dateTo').isDisplayed()}
		$('#dateTo').click()
		def pickerTo = $('#ui-datepicker-div')

		pickerTo.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerTo.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerTo.find("td a").find{ it.text() == "30" }.click()

		$(".search").eq(1).click()
	}

	/**
	 * テーブルの列名を取得
	 * @param name 取得したい列名のクラス
	 * @return 指定した列名
	 */
	String getRowName(String name) {
		exe.executeScript('document.getElementsByClassName("' + name + '")[0].scrollIntoView(true);')

		return $("#table_head").find("." + name).eq(0).text()
	}
}
