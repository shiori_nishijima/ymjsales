package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit;
import jp.co.yrc.mv.html.SelectItem
import jp.co.yrc.mv.web.VisitListController.DailyReportParam;
import jp.co.yrc.mv.web.DailyReportViewerController.SearchResult;
import jp.co.yrc.mv.web.VisitListController.VisitListSearchCondition

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.ImportDocument.Import;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ContextConfiguration

import mockit.MockUp
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CallsDto
import jp.co.yrc.mv.dto.CallsUsersDto
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.SelectionItems;
import jp.co.yrc.mv.html.SelectItemGroup;
import jp.co.yrc.mv.web.DailyReportViewerController.SearchCondition;


import org.junit.runner.RunWith;
import org.junit.experimental.runners.Enclosed;
import spock.lang.Ignore;



/**
 * 日報確認（管理者）、日報確認（セールス）画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class DailyReportViewerControllerSpec {

	/**
	 * 日報確認（管理者）初期表示のテスト
	 * */
	static class DailyReportViewerControllerSpec_日報確認（管理者）初期表示 extends DBSpecification {

		@Autowired
		DailyReportViewerController sut;

		void setupSpec() {
			// 現在日時を 2016/08/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/01 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.initAdmin(new SearchCondition(), model, principal);

			then:
			actual == "dailyReportViewer";
		}

		def "モードがadminに設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initAdmin(param, model, principal);

			then:
			model.get("mode") == "admin";
		}

		def "パラメータがModelに設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initAdmin(param, model, principal);

			then:
			model.get("searchCondition") == param;
		}

		def "年月日に本日日付が設定される" () {
			setup:
			def param = new SearchCondition();

			when:
			sut.initAdmin(param, model, principal);

			then:
			def condition = model.get("searchCondition");
			condition.year == 2016;
			condition.month == 201608;
			condition.date == 01;
		}
	}

	/**
	 * 日報確認（管理者）検索押下のテスト
	 * */
	static class DailyReportViewerControllerSpec_日報確認（管理者）検索押下 extends DBSpecification {

		@Autowired
		DailyReportViewerController sut;

		void setupSpec() {
			// 現在日時を 2016/08/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/01 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";

			when:
			String actual = sut.findAdmin(param, model, principal);

			then:
			actual == "dailyReportViewer";
		}

		def "モードがadminに設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("mode") == "admin";
		}

		def "パラメータがmodelに設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("searchCondition") == param;
			def condition = model.get("searchCondition");
			condition.div1 == param.div1;
			condition.div2 == param.div2;
			condition.div3 == param.div3;
			condition.tanto == param.tanto;
			condition.year == param.year;
			condition.month == param.month;
			condition.date == param.date;
		}

		@DBUnit
		def "検索結果が上限を超えた場合はlimitOverにtrueが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = -1;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "all";

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("limitOver") == true; // 61件より多い場合（limitを60に上書きしているため）
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり
		 * */
		@DBUnit
		def "セールスマンのコメントがあり、未読の場合は確認に「未」が設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "4b7b01ae-93c6-c3cc-1746-c5cd3bbee302"; // 日報コメントID
			r.completed = true; // 完了フラグが1の場合は既読チェックボックスが出現する
			r.confirm = false; // 未
			r.eigyoSosikiNm = "YTSTBangkok"; // 営業組織マスタTのdiv2名称+div3名称設定される
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり
		 * */
		@DBUnit
		def "セールスマンのコメントがあり、既読の場合は確認に「済」が設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "4b7b01ae-93c6-c3cc-1746-c5cd3bbee302";
			r.completed = true; // 完了フラグが１の場合は既読チェックボックスが出現する
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = null;
			r.bossFirstName = "太郎"; // 最新の既読ユーザ
			r.bossLastName = "YTJ全社"; // 最新の既読ユーザ
			r.confirmDate = new Date("2016/08/18");
			r.confirmDateString = "2016/08/18"; // 最後に上司が既読チェックをつけた日

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり
		 * */
		@DBUnit
		def "セールスマンのコメントがあり、完了していない場合は確認にブランクが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "4b7b01ae-93c6-c3cc-1746-c5cd3bbee302";
			r.completed = false; // 日報コメントは存在するが完了フラグが０の場合は既読チェックボックスが出現しない
			r.confirm = false; // ブランク
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントなし
		 * */
		@DBUnit
		def "日報コメントが存在しない場合は確認にブランクが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = null;
			r.completed = false; // 完了フラグが０の場合は既読チェックボックスが出現しない
			r.confirm = false; // ブランク
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = null;
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントなし、完了ボタン押下のみ）
		 * */
		@DBUnit
		def "セールスマンのコメントは存在しないが、完了している場合は既読チェックボックスが出現する"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "4b7b01ae-93c6-c3cc-1746-c5cd3bbee302";
			r.completed = true; // 完了フラグが1の場合は既読チェックボックスが出現する
			r.confirm = false; // 未
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = null; // セールスコメントは存在しない
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり
		 * */
		@DBUnit
		def "所属営業組織のdiv2名称+div3名称が存在しない場合は営業組織名称が設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj031"; // 販社太郎

			def expected = [:];

			def r = new SearchResult();
			r.id = "4ee401ce-ee64-2c44-de60-566ead71907d";
			r.completed = true;
			r.confirm = false;
			r.eigyoSosikiNm = "​​​​"; // 営業組織マスタTの営業組織名称が設定される
			r.firstName = "太郎1";
			r.lastName = "YTJ販社";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 1;
			r.userComment = "販社太郎が日報コメントを入力しました。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり（担当１件、社内同行１件）、日報コメントあり
		 * */
		@DBUnit
		def "社内同行となった実績を含む訪問件数が設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "4b7b01ae-93c6-c3cc-1746-c5cd3bbee302";
			r.completed = true;
			r.confirm = false;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/18");
			r.visitDateString = "2016/08/18";
			r.callCount = 2; // 担当得意先への訪問１件、社内同行者としての訪問１件
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり（予定のみ、実績なし）、日報コメントあり
		 * */
		@DBUnit
		def "実績のない訪問は訪問件数としてカウントされないため、ブランクが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "50e10d30-11d4-b02c-9795-ca21f32bd6c8";
			r.completed = true;
			r.confirm = false;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 0; // 予定のみの訪問は訪問件数としてカウントされない
			r.userComment = "予定のみの訪問に対して日報コメントを入力します。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり、日報コメントあり
		 * 検索結果：１件（訪問は２件存在するが、情報追加として登録された訪問は訪問件数としてカウントされない）
		 * */
		@DBUnit
		def "情報追加で登録された訪問は訪問件数として設定されない"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 22;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "168f537a-edb2-76a4-cd0e-2c14604ac917";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/22");
			r.visitDateString = "2016/08/22";
			r.callCount = 1; // 訪問１件＋情報追加１件（情報追加は訪問件数としてカウントされない）
			r.userComment = "セールス太郎が８月２２日の日報コメントを入力しました。";
			r.bossComment = "全社太郎が確認しました。\n2016/08/22　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/22");
			r.confirmDateString = "2016/08/22";
			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問なし、日報コメントあり
		 * */
		@DBUnit
		def "訪問は存在しないが、日報コメントが存在する場合は訪問件数にブランクが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 15;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "50910e3d-abb2-770c-376c-1180b2976eb4";
			r.completed = true;
			r.confirm = false;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/15");
			r.visitDateString = "2016/08/15";
			r.callCount = null; // 訪問データなし
			r.userComment = "訪問実績はありませんが、日報コメントを入力します。";
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問あり（前月以前の訪問）、日報コメントあり
		 * */
		@DBUnit
		def "前月以前の訪問に紐づく日報コメントが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201605;
			param.date = 20;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "168f537a-edb2-76a4-cd0e-2c14604ac917";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/05/20");
			r.visitDateString = "2016/05/20";
			r.callCount = 1;
			r.userComment = "5月20日の日報コメントを入力しました。";
			r.bossComment = "全社太郎が確認しました。\n2016/05/20　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/05/20");
			r.confirmDateString = "2016/05/20";

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：日→全て
		 * 検索結果：３件（訪問あり/日報コメントあり、訪問あり/日報コメントなし、訪問なし/日報コメントあり）
		 * */
		@DBUnit
		def "日付を全てで検索した場合はその月の日報が全て設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = -1;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			// 検索結果（訪問あり、日報コメントあり）
			def r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1; // 訪問あり
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			// 検索結果（訪問あり、日報コメントなし）
			r = new SearchResult();
			r.id = null;
			r.completed = false;
			r.confirm = false;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/16");
			r.visitDateString = "2016/08/16";
			r.callCount = 1; // 訪問あり
			r.userComment = null;
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			// 検索結果（訪問なし、日報コメントあり）
			r = new SearchResult();
			r.id = "50910e3d-abb2-770c-376c-1180b2976eb4";
			r.completed = true;
			r.confirm = true;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/15");
			r.visitDateString = "2016/08/15";
			r.callCount = null; // 訪問データなし
			r.userComment = "訪問実績はありませんが、日報コメントを入力します。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/15　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/15");
			r.confirmDateString = "2016/08/15";

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 3; // 3件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索対象：訪問なし、日報コメントなし
		 * */
		@DBUnit
		def "訪問と日報コメントの両方が存在しない場合は空のリストが検索結果に設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 18;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 0;
		}

		/**
		 * 検索条件：営業所→固定、担当者→YTJセールス太郎１
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、未読）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の日報コメントが設定される_担当者固定"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			def r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true; // 完了フラグが１の場合は既読チェックボックスが出現する
			r.confirm = false; // 未
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null; // 上司コメントは存在するが既読にしていないのでnull

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：営業所→固定、担当者→全て
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、既読）
		 * 検索結果：２件（セールス太郎１、セールス太郎４）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の日報コメントが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "all"; // 担当者→全て

			def expected = [:];

			def r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			r = new SearchResult();
			r.id = "67550948-8bc0-cda5-ba11-19b6b5287e01";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎4";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "セールス太郎４が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎４の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン\n\n販社太郎がセールス太郎４の日報を確認しました。\n2016/08/19　YTJ販社太郎1　Ｊ首都圏";
			r.bossFirstName = "太郎1";
			r.bossLastName = "YTJ販社"; // 全社太郎→販社太郎の順で確認
			r.confirmDate = new Date("2016/08/19 12:00:00");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 2; // 2件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：部門→固定、営業所→全て、担当者→YTJ部門太郎１
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、未読）
		 * */
		@DBUnit
		def "部門に紐づく担当者の日報コメントが設定される_担当者固定"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "all";
			param.tanto = "ytj041";

			def expected = [:];

			def r = new SearchResult();
			r.id = "68160b0c-daa0-6934-9b61-b026bc988e50";
			r.completed = true;
			r.confirm = false; // 未
			r.eigyoSosikiNm = "YTST​​"; // div3名称が存在しない場合はdiv名称のみが設定される
			r.firstName = "太郎1";
			r.lastName = "YTJ部門";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "部門太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎が部門太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null; // 上司コメントは存在するが既読にしていないのでnull

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				println it.eigyoSosikiNm
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：部門→固定、営業所→全て、担当者→全て
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、既読）
		 * 検索結果：２件（部門太郎、セールス太郎１）
		 * */
		@DBUnit
		def "部門に紐づく担当者の日報コメントが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "all"; // 営業所→全て
			param.tanto = "all"; // 担当者→全て

			def expected = [:];

			def r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			r = new SearchResult();
			r.id = "68160b0c-daa0-6934-9b61-b026bc988e50";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTST​​";
			r.firstName = "太郎1";
			r.lastName = "YTJ部門";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "部門太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎が部門太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン\n\n販社太郎が部門太郎の日報を確認しました。\n2016/08/19　YTJ販社太郎1　Ｊ首都圏";
			r.bossFirstName = "太郎1";
			r.bossLastName = "YTJ販社"; // 全社太郎→販社太郎の順で確認
			r.confirmDate = new Date("2016/08/19 12:00:00");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 2; // 2件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：販社→固定、部門→全て、営業所→全て、担当者→YTJ販社太郎１
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、未読）
		 * */
		@DBUnit
		def "販社に紐づく担当者の日報コメントが設定される_担当者固定"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "all"; // 部門→全て
			param.div3 = "all"; // 営業所→全て
			param.tanto = "ytj031";

			def expected = [:];

			def r = new SearchResult();
			r.id = "682c0a7a-2a1b-88e7-b253-b68a71c1515e";
			r.completed = true;
			r.confirm = false; // 未
			r.eigyoSosikiNm = "​​​​"; // div2名称とdiv3名称が存在しない場合は営業所名称が設定される
			r.firstName = "太郎1";
			r.lastName = "YTJ販社";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "販社太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎が販社太郎の日報を確認しました。\r\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null; // 上司コメントは存在するが既読にしていないのでnull

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 1; // 1件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}

		/**
		 * 検索条件：販社→固定、部門→全て、営業所→全て、担当者→全て
		 * 検索対象：訪問あり、日報コメントあり（セールスコメントあり、上司コメントあり、既読）
		 * 検索結果：２件（セールス太郎、販社太郎）
		 * */
		@DBUnit
		def "販社に紐づく担当者の日報コメントが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 19;
			param.div1 = "0070000001";
			param.div2 = "all"; // 部門→全て
			param.div3 = "all"; // 営業所→全て
			param.tanto = "all"; // 担当者→全て

			def expected = [:];

			def r = new SearchResult();
			r.id = "682c0a7a-2a1b-88e7-b253-b68a71c1515e";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "​​​​";
			r.firstName = "太郎1";
			r.lastName = "YTJ販社";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "販社太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎が販社太郎の日報を確認しました。\r\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19 00:00:00");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1;
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			when:
			sut.findAdmin(param, model, principal);

			then:
			model.get("result").size() == 2; // 2件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}
	}

	/**
	 * 日報確認（セールス）初期表示のテスト
	 * */
	static class DailyReportViewerControllerSpec_日報確認（セールス）初期表示 extends DBSpecification {

		@Autowired
		DailyReportViewerController sut;

		void setupSpec() {
			// 現在日時を 2016/08/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/01 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.initSales(new SearchCondition(), model, principal);

			then:
			actual == "dailyReportViewer";
		}

		def "モードがsalesに設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initSales(param, model, principal);

			then:
			model.get("mode") == "sales";
		}

		def "パラメータがModelに設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.initSales(param, model, principal);

			then:
			model.get("searchCondition") == param;
		}

		def "年月日に本日日付が設定される" () {
			setup:
			def param = new SearchCondition();

			when:
			sut.initSales(param, model, principal);

			then:
			def condition = model.get("searchCondition");
			condition.year == 2016;
			condition.month == 201608;
			condition.date == 01;
		}
	}

	/**
	 * 日報確認（セールス）検索押下のテスト
	 * */
	static class DailyReportViewerControllerSpec_日報確認（セールス）検索押下 extends DBSpecification {

		@Autowired
		DailyReportViewerController sut;

		void setupSpec() {
			// 現在日時を 2016/08/01 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/01 00:00:00"))
							return cal
						}
					}
		}

		@Override
		void setup() {
			super;

			// 全社権限のユーザでログイン
			setAllUser();
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YTST");
			def corpCodes = ["YTST"];
			userInfoDto.setCorpCodes(corpCodes); // 所属営業組織
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "all";

			when:
			String actual = sut.findSales(param, model, principal);

			then:
			actual == "dailyReportViewer";
		}

		def "モードがsalesに設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "all";

			when:
			sut.findSales(param, model, principal);

			then:
			model.get("mode") == "sales";
		}

		def "パラメータがmodelに設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = 17;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			when:
			sut.findSales(param, model, principal);

			then:
			model.get("searchCondition") == param;
			def condition = model.get("searchCondition");
			condition.div1 == param.div1;
			condition.div2 == param.div2;
			condition.div3 == param.div3;
			condition.tanto == param.tanto;
			condition.year == param.year;
			condition.month == param.month;
			condition.date == param.date;
		}

		/**
		 * 検索条件：日→全て
		 * 検索結果：３件（訪問あり/日報コメントあり、訪問あり/日報コメントなし、訪問なし/日報コメントあり）
		 * */
		@DBUnit
		def "日付を全てで検索した場合はその月の日報が全て設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 201608;
			param.date = -1;
			param.div1 = "0070000001";
			param.div2 = "00700000017000";
			param.div3 = "00700000017000700000";
			param.tanto = "ytj061";

			def expected = [:];

			// 検索結果（訪問あり、日報コメントあり）
			def r = new SearchResult();
			r.id = "668f0244-7819-259a-0d61-220e716e002a";
			r.completed = true;
			r.confirm = true; // 済
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/19");
			r.visitDateString = "2016/08/19";
			r.callCount = 1; // 訪問あり
			r.userComment = "セールス太郎が日報コメントを入力しました。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/19　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/19");
			r.confirmDateString = "2016/08/19";

			expected[r.id] = r;

			// 検索結果（訪問あり、日報コメントなし）
			r = new SearchResult();
			r.id = null;
			r.completed = false;
			r.confirm = false;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/16");
			r.visitDateString = "2016/08/16";
			r.callCount = 1; // 訪問あり
			r.userComment = null;
			r.bossComment = null;
			r.bossFirstName = "";
			r.bossLastName = "";
			r.confirmDate = null;
			r.confirmDateString = null;

			expected[r.id] = r;

			// 検索結果（訪問なし、日報コメントあり）
			r = new SearchResult();
			r.id = "50910e3d-abb2-770c-376c-1180b2976eb4";
			r.completed = true;
			r.confirm = true;
			r.eigyoSosikiNm = "YTSTBangkok";
			r.firstName = "太郎1";
			r.lastName = "YTJセールス";
			r.visitDate = new Date("2016/08/15");
			r.visitDateString = "2016/08/15";
			r.callCount = null; // 訪問データなし
			r.userComment = "訪問実績はありませんが、日報コメントを入力します。";
			r.bossComment = "全社太郎がセールス太郎の日報を確認しました。\n2016/08/15　YTJ全社太郎　ヨコハマタイヤジャパン";
			r.bossFirstName = "太郎";
			r.bossLastName = "YTJ全社";
			r.confirmDate = new Date("2016/08/15");
			r.confirmDateString = "2016/08/15";

			expected[r.id] = r;

			when:
			sut.findSales(param, model, principal);

			then:
			model.get("result").size() == 3; // 3件取得
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.completed == e.completed;
				assert it.confirm == e.confirm;
				assert it.eigyoSosikiNm == e.eigyoSosikiNm;
				assert it.firstName == e.firstName;
				assert it.lastName == e.lastName;
				assert it.visitDate == e.visitDate;
				assert it.visitDateString == e.visitDateString;
				assert it.callCount == e.callCount;
				assert it.userComment == e.userComment;
				assert it.bossComment == e.bossComment;
				assert it.bossFirstName == e.bossFirstName;
				assert it.bossLastName == e.bossLastName;
				assert it.confirmDate == e.confirmDate;
				assert it.confirmDateString == e.confirmDateString;
			}
		}
	}
}
