package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CallsUsersWithGroupInfoDto
import jp.co.yrc.mv.helper.SelectYearsHelper
import jp.co.yrc.mv.web.CallsUsersCountController.SearchCondition
import mockit.MockUp

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.support.BindingAwareModelMap

class CallsUsersCountControllerSpec extends DBSpecification {

	@Autowired
	CallsUsersCountController sut;
	@Autowired
	SelectYearsHelper selectYearsHelper;

	def "初期表示で正しいTemplate名を返す"(){
		setup:
		def condition = new SearchCondition();

		when:
		String actual = sut.init(condition, model, principal)

		then:
		actual == "callsUsersCount"
	}

	def "初期表示でパラメーターがModelに設定される"() {
		setup:
		def condition = new SearchCondition();

		when:
		sut.init(condition, model, principal)

		then:
		model.get("searchCondition") == condition
	}

	def "初期表示でモデルに年月が設定される"() {
		setup:
		def condition = new SearchCondition();
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)

		when:
		sut.init(condition, model, principal);

		then:
		model.get("years") != null;
		model.get("years") == expectedModel.get("years")
		model.get("months") != null;
		model.get("months") == expectedModel.get("months")
	}

	def "初期表示でモデルに空の結果リストが設定される"() {
		setup:
		def condition = new SearchCondition();

		when:
		sut.init(condition, model, principal);

		then:
		model.get("list") != null;
		model.get("list").isEmpty()  == true
	}

	def "初期表示で現在年月が設定される"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		when:
		sut.init(condition, model, principal);

		then:
		condition.getYear() == 2000
		condition.getMonth() == 1
	}


	@DBUnit
	def "検索時に正しいTemplate名を返す"(){
		setup:
		def condition = new SearchCondition();
		condition.year = 2000
		condition.month = 1

		when:
		String actual = sut.find(condition, model, principal)

		then:
		actual == "callsUsersCount"
	}

	@DBUnit
	def "検索時にパラメーターがModelに設定される"() {
		setup:
		def condition = new SearchCondition();
		condition.year = 2000
		condition.month = 1

		when:
		sut.find(condition, model, principal)

		then:
		model.get("searchCondition") == condition
	}

	@DBUnit
	def "検索時に結果なしでモデルに空の結果リストが設定される"() {
		setup:
		def condition = new SearchCondition();
		condition.year = 2000
		condition.month = 1

		when:
		sut.find(condition, model, principal);

		then:
		model.get("list") != null;
		model.get("list").isEmpty()  == true
	}

	@DBUnit
	def "検索時に現在年月が設定される"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}
		condition.year = 2000
		condition.month = 1

		when:
		sut.find(condition, model, principal);

		then:
		condition.getYear() == 2000
		condition.getMonth() == 1
	}



	@DBUnit
	def "条件現在月とdiv1指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ部門';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = null;
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎';
		o.lastName = '神奈川';
		o.div2Nm = '神奈川カンパニー';
		o.div3Nm = '神奈川本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "条件現在月とdiv1div2指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ部門';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = null;
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "条件現在月とdiv1div2div3指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		condition.div3 = "111"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}

	@DBUnit
	def "条件現在月とdiv1div2div3担当者指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		condition.div3 = "111"
		condition.tanto = "ytj051"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}



	@DBUnit
	def "条件過去月とdiv1指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/02/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ部門';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = null;
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎';
		o.lastName = '神奈川';
		o.div2Nm = '神奈川カンパニー';
		o.div3Nm = '神奈川本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "条件過去月とdiv1div2指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/02/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ部門';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = null;
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "条件過去月とdiv1div2div3指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/2/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		condition.div3 = "111"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "条件過去月とdiv1div2div3担当者指定で検索する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/2/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		condition.div2 = "011"
		condition.div3 = "111"
		condition.tanto = "ytj051"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "複数所属のユーザーは同じ集計結果でそれぞれの所属毎に表示する"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/1/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎7';
		o.lastName = 'YTJセールス';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎7';
		o.lastName = 'YTJセールス';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '品川営業所';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "グループ毎に集計される"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/1/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎2';
		o.lastName = 'YTJセールス';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 13 + 91 + 13;
		o.groupImportant = 1 + 4 + 1;
		o.groupNew = 1 + 5 + 1;
		o.groupDeepPlowing = 1 + 6 + 1;
		o.groupYCp = 1 + 7 + 1;
		o.groupOtherCp = 1 + 8 + 1;
		o.groupOne = 1 + 9 + 1;
		o.groupTwo = 1 + 10 + 1;
		o.groupThree = 1 + 11 + 1;
		o.groupFour = 1 + 12 + 1
		o.groupFive = 1 + 13 + 1;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}


	@DBUnit
	def "予定は集計対象外になる"() {
		setup:
		def condition = new SearchCondition();
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/1/01 00:00:00"))
						return cal
					}
				}

		def expected = new ArrayList();
		def o;

		o = new CallsUsersWithGroupInfoDto();
		o.firstName = '太郎1';
		o.lastName = 'YTJ営,業所';
		o.div2Nm = '東京カンパニー';
		o.div3Nm = '東京本社';
		o.callCount = 1;
		o.groupImportant = 0;
		o.groupNew = 0;
		o.groupDeepPlowing = 0;
		o.groupYCp = 0;
		o.groupOtherCp = 0;
		o.groupOne = 0;
		o.groupTwo = 0;
		o.groupThree = 0;
		o.groupFour = 0;
		o.groupFive = 0;
		expected << o

		condition.year = 2000
		condition.month = 1
		condition.div1 = "001"
		when:
		sut.find(condition, model, principal);

		then:
		expected.size == model.get("list").size
		expected.eachWithIndex { it, i ->
			def e = model.get("list").get(i)
			assert it.firstName == e.firstName
			assert it.lastName == e.lastName
			assert it.div2Nm == e.div2Nm
			assert it.div3Nm == e.div3Nm
			assert it.callCount == e.callCount
			assert it.groupImportant == e.groupImportant
			assert it.groupNew == e.groupNew
			assert it.groupDeepPlowing == e.groupDeepPlowing
			assert it.groupYCp == e.groupYCp
			assert it.groupOtherCp == e.groupOtherCp
			assert it.groupOne == e.groupOne
			assert it.groupTwo == e.groupTwo
			assert it.groupThree == e.groupThree
			assert it.groupFour == e.groupFour
			assert it.groupFive == e.groupFive
		}
	}
}
