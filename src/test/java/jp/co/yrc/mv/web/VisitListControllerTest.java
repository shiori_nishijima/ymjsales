package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import jp.co.yrc.mv.common.DataSourceBasedDBTestCaseBase;
import jp.co.yrc.mv.common.TestUtils;
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.html.SelectItem;
import jp.co.yrc.mv.html.SelectItemGroup;
import jp.co.yrc.mv.web.VisitListController.SearchResult;
import jp.co.yrc.mv.web.VisitListController.VisitListSearchCondition;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
@TransactionConfiguration
@Transactional
@WebAppConfiguration
public class VisitListControllerTest extends DataSourceBasedDBTestCaseBase {

	@Autowired
	VisitListController sut;

	Principal principal;


	@Before
	public void setUp() throws Exception {
		super.setUp();
		UserInfoDto userInfoDto = new UserInfoDto();
		userInfoDto.setId("userId");
		userInfoDto.setLastName("lastName");
		userInfoDto.setFirstName("firstName");
		Authentication authentication = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		principal = authentication;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void 訪問リストを表示() {
		Model model = mock(Model.class);

		Authentication principal = mock(Authentication.class);
		UserInfoDto o = new UserInfoDto();
		VisitListSearchCondition condition = new VisitListSearchCondition();
		List<String> corpCodes = new ArrayList<String>();
		corpCodes.add("x");
		o.setCorpCodes(corpCodes);
		when(principal.getPrincipal()).thenReturn(o);

		String actual = sut.initVisitList(condition, model, principal);
		String expected = "visitList";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(model, times(4)).addAttribute(argString.capture(), argObj.capture());
		List<Object> list = argObj.getAllValues();

		assertSelectItem((ArrayList<SelectItem>) list.get(0), "div1", "1");
		assertSelectItemGroup((ArrayList<SelectItemGroup>) list.get(1), "div2", "2");
		assertSelectItemGroup((ArrayList<SelectItemGroup>) list.get(2), "div3", "3");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void 訪問リストを検索() {
		Model model = mock(Model.class);

		VisitListSearchCondition param = new VisitListSearchCondition();
		Date date = TestUtils.convertStringToDate("2014/5/5", "yyyy/MM/dd");
		param.setDateFrom(date);
		param.setDateTo(date);
		param.setDiv1("1");
		param.setDiv2("2");
		param.setDiv3("3");
		param.setTanto("userId");

		String actual = sut.findVisitList(param, model, principal);
		String expected = "visitList";
		assertThat(actual, is(expected));

		ArgumentCaptor<String> argString = ArgumentCaptor.forClass(String.class);
		ArgumentCaptor<Object> argObj = ArgumentCaptor.forClass(Object.class);

		verify(model, times(5)).addAttribute(argString.capture(), argObj.capture());
		List<Object> list = argObj.getAllValues();

		// Modelに格納した値を確認
		ArrayList<SearchResult> result = (ArrayList<SearchResult>) list.get(0);
		assertSearchResult(result.get(0), "callIdA", "callNameA", "accountNameA");
		assertSearchResult(result.get(1), "callIdO", "callNameO", "accountNameO");

		assertSelectItem((ArrayList<SelectItem>) list.get(1), "div1", "1");
		assertSelectItemGroup((ArrayList<SelectItemGroup>) list.get(2), "div2", "2");
		assertSelectItemGroup((ArrayList<SelectItemGroup>) list.get(3), "div3", "3");
		assertSelectItemGroup((ArrayList<SelectItemGroup>) list.get(4), "lastName firstName", "userId");
	}

	public void assertSearchResult(SearchResult sr, String callId, String callName, String accountName) {
		assertThat(sr.getId(), is(callId));
		assertThat(sr.getName(), is(callName));
		assertThat(sr.getAccountsName(), is(accountName));
		assertThat(sr.getVisitDateStart(), is("2014/05/05 00:00"));
		assertThat(sr.getVisitDateEnd(), is("00:00"));
		assertThat(sr.getDescription(), is("comFreeC"));
		assertTrue(sr.isRead());
//		assertTrue(sr.isExistCommentUnRead());
		assertThat(sr.getLike(), is(1));
	}

	public void assertSelectItem(List<SelectItem> selectItems, String expectedLabel, String expectedValue) {
		assertThat(selectItems.get(0).getLabel(), is(""));
		assertThat(selectItems.get(0).getValue(), is(""));

		assertThat(selectItems.get(1).getLabel(), is(expectedLabel));
		assertThat(selectItems.get(1).getValue(), is(expectedValue));
	}

	public void assertSelectItemGroup(List<SelectItemGroup> selectItems, String expectedLabel, String expectedValue) {
		assertSelectItem(selectItems.get(0).getItems(), expectedLabel, expectedValue);
	}
}
