package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification

/**
 * ダッシュボード画面の多言語化対応(英語)テスト
 * @author komatsu
 *
 */
class DashbordLabelEnGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleEn').click()
		$('#btnLogin').click()
		waitFor{!$('#loadingView').isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "Report menu"
	}

	def "各画面名の文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()

		expect:
		$("[data-menu-id='visitList.html']").text() == "List of actual visit"
		$("[data-menu-id='dailyReportViewerSales.html']").text() == "Daily report confirmation (Sales representative)"
		$("[data-menu-id='dailyReportViewerAdmin.html']").text() == "Daily report confirmation (Manager)"
	}

	def "ヘッダーメニューの各文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()

		expect:
		$('#headerMenu-report').text() == "Report menu"
		$('#headerMenu-favorite').text() == "Favorite"
		$("#setupFavorite").text() == "Add to favorite"
	}

	def "'お気に入り解除'の文言が正しいこと"() {
		setup:
		$("#headerMenu-favorite").click()

		// お気に入りが登録されていなかったら登録する。
		if ($('#nothin-favorite').css('display') == 'block') {
			$("#headerMenu-report").click()
			$("#setupFavorite").click()
			$("[data-menu-id='visitList.html']").find("div.click-area").click()
			$("#setupFavorite").click()
		}
		$("#headerMenu-favorite").click()

		expect:
		$("#setupFavorite").text() == "Release from favorite"
	}

	def "'保存'の文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()
		def save = $("#setupFavorite")

		when:
		save.click()

		then:
		save.text() == "Save"
		save.click()
	}

	def "'お気に入りが登録されていません'の文言が正しいこと"() {
		setup:
		$("#headerMenu-favorite").click()
		$("#setupFavorite").click()
		def list = $("[data-favorite-prev='true']").find(".click-area")
		list.each{favorite->
			$(favorite).click()
		}
		$("#setupFavorite").click()

		expect:
		$("#nothing-favorite-img").text() == "Favorites are not registered."
	}
}
