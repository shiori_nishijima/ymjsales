package jp.co.yrc.mv.web

import geb.Page
import jp.co.yrc.mv.common.DateUtils;
import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit;;

/**
 * 週報画面のテスト
 * @author SHIORIN
 *
 */
class WeeklyReportGebSpec extends GebDBSpecification {

	void setupSpec() {
		to TopPage
		$('#userId').value('ytj061')
		$('#password').value('passw0rd')
		$('#login_0').click()
		$('#headerMenu-report').click()
		$('[data-menu-id="weeklyReport.html"]').find('img.dashboardImg').click()
	}

	@GebDBUnit
	def "現在日付に紐づく年月週が初期表示されていること"() {
		setup:
		if ($('#year').value() == 2016) {
			reload()
		}
		def now = DateUtils.getCalendar();
		String exYear = now.get(Calendar.YEAR)
		Integer exMonth = now.get(Calendar.MONTH) + 1
		String exWeek = now.get(Calendar.WEEK_OF_MONTH)

		expect:
		assert $('#year').value() == exYear
		assert DateUtils.toMonth(Integer.parseInt($('#month').value())) == exMonth
		assert $('#week').value() == exWeek
	}

	@GebDBUnit
	def "前週の行動のデータが正常に表示されていること"() {
		setup:
		setupForm(4)
		$("#searchButton").click()

		when:
		// 前週の行動テーブル
		def table = $('.week-calls').eq(0)
		def accountsList = table.find('.accounts-list')
		def planNum = table.find('.plan-number')
		def resultNum = table.find('.result-number')
		def planTotal = table.find('.plan-total-number')
		def resultTotal = table.find('.result-total-number')

		then:
		// 得意先
		accountsList.eq(0).text() == "得意先1\n得意先3"
		accountsList.eq(1).text() == "得意先4"
		accountsList.eq(2).text() == "得意先4"
		accountsList.eq(3).text() == "得意先1"
		accountsList.eq(4).text() == "得意先1"
		accountsList.eq(5).text() == "得意先1"
		// 訪問計画
		planNum.eq(0).text() == "6"
		planNum.eq(1).text() == "3"
		planNum.eq(2).text() == "5"
		planNum.eq(3).text() == "7"
		planNum.eq(4).text() == "9"
		planNum.eq(5).text() == "5"
		// 訪問実績
		resultNum.eq(0).text() == "8"
		resultNum.eq(1).text() == "4"
		resultNum.eq(2).text() == "6"
		resultNum.eq(3).text() == "8"
		resultNum.eq(4).text() == "5"
		resultNum.eq(5).text() == "15"
		// 訪問計画合計
		planTotal.text() == "35"
		// 訪問計画合計
		resultTotal.text() == "46"
	}

	@GebDBUnit
	def "今週の行動予定のデータが正常に表示されていること"() {
		setup:
		setupForm(4)
		$("#searchButton").click()

		when:
		// 前週の行動テーブル
		def table = $('.week-calls').eq(1)
		def accountsList = table.find('.accounts-list')
		def planNum = table.find('.plan-number')
		def planTotal = table.find('.plan-total-number')

		then:
		// 得意先
		accountsList.eq(0).text() == "得意先1\n得意先3"
		accountsList.eq(1).text() == "得意先4"
		accountsList.eq(2).text() == "得意先4"
		accountsList.eq(3).text() == "得意先1"
		accountsList.eq(4).text() == "得意先1"
		accountsList.eq(5).text() == "得意先1"
		// 訪問計画
		planNum.eq(0).text() == "2"
		planNum.eq(1).text() == "6"
		planNum.eq(2).text() == "10"
		planNum.eq(3).text() == "7"
		planNum.eq(4).text() == "9"
		planNum.eq(5).text() == "10"
		// 訪問計画合計
		planTotal.text() == "44"
	}

	@GebDBUnit
	def "報告のデータが正常に表示できていること"() {
		setup:
		setupForm(5)
		$("#searchButton").click()

		when:
		def table = $('.report tbody')
		def reportList = table.find('tr')

		then:
		// Bad News
		assert reportList.eq(1).find('td').eq(0).text() == "1"
		assert reportList.eq(1).find('td').eq(1).text() == "得意先、感度ネガティブ、情報区分１"
		assert reportList.eq(2).find('td').eq(0).text() == "1"
		assert reportList.eq(2).find('td').eq(1).text() == "案件、感度ネガティブ、情報区分１"
		// Good News
		assert reportList.eq(4).find('td').eq(0).text() == "3"
		assert reportList.eq(4).find('td').eq(1).text() == "得意先、感度ポジティブ、情報区分３"
		assert reportList.eq(5).find('td').eq(0).text() == "4"
		assert reportList.eq(5).find('td').eq(1).text() == "案件、感度ポジティブ、情報区分４"
		// その他
		assert reportList.eq(7).find('td').eq(0).text() == "2"
		assert reportList.eq(7).find('td').eq(1).text() == "得意先、感度その他、情報区分２"
		assert reportList.eq(8).find('td').eq(0).text() == "5"
		assert reportList.eq(8).find('td').eq(1).text() == "案件、感度その他、情報区分５"
	}

	@GebDBUnit
	def "報告のコメント未読、入力が正常に表示できていること"() {
		setup:
		setupForm(3)
		$("#searchButton").click()

		when:
		def table = $('.report tbody')
		def reportList = table.find('tr')

		then:
		// コメント未読
		assert reportList.eq(3).find('td').eq(2).text() == "コメント無し"
		assert reportList.eq(1).find('td').eq(2).text() == "あり"
		assert reportList.eq(5).find('td').eq(2).text() == "なし"
		// コメント入力
		assert reportList.eq(1).find('td').eq(4).text() == "未"
		assert reportList.eq(5).find('td').eq(4).text() == "済"
	}

	/**
	 * 画面を再読込
	 * */
	void reload() {
		driver.navigate().refresh()
		waitFor{$("#searchButton").isDisplayed()}
	}

	/**
	 * 検索日付を2016年9月に設定
	 * @param week 何週目か
	 */
	void setupForm(week) {
		$("#year").click()
		$("#year").find("option[value='2016']").click()

		$("#month").click()
		$("#month").find("option[value='201609']").click()

		def selector = "option[value='" + week + "']"
		$("#week").click()
		$("#week").find(selector).click()

		$("#tanto").click()
		$("#tanto").find("option[value='ytj061']").click()
	}
}
