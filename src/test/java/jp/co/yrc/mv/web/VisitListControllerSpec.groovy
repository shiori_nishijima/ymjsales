package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.Constants;
import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit;
import jp.co.yrc.mv.html.SelectItem
import jp.co.yrc.mv.web.VisitListController.SearchResult;
import jp.co.yrc.mv.web.VisitListController.VisitListSearchCondition

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.ImportDocument.Import
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.test.context.ContextConfiguration

import mockit.MockUp
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CallsDto
import jp.co.yrc.mv.dto.CallsUsersDto
import jp.co.yrc.mv.dto.UserInfoDto;
import jp.co.yrc.mv.entity.MstEigyoSosiki;
import jp.co.yrc.mv.entity.SelectionItems;

import org.junit.runner.RunWith;
import org.junit.experimental.runners.Enclosed;
import spock.lang.Ignore;


/**
 * 訪問実績画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class VisitListControllerSpec {

	/**
	 * 訪問実績画面の初期表示時のテスト
	 * 対象メソッド：initVisitList
	 * */
	static class VisitListControllerSpec_訪問実績初期表示 extends DBSpecification {
		@Autowired
		VisitListController sut;

		void setupSpec() {
			// 現在日時を 2016/07/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/07/09 00:00:00"))
							return cal
						}
					}
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.initVisitList(new VisitListSearchCondition(), model, principal);

			then:
			actual == "visitList";
		}

		def "パラメータがModelに設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			when:
			sut.initVisitList(param, model, principal);

			then:
			model.get("visitListSearchCondition") == param;
		}

		def "モードがvisitListに設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			when:
			sut.initVisitList(param, model, principal);

			then:
			model.get("mode") == "visitList";
		}

		def "販路にすべての販路が設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			def List<SelectItem> expected = new ArrayList<>();

			def s = new SelectItem("販路", "all");
			expected << s;
			s = new SelectItem("トラック", "Z2511C");
			expected << s;
			s = new SelectItem("バス", "Z2511E");
			expected << s;
			s = new SelectItem("直販その他", "Z2511F");
			expected << s;
			s = new SelectItem("ハイタク", "Z2511G");
			expected << s;
			s = new SelectItem("ＳＳ", "Z2511H");
			expected << s;
			s = new SelectItem("ＰＳ", "Z2511J");
			expected << s;
			s = new SelectItem("ＲＳ", "Z2511L");
			expected << s;
			s = new SelectItem("ＣＤ", "Z2511N");
			expected << s;
			s = new SelectItem("ＣＳ", "Z2511P");
			expected << s;
			s = new SelectItem("間販その他", "Z2511R");
			expected << s;
			s = new SelectItem("ＨＣ", "Z2511S");
			expected << s;
			s = new SelectItem("リース", "Z2511X");
			expected << s;

			when:
			sut.initVisitList(param, model, principal);

			then:
			model.get("markets").eachWithIndex { it, i ->
				def e = expected.get(i)
				assert it.label == e.label;
				assert it.value == e.value;
			}
		}

		def "実績のみ、情報ありのみにtrueに設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			when:
			sut.initVisitList(param, model, principal);

			then:
			def c = model.get("visitListSearchCondition");
			c.heldOnly == true;
			c.hasInfoOnly == true;
		}

		def "日付FromToの最小最大範囲が設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			when:
			sut.initVisitList(param, model, principal);

			then:
			def c = model.get("visitListSearchCondition");
			c.maxDate.format("yyyy/MM/dd") == "2017/12/31"
			c.minDate.format("yyyy/MM/dd") == "2014/01/01";
		}

		def "現在年月が設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			when:
			sut.initVisitList(param, model, principal);

			then:
			def c = model.get("visitListSearchCondition");
			c.dateFrom.format("yyyy/MM") == "2016/07"
		}
	}

	/**
	 * 訪問実績画面の検索ボタン押下時のテスト
	 * 対象メソッド：findVisitList
	 * */
	static class VisitListControllerSpec_訪問実績検索押下 extends DBSpecification {
		@Autowired
		VisitListController sut;

		void setupSpec() {
			// 現在日時を 2016/07/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/07/09 00:00:00"))
							return cal
						}
					}
		}

		/** 営業ユーザ */
		void setSalesUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj061");
			userInfoDto.setLastName("YTJセールス");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("06"); // 営業権限
			userInfoDto.setCompany("YMJ");
			def list = ["A040AZ", "Y00300"];
			userInfoDto.setCorpCodes(list);
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		/** 営業所ユーザ */
		void setSalesOfficeUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj051");
			userInfoDto.setLastName("YTJ営業所");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("05"); // 営業所権限
			userInfoDto.setCompany("YMJ");
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		/** 部門ユーザ */
		void setDepartmentUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj041");
			userInfoDto.setLastName("YTJ部門");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("04"); // 全社権限
			userInfoDto.setCompany("YMJ");
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		/** 販社ユーザ */
		void setCompanyUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj031");
			userInfoDto.setLastName("YTJ販社");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("03"); // 販社権限
			userInfoDto.setCompany("YMJ");
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		/** 全社ユーザ */
		void setAllUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj01");
			userInfoDto.setLastName("YTJ全社");
			userInfoDto.setFirstName("太郎");
			userInfoDto.setAuthorization("01"); // 全社権限
			userInfoDto.setCompany("YMJ");
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			// 検索条件が初期表示時の場合
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;
			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			when:
			String actual = sut.findVisitList(param, model, principal);

			then:
			actual == "visitList";
		}

		def "パラメータがmodelに設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = new Date("2016/08/31");
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = true;
			param.tantoOnly = true;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;
			param.accountId = "2000000001";
			param.mode = "visitListView";

			when:
			sut.findVisitList(param, model, principal);

			then:
			def condition = model.get("visitListSearchCondition");
			condition.dateFrom == param.dateFrom;
			condition.dateTo == param.dateTo;
			condition.market == param.market;
			condition.heldOnly == param.heldOnly;
			condition.hasInfoOnly == param.hasInfoOnly;
			condition.unreadOnly == param.unreadOnly;
			condition.div1 == param.div1;
			condition.div2 == param.div2;
			condition.div3 == param.div3;
			condition.tanto == param.tanto;
			condition.callsInfoDiv == param.callsInfoDiv;
			condition.accountId == param.accountId;
			condition.mode == param.mode;
			condition.tantoOnly == param.tantoOnly;
		}

		def "モードがvisitListに設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("mode") == "visitList";
		}

		def "販路にすべての販路が設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def List<SelectItem> expected = new ArrayList<>();

			def s = new SelectItem("販路", "all");
			expected << s;
			s = new SelectItem("トラック", "Z2511C");
			expected << s;
			s = new SelectItem("バス", "Z2511E");
			expected << s;
			s = new SelectItem("直販その他", "Z2511F");
			expected << s;
			s = new SelectItem("ハイタク", "Z2511G");
			expected << s;
			s = new SelectItem("ＳＳ", "Z2511H");
			expected << s;
			s = new SelectItem("ＰＳ", "Z2511J");
			expected << s;
			s = new SelectItem("ＲＳ", "Z2511L");
			expected << s;
			s = new SelectItem("ＣＤ", "Z2511N");
			expected << s;
			s = new SelectItem("ＣＳ", "Z2511P");
			expected << s;
			s = new SelectItem("間販その他", "Z2511R");
			expected << s;
			s = new SelectItem("ＨＣ", "Z2511S");
			expected << s;
			s = new SelectItem("リース", "Z2511X");
			expected << s;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("markets").eachWithIndex { it, i ->
				def e = expected.get(i)
				assert it.label == e.label;
				assert it.value == e.value;
			}
		}

		def "日付FromToの最小最大範囲が設定される"() {
			setup:
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			when:
			sut.findVisitList(param, model, principal);

			then:
			def c = model.get("visitListSearchCondition");
			c.maxDate.format("yyyy/MM/dd") == "2017/12/31"
			c.minDate.format("yyyy/MM/dd") == "2014/01/01";
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：取得件数が1001件より多い（limit=mv.properties参照）
		 * */
		@DBUnit
		def "検索結果が上限を超えた場合はlimitOverにtrueが設定される"() {
			setup:
			setSalesUser();
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = false; // 実績のみ×
			param.unreadOnly = false;
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("limitOver") == true;
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→セールス太郎１
		 * 検索結果：３件（担当得意先への担当訪問、担当外得意先への担当訪問、社内同行した訪問）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合_担当者固定"() {
			setup:
			setSalesUser();

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：担当した訪問）
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = [];

			expected[r.id] = r;

			// 取得結果のexpected（２件目：同じ営業組織内の社内同行した訪問）
			r = new SearchResult();
			r.id = "2962010b-d148-d3b5-0386-2431c62050b4";
			r.callId = null;
			r.name = "担当外得意先_セールス太郎1が同行";
			r.accountsName = "得意先_別の担当者";
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎7";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = ['YTJセールス太郎1[ytj061]']; // 社内同行者

			expected[r.id] = r;

			// 取得結果のexpected（３件目：同じ営業組織内の担当外得意先への訪問）
			r = new SearchResult();
			r.id = "2962010b-d148-d3b5-0386-2431c62050b5";
			r.callId = null;
			r.name = "担当外得意先_セールス太郎1が担当";
			r.accountsName = "得意先_別の担当者";
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = []; // 社内同行者

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size() == 3;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
				assert it.callsUsers == e.callsUsers; // 社内同行者のアサート
			}
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→全て
		 * 検索結果：２件（セールス太郎１，営業所太郎）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合"() {
			setup:
			setAllUser();

			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all"; // 担当者→全て
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：セールス太郎）
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：営業所太郎）
			r = new SearchResult();
			r.id = "28df0579-e0aa-3d41-404b-9000be73419c";
			r.callId = null;
			r.name = "営業所太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/15 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ営業所";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size() == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、部門→全て、担当者→YTJ部門太郎１
		 * 検索結果：２件（担当者した訪問、社内同行した訪問）
		 * */
		@DBUnit
		def "部門に紐づく担当者の訪問実績を取得する場合_担当者固定"() {
			setup:
			setDepartmentUser();
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJ部門 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "all";
			param.tanto = "ytj041";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：担当した訪問）
			def r = new SearchResult();
			r.id = "4e190ebe-72d9-7920-ab61-6935ae93e506";
			r.callId = null;
			r.name = "部門太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ部門";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = [];

			expected[r.id] = r;

			// 取得結果のexpected（２件目：社内同行した訪問）
			r = new SearchResult();
			r.id = "d1160411-a996-6d34-8aaa-1022440eba67";
			r.callId = null;
			r.name = "セールス太郎の実績_部門太郎が同行";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/22 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = ['YTJ部門太郎1[ytj041]']; // 社内同行者

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
				assert it.callsUsers == e.callsUsers; // 社内同行者のアサート
			}
		}

		/**
		 * 検索条件：初期表示時の設定、部門→東京Co、営業所→全て、担当者→全て
		 * 検索結果：３件（部門太郎（東京Co）、セールス太郎（東京Co/東京本社）、セールス太郎２（東京Co/東京官庁））
		 * */
		@DBUnit
		def "部門に紐づく担当者の訪問実績を取得する場合"() {
			setup:
			setDepartmentUser(); // ユーザはYTJ部門 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "all";
			param.tanto = "all"; // 担当者→全て
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：部門太郎）
			def r = new SearchResult();
			r.id = "4e190ebe-72d9-7920-ab61-6935ae93e506";
			r.callId = null;
			r.name = "部門太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ部門";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：セールス太郎２）
			r = new SearchResult();
			r.id = "70b7031e-8bb4-ca00-99b4-24b03fc0553d";
			r.callId = null;
			r.name = "セールス太郎２の実績";
			r.accountsName = "得意先2";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎2";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京官庁";
			expected[r.id] = r;

			// 取得結果のexpected（３件目：セールス太郎１）
			r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 3;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、販社→Ｊ首都圏、担当者→YTJ販社太郎１
		 * 検索結果：２件（担当者した訪問、社内同行した訪問）
		 * */
		@DBUnit
		def "販社に紐づく担当者の訪問実績を取得する場合_担当者固定"() {
			setup:
			setCompanyUser();
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJ販社 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "all";
			param.div3 = "all";
			param.tanto = "ytj031";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：担当した訪問）
			def r = new SearchResult();
			r.id = "28910127-c2d3-e268-2837-7c510b8730aa";
			r.callId = null;
			r.name = "販社太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/15 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ販社";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = [];

			expected[r.id] = r;

			// 取得結果のexpected（２件目：社内同行した訪問）
			r = new SearchResult();
			r.id = "d1160411-a996-6d34-8aaa-1022440eba67";
			r.callId = null;
			r.name = "セールス太郎の実績_部門太郎と販社太郎が同行";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/22 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = [
				'YTJ販社太郎1[ytj031]',
				'YTJ部門太郎1[ytj041]'
			]; // 社内同行者

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
				assert it.callsUsers == e.callsUsers; // 社内同行者のアサート
			}
		}

		/**
		 * 検索条件：初期表示時の設定、販社→Ｊ首都圏、部門→全て、営業所→全て、担当者→全て
		 * 検索結果：３件（販社太郎（Ｊ首都圏）、部門太郎１（東京Co）、部門太郎２（神奈川Co））
		 * */
		@DBUnit
		def "販社に紐づく担当者の訪問実績を取得する場合"() {
			setup:
			setCompanyUser();
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "all"; // 部門→全て
			param.div3 = "all"; // 営業所→全て
			param.tanto = "all"; // 担当者→全て
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：販社太郎）
			def r = new SearchResult();
			r.id = "28910127-c2d3-e268-2837-7c510b8730aa";
			r.callId = null;
			r.name = "販社太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/15 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ販社";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：部門太郎１）
			r = new SearchResult();
			r.id = "4e190ebe-72d9-7920-ab61-6935ae93e506";
			r.callId = null;
			r.name = "部門太郎の実績";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ部門";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（３件目：部門太郎２）
			r = new SearchResult();
			r.id = "cb8b0165-c225-0bbc-077a-18f205c61ce6";
			r.callId = null;
			r.name = "部門太郎２の実績";
			r.accountsName = "得意先_神奈川Co";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/22 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎2";
			r.assignedUserLastName = "YTJ部門";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "神奈川Co";
			r.accountSalesOfficeName = null;

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 3;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→既読、コメント未読→コメント無し
		 * */
		@DBUnit
		def "訪問が既読の場合はreadにtrueが設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = true; // 既読
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 1;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→既読、コメント未読→あり（１件）
		 * */
		@DBUnit
		def "未読のコメントが存在する場合はcommentReadCountに1が設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = true;
			r.commentReadCount = 1; // 1：未読コメントあり
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 1;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→既読、コメント未読→なし（１件）
		 * */
		@DBUnit
		def "未読のコメントが存在しない場合はcommentReadCountに0が設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = true;
			r.commentReadCount = 0; // 0：未読コメントなし
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 1;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→既読、コメント未読→なし（１件）、コメント入力→済み
		 * */
		@DBUnit
		def "コメント入力が済の場合はcommentEnteredにtrueが設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = true;
			r.commentReadCount = 0; // 0：未読コメントなし
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = true; // コメント入力済（コメント確認済マークを除く）
			r.callsReadCount = 1;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→未読、コメント未読→コメント無し、訪問理由→３件
		 * */
		@DBUnit
		def "訪問理由が複数ある場合はdivisionCに理由が複数設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = [
				'訪問：新規開拓',
				'訪問：商談あり',
				'訪問：定常'
			];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→未読、コメント未読→コメント無し、注目マーク数→1
		 * */
		@DBUnit
		def "注目マーク数が存在する場合はlikeに注目数が設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.market = "all";
			param.heldOnly = true;
			param.hasInfoOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "45810e38-9789-9bd4-7e7b-8442b82ade0c";
			r.callId = null;
			r.name = "注目マーク数テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 1; // 訪問注目ユーザ→１名
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 0; // 実績から作成した場合は０
			r.heldCount = 1;
			r.isPlan = false; // 実績から作成した場合はfalse
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、日付（From）2016/05/01、日付（To）2016/05/31
		 * 検索結果：１件、訪問→未読、コメント未読→コメント無し、５月の訪問実績を検索（現在日付→７月）
		 * */
		@DBUnit
		def "前月以前の訪問実績を検索する場合"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/05/01");
			param.dateTo = new Date("2016/05/31");
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "46c90482-807b-3d61-7490-20c6ec90421a";
			r.callId = null;
			r.name = "前月以前の訪問実績検索テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/05/20 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 0;
			r.heldCount = 1;
			r.isPlan = false;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、担当者→YTJセールス太郎４
		 * 検索結果：１件、訪問→未読、コメント未読→コメント無し、社内同行者を検索
		 * */
		@DBUnit
		def "社内同行者として登録されたデータを検索した場合"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj064"; // YTJセールス 太郎4（社内同行者）
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "4a4406ca-71ed-b038-8352-6f2e43382adb";
			r.callId = null;
			r.name = "社内同行者テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：１件、訪問→未読、コメント未読→コメント無し
		 * 詳細・・担当者→太郎１、社内同行者→太郎４のデータを、担当者→全てで検索した場合、１件取得できること
		 * */
		@DBUnit
		def "社内同行者を持つ訪問を担当者全てで検索した場合"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected
			def r = new SearchResult();
			r.id = "4a4406ca-71ed-b038-8352-6f2e43382adb";
			r.callId = null;
			r.name = "社内同行者テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			// 取得したデータが１件であること
			model.get("result").size() == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、日付（From）→８月４日、日付（To）→８月５日
		 * 検索対象：３日から６日の日報４件、２０１５年８月６日の日報
		 * 検索結果：２件（４日、５日の日報）
		 * */
		@DBUnit
		def "検索時に日付（To）が設定されている場合"() {
			setup:
			setSalesUser(); // ユーザはYTJセールス 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/04");
			param.dateTo = new Date("2016/08/05");
			param.heldOnly = true;
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：４日の日報）
			def r = new SearchResult();
			r.id = "ba4007d1-7d22-d9da-877c-26552b217045";
			r.callId = null;
			r.name = "セールス太郎の実績_0804";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/04 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：５日の日報）
			r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績_0805";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/05 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：実績のみ→チェックあり、情報ありのみ→チェックなし、未読のみ→チェックなし
		 * 検索対象：２件（実績あり→１件、実績なし→１件）
		 * 検索結果：１件（実績あり→１件）
		 * */
		@DBUnit
		def "検索時に実績のみにチェックが入っている場合"() {
			setup:
			setSalesUser(); // ユーザはYTJセールス 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/23");
			param.dateTo = new Date("2016/08/23");
			param.heldOnly = true; // 実績のみ○
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（実績あり）
			def r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績_実績あり";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/23 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1; // 実績あり
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：実績のみ→チェックなし、情報ありのみ→チェックなし、未読のみ→チェックなし
		 * 検索結果：２件（実績あり→１件、実績なし→１件）
		 * */
		@DBUnit
		def "検索時に実績のみにチェックが入っていない場合"() {
			setup:
			setSalesUser(); // ユーザはYTJセールス 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/23");
			param.dateTo = new Date("2016/08/23");
			param.heldOnly = false; // 実績のみ×
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：実績なし）
			def r = new SearchResult();
			r.id = "f6a705ed-2877-aca8-d553-22d1ba2dadc4";
			r.callId = null;
			r.name = null;
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/23 00:00";
			r.visitDateEnd = "00:00";
			r.description = null;
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 0; // 実績なし
			r.isPlan = true;
			r.infoDiv = null;
			r.maker = null;
			r.kind = null;
			r.category = null;
			r.sensitivity = null;
			r.parentType = "得意先";
			r.status = "予定"; // 訪問の状態
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：実績あり）
			r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績_実績あり";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/23 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1; // 実績あり
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：実績のみ→チェックなし、情報ありのみ→チェックなし、未読のみ→チェックあり
		 * 検索対象：２件（未読あり→１件、既読→１件）
		 * 検索結果：１件（未読あり）
		 * */
		@DBUnit
		def "検索時に未読のみにチェックが入っている場合"() {
			setup:
			setSalesUser(); // ユーザはYTJセールス 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/03");
			param.dateTo = new Date("2016/08/23");
			param.heldOnly = false;
			param.unreadOnly = true; // 未読のみ○

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（既読ユーザなし（未読））
			def r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績_既読ユーザなし";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/23 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = false; // 既読ユーザなし（未読）
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 1;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：実績のみ→チェックなし、情報ありのみ→チェックなし、未読のみ→チェックなし
		 * 検索結果：２件（未読あり→１件、既読→１件）
		 * */
		@DBUnit
		def "検索時に未読のみにチェックが入っていない場合"() {
			setup:
			setSalesUser(); // ユーザはYTJセールス 太郎1を想定

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/03");
			param.dateTo = new Date("2016/08/23");
			param.heldOnly = false;
			param.unreadOnly = false; // 未読のみ×

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（既読ユーザなし（未読））
			def r = new SearchResult();
			r.id = "ba500e3a-d707-962a-a4a5-2cb8a83241c7";
			r.callId = null;
			r.name = "セールス太郎の実績_既読ユーザなし";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/23 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = false; // 既読ユーザなし（未読）
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（既読ユーザあり）
			r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "セールス太郎の実績_既読ユーザあり";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "テスト";
			r.read = true; // 既読ユーザあり
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 2; // 訪問既読ユーザ
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size == 2;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→セールス太郎１（親が案件の訪問を検索）
		 * 検索結果：３件（担当得意先への担当訪問、担当外得意先への担当訪問、社内同行した訪問）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合_担当者固定_案件"() {
			setup:
			setSalesUser();

			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "ytj061";
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：担当した訪問）
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "実績テスト";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = [];

			expected[r.id] = r;

			// 取得結果のexpected（２件目：同じ営業組織内の社内同行した訪問）
			r = new SearchResult();
			r.id = "2962010b-d148-d3b5-0386-2431c62050b4";
			r.callId = null;
			r.name = "担当外得意先_セールス太郎1が同行";
			r.accountsName = "得意先_別の担当者";
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎7";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = ['YTJセールス太郎1[ytj061]']; // 社内同行者

			expected[r.id] = r;

			// 取得結果のexpected（３件目：同じ営業組織内の担当外得意先への訪問）
			r = new SearchResult();
			r.id = "2962010b-d148-d3b5-0386-2431c62050b5";
			r.callId = null;
			r.name = "担当外得意先_セールス太郎1が担当";
			r.accountsName = "得意先_別の担当者";
			r.marketName = "トラック";
			r.visitDateStart = "2016/08/17 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";
			r.callsUsers = []; // 社内同行者

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size() == 3;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
				assert it.callsUsers == e.callsUsers; // 社内同行者のアサート
			}
		}

		/**
		 * 検索条件：初期表示時の設定、営業所→東京本社、担当者→全て（親が案件の訪問を検索）
		 * 検索結果：２件（セールス太郎１，営業所太郎）
		 * */
		@DBUnit
		def "営業所に紐づく担当者の訪問実績を取得する場合_案件"() {
			setup:
			setAllUser();

			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all"; // 担当者→全て
			param.callsInfoDiv = null;

			def expected = [:];

			// 取得結果のexpected（１件目：セールス太郎、親が得意先）
			def r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87939";
			r.callId = null;
			r.name = "セールス太郎の実績_親が得意先";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			// 取得結果のexpected（２件目：営業所太郎、親が得意先）
			r = new SearchResult();
			r.id = "28df0579-e0aa-3d41-404b-9000be73419c";
			r.callId = null;
			r.name = "営業所太郎の実績_親が得意先";
			r.accountsName = "得意先7";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/15 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ営業所";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "得意先";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;
			
			// 取得結果のexpected（３件目：セールス太郎、親が案件）
			r = new SearchResult();
			r.id = "289c0e67-74a0-66c0-49b9-26fcf9e87999";
			r.callId = null;
			r.name = "セールス太郎の実績_親が案件";
			r.accountsName = "得意先1";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/03 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJセールス";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;
			
			// 取得結果のexpected（４件目：営業所太郎、親が案件）
			r = new SearchResult();
			r.id = "28df0579-e0aa-3d41-404b-9000be734aaa";
			r.callId = null;
			r.name = "営業所太郎の実績_親が案件";
			r.accountsName = "得意先7";
			r.marketName = "ＳＳ";
			r.visitDateStart = "2016/08/15 00:00";
			r.visitDateEnd = "00:00";
			r.description = "内容テスト";
			r.read = false;
			r.commentReadCount = -1; // -1以下：コメント無し
			r.like = 0;
			r.assignedUserFirstName = "太郎1";
			r.assignedUserLastName = "YTJ営業所";
			r.divisionC = ['訪問：定常'];
			r.commentEntered = false;
			r.callsReadCount = 0;
			r.plannedCount = 1;
			r.heldCount = 1;
			r.isPlan = true;
			r.infoDiv = "価格・販売状況・チラシ";
			r.maker = "YH";
			r.kind = "PC";
			r.category = "ナツ";
			r.sensitivity = "ポジティブな情報";
			r.parentType = "案件";
			r.status = "実績";
			r.file1 = false;
			r.file2 = false;
			r.file3 = false;
			r.marketInfo = false;
			r.accountDepartmentName = "東京Co";
			r.accountSalesOfficeName = "東京本社";

			expected[r.id] = r;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size() == 4;
			model.get("result").eachWithIndex { it, i ->
				def e = expected.get(it.id)
				assert it.id == e.id;
				assert it.callId == e.callId;
				assert it.name == e.name;
				assert it.accountsName == e.accountsName;
				assert it.marketName == e.marketName;
				assert it.visitDateStart == e.visitDateStart;
				assert it.visitDateEnd == e.visitDateEnd;
				assert it.description == e.description;
				assert it.read == e.read;
				assert it.commentReadCount == e.commentReadCount;
				assert it.like == e.like;
				assert it.assignedUserFirstName == e.assignedUserFirstName;
				assert it.assignedUserLastName == e.assignedUserLastName;
				assert it.divisionC == e.divisionC;
				assert it.commentEntered == e.commentEntered;
				assert it.callsReadCount == e.callsReadCount;
				assert it.plannedCount == e.plannedCount;
				assert it.heldCount == e.heldCount;
				assert it.isPlan == e.isPlan;
				assert it.infoDiv == e.infoDiv;
				assert it.maker == e.maker;
				assert it.kind == e.kind;
				assert it.category == e.category;
				assert it.sensitivity == e.sensitivity;
				assert it.parentType == e.parentType;
				assert it.status == e.status;
				assert it.file1 == e.file1;
				assert it.file2 == e.file2;
				assert it.file3 == e.file3;
				assert it.marketInfo == e.marketInfo;
				assert it.accountDepartmentName == e.accountDepartmentName;
				assert it.accountSalesOfficeName == e.accountSalesOfficeName;
			}
		}

		
		/**
		 * 検索条件：初期表示時の設定
		 * 検索結果：０件
		 * */
		@DBUnit
		def "検索結果が取得できなかった場合は空のリストが設定される"() {
			setup:
			setSalesUser();
			// 検索条件のセットアップ
			def param = new VisitListSearchCondition();

			param.dateFrom = new Date("2016/08/01");
			// To日付が設定されていない場合
			param.dateTo = null;
			param.heldOnly = true;
			param.unreadOnly = false;

			// ユーザはYTJセールス 太郎1を想定
			param.div1 = "0001110103";
			param.div2 = "J310";
			param.div3 = "1100";
			param.tanto = "all";
			param.callsInfoDiv = null;

			when:
			sut.findVisitList(param, model, principal);

			then:
			model.get("result").size() == 0;
		}
	}
}
