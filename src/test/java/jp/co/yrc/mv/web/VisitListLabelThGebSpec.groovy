package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.GebDBSpecification
import jp.co.yrc.mv.common.GebDBUnit

import org.openqa.selenium.JavascriptExecutor


/**
 * 訪問実績画面の多言語化(タイ語)のテスト
 * @author komatsu
 *
 */
class VisitListLabelThGebSpec extends GebDBSpecification {

	JavascriptExecutor exe = (JavascriptExecutor)driver

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{!$("#loadingView").isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])

		waitFor{$("#headerMenu-report").isDisplayed()}
		$("#headerMenu-report").click()
		waitFor{$("[data-menu-id='visitList.html'] img.dashboardImg").isDisplayed()}
		$("[data-menu-id='visitList.html'] img.dashboardImg").click()
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "รายการเยี่ยมชมตามจริง"
	}

	def "検索フォームの各文言が正しいこと"() {
		expect:
		$("#dateFrom").attr("placeholder") == "วันที่(จาก)" // 日付(From)
		$("#dateTo").attr("placeholder") == "วันที่(ถึง)" // 日付(To)
		$("#heldOnly").parent().text() == "แสดงผลเฉพาะปัจจุบัน" // 実績のみ表示
		$("#unreadOnly").parent().text() == "แสดงผลเฉพาะที่ไม่ได้อ่าน" //未読のみ表示
		$("input.search").eq(1).value() == "ค้นหา" // 検索
		$("#csvDownload").value() == "ดาวน์โหลดCSV" // CSVダウンロード
	}

	def "訪問実績一覧操作ボタンの各文言が正しいこと"() {
		expect:
		$("#selectAll").value() == "เลือกท้งหมด" // 全選択
		$("#toRead").value() == "อัพเดตที่อ่านล่าสุด" // 既読へ更新
		$(".modeChange-sort").text() == "เรียงลำดับ" // ソート
		$(".modeChange-colReorder").text() == "เรียงลำดับข้อมูล" // 項目並び替え
		$(".ColVis_MasterButton").text() == "ตั้งค่าการแสดงผลข้อมูล" // 列表示切替
	}

	def "'_MENU_'件表示の文言が正しいこと"() {
		expect:
		$("select[name='DataTables_Table_0_length']").parent().text() == "แสดง\n10\n25\n50\n100\nแถว"
	}

	def "列表示切替の各文言が正しいこと"() {
		setup:
		$(".ColVis button").click()
		waitFor{$(".ColVis_collection").isDisplayed()}

		when:
		def colVis = $(".ColVis_collection li")

		then:
		colVis.eq(0).text() == "แสดงเฉพาะบันทึกที่เยี่ยมชม" // 訪問記録のみ表示
		colVis.eq(1).text() == "แสดงเฉพาะบันทึกข้อมูล" // 情報記録のみを表示
		colVis.eq(2).text() == "ตรวจแล้วเรียบร้อย" // 既読チェック
		colVis.eq(3).text() == "เยี่ยมชม" // 訪問
		colVis.eq(4).text() == "ชื่อเรื่อง" // 件名
		colVis.eq(5).text() == "เนื้อหา" // 内容
		colVis.eq(6).text() == "เกี่ยวข้องกับ" // 関連先
		colVis.eq(7).text() == "สถานะ" // ステータス
		colVis.eq(8).text() == "จุดประสงค์ที่เยี่ยมชม" // 訪問理由
		colVis.eq(9).text() == "แผนก" // 部門
		colVis.eq(10).text() == "พื้นที่" // 営業所
		colVis.eq(11).text() == "ผู้แทนขาย" // 担当者
		colVis.eq(12).text() == "ร่วมกับ" // 社内同行者
		colVis.eq(13).text() == "แผน" // 予定数
		colVis.eq(14).text() == "ผลลัพธ์" // 実績数
		colVis.eq(15).text() == "ประเภทของข้อมูล" // 情報区分
		colVis.eq(16).text() == "ผู้ผลิต" // メーカー
		colVis.eq(17).text() == "ประเภทของผลิตภัณฑ์" // 品種
		colVis.eq(18).text() == "ฤดูร้อน/ฤดูหนาว/ทุกฤดูกาล" // カテゴリ
		colVis.eq(19).text() == "วิวัฒนาการ" // 感度
		colVis.eq(20).text() == "แนบไฟล์" // 添付ファイル
		colVis.eq(21).text() == "ความคิดเห็นที่ยังไม่ได้อ่าน" // コメント未読
		colVis.eq(22).text() == "ตัวเลขที่ถูกใส่เครื่องหมายไว้" // 注目マーク数
		colVis.eq(23).text() == "แสดงความคิดเห็น" // コメント表示
		colVis.eq(24).text() == "ใส่ความคิดเห็น" // コメント入力
		colVis.eq(25).text() == "แสดงทั้งหมด" // 全て表示

		$(".ColVis_catcher").click()
		waitFor{!$("ColVis_collectionBackground").isDisplayed()}
	}

	def "訪問実績一覧列名の各文言が正しいこと"() {
		expect:
		getRowName("readflg") == "เยี่ยมชม" // 訪問
		getRowName("visit-name") == "ชื่อเรื่อง" // 件名
		getRowName("customer-name") == "รายชื่อลูกค้า" // 得意先名
		getRowName("market") == "ช่องทางการขาย" // 販路
		getRowName("datetime") == "วันที่เข้าเยี่ยมชม" // 訪問日時
		getRowName("description") == "เนื้อหา" // 内容
		getRowName("parent-type") == "เกี่ยวข้องกับ" // 関連先
		getRowName("status") == "สถานะ" // ステータス
		getRowName("division-c") == "จุดประสงค์ที่เยี่ยมชม" // 訪問理由
		getRowName("account-department") == "แผนก" // 部門
		getRowName("account-sales-office") == "พื้นที่" // 営業所
		getRowName("assigned-user-name") == "ผู้แทนขาย" // 担当者
		getRowName("calls-users") == "ร่วมกับ" // 社内同行者
		getRowName("planned-count") == "แผน" // 予定数
		getRowName("held-count") == "ผลลัพธ์" // 実績数
		getRowName("info-div") == "ประเภทของข้อมูล" // 情報区分
		getRowName("maker") == "ผู้ผลิต" // メーカー
		getRowName("kind") == "ประเภทของผลิตภัณฑ์" // 品種
		getRowName("category") == "ฤดูร้อน/ฤดูหนาว/ทุกฤดูกาล"
		getRowName("sensitivity") == "วิวัฒนาการ" // 感度
		getRowName("file-download") == "แนบไฟล์" // 添付ファイル
		getRowName("comment-read") == "ความคิดเห็นที่ยังไม่ได้อ่าน" // コメント未読
		getRowName("important-mark") == "ตัวเลขที่ถูกใส่เครื่องหมายไว้" // 注目マーク数
		getRowName("show-comment") == "แสดงความคิดเห็น" // コメント表示
		getRowName("comment-entered") == "ใส่ความคิดเห็น" // コメント入力
		getRowName("show-view-history") == "ประวัติการค้นหา" // 閲覧履歴
	}

	def "訪問実績一覧下部の各文言が正しいこと"() {
		when:
		def foot = $(".dataTables_scrollFoot tfoot")

		then:
		foot.find(".readflg input").value() == "เยี่ยมชม" // 訪問
		foot.find(".visit-name input").attr("placeholder") == "ชื่อเรื่อง" // 件名
		foot.find(".customer-name input").attr("placeholder") == "รายชื่อลูกค้า" // 得意先名
		foot.find(".market input").value() == "ช่องทางการขาย" // 販路
		foot.find(".datetime input").attr("placeholder") == "วันที่เข้าเยี่ยมชม" // 訪問日時
		foot.find(".description input").eq(0).attr("placeholder") == "เนื้อหา" // 内容
		foot.find(".description input").eq(1).value() == "เนื้อหา" // 内容
		foot.find(".parent-type input").value() == "เกี่ยวข้องกับ" // 関連先
		foot.find(".status input").value() == "สถานะ" // ステータス
		foot.find(".division-c input").attr("placeholder") == "จุดประสงค์ที่เยี่ยมชม" // 訪問理由
		foot.find(".account-department input").attr("placeholder") == "แผนก" // 部門
		foot.find(".account-sales-office input").attr("placeholder") == "พื้นที่" // 営業所
		foot.find(".assigned-user-name input").attr("placeholder") == "ผู้แทนขาย" // 担当者
		foot.find(".calls-users input").attr("placeholder") == "ร่วมกับ" // 社内同行者
		foot.find(".planned-count input").attr("placeholder") == "แผน" // 予定数
		foot.find(".held-count input").attr("placeholder") == "ผลลัพธ์" // 実績数
		foot.find(".info-div input").value() == "ประเภทของข้อมูล" // 情報区分
		foot.find(".maker input").value() == "ผู้ผลิต" // メーカー
		foot.find(".kind input").value() == "ประเภทของผลิตภัณฑ์" // 品種
		foot.find(".category input").value() == "ฤดูร้อน/ฤดูหนาว/ทุกฤดูกาล" // カテゴリ
		foot.find(".comment-read input").value() == "ความคิดเห็นที่ยังไม่ได้อ่าน" // コメント未読
		foot.find(".important-mark input").attr("placeholder") == "ตัวเลขที่ถูกใส่เครื่องหมายไว้" // 注目マーク数
		foot.find(".comment-entered input").value() == "ใส่ความคิดเห็น" // コメント入力
		foot.find(".show-view-history input").attr("placeholder") == "ประวัติการค้นหา"// 閲覧履歴
	}

	def "'0 件中 0 から 0 まで表示'の文言が正しいこと"() {
		expect:
		$("#DataTables_Table_0_info").text() == "แสดง 0 ถึง 0 จาก 0 แถว"
	}

	def "ページ移動ボタンの各文言が正しいこと"() {
		expect:
		$(".previous").text() == "ก่อนหน้า" // 前
		$(".next").text() == "ถัดไป" // 次
	}

	@GebDBUnit
	def "訪問実績一覧の各文言が正しいこと"() {
		setup:
		setupDatePicker()

		when:
		def readflg = $("td.readflg.center")
		def commentRead = $("td.comment-read.center")
		def commentEntered = $("td.comment-entered.center")

		then:
		readflg.eq(0).text() == "อ่านแล้ว" // 既読
		readflg.eq(1).text() == "ยังไม่ได้อ่าน" // 未読
		commentRead.eq(0).text() == "ไม่มี" //なし
		commentRead.eq(1).text() == "คงอยู่" // あり
		commentRead.eq(2).text() == "ไม่มีความคิดเห็น" // コメントなし
		commentEntered.eq(0).text() == "สำเร็จ" // 済
		commentEntered.eq(1).text() == "ไม่สำเร็จ" // 未
		$("td.show-comment.center input").eq(0).value() == "แสดงความคิดเห็น" // コメント表示
	}

	@GebDBUnit
	def "未読'を'既読'に更新した場合'既読'の文言が正しいこと"() {
		setup:
		setupDatePicker()
		// 既読に更新
		$("input.readCallId").eq(2).click()

		when:
		$("#toRead").click()

		then:
		$("tbody").find(".readflg").eq(2).text() == "อ่านแล้ว"
	}

	@GebDBUnit
	def "コメント無し'にコメントを入力した場合'なし'と'済'の文言が正しいこと"() {
		setup:
		setupDatePicker()
		$("tbody").find("input[data-roll='displayComment']").eq(2).click()

		when:
		waitFor{$("#commentTable").isDisplayed()}
		$("#commentEditor").value("input")
		$("#registerComment").click()
		$("#cboxClose").click()

		then:
		def tr = $("tbody tr").eq(2)
		tr.children(".comment-read.center").text() == "ไม่มี"
		tr.children(".comment-entered").text() == "สำเร็จ"
	}

	@GebDBUnit
	def "'コメント一覧'の各文言が正しいこと"() {
		setup:
		setupDatePicker()
		$("tbody").find("input[data-roll='displayComment']").eq(1).click()
		waitFor{$("#commentTable").isDisplayed()}

		expect:
		$("#eigyoSosikiNm").parent().parent().children("th").text() == "แผนก" // 所属部署
		$("#visitUserLastName").parent().parent().children("th").text() == "ผู้เยี่ยมชม" // 訪問者
		$("#likeCount").parent().children().eq(0).text() == "ตัวเลขที่ถูกใส่เครื่องหมายไว้：" // 注目マーク数:
		$("#like").value() == "แสตมป์" // 注目マークを付ける
		$("#unlike").value() == "ยกเลิกการเลือกตรวจความคิดเห็น" // 注目マークを解除する
		$("#readComment").value() == "ทำเครื่องหมายเพื่อตรวจความคิดเห็น" //コメント確認済みにマーク
		$("#commentEditor").parent().parent().children("th").text() == "ใส่ข้อมูล" // 入力
		$("#gmailLink").text() == "Gmail" // Gmailを開く
		$("#registerComment").value() == "บันทึกหมายเหตุ" // コメント保存
		$("#cboxClose").click()
		waitFor{!$("#cboxOverlay").isDisplayed()}
	}

	@GebDBUnit
	def "'データはありません'の文言が正しいこと"() {
		setup:
		setupDatePicker()

		expect:
		$(".dataTables_empty").text() == "ไม่พบข้อมูล"
	}

	/**
	 * 検索範囲を９月１日～３０日に設定する
	 * */
	void setupDatePicker() {
		exe.executeScript("document.getElementById('dateFrom').scrollIntoView(false);")
		waitFor{$('#dateFrom').isDisplayed()}
		$('#dateFrom').click()
		def pickerFrom =  $('#ui-datepicker-div')

		pickerFrom.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerFrom.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerFrom.find("td a").find{ it.text() == "1" }.click()

		waitFor{$('#dateTo').isDisplayed()}
		$('#dateTo').click()
		def pickerTo = $('#ui-datepicker-div')

		pickerTo.find(".ui-datepicker-year").find("option").find{ it.value() == "2016" }.click()
		pickerTo.find(".ui-datepicker-month").find("option").find{ it.value() == "8" }.click()
		pickerTo.find("td a").find{ it.text() == "30" }.click()

		$(".search").eq(1).click()
	}

	/**
	 * テーブルの列名を取得
	 * @param name 取得したい列名のクラス
	 * @return 指定した列名
	 */
	String getRowName(String name) {
		exe.executeScript('document.getElementsByClassName("' + name + '")[0].scrollIntoView(true);')

		return $("#table_head").find("." + name).eq(0).text()
	}
}
