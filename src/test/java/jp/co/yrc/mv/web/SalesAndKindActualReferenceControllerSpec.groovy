package jp.co.yrc.mv.web


import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.CompassControlResultDto
import jp.co.yrc.mv.helper.SelectYearsHelper
import jp.co.yrc.mv.web.SalesAndKindActualReferenceController.Result
import jp.co.yrc.mv.web.SalesAndKindActualReferenceController.SearchCondition
import mockit.MockUp

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.support.BindingAwareModelMap

import spock.lang.Unroll


class SalesAndKindActualReferenceControllerSpec extends DBSpecification {
	@Autowired
	SalesAndKindActualReferenceController sut;
	@Autowired
	SelectYearsHelper selectYearsHelper;


	def "初期表示で正しいTemplate名を返す"() {
		when:
		String actual = sut.init(new SearchCondition(), model, principal);

		then:
		actual == "salesAndKindActualReference";
	}

	def "初期表示でパラメータがModelに設定される"() {
		setup:
		def searchCondition = new SearchCondition();
		def expectedModel = new BindingAwareModelMap();

		when:
		sut.init(searchCondition, model, principal);

		then:
		model.get("searchCondition") == searchCondition
	}

	def "初期表示でモデルに年月が設定される"() {
		setup:
		def searchCondition = new SearchCondition();
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)

		when:
		sut.init(searchCondition, model, principal);

		then:
		model.get("years") != null;
		model.get("years") == expectedModel.get("years")
		model.get("months") != null;
		model.get("months") == expectedModel.get("months")
	}

	def "初期表示で検索モードがリアルに設定される"() {
		setup:
		def searchCondition = new SearchCondition();
		def expectedModel = new BindingAwareModelMap();

		when:
		sut.init(searchCondition, model, principal);

		then:
		searchCondition.getSearchMode() == "real"
	}

	def "初期表示で現在年月が設定される"() {
		setup:
		def searchCondition = new SearchCondition();
		def expectedModel = new BindingAwareModelMap()
		// static メソッドのMock化 mockitoの機能を使う
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2000/01/01 00:00:00"))
						return cal
					}
				}

		when:
		sut.init(searchCondition, model, principal);

		then:
		searchCondition.getYear() == 2000
		searchCondition.getMonth() == 1
	}


	def "検索後に正しいTemplate名を返す"() {

		setup:
		def param = new SearchCondition()
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv1('011')
		param.setDiv1('111')

		when:
		String actual = sut.find(param, model, principal);

		then:
		actual == "salesAndKindActualReference";
	}

	def "検索時にパラメータがModelに設定される"() {
		setup:
		def param = new SearchCondition()
		def expectedModel = new BindingAwareModelMap();
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')


		when:
		sut.find(param, model, principal);

		then:
		model.get("searchCondition") == param
	}

	def "検索時にモデルに年月が設定される"() {
		setup:
		def param = new SearchCondition()
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv1('011')
		param.setDiv1('111')


		when:
		sut.find(param, model, principal);

		then:
		model.get("years") != null;
		model.get("years") == expectedModel.get("years")
		model.get("months") != null;
		model.get("months") == expectedModel.get("months")
	}

	def "検索時に検索結果がモデルに設定されている"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv1('011')
		param.setDiv1('111')
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)

		when:
		sut.find(param, model, principal);

		then:
		model.get("list") != null;
	}

	@DBUnit
	def "リアルのデータを取得する"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')
		param.setTanto('ytj061')
		param.setSearchMode("real")
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/01 00:00:00"))
						return cal
					}
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return true
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/01 00:00:00"))
						return cal
					}
				}

		def List<Result> expected = new ArrayList<>();

		def r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 12
		r.number.resultToday = 4
		r.number.resultTotal = 6
		r.number.lastYearResultTotal = 12
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 18
		r.sales.resultToday = 4
		r.sales.resultTotal = 6
		r.sales.lastYearResultTotal = 18
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 902
		r.margin.resultToday = 4
		r.margin.resultTotal = 1804
		r.margin.lastYearResultTotal = 902
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan1";
		r.planProductKindName = "計画品種1";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r


		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan3";
		r.planProductKindName = "計画品種3";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r


		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = "plan2";
		r.planProductKindName = "計画品種2";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r

		when:
		sut.find(param, model, principal);

		then:
		model.get("list").eachWithIndex { it, i ->
			def e = expected.get(i)
			assert it.reportDisplaySeq == e.reportDisplaySeq
			assert it.reportDisplayName == e.reportDisplayName
			assert it.planProductKind == e.planProductKind
			assert it.planProductKindName == e.planProductKindName
			assert it.compassKind == e.compassKind
			assert it.compassKindName == e.compassKindName
			assert it.total == e.total

			assert it.number.plan == e.number.plan
			assert it.number.resultToday == e.number.resultToday
			assert it.number.resultTotal == e.number.resultTotal
			assert it.number.lastYearResultTotal == e.number.lastYearResultTotal
			assert it.number.achievementRate == e.number.achievementRate
			assert it.number.elongation == e.number.elongation

			assert it.sales.plan == e.sales .plan
			assert it.sales.resultToday == e.sales.resultToday
			assert it.sales.resultTotal == e.sales.resultTotal
			assert it.sales.lastYearResultTotal == e.sales.lastYearResultTotal
			assert it.sales.achievementRate == e.sales.achievementRate
			assert it.sales.elongation == e.sales.elongation

			assert it.margin.plan == e.margin.plan
			assert it.margin.resultToday == e.margin.resultToday
			assert it.margin.resultTotal == e.margin.resultTotal
			assert it.margin.lastYearResultTotal == e.margin.lastYearResultTotal
			assert it.margin.achievementRate == e.margin.achievementRate
			assert it.margin.elongation == e.margin.elongation

		}

	}


	@DBUnit
	def "実績が無い場合は計画のみで一覧を取得する"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')
		param.setTanto('ytj061')
		param.setSearchMode("real")
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/01 00:00:00"))
						return cal
					}
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return true
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/01 00:00:00"))
						return cal
					}
				}

		def List<Result> expected = new ArrayList<>();

		def r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 12
		r.number.resultToday = 4
		r.number.resultTotal = 6
		r.number.lastYearResultTotal = 12
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 18
		r.sales.resultToday = 4
		r.sales.resultTotal = 6
		r.sales.lastYearResultTotal = 18
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 902
		r.margin.resultToday = 4
		r.margin.resultTotal = 1804
		r.margin.lastYearResultTotal = 902
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan1";
		r.planProductKindName = "計画品種1";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r


		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan3";
		r.planProductKindName = "計画品種3";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 2
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 2
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 2
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r


		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 0
		r.number.lastYearResultTotal = 0
		r.number.achievementRate = 0
		r.number.elongation = 0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 0
		r.sales.lastYearResultTotal = 0
		r.sales.achievementRate = 0
		r.sales.elongation = 0

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 0
		r.margin.lastYearResultTotal = 0
		r.margin.achievementRate = 0
		r.margin.elongation = 0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = "plan2";
		r.planProductKindName = "計画品種2";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 0
		r.number.lastYearResultTotal = 0
		r.number.achievementRate = 0
		r.number.elongation = 0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 0
		r.sales.lastYearResultTotal = 0
		r.sales.achievementRate = 0
		r.sales.elongation = 0

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 0
		r.margin.lastYearResultTotal = 0
		r.margin.achievementRate = 0
		r.margin.elongation = 0

		expected << r

		when:
		sut.find(param, model, principal);

		then:
		model.get("list").eachWithIndex { it, i ->
			def e = expected.get(i)
			assert it.reportDisplaySeq == e.reportDisplaySeq
			assert it.reportDisplayName == e.reportDisplayName
			assert it.planProductKind == e.planProductKind
			assert it.planProductKindName == e.planProductKindName
			assert it.compassKind == e.compassKind
			assert it.compassKindName == e.compassKindName
			assert it.total == e.total

			assert it.number.plan == e.number.plan
			assert it.number.resultToday == e.number.resultToday
			assert it.number.resultTotal == e.number.resultTotal
			assert it.number.lastYearResultTotal == e.number.lastYearResultTotal
			assert it.number.achievementRate == e.number.achievementRate
			assert it.number.elongation == e.number.elongation

			assert it.sales.plan == e.sales .plan
			assert it.sales.resultToday == e.sales.resultToday
			assert it.sales.resultTotal == e.sales.resultTotal
			assert it.sales.lastYearResultTotal == e.sales.lastYearResultTotal
			assert it.sales.achievementRate == e.sales.achievementRate
			assert it.sales.elongation == e.sales.elongation

			assert it.margin.plan == e.margin.plan
			assert it.margin.resultToday == e.margin.resultToday
			assert it.margin.resultTotal == e.margin.resultTotal
			assert it.margin.lastYearResultTotal == e.margin.lastYearResultTotal
			assert it.margin.achievementRate == e.margin.achievementRate
			assert it.margin.elongation == e.margin.elongation

		}

	}

	@DBUnit
	def "前日のデータを取得する"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(7)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')
		param.setTanto('ytj061')
		param.setSearchMode("yesterday")
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/09 00:00:00"))
						return cal
					}
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return true
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/07/09 00:00:00"))
						return cal
					}
				}

		def List<Result> expected = new ArrayList<>();

		def r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 12
		r.number.resultToday = 0
		r.number.resultTotal = 4
		r.number.lastYearResultTotal = 12
		r.number.achievementRate = 33.3
		r.number.elongation = 33.3

		r.sales.plan = 18
		r.sales.resultToday = 0
		r.sales.resultTotal = 4
		r.sales.lastYearResultTotal = 18
		r.sales.achievementRate = 21.1
		r.sales.elongation = 21.1

		r.margin.plan = 902
		r.margin.resultToday = 0
		r.margin.resultTotal = 4
		r.margin.lastYearResultTotal = 902
		r.margin.achievementRate = 0.4
		r.margin.elongation = 0.4
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan1";
		r.planProductKindName = "計画品種1";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 2
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 33.3
		r.number.elongation = 33.3

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 2
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 21.1
		r.sales.elongation = 21.1

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 2
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 0.4
		r.margin.elongation = 0.4
		expected << r


		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan3";
		r.planProductKindName = "計画品種3";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 2
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 33.3
		r.number.elongation = 33.3

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 2
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 21.1
		r.sales.elongation = 21.1

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 2
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 0.4
		r.margin.elongation = 0.4

		expected << r


		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 2
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 33.3
		r.number.elongation = 33.3

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 2
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 21.1
		r.sales.elongation = 21.1

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 2
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 0.4
		r.margin.elongation = 0.4
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = "plan2";
		r.planProductKindName = "計画品種2";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 2
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 33.3
		r.number.elongation = 33.3

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 2
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 21.1
		r.sales.elongation = 21.1

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 2
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 0.4
		r.margin.elongation = 0.4

		expected << r

		when:
		sut.find(param, model, principal);

		then:
		model.get("list").eachWithIndex { it, i ->
			def e = expected.get(i)
			assert it.reportDisplaySeq == e.reportDisplaySeq
			assert it.reportDisplayName == e.reportDisplayName
			assert it.planProductKind == e.planProductKind
			assert it.planProductKindName == e.planProductKindName
			assert it.compassKind == e.compassKind
			assert it.compassKindName == e.compassKindName
			assert it.total == e.total

			assert it.number.plan == e.number.plan
			assert it.number.resultToday == e.number.resultToday
			assert it.number.resultTotal == e.number.resultTotal
			assert it.number.lastYearResultTotal == e.number.lastYearResultTotal
			assert it.number.achievementRate == e.number.achievementRate
			assert it.number.elongation == e.number.elongation

			assert it.sales.plan == e.sales .plan
			assert it.sales.resultToday == e.sales.resultToday
			assert it.sales.resultTotal == e.sales.resultTotal
			assert it.sales.lastYearResultTotal == e.sales.lastYearResultTotal
			assert it.sales.achievementRate == e.sales.achievementRate
			assert it.sales.elongation == e.sales.elongation

			assert it.margin.plan == e.margin.plan
			assert it.margin.resultToday == e.margin.resultToday
			assert it.margin.resultTotal == e.margin.resultTotal
			assert it.margin.lastYearResultTotal == e.margin.lastYearResultTotal
			assert it.margin.achievementRate == e.margin.achievementRate
			assert it.margin.elongation == e.margin.elongation

		}

	}


	@DBUnit
	def "前月のデータを取得する"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(6)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')
		param.setTanto('ytj061')
		param.setSearchMode("lastMonth")
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/6/01 00:00:00"))
						return cal
					}
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return false
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/06/01 00:00:00"))
						return cal
					}
				}

		def List<Result> expected = new ArrayList<>();

		def r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 12
		r.number.resultToday = 0
		r.number.resultTotal = 6
		r.number.lastYearResultTotal = 12
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 18
		r.sales.resultToday = 0
		r.sales.resultTotal = 6
		r.sales.lastYearResultTotal = 18
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 902
		r.margin.resultToday = 0
		r.margin.resultTotal = 1804
		r.margin.lastYearResultTotal = 902
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan1";
		r.planProductKindName = "計画品種1";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r


		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan3";
		r.planProductKindName = "計画品種3";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r


		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = "plan2";
		r.planProductKindName = "計画品種2";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r

		when:
		sut.find(param, model, principal);

		then:
		model.get("list").eachWithIndex { it, i ->
			def e = expected.get(i)
			assert it.reportDisplaySeq == e.reportDisplaySeq
			assert it.reportDisplayName == e.reportDisplayName
			assert it.planProductKind == e.planProductKind
			assert it.planProductKindName == e.planProductKindName
			assert it.compassKind == e.compassKind
			assert it.compassKindName == e.compassKindName
			assert it.total == e.total

			assert it.number.plan == e.number.plan
			assert it.number.resultToday == e.number.resultToday
			assert it.number.resultTotal == e.number.resultTotal
			assert it.number.lastYearResultTotal == e.number.lastYearResultTotal
			assert it.number.achievementRate == e.number.achievementRate
			assert it.number.elongation == e.number.elongation

			assert it.sales.plan == e.sales .plan
			assert it.sales.resultToday == e.sales.resultToday
			assert it.sales.resultTotal == e.sales.resultTotal
			assert it.sales.lastYearResultTotal == e.sales.lastYearResultTotal
			assert it.sales.achievementRate == e.sales.achievementRate
			assert it.sales.elongation == e.sales.elongation

			assert it.margin.plan == e.margin.plan
			assert it.margin.resultToday == e.margin.resultToday
			assert it.margin.resultTotal == e.margin.resultTotal
			assert it.margin.lastYearResultTotal == e.margin.lastYearResultTotal
			assert it.margin.achievementRate == e.margin.achievementRate
			assert it.margin.elongation == e.margin.elongation

		}

	}



	@DBUnit
	def "指定月のデータを取得する"() {
		setup:
		def param = new SearchCondition();
		param.setYear(2016)
		param.setMonth(5)
		param.setDiv1('001')
		param.setDiv2('011')
		param.setDiv3('111')
		param.setTanto('ytj061')
		param.setSearchMode("nominate")
		def expectedModel = new BindingAwareModelMap();
		selectYearsHelper.create(expectedModel)
		new MockUp<DateUtils>() {
					@mockit.Mock
					Calendar getYearMonthCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/5/01 00:00:00"))
						return cal
					}
					@mockit.Mock
					boolean isCurrentYearMonth(int year, int month) {
						return false
					}
					@mockit.Mock
					Calendar getCalendar() {
						def cal = Calendar.instance
						cal.setTime(new Date("2016/05/01 00:00:00"))
						return cal
					}
				}

		def List<Result> expected = new ArrayList<>();

		def r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 12
		r.number.resultToday = 0
		r.number.resultTotal = 6
		r.number.lastYearResultTotal = 12
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 18
		r.sales.resultToday = 0
		r.sales.resultTotal = 6
		r.sales.lastYearResultTotal = 18
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 902
		r.margin.resultToday = 0
		r.margin.resultTotal = 1804
		r.margin.lastYearResultTotal = 902
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan1";
		r.planProductKindName = "計画品種1";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r


		r = new Result();
		r.reportDisplaySeq = 1;
		r.reportDisplayName = "帳票表示順1";
		r.planProductKind = "plan3";
		r.planProductKindName = "計画品種3";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r


		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = null;
		r.planProductKindName = null;
		r.total = true;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0
		expected << r;

		r = new Result();
		r.reportDisplaySeq = 2;
		r.reportDisplayName = "帳票表示順2";
		r.planProductKind = "plan2";
		r.planProductKindName = "計画品種2";
		r.total = false;
		r.number.plan = 6
		r.number.resultToday = 0
		r.number.resultTotal = 3
		r.number.lastYearResultTotal = 6
		r.number.achievementRate = 50.0
		r.number.elongation = 50.0

		r.sales.plan = 9
		r.sales.resultToday = 0
		r.sales.resultTotal = 3
		r.sales.lastYearResultTotal = 9
		r.sales.achievementRate = 32.2
		r.sales.elongation = 32.2

		r.margin.plan = 451
		r.margin.resultToday = 0
		r.margin.resultTotal = 902
		r.margin.lastYearResultTotal = 451
		r.margin.achievementRate = 200.0
		r.margin.elongation = 200.0

		expected << r

		when:
		sut.find(param, model, principal);

		then:
		model.get("list").eachWithIndex { it, i ->
			def e = expected.get(i)
			assert it.reportDisplaySeq == e.reportDisplaySeq
			assert it.reportDisplayName == e.reportDisplayName
			assert it.planProductKind == e.planProductKind
			assert it.planProductKindName == e.planProductKindName
			assert it.compassKind == e.compassKind
			assert it.compassKindName == e.compassKindName
			assert it.total == e.total

			assert it.number.plan == e.number.plan
			assert it.number.resultToday == e.number.resultToday
			assert it.number.resultTotal == e.number.resultTotal
			assert it.number.lastYearResultTotal == e.number.lastYearResultTotal
			assert it.number.achievementRate == e.number.achievementRate
			assert it.number.elongation == e.number.elongation

			assert it.sales.plan == e.sales .plan
			assert it.sales.resultToday == e.sales.resultToday
			assert it.sales.resultTotal == e.sales.resultTotal
			assert it.sales.lastYearResultTotal == e.sales.lastYearResultTotal
			assert it.sales.achievementRate == e.sales.achievementRate
			assert it.sales.elongation == e.sales.elongation

			assert it.margin.plan == e.margin.plan
			assert it.margin.resultToday == e.margin.resultToday
			assert it.margin.resultTotal == e.margin.resultTotal
			assert it.margin.lastYearResultTotal == e.margin.lastYearResultTotal
			assert it.margin.achievementRate == e.margin.achievementRate
			assert it.margin.elongation == e.margin.elongation

		}

	}




	def "集計Mapに該当する結果オブジェクトが存在しない場合は新たに作成し実績を集計する"() {
		setup:
		def map = new LinkedHashMap<Result, Result>()
		def totalMap = new LinkedHashMap<Integer, Result>()
		def o = new  CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.reportDisplayName = "reportDisplayName"
		o.number = 2
		o.sales = 3
		o.margin = 4

		when:
		sut.setTotal(map, totalMap, o);

		then:
		Result total = totalMap.get(o.reportDisplaySeq)
		total.reportDisplayName == o.reportDisplayName
		total.reportDisplaySeq == o.reportDisplaySeq
		total.planProductKind == null
		total.total == true
		total.number.resultTotal == o.number
		total.sales.resultTotal == o.sales
		total.margin.resultTotal == o.margin


		Result totalResult = map.get(new Result(o.reportDisplaySeq))
		totalResult.reportDisplayName == o.reportDisplayName
		totalResult.reportDisplaySeq == o.reportDisplaySeq
		totalResult.planProductKind == null
		totalResult.total == true
		totalResult.number.resultTotal == o.number
		totalResult.sales.resultTotal == o.sales
		totalResult.margin.resultTotal == o.margin

	}

	def "集計Mapに該当する結果オブジェクトが存在する場合はそのオブジェクトに対し実績を集計する"() {

		setup:
		def map = new LinkedHashMap<Result, Result>()
		def totalMap = new LinkedHashMap<Integer, Result>()
		def o = new  CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.reportDisplayName = "reportDisplayName"
		o.number = 2
		o.sales = 3
		o.margin = 4

		def r = new Result(o.reportDisplaySeq)
		r.margin.resultTotal = 10
		r.number.resultTotal = 10
		r.sales.resultTotal = 10
		totalMap.put(o.reportDisplaySeq, r)
		totalMap.put(2, new Result(2))
		map.put(r, r)
		map.put(new Result(2), new Result(22))

		when:
		sut.setTotal(map, totalMap, o);

		then:
		Result total = totalMap.get(o.reportDisplaySeq)
		total.number.resultTotal == o.number + 10
		total.sales.resultTotal == o.sales + 10
		total.margin.resultTotal == o.margin + 10


		Result totalResult = map.get(new Result(o.reportDisplaySeq))
		totalResult.number.resultTotal == o.number+ 10
		totalResult.sales.resultTotal == o.sales + 10
		totalResult.margin.resultTotal == o.margin+ 10
	}

	def "モードに関わらず実績Mapに該当する結果オブジェクトが存在しない場合は当日実績設定をしない"() {
		setup:
		def searchCondition = new SearchCondition();
		searchCondition.searchMode = "real"
		def expectedModel = new BindingAwareModelMap();
		def o = new CompassControlResultDto()
		o.planProductKind = "c";
		o.reportDisplaySeq = 3
		o.number = 2
		o.sales = 3
		o.margin = 4

		def map = new TreeMap<Result, Result>();
		def totalMap = new LinkedHashMap<Integer, Result>();
		def result = new Result(1, "a");
		def total = new Result(o.reportDisplaySeq)
		map.put(result, result)
		totalMap.put(o.reportDisplaySeq , total)

		when:
		sut.setToday(searchCondition, map, totalMap, o);

		then:
		result.number.resultToday == 0
		result.sales.resultToday == 0
		result.margin.resultToday == 0
		result.number.resultTotal == 0
		result.sales.resultTotal == 0
		result.margin.resultTotal == 0
	}


	def "モードに関わらず集計Mapに該当する結果オブジェクトが存在しない場合は当日設定をしない"() {
		setup:
		def searchCondition = new SearchCondition();
		searchCondition.searchMode = "real"
		def expectedModel = new BindingAwareModelMap();
		def o = new CompassControlResultDto()
		o.planProductKind = "c";
		o.reportDisplaySeq = 3
		o.number = 2
		o.sales = 3
		o.margin = 4

		def map = new TreeMap<Result, Result>();
		def totalMap = new LinkedHashMap<Integer, Result>();
		def result = new Result(o.reportDisplaySeq , o.planProductKind);
		def total = new Result(1)
		map.put(result, result)
		totalMap.put(1 , total)


		when:
		sut.setToday(searchCondition, map, totalMap, o);

		then:
		result.number.resultToday == 0
		result.sales.resultToday == 0
		result.margin.resultToday == 0
		result.number.resultTotal == 0
		result.sales.resultTotal == 0
		result.margin.resultTotal == 0
	}


	def "モードがリアルの場合は当日に値が設定され合計に足し込まれる"() {
		setup:
		def searchCondition = new SearchCondition();
		searchCondition.searchMode = "real"
		def expectedModel = new BindingAwareModelMap();
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 2
		o.sales = 3
		o.margin = 4

		def map = new TreeMap<Result, Result>();
		def totalMap = new LinkedHashMap<Integer, Result>();
		def result = new Result(o.reportDisplaySeq, o.planProductKind);
		map.put(result, result)
		map.put(new Result(2, "b"), new Result(2, "b"))

		def total = new Result(o.reportDisplaySeq)
		total.number.resultToday = 10
		total.sales.resultToday = 10
		total.margin.resultToday = 10
		totalMap.put(o.reportDisplaySeq , total)

		when:
		sut.setToday(searchCondition, map, totalMap, o);

		then:
		result.number.resultToday == o.number
		result.sales.resultToday == o.sales
		result.margin.resultToday == o.margin
		result.number.resultTotal == 0
		result.sales.resultTotal == 0
		result.margin.resultTotal == 0

		total.number.resultToday == o.number + 10
		total.sales.resultToday == o.sales + 10
		total.margin.resultToday == o.margin + 10
		total.number.resultTotal == 0
		total.sales.resultTotal == 0
		total.margin.resultTotal == 0
	}

	def "モードが前日の場合は合計から当日分が引かれる"() {
		setup:
		def searchCondition = new SearchCondition();
		searchCondition.searchMode = "yesterday"
		def expectedModel = new BindingAwareModelMap();
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 2
		o.sales = 3
		o.margin = 4

		def map = new TreeMap<Result, Result>();
		def totalMap = new LinkedHashMap<Integer, Result>();
		def result = new Result(o.reportDisplaySeq, o.planProductKind);
		result.number.resultTotal = 11
		result.sales.resultTotal = 11
		result.margin.resultTotal = 11
		map.put(result, result)
		map.put(new Result(2, "b"), new Result(2, "b"))

		def total = new Result(o.reportDisplaySeq)
		total.number.resultTotal = 10
		total.sales.resultTotal = 10
		total.margin.resultTotal = 10
		totalMap.put(o.reportDisplaySeq , total)


		when:
		sut.setToday(searchCondition, map, totalMap, o);

		then:
		result.number.resultTotal == 11- o.number
		result.sales.resultTotal == 11- o.sales
		result.margin.resultTotal == 11- o.margin
		result.number.resultToday == 0
		result.sales.resultToday == 0
		result.margin.resultToday == 0

		total.number.resultTotal == 10 - o.number
		total.sales.resultTotal == 10 - o.sales
		total.margin.resultTotal == 10 - o.margin
		total.number.resultToday == 0
		total.sales.resultToday == 0
		total.margin.resultToday == 0
	}

	def "結果オブジェクトに実績値が設定される"() {
		setup:
		def o = new CompassControlResultDto()
		o.compassKind = "compassKind"
		o.compassKindName = "compassKindName"
		o.planProductKind = "planProductKind"
		o.planProductKindName = "planProductKindName"
		o.reportDisplayName = "reportDisplayName"
		o.reportDisplaySeq = 1
		o.margin = 2
		o.sales = 3
		o.number = 4

		def map = new HashMap();
		when:
		sut.setResult(map, o);

		then:
		Result r = map.get(new Result(o.reportDisplaySeq , o.planProductKind ))
		r.margin.resultTotal == o.margin;
		r.sales.resultTotal == o.sales;
		r.number.resultTotal == o.number;
	}

	def "結果オブジェクトをDB取得項目を元に生成する"() {
		setup:
		def o = new CompassControlResultDto()
		o.compassKind = "compassKind"
		o.compassKindName = "compassKindName"
		o.planProductKind = "planProductKind"
		o.planProductKindName = "planProductKindName"
		o.reportDisplayName = "reportDisplayName"
		o.reportDisplaySeq = 1
		o.margin = 2
		o.sales = 3
		o.number = 4

		def map = new HashMap();

		when:
		Result r = sut.createResult(o);

		then:
		r.compassKind == o.compassKind
		r.compassKindName == o.compassKindName
		r.planProductKind == o.planProductKind
		r.planProductKindName == o.planProductKindName
		r.reportDisplayName == o.reportDisplayName
		r.reportDisplaySeq == o.reportDisplaySeq
		r.total == false
		r.margin.resultTotal == 0;
		r.sales.resultTotal == 0;
		r.number.resultTotal == 0;
	}

	def "前年実績と成長率設定し集計オブジェクトに前年実績を足し込む"() {
		setup :
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 4
		o.sales = 9
		o.margin = 15.68627450980392f

		def map = new TreeMap<Result, Result>();
		def totalMap = new LinkedHashMap<Integer, Result>();
		def result = new Result(o.reportDisplaySeq, o.planProductKind);
		result.number.resultTotal = 2
		result.sales.resultTotal = 3
		result.margin.resultTotal = 4
		map.put(result, result)

		def total = new Result(o.reportDisplaySeq)
		total.number.lastYearResultTotal = 10
		total.sales.lastYearResultTotal = 10
		total.margin.lastYearResultTotal = 10
		totalMap.put(o.reportDisplaySeq , total)


		when:
		sut.setElongation(map, totalMap, o)

		then:
		result.number.lastYearResultTotal == o.number
		result.sales.lastYearResultTotal ==  o.sales
		result.margin.lastYearResultTotal == o.margin
		result.number.elongation == 50
		result.sales.elongation ==  33.3
		result.margin.elongation == 25.5

		total.number.lastYearResultTotal == 10 +  o.number
		total.sales.lastYearResultTotal == 10 + o.sales
		total.margin.lastYearResultTotal == 10 + o.margin
	}

	def "実績のMapに該当する結果オブジェクトが存在する場合はそのオブジェクトに対し計画値を設定する"() {
		setup:
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 4
		o.sales = 9
		o.margin = 15.68627450980392f

		def map = new TreeMap<Result, Result>();
		def result = new Result(o.reportDisplaySeq, o.planProductKind);
		result.number.resultTotal = 2
		result.sales.resultTotal = 3
		result.margin.resultTotal = 4

		map.put(result, result)

		when:
		sut.setPlanAndAchievementRate(map, o);

		then:
		result.number.plan == o.number
		result.sales.plan == o.sales
		result.margin.plan == o.margin
		result.number.achievementRate == 50
		result.sales.achievementRate == 33.3
		result.margin.achievementRate == 25.5
	}

	def "実績のMapに該当する結果オブジェクトが存在しない場合は新たに作成し計画値を設定する"() {
		setup:
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 2
		o.sales = 3
		o.margin = 4

		def map = new TreeMap<Result, Result>();

		when:
		sut.setPlanAndAchievementRate(map, o);

		then:
		def result = map.get(new Result(o.reportDisplaySeq, o.planProductKind));
		result.reportDisplayName == o.reportDisplayName
		result.reportDisplaySeq == o.reportDisplaySeq
		result.planProductKind == o.planProductKind
		result.total == false
		result.number.plan == o.number
		result.sales.plan == o.sales
		result.margin.plan == o.margin
		result.number.achievementRate == 0
		result.sales.achievementRate == 0
		result.margin.achievementRate == 0
	}

	def "集計Mapに該当する結果オブジェクトが存在する場合はそのオブジェクトに対し計画値を設定する"() {
		setup:
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 2
		o.sales = 3
		o.margin = 4

		def totalMap = new LinkedHashMap<Integer, Result>();
		def total = new Result(o.reportDisplaySeq)
		totalMap.put(o.reportDisplaySeq , total)
		total.number.plan = 10
		total.sales.plan = 10
		total.margin.plan = 10
		def map = new TreeMap<Result, Result>();

		when:
		sut.setPlanTotal(map, totalMap, o);

		then:
		total.number.plan == o.number + 10
		total.sales.plan == o.sales + 10
		total.margin.plan == o.margin + 10


	}

	def "集計Mapに該当する結果オブジェクトが存在しない場合は新たに作成し計画値を設定する"() {
		setup:
		def o = new CompassControlResultDto()
		o.planProductKind = "a";
		o.reportDisplaySeq = 1
		o.number = 2
		o.sales = 3
		o.margin = 4

		def totalMap = new LinkedHashMap<Integer, Result>();
		def map = new TreeMap<Result, Result>();

		when:
		sut.setPlanTotal(map, totalMap, o);


		then:
		Result total = totalMap.get(o.reportDisplaySeq)
		total.reportDisplayName == o.reportDisplayName
		total.reportDisplaySeq == o.reportDisplaySeq
		total.planProductKind == null
		total.total == true
		total.number.plan == o.number
		total.sales.plan == o.sales
		total.margin.plan == o.margin


		Result totalResult = map.get(new Result(o.reportDisplaySeq))
		totalResult.reportDisplayName == o.reportDisplayName
		totalResult.reportDisplaySeq == o.reportDisplaySeq
		totalResult.planProductKind == null
		totalResult.total == true
		totalResult.number.plan == o.number
		totalResult.sales.plan == o.sales
		totalResult.margin.plan == o.margin
	}


	def "合計オブジェクトの達成率と伸長率を計算する"() {
		setup:
		def r = new Result()
		r.number.resultTotal = 1
		r.sales.resultTotal = 2
		r.margin.resultTotal =3

		r.number.plan = 10
		r.sales.plan = 8
		r.margin.plan = 9

		r.number.lastYearResultTotal = 10
		r.sales.lastYearResultTotal = 8
		r.margin.lastYearResultTotal = 9

		when:
		sut.calcTotalRate(r);

		then:
		r.number.achievementRate ==10
		r.sales.achievementRate == 25
		r.margin.achievementRate == 33.3

		r.number.elongation ==10;
		r.sales.elongation == 25
		r.margin.elongation == 33.3
	}

	def "金額と粗利を千円単位にする"() {
		setup:
		def r = new Result()
		r.sales.resultTotal = 1000;
		r.sales.resultToday = 2000
		r.sales.lastYearResultTotal = 3000
		r.sales.plan = 4000

		r.margin.resultTotal = 5000
		r.margin.resultToday =6000
		r.margin.lastYearResultTotal = 7000
		r.margin.plan = 8000

		when:
		sut.setK(r);

		then:
		r.sales.resultTotal == 1;
		r.sales.resultToday == 2
		r.sales.lastYearResultTotal == 3
		r.sales.plan == 4

		r.margin.resultTotal == 5
		r.margin.resultToday ==6
		r.margin.lastYearResultTotal == 7
		r.margin.plan == 8
	}



	@Unroll
	def "結果オブジェクト比較で［表示順:#seq1｜計画品種:#kind1］と［表示順:#seq2｜計画品種:#kind2］を比較すると#resultが返る"() {
		setup :
		def o1 = new Result(seq1, kind1);
		def o2 = new Result(seq2, kind2);

		expect:
		o1.compareTo(o2) == result

		where:
		seq1 | kind1 | seq2 | kind2 || result
		1 | null | 1 | null || 0
		1 | '1' | 1 | null || 1
		1 | null | 1 | '1' || -1
		1 | '1' | 1 | '1' || 0
		1 | '1' | 1 | '2' || -1
		1 | '2' | 1 | '1' || 1

		1 | null | 2 | null || -1
		1 | '1' | 2 | null || -1
		1 | null | 2 | '1' || -1
		1 | '1' | 2 | '1' || -1
		1 | '1' | 2 | '2' || -1
		1 | '2' | 2 | '1' || -1

		2 | null | 1 | null || 1
		2 | '1' | 1 | null || 1
		2 | null | 1 | '1' || 1
		2 | '1' | 1 | '1' || 1
		2 | '1' | 1 | '2' || 1
		2 | '2' | 1 | '1' || 1
	}
}

