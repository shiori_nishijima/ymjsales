package jp.co.yrc.mv.web;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-application-config.xml" })
public class IndexControllerTest {
	
	@Autowired
	IndexController sut;

	@Test
	public void スタート() {
		String actual = sut.start();
		String expected = "index";

		assertThat(actual, is(expected));
	}
}
