package jp.co.yrc.mv.web

import geb.Page
import jp.co.yrc.mv.common.GebDBSpecification


/**
 * ダッシュボード画面の多言語化対応(タイ語)テスト
 * @author komatsu
 *
 */
class DashbordLabelThGebSpec extends GebDBSpecification {

	void setupSpec() {
		to LoginPage
		$('#userId').value('ytj01')
		$('#password').value('passw0rd')
		$('#lblLocaleTh').click()
		$('#btnLogin').click()
		waitFor{!$('#loadingView').isDisplayed()}
		$('#btnHeaderMv').click()

		//存在するウィンドウをリストで取得
		def windows = driver.getWindowHandles();
		// ハンドルを新しく生成されたウィンドウに切り替え
		driver.switchTo().window(windows[1])
	}

	def "ページタイトルの文言が正しいこと"() {
		expect:
		driver.getTitle() == "รายงานตามรายการ"
	}

	def "各画面名の文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()

		expect:
		$("[data-menu-id='visitList.html']").text() == "รายการเยี่ยมชมตามจริง"
		$("[data-menu-id='dailyReportViewerSales.html']").text() == "รายงานประจำวันที่ได้รับการยืนยัน (ผู้แทนขาย)"
		$("[data-menu-id='dailyReportViewerAdmin.html']").text() == "รายงานประจำวันที่ได้รับการยืนยัน (ผู้จัดการ)"
	}

	def "ヘッダーメニューの各文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()

		expect:
		$('#headerMenu-report').text() == "รายงานตามรายการ"
		$('#headerMenu-favorite').text() == "รายการที่ชื่นชอบ"
		$("#setupFavorite").text() == "เพิ่มรายการที่ชื่นชอบ"
	}

	def "'お気に入り解除'の文言が正しいこと"() {
		setup:
		$("#headerMenu-favorite").click()

		// お気に入りが登録されていなかったら登録する。
		if ($('#nothin-favorite').css('display') == 'block') {
			$("#headerMenu-report").click()
			$("#setupFavorite").click()
			$("[data-menu-id='visitList.html']").find("div.click-area").click()
			$("#setupFavorite").click()
		}
		$("#headerMenu-favorite").click()

		expect:
		$("#setupFavorite").text() == "รายการที่ชื่นชอบล่าสุด"
	}

	def "'保存'の文言が正しいこと"() {
		setup:
		$("#headerMenu-report").click()
		def save = $("#setupFavorite")

		when:
		save.click()

		then:
		save.text() == "บันทึก"
		save.click()
	}

	def "'お気に入りが登録されていません'の文言が正しいこと"() {
		setup:
		$("#headerMenu-favorite").click()
		$("#setupFavorite").click()
		def list = $("[data-favorite-prev='true']").find(".click-area")
		list.each{favorite->
			$(favorite).click()
		}
		$("#setupFavorite").click()

		expect:
		$("#nothing-favorite-img").text() == "ข้อมูลที่เลือกเป็นที่ชื่นชอบไม่ได้รับการลงทะเบียน"
	}
}

class LoginPage extends Page {
	static url = "http://localhost:8080/ymjsfa/view/main.html"
	static content = {
	}
}
