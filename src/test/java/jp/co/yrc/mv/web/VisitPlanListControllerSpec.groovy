package jp.co.yrc.mv.web

import jp.co.yrc.mv.common.DBSpecification
import jp.co.yrc.mv.common.DBUnit
import jp.co.yrc.mv.common.DateUtils
import jp.co.yrc.mv.dto.UserInfoDto
import jp.co.yrc.mv.web.VisitPlanListController.AccountRow
import jp.co.yrc.mv.web.VisitPlanListController.DailyCallInfo
import jp.co.yrc.mv.web.VisitPlanListController.SearchCondition
import mockit.MockUp

import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken


/**
 * 訪問計画一覧画面のテスト
 * @author SHIORIN
 *
 */
@RunWith(Enclosed)
class VisitPlanListControllerSpec {
	/**
	 * 訪問計画一覧画面の初期表示時のテスト
	 * 対象メソッド：init
	 * */
	static class VisitPlanListControllerSpec_訪問計画一覧初期表示 extends DBSpecification {

		@Autowired
		VisitPlanListController sut;

		void setupSpec() {
			setSalesUser();

			// 現在日時を 2016/08/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/09 00:00:00"))
							return cal
						}
					}
		}

		/** 営業ユーザ */
		void setSalesUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj061");
			userInfoDto.setLastName("YTJセールス");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("06"); // 営業権限
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			when:
			String actual = sut.init(new SearchCondition(), model, principal);

			then:
			actual == "visitPlanList";
		}

		/**
		 * model に引数として渡したparamが設定されていること
		 * */
		def "パラメータに現在年月が設定される"(){
			setup:
			def param = new SearchCondition();

			when:
			sut.init(param, model, principal);

			then:
			def condition = model.get("searchCondition");
			condition == param;
			condition.year == 2016;
			condition.month == 8;
		}

		/**
		 * 年月に応じた日付と曜日が取得できていること
		 * */
		def "headerがmodelに設定される"() {
			setup:
			def param = new SearchCondition();
			def expected = createHeaderExpected();

			when:
			sut.init(param, model, principal);

			then:
			model.get("header").size() == 32; // 8月は31日までなので、31＋合計(0)
			model.get("header").eachWithIndex { it, i ->
				def e = expected.get(it.date)
				assert it.date == e.date;
				assert it.dayOfWeek == e.dayOfWeek;
				assert it.dayOfWeekLabel == e.dayOfWeekLabel;
				assert it.total == e.total;
				assert it.plan == e.plan;
				assert it.result == e.result;
			}
		}

		def "空のplanInfoListがmodelに設定される"() {
			setup:
			def param = new SearchCondition();

			when:
			sut.init(param, model, principal);

			then:
			def condition = model.get("planInfoList");
			condition.size() == 0;
		}
	}

	/**
	 * 訪問計画一覧画面の検索処理時のテスト
	 * 対象メソッド：find
	 * */
	static class VisitPlanListControllerSpec_訪問計画一覧検索処理 extends DBSpecification {
		@Autowired
		VisitPlanListController sut;

		void setupSpec() {
			setSalesUser();

			// 現在日時を 2016/08/09 に固定
			new MockUp<DateUtils>() {
						@mockit.Mock
						boolean isCurrentYearMonth(int year, int month) {
							return true
						}
						@mockit.Mock
						Calendar getCalendar() {
							def cal = Calendar.instance
							cal.setTime(new Date("2016/08/09 00:00:00"))
							return cal
						}
					}
		}

		/** 営業ユーザ */
		void setSalesUser() {
			UserInfoDto userInfoDto = new UserInfoDto();
			userInfoDto.setId("ytj061");
			userInfoDto.setLastName("YTJセールス");
			userInfoDto.setFirstName("太郎1");
			userInfoDto.setAuthorization("06"); // 営業権限
			principal = new UsernamePasswordAuthenticationToken(userInfoDto, "password");
		}

		def "正しいTemplate名を返す"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj061";

			when:
			String actual = sut.find(param, model, principal);

			then:
			actual == "visitPlanList";
		}

		def "modelに引数として渡したparamが設定される"(){
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj061";

			when:
			sut.find(param, model, principal);

			then:
			def condition = model.get("searchCondition");
			condition == param;
			condition.year == param.year;
			condition.month == param.month;
			condition.tanto == param.tanto;
		}

		/**
		 * planInfoList：得意先ごとの検索結果リスト
		 * 検索結果：日付ごとのデータ（各日１件以上）が計画と実績に設定されていること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj061"; // YTJセールス太郎1

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld";
			a.plan = 39;
			a.result = 39;
			a.dailyInfos = createEx2000000001();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other";
			a.plan = 0;
			a.result = 35;
			a.dailyInfos = createEx2000000003();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld";
			a.plan = 35;
			a.result = 0;
			a.dailyInfos = createEx2000000005();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld";
			a.plan = 0;
			a.result = 0;
			a.dailyInfos = createEx9000000001();

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
				it.dailyInfos.eachWithIndex {info, j ->
					def d = e.dailyInfos.get(j);
					assert info.date == d.date;
					assert info.dayOfWeek == d.dayOfWeek;
					assert info.dayOfWeekLabel == d.dayOfWeekLabel;
					assert info.total == d.total;
					assert info.plan == d.plan;
					assert info.result == d.result;
				}
			}
		}

		/**
		 * planInfoList：得意先ごとの検索結果リスト
		 * 検索結果：日付ごとのデータ（各日１件以上）が計画と実績に設定されていること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_過去を検索した場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 4;
			param.tanto = "ytj061";

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld";
			a.plan = 38;
			a.result = 38;
			a.dailyInfos = createEx2000000001();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other";
			a.plan = 0;
			a.result = 34;
			a.dailyInfos = createEx2000000003();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld";
			a.plan = 34;
			a.result = 0;
			a.dailyInfos = createEx2000000005();

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld";
			a.plan = 0;
			a.result = 0;
			a.dailyInfos = createEx9000000001();

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 30; // 日付ごとの訪問実績データ数
				it.dailyInfos.eachWithIndex {info, j ->
					def d = e.dailyInfos.get(j);
					assert info.date == d.date;
					assert info.dayOfWeek == d.dayOfWeek;
					assert info.dayOfWeekLabel == d.dayOfWeekLabel;
					assert info.total == d.total;
					assert info.plan == d.plan;
					assert info.result == d.result;
				}
			}
		}

		/**
		 * planInfoList：得意先ごとの検索結果リスト
		 * 検索年月：2016年2月（閏年○）
		 * 検索結果：日付ごとのデータ（各日１件）が計画と実績に設定されていること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_閏年の2月を検索した場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 2;
			param.tanto = "ytj061";

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld";
			a.plan = 29;
			a.result = 29;
			a.dailyInfos = createExFebruary(29, 1, 1);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other";
			a.plan = 0;
			a.result = 29;
			a.dailyInfos = createExFebruary(29, 0, 1);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld";
			a.plan = 29;
			a.result = 0;
			a.dailyInfos = createExFebruary(29, 1, 0);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld";
			a.plan = 0;
			a.result = 0;
			a.dailyInfos = createExFebruary(29, 0, 0);

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 29; // 日付ごとの訪問実績データ数（閏年○）
				it.dailyInfos.eachWithIndex {info, j ->
					def d = e.dailyInfos.get(j);
					assert info.date == d.date;
					assert info.dayOfWeek == d.dayOfWeek;
					assert info.dayOfWeekLabel == d.dayOfWeekLabel;
					assert info.total == d.total;
					assert info.plan == d.plan;
					assert info.result == d.result;
				}
			}
		}

		/**
		 * planInfoList：得意先ごとの検索結果リスト
		 * 検索年月：2015年2月（閏年×）
		 * 検索結果：日付ごとのデータ（各日１件）が計画と実績に設定されていること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_平年の2月を検索した場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2015;
			param.month = 2;
			param.tanto = "ytj061";

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld";
			a.plan = 28;
			a.result = 28;
			a.dailyInfos = createExFebruary(28, 1, 1);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other";
			a.plan = 0;
			a.result = 28;
			a.dailyInfos = createExFebruary(28, 0, 1);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld";
			a.plan = 28;
			a.result = 0;
			a.dailyInfos = createExFebruary(28, 1, 0);

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld";
			a.plan = 0;
			a.result = 0;
			a.dailyInfos = createExFebruary(28, 0, 0);

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 28; // 日付ごとの訪問実績データ数（閏年×）
				it.dailyInfos.eachWithIndex {info, j ->
					def d = e.dailyInfos.get(j);
					assert info.date == d.date;
					assert info.dayOfWeek == d.dayOfWeek;
					assert info.dayOfWeekLabel == d.dayOfWeekLabel;
					assert info.total == d.total;
					assert info.plan == d.plan;
					assert info.result == d.result;
				}
			}
		}

		/**
		 * モードについて：担当得意先に紐づく訪問が
		 * 計画あり・実績ありの場合は「planHeld」（訪問の中に１件以上存在すること）
		 * 計画あり・実績なしの場合は「noHeld」
		 * 計画なし・実績ありの場合は「other」
		 * 計画なし・実績なしの場合は「noHeld」がモードに設定される
		 * */
		@DBUnit
		def "得意先のモードが訪問状況によって設定される"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj061";

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 4; // 訪問実績1日×4件＋計画のみ1件
			a.result = 4; // 訪問実績1日×4件＋実績のみ1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 3; // 実績のみ1日×3件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 3; // 計画のみ1日×3件
			a.result = 0;

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld"; // 実績なし
			a.plan = 0;
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * ytj041の訪問得意先が存在する場合
		 * ytj041の担当訪問と同行訪問が取得できること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_部門に紐づく担当者の場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj041"; // YTJ部門太郎1

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 3; // 担当1件＋同行2件
			a.result = 3; // 担当1件＋同行2件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 2; // 担当1件＋同行1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 2; // 担当1件＋同行1件
			a.result = 0;

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld"; // 実績なし
			a.plan = 0;
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * ytj031の訪問得意先が存在する場合
		 * ytj031の担当訪問と同行訪問が取得できること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_販社に紐づく担当者の場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj031"; // YTJ販社太郎1

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 3; // 担当1件＋同行2件
			a.result = 3; // 担当1件＋同行2件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 2; // 担当1件＋同行1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 2; // 担当1件＋同行1件
			a.result = 0;

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld"; // 実績なし
			a.plan = 0;
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * ytj01の訪問得意先が存在する場合
		 * ytj01の担当訪問と同行訪問が取得できること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_全社に紐づく担当者の場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj01"; // YTJ全社太郎

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 3; // 担当1件＋同行2件
			a.result = 3; // 担当1件＋同行2件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 2; // 担当1件＋同行1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 2; // 担当1件＋同行1件
			a.result = 0;

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績なし";
			a.accountId = "9000000001";
			a.mode = "noHeld"; // 実績なし
			a.plan = 0;
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 4; // 担当得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * 訪問得意先に登録していない場合でも、
		 * 同行者として記録された訪問計画・実績が存在する場合は、訪問に紐づく得意先が取得できること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_同行者として登録された場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj041"; // YTJ部門太郎1

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "得意先_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 3; // 担当1件＋同行2件
			a.result = 3; // 担当1件＋同行2件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 2; // 担当1件＋同行1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "得意先_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 2; // 担当1件＋同行1件
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 3; // 同行者として記録された訪問に紐づく得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * 案件に紐づいた訪問が存在する場合、その訪問に紐づく得意先が取得できること
		 * */
		@DBUnit
		def "modelにplanInfoListが設定される_関連先が案件の訪問を含む場合"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj01"; // YTJ全社太郎

			def expected = [:];

			def a = new AccountRow();
			a.accountName = "案件_計画あり・実績あり";
			a.accountId = "2000000001";
			a.mode = "planHeld"; // 計画あり・実績あり
			a.plan = 3; // 担当1件＋同行2件
			a.result = 3; // 担当1件＋同行2件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "案件_計画なし・実績あり";
			a.accountId = "2000000003";
			a.mode = "other"; // その他
			a.plan = 0;
			a.result = 2; // 担当1件＋同行1件

			expected[a.accountId] = a;

			a = new AccountRow();
			a.accountName = "案件_計画あり・実績なし";
			a.accountId = "2000000005";
			a.mode = "noHeld"; // 実績なし
			a.plan = 2; // 担当1件＋同行1件
			a.result = 0;

			expected[a.accountId] = a;

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 3; // 得意先数
			model.get("planInfoList").eachWithIndex { it, i ->
				def e = expected.get(it.accountId)
				assert it.accountName == e.accountName;
				assert it.accountId == e.accountId;
				assert it.mode == e.mode;
				assert it.plan == e.plan;
				assert it.result == e.result;
				assert it.dailyInfos.size() == 31; // 日付ごとの訪問実績データ数
			}
		}

		/**
		 * 訪問得意先に登録されていない場合
		 * 担当外得意先に担当として訪問した計画・実績に紐づく得意先は、得意先としてカウントされない
		 * */
		@DBUnit
		def "担当外得意先に担当として訪問した得意先はplanInfoListに設定されない"() {
			setup:
			def param = new SearchCondition();
			param.year = 2016;
			param.month = 8;
			param.tanto = "ytj061"; // YTJセールス太郎1

			when:
			sut.find(param, model, principal);

			then:
			model.get("planInfoList").size() == 0; // 得意先としてカウントされないため０件
		}
	}

	/**
	 * header部分のexpectedを生成する
	 * */
	static Map<Integer, String> createHeaderExpected() {
		Map<Integer, DailyCallInfo> expected = new TreeMap<>();

		DailyCallInfo info = new DailyCallInfo();
		info.date = 0;
		info.dayOfWeek = 0;
		info.dayOfWeekLabel = null;
		info.total = true;
		info.plan = 0;
		info.result = 0;

		expected.put(0, info);

		int dayOfWeek = 2;
		Map<Integer, String> map = new TreeMap<>();
		map.put(1, "日");
		map.put(2, "月");
		map.put(3, "火");
		map.put(4, "水");
		map.put(5, "木");
		map.put(6, "金");
		map.put(7, "土");

		for (int i = 1; i <= 31; i++) {
			info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = dayOfWeek;
			info.dayOfWeekLabel = map.get(dayOfWeek);
			info.total = false;
			info.plan = 0;
			info.result = 0;
			expected.put(i, info);

			if (dayOfWeek != 7) {
				dayOfWeek++;
			} else {
				dayOfWeek = 1;
			}
		}
		return expected;
	}

	/**
	 * 得意先：2000000001に紐づく日付ごとの計画実績Expectedを生成する
	 * */
	static List<DailyCallInfo> createEx2000000001() {
		List<DailyCallInfo> list = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			DailyCallInfo info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = 0;
			info.dayOfWeekLabel = null;
			info.total = false;
			if (i == 1 || i == 25) {
				info.plan = 2;
				info.result = 2;
			} else if (i == 5 || i == 10 || i == 15) {
				info.plan = 3;
				info.result = 3;
			} else {
				info.plan = 1;
				info.result = 1;
			}
			list.add(info);
		}
		return list;
	}

	/**
	 * 得意先：2000000003に紐づく日付ごとの計画実績Expectedを生成する
	 * */
	static List<DailyCallInfo> createEx2000000003() {
		List<DailyCallInfo> list = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			DailyCallInfo info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = 0;
			info.dayOfWeekLabel = null;
			info.total = false;
			info.plan = 0;
			if (i == 17 || i == 26) {
				info.result = 2;
			} else if (i == 5) {
				info.result = 3;
			} else {
				info.result = 1;
			}
			list.add(info);
		}
		return list;
	}

	/**
	 * 得意先：2000000005に紐づく日付ごとの計画実績Expectedを生成する
	 * */
	static List<DailyCallInfo> createEx2000000005() {
		List<DailyCallInfo> list = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			DailyCallInfo info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = 0;
			info.dayOfWeekLabel = null;
			info.total = false;
			if (i == 12 || i == 16) {
				info.plan = 2;
			} else if (i == 26) {
				info.plan = 3;
			} else {
				info.plan = 1;
			}
			info.result = 0;
			list.add(info);
		}
		return list;
	}

	/**
	 * 得意先：9000000001に紐づく日付ごとの計画実績Expectedを生成する
	 * */
	static List<DailyCallInfo> createEx9000000001() {
		List<DailyCallInfo> list = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			DailyCallInfo info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = 0;
			info.dayOfWeekLabel = null;
			info.total = false;
			info.plan = 0;
			info.result = 0;
			list.add(info);
		}
		return list;
	}

	/**
	 * 得意先：２月の日付ごとの計画実績Expectedを生成する
	 * */
	static List<DailyCallInfo> createExFebruary(int dates, int plan, int result) {
		List<DailyCallInfo> list = new ArrayList<>();
		for (int i = 1; i <= dates; i++) {
			DailyCallInfo info = new DailyCallInfo();
			info.date = i;
			info.dayOfWeek = 0;
			info.dayOfWeekLabel = null;
			info.total = false;
			info.plan = plan;
			info.result = result;
			list.add(info);
		}
		return list;
	}
}
