import geb.report.CompositeReporter
import geb.report.ScreenshotReporter

import org.openqa.selenium.chrome.ChromeDriver

// 環境にあったChromeDriverを以下からダウントードして指定して下さい。
// https://code.google.com/p/selenium/wiki/ChromeDriver
System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe")

driver = { return new ChromeDriver() }

autoClearCookies = false