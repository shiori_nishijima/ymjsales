-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: yrcsfa
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_janus_replicate`
--

DROP TABLE IF EXISTS `_janus_replicate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_janus_replicate` (
  `ＨａａＳ契約番号` varchar(255) DEFAULT NULL,
  `顧客番号` varchar(255) DEFAULT NULL,
  `契・氏名会社名カナ` varchar(255) DEFAULT NULL,
  `契・氏名会社名１` varchar(255) DEFAULT NULL,
  `契・氏名会社名２` varchar(255) DEFAULT NULL,
  `契・担当部課名` varchar(255) DEFAULT NULL,
  `契・郵便番号` varchar(255) DEFAULT NULL,
  `契・住所１` varchar(255) DEFAULT NULL,
  `契・住所２` varchar(255) DEFAULT NULL,
  `契・住所３` varchar(255) DEFAULT NULL,
  `契・連絡先電話番号` varchar(255) DEFAULT NULL,
  `契・連絡先ＦＡＸ` varchar(255) DEFAULT NULL,
  `契・連絡先携帯` varchar(255) DEFAULT NULL,
  `契・法個人区分` varchar(255) DEFAULT NULL,
  `契・Ｅメールアドレス` varchar(255) DEFAULT NULL,
  `請求先番号` varchar(255) DEFAULT NULL,
  `請・氏名会社名カナ` varchar(255) DEFAULT NULL,
  `請・氏名会社名１` varchar(255) DEFAULT NULL,
  `請・氏名会社名２` varchar(255) DEFAULT NULL,
  `請・担当部課名` varchar(255) DEFAULT NULL,
  `請・郵便番号` varchar(255) DEFAULT NULL,
  `請・住所１` varchar(255) DEFAULT NULL,
  `請・住所２` varchar(255) DEFAULT NULL,
  `請・住所３` varchar(255) DEFAULT NULL,
  `請・連絡先電話番号` varchar(255) DEFAULT NULL,
  `請・連絡先ＦＡＸ` varchar(255) DEFAULT NULL,
  `請・連絡先携帯` varchar(255) DEFAULT NULL,
  `請・担当者名` varchar(255) DEFAULT NULL,
  `締日群番号` varchar(255) DEFAULT NULL,
  `支払方法区分` varchar(255) DEFAULT NULL,
  `預金者名カナ` varchar(255) DEFAULT NULL,
  `銀行払い情報・金融機関コード` varchar(255) DEFAULT NULL,
  `銀行払い情報・店舗コード` varchar(255) DEFAULT NULL,
  `銀行払い情報・預金種目` varchar(255) DEFAULT NULL,
  `銀行払い情報・口座番号` varchar(255) DEFAULT NULL,
  `郵便局払い情報・通帳記号` varchar(255) DEFAULT NULL,
  `郵便局払い情報・通帳番号` varchar(255) DEFAULT NULL,
  `クレジット払い情報・クレ会社コード` varchar(255) DEFAULT NULL,
  `クレジット払い情報・会員番号` varchar(255) DEFAULT NULL,
  `クレジット払い情報・クレジット有効期間` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_janus_replicate`
--

LOCK TABLES `_janus_replicate` WRITE;
/*!40000 ALTER TABLE `_janus_replicate` DISABLE KEYS */;
/*!40000 ALTER TABLE `_janus_replicate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_janus_replicate_service`
--

DROP TABLE IF EXISTS `_janus_replicate_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_janus_replicate_service` (
  `ＪＡＮＵＳ契約番号` varchar(255) NOT NULL,
  `ＨａａＳ契約番号` varchar(255) DEFAULT NULL,
  `サービス商品コード` varchar(255) DEFAULT NULL,
  `顧客番号` varchar(255) DEFAULT NULL,
  `請求先番号` varchar(255) DEFAULT NULL,
  `登録年月日` varchar(255) DEFAULT NULL,
  `契約年月日` varchar(255) DEFAULT NULL,
  `解約年月日` varchar(255) DEFAULT NULL,
  `国内代理店コード` varchar(255) DEFAULT NULL,
  `国際代理店コード` varchar(255) DEFAULT NULL,
  `国内支払停止印` varchar(255) DEFAULT NULL,
  `国際支払停止印` varchar(255) DEFAULT NULL,
  `国内営業コード` varchar(255) DEFAULT NULL,
  `国際営業コード` varchar(255) DEFAULT NULL,
  `企業番号` varchar(255) DEFAULT NULL,
  `事業所番号` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ＪＡＮＵＳ契約番号`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_janus_replicate_service`
--

LOCK TABLES `_janus_replicate_service` WRITE;
/*!40000 ALTER TABLE `_janus_replicate_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `_janus_replicate_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` varchar(36) NOT NULL COMMENT '企業CD',
  `name` varchar(150) DEFAULT NULL COMMENT '企業名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` varchar(36) DEFAULT NULL COMMENT '更新ユーザID',
  `created_by` varchar(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（音声／携帯組織）',
  `account_type` varchar(25) DEFAULT NULL COMMENT 'BP系フラグ',
  `industry` varchar(25) DEFAULT NULL COMMENT '業種名称',
  `annual_revenue` varchar(25) DEFAULT NULL COMMENT '最新期売上高（単位：百万）',
  `phone_fax` varchar(25) DEFAULT NULL COMMENT 'ファックス（未使用）',
  `billing_address_street` varchar(150) DEFAULT NULL COMMENT '番地（住所3）',
  `billing_address_city` varchar(100) DEFAULT NULL COMMENT '市区町村（住所2）',
  `billing_address_state` varchar(100) DEFAULT NULL COMMENT '都道府県（住所1）',
  `billing_address_postalcode` varchar(20) DEFAULT NULL COMMENT '郵便番号',
  `billing_address_country` varchar(255) DEFAULT NULL COMMENT '国（未使用）',
  `rating` varchar(25) DEFAULT NULL COMMENT '評点',
  `phone_office` varchar(25) DEFAULT NULL COMMENT '会社電話',
  `phone_alternate` varchar(25) DEFAULT NULL COMMENT 'その他電話',
  `website` varchar(255) DEFAULT NULL COMMENT 'WEBサイト',
  `ownership` varchar(100) DEFAULT NULL COMMENT '代表者',
  `employees` varchar(10) DEFAULT NULL COMMENT '従業員',
  `ticker_symbol` varchar(10) DEFAULT NULL COMMENT '証券CD',
  `shipping_address_street` varchar(150) DEFAULT NULL COMMENT '出荷先番地（未使用）',
  `shipping_address_city` varchar(100) DEFAULT NULL COMMENT '出荷先市区町村（未使用）',
  `shipping_address_state` varchar(100) DEFAULT NULL COMMENT '出荷先都道府県（未使用）',
  `shipping_address_postalcode` varchar(20) DEFAULT NULL COMMENT '出荷先郵便番号（未使用）',
  `shipping_address_country` varchar(255) DEFAULT NULL COMMENT '出荷先国（未使用）',
  `parent_id` varchar(36) DEFAULT NULL COMMENT '親企業番号',
  `sic_code` varchar(10) DEFAULT NULL COMMENT '業界CD',
  `campaign_id` varchar(36) DEFAULT NULL COMMENT 'キャンペーンID（未使用）',
  `tdb_comp_cd` varchar(36) DEFAULT NULL COMMENT '帝国DB企業番号',
  `spm_comp_cd` varchar(36) DEFAULT NULL COMMENT '登録時企業CD',
  `orgunit_voice_cd` varchar(36) DEFAULT NULL COMMENT '音声／携帯組織CD',
  `orgunit_voice_name` varchar(255) DEFAULT NULL COMMENT '音声／携帯組織名',
  `orgunit_data_cd` varchar(36) DEFAULT NULL COMMENT 'データ組織CD',
  `orgunit_data_name` varchar(255) DEFAULT NULL COMMENT 'データ組織名',
  `name_kana` varchar(255) DEFAULT NULL COMMENT '企業名称カナ',
  `orgunit_voice_market` varchar(36) DEFAULT NULL COMMENT '音声／携帯組織マーケット',
  `orgunit_data_market` varchar(36) DEFAULT NULL COMMENT 'データ組織マーケット',
  `assigned_user_data_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（データ組織）',
  `kensaku_corp_kana` varchar(34) DEFAULT NULL COMMENT '検索用企業名カナ',
  `kensaku_corp_kana_zen` varchar(68) DEFAULT NULL COMMENT '検索用企業名カナ全角',
  `kensaku_corp_name` varchar(70) DEFAULT NULL COMMENT '検索用企業名',
  `disable_flg` varchar(1) NOT NULL COMMENT '有効無効フラグ',
  `disable_reason` varchar(2) DEFAULT NULL COMMENT '無効理由',
  `gappei_corp_bng` varchar(6) DEFAULT NULL COMMENT '合併企業番号',
  `orgunit_mobile_cd` varchar(36) DEFAULT NULL COMMENT 'モバイル組織CD',
  `orgunit_mobile_name` varchar(256) DEFAULT NULL COMMENT 'モバイル組織名',
  `orgunit_mobile_market` varchar(36) DEFAULT NULL COMMENT 'モバイル組織マーケット',
  `assigned_user_mobile_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（モバイル組織）',
  `orgunit_phs_cd` varchar(36) DEFAULT NULL COMMENT 'PHS組織CD',
  `orgunit_phs_name` varchar(256) DEFAULT NULL COMMENT 'PHS組織名',
  `orgunit_phs_market` varchar(36) DEFAULT NULL COMMENT 'PHS組織マーケット',
  `assigned_user_phs_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（PHS組織）',
  `orgunit_main_cd` varchar(36) DEFAULT NULL COMMENT '企業責任組織CD',
  `orgunit_main_name` varchar(256) DEFAULT NULL COMMENT '企業責任組織名',
  `assigned_user_main_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（企業責任組織）',
  `kensaku_corp_name2` varchar(70) DEFAULT NULL,
  `common_corp_cd` varchar(9) DEFAULT NULL,
  `group_addr_name_eng` varchar(80) DEFAULT NULL,
  `all_zip_cd` varchar(10) DEFAULT NULL,
  `country_cd` varchar(3) DEFAULT NULL,
  `city_name_eng` varchar(80) DEFAULT NULL,
  `district_name_eng` varchar(200) DEFAULT NULL,
  `local_company_name_eng` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tdb_comp_cd` (`tdb_comp_cd`),
  KEY `idx_spm_comp_cd` (`spm_comp_cd`),
  KEY `idx_assigned_user_id` (`assigned_user_id`),
  KEY `idx_assigned_user_data_id` (`assigned_user_data_id`),
  KEY `idx_assigned_user_mobile_id` (`assigned_user_mobile_id`),
  KEY `idx_accnt_parent_id` (`parent_id`),
  KEY `idx_billing_address_city` (`billing_address_city`),
  KEY `idx_billing_address_state` (`billing_address_state`),
  KEY `idx_name` (`name`),
  KEY `idx_name_kana` (`name_kana`),
  KEY `idx_industry` (`industry`),
  KEY `idx_disable_flg` (`disable_flg`),
  KEY `idx_kensaku_corp_name` (`kensaku_corp_name`),
  KEY `idx_assigned_user_phs_id` (`assigned_user_phs_id`) USING BTREE,
  KEY `idx_assigned_user_main_id` (`assigned_user_main_id`) USING BTREE,
  KEY `idx_kensaku_corp_name2` (`kensaku_corp_name2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企業';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_audit`
--

DROP TABLE IF EXISTS `accounts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_audit`
--

LOCK TABLES `accounts_audit` WRITE;
/*!40000 ALTER TABLE `accounts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_bugs`
--

DROP TABLE IF EXISTS `accounts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_bugs`
--

LOCK TABLES `accounts_bugs` WRITE;
/*!40000 ALTER TABLE `accounts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_cases`
--

DROP TABLE IF EXISTS `accounts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_cases`
--

LOCK TABLES `accounts_cases` WRITE;
/*!40000 ALTER TABLE `accounts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_contacts`
--

DROP TABLE IF EXISTS `accounts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `contact_id` varchar(36) DEFAULT NULL COMMENT '企業担当者CD',
  `account_id` varchar(36) DEFAULT NULL COMMENT '企業CD',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_contacts`
--

LOCK TABLES `accounts_contacts` WRITE;
/*!40000 ALTER TABLE `accounts_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_opportunities`
--

DROP TABLE IF EXISTS `accounts_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `opportunity_id` varchar(36) DEFAULT NULL COMMENT '案件CD',
  `account_id` varchar(36) DEFAULT NULL COMMENT '企業CD',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_opportunity_id` (`opportunity_id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_opportunities`
--

LOCK TABLES `accounts_opportunities` WRITE;
/*!40000 ALTER TABLE `accounts_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_actions`
--

DROP TABLE IF EXISTS `acl_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_actions`
--

LOCK TABLES `acl_actions` WRITE;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles`
--

DROP TABLE IF EXISTS `acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles`
--

LOCK TABLES `acl_roles` WRITE;
/*!40000 ALTER TABLE `acl_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_actions`
--

DROP TABLE IF EXISTS `acl_roles_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_actions`
--

LOCK TABLES `acl_roles_actions` WRITE;
/*!40000 ALTER TABLE `acl_roles_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_users`
--

DROP TABLE IF EXISTS `acl_roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_users`
--

LOCK TABLES `acl_roles_users` WRITE;
/*!40000 ALTER TABLE `acl_roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) NOT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_book`
--

LOCK TABLES `address_book` WRITE;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apachelog`
--

DROP TABLE IF EXISTS `apachelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apachelog` (
  `logdatetime` date DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `transferedbyte` varchar(10) DEFAULT NULL,
  `logdate` date DEFAULT NULL,
  `loghour` varchar(2) DEFAULT NULL,
  `X_Forwarded_For` varchar(15) DEFAULT NULL,
  `method` varchar(10) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `querystring` varchar(2000) DEFAULT NULL,
  `useragent` varchar(250) DEFAULT NULL,
  `webap` varchar(10) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `action` varchar(50) DEFAULT NULL,
  `loadtime` date DEFAULT NULL,
  `temp` varchar(3) DEFAULT NULL,
  KEY `idx_logdate` (`logdate`),
  KEY `idx_hour` (`loghour`),
  KEY `idx_module` (`module`),
  KEY `idx_action` (`action`),
  KEY `idx_path` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8 COMMENT='apachelog to db'
/*!50100 PARTITION BY RANGE (YEAR(LOGDATETIME))
(PARTITION P2008 VALUES LESS THAN (2009) ENGINE = InnoDB,
 PARTITION P2009 VALUES LESS THAN (2010) ENGINE = InnoDB,
 PARTITION P2010 VALUES LESS THAN (2011) ENGINE = InnoDB,
 PARTITION P2011 VALUES LESS THAN (2012) ENGINE = InnoDB,
 PARTITION PNOW VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apachelog`
--

LOCK TABLES `apachelog` WRITE;
/*!40000 ALTER TABLE `apachelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `apachelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_smt_mst_login_account`
--

DROP TABLE IF EXISTS `biz_smt_mst_login_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_smt_mst_login_account` (
  `ACCOUNT_ID` varchar(15) NOT NULL,
  `TANTO_CD` varchar(15) DEFAULT NULL,
  `STR_KOJIN_ID` varchar(20) DEFAULT NULL,
  `ROLE_CD` varchar(4) DEFAULT NULL,
  `ACS_ACCOUNT` varchar(15) DEFAULT NULL,
  `ACCESS_FROM_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACCESS_TO_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_MEMO` varchar(255) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_smt_mst_login_account`
--

LOCK TABLES `biz_smt_mst_login_account` WRITE;
/*!40000 ALTER TABLE `biz_smt_mst_login_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_smt_mst_login_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_tb_authentication`
--

DROP TABLE IF EXISTS `biz_tb_authentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_tb_authentication` (
  `ACCOUNT_ID` varchar(15) NOT NULL DEFAULT '',
  `ERROR_COUNT` int(11) DEFAULT NULL,
  `AVAILABLE_FLG` int(11) DEFAULT NULL,
  `ADMIN_FLG` int(11) DEFAULT NULL,
  `LOCK_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LAST_LOGIN_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REGIST_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REGIST_USER` varchar(20) DEFAULT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_USER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_tb_authentication`
--

LOCK TABLES `biz_tb_authentication` WRITE;
/*!40000 ALTER TABLE `biz_tb_authentication` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_tb_authentication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_tb_goods_mst`
--

DROP TABLE IF EXISTS `biz_tb_goods_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_tb_goods_mst` (
  `GOODS_ID` varchar(45) NOT NULL,
  `GOODS_CD` varchar(45) DEFAULT NULL,
  `ITM_LVL4_ID` varchar(30) DEFAULT NULL,
  `GOODS_NM` varchar(150) DEFAULT NULL,
  `MAKER_NM` varchar(255) DEFAULT NULL,
  `MAKER_NM_ENG` varchar(255) DEFAULT NULL,
  `BRAND_NM` varchar(255) DEFAULT NULL,
  `BRAND_NM_SHORT` varchar(255) DEFAULT NULL,
  `BRAND_NM_ENG` varchar(255) DEFAULT NULL,
  `COLOR_NM` varchar(255) DEFAULT NULL,
  `STD_COLOR_NM` varchar(255) DEFAULT NULL,
  `MODEL_CD` varchar(120) DEFAULT NULL,
  `MODEL_NM` varchar(255) DEFAULT NULL,
  `SERVICE_GEN_CD` varchar(6) DEFAULT NULL,
  `MS_TYP_NM` varchar(150) DEFAULT NULL,
  `BTRY_CATLOG_ITM_CD` varchar(6) DEFAULT NULL,
  `BTRY_NM` varchar(100) DEFAULT NULL,
  `ITM_EFFECT_DT_FROM` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ITM_EFFECT_DT_TO` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `STD_SET_FLG` varchar(1) DEFAULT NULL,
  `STD_SET_CD` varchar(6) DEFAULT NULL,
  `UTY_CD` varchar(3) DEFAULT NULL,
  `REGIST_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REGIST_USER` varchar(20) DEFAULT NULL,
  `UPDATE_DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATE_USER` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`GOODS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_tb_goods_mst`
--

LOCK TABLES `biz_tb_goods_mst` WRITE;
/*!40000 ALTER TABLE `biz_tb_goods_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_tb_goods_mst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `priority` varchar(25) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs_audit`
--

DROP TABLE IF EXISTS `bugs_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs_audit`
--

LOCK TABLES `bugs_audit` WRITE;
/*!40000 ALTER TABLE `bugs_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls`
--

DROP TABLE IF EXISTS `calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls` (
  `id` char(36) NOT NULL COMMENT '活動CD',
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '活動件名',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新ユーザID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT '担当者',
  `duration_hours` int(2) DEFAULT NULL COMMENT '時間（未使用）',
  `duration_minutes` int(2) DEFAULT NULL COMMENT '分（未使用）',
  `date_start` datetime DEFAULT NULL COMMENT '活動日時(from)',
  `date_end` date DEFAULT NULL COMMENT '終了日',
  `parent_type` varchar(25) DEFAULT NULL COMMENT '関連先Type',
  `status` varchar(25) NOT NULL DEFAULT 'Planned' COMMENT 'ステータス',
  `direction` varchar(25) DEFAULT NULL COMMENT 'インポート調査',
  `parent_id` char(36) DEFAULT NULL COMMENT '関連先ID',
  `reminder_time` int(4) DEFAULT '-1' COMMENT 'リマインダー時間(未使用）',
  `outlook_id` varchar(255) DEFAULT NULL COMMENT 'Outlook ID(未使用）',
  `google_event_id` varchar(100) DEFAULT NULL,
  `google_attendee_mails` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_status` (`status`),
  KEY `idx_assigned_user_id` (`assigned_user_id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls`
--

LOCK TABLES `calls` WRITE;
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_contacts`
--

DROP TABLE IF EXISTS `calls_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `call_id` varchar(36) DEFAULT NULL COMMENT '活動ID',
  `contact_id` varchar(36) DEFAULT NULL COMMENT '企業担当者ID',
  `required` varchar(1) DEFAULT '1' COMMENT '（未使用）',
  `accept_status` varchar(25) DEFAULT 'none' COMMENT '（未使用２）',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_contacts`
--

LOCK TABLES `calls_contacts` WRITE;
/*!40000 ALTER TABLE `calls_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_cstm`
--

DROP TABLE IF EXISTS `calls_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_cstm` (
  `id_c` char(36) NOT NULL COMMENT '活動CD',
  `account_id_c` varchar(36) DEFAULT NULL COMMENT '企業（ダミー）',
  `contact_id_c` varchar(36) DEFAULT NULL COMMENT '活動先ID(企業担当者ID)',
  `contact_name_c` varchar(255) DEFAULT NULL COMMENT '企業担当者（ダミー）',
  `next_step_c` varchar(100) DEFAULT NULL COMMENT '次ステップ(不使用)',
  `op_corp_c` varchar(100) DEFAULT NULL COMMENT '商談先(不使用)',
  `com_free_c` text COMMENT '内容詳細',
  `pam_name_c` varchar(25) DEFAULT NULL COMMENT 'PAM活動日付',
  `pam_sv_num_c` int(4) DEFAULT NULL COMMENT '開通着手回線数',
  `companion_c` varchar(150) DEFAULT NULL,
  `time_end_c` time NOT NULL DEFAULT '23:59:59' COMMENT '活動時間(TO)',
  `division_c` varchar(100) NOT NULL DEFAULT 'ヒアリング' COMMENT '活動区分',
  `googlecal_id_c` varchar(255) DEFAULT NULL COMMENT 'GカレンダーURL',
  `googlecal_published_c` varchar(25) DEFAULT NULL COMMENT 'Gカレンダー予定作成日時',
  `googlecal_updated_c` varchar(25) DEFAULT NULL COMMENT 'Gカレンダー更新日時',
  `googlecal_link_edit_c` varchar(255) DEFAULT NULL COMMENT 'Gカレンダー更新URL',
  `googlecal_author_c` varchar(100) DEFAULT NULL COMMENT 'Gカレンダー予定作成者',
  `googlecal_email_c` varchar(100) DEFAULT NULL COMMENT 'Gカレンダー予定作成者メールアドレス',
  `contact_title_c` varchar(100) DEFAULT NULL COMMENT '活動先職位',
  `contact_department_c` varchar(255) DEFAULT NULL COMMENT '活動先部署名',
  `google_event_id` text,
  `google_attendee_mails` text,
  `google_html_link_id` text,
  PRIMARY KEY (`id_c`),
  KEY `idx_account_id_c` (`account_id_c`),
  KEY `idx_contact_id_c` (`contact_id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_cstm`
--

LOCK TABLES `calls_cstm` WRITE;
/*!40000 ALTER TABLE `calls_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_leads`
--

DROP TABLE IF EXISTS `calls_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_cal` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_leads`
--

LOCK TABLES `calls_leads` WRITE;
/*!40000 ALTER TABLE `calls_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_listviewer`
--

DROP TABLE IF EXISTS `calls_listviewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_listviewer` (
  `id` varchar(36) NOT NULL COMMENT '活動履歴ID',
  `name` varchar(150) DEFAULT NULL COMMENT '活動件名',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `created_by` varchar(12) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` varchar(12) DEFAULT NULL COMMENT '活動担当者ID',
  `mast_organizationunit_id` varchar(3) DEFAULT NULL COMMENT '本部',
  `mast_organizationunit2_id` varchar(3) DEFAULT NULL COMMENT '統括部',
  `mast_organizationunit3_id` varchar(3) DEFAULT NULL COMMENT '部',
  `mast_organizationunit4_id` varchar(3) DEFAULT NULL COMMENT '課',
  `sales_staff_code` varchar(12) DEFAULT NULL COMMENT '活動担当者CD',
  `sales_staff_name` varchar(62) DEFAULT NULL COMMENT '活動担当者姓名',
  `sales_group_code` varchar(21) DEFAULT NULL COMMENT '活動担当者グループCD',
  `sales_group_name` varchar(128) DEFAULT NULL COMMENT '活動担当者グループ名',
  `date_start_fmt` date DEFAULT NULL COMMENT '訪問日付(From)',
  `date_start_fmt2` datetime DEFAULT NULL COMMENT '訪問日時(From)',
  `call_name` varchar(150) DEFAULT NULL COMMENT '活動件名',
  `parent_type_desc` varchar(12) DEFAULT NULL COMMENT '関連先',
  `account_id` varchar(6) DEFAULT NULL COMMENT '企業番号',
  `account_name` varchar(150) DEFAULT NULL COMMENT '企業名',
  `opportunity_id` varchar(36) DEFAULT NULL COMMENT '案件CD',
  `anken_id_c` varchar(20) DEFAULT NULL COMMENT '案件ID',
  `opportunity_name` varchar(255) DEFAULT NULL COMMENT '案件名',
  `status` varchar(25) NOT NULL DEFAULT 'Planned' COMMENT '活動ステータス',
  `com_free_c` text COMMENT '活動内容',
  `com_free_c20` varchar(20) DEFAULT NULL COMMENT '活動内容20文字',
  `date_entered_fmt` date DEFAULT NULL COMMENT '活動作成日付',
  `date_entered_fmt2` datetime DEFAULT NULL COMMENT '活動作成日時',
  `enter_staff_code` varchar(12) DEFAULT NULL COMMENT '活動作成者CD',
  `enter_staff_name` varchar(62) DEFAULT NULL COMMENT '活動作成者姓名',
  `date_modified_fmt` date DEFAULT NULL COMMENT '活動最終更新日付',
  `date_modified_fmt2` datetime DEFAULT NULL COMMENT '活動最終更新日時',
  `update_staff_code` varchar(12) DEFAULT NULL COMMENT '活動更新者CD',
  `update_staff_name` varchar(62) DEFAULT NULL COMMENT '活動更新者姓名',
  `update_group_code` varchar(21) DEFAULT NULL COMMENT '活動更新者グループCD',
  `update_group_name` varchar(128) DEFAULT NULL COMMENT '活動更新者グループ名',
  `pam_name_c` varchar(25) DEFAULT NULL COMMENT 'PAM活動内容',
  `pam_sv_num_c` int(4) DEFAULT NULL COMMENT '開通着手回線数',
  `companion_c` varchar(255) DEFAULT NULL COMMENT '社内同行者',
  `time_end_c` time DEFAULT NULL COMMENT '訪問日時(to)',
  `division_c` varchar(100) NOT NULL DEFAULT 'ヒアリング' COMMENT '活動区分',
  `contact_id_c` varchar(36) DEFAULT NULL COMMENT '活動先担当者CD',
  `contact_first_name` varchar(100) DEFAULT NULL COMMENT '活動先担当者名',
  `contact_last_name` varchar(100) DEFAULT NULL COMMENT '活動先担当者姓',
  `contact_name` varchar(202) DEFAULT NULL COMMENT '活動先担当者姓名',
  `contact_title_c` varchar(100) DEFAULT NULL COMMENT '活動先職位',
  `contact_department_c` varchar(255) DEFAULT NULL COMMENT '活動先部署',
  `outlook_id` varchar(255) DEFAULT NULL COMMENT '活動場所',
  PRIMARY KEY (`id`),
  KEY `idx_organizationunit` (`mast_organizationunit_id`,`mast_organizationunit2_id`,`mast_organizationunit3_id`,`mast_organizationunit4_id`),
  KEY `idx_date_start_fmt` (`date_start_fmt`),
  KEY `idx_companion_c` (`companion_c`),
  KEY `idx_sales_staff_code` (`sales_staff_code`),
  KEY `idx_status` (`status`),
  KEY `idx_division_c` (`division_c`),
  KEY `idx_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活動ビューア';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_listviewer`
--

LOCK TABLES `calls_listviewer` WRITE;
/*!40000 ALTER TABLE `calls_listviewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_listviewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_listviewer_audit`
--

DROP TABLE IF EXISTS `calls_listviewer_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_listviewer_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  KEY `idx_calls_listviewer_primary` (`id`),
  KEY `idx_calls_listviewer_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_listviewer_audit`
--

LOCK TABLES `calls_listviewer_audit` WRITE;
/*!40000 ALTER TABLE `calls_listviewer_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_listviewer_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_users`
--

DROP TABLE IF EXISTS `calls_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `call_id` varchar(36) DEFAULT NULL COMMENT '活動CD',
  `user_id` varchar(36) DEFAULT NULL COMMENT 'ユーザCD',
  `required` varchar(1) DEFAULT '1' COMMENT '必須項目(未使用)',
  `accept_status` varchar(25) DEFAULT 'none' COMMENT 'ステータス',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_users`
--

LOCK TABLES `calls_users` WRITE;
/*!40000 ALTER TABLE `calls_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_log`
--

DROP TABLE IF EXISTS `campaign_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(25) DEFAULT NULL,
  `activity_type` varchar(25) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_log`
--

LOCK TABLES `campaign_log` WRITE;
/*!40000 ALTER TABLE `campaign_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_trkrs`
--

DROP TABLE IF EXISTS `campaign_trkrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_trkrs`
--

LOCK TABLES `campaign_trkrs` WRITE;
/*!40000 ALTER TABLE `campaign_trkrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_trkrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(25) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns`
--

LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns_audit`
--

DROP TABLE IF EXISTS `campaigns_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns_audit`
--

LOCK TABLES `campaigns_audit` WRITE;
/*!40000 ALTER TABLE `campaigns_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` varchar(36) NOT NULL DEFAULT '' COMMENT 'ケースCD',
  `name` varchar(255) DEFAULT NULL COMMENT 'ケース名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` varchar(36) DEFAULT NULL COMMENT '更新者（ユーザCD）',
  `created_by` varchar(36) DEFAULT NULL COMMENT '作成者（ユーザCD）',
  `description` text COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` varchar(36) DEFAULT NULL COMMENT 'アサイン先（ユーザCD）',
  `case_number` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ケース番号',
  `type` varchar(255) DEFAULT NULL COMMENT 'タイプ',
  `status` varchar(25) DEFAULT NULL COMMENT 'ステータス（未使用）',
  `priority` varchar(25) DEFAULT NULL COMMENT '優先度（未使用）',
  `resolution` text COMMENT '解決（未使用）',
  `work_log` text COMMENT '（未使用）',
  `account_id` varchar(36) DEFAULT NULL COMMENT '企業番号',
  `website` varchar(200) DEFAULT NULL COMMENT '外部リンク1',
  `website2` varchar(200) DEFAULT NULL COMMENT '外部リンク',
  `website3` varchar(200) DEFAULT NULL COMMENT '外部リンク3',
  `website4` varchar(200) DEFAULT NULL COMMENT '外部リンク4（未使用）',
  `website5` varchar(200) DEFAULT NULL COMMENT '外部リンク5（未使用）',
  `doc_id_c` varchar(12) DEFAULT NULL COMMENT '問合せ番号（未使用）',
  `potential` double(11,0) DEFAULT NULL COMMENT 'ポテンシャル回線数',
  `amount_month` double(11,0) DEFAULT NULL COMMENT '利用金額（月）',
  `career` varchar(200) DEFAULT NULL COMMENT 'キャリア',
  `contract_exp_end_date` datetime DEFAULT NULL COMMENT '契約満了予定日',
  `mobile_mou` double(11,0) DEFAULT NULL COMMENT 'モバイルMOU',
  `mobile_packets` double(11,0) DEFAULT NULL COMMENT 'モバイルパケット数',
  `voice_pbx_vender` varchar(200) DEFAULT NULL COMMENT '音声PBXベンダー',
  `voice_pbx_model` varchar(200) DEFAULT NULL COMMENT '音声PBX機種名',
  `voice_pbx_switch_date` datetime DEFAULT NULL COMMENT '音声PBX交換予定日',
  `data_exploitation_nw` varchar(200) DEFAULT NULL COMMENT 'データ現利用NWサービス',
  `data_center_use` varchar(25) DEFAULT NULL COMMENT 'データセンター利用',
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2515 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger cases_trigger before UPDATE on cases FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.account_id != new.account_id) THEN 
      INSERT INTO cases_audit values(UUID(), old.id, current, new.modified_user_id, 'account_id', 'relate',old.account_id, new.account_id, null, null);
    END IF;
    IF (old.assigned_user_id != new.assigned_user_id) THEN 
      INSERT INTO cases_audit values(UUID(), old.id, current, new.modified_user_id, 'assigned_user_id', 'relate', old.assigned_user_id, new.assigned_user_id, null, null);
    END IF;
    IF (old.name != new.name) THEN 
      INSERT INTO cases_audit values(UUID(), old.id, current, new.modified_user_id, 'name','name', old.name,new.name, null, null);
    END IF;
    IF (old.priority != new.priority) THEN 
      INSERT INTO cases_audit values(UUID(), old.id, current, new.modified_user_id, 'priority', 'enum', old.priority, new.priority, null, null);
    END IF;
    IF (old.status != new.status) THEN 
      INSERT INTO cases_audit values(UUID(), old.id, current, new.modified_user_id, 'status','enum', old.status, new.status, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cases_audit`
--

DROP TABLE IF EXISTS `cases_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_audit`
--

LOCK TABLES `cases_audit` WRITE;
/*!40000 ALTER TABLE `cases_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_bugs`
--

DROP TABLE IF EXISTS `cases_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_bugs`
--

LOCK TABLES `cases_bugs` WRITE;
/*!40000 ALTER TABLE `cases_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` char(36) NOT NULL COMMENT 'コメントID',
  `name` varchar(25) DEFAULT NULL COMMENT '名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` varchar(200) DEFAULT NULL COMMENT 'コメント内容',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先',
  `parent_id` char(36) DEFAULT NULL COMMENT '関連先ID',
  `parent_type` varchar(25) DEFAULT NULL COMMENT '関連先Type',
  `is_gadget` tinyint(1) DEFAULT '0' COMMENT 'Gガジェットフラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `category` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `value` text,
  PRIMARY KEY (`category`,`name`),
  KEY `idx_config_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` char(36) NOT NULL COMMENT '企業担当者CD',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者（ユーザCD）',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者（ユーザCD）',
  `description` text COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先（ユーザCD）',
  `salutation` varchar(5) DEFAULT NULL COMMENT '敬称',
  `first_name` varchar(100) DEFAULT NULL COMMENT '名',
  `last_name` varchar(100) DEFAULT NULL COMMENT '姓',
  `title` varchar(100) DEFAULT NULL COMMENT '職位',
  `department` varchar(255) DEFAULT NULL COMMENT '部署',
  `do_not_call` tinyint(1) DEFAULT '0' COMMENT '電話不可',
  `phone_home` varchar(25) DEFAULT NULL COMMENT '自宅電話',
  `phone_mobile` varchar(25) DEFAULT NULL COMMENT '携帯電話',
  `phone_work` varchar(25) DEFAULT NULL COMMENT '会社電話',
  `phone_other` varchar(25) DEFAULT NULL COMMENT 'その他電話',
  `phone_fax` varchar(25) DEFAULT NULL COMMENT 'FAX',
  `primary_address_street` varchar(150) DEFAULT NULL COMMENT '住所 番地',
  `primary_address_city` varchar(250) DEFAULT NULL COMMENT '住所 市区町村',
  `primary_address_state` varchar(100) DEFAULT NULL COMMENT '住所 都道府県',
  `primary_address_postalcode` varchar(20) DEFAULT NULL COMMENT '住所 郵便番号',
  `primary_address_country` varchar(255) DEFAULT NULL COMMENT '住所 国',
  `alt_address_street` varchar(150) DEFAULT NULL COMMENT 'その他住所 番地',
  `alt_address_city` varchar(100) DEFAULT NULL COMMENT 'その他住所 市区町村',
  `alt_address_state` varchar(100) DEFAULT NULL COMMENT 'その他住所 都道府県',
  `alt_address_postalcode` varchar(20) DEFAULT NULL COMMENT 'その他住所 郵便番号',
  `alt_address_country` varchar(255) DEFAULT NULL COMMENT 'その他住所 国',
  `assistant` varchar(75) DEFAULT NULL COMMENT 'アシスタント',
  `assistant_phone` varchar(25) DEFAULT NULL COMMENT 'アシスタント電話番号',
  `lead_source` varchar(100) DEFAULT NULL COMMENT 'リードソース',
  `reports_to_id` char(36) DEFAULT NULL COMMENT '上司',
  `birthdate` date DEFAULT NULL COMMENT '誕生日（未使用）',
  `portal_name` varchar(255) DEFAULT NULL COMMENT 'portal_name（未使用）',
  `portal_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'portal_active（未使用）',
  `portal_app` varchar(255) DEFAULT NULL COMMENT 'portal_app（未使用）',
  `campaign_id` char(36) DEFAULT NULL COMMENT 'キャンペーンCD（未使用）',
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger contacts_trigger before UPDATE on contacts FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.assigned_user_id != new.assigned_user_id) THEN 
      INSERT INTO contacts_audit values(UUID(), old.id, current, new.modified_user_id, 'assigned_user_id', 'relate', old.assigned_user_id, new.assigned_user_id, null, null);
    END IF;
    IF (old.do_not_call != new.do_not_call) THEN 
      INSERT INTO contacts_audit values(UUID(), old.id, current, new.modified_user_id, 'do_not_call', 'bool', old.do_not_call, new.do_not_call, null, null);
    END IF;
    IF (old.phone_work != new.phone_work) THEN 
      INSERT INTO contacts_audit values(UUID(), old.id, current, new.modified_user_id, 'phone_work', 'phone', old.phone_work, new.phone_work, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `contacts_audit`
--

DROP TABLE IF EXISTS `contacts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_audit`
--

LOCK TABLES `contacts_audit` WRITE;
/*!40000 ALTER TABLE `contacts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_bugs`
--

DROP TABLE IF EXISTS `contacts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_bugs`
--

LOCK TABLES `contacts_bugs` WRITE;
/*!40000 ALTER TABLE `contacts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_cases`
--

DROP TABLE IF EXISTS `contacts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `contact_id` varchar(36) DEFAULT NULL COMMENT '企業担当者CD',
  `case_id` varchar(36) DEFAULT NULL COMMENT 'ケースCD',
  `contact_role` varchar(50) DEFAULT NULL COMMENT '（未使用？）',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_cases`
--

LOCK TABLES `contacts_cases` WRITE;
/*!40000 ALTER TABLE `contacts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_users`
--

DROP TABLE IF EXISTS `contacts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_users`
--

LOCK TABLES `contacts_users` WRITE;
/*!40000 ALTER TABLE `contacts_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) NOT NULL,
  `symbol` varchar(36) NOT NULL,
  `iso4217` varchar(3) NOT NULL,
  `conversion_rate` double NOT NULL DEFAULT '0',
  `status` varchar(25) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboards`
--

DROP TABLE IF EXISTS `dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboards` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_dashboard_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboards`
--

LOCK TABLES `dashboards` WRITE;
/*!40000 ALTER TABLE `dashboards` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_revisions`
--

DROP TABLE IF EXISTS `document_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `file_ext` varchar(25) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(25) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_revisions`
--

LOCK TABLES `document_revisions` WRITE;
/*!40000 ALTER TABLE `document_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `document_name` varchar(255) NOT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(25) DEFAULT NULL,
  `subcategory_id` varchar(25) DEFAULT NULL,
  `status_id` varchar(25) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addr_bean_rel`
--

DROP TABLE IF EXISTS `email_addr_bean_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(25) NOT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addr_bean_rel`
--

LOCK TABLES `email_addr_bean_rel` WRITE;
/*!40000 ALTER TABLE `email_addr_bean_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_addr_bean_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addresses`
--

DROP TABLE IF EXISTS `email_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `email_address_caps` varchar(255) NOT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addresses`
--

LOCK TABLES `email_addresses` WRITE;
/*!40000 ALTER TABLE `email_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_cache`
--

DROP TABLE IF EXISTS `email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_cache` (
  `ie_id` char(36) NOT NULL,
  `mbox` varchar(60) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime NOT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned NOT NULL,
  `imap_uid` int(10) unsigned NOT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) NOT NULL,
  `flagged` tinyint(4) NOT NULL,
  `answered` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `seen` tinyint(4) NOT NULL,
  `draft` tinyint(4) NOT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_cache`
--

LOCK TABLES `email_cache` WRITE;
/*!40000 ALTER TABLE `email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing`
--

DROP TABLE IF EXISTS `email_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(25) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing`
--

LOCK TABLES `email_marketing` WRITE;
/*!40000 ALTER TABLE `email_marketing` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing_prospect_lists`
--

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing_prospect_lists`
--

LOCK TABLES `email_marketing_prospect_lists` WRITE;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `text_only` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailman`
--

DROP TABLE IF EXISTS `emailman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailman`
--

LOCK TABLES `emailman` WRITE;
/*!40000 ALTER TABLE `emailman` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `intent` varchar(25) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_beans`
--

DROP TABLE IF EXISTS `emails_beans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_beans` (
  `id` varchar(36) NOT NULL,
  `email_id` varchar(36) DEFAULT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `bean_module` varchar(36) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_beans`
--

LOCK TABLES `emails_beans` WRITE;
/*!40000 ALTER TABLE `emails_beans` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_beans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_email_addr_rel`
--

DROP TABLE IF EXISTS `emails_email_addr_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_email_addr_rel`
--

LOCK TABLES `emails_email_addr_rel` WRITE;
/*!40000 ALTER TABLE `emails_email_addr_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_email_addr_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_text`
--

DROP TABLE IF EXISTS `emails_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_text` (
  `email_id` varchar(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_text`
--

LOCK TABLES `emails_text` WRITE;
/*!40000 ALTER TABLE `emails_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` char(36) NOT NULL COMMENT 'お気に入りID',
  `user_id` char(36) DEFAULT NULL COMMENT '登録ユーザID',
  `parent_id` char(36) DEFAULT NULL COMMENT '関連先ID',
  `parent_type` varchar(25) DEFAULT NULL COMMENT '関連先Type',
  `date_entered` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_feed_name` (`title`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds`
--

LOCK TABLES `feeds` WRITE;
/*!40000 ALTER TABLE `feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields_meta_data`
--

DROP TABLE IF EXISTS `fields_meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT '',
  `ext2` varchar(255) DEFAULT '',
  `ext3` varchar(255) DEFAULT '',
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields_meta_data`
--

LOCK TABLES `fields_meta_data` WRITE;
/*!40000 ALTER TABLE `fields_meta_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `fields_meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` varchar(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `content` blob,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `assigned_user_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cont_owner_id_and_name` (`assigned_user_id`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) NOT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders`
--

LOCK TABLES `folders` WRITE;
/*!40000 ALTER TABLE `folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_rel`
--

DROP TABLE IF EXISTS `folders_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) NOT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_folders_rel_folder_id` (`folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_rel`
--

LOCK TABLES `folders_rel` WRITE;
/*!40000 ALTER TABLE `folders_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_subscriptions`
--

DROP TABLE IF EXISTS `folders_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_subscriptions`
--

LOCK TABLES `folders_subscriptions` WRITE;
/*!40000 ALTER TABLE `folders_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `if_str_assignment`
--

DROP TABLE IF EXISTS `if_str_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `if_str_assignment` (
  `kojin_id` varchar(20) DEFAULT NULL,
  `div_cd` varchar(8) DEFAULT NULL,
  `kaisha_cd` varchar(4) DEFAULT NULL,
  `post_kbn` varchar(4) DEFAULT NULL,
  `proceeding_ratio` double DEFAULT NULL,
  `title_cd` varchar(4) DEFAULT NULL,
  `title_nm` varchar(79) DEFAULT NULL,
  `yakuwari_cd` varchar(4) DEFAULT NULL,
  `yakuwari_nm` varchar(79) DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `if_str_assignment`
--

LOCK TABLES `if_str_assignment` WRITE;
/*!40000 ALTER TABLE `if_str_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `if_str_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `if_str_assignment_old`
--

DROP TABLE IF EXISTS `if_str_assignment_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `if_str_assignment_old` (
  `kojin_id` varchar(20) DEFAULT NULL,
  `div_cd` varchar(8) DEFAULT NULL,
  `kaisha_cd` varchar(4) DEFAULT NULL,
  `post_kbn` varchar(4) DEFAULT NULL,
  `proceeding_ratio` double DEFAULT NULL,
  `title_cd` varchar(4) DEFAULT NULL,
  `title_nm` varchar(79) DEFAULT NULL,
  `yakuwari_cd` varchar(4) DEFAULT NULL,
  `yakuwari_nm` varchar(79) DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `if_str_assignment_old`
--

LOCK TABLES `if_str_assignment_old` WRITE;
/*!40000 ALTER TABLE `if_str_assignment_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `if_str_assignment_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `if_str_assignment_sum`
--

DROP TABLE IF EXISTS `if_str_assignment_sum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `if_str_assignment_sum` (
  `kojin_id` varchar(20) DEFAULT NULL,
  `div_cd` varchar(341) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `if_str_assignment_sum`
--

LOCK TABLES `if_str_assignment_sum` WRITE;
/*!40000 ALTER TABLE `if_str_assignment_sum` DISABLE KEYS */;
/*!40000 ALTER TABLE `if_str_assignment_sum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `if_str_assignment_sum_old`
--

DROP TABLE IF EXISTS `if_str_assignment_sum_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `if_str_assignment_sum_old` (
  `kojin_id` varchar(20) DEFAULT NULL,
  `div_cd` varchar(341) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `if_str_assignment_sum_old`
--

LOCK TABLES `if_str_assignment_sum_old` WRITE;
/*!40000 ALTER TABLE `if_str_assignment_sum_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `if_str_assignment_sum_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iframes`
--

DROP TABLE IF EXISTS `iframes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iframes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `placement` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cont_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iframes`
--

LOCK TABLES `iframes` WRITE;
/*!40000 ALTER TABLE `iframes` DISABLE KEYS */;
/*!40000 ALTER TABLE `iframes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `ifview_eida_accounts`
--

DROP TABLE IF EXISTS `ifview_eida_accounts`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_accounts` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `ACCOUNT_TYPE` tinyint NOT NULL,
  `INDUSTRY` tinyint NOT NULL,
  `ANNUAL_REVENUE` tinyint NOT NULL,
  `PHONE_FAX` tinyint NOT NULL,
  `BILLING_ADDRESS_STREET` tinyint NOT NULL,
  `BILLING_ADDRESS_CITY` tinyint NOT NULL,
  `BILLING_ADDRESS_STATE` tinyint NOT NULL,
  `BILLING_ADDRESS_POSTALCODE` tinyint NOT NULL,
  `BILLING_ADDRESS_COUNTRY` tinyint NOT NULL,
  `RATING` tinyint NOT NULL,
  `PHONE_OFFICE` tinyint NOT NULL,
  `PHONE_ALTERNATE` tinyint NOT NULL,
  `WEBSITE` tinyint NOT NULL,
  `OWNERSHIP` tinyint NOT NULL,
  `EMPLOYEES` tinyint NOT NULL,
  `TICKER_SYMBOL` tinyint NOT NULL,
  `SHIPPING_ADDRESS_STREET` tinyint NOT NULL,
  `SHIPPING_ADDRESS_CITY` tinyint NOT NULL,
  `SHIPPING_ADDRESS_STATE` tinyint NOT NULL,
  `SHIPPING_ADDRESS_POSTALCODE` tinyint NOT NULL,
  `SHIPPING_ADDRESS_COUNTRY` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `SIC_CODE` tinyint NOT NULL,
  `CAMPAIGN_ID` tinyint NOT NULL,
  `TDB_COMP_CD` tinyint NOT NULL,
  `SPM_COMP_CD` tinyint NOT NULL,
  `ORGUNIT_VOICE_CD` tinyint NOT NULL,
  `ORGUNIT_VOICE_NAME` tinyint NOT NULL,
  `ORGUNIT_DATA_CD` tinyint NOT NULL,
  `ORGUNIT_DATA_NAME` tinyint NOT NULL,
  `NAME_KANA` tinyint NOT NULL,
  `ORGUNIT_VOICE_MARKET` tinyint NOT NULL,
  `ORGUNIT_DATA_MARKET` tinyint NOT NULL,
  `ASSIGNED_USER_DATA_ID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_accounts_cases`
--

DROP TABLE IF EXISTS `ifview_eida_accounts_cases`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_cases`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_accounts_cases` (
  `ID` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `CASE_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_accounts_contacts`
--

DROP TABLE IF EXISTS `ifview_eida_accounts_contacts`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_contacts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_accounts_contacts` (
  `ID` tinyint NOT NULL,
  `CONTACT_ID` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_accounts_opportunities`
--

DROP TABLE IF EXISTS `ifview_eida_accounts_opportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_opportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_accounts_opportunities` (
  `ID` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_calls`
--

DROP TABLE IF EXISTS `ifview_eida_calls`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_calls` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `DURATION_HOURS` tinyint NOT NULL,
  `DURATION_MINUTES` tinyint NOT NULL,
  `DATE_START` tinyint NOT NULL,
  `DATE_END` tinyint NOT NULL,
  `PARENT_TYPE` tinyint NOT NULL,
  `STATUS` tinyint NOT NULL,
  `DIRECTION` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `REMINDER_TIME` tinyint NOT NULL,
  `OUTLOOK_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `ACCOUNT_ID_C` tinyint NOT NULL,
  `CONTACT_ID_C` tinyint NOT NULL,
  `CONTACT_NAME_C` tinyint NOT NULL,
  `NEXT_STEP_C` tinyint NOT NULL,
  `OP_CORP_C` tinyint NOT NULL,
  `COM_FREE_C` tinyint NOT NULL,
  `PAM_NAME_C` tinyint NOT NULL,
  `PAM_SV_NUM_C` tinyint NOT NULL,
  `COMPANION_C` tinyint NOT NULL,
  `TIME_END_C` tinyint NOT NULL,
  `DIVISION_C` tinyint NOT NULL,
  `GOOGLECAL_ID_C` tinyint NOT NULL,
  `GOOGLECAL_PUBLISHED_C` tinyint NOT NULL,
  `GOOGLECAL_UPDATED_C` tinyint NOT NULL,
  `GOOGLECAL_LINK_EDIT_C` tinyint NOT NULL,
  `GOOGLECAL_AUTHOR_C` tinyint NOT NULL,
  `GOOGLECAL_EMAIL_C` tinyint NOT NULL,
  `CONTACT_TITLE_C` tinyint NOT NULL,
  `CONTACT_DEPARTMENT_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_calls_contacts`
--

DROP TABLE IF EXISTS `ifview_eida_calls_contacts`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_contacts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_calls_contacts` (
  `ID` tinyint NOT NULL,
  `CALL_ID` tinyint NOT NULL,
  `CONTACT_ID` tinyint NOT NULL,
  `REQUIRED` tinyint NOT NULL,
  `ACCEPT_STATUS` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_calls_n`
--

DROP TABLE IF EXISTS `ifview_eida_calls_n`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_n`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_calls_n` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `DURATION_HOURS` tinyint NOT NULL,
  `DURATION_MINUTES` tinyint NOT NULL,
  `DATE_START` tinyint NOT NULL,
  `DATE_END` tinyint NOT NULL,
  `PARENT_TYPE` tinyint NOT NULL,
  `STATUS` tinyint NOT NULL,
  `DIRECTION` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `REMINDER_TIME` tinyint NOT NULL,
  `OUTLOOK_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `ACCOUNT_ID_C` tinyint NOT NULL,
  `CONTACT_ID_C` tinyint NOT NULL,
  `CONTACT_NAME_C` tinyint NOT NULL,
  `NEXT_STEP_C` tinyint NOT NULL,
  `OP_CORP_C` tinyint NOT NULL,
  `COM_FREE_C` tinyint NOT NULL,
  `PAM_NAME_C` tinyint NOT NULL,
  `PAM_SV_NUM_C` tinyint NOT NULL,
  `COMPANION_C` tinyint NOT NULL,
  `TIME_END_C` tinyint NOT NULL,
  `DIVISION_C` tinyint NOT NULL,
  `GOOGLECAL_ID_C` tinyint NOT NULL,
  `GOOGLECAL_PUBLISHED_C` tinyint NOT NULL,
  `GOOGLECAL_UPDATED_C` tinyint NOT NULL,
  `GOOGLECAL_LINK_EDIT_C` tinyint NOT NULL,
  `GOOGLECAL_AUTHOR_C` tinyint NOT NULL,
  `GOOGLECAL_EMAIL_C` tinyint NOT NULL,
  `CONTACT_TITLE_C` tinyint NOT NULL,
  `CONTACT_DEPARTMENT_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_calls_no_text`
--

DROP TABLE IF EXISTS `ifview_eida_calls_no_text`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_no_text`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_calls_no_text` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `DURATION_HOURS` tinyint NOT NULL,
  `DURATION_MINUTES` tinyint NOT NULL,
  `DATE_START` tinyint NOT NULL,
  `DATE_END` tinyint NOT NULL,
  `PARENT_TYPE` tinyint NOT NULL,
  `STATUS` tinyint NOT NULL,
  `DIRECTION` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `REMINDER_TIME` tinyint NOT NULL,
  `OUTLOOK_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `ACCOUNT_ID_C` tinyint NOT NULL,
  `CONTACT_ID_C` tinyint NOT NULL,
  `CONTACT_NAME_C` tinyint NOT NULL,
  `NEXT_STEP_C` tinyint NOT NULL,
  `OP_CORP_C` tinyint NOT NULL,
  `COM_FREE_C` tinyint NOT NULL,
  `PAM_NAME_C` tinyint NOT NULL,
  `PAM_SV_NUM_C` tinyint NOT NULL,
  `COMPANION_C` tinyint NOT NULL,
  `TIME_END_C` tinyint NOT NULL,
  `DIVISION_C` tinyint NOT NULL,
  `GOOGLECAL_ID_C` tinyint NOT NULL,
  `GOOGLECAL_PUBLISHED_C` tinyint NOT NULL,
  `GOOGLECAL_UPDATED_C` tinyint NOT NULL,
  `GOOGLECAL_LINK_EDIT_C` tinyint NOT NULL,
  `GOOGLECAL_AUTHOR_C` tinyint NOT NULL,
  `GOOGLECAL_EMAIL_C` tinyint NOT NULL,
  `CONTACT_TITLE_C` tinyint NOT NULL,
  `CONTACT_DEPARTMENT_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_calls_users`
--

DROP TABLE IF EXISTS `ifview_eida_calls_users`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_calls_users` (
  `ID` tinyint NOT NULL,
  `CALL_ID` tinyint NOT NULL,
  `USER_ID` tinyint NOT NULL,
  `REQUIRED` tinyint NOT NULL,
  `ACCEPT_STATUS` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_contacts_cases`
--

DROP TABLE IF EXISTS `ifview_eida_contacts_cases`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_contacts_cases`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_contacts_cases` (
  `ID` tinyint NOT NULL,
  `CONTACT_ID` tinyint NOT NULL,
  `CASE_ID` tinyint NOT NULL,
  `CONTACT_ROLE` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_email_addr_bean_rel`
--

DROP TABLE IF EXISTS `ifview_eida_email_addr_bean_rel`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_email_addr_bean_rel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_email_addr_bean_rel` (
  `ID` tinyint NOT NULL,
  `EMAIL_ADDRESS_ID` tinyint NOT NULL,
  `BEAN_ID` tinyint NOT NULL,
  `BEAN_MODULE` tinyint NOT NULL,
  `PRIMARY_ADDRESS` tinyint NOT NULL,
  `REPLY_TO_ADDRESS` tinyint NOT NULL,
  `DATE_CREATED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_email_addresses`
--

DROP TABLE IF EXISTS `ifview_eida_email_addresses`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_email_addresses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_email_addresses` (
  `ID` tinyint NOT NULL,
  `EMAIL_ADDRESS` tinyint NOT NULL,
  `EMAIL_ADDRESS_CAPS` tinyint NOT NULL,
  `INVALID_EMAIL` tinyint NOT NULL,
  `OPT_OUT` tinyint NOT NULL,
  `DATE_CREATED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_leads`
--

DROP TABLE IF EXISTS `ifview_eida_leads`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_leads` (
  `ID` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `LEAD_LEAD_SOURCE` tinyint NOT NULL,
  `LEAD_TYPE` tinyint NOT NULL,
  `LEAD_STATUS` tinyint NOT NULL,
  `LEAD_ACCOUNT_NAME` tinyint NOT NULL,
  `LEAD_CLOSE_REASON` tinyint NOT NULL,
  `CLOSE_OPPORTUNITIES_ID` tinyint NOT NULL,
  `LEAD_SALE_ITEM_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_leads_accounts`
--

DROP TABLE IF EXISTS `ifview_eida_leads_accounts`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_accounts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_leads_accounts` (
  `ID` tinyint NOT NULL,
  `LEAD_ID` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_leads_contacts`
--

DROP TABLE IF EXISTS `ifview_eida_leads_contacts`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_contacts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_leads_contacts` (
  `ID` tinyint NOT NULL,
  `LEAD_ID` tinyint NOT NULL,
  `CONTACT_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_leads_opportunities`
--

DROP TABLE IF EXISTS `ifview_eida_leads_opportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_opportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_leads_opportunities` (
  `ID` tinyint NOT NULL,
  `LEAD_ID` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_mast_billiard`
--

DROP TABLE IF EXISTS `ifview_eida_mast_billiard`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_billiard`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_mast_billiard` (
  `id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `modified_user_id` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `assigned_user_id` tinyint NOT NULL,
  `billiard_maker` tinyint NOT NULL,
  `billiard_model` tinyint NOT NULL,
  `billiard_color` tinyint NOT NULL,
  `billiard_order` tinyint NOT NULL,
  `display_disabled` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_mast_organizationunit`
--

DROP TABLE IF EXISTS `ifview_eida_mast_organizationunit`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_organizationunit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_mast_organizationunit` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `ORDRNO` tinyint NOT NULL,
  `OGUSNM` tinyint NOT NULL,
  `OGUTID` tinyint NOT NULL,
  `OGUTCL` tinyint NOT NULL,
  `UPOGCD` tinyint NOT NULL,
  `MGSFCD` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_mast_serviceline`
--

DROP TABLE IF EXISTS `ifview_eida_mast_serviceline`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_serviceline`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_mast_serviceline` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `SVLSNM` tinyint NOT NULL,
  `SLGRCD` tinyint NOT NULL,
  `SLGRNM` tinyint NOT NULL,
  `ORDRNO` tinyint NOT NULL,
  `FCQTCL` tinyint NOT NULL,
  `FQFXFG` tinyint NOT NULL,
  `FMYMFG` tinyint NOT NULL,
  `TOYMFG` tinyint NOT NULL,
  `EVLNRT` tinyint NOT NULL,
  `DVMTFG` tinyint NOT NULL,
  `SVDTFG` tinyint NOT NULL,
  `FCQTNM` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `KEY_DRIVER_ORDRNO` tinyint NOT NULL,
  `LENDING_GUARANTEE_ID` tinyint NOT NULL,
  `LENDING_GUARANTEE_NAME` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opadd_partner`
--

DROP TABLE IF EXISTS `ifview_eida_opadd_partner`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opadd_partner`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opadd_partner` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `PARENT_TYPE` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `TEST_C` tinyint NOT NULL,
  `ACCOUNT_ID_C` tinyint NOT NULL,
  `ACCOUNT_NAME_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opadd_partnpportunities`
--

DROP TABLE IF EXISTS `ifview_eida_opadd_partnpportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opadd_partnpportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opadd_partnpportunities` (
  `ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `OPADD_PARTNER_IDA` tinyint NOT NULL,
  `OPPORTUNITIES_IDB` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opp_salessup`
--

DROP TABLE IF EXISTS `ifview_eida_opp_salessup`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_salessup`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opp_salessup` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `ROLE_PROPOSED` tinyint NOT NULL,
  `PS_SALES` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opp_salessupportunities_c`
--

DROP TABLE IF EXISTS `ifview_eida_opp_salessupportunities_c`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_salessupportunities_c`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opp_salessupportunities_c` (
  `ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `OPP_SALESS3AF5UNITIES_IDA` tinyint NOT NULL,
  `OPP_SALESS78CEALESSUP_IDB` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opp_se`
--

DROP TABLE IF EXISTS `ifview_eida_opp_se`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opp_se` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `STATUS` tinyint NOT NULL,
  `SEORDERACCURACY` tinyint NOT NULL,
  `PROJECTNAME` tinyint NOT NULL,
  `SESTRATEGICCLASSIFICATION` tinyint NOT NULL,
  `SEITEMSUMMARY` tinyint NOT NULL,
  `SALESTEMPNUM` tinyint NOT NULL,
  `SALESMONTHNUM` tinyint NOT NULL,
  `CONTRACTTERMMONTHLYBASIS` tinyint NOT NULL,
  `PSSALESTEMPNUM` tinyint NOT NULL,
  `PSSALESMONTHNUM` tinyint NOT NULL,
  `PSBUDGETSCHEDULE` tinyint NOT NULL,
  `CAUSEORDERDFAILURE` tinyint NOT NULL,
  `SPARE` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opp_se_audit`
--

DROP TABLE IF EXISTS `ifview_eida_opp_se_audit`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se_audit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opp_se_audit` (
  `ID` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `DATE_CREATED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `FIELD_NAME` tinyint NOT NULL,
  `DATA_TYPE` tinyint NOT NULL,
  `BEFORE_VALUE_STRING` tinyint NOT NULL,
  `AFTER_VALUE_STRING` tinyint NOT NULL,
  `BEFORE_VALUE_TEXT` tinyint NOT NULL,
  `AFTER_VALUE_TEXT` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opp_se_opportunities_c`
--

DROP TABLE IF EXISTS `ifview_eida_opp_se_opportunities_c`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se_opportunities_c`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opp_se_opportunities_c` (
  `ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `OPP_SE_OPP7A17UNITIES_IDA` tinyint NOT NULL,
  `OPP_SE_OPP1446SOPP_SE_IDB` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opportunities`
--

DROP TABLE IF EXISTS `ifview_eida_opportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opportunities` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `CAMPAIGN_ID` tinyint NOT NULL,
  `LEAD_SOURCE` tinyint NOT NULL,
  `AMOUNT` tinyint NOT NULL,
  `AMOUNT_USDOLLAR` tinyint NOT NULL,
  `CURRENCY_ID` tinyint NOT NULL,
  `DATE_CLOSED` tinyint NOT NULL,
  `NEXT_STEP` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `PROBABILITY` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `SUP_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `AAA_C` tinyint NOT NULL,
  `OP_COMPE_C` tinyint NOT NULL,
  `OP_COMPNAME_MEMO_C` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_DATA_ORGAN_C` tinyint NOT NULL,
  `OP_DEP_DETAIL_C` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OP_ENDSV_CATE_C` tinyint NOT NULL,
  `OP_ENDSV_CLASS_C` tinyint NOT NULL,
  `OP_ENDSV_NUM_C` tinyint NOT NULL,
  `OP_ENDSV_UNIT_C` tinyint NOT NULL,
  `OP_ENDSVEND_MM_C` tinyint NOT NULL,
  `OP_ENDSVEND_YYYY_C` tinyint NOT NULL,
  `OP_ENDSVSTART_MM_C` tinyint NOT NULL,
  `OP_ENDSVSTART_YYYY_C` tinyint NOT NULL,
  `OP_NEWSV_CATE_C` tinyint NOT NULL,
  `OP_NEWSV_CLASS_C` tinyint NOT NULL,
  `OP_NEWSV_NUM_C` tinyint NOT NULL,
  `OP_NEWSV_UNIT_C` tinyint NOT NULL,
  `OP_NEWSVEND_MM_C` tinyint NOT NULL,
  `OP_NEWSVEND_YYYY_C` tinyint NOT NULL,
  `OP_NEWSVSTART_MM_C` tinyint NOT NULL,
  `OP_NEWSVSTART_YYYY_C` tinyint NOT NULL,
  `OP_ONSEIKEITAI_ORGAN_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_OWNER_C` tinyint NOT NULL,
  `OP_PARTNER_C` tinyint NOT NULL,
  `OP_SALES_MAIN_C` tinyint NOT NULL,
  `OP_SALES_SUPPORT_C` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STS_C` tinyint NOT NULL,
  `OP_SV_CATE_C` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_FORECAST_C` tinyint NOT NULL,
  `OPPORTUNITY_NUMBER_C` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `DOC_ID_C` tinyint NOT NULL,
  `PROPOSAL1_C` tinyint NOT NULL,
  `PROPOSAL_PRICE1_C` tinyint NOT NULL,
  `DISCOUNT_RATE1_C` tinyint NOT NULL,
  `PROPOSAL2_C` tinyint NOT NULL,
  `PROPOSAL_PRICE2_C` tinyint NOT NULL,
  `DISCOUNT_RATE2_C` tinyint NOT NULL,
  `CLOSED_SITUATION_C` tinyint NOT NULL,
  `VICTORY_DEFEAT_CAUSE_C` tinyint NOT NULL,
  `VICTORY_DEFEAT_CAUSE_DETAIL_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `Y_INTEREST_C` tinyint NOT NULL,
  `Y_INTEREST_AREA_C` tinyint NOT NULL,
  `Y_NG_REASON_C` tinyint NOT NULL,
  `Y_AGENCY_C` tinyint NOT NULL,
  `Y_MEDIUM_C` tinyint NOT NULL,
  `Y_REMARKS_C` tinyint NOT NULL,
  `Y_POSTURL_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opportunities_audit`
--

DROP TABLE IF EXISTS `ifview_eida_opportunities_audit`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_audit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opportunities_audit` (
  `ID` tinyint NOT NULL,
  `PARENT_ID` tinyint NOT NULL,
  `DATE_CREATED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `FIELD_NAME` tinyint NOT NULL,
  `DATA_TYPE` tinyint NOT NULL,
  `BEFORE_VALUE_STRING` tinyint NOT NULL,
  `AFTER_VALUE_STRING` tinyint NOT NULL,
  `BEFORE_VALUE_TEXT` tinyint NOT NULL,
  `AFTER_VALUE_TEXT` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opportunities_no_text`
--

DROP TABLE IF EXISTS `ifview_eida_opportunities_no_text`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_no_text`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opportunities_no_text` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `CAMPAIGN_ID` tinyint NOT NULL,
  `LEAD_SOURCE` tinyint NOT NULL,
  `AMOUNT` tinyint NOT NULL,
  `AMOUNT_USDOLLAR` tinyint NOT NULL,
  `CURRENCY_ID` tinyint NOT NULL,
  `DATE_CLOSED` tinyint NOT NULL,
  `NEXT_STEP` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `PROBABILITY` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `SUP_ID` tinyint NOT NULL,
  `ID_C` tinyint NOT NULL,
  `AAA_C` tinyint NOT NULL,
  `OP_COMPE_C` tinyint NOT NULL,
  `OP_COMPNAME_MEMO_C` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_DATA_ORGAN_C` tinyint NOT NULL,
  `OP_DEP_DETAIL_C` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OP_ENDSV_CATE_C` tinyint NOT NULL,
  `OP_ENDSV_CLASS_C` tinyint NOT NULL,
  `OP_ENDSV_NUM_C` tinyint NOT NULL,
  `OP_ENDSV_UNIT_C` tinyint NOT NULL,
  `OP_ENDSVEND_MM_C` tinyint NOT NULL,
  `OP_ENDSVEND_YYYY_C` tinyint NOT NULL,
  `OP_ENDSVSTART_MM_C` tinyint NOT NULL,
  `OP_ENDSVSTART_YYYY_C` tinyint NOT NULL,
  `OP_NEWSV_CATE_C` tinyint NOT NULL,
  `OP_NEWSV_CLASS_C` tinyint NOT NULL,
  `OP_NEWSV_NUM_C` tinyint NOT NULL,
  `OP_NEWSV_UNIT_C` tinyint NOT NULL,
  `OP_NEWSVEND_MM_C` tinyint NOT NULL,
  `OP_NEWSVEND_YYYY_C` tinyint NOT NULL,
  `OP_NEWSVSTART_MM_C` tinyint NOT NULL,
  `OP_NEWSVSTART_YYYY_C` tinyint NOT NULL,
  `OP_ONSEIKEITAI_ORGAN_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_OWNER_C` tinyint NOT NULL,
  `OP_PARTNER_C` tinyint NOT NULL,
  `OP_SALES_MAIN_C` tinyint NOT NULL,
  `OP_SALES_SUPPORT_C` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STS_C` tinyint NOT NULL,
  `OP_SV_CATE_C` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_FORECAST_C` tinyint NOT NULL,
  `OPPORTUNITY_NUMBER_C` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `DOC_ID_C` tinyint NOT NULL,
  `PROPOSAL1_C` tinyint NOT NULL,
  `PROPOSAL_PRICE1_C` tinyint NOT NULL,
  `DISCOUNT_RATE1_C` tinyint NOT NULL,
  `PROPOSAL2_C` tinyint NOT NULL,
  `PROPOSAL_PRICE2_C` tinyint NOT NULL,
  `DISCOUNT_RATE2_C` tinyint NOT NULL,
  `CLOSED_SITUATION_C` tinyint NOT NULL,
  `VICTORY_DEFEAT_CAUSE_C` tinyint NOT NULL,
  `VICTORY_DEFEAT_CAUSE_DETAIL_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `Y_INTEREST_C` tinyint NOT NULL,
  `Y_INTEREST_AREA_C` tinyint NOT NULL,
  `Y_NG_REASON_C` tinyint NOT NULL,
  `Y_AGENCY_C` tinyint NOT NULL,
  `Y_MEDIUM_C` tinyint NOT NULL,
  `Y_REMARKS_C` tinyint NOT NULL,
  `Y_POSTURL_C` tinyint NOT NULL,
  `RESERVE1_C` tinyint NOT NULL,
  `RESERVE2_C` tinyint NOT NULL,
  `RESERVE3_C` tinyint NOT NULL,
  `RESERVE4_C` tinyint NOT NULL,
  `RESERVE5_C` tinyint NOT NULL,
  `RESERVE6_C` tinyint NOT NULL,
  `RESERVE7_C` tinyint NOT NULL,
  `RESERVE8_C` tinyint NOT NULL,
  `RESERVE9_C` tinyint NOT NULL,
  `RESERVE10_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_opportunities_users`
--

DROP TABLE IF EXISTS `ifview_eida_opportunities_users`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_opportunities_users` (
  `ID` tinyint NOT NULL,
  `USER_ID` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_sv_cancel`
--

DROP TABLE IF EXISTS `ifview_eida_sv_cancel`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_cancel`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_sv_cancel` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `CLASS` tinyint NOT NULL,
  `CATE` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `YYYY` tinyint NOT NULL,
  `MM` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `MONTH_NUM_VARIABLE_PROFITS` tinyint NOT NULL,
  `TEMP_NUM_VARIABLE_PROFITS` tinyint NOT NULL,
  `POSTED_MONTH_AGE` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `MONTH_AGE` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_sv_cancel_opportunities`
--

DROP TABLE IF EXISTS `ifview_eida_sv_cancel_opportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_cancel_opportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_sv_cancel_opportunities` (
  `ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `SV_CANCEL_IDA` tinyint NOT NULL,
  `OPPORTUNITIES_IDB` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_sv_suggest`
--

DROP TABLE IF EXISTS `ifview_eida_sv_suggest`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_suggest`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_sv_suggest` (
  `ID` tinyint NOT NULL,
  `NAME` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `CLASS` tinyint NOT NULL,
  `CATE` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `YYYY` tinyint NOT NULL,
  `MM` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `MONTH_NUM_VARIABLE_PROFITS` tinyint NOT NULL,
  `TEMP_NUM_VARIABLE_PROFITS` tinyint NOT NULL,
  `POSTED_MONTH_AGE` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `MONTH_AGE` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_sv_suggest_opportunities`
--

DROP TABLE IF EXISTS `ifview_eida_sv_suggest_opportunities`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_suggest_opportunities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_sv_suggest_opportunities` (
  `ID` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `SV_SUGGEST_IDA` tinyint NOT NULL,
  `OPPORTUNITIES_IDB` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `ifview_eida_users`
--

DROP TABLE IF EXISTS `ifview_eida_users`;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `ifview_eida_users` (
  `ID` tinyint NOT NULL,
  `USER_NAME` tinyint NOT NULL,
  `USER_HASH` tinyint NOT NULL,
  `AUTHENTICATE_ID` tinyint NOT NULL,
  `SUGAR_LOGIN` tinyint NOT NULL,
  `FIRST_NAME` tinyint NOT NULL,
  `LAST_NAME` tinyint NOT NULL,
  `REPORTS_TO_ID` tinyint NOT NULL,
  `IS_ADMIN` tinyint NOT NULL,
  `RECEIVE_NOTIFICATIONS` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `TITLE` tinyint NOT NULL,
  `DEPARTMENT` tinyint NOT NULL,
  `PHONE_HOME` tinyint NOT NULL,
  `PHONE_MOBILE` tinyint NOT NULL,
  `PHONE_WORK` tinyint NOT NULL,
  `PHONE_OTHER` tinyint NOT NULL,
  `PHONE_FAX` tinyint NOT NULL,
  `STATUS` tinyint NOT NULL,
  `ADDRESS_STREET` tinyint NOT NULL,
  `ADDRESS_CITY` tinyint NOT NULL,
  `ADDRESS_STATE` tinyint NOT NULL,
  `ADDRESS_COUNTRY` tinyint NOT NULL,
  `ADDRESS_POSTALCODE` tinyint NOT NULL,
  `USER_PREFERENCES` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `PORTAL_ONLY` tinyint NOT NULL,
  `EMPLOYEE_STATUS` tinyint NOT NULL,
  `MESSENGER_ID` tinyint NOT NULL,
  `MESSENGER_TYPE` tinyint NOT NULL,
  `IS_GROUP` tinyint NOT NULL,
  `SPM_STFCD` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `ORGUNIT_CD` tinyint NOT NULL,
  `ORGUNIT_NAME` tinyint NOT NULL,
  `GOOGLE_MMAIL_C` tinyint NOT NULL,
  `GOOGLE_MCALLS_C` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `import_maps`
--

DROP TABLE IF EXISTS `import_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(36) NOT NULL,
  `source` varchar(36) NOT NULL,
  `module` varchar(36) NOT NULL,
  `content` blob,
  `has_header` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) NOT NULL DEFAULT 'no',
  `enclosure` varchar(1) NOT NULL,
  `delimiter` varchar(1) NOT NULL,
  `default_values` blob,
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_maps`
--

LOCK TABLES `import_maps` WRITE;
/*!40000 ALTER TABLE `import_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email`
--

DROP TABLE IF EXISTS `inbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'Active',
  `server_url` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `email_password` varchar(100) NOT NULL,
  `port` int(5) NOT NULL,
  `service` varchar(50) NOT NULL,
  `mailbox` varchar(50) NOT NULL,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) NOT NULL DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email`
--

LOCK TABLES `inbound_email` WRITE;
/*!40000 ALTER TABLE `inbound_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_autoreply`
--

DROP TABLE IF EXISTS `inbound_email_autoreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `autoreplied_to` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_autoreply`
--

LOCK TABLES `inbound_email_autoreply` WRITE;
/*!40000 ALTER TABLE `inbound_email_autoreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_autoreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_cache_ts`
--

DROP TABLE IF EXISTS `inbound_email_cache_ts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_cache_ts`
--

LOCK TABLES `inbound_email_cache_ts` WRITE;
/*!40000 ALTER TABLE `inbound_email_cache_ts` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_cache_ts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(5) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(25) DEFAULT NULL,
  `phone_mobile` varchar(25) DEFAULT NULL,
  `phone_work` varchar(25) DEFAULT NULL,
  `phone_other` varchar(25) DEFAULT NULL,
  `phone_fax` varchar(25) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(25) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `lead_lead_source` varchar(128) DEFAULT NULL,
  `lead_type` varchar(128) DEFAULT NULL,
  `lead_status` varchar(128) DEFAULT NULL,
  `lead_account_name` varchar(150) DEFAULT NULL,
  `lead_close_reason` varchar(256) DEFAULT NULL,
  `close_opportunities_id` varchar(36) DEFAULT NULL,
  `codama_key` varchar(100) DEFAULT NULL,
  `close_date` varchar(20) DEFAULT NULL,
  `approach_date` varchar(20) DEFAULT NULL,
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger leads_trigger before UPDATE on leads FOR each row
BEGIN 
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.assigned_user_id != new.assigned_user_id) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'assigned_user_id', 'relate', old.assigned_user_id, new.assigned_user_id, null, null);
    END IF;
    IF (old.lead_account_name != new.lead_account_name) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'lead_account_name', 'varchar', old.lead_account_name, new.lead_account_name, null, null);
    END IF;
    IF (old.lead_lead_source != new.lead_lead_source) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'lead_lead_source', 'enum', old.lead_lead_source, new.lead_lead_source, null, null);
    END IF;
    IF (old.lead_close_reason != new.lead_close_reason) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'lead_close_reason', 'enum', old.lead_close_reason, new.lead_close_reason, null, null);
    END IF;
    IF (old.lead_status != new.lead_status) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'lead_status', 'enum', old.lead_status, new.lead_status, null, null);
    END IF;
    IF (old.lead_type != new.lead_type) THEN
      INSERT INTO leads_audit values(UUID(), old.id, current, new.modified_user_id, 'lead_type', 'enum', old.lead_type, new.lead_type, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `leads_accounts`
--

DROP TABLE IF EXISTS `leads_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_accounts` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `lead_id` varchar(36) DEFAULT NULL COMMENT 'リード_ID',
  `account_id` varchar(36) DEFAULT NULL COMMENT '企業_ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_lead_id` (`lead_id`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_lead_account` (`lead_id`,`account_id`),
  KEY `idx_oppid_del_accid` (`account_id`,`deleted`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_accounts`
--

LOCK TABLES `leads_accounts` WRITE;
/*!40000 ALTER TABLE `leads_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_audit`
--

DROP TABLE IF EXISTS `leads_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_audit`
--

LOCK TABLES `leads_audit` WRITE;
/*!40000 ALTER TABLE `leads_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_contacts`
--

DROP TABLE IF EXISTS `leads_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_contacts` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `lead_id` varchar(36) DEFAULT NULL COMMENT 'リード_ID',
  `contact_id` varchar(36) DEFAULT NULL COMMENT '企業担当者_ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_lead_id` (`lead_id`),
  KEY `idx_contact_id` (`contact_id`),
  KEY `idx_lead_contact` (`lead_id`,`contact_id`),
  KEY `idx_oppid_del_accid` (`contact_id`,`deleted`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_contacts`
--

LOCK TABLES `leads_contacts` WRITE;
/*!40000 ALTER TABLE `leads_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_cstm`
--

DROP TABLE IF EXISTS `leads_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_cstm` (
  `id_c` char(36) NOT NULL COMMENT 'リード_ID',
  `lead_sale_item_c` varchar(128) DEFAULT NULL,
  `lead_number_c` int(11) NOT NULL AUTO_INCREMENT COMMENT 'リードID生成用オートインクリメント（自動採番）',
  `lead_id_c` varchar(20) DEFAULT NULL COMMENT 'リードID（自動採番）',
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_c`),
  KEY `lead_number_c` (`lead_number_c`),
  KEY `idx_lead_id_c` (`lead_id_c`)
) ENGINE=InnoDB AUTO_INCREMENT=1012622 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_cstm`
--

LOCK TABLES `leads_cstm` WRITE;
/*!40000 ALTER TABLE `leads_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_cstm` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger leads_cstm_trigger before UPDATE  on leads_cstm FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.lead_sale_item_c != new.lead_sale_item_c) THEN
      INSERT INTO leads_audit values(UUID(), old.id_c, current, (SELECT leads.modified_user_id FROM leads left outer join leads_cstm on leads.id = leads_cstm.id_c where leads.id = old.id_c), 'lead_sale_item_c', 'multienum', old.lead_sale_item_c, new.lead_sale_item_c, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `leads_opportunities`
--

DROP TABLE IF EXISTS `leads_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_opportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `lead_id` varchar(36) DEFAULT NULL COMMENT 'リード_ID',
  `opportunity_id` varchar(36) DEFAULT NULL COMMENT '案件_ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_lead_id` (`lead_id`),
  KEY `idx_opportunity_id` (`opportunity_id`),
  KEY `idx_lead_opportunity` (`lead_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_opportunities`
--

LOCK TABLES `leads_opportunities` WRITE;
/*!40000 ALTER TABLE `leads_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linked_documents`
--

DROP TABLE IF EXISTS `linked_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linked_documents`
--

LOCK TABLES `linked_documents` WRITE;
/*!40000 ALTER TABLE `linked_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_info`
--

DROP TABLE IF EXISTS `log_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_info` (
  `id` char(36) NOT NULL COMMENT 'ログUUID_ID',
  `user_id` char(36) DEFAULT NULL COMMENT 'ログインユーザID',
  `user_tracking_code` char(36) DEFAULT NULL COMMENT 'ユーザ追跡コード',
  `last_login_timestamp` datetime DEFAULT NULL COMMENT 'ログインタイムスタンプ',
  `page` char(36) DEFAULT NULL COMMENT 'ページ',
  `event_timestampe` datetime DEFAULT NULL COMMENT 'イベント発生タイムスタンプ',
  `event_element_id` char(36) DEFAULT NULL COMMENT 'イベント対象エレメント',
  `event_data_id` char(36) DEFAULT NULL COMMENT 'イベント対象データ',
  `event_name` char(36) DEFAULT NULL COMMENT 'イベント名',
  `ua` char(255) DEFAULT NULL COMMENT 'ユーザエージェント',
  `error` text COMMENT 'エラーメッセージ',
  `date_entered` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_tracking_code` (`user_tracking_code`,`event_timestampe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_info`
--

LOCK TABLES `log_info` WRITE;
/*!40000 ALTER TABLE `log_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_billiard`
--

DROP TABLE IF EXISTS `mast_billiard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_billiard` (
  `id` char(36) NOT NULL COMMENT '機種コード',
  `name` varchar(255) NOT NULL COMMENT '機種コード(未使用)',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` text COMMENT '詳細(未使用)',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先(未使用)',
  `billiard_maker` varchar(20) DEFAULT NULL COMMENT 'メーカー',
  `billiard_model` varchar(20) DEFAULT NULL COMMENT 'モデル',
  `billiard_color` varchar(20) DEFAULT NULL COMMENT '色',
  `billiard_order` varchar(20) DEFAULT NULL COMMENT '表示順',
  `display_disabled` int(11) DEFAULT NULL COMMENT '非表示フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_billiard`
--

LOCK TABLES `mast_billiard` WRITE;
/*!40000 ALTER TABLE `mast_billiard` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_billiard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_billiard_audit`
--

DROP TABLE IF EXISTS `mast_billiard_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_billiard_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_billiard_audit`
--

LOCK TABLES `mast_billiard_audit` WRITE;
/*!40000 ALTER TABLE `mast_billiard_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_billiard_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_organizationunit`
--

DROP TABLE IF EXISTS `mast_organizationunit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_organizationunit` (
  `id` char(36) NOT NULL COMMENT '組織CD',
  `name` varchar(255) NOT NULL COMMENT '組織名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先（未使用）',
  `ordrno` int(11) DEFAULT NULL COMMENT '表示順番号',
  `ogusnm` varchar(50) DEFAULT NULL COMMENT '組織単位略称（未使用）',
  `ogutid` varchar(40) DEFAULT NULL COMMENT '組織単位ID',
  `ogutcl` int(11) DEFAULT NULL COMMENT '組織単位区分',
  `upogcd` int(11) DEFAULT NULL COMMENT '上位組織単位CD',
  `mgsfcd` varchar(11) DEFAULT NULL COMMENT '責任者社員番号',
  `div_cd` varchar(11) DEFAULT NULL COMMENT '人事部署コード',
  `div_nm` varchar(255) DEFAULT NULL COMMENT '部署名称',
  `div_nm_short` varchar(255) DEFAULT NULL COMMENT '部署略称',
  `up_div_cd` varchar(11) DEFAULT NULL COMMENT '上位人事部署コード',
  `mng_tanto_cd` varchar(15) DEFAULT NULL COMMENT '責任者社員番号',
  `mng_grptanto_cd` varchar(15) DEFAULT NULL COMMENT '責任者共通社員番号',
  `div_unit_kbn` varchar(40) DEFAULT NULL COMMENT '組織単位区分',
  `kaisha_cd` varchar(4) DEFAULT NULL COMMENT '会社区分コード',
  PRIMARY KEY (`id`),
  KEY `idx_ogusnm` (`ogusnm`),
  KEY `idx_ogutid` (`ogutid`(10)),
  KEY `idx_upogcd` (`upogcd`),
  KEY `idx_mgsfcd` (`mgsfcd`),
  KEY `idx_name` (`name`(60))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_organizationunit`
--

LOCK TABLES `mast_organizationunit` WRITE;
/*!40000 ALTER TABLE `mast_organizationunit` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_organizationunit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_organizationunit_audit`
--

DROP TABLE IF EXISTS `mast_organizationunit_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_organizationunit_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_organizationunit_audit`
--

LOCK TABLES `mast_organizationunit_audit` WRITE;
/*!40000 ALTER TABLE `mast_organizationunit_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_organizationunit_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_serviceline`
--

DROP TABLE IF EXISTS `mast_serviceline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_serviceline` (
  `id` char(36) NOT NULL COMMENT 'サービスラインCD',
  `name` varchar(255) NOT NULL COMMENT 'サービスライン名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先（未使用）',
  `svlsnm` varchar(60) DEFAULT NULL COMMENT 'サービスライン略称（未使用）',
  `slgrcd` varchar(4) DEFAULT NULL COMMENT 'サービスライングループCD',
  `slgrnm` varchar(100) DEFAULT NULL COMMENT 'サービスライングループ名称',
  `ordrno` int(11) DEFAULT NULL COMMENT '表示順番号',
  `fcqtcl` int(11) DEFAULT NULL COMMENT 'デフォルト予測数量区分',
  `fqfxfg` int(11) DEFAULT NULL COMMENT '予測数量区分固定フラグ（未使用）',
  `fmymfg` int(11) DEFAULT NULL COMMENT 'サービス開始年月要否フラグ（未使用）',
  `toymfg` int(11) DEFAULT NULL COMMENT 'サービス終了年月要否フラグ（未使用）',
  `evlnrt` double(13,2) DEFAULT NULL COMMENT '評価回線数算出係数',
  `dvmtfg` int(11) DEFAULT NULL COMMENT 'キードライバ換算フラグ',
  `svdtfg` double(15,4) DEFAULT NULL,
  `fcqtnm` varchar(20) DEFAULT NULL COMMENT 'デフォルト予測数量区分名称',
  `key_driver_id` varchar(4) DEFAULT NULL COMMENT 'キードライバーID',
  `key_driver_name` varchar(20) DEFAULT NULL COMMENT 'キードライバー名称',
  `key_driver_ordrno` int(11) DEFAULT NULL COMMENT 'キードライバー内訳表示順',
  `lending_guarantee_id` varchar(20) DEFAULT '900' COMMENT '与信グループID',
  `lending_guarantee_name` varchar(20) DEFAULT '音声以外' COMMENT '与信グループ名称',
  PRIMARY KEY (`id`),
  KEY `idx_ordrno` (`ordrno`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_serviceline`
--

LOCK TABLES `mast_serviceline` WRITE;
/*!40000 ALTER TABLE `mast_serviceline` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_serviceline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_serviceline_audit`
--

DROP TABLE IF EXISTS `mast_serviceline_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_serviceline_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_serviceline_audit`
--

LOCK TABLES `mast_serviceline_audit` WRITE;
/*!40000 ALTER TABLE `mast_serviceline_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_serviceline_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_serviceline_old`
--

DROP TABLE IF EXISTS `mast_serviceline_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_serviceline_old` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `svlsnm` varchar(60) DEFAULT NULL,
  `slgrcd` varchar(4) DEFAULT NULL,
  `slgrnm` varchar(100) DEFAULT NULL,
  `ordrno` int(11) DEFAULT NULL,
  `fcqtcl` int(11) DEFAULT NULL,
  `fqfxfg` int(11) DEFAULT NULL,
  `fmymfg` int(11) DEFAULT NULL,
  `toymfg` int(11) DEFAULT NULL,
  `evlnrt` int(11) DEFAULT NULL,
  `dvmtfg` int(11) DEFAULT NULL,
  `svdtfg` int(11) DEFAULT NULL,
  `fcqtnm` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_serviceline_old`
--

LOCK TABLES `mast_serviceline_old` WRITE;
/*!40000 ALTER TABLE `mast_serviceline_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_serviceline_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mast_skill`
--

DROP TABLE IF EXISTS `mast_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mast_skill` (
  `id` char(36) NOT NULL COMMENT '依頼案件種別CD',
  `name` varchar(255) NOT NULL COMMENT '依頼案件種別名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` varchar(36) DEFAULT NULL COMMENT '更新者',
  `created_by` varchar(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `category_cd` varchar(4) DEFAULT NULL COMMENT 'カテゴリCD',
  `category_nm` varchar(100) DEFAULT NULL COMMENT 'カテゴリ名称',
  `orderno` int(11) DEFAULT NULL COMMENT '表示順番号',
  `cal_coefficient` double(13,2) DEFAULT NULL COMMENT '対応内容算出係数',
  `cal_method` int(4) DEFAULT NULL COMMENT '対応内容算出方法',
  `other1` varchar(255) DEFAULT NULL COMMENT '予備',
  PRIMARY KEY (`id`),
  KEY `idx_orderno` (`orderno`),
  KEY `idx_category_cd` (`category_cd`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEスキルマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mast_skill`
--

LOCK TABLES `mast_skill` WRITE;
/*!40000 ALTER TABLE `mast_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `mast_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `outlook_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_contacts`
--

DROP TABLE IF EXISTS `meetings_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_contacts`
--

LOCK TABLES `meetings_contacts` WRITE;
/*!40000 ALTER TABLE `meetings_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_leads`
--

DROP TABLE IF EXISTS `meetings_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_leads`
--

LOCK TABLES `meetings_leads` WRITE;
/*!40000 ALTER TABLE `meetings_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_users`
--

DROP TABLE IF EXISTS `meetings_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_users`
--

LOCK TABLES `meetings_users` WRITE;
/*!40000 ALTER TABLE `meetings_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_eigyo_sosiki`
--

DROP TABLE IF EXISTS `mst_eigyo_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_eigyo_sosiki` (
  `eigyo_sosiki_cd` varchar(21) NOT NULL DEFAULT '',
  `eigyo_sosiki_disp_cd` varchar(21) DEFAULT NULL,
  `op_start_date` datetime DEFAULT NULL,
  `eigyo_sosiki_lv` double DEFAULT NULL,
  `agent_cd` varchar(20) DEFAULT NULL,
  `old_div_cd` varchar(8) DEFAULT NULL,
  `eigyo_sosiki_nm` varchar(128) DEFAULT NULL,
  `eigyo_sosiki_short_nm` varchar(128) DEFAULT NULL,
  `corp_cd` char(3) DEFAULT NULL,
  `div1_cd` char(3) DEFAULT NULL,
  `div2_cd` char(3) DEFAULT NULL,
  `div3_cd` char(3) DEFAULT NULL,
  `div4_cd` char(3) DEFAULT NULL,
  `div5_cd` char(3) DEFAULT NULL,
  `div6_cd` char(3) DEFAULT NULL,
  `div1_nm` varchar(64) DEFAULT NULL,
  `div2_nm` varchar(64) DEFAULT NULL,
  `div3_nm` varchar(64) DEFAULT NULL,
  `div4_nm` varchar(64) DEFAULT NULL,
  `div5_nm` varchar(64) DEFAULT NULL,
  `div6_nm` varchar(64) DEFAULT NULL,
  `sort_cd` varchar(13) DEFAULT NULL,
  `jinji_div_cd` varchar(11) DEFAULT NULL,
  `sum_cd` varchar(13) DEFAULT NULL,
  `line_mng_tanto_cd` varchar(15) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`eigyo_sosiki_cd`,`del_flg`),
  KEY `idx_eigyo_sosiki_disp_cd` (`eigyo_sosiki_disp_cd`),
  KEY `idx_mlt_corp` (`corp_cd`,`eigyo_sosiki_lv`,`div1_cd`,`div2_cd`,`div3_cd`,`div4_cd`,`sort_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_eigyo_sosiki`
--

LOCK TABLES `mst_eigyo_sosiki` WRITE;
/*!40000 ALTER TABLE `mst_eigyo_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_eigyo_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_eigyo_sosiki_old`
--

DROP TABLE IF EXISTS `mst_eigyo_sosiki_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_eigyo_sosiki_old` (
  `eigyo_sosiki_cd` varchar(21) NOT NULL DEFAULT '',
  `eigyo_sosiki_disp_cd` varchar(21) DEFAULT NULL,
  `op_start_date` datetime DEFAULT NULL,
  `eigyo_sosiki_lv` double DEFAULT NULL,
  `agent_cd` varchar(20) DEFAULT NULL,
  `old_div_cd` varchar(8) DEFAULT NULL,
  `eigyo_sosiki_nm` varchar(128) DEFAULT NULL,
  `eigyo_sosiki_short_nm` varchar(128) DEFAULT NULL,
  `corp_cd` char(3) DEFAULT NULL,
  `div1_cd` char(3) DEFAULT NULL,
  `div2_cd` char(3) DEFAULT NULL,
  `div3_cd` char(3) DEFAULT NULL,
  `div4_cd` char(3) DEFAULT NULL,
  `div5_cd` char(3) DEFAULT NULL,
  `div6_cd` char(3) DEFAULT NULL,
  `div1_nm` varchar(64) DEFAULT NULL,
  `div2_nm` varchar(64) DEFAULT NULL,
  `div3_nm` varchar(64) DEFAULT NULL,
  `div4_nm` varchar(64) DEFAULT NULL,
  `div5_nm` varchar(64) DEFAULT NULL,
  `div6_nm` varchar(64) DEFAULT NULL,
  `sort_cd` varchar(13) DEFAULT NULL,
  `jinji_div_cd` varchar(11) DEFAULT NULL,
  `sum_cd` varchar(13) DEFAULT NULL,
  `line_mng_tanto_cd` varchar(15) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`eigyo_sosiki_cd`,`del_flg`),
  KEY `idx_eigyo_sosiki_disp_cd` (`eigyo_sosiki_disp_cd`),
  KEY `idx_mlt_corp` (`corp_cd`,`eigyo_sosiki_lv`,`div1_cd`,`div2_cd`,`div3_cd`,`div4_cd`,`sort_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_eigyo_sosiki_old`
--

LOCK TABLES `mst_eigyo_sosiki_old` WRITE;
/*!40000 ALTER TABLE `mst_eigyo_sosiki_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_eigyo_sosiki_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_jinji_sosiki`
--

DROP TABLE IF EXISTS `mst_jinji_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_jinji_sosiki` (
  `div_cd` varchar(11) NOT NULL DEFAULT '',
  `div_nm` varchar(255) DEFAULT NULL,
  `div_nm_short` varchar(255) DEFAULT NULL,
  `up_div_cd` varchar(11) DEFAULT NULL,
  `mng_tanto_cd` varchar(15) DEFAULT NULL,
  `mng_grptanto_cd` varchar(15) DEFAULT NULL,
  `div_unit_id` varchar(40) DEFAULT NULL,
  `div_unit_kbn` varchar(11) DEFAULT NULL,
  `ord_no` varchar(11) DEFAULT NULL,
  `kaisha_cd` char(4) DEFAULT NULL,
  `org_del_flg` char(1) DEFAULT NULL,
  `org_regist_date` datetime DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `org_regist_nm` varchar(36) DEFAULT NULL,
  `org_update_nm` varchar(36) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`div_cd`,`del_flg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_jinji_sosiki`
--

LOCK TABLES `mst_jinji_sosiki` WRITE;
/*!40000 ALTER TABLE `mst_jinji_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_jinji_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_jinji_sosiki_old`
--

DROP TABLE IF EXISTS `mst_jinji_sosiki_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_jinji_sosiki_old` (
  `div_cd` varchar(11) NOT NULL DEFAULT '',
  `div_nm` varchar(255) DEFAULT NULL,
  `div_nm_short` varchar(255) DEFAULT NULL,
  `up_div_cd` varchar(11) DEFAULT NULL,
  `mng_tanto_cd` varchar(15) DEFAULT NULL,
  `mng_grptanto_cd` varchar(15) DEFAULT NULL,
  `div_unit_id` varchar(40) DEFAULT NULL,
  `div_unit_kbn` varchar(11) DEFAULT NULL,
  `ord_no` varchar(11) DEFAULT NULL,
  `kaisha_cd` char(4) DEFAULT NULL,
  `org_del_flg` char(1) DEFAULT NULL,
  `org_regist_date` datetime DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `org_regist_nm` varchar(36) DEFAULT NULL,
  `org_update_nm` varchar(36) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`div_cd`,`del_flg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_jinji_sosiki_old`
--

LOCK TABLES `mst_jinji_sosiki_old` WRITE;
/*!40000 ALTER TABLE `mst_jinji_sosiki_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_jinji_sosiki_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_login_account`
--

DROP TABLE IF EXISTS `mst_login_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_login_account` (
  `ACCOUNT_ID` varchar(15) NOT NULL,
  `TANTO_CD` varchar(15) DEFAULT NULL,
  `STR_KOJIN_ID` varchar(20) DEFAULT NULL,
  `ROLE_CD` varchar(4) DEFAULT NULL,
  `ACS_ACCOUNT` varchar(15) DEFAULT NULL,
  `ACCESS_FROM_DATE` datetime DEFAULT NULL,
  `ACCESS_TO_DATE` datetime DEFAULT NULL,
  `UPDATE_MEMO` varchar(255) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_login_account`
--

LOCK TABLES `mst_login_account` WRITE;
/*!40000 ALTER TABLE `mst_login_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_login_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_tanto`
--

DROP TABLE IF EXISTS `mst_tanto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_tanto` (
  `tanto_cd` varchar(15) NOT NULL DEFAULT '',
  `last_nm` varchar(40) DEFAULT NULL,
  `first_nm` varchar(40) DEFAULT NULL,
  `name_kanji` varchar(80) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `stf_cd` varchar(8) DEFAULT NULL,
  `str_kojin_id` varchar(20) DEFAULT NULL,
  `group_tanto_cd` varchar(7) DEFAULT NULL,
  `kaisha_cd` varchar(4) DEFAULT NULL,
  `sbm_tanto_cd` varchar(30) DEFAULT NULL,
  `sbb_tanto_cd` varchar(30) DEFAULT NULL,
  `sbtm_tanto_cd` varchar(30) DEFAULT NULL,
  `div_cd` varchar(11) DEFAULT NULL,
  `div_nm` varchar(255) DEFAULT NULL,
  `div_unit_id` varchar(40) DEFAULT NULL,
  `mng_tanto_cd` varchar(15) DEFAULT NULL,
  `mng_grptanto_cd` varchar(15) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `email_adr` varchar(241) DEFAULT NULL,
  `agent_flg` char(1) NOT NULL,
  `agent_cd` varchar(20) DEFAULT NULL,
  `org_del_flg` char(1) DEFAULT NULL,
  `org_regist_date` datetime DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `org_regist_nm` varchar(36) DEFAULT NULL,
  `org_update_nm` varchar(36) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tanto_cd`,`del_flg`),
  KEY `idx_tanto_sbtm_tanto_cd` (`sbtm_tanto_cd`),
  KEY `idx_tanto_str_kojin_id` (`str_kojin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_tanto`
--

LOCK TABLES `mst_tanto` WRITE;
/*!40000 ALTER TABLE `mst_tanto` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_tanto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_tanto_old`
--

DROP TABLE IF EXISTS `mst_tanto_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_tanto_old` (
  `tanto_cd` varchar(15) NOT NULL DEFAULT '',
  `last_nm` varchar(40) DEFAULT NULL,
  `first_nm` varchar(40) DEFAULT NULL,
  `name_kanji` varchar(80) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `stf_cd` varchar(8) DEFAULT NULL,
  `str_kojin_id` varchar(20) DEFAULT NULL,
  `group_tanto_cd` varchar(7) DEFAULT NULL,
  `kaisha_cd` varchar(4) DEFAULT NULL,
  `sbm_tanto_cd` varchar(30) DEFAULT NULL,
  `sbb_tanto_cd` varchar(30) DEFAULT NULL,
  `sbtm_tanto_cd` varchar(30) DEFAULT NULL,
  `div_cd` varchar(11) DEFAULT NULL,
  `div_nm` varchar(255) DEFAULT NULL,
  `div_unit_id` varchar(40) DEFAULT NULL,
  `mng_tanto_cd` varchar(15) DEFAULT NULL,
  `mng_grptanto_cd` varchar(15) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `email_adr` varchar(241) DEFAULT NULL,
  `agent_flg` char(1) NOT NULL,
  `agent_cd` varchar(20) DEFAULT NULL,
  `org_del_flg` char(1) DEFAULT NULL,
  `org_regist_date` datetime DEFAULT NULL,
  `org_update_date` datetime DEFAULT NULL,
  `org_regist_nm` varchar(36) DEFAULT NULL,
  `org_update_nm` varchar(36) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tanto_cd`,`del_flg`),
  KEY `idx_tanto_sbtm_tanto_cd` (`sbtm_tanto_cd`),
  KEY `idx_tanto_str_kojin_id` (`str_kojin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_tanto_old`
--

LOCK TABLES `mst_tanto_old` WRITE;
/*!40000 ALTER TABLE `mst_tanto_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_tanto_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_tanto_s_sosiki`
--

DROP TABLE IF EXISTS `mst_tanto_s_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_tanto_s_sosiki` (
  `tanto_cd` varchar(15) NOT NULL DEFAULT '',
  `eigyo_sosiki_cd` varchar(21) NOT NULL DEFAULT '',
  `main_flg` char(1) NOT NULL,
  `sbtm_tanto_cd` varchar(15) DEFAULT NULL,
  `group_tanto_cd` varchar(15) DEFAULT NULL,
  `sp_tanto_cd` varchar(15) DEFAULT NULL,
  `mobile_stf_cd` varchar(15) DEFAULT NULL,
  `hosyu_sosiki_cd` varchar(21) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tanto_cd`,`eigyo_sosiki_cd`,`del_flg`),
  KEY `idx_sbtm_tanto_cd` (`sbtm_tanto_cd`),
  KEY `idx_eigyo_sosiki` (`eigyo_sosiki_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_tanto_s_sosiki`
--

LOCK TABLES `mst_tanto_s_sosiki` WRITE;
/*!40000 ALTER TABLE `mst_tanto_s_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_tanto_s_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_tanto_s_sosiki_old`
--

DROP TABLE IF EXISTS `mst_tanto_s_sosiki_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_tanto_s_sosiki_old` (
  `tanto_cd` varchar(15) NOT NULL DEFAULT '',
  `eigyo_sosiki_cd` varchar(21) NOT NULL DEFAULT '',
  `main_flg` char(1) NOT NULL,
  `sbtm_tanto_cd` varchar(15) DEFAULT NULL,
  `group_tanto_cd` varchar(15) DEFAULT NULL,
  `sp_tanto_cd` varchar(15) DEFAULT NULL,
  `mobile_stf_cd` varchar(15) DEFAULT NULL,
  `hosyu_sosiki_cd` varchar(21) DEFAULT NULL,
  `del_flg` char(1) NOT NULL,
  `regist_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `regist_nm` varchar(64) DEFAULT NULL,
  `update_nm` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tanto_cd`,`eigyo_sosiki_cd`,`del_flg`),
  KEY `idx_sbtm_tanto_cd` (`sbtm_tanto_cd`),
  KEY `idx_eigyo_sosiki` (`eigyo_sosiki_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_tanto_s_sosiki_old`
--

LOCK TABLES `mst_tanto_s_sosiki_old` WRITE;
/*!40000 ALTER TABLE `mst_tanto_s_sosiki_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_tanto_s_sosiki_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navagis_calls_info`
--

DROP TABLE IF EXISTS `navagis_calls_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navagis_calls_info` (
  `id` varchar(36) NOT NULL COMMENT '活動履歴ID',
  `name` varchar(150) DEFAULT '' COMMENT '活動件名',
  `assigned_user_id` varchar(36) NOT NULL COMMENT '担当者',
  `date_start` datetime NOT NULL COMMENT '活動日時(from)',
  `time_end_c` time NOT NULL COMMENT '活動日時(to)',
  `status` varchar(10) NOT NULL COMMENT 'ステータス',
  `parent_type` varchar(15) NOT NULL COMMENT '関連先Type',
  `account_id` varchar(6) NOT NULL COMMENT '企業番号',
  `account_name` varchar(80) NOT NULL COMMENT '企業名',
  `anken_id_c` varchar(7) DEFAULT '' COMMENT '案件ID',
  `opportunity_name` varchar(255) DEFAULT '' COMMENT '案件名',
  `division_c` varchar(100) NOT NULL COMMENT '活動区分',
  `companion_c` varchar(255) DEFAULT '' COMMENT '社内同行者',
  `contact_title_c` varchar(100) DEFAULT '' COMMENT '活動先職位',
  `contact_department_c` varchar(255) DEFAULT '' COMMENT '活動先部署名',
  `outlook_id` varchar(255) DEFAULT '' COMMENT '活動住所',
  `account_id_c` varchar(2) DEFAULT '' COMMENT '案件ステップ',
  `email_address` varchar(255) DEFAULT '' COMMENT '担当者メールアドレス',
  `div1_cd` varchar(3) DEFAULT '' COMMENT '本部CD',
  `div2_cd` varchar(3) DEFAULT '' COMMENT '統括部CD',
  `div3_cd` varchar(3) DEFAULT '' COMMENT '部CD',
  `div4_cd` varchar(3) DEFAULT '' COMMENT '課CD',
  `div1_nm` varchar(255) DEFAULT '' COMMENT '本部NM',
  `div2_nm` varchar(255) DEFAULT '' COMMENT '統括部NM',
  `div3_nm` varchar(255) DEFAULT '' COMMENT '部NM',
  `div4_nm` varchar(255) DEFAULT '' COMMENT '課NM',
  `date_modified` datetime NOT NULL COMMENT '更新日',
  `modified_user_id` varchar(36) NOT NULL COMMENT '更新者',
  `modified_user_mail` varchar(255) DEFAULT '' COMMENT '更新者メールアドレス',
  `date_entered` datetime NOT NULL COMMENT '入力日',
  `created_by` varchar(36) NOT NULL COMMENT '作成者',
  `created_by_mail` varchar(255) DEFAULT '' COMMENT '作成者メールアドレス',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_date_modified` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Navagis連携用活動情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navagis_calls_info`
--

LOCK TABLES `navagis_calls_info` WRITE;
/*!40000 ALTER TABLE `navagis_calls_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `navagis_calls_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `navagis_calls_listviewer`
--

DROP TABLE IF EXISTS `navagis_calls_listviewer`;
/*!50001 DROP VIEW IF EXISTS `navagis_calls_listviewer`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `navagis_calls_listviewer` (
  `id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `assigned_user_id` tinyint NOT NULL,
  `date_start` tinyint NOT NULL,
  `time_end_c` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `parent_type` tinyint NOT NULL,
  `account_id` tinyint NOT NULL,
  `account_name` tinyint NOT NULL,
  `anken_id_c` tinyint NOT NULL,
  `opportunity_name` tinyint NOT NULL,
  `division_c` tinyint NOT NULL,
  `companion_c` tinyint NOT NULL,
  `contact_title_c` tinyint NOT NULL,
  `contact_department_c` tinyint NOT NULL,
  `outlook_id` tinyint NOT NULL,
  `account_id_c` tinyint NOT NULL,
  `email_address` tinyint NOT NULL,
  `div1_cd` tinyint NOT NULL,
  `div2_cd` tinyint NOT NULL,
  `div3_cd` tinyint NOT NULL,
  `div4_cd` tinyint NOT NULL,
  `div1_nm` tinyint NOT NULL,
  `div2_nm` tinyint NOT NULL,
  `div3_nm` tinyint NOT NULL,
  `div4_nm` tinyint NOT NULL,
  `date_modified` tinyint NOT NULL,
  `modified_user_id` tinyint NOT NULL,
  `modified_user_mail` tinyint NOT NULL,
  `date_entered` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `created_by_mail` tinyint NOT NULL,
  `deleted` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `navagis_opportunities_info`
--

DROP TABLE IF EXISTS `navagis_opportunities_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navagis_opportunities_info` (
  `id` varchar(36) NOT NULL COMMENT '案件CD',
  `anken_id_c` varchar(7) NOT NULL COMMENT '案件ID',
  `name` varchar(255) NOT NULL COMMENT '案件名',
  `assigned_user_id` varchar(36) NOT NULL COMMENT '担当者',
  `account_id` varchar(6) NOT NULL COMMENT '企業番号',
  `account_name` varchar(80) NOT NULL COMMENT '企業名',
  `op_cont_plandate_c` date NOT NULL COMMENT '契約予定日',
  `op_order_poss_c` varchar(2) NOT NULL COMMENT '受注確度',
  `op_step_c` varchar(2) NOT NULL COMMENT 'ステップ',
  `op_commit_c` varchar(3) DEFAULT '' COMMENT 'フォーキャスト',
  `email_address` varchar(255) DEFAULT '' COMMENT '担当者メールアドレス',
  `div1_cd` varchar(3) DEFAULT '' COMMENT '本部CD',
  `div2_cd` varchar(3) DEFAULT '' COMMENT '統括部CD',
  `div3_cd` varchar(3) DEFAULT '' COMMENT '部CD',
  `div4_cd` varchar(3) DEFAULT '' COMMENT '課CD',
  `div1_nm` varchar(255) DEFAULT '' COMMENT '本部NM',
  `div2_nm` varchar(255) DEFAULT '' COMMENT '統括部NM',
  `div3_nm` varchar(255) DEFAULT '' COMMENT '部NM',
  `div4_nm` varchar(255) DEFAULT '' COMMENT '課NM',
  `date_modified` datetime NOT NULL COMMENT '更新日',
  `modified_user_id` varchar(36) NOT NULL COMMENT '更新者',
  `modified_user_mail` varchar(255) DEFAULT '' COMMENT '更新者メールアドレス',
  `date_entered` datetime NOT NULL COMMENT '入力日',
  `created_by` varchar(36) NOT NULL COMMENT '作成者',
  `created_by_mail` varchar(255) DEFAULT '' COMMENT '作成者メールアドレス',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  PRIMARY KEY (`id`),
  KEY `idx_anken_id_c` (`anken_id_c`),
  KEY `idx_date_modified` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Navagis連携用案件情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navagis_opportunities_info`
--

LOCK TABLES `navagis_opportunities_info` WRITE;
/*!40000 ALTER TABLE `navagis_opportunities_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `navagis_opportunities_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `navagis_opportunities_listviewer`
--

DROP TABLE IF EXISTS `navagis_opportunities_listviewer`;
/*!50001 DROP VIEW IF EXISTS `navagis_opportunities_listviewer`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `navagis_opportunities_listviewer` (
  `id` tinyint NOT NULL,
  `anken_id_c` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `assigned_user_id` tinyint NOT NULL,
  `account_id` tinyint NOT NULL,
  `account_name` tinyint NOT NULL,
  `op_cont_plandate_c` tinyint NOT NULL,
  `op_order_poss_c` tinyint NOT NULL,
  `op_step_c` tinyint NOT NULL,
  `op_commit_c` tinyint NOT NULL,
  `email_address` tinyint NOT NULL,
  `div1_cd` tinyint NOT NULL,
  `div2_cd` tinyint NOT NULL,
  `div3_cd` tinyint NOT NULL,
  `div4_cd` tinyint NOT NULL,
  `div1_nm` tinyint NOT NULL,
  `div2_nm` tinyint NOT NULL,
  `div3_nm` tinyint NOT NULL,
  `div4_nm` tinyint NOT NULL,
  `date_modified` tinyint NOT NULL,
  `modified_user_id` tinyint NOT NULL,
  `modified_user_mail` tinyint NOT NULL,
  `date_entered` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `created_by_mail` tinyint NOT NULL,
  `deleted` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) NOT NULL DEFAULT '0',
  `embed_flag` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_endsv`
--

DROP TABLE IF EXISTS `opadd_endsv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_endsv` (
  `id` char(36) NOT NULL COMMENT '終了サービス_ID',
  `name` varchar(50) DEFAULT 'name of endsv' COMMENT 'ダミー(未使用）',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` text COMMENT '詳細(未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'ダミー(未使用）',
  `endsv_class` varchar(100) NOT NULL COMMENT '種別',
  `endsv_cate` varchar(100) NOT NULL COMMENT 'サービス分類',
  `endsv_start_yyyy` varchar(100) DEFAULT NULL COMMENT '提供開始年',
  `endsv_start_mm` varchar(100) DEFAULT NULL COMMENT '提供開始月',
  `endsv_end_yyyy` varchar(100) NOT NULL COMMENT '提供終了年',
  `endsv_end_mm` varchar(100) NOT NULL COMMENT '提供終了月',
  `endsv_unit` varchar(100) NOT NULL COMMENT '単位',
  `endsv_num` double NOT NULL COMMENT '金額/数量',
  PRIMARY KEY (`id`),
  KEY `idx_endsv_cate` (`endsv_cate`),
  KEY `idx_endsv_unit` (`endsv_unit`),
  KEY `idx_ASSIGNED_USER_ID` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_endsv`
--

LOCK TABLES `opadd_endsv` WRITE;
/*!40000 ALTER TABLE `opadd_endsv` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_endsv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_endsv_audit`
--

DROP TABLE IF EXISTS `opadd_endsv_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_endsv_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_endsv_audit`
--

LOCK TABLES `opadd_endsv_audit` WRITE;
/*!40000 ALTER TABLE `opadd_endsv_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_endsv_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_endsv_cstm`
--

DROP TABLE IF EXISTS `opadd_endsv_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_endsv_cstm` (
  `id_c` char(36) NOT NULL COMMENT '終了サービス_ID',
  `parent_id` varchar(36) DEFAULT NULL COMMENT '関連先ID(案件CD）',
  `parent_type` varchar(100) DEFAULT NULL COMMENT '関連タイプ',
  PRIMARY KEY (`id_c`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_endsv_cstm`
--

LOCK TABLES `opadd_endsv_cstm` WRITE;
/*!40000 ALTER TABLE `opadd_endsv_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_endsv_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_endsvpportunities`
--

DROP TABLE IF EXISTS `opadd_endsvpportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_endsvpportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `opadd_endsv_ida` varchar(36) DEFAULT NULL COMMENT '終了サービス_ID',
  `opportunities_idb` varchar(36) DEFAULT NULL COMMENT '案件_ID',
  PRIMARY KEY (`id`),
  KEY `opadd_endsvpportunities_ida1` (`opadd_endsv_ida`),
  KEY `opadd_endsvpportunities_idb2` (`opportunities_idb`),
  KEY `idx_deleted` (`deleted`),
  KEY `alt_opadd_endsvpportunities` (`opadd_endsv_ida`,`opportunities_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_endsvpportunities`
--

LOCK TABLES `opadd_endsvpportunities` WRITE;
/*!40000 ALTER TABLE `opadd_endsvpportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_endsvpportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_newsv`
--

DROP TABLE IF EXISTS `opadd_newsv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_newsv` (
  `id` char(36) NOT NULL COMMENT '新規サービス_ID',
  `name` varchar(50) DEFAULT 'name of newsv' COMMENT 'ダミー(未使用）',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細(未使用）',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先(未使用）',
  `newsv_class` varchar(100) NOT NULL COMMENT '種別',
  `newsv_cate` varchar(100) NOT NULL COMMENT 'サービス分類',
  `newsv_start_yyyy` varchar(100) NOT NULL COMMENT '提供開始年',
  `newsv_start_mm` varchar(100) NOT NULL COMMENT '提供開始月',
  `newsv_end_yyyy` varchar(100) DEFAULT NULL COMMENT '提供終了年',
  `newsv_end_mm` varchar(100) DEFAULT NULL COMMENT '提供終了月',
  `newsv_unit` varchar(100) NOT NULL COMMENT '単位',
  `newsv_num` double NOT NULL COMMENT '金額/数量',
  PRIMARY KEY (`id`),
  KEY `idx_newsv_cate` (`newsv_cate`),
  KEY `idx_newsv_unit` (`newsv_unit`),
  KEY `idx_ASSIGNED_USER_ID` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_newsv`
--

LOCK TABLES `opadd_newsv` WRITE;
/*!40000 ALTER TABLE `opadd_newsv` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_newsv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_newsv_audit`
--

DROP TABLE IF EXISTS `opadd_newsv_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_newsv_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_newsv_audit`
--

LOCK TABLES `opadd_newsv_audit` WRITE;
/*!40000 ALTER TABLE `opadd_newsv_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_newsv_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_newsv_cstm`
--

DROP TABLE IF EXISTS `opadd_newsv_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_newsv_cstm` (
  `id_c` char(36) NOT NULL COMMENT '新規サービス_ID',
  `parent_id` varchar(36) DEFAULT NULL COMMENT '関連先ID(案件CD）',
  `parent_type` varchar(100) DEFAULT NULL COMMENT '関連タイプ',
  `billiard_form` varchar(20) DEFAULT NULL COMMENT '新規区分',
  `billiard_model_code` varchar(20) DEFAULT NULL COMMENT '機種コード',
  `billiard_maker` varchar(20) DEFAULT NULL COMMENT 'メーカ',
  `billiard_model` varchar(20) DEFAULT NULL COMMENT 'モデル',
  `billiard_color` varchar(20) DEFAULT NULL COMMENT '色',
  PRIMARY KEY (`id_c`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_newsv_cstm`
--

LOCK TABLES `opadd_newsv_cstm` WRITE;
/*!40000 ALTER TABLE `opadd_newsv_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_newsv_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_newsvpportunities`
--

DROP TABLE IF EXISTS `opadd_newsvpportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_newsvpportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `opadd_newsv_ida` varchar(36) DEFAULT NULL COMMENT '新規サービス_ID',
  `opportunities_idb` varchar(36) DEFAULT NULL COMMENT '案件_ID',
  PRIMARY KEY (`id`),
  KEY `opadd_newsvpportunities_ida1` (`opadd_newsv_ida`),
  KEY `opadd_newsvpportunities_idb2` (`opportunities_idb`),
  KEY `idx_deleted` (`deleted`),
  KEY `opadd_idx` (`opadd_newsv_ida`,`opportunities_idb`),
  KEY `alt_opadd_newsvpportunities` (`opadd_newsv_ida`,`opportunities_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_newsvpportunities`
--

LOCK TABLES `opadd_newsvpportunities` WRITE;
/*!40000 ALTER TABLE `opadd_newsvpportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_newsvpportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_partner`
--

DROP TABLE IF EXISTS `opadd_partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_partner` (
  `id` char(36) NOT NULL COMMENT 'ビジネスパートナーCD',
  `name` varchar(50) DEFAULT 'name of Partner' COMMENT 'ダミー（未使用）',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日ID',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先（未使用）',
  `parent_type` varchar(100) DEFAULT NULL COMMENT '関連先タイプ',
  `parent_id` char(36) DEFAULT NULL COMMENT '関連先ID（案件CD）',
  `account_id` char(36) DEFAULT NULL COMMENT '企業番号（未使用）',
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_partner`
--

LOCK TABLES `opadd_partner` WRITE;
/*!40000 ALTER TABLE `opadd_partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_partner_audit`
--

DROP TABLE IF EXISTS `opadd_partner_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_partner_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_partner_audit`
--

LOCK TABLES `opadd_partner_audit` WRITE;
/*!40000 ALTER TABLE `opadd_partner_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_partner_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_partner_cstm`
--

DROP TABLE IF EXISTS `opadd_partner_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_partner_cstm` (
  `id_c` char(36) NOT NULL COMMENT 'ID',
  `test_c` varchar(25) DEFAULT NULL,
  `account_id_c` varchar(36) DEFAULT NULL COMMENT '企業番号',
  `account_name_c` varchar(255) DEFAULT NULL COMMENT '企業名（ダミー）',
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_partner_cstm`
--

LOCK TABLES `opadd_partner_cstm` WRITE;
/*!40000 ALTER TABLE `opadd_partner_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_partner_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opadd_partnpportunities`
--

DROP TABLE IF EXISTS `opadd_partnpportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opadd_partnpportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `opadd_partner_ida` varchar(36) DEFAULT NULL COMMENT 'ビジネスパートナーCD',
  `opportunities_idb` varchar(36) DEFAULT NULL COMMENT '案件CD',
  PRIMARY KEY (`id`),
  KEY `opadd_partnpportunities_ida1` (`opadd_partner_ida`),
  KEY `opadd_partnpportunities_idb2` (`opportunities_idb`),
  KEY `idx_deleted` (`deleted`),
  KEY `alt_opadd_partnpportunities` (`opadd_partner_ida`,`opportunities_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opadd_partnpportunities`
--

LOCK TABLES `opadd_partnpportunities` WRITE;
/*!40000 ALTER TABLE `opadd_partnpportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opadd_partnpportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opls2_callviewer`
--

DROP TABLE IF EXISTS `opls2_callviewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opls2_callviewer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `mast_organizationunit_id` varchar(25) DEFAULT NULL,
  `mast_organizationunit_name` varchar(25) DEFAULT NULL,
  `mast_organizationunit2_id` varchar(25) DEFAULT NULL,
  `mast_organizationunit2_name` varchar(25) DEFAULT NULL,
  `mast_organizationunit3_id` varchar(25) DEFAULT NULL,
  `mast_organizationunit3_name` varchar(25) DEFAULT NULL,
  `sales_staff_code` varchar(25) DEFAULT NULL,
  `call_date_from` date DEFAULT NULL,
  `call_date_to` date DEFAULT NULL,
  `call_name` varchar(25) DEFAULT NULL,
  `next_step_c` varchar(25) DEFAULT NULL,
  `parent_type_desc` varchar(25) DEFAULT NULL,
  `account_id` varchar(25) DEFAULT NULL,
  `account_name` varchar(25) DEFAULT NULL,
  `anken_id_c` varchar(25) DEFAULT NULL,
  `opportunity_name` varchar(25) DEFAULT NULL,
  `sales_group_code` varchar(25) DEFAULT NULL,
  `sales_group_name` varchar(25) DEFAULT NULL,
  `status_desc` varchar(25) DEFAULT NULL,
  `sales_staff_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opls2_callviewer`
--

LOCK TABLES `opls2_callviewer` WRITE;
/*!40000 ALTER TABLE `opls2_callviewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `opls2_callviewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opls2_callviewer_audit`
--

DROP TABLE IF EXISTS `opls2_callviewer_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opls2_callviewer_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opls2_callviewer_audit`
--

LOCK TABLES `opls2_callviewer_audit` WRITE;
/*!40000 ALTER TABLE `opls2_callviewer_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opls2_callviewer_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oplst_list`
--

DROP TABLE IF EXISTS `oplst_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oplst_list` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `sv_class` varchar(100) DEFAULT NULL,
  `sv_cate` varchar(100) DEFAULT NULL,
  `sv_class_desc` varchar(25) DEFAULT NULL,
  `sv_cate_name` varchar(100) DEFAULT NULL,
  `sv_start_yyyymm` varchar(25) DEFAULT NULL,
  `sv_end_yyyymm` varchar(25) DEFAULT NULL,
  `sv_start_end` varchar(25) DEFAULT NULL,
  `sv_unit` varchar(100) DEFAULT NULL,
  `sv_num` int(11) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `partner_name` varchar(255) DEFAULT NULL,
  `op_compname_memo_c` varchar(255) DEFAULT NULL,
  `opportunity_number_c` int(11) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `owner_name` varchar(100) DEFAULT NULL,
  `main_sales_name` varchar(100) DEFAULT NULL,
  `op_sts_c` text,
  `op_step_c` text,
  `op_commit_c` text,
  `op_commit_c_desc` varchar(25) DEFAULT NULL,
  `op_forecast_c` text,
  `op_forecast_c_desc` varchar(25) DEFAULT NULL,
  `op_cont_plandate_c` date DEFAULT NULL,
  `account_name_kana` varchar(255) DEFAULT NULL,
  `account_sales_staff_code` varchar(25) DEFAULT NULL,
  `account_sales_name` varchar(100) DEFAULT NULL,
  `account_sales_group_code` varchar(25) DEFAULT NULL,
  `account_sales_group_name` varchar(100) DEFAULT NULL,
  `main_sales_staff_code` varchar(25) DEFAULT NULL,
  `main_sales_group_code` varchar(25) DEFAULT NULL,
  `main_sales_group_name` varchar(100) DEFAULT NULL,
  `op_order_poss_c` varchar(100) DEFAULT NULL,
  `owner_staff_code` varchar(25) DEFAULT NULL,
  `partner_id` varchar(25) DEFAULT NULL,
  `sales_support_staff_code` varchar(25) DEFAULT NULL,
  `sales_support_name` varchar(255) DEFAULT NULL,
  `sales_support_group_code` varchar(25) DEFAULT NULL,
  `sales_support_group_name` varchar(255) DEFAULT NULL,
  `op_depend_c` varchar(100) DEFAULT NULL,
  `account_id` varchar(100) DEFAULT NULL,
  `opportunity_id` varchar(100) DEFAULT NULL,
  `op_group_type` varchar(100) DEFAULT '001',
  `op_group_type_desc` varchar(25) DEFAULT NULL,
  `mast_organizationunit_id` varchar(100) DEFAULT NULL,
  `mast_organizationunit_name` varchar(255) DEFAULT NULL,
  `mast_organizationunit2_id` varchar(100) DEFAULT NULL,
  `mast_organizationunit2_name` varchar(255) DEFAULT NULL,
  `mast_organizationunit3_id` varchar(100) DEFAULT NULL,
  `mast_organizationunit3_name` varchar(255) DEFAULT NULL,
  `op_cont_plandate_from` date DEFAULT NULL,
  `op_cont_plandate_to` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oplst_list`
--

LOCK TABLES `oplst_list` WRITE;
/*!40000 ALTER TABLE `oplst_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `oplst_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oplst_list_audit`
--

DROP TABLE IF EXISTS `oplst_list_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oplst_list_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oplst_list_audit`
--

LOCK TABLES `oplst_list_audit` WRITE;
/*!40000 ALTER TABLE `oplst_list_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `oplst_list_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opp_salessup`
--

DROP TABLE IF EXISTS `opp_salessup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_salessup` (
  `id` char(36) NOT NULL COMMENT '営業サポートCD',
  `name` varchar(255) DEFAULT NULL COMMENT '名称(未使用)',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` varchar(255) DEFAULT NULL COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT '営業担当者ID',
  `role_proposed` varchar(255) DEFAULT NULL COMMENT '役割',
  `ps_sales` double(13,2) DEFAULT NULL,
  `calling_agent` varchar(20) DEFAULT NULL,
  `req_opp_class_cd` varchar(255) NOT NULL,
  `comp_date_from` date DEFAULT NULL,
  `comp_date_to` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_salessup_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='営業サポート';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_salessup`
--

LOCK TABLES `opp_salessup` WRITE;
/*!40000 ALTER TABLE `opp_salessup` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_salessup` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger opp_salessup_trigger before UPDATE on opp_salessup FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.assigned_user_id != new.assigned_user_id) THEN 
      INSERT INTO opp_salessup_audit values(UUID(), old.id, current, new.modified_user_id, 'assigned_user_id', 'relate', old.assigned_user_id, new.assigned_user_id, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `opp_salessup_audit`
--

DROP TABLE IF EXISTS `opp_salessup_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_salessup_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opp_salessup_primary` (`id`),
  KEY `idx_opp_salessup_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_salessup_audit`
--

LOCK TABLES `opp_salessup_audit` WRITE;
/*!40000 ALTER TABLE `opp_salessup_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_salessup_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opp_salessupportunities_c`
--

DROP TABLE IF EXISTS `opp_salessupportunities_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_salessupportunities_c` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `opp_saless3af5unities_ida` varchar(36) DEFAULT NULL COMMENT '案件CD',
  `opp_saless78cealessup_idb` varchar(36) DEFAULT NULL COMMENT '営業サポートCD',
  PRIMARY KEY (`id`),
  KEY `opp_salessu_opportunities_ida1` (`opp_saless3af5unities_ida`),
  KEY `opp_salessu_opportunities_alt` (`opp_saless78cealessup_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='案件-営業サポート';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_salessupportunities_c`
--

LOCK TABLES `opp_salessupportunities_c` WRITE;
/*!40000 ALTER TABLE `opp_salessupportunities_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_salessupportunities_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opp_se`
--

DROP TABLE IF EXISTS `opp_se`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_se` (
  `id` char(36) NOT NULL COMMENT 'SE案件CD',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` varchar(1000) DEFAULT NULL COMMENT '詳細（未使用）',
  `deleted` tinyint(1) DEFAULT NULL COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT '営業担当者',
  `status` varchar(100) DEFAULT NULL COMMENT 'ステータス',
  `seorderaccuracy` varchar(100) DEFAULT NULL COMMENT 'SE受注確度（%）',
  `projectname` varchar(255) DEFAULT NULL COMMENT 'プロジェクト名',
  `sestrategicclassification` varchar(1000) DEFAULT NULL COMMENT 'SE戦略分類',
  `seitemsummary` varchar(1000) DEFAULT NULL COMMENT 'SE案件概要',
  `salestempnum` double(11,0) DEFAULT NULL COMMENT '売上（一時金）',
  `salesmonthnum` double(11,0) DEFAULT NULL COMMENT '売上（月額）',
  `contracttermmonthlybasis` int(3) DEFAULT NULL COMMENT '契約期間（ヶ月）',
  `pssalestempnum` double(11,0) DEFAULT NULL COMMENT 'PS売上（一時金）',
  `pssalesmonthnum` double(11,0) DEFAULT NULL COMMENT 'PS売上（月額）',
  `psbudgetschedule` date DEFAULT NULL COMMENT 'PS計上予定時期',
  `causeorderdfailure` varchar(1000) DEFAULT NULL COMMENT '受注失注理由',
  `spare` varchar(1000) DEFAULT NULL COMMENT '予備',
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SE案件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_se`
--

LOCK TABLES `opp_se` WRITE;
/*!40000 ALTER TABLE `opp_se` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_se` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger opp_se_trigger before UPDATE on opp_se FOR each row
BEGIN 
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.psbudgetschedule != new.psbudgetschedule) THEN 
      INSERT INTO opp_se_audit values(UUID(), old.id, current, new.modified_user_id, 'psbudgetschedule', 'date', old.psbudgetschedule,  new.psbudgetschedule, null, null);
    END IF;
    IF (old.seorderaccuracy != new.seorderaccuracy) THEN 
      INSERT INTO opp_se_audit values(UUID(), old.id, current, new.modified_user_id, 'seorderaccuracy', 'enum', old.seorderaccuracy,  new.seorderaccuracy, null, null);
    END IF;
    IF (old.status != new.status) THEN 
      INSERT INTO opp_se_audit values(UUID(), old.id, current, new.modified_user_id, 'status', 'enum', old.status,  new.status, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `opp_se_audit`
--

DROP TABLE IF EXISTS `opp_se_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_se_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_se_audit`
--

LOCK TABLES `opp_se_audit` WRITE;
/*!40000 ALTER TABLE `opp_se_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_se_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opp_se_opportunities_c`
--

DROP TABLE IF EXISTS `opp_se_opportunities_c`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opp_se_opportunities_c` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) DEFAULT NULL COMMENT '削除済み',
  `opp_se_opp7a17unities_ida` varchar(36) DEFAULT NULL COMMENT '案件CD',
  `opp_se_opp1446sopp_se_idb` varchar(36) DEFAULT NULL COMMENT 'SE案件CD',
  PRIMARY KEY (`id`),
  KEY `opp_se_opportunities_ida1` (`opp_se_opp7a17unities_ida`),
  KEY `opp_se_opportunities_alt` (`opp_se_opp1446sopp_se_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='案件-SE案件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opp_se_opportunities_c`
--

LOCK TABLES `opp_se_opportunities_c` WRITE;
/*!40000 ALTER TABLE `opp_se_opportunities_c` DISABLE KEYS */;
/*!40000 ALTER TABLE `opp_se_opportunities_c` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities`
--

DROP TABLE IF EXISTS `opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL COMMENT '案件CD',
  `name` varchar(255) DEFAULT NULL COMMENT '案件名',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新ユーザID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` text COMMENT '詳細',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先（営業担当者）id',
  `opportunity_type` varchar(255) DEFAULT NULL COMMENT '案件区分',
  `campaign_id` char(36) DEFAULT NULL COMMENT 'キャンペーンID SPM案件ID（未使用）',
  `lead_source` varchar(50) DEFAULT NULL COMMENT 'リードソース',
  `amount` double DEFAULT NULL COMMENT '金額（未使用）',
  `amount_usdollar` double DEFAULT NULL COMMENT '金額USD（未使用）',
  `currency_id` char(36) DEFAULT NULL COMMENT '通貨ID（未使用）',
  `date_closed` date DEFAULT NULL COMMENT '決着日',
  `next_step` varchar(100) DEFAULT NULL COMMENT '次ステップ（未使用）',
  `sales_stage` varchar(200) DEFAULT NULL COMMENT 'キャンペーンCD',
  `probability` double DEFAULT NULL COMMENT '確度 (%)（未使用）',
  `owner_id` char(36) NOT NULL COMMENT '上長id',
  `sup_id` char(36) DEFAULT NULL COMMENT '登録時企業CD',
  `calling_agent` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_campaign_id` (`campaign_id`),
  KEY `idx_owner_id` (`owner_id`),
  KEY `idx_deleted` (`deleted`),
  KEY `idx_next_step` (`next_step`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities`
--

LOCK TABLES `opportunities` WRITE;
/*!40000 ALTER TABLE `opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger opportunities_trigger before UPDATE on opportunities FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.assigned_user_id != new.assigned_user_id) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id, current, new.modified_user_id, 'assigned_user_id', 'relate', old.assigned_user_id, new.assigned_user_id, null, null);
    END IF;
    IF (old.name != new.name) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id, current, new.modified_user_id, 'name', 'varchar', old.name,new.name, null, null);
    END IF;
    SET new.calling_agent = '';
    
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `opportunities_audit`
--

DROP TABLE IF EXISTS `opportunities_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_audit`
--

LOCK TABLES `opportunities_audit` WRITE;
/*!40000 ALTER TABLE `opportunities_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_contacts`
--

DROP TABLE IF EXISTS `opportunities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_contacts`
--

LOCK TABLES `opportunities_contacts` WRITE;
/*!40000 ALTER TABLE `opportunities_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_cstm`
--

DROP TABLE IF EXISTS `opportunities_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_cstm` (
  `id_c` char(36) NOT NULL COMMENT '案件CD',
  `aaa_c` varchar(100) DEFAULT '' COMMENT 'ごみ（未使用）',
  `op_compe_c` varchar(100) DEFAULT NULL COMMENT '競合・既存キャリア',
  `op_compname_memo_c` varchar(255) DEFAULT NULL COMMENT '企業名備考',
  `op_cont_plandate_c` date DEFAULT NULL COMMENT '契約予定日',
  `op_data_organ_c` varchar(255) DEFAULT NULL COMMENT 'データ　組織（未使用）',
  `op_dep_detail_c` text COMMENT 'ディペンデンシ内容',
  `op_depend_c` varchar(255) DEFAULT NULL COMMENT 'ディペンデンシ',
  `op_endsv_cate_c` varchar(100) DEFAULT NULL COMMENT '終了サービス：サービス分類（未使用）',
  `op_endsv_class_c` varchar(100) DEFAULT NULL COMMENT '終了サービス：種別（未使用）',
  `op_endsv_num_c` varchar(255) DEFAULT NULL COMMENT '終了サービス：サービス分類（未使用）',
  `op_plan2_c` varchar(255) DEFAULT NULL COMMENT '提案プラン2',
  `op_remarks2_c` text COMMENT '備考2',
  `op_endsv_unit_c` varchar(100) DEFAULT NULL COMMENT '終了サービス：金額/数量（未使用）',
  `op_endsvend_mm_c` varchar(100) DEFAULT NULL COMMENT '終了サービス：提供終了月（未使用）',
  `op_endsvend_yyyy_c` varchar(100) DEFAULT '' COMMENT '終了サービス：サービス提供終了年（未使用）',
  `op_endsvstart_mm_c` varchar(100) DEFAULT NULL COMMENT '終了サービス：提供開始月（未使用）',
  `op_endsvstart_yyyy_c` varchar(100) DEFAULT '' COMMENT '終了サービス：サービス提供開始年（未使用）',
  `op_newsv_cate_c` varchar(100) DEFAULT NULL COMMENT '新規サービス：提供開始月（未使用）',
  `op_newsv_class_c` varchar(100) DEFAULT NULL COMMENT '新規サービス：種別（未使用）',
  `op_newsv_num_c` varchar(255) DEFAULT NULL COMMENT '新規サービス：金額/数量（未使用）',
  `op_plan1_c` varchar(255) DEFAULT NULL COMMENT '提案プラン1',
  `op_remarks1_c` text COMMENT '備考1',
  `op_newsv_unit_c` varchar(100) DEFAULT NULL COMMENT '新規サービス：単位（未使用）',
  `op_newsvend_mm_c` varchar(100) DEFAULT NULL COMMENT '新規サービス：サービス提供終了月（未使用）',
  `op_newsvend_yyyy_c` varchar(100) DEFAULT '' COMMENT '新規サービス：サービス提供終了年（未使用）',
  `op_newsvstart_mm_c` varchar(100) DEFAULT NULL COMMENT '新規サービス：サービス提供開始月（未使用）',
  `op_newsvstart_yyyy_c` varchar(100) DEFAULT '' COMMENT '新規サービス：サービス提供開始年（未使用）',
  `op_onseikeitai_organ_c` varchar(255) DEFAULT NULL COMMENT '音声/携帯　組織（未使用）',
  `op_order_poss_c` varchar(100) NOT NULL DEFAULT '10' COMMENT '受注確度',
  `op_owner_c` varchar(255) DEFAULT NULL COMMENT 'オーナ（ごみ）（未使用）',
  `op_partner_c` varchar(255) DEFAULT NULL COMMENT 'ビジネスパートナ（未使用）',
  `op_sales_main_c` varchar(255) DEFAULT NULL COMMENT '営業（主管）（未使用）',
  `op_sales_support_c` varchar(255) DEFAULT NULL COMMENT '営業（サポート）（未使用）',
  `op_step_c` varchar(100) NOT NULL DEFAULT '10' COMMENT 'ステップ',
  `op_sts_c` varchar(100) NOT NULL DEFAULT '' COMMENT '進捗状況（未使用）',
  `op_sv_cate_c` varchar(100) DEFAULT NULL COMMENT '主要サービス分類（未使用）',
  `op_commit_c` varchar(100) DEFAULT '' COMMENT 'フォーキャスト（旧コミットメント）',
  `op_forecast_c` varchar(100) DEFAULT '' COMMENT 'フォーキャスト（未使用）',
  `opportunity_number_c` int(11) NOT NULL AUTO_INCREMENT COMMENT '案件ID生成用オートインクリメント（自動採番）',
  `anken_id_c` varchar(20) DEFAULT NULL COMMENT '案件ID（自動採番）',
  `doc_id_c` varchar(12) DEFAULT NULL COMMENT '問合せ番号（未使用）',
  `proposal1_c` varchar(1000) DEFAULT NULL COMMENT '提案内容1',
  `proposal_price1_c` double(11,0) DEFAULT NULL COMMENT '提案価格1',
  `discount_rate1_c` double(5,2) DEFAULT NULL COMMENT '割引率1',
  `proposal2_c` varchar(1000) DEFAULT NULL COMMENT '提案内容2',
  `proposal_price2_c` double(11,0) DEFAULT NULL COMMENT '提案価格2',
  `discount_rate2_c` double(5,2) DEFAULT NULL COMMENT '割引率2',
  `closed_situation_c` varchar(255) DEFAULT NULL COMMENT '決着状況',
  `victory_defeat_cause_c` varchar(255) DEFAULT NULL COMMENT '勝因／敗因',
  `victory_defeat_cause_detail_c` varchar(1000) DEFAULT NULL COMMENT '勝因／敗因詳細',
  `emphasis_strategy1_c` varchar(255) DEFAULT NULL COMMENT '進捗チェック（社内、競合）',
  `emphasis_strategy2_c` varchar(255) DEFAULT NULL COMMENT '進捗チェック（企業）',
  `measure_dept_c` varchar(255) DEFAULT NULL COMMENT '本部施策',
  `measure_company_c` varchar(255) DEFAULT NULL COMMENT '全社施策',
  `y_interest_c` varchar(25) DEFAULT NULL COMMENT 'Y興味',
  `y_interest_area_c` varchar(100) DEFAULT NULL COMMENT 'Y関心領域',
  `y_ng_reason_c` varchar(100) DEFAULT NULL COMMENT 'Y無し理由',
  `y_agency_c` varchar(500) DEFAULT NULL COMMENT 'Y利用代理店',
  `y_medium_c` varchar(100) DEFAULT NULL COMMENT 'Y利用媒体',
  `y_remarks_c` varchar(1000) DEFAULT NULL COMMENT 'Y備考欄',
  `y_posturl_c` varchar(200) DEFAULT NULL COMMENT 'Y提案先URL',
  `reserve1_c` varchar(255) DEFAULT NULL COMMENT '予備1',
  `reserve2_c` varchar(255) DEFAULT NULL COMMENT '予備2',
  `reserve3_c` varchar(255) DEFAULT NULL COMMENT '予備3',
  `reserve4_c` varchar(255) DEFAULT NULL COMMENT '予備4',
  `reserve5_c` varchar(255) DEFAULT NULL COMMENT '予備5',
  `reserve6_c` varchar(255) DEFAULT NULL COMMENT '予備6',
  `reserve7_c` varchar(255) DEFAULT NULL COMMENT '予備7',
  `reserve8_c` varchar(255) DEFAULT NULL COMMENT '予備8',
  `reserve9_c` varchar(255) DEFAULT NULL COMMENT '予備9',
  `reserve10_c` varchar(255) DEFAULT NULL COMMENT '予備10',
  `calling_agent` varchar(20) DEFAULT NULL,
  `marginal_profit_c` double(11,0) DEFAULT NULL,
  PRIMARY KEY (`id_c`),
  KEY `idx_op_compe_c` (`op_compe_c`(10)),
  KEY `idx_op_step_c` (`op_step_c`(36)),
  KEY `idx_op_depend_c` (`op_depend_c`(10)),
  KEY `idx_anken_id_c` (`anken_id_c`),
  KEY `idx_opportunity_number_c` (`opportunity_number_c`),
  KEY `idx_op_cont_plandate_c` (`op_cont_plandate_c`),
  KEY `idx_op_commit_c` (`op_commit_c`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2432273 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_cstm`
--

LOCK TABLES `opportunities_cstm` WRITE;
/*!40000 ALTER TABLE `opportunities_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_cstm` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger opportunities_cstm_trigger before UPDATE on opportunities_cstm FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.closed_situation_c != new.closed_situation_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'closed_situation_c', 'enum', old.closed_situation_c, new.closed_situation_c, null, null);
    END IF;
    IF (old.emphasis_strategy1_c != new.emphasis_strategy1_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'emphasis_strategy1_c', 'multienum', old.emphasis_strategy1_c, new.emphasis_strategy1_c, null, new.op_owner_c);
    END IF;
    IF (old.emphasis_strategy2_c != new.emphasis_strategy2_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'emphasis_strategy2_c', 'multienum', old.emphasis_strategy2_c, new.emphasis_strategy2_c, null, new.op_owner_c);
    END IF;
    IF (old.measure_company_c != new.measure_company_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'measure_company_c', 'enum', old.measure_company_c, new.measure_company_c, null, null);
    END IF;
    IF (old.measure_dept_c != new.measure_dept_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'measure_dept_c', 'enum', old.measure_dept_c, new.measure_dept_c, null,	 null);
    END IF;
    IF (old.op_commit_c != new.op_commit_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'op_commit_c', 'enum', old.op_commit_c, new.op_commit_c, null, new.op_owner_c);
    END IF;
    IF (old.op_compe_c != new.op_compe_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'op_compe_c', 'enum', old.op_compe_c, new.op_compe_c, null, null);
    END IF;
    IF (old.op_cont_plandate_c != new.op_cont_plandate_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'op_cont_plandate_c', 'date', old.op_cont_plandate_c, new.op_cont_plandate_c, null, new.op_owner_c);
    END IF;
    IF (old.op_order_poss_c != new.op_order_poss_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'op_order_poss_c', 'enum', old.op_order_poss_c, new.op_order_poss_c, null, new.op_owner_c);
    END IF;
    IF (old.op_step_c != new.op_step_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c where opportunities.id = old.id_c), 'op_step_c', 'enum', old.op_step_c, new.op_step_c, null, new.op_owner_c);
    END IF;
    IF (old.doc_id_c != new.doc_id_c) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c WHERE opportunities.id  = old.id_c), 'doc_id_c', 'enum', old.doc_id_c, new.doc_id_c, null, null);
    END IF;
    IF ((old.marginal_profit_c != new.marginal_profit_c) || ((old.marginal_profit_c IS NULL) != (new.marginal_profit_c IS NULL))) THEN 
      INSERT INTO opportunities_audit values(UUID(), old.id_c, current, (SELECT modified_user_id FROM opportunities inner join opportunities_cstm on opportunities.id = opportunities_cstm.id_c WHERE opportunities.id  = old.id_c), 'marginal_profit_c', 'int', old.marginal_profit_c, new.marginal_profit_c, null, null);
    END IF;
    SET new.calling_agent = '';
	SET new.op_owner_c = null;
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `opportunities_users`
--

DROP TABLE IF EXISTS `opportunities_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_users` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_opp_usr` (`user_id`),
  KEY `idx_usr_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_users` (`opportunity_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_users`
--

LOCK TABLES `opportunities_users` WRITE;
/*!40000 ALTER TABLE `opportunities_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_email`
--

DROP TABLE IF EXISTS `outbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(6) NOT NULL DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) NOT NULL DEFAULT 'sendmail',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT NULL,
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` tinyint(1) DEFAULT '0',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  PRIMARY KEY (`id`),
  KEY `oe_user_id_idx` (`id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_email`
--

LOCK TABLES `outbound_email` WRITE;
/*!40000 ALTER TABLE `outbound_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `outbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `estimated_start_date` date NOT NULL,
  `estimated_end_date` date NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `project_id` char(36) DEFAULT NULL,
  `project_task_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `duration_unit` text NOT NULL,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT '0',
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task_audit`
--

DROP TABLE IF EXISTS `project_task_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_audit`
--

LOCK TABLES `project_task_audit` WRITE;
/*!40000 ALTER TABLE `project_task_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_accounts`
--

DROP TABLE IF EXISTS `projects_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_accounts`
--

LOCK TABLES `projects_accounts` WRITE;
/*!40000 ALTER TABLE `projects_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_bugs`
--

DROP TABLE IF EXISTS `projects_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_bugs`
--

LOCK TABLES `projects_bugs` WRITE;
/*!40000 ALTER TABLE `projects_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_cases`
--

DROP TABLE IF EXISTS `projects_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_cases`
--

LOCK TABLES `projects_cases` WRITE;
/*!40000 ALTER TABLE `projects_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_contacts`
--

DROP TABLE IF EXISTS `projects_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_contacts`
--

LOCK TABLES `projects_contacts` WRITE;
/*!40000 ALTER TABLE `projects_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_opportunities`
--

DROP TABLE IF EXISTS `projects_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_opportunities`
--

LOCK TABLES `projects_opportunities` WRITE;
/*!40000 ALTER TABLE `projects_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_products`
--

DROP TABLE IF EXISTS `projects_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_products`
--

LOCK TABLES `projects_products` WRITE;
/*!40000 ALTER TABLE `projects_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_list_campaigns`
--

DROP TABLE IF EXISTS `prospect_list_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_list_campaigns`
--

LOCK TABLES `prospect_list_campaigns` WRITE;
/*!40000 ALTER TABLE `prospect_list_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_list_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists`
--

DROP TABLE IF EXISTS `prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(25) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists`
--

LOCK TABLES `prospect_lists` WRITE;
/*!40000 ALTER TABLE `prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists_prospects`
--

DROP TABLE IF EXISTS `prospect_lists_prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists_prospects`
--

LOCK TABLES `prospect_lists_prospects` WRITE;
/*!40000 ALTER TABLE `prospect_lists_prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists_prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospects`
--

DROP TABLE IF EXISTS `prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(5) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(25) DEFAULT NULL,
  `phone_mobile` varchar(25) DEFAULT NULL,
  `phone_work` varchar(25) DEFAULT NULL,
  `phone_other` varchar(25) DEFAULT NULL,
  `phone_fax` varchar(25) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(25) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospects`
--

LOCK TABLES `prospects` WRITE;
/*!40000 ALTER TABLE `prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationships`
--

DROP TABLE IF EXISTS `relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(64) DEFAULT NULL,
  `join_key_lhs` varchar(64) DEFAULT NULL,
  `join_key_rhs` varchar(64) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_rel_name` (`relationship_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationships`
--

LOCK TABLES `relationships` WRITE;
/*!40000 ALTER TABLE `relationships` DISABLE KEYS */;
/*!40000 ALTER TABLE `relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releases`
--

DROP TABLE IF EXISTS `releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releases`
--

LOCK TABLES `releases` WRITE;
/*!40000 ALTER TABLE `releases` DISABLE KEYS */;
/*!40000 ALTER TABLE `releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modules`
--

DROP TABLE IF EXISTS `roles_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modules`
--

LOCK TABLES `roles_modules` WRITE;
/*!40000 ALTER TABLE `roles_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_search`
--

DROP TABLE IF EXISTS `saved_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_search`
--

LOCK TABLES `saved_search` WRITE;
/*!40000 ALTER TABLE `saved_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers`
--

DROP TABLE IF EXISTS `schedulers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `job` varchar(255) NOT NULL,
  `date_time_start` datetime NOT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) NOT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers`
--

LOCK TABLES `schedulers` WRITE;
/*!40000 ALTER TABLE `schedulers` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedulers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers_times`
--

DROP TABLE IF EXISTS `schedulers_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers_times` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `scheduler_id` char(36) NOT NULL,
  `execute_time` datetime NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'ready',
  PRIMARY KEY (`id`),
  KEY `idx_scheduler_id` (`scheduler_id`,`execute_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers_times`
--

LOCK TABLES `schedulers_times` WRITE;
/*!40000 ALTER TABLE `schedulers_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedulers_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `se_skill_viewer`
--

DROP TABLE IF EXISTS `se_skill_viewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `se_skill_viewer` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `created_by` varchar(36) DEFAULT NULL COMMENT '作成者',
  `modified_user_id` varchar(36) DEFAULT NULL COMMENT '更新ユーザID',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `anken_id_c` varchar(20) DEFAULT NULL COMMENT '案件ID',
  `opp_salessup_id` varchar(36) DEFAULT NULL COMMENT '営業サポートCD',
  `assigned_user_id` varchar(36) DEFAULT NULL COMMENT 'SE社員番号',
  `req_opp_class_cd` varchar(255) DEFAULT NULL COMMENT '依頼案件種別CD',
  `category_cd` varchar(4) DEFAULT NULL COMMENT 'カテゴリCD',
  PRIMARY KEY (`id`),
  KEY `idx_assigned_user_id` (`assigned_user_id`),
  KEY `idx_req_opp_class_cd` (`req_opp_class_cd`),
  KEY `idx_category_cd` (`category_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEスキルビューア';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `se_skill_viewer`
--

LOCK TABLES `se_skill_viewer` WRITE;
/*!40000 ALTER TABLE `se_skill_viewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `se_skill_viewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selistviewer`
--

DROP TABLE IF EXISTS `selistviewer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selistviewer` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `id` varchar(36) NOT NULL COMMENT 'SE案件CD',
  `name` varchar(255) DEFAULT NULL COMMENT 'SE案件名称',
  `date_entered` datetime DEFAULT NULL COMMENT 'SE案件入力日',
  `date_modified` datetime DEFAULT NULL COMMENT 'SE案件更新日',
  `modified_user_id` varchar(36) DEFAULT NULL COMMENT 'SE案件更新者ID',
  `created_by` varchar(36) DEFAULT NULL COMMENT 'SE案件作成者ID',
  `description` varchar(255) DEFAULT NULL COMMENT 'SE案件詳細',
  `deleted` tinyint(1) DEFAULT NULL COMMENT 'SE案件削除済み',
  `mast_organizationunit_id` varchar(36) DEFAULT NULL COMMENT '本部ID',
  `mast_organizationunit_name` varchar(255) DEFAULT NULL COMMENT '本部',
  `mast_organizationunit2_id` varchar(36) DEFAULT NULL COMMENT '統括部ID',
  `mast_organizationunit2_name` varchar(255) DEFAULT NULL COMMENT '統括部',
  `mast_organizationunit3_id` varchar(36) DEFAULT NULL COMMENT '部ID',
  `mast_organizationunit3_name` varchar(255) DEFAULT NULL COMMENT '部',
  `mast_organizationunit4_id` varchar(36) DEFAULT NULL COMMENT '課ID',
  `mast_organizationunit4_name` varchar(255) DEFAULT NULL COMMENT '課',
  `se_assigned_user_id` varchar(36) DEFAULT NULL COMMENT 'SE案件アサインユーザID',
  `status` varchar(100) DEFAULT NULL COMMENT 'ステータスNo',
  `status_desc` varchar(255) DEFAULT NULL COMMENT 'ステータス',
  `seorderaccuracy` varchar(100) DEFAULT NULL COMMENT 'SE受注確度(%)',
  `projectname` varchar(255) DEFAULT NULL COMMENT 'プロジェクト名',
  `sestrategicclassification` varchar(255) DEFAULT NULL COMMENT 'SE戦略分類No',
  `sestrategicclassification_desc` varchar(1000) DEFAULT NULL COMMENT 'SE戦略分類',
  `seitemsummary` varchar(1000) DEFAULT NULL COMMENT 'SE案件概要',
  `salestempnum` double(11,0) DEFAULT NULL COMMENT '売上(一時金)',
  `salesmonthnum` double(11,0) DEFAULT NULL COMMENT '売上(月額)',
  `contracttermmonthlybasis` int(3) DEFAULT NULL COMMENT '契約期間(ヶ月)',
  `pssalestempnum` double(11,0) DEFAULT NULL COMMENT 'PS売上(一時金)',
  `pssalesmonthnum` double(11,0) DEFAULT NULL COMMENT 'PS売上(月額)',
  `psbudgetschedule` date DEFAULT NULL COMMENT 'PS計上予定時期',
  `causeorderdfailure` varchar(1000) DEFAULT NULL COMMENT '受注失注理由',
  `spare` varchar(1000) DEFAULT NULL COMMENT '予備',
  `salessup_assigned_user_id` varchar(36) DEFAULT NULL COMMENT '営業サポート担当者ID',
  `salessup_assigned_user_name` varchar(255) DEFAULT NULL COMMENT '営業サポート担当者名',
  `salessup_organization_id` varchar(36) DEFAULT NULL COMMENT '営業サポート組織CD',
  `salessup_organization_name` varchar(255) DEFAULT NULL COMMENT '営業サポート組織名',
  `role_proposed` varchar(255) DEFAULT NULL COMMENT '役割No',
  `role_proposed_desc` varchar(255) DEFAULT NULL COMMENT '役割',
  `ps_sales` double(11,0) DEFAULT '0' COMMENT 'PS売上',
  `opp_id` varchar(36) NOT NULL COMMENT '案件CD',
  `opp_name` varchar(255) DEFAULT NULL COMMENT '案件名',
  `assigned_user_id` varchar(36) DEFAULT NULL COMMENT '営業担当者ID',
  `assigned_user_name` varchar(255) DEFAULT NULL COMMENT '営業担当者名',
  `organization_id` varchar(36) DEFAULT NULL COMMENT '営業組織CD',
  `organization_name` varchar(255) DEFAULT NULL COMMENT '営業組織名',
  `anken_id_c` varchar(20) DEFAULT NULL COMMENT '案件ID',
  `op_cont_plandate_c` date DEFAULT NULL COMMENT '契約予定日',
  `accounts_id` varchar(36) NOT NULL COMMENT '企業番号',
  `accounts_name` varchar(150) DEFAULT NULL COMMENT '企業名称',
  `date_entered_fmt` date DEFAULT NULL COMMENT '作成日(補正後)',
  `date_entered_fmt2` datetime DEFAULT NULL COMMENT '作成日時(補正後)',
  `entered_cd` varchar(36) DEFAULT NULL COMMENT '作成者コード',
  `entered_name` varchar(255) DEFAULT NULL COMMENT '作成者名',
  `date_modified_fmt` date DEFAULT NULL COMMENT '更新日(補正後)',
  `date_modified_fmt2` datetime DEFAULT NULL COMMENT '更新日時(補正後)',
  `modified_cd` varchar(36) DEFAULT NULL COMMENT '更新者コード',
  `modified_name` varchar(255) DEFAULT NULL COMMENT '更新者名',
  PRIMARY KEY (`p_id`),
  KEY `p_id` (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SE案件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selistviewer`
--

LOCK TABLES `selistviewer` WRITE;
/*!40000 ALTER TABLE `selistviewer` DISABLE KEYS */;
/*!40000 ALTER TABLE `selistviewer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selistviewer_audit`
--

DROP TABLE IF EXISTS `selistviewer_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `selistviewer_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  KEY `idx_selistviewer_primary` (`id`),
  KEY `idx_selistviewer_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selistviewer_audit`
--

LOCK TABLES `selistviewer_audit` WRITE;
/*!40000 ALTER TABLE `selistviewer_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `selistviewer_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistical_information`
--

DROP TABLE IF EXISTS `statistical_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistical_information` (
  `user_name` varchar(60) NOT NULL COMMENT '社員番号',
  `spm_orgunit_cd` varchar(8) DEFAULT '' COMMENT 'SPM組織コード',
  `op_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '日付',
  `pc_mb` int(1) DEFAULT '0' COMMENT 'PC or MB',
  `module` varchar(36) DEFAULT NULL COMMENT 'モジュール名',
  `action` varchar(36) DEFAULT NULL COMMENT 'アクション名',
  `target_module` varchar(36) DEFAULT NULL COMMENT 'ターゲットモジュール名',
  `ope_type` int(1) DEFAULT NULL COMMENT '操作',
  `recode` varchar(36) DEFAULT NULL COMMENT 'レコードID',
  `param` varchar(512) DEFAULT NULL COMMENT 'パラメータ',
  `reserve1` varchar(256) DEFAULT NULL COMMENT '予備1',
  `reserve2` varchar(256) DEFAULT NULL COMMENT '予備2',
  `reserve3` varchar(256) DEFAULT NULL COMMENT '予備3',
  `reserve4` varchar(256) DEFAULT NULL COMMENT '予備4',
  `reserve5` varchar(256) DEFAULT NULL COMMENT '予備5',
  KEY `idx_user_name` (`user_name`),
  KEY `idx_spm_orgunit_cd` (`spm_orgunit_cd`),
  KEY `idx_op_date` (`op_date`),
  KEY `idx_module` (`module`),
  KEY `idx_action` (`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='統計情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistical_information`
--

LOCK TABLES `statistical_information` WRITE;
/*!40000 ALTER TABLE `statistical_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistical_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sugarfeed`
--

DROP TABLE IF EXISTS `sugarfeed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sugarfeed`
--

LOCK TABLES `sugarfeed` WRITE;
/*!40000 ALTER TABLE `sugarfeed` DISABLE KEYS */;
/*!40000 ALTER TABLE `sugarfeed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sv_cancel`
--

DROP TABLE IF EXISTS `sv_cancel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_cancel` (
  `id` char(36) NOT NULL COMMENT '解約サービスCD',
  `name` varchar(255) NOT NULL COMMENT '名称(未使用)',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` text COMMENT '詳細(未使用)',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先(未使用)',
  `class` varchar(20) NOT NULL COMMENT '種別',
  `cate` varchar(20) NOT NULL COMMENT 'サービス分類',
  `month_num` double(11,0) NOT NULL COMMENT '月額',
  `temp_num` double(11,0) NOT NULL COMMENT '一時金',
  `line_num` double(11,0) NOT NULL COMMENT '回線数',
  `yyyy` varchar(4) NOT NULL COMMENT '提供予定年',
  `mm` varchar(2) NOT NULL COMMENT '提供予定月',
  `billiard_form` varchar(100) DEFAULT NULL COMMENT '新規区分',
  `billiard_model_code` varchar(20) DEFAULT NULL COMMENT '機種コード',
  `billiard_maker` varchar(20) DEFAULT NULL COMMENT 'メーカー',
  `billiard_model` varchar(20) DEFAULT NULL COMMENT '機種',
  `billiard_color` varchar(20) DEFAULT NULL COMMENT '色',
  `month_num_variable_profits` double(11,0) NOT NULL DEFAULT '0' COMMENT '月額（変動利益）',
  `temp_num_variable_profits` double(11,0) NOT NULL DEFAULT '0' COMMENT '一時金（変動利益）',
  `posted_month_age` int(3) DEFAULT NULL COMMENT '掲載期間（ヶ月）',
  `billiard_choice_day` date DEFAULT NULL COMMENT '玉繰り希望日',
  `billiard_memo` varchar(255) DEFAULT NULL COMMENT '玉繰りメモ',
  `calling_agent` varchar(20) DEFAULT NULL,
  `month_num_local_currency` double(11,2) NOT NULL DEFAULT '0.00' COMMENT '月額（現地通貨）',
  `month_num_unit` varchar(5) DEFAULT NULL COMMENT '月額（現地通貨）単位',
  `temp_num_local_currency` double(11,2) NOT NULL DEFAULT '0.00' COMMENT '一時金（現地通貨）',
  `temp_num_unit` varchar(5) DEFAULT NULL COMMENT '一時金（現地通貨）単位',
  `month_age` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cate` (`cate`),
  KEY `idx_class` (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='解約サービス';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_cancel`
--

LOCK TABLES `sv_cancel` WRITE;
/*!40000 ALTER TABLE `sv_cancel` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_cancel` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger sv_cancel_trigger before UPDATE on sv_cancel FOR each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.cate != new.cate) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'cate', 'varchar', old.cate, new.cate, null, null);
    END IF;
    IF (old.line_num != new.line_num) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'line_num', 'int', old.line_num, new.line_num, null, null);
    END IF;
    IF (old.month_num != new.month_num) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'month_num', 'int', old.month_num, new.month_num, null, null);
    END IF;
    IF (old.month_num_variable_profits != new.month_num_variable_profits) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'month_num_variable_profits', 'int', old.month_num_variable_profits, new.month_num_variable_profits, null, null);
    END IF;
    IF (old.temp_num != new.temp_num) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'temp_num', 'int', old.temp_num, new.temp_num, null, null);
    END IF;
    IF (old.temp_num_variable_profits != new.temp_num_variable_profits) THEN
      INSERT INTO sv_cancel_audit values(UUID(), old.id, current, new.modified_user_id, 'temp_num_variable_profits', 'int', old.temp_num_variable_profits, new.temp_num_variable_profits, null, null);
    END IF;
  SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sv_cancel_audit`
--

DROP TABLE IF EXISTS `sv_cancel_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_cancel_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_cancel_audit`
--

LOCK TABLES `sv_cancel_audit` WRITE;
/*!40000 ALTER TABLE `sv_cancel_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_cancel_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sv_cancel_old`
--

DROP TABLE IF EXISTS `sv_cancel_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_cancel_old` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `class` varchar(100) NOT NULL,
  `cate` varchar(100) NOT NULL,
  `month_num` int(11) DEFAULT NULL,
  `temp_num` int(11) DEFAULT NULL,
  `line_num` int(11) DEFAULT NULL,
  `yyyy` varchar(100) NOT NULL,
  `mm` varchar(100) NOT NULL,
  `billiard_form` varchar(100) DEFAULT NULL,
  `billiard_model_code` varchar(20) DEFAULT NULL,
  `billiard_maker` varchar(20) DEFAULT NULL,
  `billiard_model` varchar(20) DEFAULT NULL,
  `billiard_color` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_cancel_old`
--

LOCK TABLES `sv_cancel_old` WRITE;
/*!40000 ALTER TABLE `sv_cancel_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_cancel_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sv_cancel_opportunities`
--

DROP TABLE IF EXISTS `sv_cancel_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_cancel_opportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `sv_cancel_ida` varchar(36) DEFAULT NULL COMMENT '解約サービスCD',
  `opportunities_idb` varchar(36) DEFAULT NULL COMMENT '案件CD',
  PRIMARY KEY (`id`),
  KEY `sv_cancel_opportunities_ida1` (`sv_cancel_ida`),
  KEY `sv_cancel_opportunities_idb2` (`opportunities_idb`),
  KEY `sv_cancel_opportunities_idc3` (`sv_cancel_ida`,`opportunities_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_cancel_opportunities`
--

LOCK TABLES `sv_cancel_opportunities` WRITE;
/*!40000 ALTER TABLE `sv_cancel_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_cancel_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sv_suggest`
--

DROP TABLE IF EXISTS `sv_suggest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_suggest` (
  `id` char(36) NOT NULL COMMENT '提案サービスCD',
  `name` varchar(255) NOT NULL COMMENT '名称(未使用)',
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者ID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者ID',
  `description` text COMMENT '詳細(未使用)',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) DEFAULT NULL COMMENT 'アサイン先(未使用)',
  `class` varchar(20) NOT NULL COMMENT '種別',
  `cate` varchar(20) NOT NULL COMMENT 'サービス分類',
  `month_num` double(11,0) NOT NULL COMMENT '月額',
  `temp_num` double(11,0) NOT NULL COMMENT '一時金',
  `line_num` double(11,0) NOT NULL COMMENT '回線数',
  `yyyy` varchar(4) NOT NULL COMMENT '提供予定年',
  `mm` varchar(2) NOT NULL COMMENT '提供予定月',
  `billiard_form` varchar(100) DEFAULT NULL COMMENT '新規区分',
  `billiard_model_code` varchar(20) DEFAULT NULL COMMENT '機種コード',
  `billiard_maker` varchar(20) DEFAULT NULL COMMENT 'メーカー',
  `billiard_model` varchar(20) DEFAULT NULL COMMENT '機種',
  `billiard_color` varchar(20) DEFAULT NULL COMMENT '色',
  `month_num_variable_profits` double(11,0) NOT NULL DEFAULT '0' COMMENT '月額（変動利益）',
  `temp_num_variable_profits` double(11,0) NOT NULL DEFAULT '0' COMMENT '一時金（変動利益）',
  `posted_month_age` int(3) DEFAULT NULL COMMENT '掲載期間（ヶ月）',
  `billiard_choice_day` date DEFAULT NULL COMMENT '玉繰り希望日',
  `billiard_memo` varchar(255) DEFAULT NULL COMMENT '玉繰りメモ',
  `calling_agent` varchar(20) DEFAULT NULL,
  `month_num_local_currency` double(11,2) NOT NULL DEFAULT '0.00' COMMENT '月額（現地通貨）',
  `month_num_unit` varchar(5) DEFAULT NULL COMMENT '月額（現地通貨）単位',
  `temp_num_local_currency` double(11,2) NOT NULL DEFAULT '0.00' COMMENT '一時金（現地通貨）',
  `temp_num_unit` varchar(5) DEFAULT NULL COMMENT '一時金（現地通貨）単位',
  `month_age` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cate` (`cate`),
  KEY `idx_class` (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提案サービス';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_suggest`
--

LOCK TABLES `sv_suggest` WRITE;
/*!40000 ALTER TABLE `sv_suggest` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_suggest` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
delimiter ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger sv_suggest_trigger before UPDATE on sv_suggest for each row
BEGIN
  DECLARE current DATETIME;
  IF (new.calling_agent = 'ipadsfa') THEN
    SET current = UTC_TIMESTAMP();
    IF (old.cate != new.cate) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'cate', 'varchar', old.cate, new.cate, null, null);
    END IF;
    IF (old.line_num != new.line_num) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'line_num', 'int', old.line_num, new.line_num, null, null);
    END IF;
    IF (old.month_num != new.month_num) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'month_num', 'int', old.month_num, new.month_num, null, null);
    END IF;
    IF (old.month_num_variable_profits != new.month_num_variable_profits) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'month_num_variable_profits', 'int', old.month_num_variable_profits, new.month_num_variable_profits, null, null);
    END IF;
    IF (old.temp_num != new.temp_num) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'temp_num', 'int', old.temp_num,new.temp_num, null, null);
    END IF;
    IF (old.temp_num_variable_profits != new.temp_num_variable_profits) THEN 
      INSERT INTO sv_suggest_audit values(UUID(), old.id, current, new.modified_user_id, 'temp_num_variable_profits', 'int', old.temp_num_variable_profits, new.temp_num_variable_profits, null, null);
    END IF;
    SET new.calling_agent = '';
  END IF;
END */;;
delimiter ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sv_suggest_audit`
--

DROP TABLE IF EXISTS `sv_suggest_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_suggest_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_suggest_audit`
--

LOCK TABLES `sv_suggest_audit` WRITE;
/*!40000 ALTER TABLE `sv_suggest_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_suggest_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sv_suggest_opportunities`
--

DROP TABLE IF EXISTS `sv_suggest_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sv_suggest_opportunities` (
  `id` varchar(36) NOT NULL COMMENT 'ID',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `sv_suggest_ida` varchar(36) DEFAULT NULL COMMENT '提案サービスCD',
  `opportunities_idb` varchar(36) DEFAULT NULL COMMENT '案件CD',
  PRIMARY KEY (`id`),
  KEY `sv_suggest_pportunities_ida1` (`sv_suggest_ida`),
  KEY `sv_suggest_pportunities_idb2` (`opportunities_idb`),
  KEY `sv_suggest_pportunities_idc3` (`sv_suggest_ida`,`opportunities_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sv_suggest_opportunities`
--

LOCK TABLES `sv_suggest_opportunities` WRITE;
/*!40000 ALTER TABLE `sv_suggest_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `sv_suggest_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` char(36) NOT NULL COMMENT 'ToDoID',
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL COMMENT '入力日',
  `date_modified` datetime DEFAULT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新ユーザID',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `description` varchar(128) NOT NULL COMMENT '内容',
  `deleted` tinyint(1) DEFAULT '0' COMMENT '削除済み',
  `assigned_user_id` char(36) NOT NULL COMMENT '担当者',
  `status` varchar(25) NOT NULL DEFAULT 'Planned' COMMENT 'ステータス',
  `date_due_flag` tinyint(1) DEFAULT '1',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '1',
  `date_start` date NOT NULL COMMENT '日付(予定日)',
  `parent_type` varchar(25) DEFAULT NULL COMMENT '関連先Type',
  `parent_id` char(36) DEFAULT NULL COMMENT '関連先ID',
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker`
--

DROP TABLE IF EXISTS `tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`date_modified`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY RANGE (YEAR(date_modified))
(PARTITION p2009 VALUES LESS THAN (2010) ENGINE = InnoDB,
 PARTITION p2010 VALUES LESS THAN (2011) ENGINE = InnoDB,
 PARTITION p2011 VALUES LESS THAN (2012) ENGINE = InnoDB,
 PARTITION pnow VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker`
--

LOCK TABLES `tracker` WRITE;
/*!40000 ALTER TABLE `tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker_2010`
--

DROP TABLE IF EXISTS `tracker_2010`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker_2010` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(255) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker_2010`
--

LOCK TABLES `tracker_2010` WRITE;
/*!40000 ALTER TABLE `tracker_2010` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracker_2010` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker_2011`
--

DROP TABLE IF EXISTS `tracker_2011`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker_2011` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action` varchar(255) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker_2011`
--

LOCK TABLES `tracker_2011` WRITE;
/*!40000 ALTER TABLE `tracker_2011` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracker_2011` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_history`
--

DROP TABLE IF EXISTS `upgrade_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` text,
  `date_entered` datetime NOT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_history`
--

LOCK TABLES `upgrade_history` WRITE;
/*!40000 ALTER TABLE `upgrade_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgrade_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  `contents` text,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) NOT NULL COMMENT 'ユーザCD',
  `user_name` varchar(60) DEFAULT NULL COMMENT 'ユーザ名（社員番号）',
  `user_hash` varchar(32) DEFAULT NULL COMMENT '（未使用）',
  `authenticate_id` varchar(100) DEFAULT NULL COMMENT '（未使用）',
  `sugar_login` tinyint(1) DEFAULT '1' COMMENT 'Sugarユーザ',
  `first_name` varchar(30) DEFAULT NULL COMMENT '名',
  `last_name` varchar(30) DEFAULT NULL COMMENT '姓',
  `reports_to_id` char(36) DEFAULT NULL COMMENT '上司ID（社員番号）',
  `is_admin` tinyint(1) DEFAULT '0' COMMENT '管理者',
  `receive_notifications` tinyint(1) DEFAULT '1' COMMENT 'アサインの通知',
  `description` text COMMENT '詳細',
  `date_entered` datetime NOT NULL COMMENT '入力日',
  `date_modified` datetime NOT NULL COMMENT '更新日',
  `modified_user_id` char(36) DEFAULT NULL COMMENT '更新者',
  `created_by` char(36) DEFAULT NULL COMMENT '作成者',
  `title` varchar(50) DEFAULT NULL COMMENT '職位',
  `department` varchar(50) DEFAULT NULL COMMENT '部署（未使用）',
  `phone_home` varchar(50) DEFAULT NULL COMMENT '自宅の電話番号（未使用）',
  `phone_mobile` varchar(50) DEFAULT NULL COMMENT '携帯電話（未使用）',
  `phone_work` varchar(50) DEFAULT NULL COMMENT '勤務先電話番号（未使用）',
  `phone_other` varchar(50) DEFAULT NULL COMMENT '他の電話番号（未使用）',
  `phone_fax` varchar(50) DEFAULT NULL COMMENT 'FAX（未使用）',
  `status` varchar(25) DEFAULT NULL COMMENT 'ステータス',
  `address_street` varchar(150) DEFAULT NULL COMMENT '住所 番地（未使用）',
  `address_city` varchar(100) DEFAULT NULL COMMENT '住所 市区町村（未使用）',
  `address_state` varchar(100) DEFAULT NULL COMMENT '住所 都道府県（未使用）',
  `address_country` varchar(25) DEFAULT NULL COMMENT '住所 国（未使用）',
  `address_postalcode` varchar(9) DEFAULT NULL COMMENT '住所 郵便番号（未使用）',
  `user_preferences` text COMMENT 'プリファレンス（未使用）',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `portal_only` tinyint(1) DEFAULT '0' COMMENT 'ポータルのみのユーザ（未使用）',
  `employee_status` varchar(25) DEFAULT NULL COMMENT '従業員のステータス（未使用）',
  `messenger_id` varchar(25) DEFAULT NULL COMMENT 'IM名（未使用）',
  `messenger_type` varchar(25) DEFAULT NULL COMMENT 'IMタイプ（未使用）',
  `is_group` tinyint(1) DEFAULT '0' COMMENT 'グループ（未使用）',
  `spm_stfcd` varchar(8) DEFAULT NULL COMMENT 'SPMスタッフコード',
  `spm_orgunit_cd` varchar(8) DEFAULT NULL COMMENT 'SPM組織コード',
  `spm_orgunit_name` varchar(100) DEFAULT NULL COMMENT 'SPM組織名称',
  `orgunit_cd` varchar(20) DEFAULT NULL COMMENT 'SBTM組織コード',
  `orgunit_name` varchar(100) DEFAULT NULL COMMENT 'SBTM組織名称（未使用）',
  `tanto_cd` varchar(15) NOT NULL COMMENT '担当者コード',
  `str_kojin_id` varchar(20) DEFAULT NULL COMMENT 'STRAIGHT個人ID',
  `group_tanto_cd` varchar(7) DEFAULT NULL COMMENT '共通社員番号',
  `kaisha_cd` varchar(4) DEFAULT NULL COMMENT '原籍会社コード',
  `sbb_tanto_cd` varchar(30) DEFAULT NULL COMMENT '従業員番号（SBB）',
  `sbm_tanto_cd` varchar(30) DEFAULT NULL COMMENT '従業員番号（SBM）',
  `sbtm_tanto_cd` varchar(30) DEFAULT NULL COMMENT '従業員番号（SBTM）',
  `div_cd` varchar(11) DEFAULT NULL COMMENT '人事部署コード',
  `div_nm` varchar(256) DEFAULT NULL COMMENT '人事部署部署名称',
  `div_unit_id` varchar(40) DEFAULT NULL COMMENT '組織単位ID',
  `mng_tanto_cd` varchar(15) DEFAULT NULL COMMENT '責任者担当者コード',
  `mng_grptanto_cd` varchar(15) DEFAULT NULL COMMENT '責任者共通社員番号',
  `phone_no` varchar(20) DEFAULT NULL COMMENT '緊急連絡先',
  `agent_flg` char(1) DEFAULT NULL COMMENT '代理店フラグ',
  `agent_cd` varchar(20) DEFAULT NULL COMMENT '代理店コード',
  `gmail_username` varchar(255) DEFAULT NULL,
  `gmail_password` varchar(255) DEFAULT NULL,
  `eigyo_sosiki_short_nm` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`),
  KEY `idx_spm_orgunit_name` (`spm_orgunit_name`(60)),
  KEY `idx_spm_stfcd` (`spm_stfcd`),
  KEY `idx_spm_orgunit_cd` (`spm_orgunit_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_cstm`
--

DROP TABLE IF EXISTS `users_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_cstm` (
  `id_c` char(36) NOT NULL COMMENT 'ユーザCD',
  `google_mmail_c` varchar(255) DEFAULT NULL COMMENT 'Gmailアドレス',
  `google_mcalls_c` varchar(2) DEFAULT NULL COMMENT 'Googleカレンダー連携フラグ',
  `google_oauth_token_c` text,
  `sns_set_c` varchar(3) DEFAULT 'on',
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_cstm`
--

LOCK TABLES `users_cstm` WRITE;
/*!40000 ALTER TABLE `users_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_del`
--

DROP TABLE IF EXISTS `users_del`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_del` (
  `id` char(36) CHARACTER SET utf8 NOT NULL COMMENT 'ユーザ_ID',
  `user_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ユーザ名（社員番号）',
  `user_hash` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '（未使用）',
  `authenticate_id` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '（未使用）',
  `sugar_login` tinyint(1) DEFAULT '1' COMMENT 'Sugarユーザ',
  `first_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '名',
  `last_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '姓',
  `reports_to_id` char(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '上司ID（社員番号）',
  `is_admin` tinyint(1) DEFAULT '0' COMMENT '管理者',
  `receive_notifications` tinyint(1) DEFAULT '1' COMMENT 'アサインの通知',
  `description` text CHARACTER SET utf8 COMMENT '詳細',
  `date_entered` datetime NOT NULL COMMENT '入力日',
  `date_modified` datetime NOT NULL COMMENT '更新日',
  `modified_user_id` char(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',
  `created_by` char(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '作成者',
  `title` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '職位',
  `department` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '部署（未使用）',
  `phone_home` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '自宅の電話番号（未使用）',
  `phone_mobile` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '携帯電話（未使用）',
  `phone_work` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '勤務先電話番号（未使用）',
  `phone_other` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '他の電話番号（未使用）',
  `phone_fax` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'FAX（未使用）',
  `status` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT 'ステータス',
  `address_street` varchar(150) CHARACTER SET utf8 DEFAULT NULL COMMENT '住所 番地（未使用）',
  `address_city` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '住所 市区町村（未使用）',
  `address_state` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '住所 都道府県（未使用）',
  `address_country` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT '住所 国（未使用）',
  `address_postalcode` varchar(9) CHARACTER SET utf8 DEFAULT NULL COMMENT '住所 郵便番号（未使用）',
  `user_preferences` text CHARACTER SET utf8 COMMENT 'プリファレンス（未使用）',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '削除済み',
  `portal_only` tinyint(1) DEFAULT '0' COMMENT 'ポータルのみのユーザ（未使用）',
  `employee_status` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT '従業員のステータス（未使用）',
  `messenger_id` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT 'IM名（未使用）',
  `messenger_type` varchar(25) CHARACTER SET utf8 DEFAULT NULL COMMENT 'IMタイプ（未使用）',
  `is_group` tinyint(1) DEFAULT '0' COMMENT 'グループ（未使用）',
  `spm_stfcd` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SPMスタッフコード',
  `spm_orgunit_cd` varchar(8) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SPM組織コード',
  `spm_orgunit_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SPM組織名称',
  `orgunit_cd` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SBTM組織コード',
  `orgunit_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SBTM組織名称（未使用）',
  `tanto_cd` varchar(15) CHARACTER SET utf8 NOT NULL COMMENT '担当者コード',
  `str_kojin_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'STRAIGHT個人ID',
  `group_tanto_cd` varchar(7) CHARACTER SET utf8 DEFAULT NULL COMMENT '共通社員番号',
  `kaisha_cd` varchar(4) CHARACTER SET utf8 DEFAULT NULL COMMENT '原籍会社コード',
  `sbb_tanto_cd` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '従業員番号（SBB）',
  `sbm_tanto_cd` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '従業員番号（SBM）',
  `sbtm_tanto_cd` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '従業員番号（SBTM）',
  `div_cd` varchar(11) CHARACTER SET utf8 DEFAULT NULL COMMENT '人事部署コード',
  `div_nm` varchar(256) CHARACTER SET utf8 DEFAULT NULL COMMENT '人事部署部署名称',
  `div_unit_id` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '組織単位ID',
  `mng_tanto_cd` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '責任者担当者コード',
  `mng_grptanto_cd` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '責任者共通社員番号',
  `phone_no` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '緊急連絡先',
  `agent_flg` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT '代理店フラグ',
  `agent_cd` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '代理店コード',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_del`
--

LOCK TABLES `users_del` WRITE;
/*!40000 ALTER TABLE `users_del` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_del` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_feeds`
--

DROP TABLE IF EXISTS `users_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_feeds`
--

LOCK TABLES `users_feeds` WRITE;
/*!40000 ALTER TABLE `users_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_last_import`
--

DROP TABLE IF EXISTS `users_last_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `import_module` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_last_import`
--

LOCK TABLES `users_last_import` WRITE;
/*!40000 ALTER TABLE `users_last_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_last_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_mobile_pin`
--

DROP TABLE IF EXISTS `users_mobile_pin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_mobile_pin` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `pin_cd` varchar(50) DEFAULT NULL,
  `terminal_info` varchar(50) DEFAULT NULL,
  `passwd_hash` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_mobile_pin`
--

LOCK TABLES `users_mobile_pin` WRITE;
/*!40000 ALTER TABLE `users_mobile_pin` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_mobile_pin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_mobile_pin_audit`
--

DROP TABLE IF EXISTS `users_mobile_pin_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_mobile_pin_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_mobile_pin_audit`
--

LOCK TABLES `users_mobile_pin_audit` WRITE;
/*!40000 ALTER TABLE `users_mobile_pin_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_mobile_pin_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_password_link`
--

DROP TABLE IF EXISTS `users_password_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_password_link`
--

LOCK TABLES `users_password_link` WRITE;
/*!40000 ALTER TABLE `users_password_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_password_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_signatures`
--

DROP TABLE IF EXISTS `users_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_signatures`
--

LOCK TABLES `users_signatures` WRITE;
/*!40000 ALTER TABLE `users_signatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_signatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vcals`
--

DROP TABLE IF EXISTS `vcals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(25) DEFAULT NULL,
  `source` varchar(25) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vcals`
--

LOCK TABLES `vcals` WRITE;
/*!40000 ALTER TABLE `vcals` DISABLE KEYS */;
/*!40000 ALTER TABLE `vcals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vd_calls_listviewer`
--

DROP TABLE IF EXISTS `vd_calls_listviewer`;
/*!50001 DROP VIEW IF EXISTS `vd_calls_listviewer`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vd_calls_listviewer` (
  `id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `date_entered` tinyint NOT NULL,
  `date_modified` tinyint NOT NULL,
  `modified_user_id` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `assigned_user_id` tinyint NOT NULL,
  `mast_organizationunit_id` tinyint NOT NULL,
  `mast_organizationunit2_id` tinyint NOT NULL,
  `mast_organizationunit3_id` tinyint NOT NULL,
  `mast_organizationunit4_id` tinyint NOT NULL,
  `sales_staff_code` tinyint NOT NULL,
  `sales_staff_name` tinyint NOT NULL,
  `sales_group_code` tinyint NOT NULL,
  `sales_group_name` tinyint NOT NULL,
  `date_start_fmt` tinyint NOT NULL,
  `date_start_fmt2` tinyint NOT NULL,
  `call_name` tinyint NOT NULL,
  `parent_type_desc` tinyint NOT NULL,
  `account_id` tinyint NOT NULL,
  `account_name` tinyint NOT NULL,
  `opportunity_id` tinyint NOT NULL,
  `anken_id_c` tinyint NOT NULL,
  `opportunity_name` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `com_free_c` tinyint NOT NULL,
  `com_free_c20` tinyint NOT NULL,
  `date_entered_fmt` tinyint NOT NULL,
  `date_entered_fmt2` tinyint NOT NULL,
  `enter_staff_code` tinyint NOT NULL,
  `enter_staff_name` tinyint NOT NULL,
  `date_modified_fmt` tinyint NOT NULL,
  `date_modified_fmt2` tinyint NOT NULL,
  `update_staff_code` tinyint NOT NULL,
  `update_staff_name` tinyint NOT NULL,
  `update_group_code` tinyint NOT NULL,
  `update_group_name` tinyint NOT NULL,
  `pam_name_c` tinyint NOT NULL,
  `pam_sv_num_c` tinyint NOT NULL,
  `companion_c` tinyint NOT NULL,
  `time_end_c` tinyint NOT NULL,
  `division_c` tinyint NOT NULL,
  `contact_id_c` tinyint NOT NULL,
  `contact_first_name` tinyint NOT NULL,
  `contact_last_name` tinyint NOT NULL,
  `contact_name` tinyint NOT NULL,
  `contact_title_c` tinyint NOT NULL,
  `contact_department_c` tinyint NOT NULL,
  `outlook_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vd_calls_listviewer_audit`
--

DROP TABLE IF EXISTS `vd_calls_listviewer_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vd_calls_listviewer_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vd_calls_listviewer_audit`
--

LOCK TABLES `vd_calls_listviewer_audit` WRITE;
/*!40000 ALTER TABLE `vd_calls_listviewer_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `vd_calls_listviewer_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vd_calls_listviewer_del`
--

DROP TABLE IF EXISTS `vd_calls_listviewer_del`;
/*!50001 DROP VIEW IF EXISTS `vd_calls_listviewer_del`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vd_calls_listviewer_del` (
  `id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `date_entered` tinyint NOT NULL,
  `date_modified` tinyint NOT NULL,
  `modified_user_id` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `deleted` tinyint NOT NULL,
  `assigned_user_id` tinyint NOT NULL,
  `mast_organizationunit_id` tinyint NOT NULL,
  `mast_organizationunit2_id` tinyint NOT NULL,
  `mast_organizationunit3_id` tinyint NOT NULL,
  `mast_organizationunit4_id` tinyint NOT NULL,
  `sales_staff_code` tinyint NOT NULL,
  `sales_staff_name` tinyint NOT NULL,
  `sales_group_code` tinyint NOT NULL,
  `sales_group_name` tinyint NOT NULL,
  `date_start_fmt` tinyint NOT NULL,
  `date_start_fmt2` tinyint NOT NULL,
  `call_name` tinyint NOT NULL,
  `parent_type_desc` tinyint NOT NULL,
  `account_id` tinyint NOT NULL,
  `account_name` tinyint NOT NULL,
  `opportunity_id` tinyint NOT NULL,
  `anken_id_c` tinyint NOT NULL,
  `opportunity_name` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `com_free_c` tinyint NOT NULL,
  `com_free_c20` tinyint NOT NULL,
  `date_entered_fmt` tinyint NOT NULL,
  `date_entered_fmt2` tinyint NOT NULL,
  `enter_staff_code` tinyint NOT NULL,
  `enter_staff_name` tinyint NOT NULL,
  `date_modified_fmt` tinyint NOT NULL,
  `date_modified_fmt2` tinyint NOT NULL,
  `update_staff_code` tinyint NOT NULL,
  `update_staff_name` tinyint NOT NULL,
  `update_group_code` tinyint NOT NULL,
  `update_group_name` tinyint NOT NULL,
  `pam_name_c` tinyint NOT NULL,
  `pam_sv_num_c` tinyint NOT NULL,
  `companion_c` tinyint NOT NULL,
  `time_end_c` tinyint NOT NULL,
  `division_c` tinyint NOT NULL,
  `contact_id_c` tinyint NOT NULL,
  `contact_first_name` tinyint NOT NULL,
  `contact_last_name` tinyint NOT NULL,
  `contact_name` tinyint NOT NULL,
  `contact_title_c` tinyint NOT NULL,
  `contact_department_c` tinyint NOT NULL,
  `outlook_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `file_version` varchar(255) NOT NULL,
  `db_version` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_version` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_anken_list_audit`
--

DROP TABLE IF EXISTS `view_anken_list_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_anken_list_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_anken_list_audit`
--

LOCK TABLES `view_anken_list_audit` WRITE;
/*!40000 ALTER TABLE `view_anken_list_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `view_anken_list_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_anken_list_fy10_audit`
--

DROP TABLE IF EXISTS `view_anken_list_fy10_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_anken_list_fy10_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_anken_list_fy10_audit`
--

LOCK TABLES `view_anken_list_fy10_audit` WRITE;
/*!40000 ALTER TABLE `view_anken_list_fy10_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `view_anken_list_fy10_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_opp2_fy10`
--

DROP TABLE IF EXISTS `view_opp2_fy10`;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp2_fy10` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `SV_DATE_MODIFIED_FMT` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT1_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_NM` tinyint NOT NULL,
  `OP_CONT_PLANMONTH` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `OP_MANAGE_FORECAST` tinyint NOT NULL,
  `SPECIAL_PRI` tinyint NOT NULL,
  `SPECIAL_BRI` tinyint NOT NULL,
  `SPECIAL_PRI1` tinyint NOT NULL,
  `SPECIAL_PRI20` tinyint NOT NULL,
  `DATA_PHYSICS` tinyint NOT NULL,
  `DATA_CHANGE` tinyint NOT NULL,
  `DATA_GET_PHYSICS` tinyint NOT NULL,
  `DATA_GET_CHANGE` tinyint NOT NULL,
  `MOBILE` tinyint NOT NULL,
  `HS_SALE_END` tinyint NOT NULL,
  `DATA_CARD_SALE_END` tinyint NOT NULL,
  `SMART_PHONE_SALE_END` tinyint NOT NULL,
  `HS_ALLOTMENT` tinyint NOT NULL,
  `DATA_CARD_ALLOTMENT` tinyint NOT NULL,
  `SMART_PHONE_ALLOTMENT` tinyint NOT NULL,
  `HS_RENTAL` tinyint NOT NULL,
  `DATA_CARD_RENTAL` tinyint NOT NULL,
  `SMART_PHONE_RENTAL` tinyint NOT NULL,
  `PHS` tinyint NOT NULL,
  `WHITE_OFFERS` tinyint NOT NULL,
  `MODEL_CHANGE` tinyint NOT NULL,
  `ML` tinyint NOT NULL,
  `CLOUD_PHYSICS` tinyint NOT NULL,
  `CLOUD_CHANGE` tinyint NOT NULL,
  `YAHOO_PHYSICS` tinyint NOT NULL,
  `YAHOO_CHANGE` tinyint NOT NULL,
  `HS` tinyint NOT NULL,
  `SMART_PHONE` tinyint NOT NULL,
  `DATA_CARD` tinyint NOT NULL,
  `IPAD` tinyint NOT NULL,
  `GOOGLEAPPS` tinyint NOT NULL,
  `O2O` tinyint NOT NULL,
  `PAYPAL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp2_fy10_2`
--

DROP TABLE IF EXISTS `view_opp2_fy10_2`;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp2_fy10_2` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp2_fy10_del`
--

DROP TABLE IF EXISTS `view_opp2_fy10_del`;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_del`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp2_fy10_del` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `SV_DATE_MODIFIED_FMT` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT1_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_NM` tinyint NOT NULL,
  `OP_CONT_PLANMONTH` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `OP_MANAGE_FORECAST` tinyint NOT NULL,
  `MONTH_AGE` tinyint NOT NULL,
  `SPECIAL_PRI` tinyint NOT NULL,
  `SPECIAL_BRI` tinyint NOT NULL,
  `SPECIAL_PRI1` tinyint NOT NULL,
  `SPECIAL_PRI20` tinyint NOT NULL,
  `DATA_PHYSICS` tinyint NOT NULL,
  `DATA_CHANGE` tinyint NOT NULL,
  `DATA_GET_PHYSICS` tinyint NOT NULL,
  `DATA_GET_CHANGE` tinyint NOT NULL,
  `MOBILE` tinyint NOT NULL,
  `HS_SALE_END` tinyint NOT NULL,
  `DATA_CARD_SALE_END` tinyint NOT NULL,
  `SMART_PHONE_SALE_END` tinyint NOT NULL,
  `HS_ALLOTMENT` tinyint NOT NULL,
  `DATA_CARD_ALLOTMENT` tinyint NOT NULL,
  `SMART_PHONE_ALLOTMENT` tinyint NOT NULL,
  `HS_RENTAL` tinyint NOT NULL,
  `DATA_CARD_RENTAL` tinyint NOT NULL,
  `SMART_PHONE_RENTAL` tinyint NOT NULL,
  `PHS` tinyint NOT NULL,
  `WHITE_OFFERS` tinyint NOT NULL,
  `MODEL_CHANGE` tinyint NOT NULL,
  `ML` tinyint NOT NULL,
  `CLOUD_PHYSICS` tinyint NOT NULL,
  `CLOUD_CHANGE` tinyint NOT NULL,
  `YAHOO_PHYSICS` tinyint NOT NULL,
  `YAHOO_CHANGE` tinyint NOT NULL,
  `HS` tinyint NOT NULL,
  `SMART_PHONE` tinyint NOT NULL,
  `DATA_CARD` tinyint NOT NULL,
  `IPAD` tinyint NOT NULL,
  `GOOGLEAPPS` tinyint NOT NULL,
  `O2O` tinyint NOT NULL,
  `PAYPAL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp2_fy10_del_test`
--

DROP TABLE IF EXISTS `view_opp2_fy10_del_test`;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_del_test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp2_fy10_del_test` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp_fy10`
--

DROP TABLE IF EXISTS `view_opp_fy10`;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp_fy10` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `SV_DATE_MODIFIED_FMT` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT1_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_NM` tinyint NOT NULL,
  `OP_CONT_PLANMONTH` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `OP_MANAGE_FORECAST` tinyint NOT NULL,
  `SPECIAL_PRI` tinyint NOT NULL,
  `SPECIAL_BRI` tinyint NOT NULL,
  `SPECIAL_PRI1` tinyint NOT NULL,
  `SPECIAL_PRI20` tinyint NOT NULL,
  `DATA_PHYSICS` tinyint NOT NULL,
  `DATA_CHANGE` tinyint NOT NULL,
  `DATA_GET_PHYSICS` tinyint NOT NULL,
  `DATA_GET_CHANGE` tinyint NOT NULL,
  `MOBILE` tinyint NOT NULL,
  `HS_SALE_END` tinyint NOT NULL,
  `DATA_CARD_SALE_END` tinyint NOT NULL,
  `SMART_PHONE_SALE_END` tinyint NOT NULL,
  `HS_ALLOTMENT` tinyint NOT NULL,
  `DATA_CARD_ALLOTMENT` tinyint NOT NULL,
  `SMART_PHONE_ALLOTMENT` tinyint NOT NULL,
  `HS_RENTAL` tinyint NOT NULL,
  `DATA_CARD_RENTAL` tinyint NOT NULL,
  `SMART_PHONE_RENTAL` tinyint NOT NULL,
  `PHS` tinyint NOT NULL,
  `WHITE_OFFERS` tinyint NOT NULL,
  `MODEL_CHANGE` tinyint NOT NULL,
  `ML` tinyint NOT NULL,
  `CLOUD_PHYSICS` tinyint NOT NULL,
  `CLOUD_CHANGE` tinyint NOT NULL,
  `YAHOO_PHYSICS` tinyint NOT NULL,
  `YAHOO_CHANGE` tinyint NOT NULL,
  `HS` tinyint NOT NULL,
  `SMART_PHONE` tinyint NOT NULL,
  `DATA_CARD` tinyint NOT NULL,
  `IPAD` tinyint NOT NULL,
  `GOOGLEAPPS` tinyint NOT NULL,
  `O2O` tinyint NOT NULL,
  `PAYPAL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp_fy10_2`
--

DROP TABLE IF EXISTS `view_opp_fy10_2`;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp_fy10_2` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp_fy10_cstm`
--

DROP TABLE IF EXISTS `view_opp_fy10_cstm`;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_cstm`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp_fy10_cstm` (
  `ID` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `OP_DEP_DETAIL_C` tinyint NOT NULL,
  `OP_COMPNAME_MEMO_C` tinyint NOT NULL,
  `SUP_ID` tinyint NOT NULL,
  `LEAD_SOURCE` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_opp_fy10_del`
--

DROP TABLE IF EXISTS `view_opp_fy10_del`;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_del`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_opp_fy10_del` (
  `ID` tinyint NOT NULL,
  `DELETED` tinyint NOT NULL,
  `DATE_ENTERED` tinyint NOT NULL,
  `DATE_MODIFIED` tinyint NOT NULL,
  `CREATED_BY` tinyint NOT NULL,
  `MODIFIED_USER_ID` tinyint NOT NULL,
  `ASSIGNED_USER_ID` tinyint NOT NULL,
  `OP_GROUP_TYPE` tinyint NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` tinyint NOT NULL,
  `EIGYO_SOSIKI_NM` tinyint NOT NULL,
  `CORP_CD` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_ID` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_ID` tinyint NOT NULL,
  `SV_CLASS` tinyint NOT NULL,
  `SV_CLASS_DESC` tinyint NOT NULL,
  `SV_CATE` tinyint NOT NULL,
  `SV_CATE_NAME` tinyint NOT NULL,
  `SV_START_END` tinyint NOT NULL,
  `SV_UNIT` tinyint NOT NULL,
  `SV_NUM` tinyint NOT NULL,
  `MONTH_NUM` tinyint NOT NULL,
  `TEMP_NUM` tinyint NOT NULL,
  `LINE_NUM` tinyint NOT NULL,
  `ACCOUNT_ID` tinyint NOT NULL,
  `ACCOUNT_NAME` tinyint NOT NULL,
  `OPPORTUNITY_ID` tinyint NOT NULL,
  `ANKEN_ID_C` tinyint NOT NULL,
  `OPPORTUNITY_NAME` tinyint NOT NULL,
  `OWNER_ID` tinyint NOT NULL,
  `OWNER_NAME` tinyint NOT NULL,
  `MAIN_SALES_NAME` tinyint NOT NULL,
  `OP_STEP_C` tinyint NOT NULL,
  `OP_STEP_C_DESC` tinyint NOT NULL,
  `OP_CONT_PLANDATE_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C` tinyint NOT NULL,
  `OP_ORDER_POSS_C_DESC` tinyint NOT NULL,
  `OP_COMMIT_C` tinyint NOT NULL,
  `OP_COMMIT_C_DESC` tinyint NOT NULL,
  `DATE_MODIFIED_FMT` tinyint NOT NULL,
  `KEY_DRIVER_ID` tinyint NOT NULL,
  `KEY_DRIVER_NAME` tinyint NOT NULL,
  `SV_TYPE` tinyint NOT NULL,
  `SV_ID` tinyint NOT NULL,
  `DATE_ENTERED_FMT` tinyint NOT NULL,
  `SALES_STAGE` tinyint NOT NULL,
  `SPM_ORGUNIT_CD` tinyint NOT NULL,
  `SPM_ORGUNIT_NAME` tinyint NOT NULL,
  `OP_DEPEND_C` tinyint NOT NULL,
  `OPPORTUNITY_TYPE` tinyint NOT NULL,
  `SV_DATE_MODIFIED` tinyint NOT NULL,
  `BILLIARD_FORM` tinyint NOT NULL,
  `BILLIARD_MODEL_CODE` tinyint NOT NULL,
  `BILLIARD_MAKER` tinyint NOT NULL,
  `BILLIARD_MODEL` tinyint NOT NULL,
  `BILLIARD_COLOR` tinyint NOT NULL,
  `EMPHASIS_STRATEGY1_C` tinyint NOT NULL,
  `EMPHASIS_STRATEGY2_C` tinyint NOT NULL,
  `MEASURE_DEPT_C` tinyint NOT NULL,
  `MEASURE_COMPANY_C` tinyint NOT NULL,
  `EM_STRATEGY1_030` tinyint NOT NULL,
  `EM_STRATEGY1_040` tinyint NOT NULL,
  `EM_STRATEGY2_050` tinyint NOT NULL,
  `EM_STRATEGY2_060` tinyint NOT NULL,
  `EM_STRATEGY2_070` tinyint NOT NULL,
  `EM_STRATEGY2_080` tinyint NOT NULL,
  `EM_STRATEGY2_090` tinyint NOT NULL,
  `EM_STRATEGY2_100` tinyint NOT NULL,
  `BILLIARD_CHOICE_DAY` tinyint NOT NULL,
  `BILLIARD_MEMO` tinyint NOT NULL,
  `SV_DATE_MODIFIED_FMT` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT1_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT2_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT3_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT4_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT5_NM` tinyint NOT NULL,
  `MAST_ORGANIZATIONUNIT6_NM` tinyint NOT NULL,
  `OP_CONT_PLANMONTH` tinyint NOT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `MONTH_NUM_UNIT` tinyint NOT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` tinyint NOT NULL,
  `TEMP_NUM_UNIT` tinyint NOT NULL,
  `OP_MANAGE_FORECAST` tinyint NOT NULL,
  `MONTH_AGE` tinyint NOT NULL,
  `SPECIAL_PRI` tinyint NOT NULL,
  `SPECIAL_BRI` tinyint NOT NULL,
  `SPECIAL_PRI1` tinyint NOT NULL,
  `SPECIAL_PRI20` tinyint NOT NULL,
  `DATA_PHYSICS` tinyint NOT NULL,
  `DATA_CHANGE` tinyint NOT NULL,
  `DATA_GET_PHYSICS` tinyint NOT NULL,
  `DATA_GET_CHANGE` tinyint NOT NULL,
  `MOBILE` tinyint NOT NULL,
  `HS_SALE_END` tinyint NOT NULL,
  `DATA_CARD_SALE_END` tinyint NOT NULL,
  `SMART_PHONE_SALE_END` tinyint NOT NULL,
  `HS_ALLOTMENT` tinyint NOT NULL,
  `DATA_CARD_ALLOTMENT` tinyint NOT NULL,
  `SMART_PHONE_ALLOTMENT` tinyint NOT NULL,
  `HS_RENTAL` tinyint NOT NULL,
  `DATA_CARD_RENTAL` tinyint NOT NULL,
  `SMART_PHONE_RENTAL` tinyint NOT NULL,
  `PHS` tinyint NOT NULL,
  `WHITE_OFFERS` tinyint NOT NULL,
  `MODEL_CHANGE` tinyint NOT NULL,
  `ML` tinyint NOT NULL,
  `CLOUD_PHYSICS` tinyint NOT NULL,
  `CLOUD_CHANGE` tinyint NOT NULL,
  `YAHOO_PHYSICS` tinyint NOT NULL,
  `YAHOO_CHANGE` tinyint NOT NULL,
  `HS` tinyint NOT NULL,
  `SMART_PHONE` tinyint NOT NULL,
  `DATA_CARD` tinyint NOT NULL,
  `IPAD` tinyint NOT NULL,
  `GOOGLEAPPS` tinyint NOT NULL,
  `O2O` tinyint NOT NULL,
  `PAYPAL` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wk_if_str_assignment`
--

DROP TABLE IF EXISTS `wk_if_str_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_if_str_assignment` (
  `KOJIN_ID` varchar(20) DEFAULT NULL,
  `DIV_CD` varchar(8) DEFAULT NULL,
  `KAISHA_CD` varchar(4) DEFAULT NULL,
  `POST_KBN` varchar(4) DEFAULT NULL,
  `PROCEEDING_RATIO` double DEFAULT NULL,
  `TITLE_CD` varchar(4) DEFAULT NULL,
  `TITLE_NM` varchar(79) DEFAULT NULL,
  `YAKUWARI_CD` varchar(4) DEFAULT NULL,
  `YAKUWARI_NM` varchar(79) DEFAULT NULL,
  `ORG_UPDATE_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_if_str_assignment`
--

LOCK TABLES `wk_if_str_assignment` WRITE;
/*!40000 ALTER TABLE `wk_if_str_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_if_str_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_corp_tanto`
--

DROP TABLE IF EXISTS `wk_mst_corp_tanto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_corp_tanto` (
  `UNIQ_CORP_CD` char(6) NOT NULL,
  `ACTUAL_MONTH` datetime DEFAULT NULL,
  `ACCOUNT_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `ACCOUNT_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `ACCOUNT_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `ACCOUNT_OFFICE1` varchar(100) DEFAULT NULL,
  `ACCOUNT_OFFICE2` varchar(100) DEFAULT NULL,
  `ACCOUNT_DEPT` varchar(100) DEFAULT NULL,
  `ACCOUNT_TAN_ID` varchar(15) DEFAULT NULL,
  `ACCOUNT_TAN_NAME` varchar(30) DEFAULT NULL,
  `VOICE_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `VOICE_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `VOICE_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `VOICE_OFFICE1` varchar(100) DEFAULT NULL,
  `VOICE_OFFICE2` varchar(100) DEFAULT NULL,
  `VOICE_DEPT` varchar(100) DEFAULT NULL,
  `VOICE_TAN_ID` varchar(15) DEFAULT NULL,
  `VOICE_TAN_NAME` varchar(30) DEFAULT NULL,
  `DATA_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `DATA_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `DATA_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `DATA_OFFICE1` varchar(100) DEFAULT NULL,
  `DATA_OFFICE2` varchar(100) DEFAULT NULL,
  `DATA_DEPT` varchar(100) DEFAULT NULL,
  `DATA_TAN_ID` varchar(15) DEFAULT NULL,
  `DATA_TAN_NAME` varchar(30) DEFAULT NULL,
  `BP_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `BP_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `BP_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `BP_OFFICE1` varchar(100) DEFAULT NULL,
  `BP_OFFICE2` varchar(100) DEFAULT NULL,
  `BP_DEPT` varchar(100) DEFAULT NULL,
  `BP_TAN_ID` varchar(15) DEFAULT NULL,
  `BP_TAN_NAME` varchar(30) DEFAULT NULL,
  `VOICE_BP_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `VOICE_BP_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `VOICE_BP_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `VOICE_BP_OFFICE1` varchar(100) DEFAULT NULL,
  `VOICE_BP_OFFICE2` varchar(100) DEFAULT NULL,
  `VOICE_BP_DEPT` varchar(100) DEFAULT NULL,
  `VOICE_BP_TAN_ID` varchar(15) DEFAULT NULL,
  `VOICE_BP_TAN_NAME` varchar(30) DEFAULT NULL,
  `NIS_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `NIS_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `NIS_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `NIS_OFFICE1` varchar(100) DEFAULT NULL,
  `NIS_OFFICE2` varchar(100) DEFAULT NULL,
  `NIS_DEPT` varchar(100) DEFAULT NULL,
  `NIS_TAN_ID` varchar(15) DEFAULT NULL,
  `NIS_TAN_NAME` varchar(30) DEFAULT NULL,
  `IN_DATA_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `IN_DATA_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `IN_DATA_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `IN_DATA_OFFICE1` varchar(100) DEFAULT NULL,
  `IN_DATA_OFFICE2` varchar(100) DEFAULT NULL,
  `IN_DATA_DEPT` varchar(100) DEFAULT NULL,
  `IN_DATA_TANTO_ID` varchar(15) DEFAULT NULL,
  `IN_DATA_TAN_NAME` varchar(30) DEFAULT NULL,
  `IN_CARRIER_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `IN_CARRIER_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `IN_CARRIER_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `IN_CARRIER_OFFICE1` varchar(100) DEFAULT NULL,
  `IN_CARRIER_OFFICE2` varchar(100) DEFAULT NULL,
  `IN_CARRIER_DEPT` varchar(100) DEFAULT NULL,
  `IN_CARRIER_TANTO_ID` varchar(15) DEFAULT NULL,
  `IN_CARRIER_TAN_NAME` varchar(30) DEFAULT NULL,
  `IN_WS_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `IN_WS_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `IN_WS_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `IN_WS_OFFICE1` varchar(100) DEFAULT NULL,
  `IN_WS_OFFICE2` varchar(100) DEFAULT NULL,
  `IN_WS_DEPT` varchar(100) DEFAULT NULL,
  `IN_WS_TANTO_ID` varchar(15) DEFAULT NULL,
  `IN_WS_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE1_S_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `RESERVE1_S_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `RESERVE1_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `RESERVE1_OFFICE1` varchar(100) DEFAULT NULL,
  `RESERVE1_OFFICE2` varchar(100) DEFAULT NULL,
  `RESERVE1_DEPT` varchar(100) DEFAULT NULL,
  `RESERVE1_TANTO_ID` varchar(15) DEFAULT NULL,
  `RESERVE1_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE1_LAVEL` varchar(30) DEFAULT NULL,
  `RESERVE2_S_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `RESERVE2_S_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `RESERVE2_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `RESERVE2_OFFICE1` varchar(100) DEFAULT NULL,
  `RESERVE2_OFFICE2` varchar(100) DEFAULT NULL,
  `RESERVE2_DEPT` varchar(100) DEFAULT NULL,
  `RESERVE2_TANTO_ID` varchar(15) DEFAULT NULL,
  `RESERVE2_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE2_LAVEL` varchar(30) DEFAULT NULL,
  `RESERVE3_S_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `RESERVE3_S_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `RESERVE3_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `RESERVE3_OFFICE1` varchar(100) DEFAULT NULL,
  `RESERVE3_OFFICE2` varchar(100) DEFAULT NULL,
  `RESERVE3_DEPT` varchar(100) DEFAULT NULL,
  `RESERVE3_TANTO_ID` varchar(15) DEFAULT NULL,
  `RESERVE3_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE3_LAVEL` varchar(30) DEFAULT NULL,
  `RESERVE4_S_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `RESERVE4_S_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `RESERVE4_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `RESERVE4_OFFICE1` varchar(100) DEFAULT NULL,
  `RESERVE4_OFFICE2` varchar(100) DEFAULT NULL,
  `RESERVE4_DEPT` varchar(100) DEFAULT NULL,
  `RESERVE4_TANTO_ID` varchar(15) DEFAULT NULL,
  `RESERVE4_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE4_LAVEL` varchar(30) DEFAULT NULL,
  `RESERVE5_S_DEPT_S_CD` varchar(21) DEFAULT NULL,
  `RESERVE5_S_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `RESERVE5_OLD_DEPT_S_CD` varchar(8) DEFAULT NULL,
  `RESERVE5_OFFICE1` varchar(100) DEFAULT NULL,
  `RESERVE5_OFFICE2` varchar(100) DEFAULT NULL,
  `RESERVE5_DEPT` varchar(100) DEFAULT NULL,
  `RESERVE5_TANTO_ID` varchar(15) DEFAULT NULL,
  `RESERVE5_TAN_NAME` varchar(30) DEFAULT NULL,
  `RESERVE5_LAVEL` varchar(30) DEFAULT NULL,
  `TAN_AREA_CD` char(8) DEFAULT NULL,
  `TAN_AREA_NM` varchar(30) DEFAULT NULL,
  `REMARK_CORPRATE` char(2) DEFAULT NULL,
  `REMARK_CORPRATE_CONT` varchar(20) DEFAULT NULL,
  `BP_PLUS_FLG` char(10) DEFAULT NULL,
  `BP_OPE_STS` varchar(20) DEFAULT NULL,
  `OPEN_MKT_VOICE` varchar(20) DEFAULT NULL,
  `OPEN_MKT_VOICE_MEMO` varchar(100) DEFAULT NULL,
  `OPEN_MKT_DATA` varchar(20) DEFAULT NULL,
  `OPEN_MKT_DATA_MEMO` varchar(100) DEFAULT NULL,
  `MEMO1` varchar(64) DEFAULT NULL,
  `MEMO2` varchar(64) DEFAULT NULL,
  `MEMO3` varchar(64) DEFAULT NULL,
  `MEMO4` varchar(64) DEFAULT NULL,
  `MEMO5` varchar(64) DEFAULT NULL,
  `SYSTEM_DAY` varchar(8) DEFAULT NULL,
  `UPDATE_DAY` varchar(8) DEFAULT NULL,
  `DEL_FLG` char(1) NOT NULL DEFAULT '',
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  `MOBILE_DEPT_DISP_CD` varchar(21) DEFAULT NULL,
  `OPEN_MKT_MOBILE` varchar(20) DEFAULT NULL,
  `MOBILE_TAN_ID` varchar(15) DEFAULT NULL,
  `MOBILE_DEPT_S_CD` varchar(21) DEFAULT NULL,
  PRIMARY KEY (`UNIQ_CORP_CD`,`DEL_FLG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_corp_tanto`
--

LOCK TABLES `wk_mst_corp_tanto` WRITE;
/*!40000 ALTER TABLE `wk_mst_corp_tanto` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_corp_tanto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_eigyo_sosiki`
--

DROP TABLE IF EXISTS `wk_mst_eigyo_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_eigyo_sosiki` (
  `EIGYO_SOSIKI_CD` varchar(21) NOT NULL,
  `EIGYO_SOSIKI_DISP_CD` varchar(21) DEFAULT NULL,
  `OP_START_DATE` datetime DEFAULT NULL,
  `EIGYO_SOSIKI_LV` double DEFAULT NULL,
  `AGENT_CD` varchar(20) DEFAULT NULL,
  `OLD_DIV_CD` varchar(8) DEFAULT NULL,
  `EIGYO_SOSIKI_NM` varchar(128) DEFAULT NULL,
  `EIGYO_SOSIKI_SHORT_NM` varchar(128) DEFAULT NULL,
  `CORP_CD` char(3) DEFAULT NULL,
  `DIV1_CD` char(3) DEFAULT NULL,
  `DIV2_CD` char(3) DEFAULT NULL,
  `DIV3_CD` char(3) DEFAULT NULL,
  `DIV4_CD` char(3) DEFAULT NULL,
  `DIV5_CD` char(3) DEFAULT NULL,
  `DIV6_CD` char(3) DEFAULT NULL,
  `DIV1_NM` varchar(64) DEFAULT NULL,
  `DIV2_NM` varchar(64) DEFAULT NULL,
  `DIV3_NM` varchar(64) DEFAULT NULL,
  `DIV4_NM` varchar(64) DEFAULT NULL,
  `DIV5_NM` varchar(64) DEFAULT NULL,
  `DIV6_NM` varchar(64) DEFAULT NULL,
  `SORT_CD` varchar(13) DEFAULT NULL,
  `JINJI_DIV_CD` varchar(11) DEFAULT NULL,
  `SUM_CD` varchar(13) DEFAULT NULL,
  `LINE_MNG_TANTO_CD` varchar(15) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `DEL_FLG` char(1) NOT NULL DEFAULT '',
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`EIGYO_SOSIKI_CD`,`DEL_FLG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_eigyo_sosiki`
--

LOCK TABLES `wk_mst_eigyo_sosiki` WRITE;
/*!40000 ALTER TABLE `wk_mst_eigyo_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_eigyo_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_jinji_sosiki`
--

DROP TABLE IF EXISTS `wk_mst_jinji_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_jinji_sosiki` (
  `DIV_CD` varchar(11) NOT NULL DEFAULT '',
  `DIV_NM` varchar(255) DEFAULT NULL,
  `DIV_NM_SHORT` varchar(255) DEFAULT NULL,
  `UP_DIV_CD` varchar(11) DEFAULT NULL,
  `MNG_TANTO_CD` varchar(15) DEFAULT NULL,
  `MNG_GRPTANTO_CD` varchar(15) DEFAULT NULL,
  `DIV_UNIT_ID` varchar(40) DEFAULT NULL,
  `DIV_UNIT_KBN` varchar(11) DEFAULT NULL,
  `ORD_NO` varchar(11) DEFAULT NULL,
  `KAISHA_CD` char(4) DEFAULT NULL,
  `ORG_DEL_FLG` char(1) DEFAULT NULL,
  `ORG_REGIST_DATE` datetime DEFAULT NULL,
  `ORG_UPDATE_DATE` datetime DEFAULT NULL,
  `ORG_REGIST_NM` varchar(36) DEFAULT NULL,
  `ORG_UPDATE_NM` varchar(36) DEFAULT NULL,
  `DEL_FLG` char(1) NOT NULL DEFAULT '',
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`DIV_CD`,`DEL_FLG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_jinji_sosiki`
--

LOCK TABLES `wk_mst_jinji_sosiki` WRITE;
/*!40000 ALTER TABLE `wk_mst_jinji_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_jinji_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_login_account`
--

DROP TABLE IF EXISTS `wk_mst_login_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_login_account` (
  `ACCOUNT_ID` varchar(15) NOT NULL,
  `TANTO_CD` varchar(15) DEFAULT NULL,
  `STR_KOJIN_ID` varchar(20) DEFAULT NULL,
  `ROLE_CD` varchar(4) DEFAULT NULL,
  `ACS_ACCOUNT` varchar(15) DEFAULT NULL,
  `ACCESS_FROM_DATE` datetime DEFAULT NULL,
  `ACCESS_TO_DATE` datetime DEFAULT NULL,
  `UPDATE_MEMO` varchar(255) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_login_account`
--

LOCK TABLES `wk_mst_login_account` WRITE;
/*!40000 ALTER TABLE `wk_mst_login_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_login_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_regist_corp`
--

DROP TABLE IF EXISTS `wk_mst_regist_corp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_regist_corp` (
  `REGIST_CORP_CD` varchar(12) NOT NULL,
  `UNIQ_CORP_CD` char(6) DEFAULT NULL,
  `FIX_FLG` char(1) DEFAULT NULL,
  `G3_CORP_CD` varchar(16) DEFAULT NULL,
  `TEIKOKU_DB_KIGYO_BNG` char(10) DEFAULT NULL,
  `HOJINKAKU_FLG` char(2) DEFAULT NULL,
  `CORP_NAME_KANA` varchar(60) DEFAULT NULL,
  `CORP_NAME_KANA_ZEN` varchar(120) DEFAULT NULL,
  `CORP_NAME` varchar(70) DEFAULT NULL,
  `KENSAKU_CORP_KANA` varchar(34) DEFAULT NULL,
  `KENSAKU_CORP_KANA_ZEN` varchar(68) DEFAULT NULL,
  `KENSAKU_CORP_NAME` varchar(70) DEFAULT NULL,
  `ZIP` varchar(7) DEFAULT NULL,
  `ADDR` varchar(255) DEFAULT NULL,
  `ADDR1` varchar(10) DEFAULT NULL,
  `ADDR2` varchar(50) DEFAULT NULL,
  `ADDR3` varchar(200) DEFAULT NULL,
  `ADDR4` varchar(200) DEFAULT NULL,
  `ADDR5` varchar(200) DEFAULT NULL,
  `DENWA_BNG` varchar(13) DEFAULT NULL,
  `SHUGYO_CD` varchar(5) DEFAULT NULL,
  `DAIHYO_SHA_KANA` varchar(20) DEFAULT NULL,
  `DAIHYO_SHA_KANA_ZEN` varchar(40) DEFAULT NULL,
  `DAIHYO_SHA_NAME` varchar(40) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `UPDATE_ROUTE` char(1) DEFAULT NULL,
  `UPDATE_MEMO` varchar(64) DEFAULT NULL,
  `MEMO1` varchar(64) DEFAULT NULL,
  `MEMO2` varchar(64) DEFAULT NULL,
  `MEMO3` varchar(64) DEFAULT NULL,
  `MEMO4` varchar(64) DEFAULT NULL,
  `MEMO5` varchar(64) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`REGIST_CORP_CD`),
  KEY `UNIQ_CORP_CD` (`UNIQ_CORP_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_regist_corp`
--

LOCK TABLES `wk_mst_regist_corp` WRITE;
/*!40000 ALTER TABLE `wk_mst_regist_corp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_regist_corp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_tanto`
--

DROP TABLE IF EXISTS `wk_mst_tanto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_tanto` (
  `TANTO_CD` varchar(15) NOT NULL DEFAULT '',
  `LAST_NM` varchar(40) DEFAULT NULL,
  `FIRST_NM` varchar(40) DEFAULT NULL,
  `NAME_KANJI` varchar(80) DEFAULT NULL,
  `TITLE` varchar(50) DEFAULT NULL,
  `STF_CD` varchar(8) DEFAULT NULL,
  `STR_KOJIN_ID` varchar(20) DEFAULT NULL,
  `GROUP_TANTO_CD` varchar(7) DEFAULT NULL,
  `KAISHA_CD` varchar(4) DEFAULT NULL,
  `SBM_TANTO_CD` varchar(30) DEFAULT NULL,
  `SBB_TANTO_CD` varchar(30) DEFAULT NULL,
  `SBTM_TANTO_CD` varchar(30) DEFAULT NULL,
  `DIV_CD` varchar(11) DEFAULT NULL,
  `DIV_NM` varchar(255) DEFAULT NULL,
  `DIV_UNIT_ID` varchar(40) DEFAULT NULL,
  `MNG_TANTO_CD` varchar(15) DEFAULT NULL,
  `MNG_GRPTANTO_CD` varchar(15) DEFAULT NULL,
  `PHONE_NO` varchar(20) DEFAULT NULL,
  `EMAIL_ADR` varchar(241) DEFAULT NULL,
  `AGENT_FLG` char(1) DEFAULT NULL,
  `AGENT_CD` varchar(20) DEFAULT NULL,
  `ORG_DEL_FLG` char(1) DEFAULT NULL,
  `ORG_REGIST_DATE` datetime DEFAULT NULL,
  `ORG_UPDATE_DATE` datetime DEFAULT NULL,
  `ORG_REGIST_NM` varchar(36) DEFAULT NULL,
  `ORG_UPDATE_NM` varchar(36) DEFAULT NULL,
  `DEL_FLG` char(1) NOT NULL DEFAULT '',
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`TANTO_CD`,`DEL_FLG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_tanto`
--

LOCK TABLES `wk_mst_tanto` WRITE;
/*!40000 ALTER TABLE `wk_mst_tanto` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_tanto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_tanto_s_sosiki`
--

DROP TABLE IF EXISTS `wk_mst_tanto_s_sosiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_tanto_s_sosiki` (
  `TANTO_CD` varchar(15) NOT NULL,
  `EIGYO_SOSIKI_CD` varchar(21) NOT NULL,
  `MAIN_FLG` char(1) DEFAULT NULL,
  `SBTM_TANTO_CD` varchar(15) DEFAULT NULL,
  `GROUP_TANTO_CD` varchar(15) DEFAULT NULL,
  `SP_TANTO_CD` varchar(15) DEFAULT NULL,
  `MOBILE_STF_CD` varchar(15) DEFAULT NULL,
  `HOSYU_SOSIKI_CD` varchar(21) DEFAULT NULL,
  `DEL_FLG` char(1) NOT NULL DEFAULT '',
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`TANTO_CD`,`EIGYO_SOSIKI_CD`,`DEL_FLG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_tanto_s_sosiki`
--

LOCK TABLES `wk_mst_tanto_s_sosiki` WRITE;
/*!40000 ALTER TABLE `wk_mst_tanto_s_sosiki` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_tanto_s_sosiki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_tdb`
--

DROP TABLE IF EXISTS `wk_mst_tdb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_tdb` (
  `TEIKOKU_DB_KIGYO_BNG` char(9) NOT NULL DEFAULT '',
  `HOJINKAKU_FLG` char(2) DEFAULT NULL,
  `KIGYO_NAME_KANA` varchar(60) DEFAULT NULL,
  `KIGYO_NAME` varchar(70) DEFAULT NULL,
  `KENSAKU_KIGYO_KANA` varchar(34) DEFAULT NULL,
  `KENSAKU_KIGYO_NAME` varchar(70) DEFAULT NULL,
  `ZIP` varchar(7) DEFAULT NULL,
  `BAR_CD_BNG` char(23) DEFAULT NULL,
  `TEIKOKU_ADDR_CD` char(10) DEFAULT NULL,
  `ADDR` varchar(255) DEFAULT NULL,
  `ADDR1` varchar(10) DEFAULT NULL,
  `ADDR2` varchar(200) DEFAULT NULL,
  `ADDR3` varchar(200) DEFAULT NULL,
  `ADDR4` varchar(200) DEFAULT NULL,
  `ADDR5` varchar(200) DEFAULT NULL,
  `DENWA_BNG` varchar(13) DEFAULT NULL,
  `SHUGYO_CD` char(5) DEFAULT NULL,
  `JUGYO_CD` char(5) DEFAULT NULL,
  `SHIHONKIN` double DEFAULT NULL,
  `JUGYOIN_NUM` double DEFAULT NULL,
  `SOGYO_NENGETU` char(6) DEFAULT NULL,
  `SETURITU_NENGETU` char(6) DEFAULT NULL,
  `HYOTEN` double DEFAULT NULL,
  `JIGYOSHO_NUM` double DEFAULT NULL,
  `KESSANKI2` char(6) DEFAULT NULL,
  `URIAGEDAKA2` double DEFAULT NULL,
  `RIEKI_MISHO_FLG2` char(1) DEFAULT NULL,
  `RIEKI2` double DEFAULT NULL,
  `HAITORITU2` double DEFAULT NULL,
  `JIKOSHIHON_RATE2` double DEFAULT NULL,
  `KESSAN_UMU_FLG2` char(1) DEFAULT NULL,
  `HOJIN_SHOTOKU2` double DEFAULT NULL,
  `KESSANKI1` char(6) DEFAULT NULL,
  `URIAGEDAKA1` double DEFAULT NULL,
  `RIEKI_MISHO_FLG1` char(1) DEFAULT NULL,
  `RIEKI1` double DEFAULT NULL,
  `HAITORITU1` double DEFAULT NULL,
  `JIKOSHIHON_RATE1` double DEFAULT NULL,
  `KESSAN_UMU_FLG1` char(1) DEFAULT NULL,
  `HOJIN_SHOTOKU1` double DEFAULT NULL,
  `ZENKOKU_SHA_NUM` double DEFAULT NULL,
  `ZENKOKU_RANK` double DEFAULT NULL,
  `TODOHUKEN_BETU_SHA_NUM` double DEFAULT NULL,
  `TODOHUKEN_BETU_RANK` double DEFAULT NULL,
  `DAIHYO_SHA_YAKUSHOKU_NAME` varchar(20) DEFAULT NULL,
  `DAIHYO_SHA_KANA` varchar(20) DEFAULT NULL,
  `DAIHYO_SHA_NAME` varchar(40) DEFAULT NULL,
  `TORIHIKI_BANK_CD1` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD2` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD3` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD4` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD5` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD6` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD7` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD8` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD9` char(7) DEFAULT NULL,
  `TORIHIKI_BANK_CD10` char(7) DEFAULT NULL,
  `SHIIRE_SAKI1` varchar(60) DEFAULT NULL,
  `HANBAI_SAKI1` varchar(60) DEFAULT NULL,
  `OYA_KIGYO_NAME` varchar(30) DEFAULT NULL,
  `SHUPPANBUTU_FLG` char(1) DEFAULT NULL,
  `KABUSHIKI_KOKAI_FLG` char(1) DEFAULT NULL,
  `SHUGYO_NAME` varchar(18) DEFAULT NULL,
  `JUGYO_NAME` varchar(18) DEFAULT NULL,
  `TORIHIKI_BANK1` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK2` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK3` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK4` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK5` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK6` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK7` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK8` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK9` varchar(30) DEFAULT NULL,
  `TORIHIKI_BANK10` varchar(30) DEFAULT NULL,
  `SOSHIKI_KBN` char(1) DEFAULT NULL,
  `JIGYO_NAIYO` varchar(255) DEFAULT NULL,
  `JYD_RANK` char(2) DEFAULT NULL,
  `KANYU_SERVICE` char(40) DEFAULT NULL,
  `SHOKEN_CD` char(4) DEFAULT NULL,
  `URL` varchar(50) DEFAULT NULL,
  `KABUNUSHI` varchar(100) DEFAULT NULL,
  `ACCOUNT_KBN` char(2) DEFAULT NULL,
  `GAPPEI_KIGYO_BNG` char(6) DEFAULT NULL,
  `SEGMENT_CD` varchar(4) DEFAULT NULL,
  `YOBI1` char(2) DEFAULT NULL,
  `YOBI2` char(2) DEFAULT NULL,
  `YOBI3` char(2) DEFAULT NULL,
  `YOBI4` char(2) DEFAULT NULL,
  `YOBI5` char(2) DEFAULT NULL,
  `TOROKU_DATE` datetime DEFAULT NULL,
  `TOROKU_SHA` char(7) DEFAULT NULL,
  `KOSHIN_DATE` datetime DEFAULT NULL,
  `KOSHIN_SHA` char(7) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`TEIKOKU_DB_KIGYO_BNG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_tdb`
--

LOCK TABLES `wk_mst_tdb` WRITE;
/*!40000 ALTER TABLE `wk_mst_tdb` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_tdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wk_mst_uniq_corp`
--

DROP TABLE IF EXISTS `wk_mst_uniq_corp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wk_mst_uniq_corp` (
  `UNIQ_CORP_CD` char(6) NOT NULL,
  `TEIKOKU_DB_KIGYO_BNG` char(10) DEFAULT NULL,
  `HOJINKAKU_FLG` char(2) DEFAULT NULL,
  `CORP_NAME_KANA` varchar(60) DEFAULT NULL,
  `CORP_NAME_KANA_ZEN` varchar(120) DEFAULT NULL,
  `CORP_NAME` varchar(70) DEFAULT NULL,
  `KENSAKU_CORP_KANA` varchar(34) DEFAULT NULL,
  `KENSAKU_CORP_KANA_ZEN` varchar(68) DEFAULT NULL,
  `KENSAKU_CORP_NAME` varchar(70) DEFAULT NULL,
  `ZIP` varchar(7) DEFAULT NULL,
  `ADDR` varchar(255) DEFAULT NULL,
  `ADDR1` varchar(10) DEFAULT NULL,
  `ADDR2` varchar(200) DEFAULT NULL,
  `ADDR3` varchar(200) DEFAULT NULL,
  `ADDR4` varchar(200) DEFAULT NULL,
  `ADDR5` varchar(200) DEFAULT NULL,
  `DENWA_BNG` varchar(13) DEFAULT NULL,
  `SHUGYO_CD` char(5) DEFAULT NULL,
  `JUGYO_CD` char(5) DEFAULT NULL,
  `DAIHYO_SHA_YAKUSHOKU_NAME` varchar(20) DEFAULT NULL,
  `DAIHYO_SHA_KANA` varchar(20) DEFAULT NULL,
  `DAIHYO_SHA_KANA_ZEN` varchar(40) DEFAULT NULL,
  `DAIHYO_SHA_NAME` varchar(40) DEFAULT NULL,
  `SHUGYO_NAME` varchar(18) DEFAULT NULL,
  `JUGYO_NAME` varchar(18) DEFAULT NULL,
  `SOSHIKI_KBN` char(1) DEFAULT NULL,
  `JIGYO_NAIYO` varchar(255) DEFAULT NULL,
  `JYD_RANK` char(2) DEFAULT NULL,
  `SHOKEN_CD` char(4) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `GAPPEI_CORP_BNG` char(6) DEFAULT NULL,
  `TOP_CORP_FLG` char(1) DEFAULT NULL,
  `TOP_CORP_CD` varchar(6) DEFAULT NULL,
  `SB_IND_1` varchar(100) DEFAULT NULL,
  `SB_IND_2` varchar(100) DEFAULT NULL,
  `SB_IND_3` varchar(100) DEFAULT NULL,
  `MEMO1` varchar(64) DEFAULT NULL,
  `MEMO2` varchar(64) DEFAULT NULL,
  `MEMO3` varchar(64) DEFAULT NULL,
  `MEMO4` varchar(64) DEFAULT NULL,
  `MEMO5` varchar(64) DEFAULT NULL,
  `DISABLE_FLG` char(1) DEFAULT NULL,
  `DISABLE_REASON` char(2) DEFAULT NULL,
  `TOROKU_DATE` datetime DEFAULT NULL,
  `KOSHIN_DATE` datetime DEFAULT NULL,
  `TOROKU_SHA` char(7) DEFAULT NULL,
  `KOSHIN_SHA` char(7) DEFAULT NULL,
  `DEL_FLG` char(1) DEFAULT NULL,
  `REGIST_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `REGIST_NM` varchar(64) DEFAULT NULL,
  `UPDATE_NM` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`UNIQ_CORP_CD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wk_mst_uniq_corp`
--

LOCK TABLES `wk_mst_uniq_corp` WRITE;
/*!40000 ALTER TABLE `wk_mst_uniq_corp` DISABLE KEYS */;
/*!40000 ALTER TABLE `wk_mst_uniq_corp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_opp_id`
--

DROP TABLE IF EXISTS `zz_copy_opp_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_opp_id` (
  `id` varchar(36) DEFAULT NULL,
  KEY `idx_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_opp_id`
--

LOCK TABLES `zz_copy_opp_id` WRITE;
/*!40000 ALTER TABLE `zz_copy_opp_id` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_opp_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_vd_anken_list`
--

DROP TABLE IF EXISTS `zz_copy_vd_anken_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_vd_anken_list` (
  `anken_kingaku_cd` varchar(4) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `anken_id_c` varchar(20) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `op_compname_memo_c` varchar(255) DEFAULT NULL,
  `orgunit_voice_cd` varchar(36) DEFAULT NULL,
  `voice_unit_name` varchar(255) DEFAULT NULL,
  `opp_main_group_code` varchar(16) DEFAULT NULL,
  `opp_main_group_name` varchar(255) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `description` text,
  `date_entered_fmt` varchar(8) DEFAULT NULL,
  `op_cont_plandate_c_fmt` varchar(8) DEFAULT NULL,
  `status_desc` varchar(4) DEFAULT NULL,
  `op_step_c_desc` varchar(5) DEFAULT NULL,
  `op_order_poss_c_desc` varchar(1) DEFAULT NULL,
  `op_version` varchar(4) DEFAULT NULL,
  `date_modified_fmt` varchar(19) DEFAULT NULL,
  `modified_staff_code` varchar(60) DEFAULT NULL,
  `modified_staff_name` varchar(61) DEFAULT NULL,
  `modified_group_code` varchar(16) DEFAULT NULL,
  `modified_group_name` varchar(255) DEFAULT NULL,
  `sv_class` varchar(100) DEFAULT NULL,
  `sv_class_desc` varchar(7) DEFAULT NULL,
  `sv_cate` varchar(100) DEFAULT NULL,
  `sv_cate_name` varchar(255) DEFAULT NULL,
  `sv_unit` varchar(100) DEFAULT NULL,
  `sv_num` double DEFAULT NULL,
  `op_anr` varchar(4) DEFAULT NULL,
  `op_cost` varchar(4) DEFAULT NULL,
  `sv_start_yyyymm` varchar(200) DEFAULT NULL,
  `sv_end_yyyymm` varchar(200) DEFAULT NULL,
  `op_owner_judge` varchar(4) DEFAULT NULL,
  `op_commit_c_desc` varchar(6) DEFAULT NULL,
  `op_dep_detail_c` text,
  `main_sales_staff_code` varchar(60) DEFAULT NULL,
  `main_sales_name` varchar(61) DEFAULT NULL,
  `main_sales_group_code` varchar(16) DEFAULT NULL,
  `main_sales_group_name` varchar(255) DEFAULT NULL,
  `owner_staff_code` varchar(60) DEFAULT NULL,
  `owner_name` varchar(61) DEFAULT NULL,
  `owner_group_code` varchar(16) DEFAULT NULL,
  `owner_group_name` varchar(255) DEFAULT NULL,
  `yosoku_kaisensu` varchar(4) DEFAULT NULL,
  `sv_start_month_num` varchar(4) DEFAULT NULL,
  `partner_id` varchar(36) DEFAULT NULL,
  `partner_name` varchar(150) DEFAULT NULL,
  `op_depend_c` varchar(2) DEFAULT NULL,
  `op_depend_c_desc` varchar(7) DEFAULT NULL,
  `mobile_color_cd` varchar(4) DEFAULT NULL,
  `mobile_model_name` varchar(4) DEFAULT NULL,
  `mobile_color_name` varchar(4) DEFAULT NULL,
  `get_datetime` varchar(17) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_vd_anken_list`
--

LOCK TABLES `zz_copy_vd_anken_list` WRITE;
/*!40000 ALTER TABLE `zz_copy_vd_anken_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_vd_anken_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_vd_opp_sv_uniq`
--

DROP TABLE IF EXISTS `zz_copy_vd_opp_sv_uniq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_vd_opp_sv_uniq` (
  `opportunities_idb` varchar(36) DEFAULT NULL,
  KEY `idx_opportunities_idb` (`opportunities_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_vd_opp_sv_uniq`
--

LOCK TABLES `zz_copy_vd_opp_sv_uniq` WRITE;
/*!40000 ALTER TABLE `zz_copy_vd_opp_sv_uniq` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_vd_opp_sv_uniq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_view_anken_list_fy10`
--

DROP TABLE IF EXISTS `zz_copy_view_anken_list_fy10`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_view_anken_list_fy10` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `modified_user_id` varchar(36) DEFAULT NULL,
  `assigned_user_id` varchar(36) DEFAULT NULL,
  `op_group_type` varchar(3) DEFAULT NULL,
  `EIGYO_SOSIKI_DISP_CD` varchar(21) DEFAULT NULL,
  `eigyo_sosiki_nm` varchar(128) DEFAULT NULL,
  `corp_cd` varchar(3) DEFAULT NULL,
  `mast_organizationunit_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit2_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit3_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit4_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit5_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit6_id` varchar(3) DEFAULT NULL,
  `sv_class` varchar(8) DEFAULT NULL,
  `sv_class_desc` varchar(20) DEFAULT NULL,
  `sv_cate` varchar(8) DEFAULT NULL,
  `sv_cate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sv_start_end` varchar(30) DEFAULT NULL,
  `sv_unit` varchar(20) DEFAULT NULL,
  `sv_num` double DEFAULT NULL,
  `sv_month_num` double DEFAULT NULL,
  `sv_temp_num` double DEFAULT NULL,
  `sv_line_num` double DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `anken_id_c` varchar(20) DEFAULT NULL,
  `opportunity_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_id` varchar(36) DEFAULT NULL,
  `owner_name` varchar(61) DEFAULT NULL,
  `main_sales_name` varchar(61) DEFAULT NULL,
  `op_step_c` varchar(8) DEFAULT NULL,
  `op_step_c_desc` varchar(20) DEFAULT NULL,
  `op_cont_plandate_c` date DEFAULT NULL,
  `op_order_poss_c` varchar(8) DEFAULT NULL,
  `op_order_poss_c_desc` varchar(20) DEFAULT NULL,
  `op_commit_c` varchar(8) DEFAULT NULL,
  `op_commit_c_desc` varchar(20) DEFAULT NULL,
  `date_modified_fmt` varchar(16) DEFAULT NULL,
  `key_driver_id` varchar(3) DEFAULT NULL,
  `key_driver_name` varchar(30) DEFAULT NULL,
  `sv_type` varchar(6) NOT NULL DEFAULT '',
  `sv_id` varchar(36) NOT NULL DEFAULT '',
  `date_entered_fmt` varchar(16) DEFAULT NULL,
  `sales_stage` varchar(200) DEFAULT NULL,
  `spm_orgunit_cd` varchar(8) DEFAULT NULL,
  `spm_orgunit_name` varchar(100) DEFAULT NULL,
  `op_depend_c` varchar(40) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `sv_date_modified` datetime DEFAULT NULL,
  `billiard_form` varchar(50) DEFAULT NULL,
  `billiard_model_code` varchar(50) DEFAULT NULL,
  `billiard_maker` varchar(50) DEFAULT NULL,
  `billiard_model` varchar(50) DEFAULT NULL,
  `billiard_color` varchar(50) DEFAULT NULL,
  `emphasis_strategy1_c` varchar(255) DEFAULT NULL,
  `emphasis_strategy2_c` varchar(255) DEFAULT NULL,
  `measure_dept_c` varchar(255) DEFAULT NULL,
  `measure_company_c` varchar(255) DEFAULT NULL,
  `em_strategy1_030` varchar(16) DEFAULT NULL,
  `em_strategy1_040` varchar(16) DEFAULT NULL,
  `em_strategy2_050` varchar(16) DEFAULT NULL,
  `em_strategy2_060` varchar(16) DEFAULT NULL,
  `em_strategy2_070` varchar(16) DEFAULT NULL,
  `em_strategy2_080` varchar(16) DEFAULT NULL,
  `em_strategy2_090` varchar(16) DEFAULT NULL,
  `em_strategy2_100` varchar(16) DEFAULT NULL,
  `billiard_choice_day` date DEFAULT NULL,
  `billiard_memo` varchar(255) DEFAULT NULL,
  `sv_date_modified_fmt` varchar(16) DEFAULT NULL,
  `mast_organizationunit1_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit2_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit3_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit4_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit5_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit6_nm` varchar(255) DEFAULT NULL,
  `op_cont_planmonth` char(7) DEFAULT NULL,
  `op_manage_forecast` varchar(20) DEFAULT NULL,
  `special_pri` double DEFAULT NULL,
  `special_bri` double DEFAULT NULL,
  `special_pri1` double DEFAULT NULL,
  `special_pri20` double DEFAULT NULL,
  `data_physics` double DEFAULT NULL,
  `data_change` double DEFAULT NULL,
  `data_get_physics` double DEFAULT NULL,
  `data_get_change` double DEFAULT NULL,
  `mobile` double DEFAULT NULL,
  `hs_sale_end` double DEFAULT NULL,
  `data_card_sale_end` double DEFAULT NULL,
  `smart_phone_sale_end` double DEFAULT NULL,
  `hs_allotment` double DEFAULT NULL,
  `data_card_allotment` double DEFAULT NULL,
  `smart_phone_allotment` double DEFAULT NULL,
  `hs_rental` double DEFAULT NULL,
  `data_card_rental` double DEFAULT NULL,
  `smart_phone_rental` double DEFAULT NULL,
  `phs` double DEFAULT NULL,
  `white_offers` double DEFAULT NULL,
  `model_change` double DEFAULT NULL,
  `ml` double DEFAULT NULL,
  `cloud_physics` double DEFAULT NULL,
  `cloud_change` double DEFAULT NULL,
  `yahoo_physics` double DEFAULT NULL,
  `yahoo_change` double DEFAULT NULL,
  `hs` double DEFAULT NULL,
  `smart_phone` double DEFAULT NULL,
  `data_card` double DEFAULT NULL,
  `ipad` double DEFAULT NULL,
  `googleapps` double DEFAULT NULL,
  PRIMARY KEY (`id`,`sv_id`,`sv_type`),
  KEY `idx_assigned_user_id` (`assigned_user_id`(7)),
  KEY `idx_organizationunit` (`op_group_type`,`mast_organizationunit_id`,`mast_organizationunit2_id`,`mast_organizationunit3_id`,`mast_organizationunit4_id`,`sv_month_num`,`sv_temp_num`,`sv_line_num`),
  KEY `idx_sv_class` (`sv_class`),
  KEY `idx_op_step_c` (`op_step_c`),
  KEY `idx_op_cont_plandate_c` (`op_cont_plandate_c`),
  KEY `idx_op_order_poss_c` (`op_order_poss_c`),
  KEY `idx_date_modified` (`date_modified_fmt`),
  KEY `idx_op_commit_c` (`op_commit_c`),
  KEY `idx_opportunity_name` (`opportunity_name`),
  KEY `idx_sv_cate_name` (`sv_cate_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_view_anken_list_fy10`
--

LOCK TABLES `zz_copy_view_anken_list_fy10` WRITE;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_view_anken_list_fy10_cstm_new`
--

DROP TABLE IF EXISTS `zz_copy_view_anken_list_fy10_cstm_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_view_anken_list_fy10_cstm_new` (
  `ID_C` char(36) NOT NULL,
  `description` varchar(800) DEFAULT NULL,
  `op_dep_detail_c` varchar(800) DEFAULT NULL,
  `op_compname_memo_c` varchar(255) DEFAULT NULL,
  `sup_id` varchar(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID_C`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_view_anken_list_fy10_cstm_new`
--

LOCK TABLES `zz_copy_view_anken_list_fy10_cstm_new` WRITE;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10_cstm_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10_cstm_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_copy_view_anken_list_fy10_new`
--

DROP TABLE IF EXISTS `zz_copy_view_anken_list_fy10_new`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_copy_view_anken_list_fy10_new` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `modified_user_id` varchar(36) DEFAULT NULL,
  `assigned_user_id` varchar(36) DEFAULT NULL,
  `op_group_type` varchar(3) DEFAULT NULL,
  `EIGYO_SOSIKI_DISP_CD` varchar(21) DEFAULT NULL,
  `eigyo_sosiki_nm` varchar(128) DEFAULT NULL,
  `corp_cd` varchar(3) DEFAULT NULL,
  `mast_organizationunit_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit2_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit3_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit4_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit5_id` varchar(3) DEFAULT NULL,
  `mast_organizationunit6_id` varchar(3) DEFAULT NULL,
  `sv_class` varchar(8) DEFAULT NULL,
  `sv_class_desc` varchar(30) DEFAULT NULL,
  `sv_cate` varchar(8) DEFAULT NULL,
  `sv_cate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sv_start_end` varchar(30) DEFAULT NULL,
  `sv_unit` varchar(20) DEFAULT NULL,
  `sv_num` double DEFAULT NULL,
  `sv_month_num` double DEFAULT NULL,
  `sv_temp_num` double DEFAULT NULL,
  `sv_line_num` double DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `anken_id_c` varchar(20) DEFAULT NULL,
  `opportunity_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_id` varchar(36) DEFAULT NULL,
  `owner_name` varchar(61) DEFAULT NULL,
  `main_sales_name` varchar(61) DEFAULT NULL,
  `op_step_c` varchar(8) DEFAULT NULL,
  `op_step_c_desc` varchar(20) DEFAULT NULL,
  `op_cont_plandate_c` date DEFAULT NULL,
  `op_order_poss_c` varchar(8) DEFAULT NULL,
  `op_order_poss_c_desc` varchar(20) DEFAULT NULL,
  `op_commit_c` varchar(8) DEFAULT NULL,
  `op_commit_c_desc` varchar(20) DEFAULT NULL,
  `date_modified_fmt` varchar(16) DEFAULT NULL,
  `key_driver_id` varchar(3) DEFAULT NULL,
  `key_driver_name` varchar(30) DEFAULT NULL,
  `sv_type` varchar(6) NOT NULL DEFAULT '',
  `sv_id` varchar(36) NOT NULL DEFAULT '',
  `date_entered_fmt` varchar(16) DEFAULT NULL,
  `sales_stage` varchar(200) DEFAULT NULL,
  `spm_orgunit_cd` varchar(8) DEFAULT NULL,
  `spm_orgunit_name` varchar(100) DEFAULT NULL,
  `op_depend_c` varchar(40) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `sv_date_modified` datetime DEFAULT NULL,
  `billiard_form` varchar(50) DEFAULT NULL,
  `billiard_model_code` varchar(50) DEFAULT NULL,
  `billiard_maker` varchar(50) DEFAULT NULL,
  `billiard_model` varchar(50) DEFAULT NULL,
  `billiard_color` varchar(50) DEFAULT NULL,
  `emphasis_strategy1_c` varchar(255) DEFAULT NULL,
  `emphasis_strategy2_c` varchar(255) DEFAULT NULL,
  `measure_dept_c` varchar(255) DEFAULT NULL,
  `measure_company_c` varchar(255) DEFAULT NULL,
  `em_strategy1_030` varchar(16) DEFAULT NULL,
  `em_strategy1_040` varchar(16) DEFAULT NULL,
  `em_strategy2_050` varchar(16) DEFAULT NULL,
  `em_strategy2_060` varchar(16) DEFAULT NULL,
  `em_strategy2_070` varchar(16) DEFAULT NULL,
  `em_strategy2_080` varchar(16) DEFAULT NULL,
  `em_strategy2_090` varchar(16) DEFAULT NULL,
  `em_strategy2_100` varchar(16) DEFAULT NULL,
  `billiard_choice_day` date DEFAULT NULL,
  `billiard_memo` varchar(255) DEFAULT NULL,
  `sv_date_modified_fmt` varchar(16) DEFAULT NULL,
  `mast_organizationunit1_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit2_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit3_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit4_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit5_nm` varchar(255) DEFAULT NULL,
  `mast_organizationunit6_nm` varchar(255) DEFAULT NULL,
  `op_cont_planmonth` char(7) DEFAULT NULL,
  `MONTH_NUM_LOCAL_CURRENCY` double(11,2) DEFAULT NULL,
  `MONTH_NUM_UNIT` varchar(5) DEFAULT NULL,
  `TEMP_NUM_LOCAL_CURRENCY` double(11,2) DEFAULT NULL,
  `TEMP_NUM_UNIT` varchar(5) DEFAULT NULL,
  `op_manage_forecast` varchar(20) DEFAULT NULL,
  `month_age` int(3) DEFAULT NULL,
  `special_pri` double DEFAULT NULL,
  `special_bri` double DEFAULT NULL,
  `special_pri1` double DEFAULT NULL,
  `special_pri20` double DEFAULT NULL,
  `data_physics` double DEFAULT NULL,
  `data_change` double DEFAULT NULL,
  `data_get_physics` double DEFAULT NULL,
  `data_get_change` double DEFAULT NULL,
  `mobile` double DEFAULT NULL,
  `hs_sale_end` double DEFAULT NULL,
  `data_card_sale_end` double DEFAULT NULL,
  `smart_phone_sale_end` double DEFAULT NULL,
  `hs_allotment` double DEFAULT NULL,
  `data_card_allotment` double DEFAULT NULL,
  `smart_phone_allotment` double DEFAULT NULL,
  `hs_rental` double DEFAULT NULL,
  `data_card_rental` double DEFAULT NULL,
  `smart_phone_rental` double DEFAULT NULL,
  `phs` double DEFAULT NULL,
  `white_offers` double DEFAULT NULL,
  `model_change` double DEFAULT NULL,
  `ml` double DEFAULT NULL,
  `cloud_physics` double DEFAULT NULL,
  `cloud_change` double DEFAULT NULL,
  `yahoo_physics` double DEFAULT NULL,
  `yahoo_change` double DEFAULT NULL,
  `hs` double DEFAULT NULL,
  `smart_phone` double DEFAULT NULL,
  `data_card` double DEFAULT NULL,
  `ipad` double DEFAULT NULL,
  `googleapps` double DEFAULT NULL,
  `o2o` double DEFAULT NULL,
  `paypal` double DEFAULT NULL,
  PRIMARY KEY (`id`,`sv_id`,`sv_type`),
  KEY `idx_assigned_user_id` (`assigned_user_id`(7)),
  KEY `idx_organizationunit` (`op_group_type`,`mast_organizationunit_id`,`mast_organizationunit2_id`,`mast_organizationunit3_id`,`mast_organizationunit4_id`,`sv_month_num`,`sv_temp_num`,`sv_line_num`),
  KEY `idx_sv_class` (`sv_class`),
  KEY `idx_op_step_c` (`op_step_c`),
  KEY `idx_op_cont_plandate_c` (`op_cont_plandate_c`),
  KEY `idx_op_order_poss_c` (`op_order_poss_c`),
  KEY `idx_date_modified` (`date_modified_fmt`),
  KEY `idx_op_commit_c` (`op_commit_c`),
  KEY `idx_opportunity_name` (`opportunity_name`),
  KEY `idx_sv_cate_name` (`sv_cate_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_copy_view_anken_list_fy10_new`
--

LOCK TABLES `zz_copy_view_anken_list_fy10_new` WRITE;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10_new` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_copy_view_anken_list_fy10_new` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_snap_login_user_count`
--

DROP TABLE IF EXISTS `zz_snap_login_user_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_snap_login_user_count` (
  `yyyymmdd` varchar(16) NOT NULL DEFAULT '',
  `hhmm` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(16) NOT NULL DEFAULT '',
  `cnt` int(4) DEFAULT NULL,
  PRIMARY KEY (`yyyymmdd`,`hhmm`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_snap_login_user_count`
--

LOCK TABLES `zz_snap_login_user_count` WRITE;
/*!40000 ALTER TABLE `zz_snap_login_user_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_snap_login_user_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_snap_login_user_count_2008`
--

DROP TABLE IF EXISTS `zz_snap_login_user_count_2008`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_snap_login_user_count_2008` (
  `yyyymmdd` varchar(16) NOT NULL DEFAULT '',
  `hhmm` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(16) NOT NULL DEFAULT '',
  `cnt` int(4) DEFAULT NULL,
  PRIMARY KEY (`yyyymmdd`,`hhmm`,`user_id`) KEY_BLOCK_SIZE=1024
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_snap_login_user_count_2008`
--

LOCK TABLES `zz_snap_login_user_count_2008` WRITE;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2008` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2008` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_snap_login_user_count_2009`
--

DROP TABLE IF EXISTS `zz_snap_login_user_count_2009`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_snap_login_user_count_2009` (
  `yyyymmdd` varchar(16) NOT NULL DEFAULT '',
  `hhmm` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(16) NOT NULL DEFAULT '',
  `cnt` int(4) DEFAULT NULL,
  PRIMARY KEY (`yyyymmdd`,`hhmm`,`user_id`) KEY_BLOCK_SIZE=1024
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_snap_login_user_count_2009`
--

LOCK TABLES `zz_snap_login_user_count_2009` WRITE;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2009` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2009` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_snap_login_user_count_2010`
--

DROP TABLE IF EXISTS `zz_snap_login_user_count_2010`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_snap_login_user_count_2010` (
  `yyyymmdd` varchar(16) NOT NULL DEFAULT '',
  `hhmm` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(16) NOT NULL DEFAULT '',
  `cnt` int(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_snap_login_user_count_2010`
--

LOCK TABLES `zz_snap_login_user_count_2010` WRITE;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2010` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2010` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_snap_login_user_count_2011`
--

DROP TABLE IF EXISTS `zz_snap_login_user_count_2011`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_snap_login_user_count_2011` (
  `yyyymmdd` varchar(16) NOT NULL DEFAULT '',
  `hhmm` varchar(10) NOT NULL DEFAULT '',
  `user_id` varchar(16) NOT NULL DEFAULT '',
  `cnt` int(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_snap_login_user_count_2011`
--

LOCK TABLES `zz_snap_login_user_count_2011` WRITE;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2011` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_snap_login_user_count_2011` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `ifview_eida_accounts`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_accounts`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_accounts` AS select `accounts`.`id` AS `ID`,`accounts`.`name` AS `NAME`,`accounts`.`date_entered` AS `DATE_ENTERED`,`accounts`.`date_modified` AS `DATE_MODIFIED`,`accounts`.`modified_user_id` AS `MODIFIED_USER_ID`,`accounts`.`created_by` AS `CREATED_BY`,`accounts`.`description` AS `DESCRIPTION`,`accounts`.`deleted` AS `DELETED`,`accounts`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`accounts`.`account_type` AS `ACCOUNT_TYPE`,`accounts`.`industry` AS `INDUSTRY`,`accounts`.`annual_revenue` AS `ANNUAL_REVENUE`,`accounts`.`phone_fax` AS `PHONE_FAX`,`accounts`.`billing_address_street` AS `BILLING_ADDRESS_STREET`,`accounts`.`billing_address_city` AS `BILLING_ADDRESS_CITY`,`accounts`.`billing_address_state` AS `BILLING_ADDRESS_STATE`,`accounts`.`billing_address_postalcode` AS `BILLING_ADDRESS_POSTALCODE`,`accounts`.`billing_address_country` AS `BILLING_ADDRESS_COUNTRY`,`accounts`.`rating` AS `RATING`,`accounts`.`phone_office` AS `PHONE_OFFICE`,`accounts`.`phone_alternate` AS `PHONE_ALTERNATE`,`accounts`.`website` AS `WEBSITE`,`accounts`.`ownership` AS `OWNERSHIP`,`accounts`.`employees` AS `EMPLOYEES`,`accounts`.`ticker_symbol` AS `TICKER_SYMBOL`,`accounts`.`shipping_address_street` AS `SHIPPING_ADDRESS_STREET`,`accounts`.`shipping_address_city` AS `SHIPPING_ADDRESS_CITY`,`accounts`.`shipping_address_state` AS `SHIPPING_ADDRESS_STATE`,`accounts`.`shipping_address_postalcode` AS `SHIPPING_ADDRESS_POSTALCODE`,`accounts`.`shipping_address_country` AS `SHIPPING_ADDRESS_COUNTRY`,`accounts`.`parent_id` AS `PARENT_ID`,`accounts`.`sic_code` AS `SIC_CODE`,`accounts`.`campaign_id` AS `CAMPAIGN_ID`,`accounts`.`tdb_comp_cd` AS `TDB_COMP_CD`,`accounts`.`spm_comp_cd` AS `SPM_COMP_CD`,`accounts`.`orgunit_voice_cd` AS `ORGUNIT_VOICE_CD`,`accounts`.`orgunit_voice_name` AS `ORGUNIT_VOICE_NAME`,`accounts`.`orgunit_data_cd` AS `ORGUNIT_DATA_CD`,`accounts`.`orgunit_data_name` AS `ORGUNIT_DATA_NAME`,`accounts`.`name_kana` AS `NAME_KANA`,`accounts`.`orgunit_voice_market` AS `ORGUNIT_VOICE_MARKET`,`accounts`.`orgunit_data_market` AS `ORGUNIT_DATA_MARKET`,`accounts`.`assigned_user_data_id` AS `ASSIGNED_USER_DATA_ID` from `accounts` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_accounts_cases`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_accounts_cases`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_cases`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_accounts_cases` AS select `accounts_cases`.`id` AS `ID`,`accounts_cases`.`account_id` AS `ACCOUNT_ID`,`accounts_cases`.`case_id` AS `CASE_ID`,`accounts_cases`.`date_modified` AS `DATE_MODIFIED`,`accounts_cases`.`deleted` AS `DELETED` from `accounts_cases` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_accounts_contacts`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_accounts_contacts`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_contacts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_accounts_contacts` AS select `accounts_contacts`.`id` AS `ID`,`accounts_contacts`.`contact_id` AS `CONTACT_ID`,`accounts_contacts`.`account_id` AS `ACCOUNT_ID`,addtime(`accounts_contacts`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`accounts_contacts`.`deleted` AS `DELETED` from `accounts_contacts` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_accounts_opportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_accounts_opportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_accounts_opportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_accounts_opportunities` AS select `accounts_opportunities`.`id` AS `ID`,`accounts_opportunities`.`opportunity_id` AS `OPPORTUNITY_ID`,`accounts_opportunities`.`account_id` AS `ACCOUNT_ID`,addtime(`accounts_opportunities`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`accounts_opportunities`.`deleted` AS `DELETED` from `accounts_opportunities` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_calls`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_calls`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_calls` AS select `aa`.`id` AS `ID`,`aa`.`name` AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`duration_hours` AS `DURATION_HOURS`,`aa`.`duration_minutes` AS `DURATION_MINUTES`,addtime(`aa`.`date_start`,'09:00:00') AS `DATE_START`,date_format(addtime(`aa`.`date_end`,'09:00:00'),'%Y/%m/%d') AS `DATE_END`,`aa`.`parent_type` AS `PARENT_TYPE`,`aa`.`status` AS `STATUS`,`aa`.`direction` AS `DIRECTION`,`aa`.`parent_id` AS `PARENT_ID`,`aa`.`reminder_time` AS `REMINDER_TIME`,`aa`.`outlook_id` AS `OUTLOOK_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`account_id_c` AS `ACCOUNT_ID_C`,`bb`.`contact_id_c` AS `CONTACT_ID_C`,`bb`.`contact_name_c` AS `CONTACT_NAME_C`,`bb`.`next_step_c` AS `NEXT_STEP_C`,`bb`.`op_corp_c` AS `OP_CORP_C`,`bb`.`com_free_c` AS `COM_FREE_C`,`bb`.`pam_name_c` AS `PAM_NAME_C`,`bb`.`pam_sv_num_c` AS `PAM_SV_NUM_C`,trim(both '^,^' from `bb`.`companion_c`) AS `COMPANION_C`,`bb`.`time_end_c` AS `TIME_END_C`,`bb`.`division_c` AS `DIVISION_C`,`bb`.`googlecal_id_c` AS `GOOGLECAL_ID_C`,`bb`.`googlecal_published_c` AS `GOOGLECAL_PUBLISHED_C`,`bb`.`googlecal_updated_c` AS `GOOGLECAL_UPDATED_C`,`bb`.`googlecal_link_edit_c` AS `GOOGLECAL_LINK_EDIT_C`,`bb`.`googlecal_author_c` AS `GOOGLECAL_AUTHOR_C`,`bb`.`googlecal_email_c` AS `GOOGLECAL_EMAIL_C`,trim(both '^,^' from `bb`.`contact_title_c`) AS `CONTACT_TITLE_C`,trim(both '^,^' from `bb`.`contact_department_c`) AS `CONTACT_DEPARTMENT_C` from (`calls` `aa` left join `calls_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_calls_contacts`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_calls_contacts`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_contacts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_calls_contacts` AS select `calls_contacts`.`id` AS `ID`,`calls_contacts`.`call_id` AS `CALL_ID`,`calls_contacts`.`contact_id` AS `CONTACT_ID`,`calls_contacts`.`required` AS `REQUIRED`,`calls_contacts`.`accept_status` AS `ACCEPT_STATUS`,addtime(`calls_contacts`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`calls_contacts`.`deleted` AS `DELETED` from `calls_contacts` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_calls_n`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_calls_n`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_n`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_calls_n` AS select `aa`.`id` AS `ID`,`aa`.`name` AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`duration_hours` AS `DURATION_HOURS`,`aa`.`duration_minutes` AS `DURATION_MINUTES`,addtime(`aa`.`date_start`,'09:00:00') AS `DATE_START`,date_format(addtime(`aa`.`date_end`,'09:00:00'),'%Y/%m/%d') AS `DATE_END`,`aa`.`parent_type` AS `PARENT_TYPE`,`aa`.`status` AS `STATUS`,`aa`.`direction` AS `DIRECTION`,`aa`.`parent_id` AS `PARENT_ID`,`aa`.`reminder_time` AS `REMINDER_TIME`,`aa`.`outlook_id` AS `OUTLOOK_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`account_id_c` AS `ACCOUNT_ID_C`,`bb`.`contact_id_c` AS `CONTACT_ID_C`,`bb`.`contact_name_c` AS `CONTACT_NAME_C`,`bb`.`next_step_c` AS `NEXT_STEP_C`,`bb`.`op_corp_c` AS `OP_CORP_C`,`bb`.`com_free_c` AS `COM_FREE_C`,`bb`.`pam_name_c` AS `PAM_NAME_C`,`bb`.`pam_sv_num_c` AS `PAM_SV_NUM_C`,trim(both '^,^' from `bb`.`companion_c`) AS `COMPANION_C`,`bb`.`time_end_c` AS `TIME_END_C`,`bb`.`division_c` AS `DIVISION_C`,`bb`.`googlecal_id_c` AS `GOOGLECAL_ID_C`,`bb`.`googlecal_published_c` AS `GOOGLECAL_PUBLISHED_C`,`bb`.`googlecal_updated_c` AS `GOOGLECAL_UPDATED_C`,`bb`.`googlecal_link_edit_c` AS `GOOGLECAL_LINK_EDIT_C`,`bb`.`googlecal_author_c` AS `GOOGLECAL_AUTHOR_C`,`bb`.`googlecal_email_c` AS `GOOGLECAL_EMAIL_C`,trim(both '^,^' from `bb`.`contact_title_c`) AS `CONTACT_TITLE_C`,trim(both '^,^' from `bb`.`contact_department_c`) AS `CONTACT_DEPARTMENT_C` from (`calls` `aa` left join `calls_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_calls_no_text`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_calls_no_text`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_no_text`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_calls_no_text` AS select `aa`.`id` AS `ID`,left(`aa`.`name`,12) AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`duration_hours` AS `DURATION_HOURS`,`aa`.`duration_minutes` AS `DURATION_MINUTES`,addtime(`aa`.`date_start`,'09:00:00') AS `DATE_START`,date_format(addtime(`aa`.`date_end`,'09:00:00'),'%Y/%m/%d') AS `DATE_END`,`aa`.`parent_type` AS `PARENT_TYPE`,`aa`.`status` AS `STATUS`,`aa`.`direction` AS `DIRECTION`,`aa`.`parent_id` AS `PARENT_ID`,`aa`.`reminder_time` AS `REMINDER_TIME`,`aa`.`outlook_id` AS `OUTLOOK_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`account_id_c` AS `ACCOUNT_ID_C`,`bb`.`contact_id_c` AS `CONTACT_ID_C`,`bb`.`contact_name_c` AS `CONTACT_NAME_C`,`bb`.`next_step_c` AS `NEXT_STEP_C`,`bb`.`op_corp_c` AS `OP_CORP_C`,NULL AS `COM_FREE_C`,`bb`.`pam_name_c` AS `PAM_NAME_C`,`bb`.`pam_sv_num_c` AS `PAM_SV_NUM_C`,trim(both '^,^' from `bb`.`companion_c`) AS `COMPANION_C`,`bb`.`time_end_c` AS `TIME_END_C`,`bb`.`division_c` AS `DIVISION_C`,`bb`.`googlecal_id_c` AS `GOOGLECAL_ID_C`,`bb`.`googlecal_published_c` AS `GOOGLECAL_PUBLISHED_C`,`bb`.`googlecal_updated_c` AS `GOOGLECAL_UPDATED_C`,`bb`.`googlecal_link_edit_c` AS `GOOGLECAL_LINK_EDIT_C`,`bb`.`googlecal_author_c` AS `GOOGLECAL_AUTHOR_C`,`bb`.`googlecal_email_c` AS `GOOGLECAL_EMAIL_C`,trim(both '^,^' from `bb`.`contact_title_c`) AS `CONTACT_TITLE_C`,trim(both '^,^' from `bb`.`contact_title_c`) AS `CONTACT_DEPARTMENT_C` from (`calls` `aa` left join `calls_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_calls_users`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_calls_users`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_calls_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_calls_users` AS select `calls_users`.`id` AS `ID`,`calls_users`.`call_id` AS `CALL_ID`,`calls_users`.`user_id` AS `USER_ID`,`calls_users`.`required` AS `REQUIRED`,`calls_users`.`accept_status` AS `ACCEPT_STATUS`,addtime(`calls_users`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`calls_users`.`deleted` AS `DELETED` from `calls_users` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_contacts_cases`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_contacts_cases`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_contacts_cases`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_contacts_cases` AS select `contacts_cases`.`id` AS `ID`,`contacts_cases`.`contact_id` AS `CONTACT_ID`,`contacts_cases`.`case_id` AS `CASE_ID`,`contacts_cases`.`contact_role` AS `CONTACT_ROLE`,`contacts_cases`.`date_modified` AS `DATE_MODIFIED`,`contacts_cases`.`deleted` AS `DELETED` from `contacts_cases` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_email_addr_bean_rel`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_email_addr_bean_rel`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_email_addr_bean_rel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_email_addr_bean_rel` AS select `email_addr_bean_rel`.`id` AS `ID`,`email_addr_bean_rel`.`email_address_id` AS `EMAIL_ADDRESS_ID`,`email_addr_bean_rel`.`bean_id` AS `BEAN_ID`,`email_addr_bean_rel`.`bean_module` AS `BEAN_MODULE`,`email_addr_bean_rel`.`primary_address` AS `PRIMARY_ADDRESS`,`email_addr_bean_rel`.`reply_to_address` AS `REPLY_TO_ADDRESS`,addtime(`email_addr_bean_rel`.`date_created`,'09:00:00') AS `DATE_CREATED`,addtime(`email_addr_bean_rel`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`email_addr_bean_rel`.`deleted` AS `DELETED` from `email_addr_bean_rel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_email_addresses`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_email_addresses`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_email_addresses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_email_addresses` AS select `email_addresses`.`id` AS `ID`,`email_addresses`.`email_address` AS `EMAIL_ADDRESS`,`email_addresses`.`email_address_caps` AS `EMAIL_ADDRESS_CAPS`,`email_addresses`.`invalid_email` AS `INVALID_EMAIL`,`email_addresses`.`opt_out` AS `OPT_OUT`,addtime(`email_addresses`.`date_created`,'09:00:00') AS `DATE_CREATED`,addtime(`email_addresses`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`email_addresses`.`deleted` AS `DELETED` from `email_addresses` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_leads`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_leads`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_leads` AS select `aa`.`id` AS `ID`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`lead_lead_source` AS `LEAD_LEAD_SOURCE`,`aa`.`lead_type` AS `LEAD_TYPE`,`aa`.`lead_status` AS `LEAD_STATUS`,`aa`.`lead_account_name` AS `LEAD_ACCOUNT_NAME`,`aa`.`lead_close_reason` AS `LEAD_CLOSE_REASON`,`aa`.`close_opportunities_id` AS `CLOSE_OPPORTUNITIES_ID`,`bb`.`lead_sale_item_c` AS `LEAD_SALE_ITEM_C` from (`leads` `aa` left join `leads_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_leads_accounts`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_leads_accounts`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_accounts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_leads_accounts` AS select `aa`.`id` AS `ID`,`aa`.`lead_id` AS `LEAD_ID`,`aa`.`account_id` AS `ACCOUNT_ID`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`deleted` AS `DELETED` from `leads_accounts` `aa` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_leads_contacts`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_leads_contacts`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_contacts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_leads_contacts` AS select `aa`.`id` AS `ID`,`aa`.`lead_id` AS `LEAD_ID`,`aa`.`contact_id` AS `CONTACT_ID`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`deleted` AS `DELETED` from `leads_contacts` `aa` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_leads_opportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_leads_opportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_leads_opportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_leads_opportunities` AS select `aa`.`id` AS `ID`,`aa`.`lead_id` AS `LEAD_ID`,`aa`.`opportunity_id` AS `OPPORTUNITY_ID`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`deleted` AS `DELETED` from `leads_opportunities` `aa` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_mast_billiard`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_mast_billiard`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_billiard`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_mast_billiard` AS select `mast_billiard`.`id` AS `id`,`mast_billiard`.`name` AS `name`,`mast_billiard`.`date_entered` AS `DATE_ENTERED`,`mast_billiard`.`date_modified` AS `DATE_MODIFIED`,`mast_billiard`.`modified_user_id` AS `modified_user_id`,`mast_billiard`.`created_by` AS `created_by`,`mast_billiard`.`description` AS `description`,`mast_billiard`.`deleted` AS `deleted`,`mast_billiard`.`assigned_user_id` AS `assigned_user_id`,`mast_billiard`.`billiard_maker` AS `billiard_maker`,`mast_billiard`.`billiard_model` AS `billiard_model`,`mast_billiard`.`billiard_color` AS `billiard_color`,`mast_billiard`.`billiard_order` AS `billiard_order`,`mast_billiard`.`display_disabled` AS `display_disabled` from `mast_billiard` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_mast_organizationunit`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_mast_organizationunit`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_organizationunit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_mast_organizationunit` AS select `mast_organizationunit`.`id` AS `ID`,`mast_organizationunit`.`name` AS `NAME`,addtime(`mast_organizationunit`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`mast_organizationunit`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`mast_organizationunit`.`modified_user_id` AS `MODIFIED_USER_ID`,`mast_organizationunit`.`created_by` AS `CREATED_BY`,`mast_organizationunit`.`description` AS `DESCRIPTION`,`mast_organizationunit`.`deleted` AS `DELETED`,`mast_organizationunit`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`mast_organizationunit`.`ordrno` AS `ORDRNO`,`mast_organizationunit`.`ogusnm` AS `OGUSNM`,`mast_organizationunit`.`ogutid` AS `OGUTID`,`mast_organizationunit`.`ogutcl` AS `OGUTCL`,`mast_organizationunit`.`upogcd` AS `UPOGCD`,`mast_organizationunit`.`mgsfcd` AS `MGSFCD` from `mast_organizationunit` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_mast_serviceline`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_mast_serviceline`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_mast_serviceline`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_mast_serviceline` AS select `mast_serviceline`.`id` AS `ID`,`mast_serviceline`.`name` AS `NAME`,addtime(`mast_serviceline`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`mast_serviceline`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`mast_serviceline`.`modified_user_id` AS `MODIFIED_USER_ID`,`mast_serviceline`.`created_by` AS `CREATED_BY`,`mast_serviceline`.`description` AS `DESCRIPTION`,`mast_serviceline`.`deleted` AS `DELETED`,`mast_serviceline`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`mast_serviceline`.`svlsnm` AS `SVLSNM`,`mast_serviceline`.`slgrcd` AS `SLGRCD`,`mast_serviceline`.`slgrnm` AS `SLGRNM`,`mast_serviceline`.`ordrno` AS `ORDRNO`,`mast_serviceline`.`fcqtcl` AS `FCQTCL`,`mast_serviceline`.`fqfxfg` AS `FQFXFG`,`mast_serviceline`.`fmymfg` AS `FMYMFG`,`mast_serviceline`.`toymfg` AS `TOYMFG`,`mast_serviceline`.`evlnrt` AS `EVLNRT`,`mast_serviceline`.`dvmtfg` AS `DVMTFG`,`mast_serviceline`.`svdtfg` AS `SVDTFG`,`mast_serviceline`.`fcqtnm` AS `FCQTNM`,`mast_serviceline`.`key_driver_id` AS `KEY_DRIVER_ID`,`mast_serviceline`.`key_driver_name` AS `KEY_DRIVER_NAME`,`mast_serviceline`.`key_driver_ordrno` AS `KEY_DRIVER_ORDRNO`,`mast_serviceline`.`lending_guarantee_id` AS `LENDING_GUARANTEE_ID`,`mast_serviceline`.`lending_guarantee_name` AS `LENDING_GUARANTEE_NAME` from `mast_serviceline` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opadd_partner`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opadd_partner`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opadd_partner`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opadd_partner` AS select `aa`.`id` AS `ID`,`aa`.`name` AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`parent_type` AS `PARENT_TYPE`,`aa`.`parent_id` AS `PARENT_ID`,`aa`.`account_id` AS `ACCOUNT_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`test_c` AS `TEST_C`,`bb`.`account_id_c` AS `ACCOUNT_ID_C`,`bb`.`account_name_c` AS `ACCOUNT_NAME_C` from (`opadd_partner` `aa` left join `opadd_partner_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opadd_partnpportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opadd_partnpportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opadd_partnpportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opadd_partnpportunities` AS select `opadd_partnpportunities`.`id` AS `ID`,addtime(`opadd_partnpportunities`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`opadd_partnpportunities`.`deleted` AS `DELETED`,`opadd_partnpportunities`.`opadd_partner_ida` AS `OPADD_PARTNER_IDA`,`opadd_partnpportunities`.`opportunities_idb` AS `OPPORTUNITIES_IDB` from `opadd_partnpportunities` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opp_salessup`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opp_salessup`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_salessup`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opp_salessup` AS select `opp_salessup`.`id` AS `ID`,`opp_salessup`.`name` AS `NAME`,addtime(`opp_salessup`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`opp_salessup`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`opp_salessup`.`modified_user_id` AS `MODIFIED_USER_ID`,`opp_salessup`.`created_by` AS `CREATED_BY`,`opp_salessup`.`description` AS `DESCRIPTION`,`opp_salessup`.`deleted` AS `DELETED`,`opp_salessup`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`opp_salessup`.`role_proposed` AS `ROLE_PROPOSED`,`opp_salessup`.`ps_sales` AS `PS_SALES` from `opp_salessup` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opp_salessupportunities_c`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opp_salessupportunities_c`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_salessupportunities_c`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opp_salessupportunities_c` AS select `opp_salessupportunities_c`.`id` AS `ID`,addtime(`opp_salessupportunities_c`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`opp_salessupportunities_c`.`deleted` AS `DELETED`,`opp_salessupportunities_c`.`opp_saless3af5unities_ida` AS `OPP_SALESS3AF5UNITIES_IDA`,`opp_salessupportunities_c`.`opp_saless78cealessup_idb` AS `OPP_SALESS78CEALESSUP_IDB` from `opp_salessupportunities_c` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opp_se`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opp_se`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opp_se` AS select `opp_se`.`id` AS `ID`,`opp_se`.`name` AS `NAME`,addtime(`opp_se`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`opp_se`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`opp_se`.`modified_user_id` AS `MODIFIED_USER_ID`,`opp_se`.`created_by` AS `CREATED_BY`,`opp_se`.`description` AS `DESCRIPTION`,`opp_se`.`deleted` AS `DELETED`,`opp_se`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`opp_se`.`status` AS `STATUS`,`opp_se`.`seorderaccuracy` AS `SEORDERACCURACY`,`opp_se`.`projectname` AS `PROJECTNAME`,`opp_se`.`sestrategicclassification` AS `SESTRATEGICCLASSIFICATION`,`opp_se`.`seitemsummary` AS `SEITEMSUMMARY`,`opp_se`.`salestempnum` AS `SALESTEMPNUM`,`opp_se`.`salesmonthnum` AS `SALESMONTHNUM`,`opp_se`.`contracttermmonthlybasis` AS `CONTRACTTERMMONTHLYBASIS`,`opp_se`.`pssalestempnum` AS `PSSALESTEMPNUM`,`opp_se`.`pssalesmonthnum` AS `PSSALESMONTHNUM`,`opp_se`.`psbudgetschedule` AS `PSBUDGETSCHEDULE`,`opp_se`.`causeorderdfailure` AS `CAUSEORDERDFAILURE`,`opp_se`.`spare` AS `SPARE` from `opp_se` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opp_se_audit`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opp_se_audit`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se_audit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opp_se_audit` AS select `opp_se_audit`.`id` AS `ID`,`opp_se_audit`.`parent_id` AS `PARENT_ID`,addtime(`opp_se_audit`.`date_created`,'09:00:00') AS `DATE_CREATED`,`opp_se_audit`.`created_by` AS `CREATED_BY`,`opp_se_audit`.`field_name` AS `FIELD_NAME`,`opp_se_audit`.`data_type` AS `DATA_TYPE`,`opp_se_audit`.`before_value_string` AS `BEFORE_VALUE_STRING`,`opp_se_audit`.`after_value_string` AS `AFTER_VALUE_STRING`,`opp_se_audit`.`before_value_text` AS `BEFORE_VALUE_TEXT`,`opp_se_audit`.`after_value_text` AS `AFTER_VALUE_TEXT` from `opp_se_audit` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opp_se_opportunities_c`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opp_se_opportunities_c`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opp_se_opportunities_c`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opp_se_opportunities_c` AS select `opp_se_opportunities_c`.`id` AS `ID`,addtime(`opp_se_opportunities_c`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`opp_se_opportunities_c`.`deleted` AS `DELETED`,`opp_se_opportunities_c`.`opp_se_opp7a17unities_ida` AS `OPP_SE_OPP7A17UNITIES_IDA`,`opp_se_opportunities_c`.`opp_se_opp1446sopp_se_idb` AS `OPP_SE_OPP1446SOPP_SE_IDB` from `opp_se_opportunities_c` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opportunities` AS select `aa`.`id` AS `ID`,`aa`.`name` AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`description` AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`opportunity_type` AS `OPPORTUNITY_TYPE`,`aa`.`campaign_id` AS `CAMPAIGN_ID`,`aa`.`lead_source` AS `LEAD_SOURCE`,`aa`.`amount` AS `AMOUNT`,`aa`.`amount_usdollar` AS `AMOUNT_USDOLLAR`,`aa`.`currency_id` AS `CURRENCY_ID`,`aa`.`date_closed` AS `DATE_CLOSED`,`aa`.`next_step` AS `NEXT_STEP`,`aa`.`sales_stage` AS `SALES_STAGE`,`aa`.`probability` AS `PROBABILITY`,`aa`.`owner_id` AS `OWNER_ID`,`aa`.`sup_id` AS `SUP_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`aaa_c` AS `AAA_C`,`bb`.`op_compe_c` AS `OP_COMPE_C`,`bb`.`op_compname_memo_c` AS `OP_COMPNAME_MEMO_C`,`bb`.`op_cont_plandate_c` AS `OP_CONT_PLANDATE_C`,`bb`.`op_data_organ_c` AS `OP_DATA_ORGAN_C`,`bb`.`op_dep_detail_c` AS `OP_DEP_DETAIL_C`,`bb`.`op_depend_c` AS `OP_DEPEND_C`,`bb`.`op_endsv_cate_c` AS `OP_ENDSV_CATE_C`,`bb`.`op_endsv_class_c` AS `OP_ENDSV_CLASS_C`,`bb`.`op_endsv_num_c` AS `OP_ENDSV_NUM_C`,`bb`.`op_endsv_unit_c` AS `OP_ENDSV_UNIT_C`,`bb`.`op_endsvend_mm_c` AS `OP_ENDSVEND_MM_C`,`bb`.`op_endsvend_yyyy_c` AS `OP_ENDSVEND_YYYY_C`,`bb`.`op_endsvstart_mm_c` AS `OP_ENDSVSTART_MM_C`,`bb`.`op_endsvstart_yyyy_c` AS `OP_ENDSVSTART_YYYY_C`,`bb`.`op_newsv_cate_c` AS `OP_NEWSV_CATE_C`,`bb`.`op_newsv_class_c` AS `OP_NEWSV_CLASS_C`,`bb`.`op_newsv_num_c` AS `OP_NEWSV_NUM_C`,`bb`.`op_newsv_unit_c` AS `OP_NEWSV_UNIT_C`,`bb`.`op_newsvend_mm_c` AS `OP_NEWSVEND_MM_C`,`bb`.`op_newsvend_yyyy_c` AS `OP_NEWSVEND_YYYY_C`,`bb`.`op_newsvstart_mm_c` AS `OP_NEWSVSTART_MM_C`,`bb`.`op_newsvstart_yyyy_c` AS `OP_NEWSVSTART_YYYY_C`,`bb`.`op_onseikeitai_organ_c` AS `OP_ONSEIKEITAI_ORGAN_C`,`bb`.`op_order_poss_c` AS `OP_ORDER_POSS_C`,`bb`.`op_owner_c` AS `OP_OWNER_C`,`bb`.`op_partner_c` AS `OP_PARTNER_C`,`bb`.`op_sales_main_c` AS `OP_SALES_MAIN_C`,`bb`.`op_sales_support_c` AS `OP_SALES_SUPPORT_C`,`bb`.`op_step_c` AS `OP_STEP_C`,`bb`.`op_sts_c` AS `OP_STS_C`,`bb`.`op_sv_cate_c` AS `OP_SV_CATE_C`,`bb`.`op_commit_c` AS `OP_COMMIT_C`,`bb`.`op_forecast_c` AS `OP_FORECAST_C`,`bb`.`opportunity_number_c` AS `OPPORTUNITY_NUMBER_C`,`bb`.`anken_id_c` AS `ANKEN_ID_C`,`bb`.`doc_id_c` AS `DOC_ID_C`,`bb`.`proposal1_c` AS `PROPOSAL1_C`,`bb`.`proposal_price1_c` AS `PROPOSAL_PRICE1_C`,`bb`.`discount_rate1_c` AS `DISCOUNT_RATE1_C`,`bb`.`proposal2_c` AS `PROPOSAL2_C`,`bb`.`proposal_price2_c` AS `PROPOSAL_PRICE2_C`,`bb`.`discount_rate2_c` AS `DISCOUNT_RATE2_C`,`bb`.`closed_situation_c` AS `CLOSED_SITUATION_C`,`bb`.`victory_defeat_cause_c` AS `VICTORY_DEFEAT_CAUSE_C`,`bb`.`victory_defeat_cause_detail_c` AS `VICTORY_DEFEAT_CAUSE_DETAIL_C`,`bb`.`emphasis_strategy1_c` AS `EMPHASIS_STRATEGY1_C`,`bb`.`emphasis_strategy2_c` AS `EMPHASIS_STRATEGY2_C`,`bb`.`measure_dept_c` AS `MEASURE_DEPT_C`,`bb`.`measure_company_c` AS `MEASURE_COMPANY_C`,`bb`.`y_interest_c` AS `Y_INTEREST_C`,`bb`.`y_interest_area_c` AS `Y_INTEREST_AREA_C`,`bb`.`y_ng_reason_c` AS `Y_NG_REASON_C`,`bb`.`y_agency_c` AS `Y_AGENCY_C`,`bb`.`y_medium_c` AS `Y_MEDIUM_C`,`bb`.`y_remarks_c` AS `Y_REMARKS_C`,`bb`.`y_posturl_c` AS `Y_POSTURL_C` from (`opportunities` `aa` left join `opportunities_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opportunities_audit`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opportunities_audit`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_audit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opportunities_audit` AS select `opportunities_audit`.`id` AS `ID`,`opportunities_audit`.`parent_id` AS `PARENT_ID`,addtime(`opportunities_audit`.`date_created`,'09:00:00') AS `DATE_CREATED`,`opportunities_audit`.`created_by` AS `CREATED_BY`,`opportunities_audit`.`field_name` AS `FIELD_NAME`,`opportunities_audit`.`data_type` AS `DATA_TYPE`,`opportunities_audit`.`before_value_string` AS `BEFORE_VALUE_STRING`,`opportunities_audit`.`after_value_string` AS `AFTER_VALUE_STRING`,`opportunities_audit`.`before_value_text` AS `BEFORE_VALUE_TEXT`,`opportunities_audit`.`after_value_text` AS `AFTER_VALUE_TEXT` from `opportunities_audit` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opportunities_no_text`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opportunities_no_text`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_no_text`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opportunities_no_text` AS select `aa`.`id` AS `ID`,`aa`.`name` AS `NAME`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,NULL AS `DESCRIPTION`,`aa`.`deleted` AS `DELETED`,`aa`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`aa`.`opportunity_type` AS `OPPORTUNITY_TYPE`,`aa`.`campaign_id` AS `CAMPAIGN_ID`,`aa`.`lead_source` AS `LEAD_SOURCE`,`aa`.`amount` AS `AMOUNT`,`aa`.`amount_usdollar` AS `AMOUNT_USDOLLAR`,`aa`.`currency_id` AS `CURRENCY_ID`,`aa`.`date_closed` AS `DATE_CLOSED`,`aa`.`next_step` AS `NEXT_STEP`,`aa`.`sales_stage` AS `SALES_STAGE`,`aa`.`probability` AS `PROBABILITY`,`aa`.`owner_id` AS `OWNER_ID`,`aa`.`sup_id` AS `SUP_ID`,`bb`.`id_c` AS `ID_C`,`bb`.`aaa_c` AS `AAA_C`,`bb`.`op_compe_c` AS `OP_COMPE_C`,`bb`.`op_compname_memo_c` AS `OP_COMPNAME_MEMO_C`,`bb`.`op_cont_plandate_c` AS `OP_CONT_PLANDATE_C`,`bb`.`op_data_organ_c` AS `OP_DATA_ORGAN_C`,NULL AS `OP_DEP_DETAIL_C`,`bb`.`op_depend_c` AS `OP_DEPEND_C`,`bb`.`op_endsv_cate_c` AS `OP_ENDSV_CATE_C`,`bb`.`op_endsv_class_c` AS `OP_ENDSV_CLASS_C`,`bb`.`op_endsv_num_c` AS `OP_ENDSV_NUM_C`,`bb`.`op_endsv_unit_c` AS `OP_ENDSV_UNIT_C`,`bb`.`op_endsvend_mm_c` AS `OP_ENDSVEND_MM_C`,`bb`.`op_endsvend_yyyy_c` AS `OP_ENDSVEND_YYYY_C`,`bb`.`op_endsvstart_mm_c` AS `OP_ENDSVSTART_MM_C`,`bb`.`op_endsvstart_yyyy_c` AS `OP_ENDSVSTART_YYYY_C`,`bb`.`op_newsv_cate_c` AS `OP_NEWSV_CATE_C`,`bb`.`op_newsv_class_c` AS `OP_NEWSV_CLASS_C`,`bb`.`op_newsv_num_c` AS `OP_NEWSV_NUM_C`,`bb`.`op_newsv_unit_c` AS `OP_NEWSV_UNIT_C`,`bb`.`op_newsvend_mm_c` AS `OP_NEWSVEND_MM_C`,`bb`.`op_newsvend_yyyy_c` AS `OP_NEWSVEND_YYYY_C`,`bb`.`op_newsvstart_mm_c` AS `OP_NEWSVSTART_MM_C`,`bb`.`op_newsvstart_yyyy_c` AS `OP_NEWSVSTART_YYYY_C`,`bb`.`op_onseikeitai_organ_c` AS `OP_ONSEIKEITAI_ORGAN_C`,`bb`.`op_order_poss_c` AS `OP_ORDER_POSS_C`,`bb`.`op_owner_c` AS `OP_OWNER_C`,`bb`.`op_partner_c` AS `OP_PARTNER_C`,`bb`.`op_sales_main_c` AS `OP_SALES_MAIN_C`,`bb`.`op_sales_support_c` AS `OP_SALES_SUPPORT_C`,`bb`.`op_step_c` AS `OP_STEP_C`,`bb`.`op_sts_c` AS `OP_STS_C`,`bb`.`op_sv_cate_c` AS `OP_SV_CATE_C`,`bb`.`op_commit_c` AS `OP_COMMIT_C`,`bb`.`op_forecast_c` AS `OP_FORECAST_C`,`bb`.`opportunity_number_c` AS `OPPORTUNITY_NUMBER_C`,`bb`.`anken_id_c` AS `ANKEN_ID_C`,`bb`.`doc_id_c` AS `DOC_ID_C`,`bb`.`proposal1_c` AS `PROPOSAL1_C`,`bb`.`proposal_price1_c` AS `PROPOSAL_PRICE1_C`,`bb`.`discount_rate1_c` AS `DISCOUNT_RATE1_C`,`bb`.`proposal2_c` AS `PROPOSAL2_C`,`bb`.`proposal_price2_c` AS `PROPOSAL_PRICE2_C`,`bb`.`discount_rate2_c` AS `DISCOUNT_RATE2_C`,`bb`.`closed_situation_c` AS `CLOSED_SITUATION_C`,`bb`.`victory_defeat_cause_c` AS `VICTORY_DEFEAT_CAUSE_C`,NULL AS `VICTORY_DEFEAT_CAUSE_DETAIL_C`,`bb`.`emphasis_strategy1_c` AS `EMPHASIS_STRATEGY1_C`,`bb`.`emphasis_strategy2_c` AS `EMPHASIS_STRATEGY2_C`,`bb`.`measure_dept_c` AS `MEASURE_DEPT_C`,`bb`.`measure_company_c` AS `MEASURE_COMPANY_C`,`bb`.`y_interest_c` AS `Y_INTEREST_C`,`bb`.`y_interest_area_c` AS `Y_INTEREST_AREA_C`,`bb`.`y_ng_reason_c` AS `Y_NG_REASON_C`,`bb`.`y_agency_c` AS `Y_AGENCY_C`,`bb`.`y_medium_c` AS `Y_MEDIUM_C`,NULL AS `Y_REMARKS_C`,`bb`.`y_posturl_c` AS `Y_POSTURL_C`,NULL AS `RESERVE1_C`,NULL AS `RESERVE2_C`,NULL AS `RESERVE3_C`,NULL AS `RESERVE4_C`,NULL AS `RESERVE5_C`,NULL AS `RESERVE6_C`,NULL AS `RESERVE7_C`,NULL AS `RESERVE8_C`,NULL AS `RESERVE9_C`,NULL AS `RESERVE10_C` from (`opportunities` `aa` left join `opportunities_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_opportunities_users`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_opportunities_users`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_opportunities_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_opportunities_users` AS select `opsop`.`opp_saless78cealessup_idb` AS `ID`,`ops`.`assigned_user_id` AS `USER_ID`,`opsop`.`opp_saless3af5unities_ida` AS `OPPORTUNITY_ID`,addtime(`ops`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,if((`opsop`.`deleted` = 1),1,if((`ops`.`deleted` = 1),1,if((`opsop`.`deleted` = 0),`opsop`.`deleted`,`ops`.`deleted`))) AS `DELETED` from ((`opp_salessupportunities_c` `opsop` join `opp_salessup` `ops` on(((`opsop`.`opp_saless78cealessup_idb` = `ops`.`id`) and (`ops`.`deleted` = '0')))) join `opportunities` `opp` on(((`opsop`.`opp_saless3af5unities_ida` = `opp`.`id`) and (`opp`.`deleted` = '0')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_sv_cancel`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_sv_cancel`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_cancel`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_sv_cancel` AS select `sv_cancel`.`id` AS `ID`,`sv_cancel`.`name` AS `NAME`,addtime(`sv_cancel`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`sv_cancel`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`sv_cancel`.`modified_user_id` AS `MODIFIED_USER_ID`,`sv_cancel`.`created_by` AS `CREATED_BY`,`sv_cancel`.`description` AS `DESCRIPTION`,`sv_cancel`.`deleted` AS `DELETED`,`sv_cancel`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`sv_cancel`.`class` AS `CLASS`,`sv_cancel`.`cate` AS `CATE`,`sv_cancel`.`month_num` AS `MONTH_NUM`,`sv_cancel`.`temp_num` AS `TEMP_NUM`,`sv_cancel`.`line_num` AS `LINE_NUM`,`sv_cancel`.`yyyy` AS `YYYY`,`sv_cancel`.`mm` AS `MM`,`sv_cancel`.`billiard_form` AS `BILLIARD_FORM`,`sv_cancel`.`billiard_model_code` AS `BILLIARD_MODEL_CODE`,`sv_cancel`.`billiard_maker` AS `BILLIARD_MAKER`,`sv_cancel`.`billiard_model` AS `BILLIARD_MODEL`,`sv_cancel`.`billiard_color` AS `BILLIARD_COLOR`,`sv_cancel`.`month_num_variable_profits` AS `MONTH_NUM_VARIABLE_PROFITS`,`sv_cancel`.`temp_num_variable_profits` AS `TEMP_NUM_VARIABLE_PROFITS`,`sv_cancel`.`posted_month_age` AS `POSTED_MONTH_AGE`,`sv_cancel`.`billiard_choice_day` AS `BILLIARD_CHOICE_DAY`,`sv_cancel`.`billiard_memo` AS `BILLIARD_MEMO`,`sv_cancel`.`month_num_local_currency` AS `MONTH_NUM_LOCAL_CURRENCY`,`sv_cancel`.`month_num_unit` AS `MONTH_NUM_UNIT`,`sv_cancel`.`temp_num_local_currency` AS `TEMP_NUM_LOCAL_CURRENCY`,`sv_cancel`.`temp_num_unit` AS `TEMP_NUM_UNIT`,`sv_cancel`.`month_age` AS `MONTH_AGE` from `sv_cancel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_sv_cancel_opportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_sv_cancel_opportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_cancel_opportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_sv_cancel_opportunities` AS select `sv_cancel_opportunities`.`id` AS `ID`,addtime(`sv_cancel_opportunities`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`sv_cancel_opportunities`.`deleted` AS `DELETED`,`sv_cancel_opportunities`.`sv_cancel_ida` AS `SV_CANCEL_IDA`,`sv_cancel_opportunities`.`opportunities_idb` AS `OPPORTUNITIES_IDB` from `sv_cancel_opportunities` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_sv_suggest`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_sv_suggest`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_suggest`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_sv_suggest` AS select `sv_suggest`.`id` AS `ID`,`sv_suggest`.`name` AS `NAME`,addtime(`sv_suggest`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`sv_suggest`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`sv_suggest`.`modified_user_id` AS `MODIFIED_USER_ID`,`sv_suggest`.`created_by` AS `CREATED_BY`,`sv_suggest`.`description` AS `DESCRIPTION`,`sv_suggest`.`deleted` AS `DELETED`,`sv_suggest`.`assigned_user_id` AS `ASSIGNED_USER_ID`,`sv_suggest`.`class` AS `CLASS`,`sv_suggest`.`cate` AS `CATE`,`sv_suggest`.`month_num` AS `MONTH_NUM`,`sv_suggest`.`temp_num` AS `TEMP_NUM`,`sv_suggest`.`line_num` AS `LINE_NUM`,`sv_suggest`.`yyyy` AS `YYYY`,`sv_suggest`.`mm` AS `MM`,`sv_suggest`.`billiard_form` AS `BILLIARD_FORM`,`sv_suggest`.`billiard_model_code` AS `BILLIARD_MODEL_CODE`,`sv_suggest`.`billiard_maker` AS `BILLIARD_MAKER`,`sv_suggest`.`billiard_model` AS `BILLIARD_MODEL`,`sv_suggest`.`billiard_color` AS `BILLIARD_COLOR`,`sv_suggest`.`month_num_variable_profits` AS `MONTH_NUM_VARIABLE_PROFITS`,`sv_suggest`.`temp_num_variable_profits` AS `TEMP_NUM_VARIABLE_PROFITS`,`sv_suggest`.`posted_month_age` AS `POSTED_MONTH_AGE`,`sv_suggest`.`billiard_choice_day` AS `BILLIARD_CHOICE_DAY`,`sv_suggest`.`billiard_memo` AS `BILLIARD_MEMO`,`sv_suggest`.`month_num_local_currency` AS `MONTH_NUM_LOCAL_CURRENCY`,`sv_suggest`.`month_num_unit` AS `MONTH_NUM_UNIT`,`sv_suggest`.`temp_num_local_currency` AS `TEMP_NUM_LOCAL_CURRENCY`,`sv_suggest`.`temp_num_unit` AS `TEMP_NUM_UNIT`,`sv_suggest`.`month_age` AS `MONTH_AGE` from `sv_suggest` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_sv_suggest_opportunities`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_sv_suggest_opportunities`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_sv_suggest_opportunities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_sv_suggest_opportunities` AS select `sv_suggest_opportunities`.`id` AS `ID`,addtime(`sv_suggest_opportunities`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`sv_suggest_opportunities`.`deleted` AS `DELETED`,`sv_suggest_opportunities`.`sv_suggest_ida` AS `SV_SUGGEST_IDA`,`sv_suggest_opportunities`.`opportunities_idb` AS `OPPORTUNITIES_IDB` from `sv_suggest_opportunities` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ifview_eida_users`
--

/*!50001 DROP TABLE IF EXISTS `ifview_eida_users`*/;
/*!50001 DROP VIEW IF EXISTS `ifview_eida_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ifview_eida_users` AS select `aa`.`id` AS `ID`,`aa`.`user_name` AS `USER_NAME`,`aa`.`user_hash` AS `USER_HASH`,`aa`.`authenticate_id` AS `AUTHENTICATE_ID`,`aa`.`sugar_login` AS `SUGAR_LOGIN`,`aa`.`first_name` AS `FIRST_NAME`,`aa`.`last_name` AS `LAST_NAME`,`aa`.`reports_to_id` AS `REPORTS_TO_ID`,`aa`.`is_admin` AS `IS_ADMIN`,`aa`.`receive_notifications` AS `RECEIVE_NOTIFICATIONS`,`aa`.`description` AS `DESCRIPTION`,addtime(`aa`.`date_entered`,'09:00:00') AS `DATE_ENTERED`,addtime(`aa`.`date_modified`,'09:00:00') AS `DATE_MODIFIED`,`aa`.`modified_user_id` AS `MODIFIED_USER_ID`,`aa`.`created_by` AS `CREATED_BY`,`aa`.`title` AS `TITLE`,`aa`.`department` AS `DEPARTMENT`,`aa`.`phone_home` AS `PHONE_HOME`,`aa`.`phone_mobile` AS `PHONE_MOBILE`,`aa`.`phone_work` AS `PHONE_WORK`,`aa`.`phone_other` AS `PHONE_OTHER`,`aa`.`phone_fax` AS `PHONE_FAX`,`aa`.`status` AS `STATUS`,`aa`.`address_street` AS `ADDRESS_STREET`,`aa`.`address_city` AS `ADDRESS_CITY`,`aa`.`address_state` AS `ADDRESS_STATE`,`aa`.`address_country` AS `ADDRESS_COUNTRY`,`aa`.`address_postalcode` AS `ADDRESS_POSTALCODE`,`aa`.`user_preferences` AS `USER_PREFERENCES`,`aa`.`deleted` AS `DELETED`,`aa`.`portal_only` AS `PORTAL_ONLY`,`aa`.`employee_status` AS `EMPLOYEE_STATUS`,`aa`.`messenger_id` AS `MESSENGER_ID`,`aa`.`messenger_type` AS `MESSENGER_TYPE`,`aa`.`is_group` AS `IS_GROUP`,`aa`.`spm_stfcd` AS `SPM_STFCD`,`aa`.`spm_orgunit_cd` AS `SPM_ORGUNIT_CD`,`aa`.`spm_orgunit_name` AS `SPM_ORGUNIT_NAME`,`aa`.`orgunit_cd` AS `ORGUNIT_CD`,`aa`.`orgunit_name` AS `ORGUNIT_NAME`,`bb`.`google_mmail_c` AS `GOOGLE_MMAIL_C`,`bb`.`google_mcalls_c` AS `GOOGLE_MCALLS_C` from (`users` `aa` left join `users_cstm` `bb` on((`aa`.`id` = `bb`.`id_c`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `navagis_calls_listviewer`
--

/*!50001 DROP TABLE IF EXISTS `navagis_calls_listviewer`*/;
/*!50001 DROP VIEW IF EXISTS `navagis_calls_listviewer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `navagis_calls_listviewer` AS select `cal`.`id` AS `id`,ifnull(replace(`cal`.`name`,'	',' '),'') AS `name`,`cal`.`assigned_user_id` AS `assigned_user_id`,date_format((`cal`.`date_start` + interval 9 hour),'%Y-%m-%d %H:%i') AS `date_start`,date_format(`ccm`.`time_end_c`,'%H:%i') AS `time_end_c`,`cal`.`status` AS `status`,`cal`.`parent_type` AS `parent_type`,(case `cal`.`parent_type` when 'Accounts' then `acc`.`id` when 'Opportunities' then `oppacc`.`id` else '' end) AS `account_id`,(case `cal`.`parent_type` when 'Accounts' then replace(`acc`.`name`,'	',' ') when 'Opportunities' then replace(`oppacc`.`name`,'	',' ') else '' end) AS `account_name`,(case `cal`.`parent_type` when 'Opportunities' then `opc`.`anken_id_c` else '' end) AS `anken_id_c`,(case `cal`.`parent_type` when 'Opportunities' then replace(`opp`.`name`,'	',' ') else '' end) AS `opportunity_name`,`ccm`.`division_c` AS `division_c`,ifnull(`ccm`.`companion_c`,'') AS `companion_c`,ifnull(`ccm`.`contact_title_c`,'') AS `contact_title_c`,ifnull(`ccm`.`contact_department_c`,'') AS `contact_department_c`,ifnull(`cal`.`outlook_id`,'') AS `outlook_id`,ifnull(`ccm`.`account_id_c`,'') AS `account_id_c`,ifnull(`ea_usr`.`email_address`,'') AS `email_address`,ifnull(`es1`.`div1_cd`,'') AS `div1_cd`,ifnull(`es1`.`div2_cd`,'') AS `div2_cd`,ifnull(`es1`.`div3_cd`,'') AS `div3_cd`,ifnull(`es1`.`div4_cd`,'') AS `div4_cd`,ifnull(`es1`.`div1_nm`,'') AS `div1_nm`,ifnull(`es1`.`div2_nm`,'') AS `div2_nm`,ifnull(`es1`.`div3_nm`,'') AS `div3_nm`,ifnull(`es1`.`div4_nm`,'') AS `div4_nm`,date_format((`cal`.`date_modified` + interval 9 hour),'%Y-%m-%d %H:%i:%s') AS `date_modified`,`cal`.`modified_user_id` AS `modified_user_id`,ifnull(`ea_mod`.`email_address`,'') AS `modified_user_mail`,date_format((`cal`.`date_entered` + interval 9 hour),'%Y-%m-%d %H:%i:%s') AS `date_entered`,`cal`.`created_by` AS `created_by`,ifnull(`ea_cre`.`email_address`,'') AS `created_by_mail`,`cal`.`deleted` AS `deleted` from ((((((((((((`calls` `cal` join `calls_cstm` `ccm` on((`cal`.`id` = `ccm`.`id_c`))) left join `accounts` `acc` on((`cal`.`parent_id` = `acc`.`id`))) left join `opportunities` `opp` on((`cal`.`parent_id` = `opp`.`id`))) left join `opportunities_cstm` `opc` on((`opp`.`id` = `opc`.`id_c`))) left join `accounts_opportunities` `acop` on((`opp`.`id` = `acop`.`opportunity_id`))) left join `accounts` `oppacc` on((`acop`.`account_id` = `oppacc`.`id`))) left join `users` `usr1` on((`cal`.`assigned_user_id` = `usr1`.`id`))) left join `email_addresses` `ea_usr` on((`usr1`.`id` = `ea_usr`.`id`))) left join `mst_tanto_s_sosiki` `ts1` on(((`usr1`.`id` = `ts1`.`tanto_cd`) and (`ts1`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es1` on(((`ts1`.`eigyo_sosiki_cd` = `es1`.`eigyo_sosiki_cd`) and (`es1`.`del_flg` = '0')))) left join `email_addresses` `ea_mod` on((`cal`.`modified_user_id` = `ea_mod`.`id`))) left join `email_addresses` `ea_cre` on((`cal`.`created_by` = `ea_cre`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `navagis_opportunities_listviewer`
--

/*!50001 DROP TABLE IF EXISTS `navagis_opportunities_listviewer`*/;
/*!50001 DROP VIEW IF EXISTS `navagis_opportunities_listviewer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `navagis_opportunities_listviewer` AS select `opp`.`id` AS `id`,`opc`.`anken_id_c` AS `anken_id_c`,replace(`opp`.`name`,'	',' ') AS `name`,`opp`.`assigned_user_id` AS `assigned_user_id`,`oppacc`.`id` AS `account_id`,replace(`oppacc`.`name`,'	',' ') AS `account_name`,date_format(`opc`.`op_cont_plandate_c`,'%Y-%m-%d') AS `op_cont_plandate_c`,`opc`.`op_order_poss_c` AS `op_order_poss_c`,`opc`.`op_step_c` AS `op_step_c`,`opc`.`op_commit_c` AS `op_commit_c`,ifnull(`ea_usr`.`email_address`,'') AS `email_address`,ifnull(`es1`.`div1_cd`,'') AS `div1_cd`,ifnull(`es1`.`div2_cd`,'') AS `div2_cd`,ifnull(`es1`.`div3_cd`,'') AS `div3_cd`,ifnull(`es1`.`div4_cd`,'') AS `div4_cd`,ifnull(`es1`.`div1_nm`,'') AS `div1_nm`,ifnull(`es1`.`div2_nm`,'') AS `div2_nm`,ifnull(`es1`.`div3_nm`,'') AS `div3_nm`,ifnull(`es1`.`div4_nm`,'') AS `div4_nm`,date_format((`opp`.`date_modified` + interval 9 hour),'%Y-%m-%d %H:%i:%s') AS `date_modified`,`opp`.`modified_user_id` AS `modified_user_id`,ifnull(`ea_mod`.`email_address`,'') AS `modified_user_mail`,date_format((`opp`.`date_entered` + interval 9 hour),'%Y-%m-%d %H:%i:%s') AS `date_entered`,`opp`.`created_by` AS `created_by`,ifnull(`ea_cre`.`email_address`,'') AS `created_by_mail`,`opp`.`deleted` AS `deleted` from (((((((((`opportunities` `opp` join `opportunities_cstm` `opc` on((`opp`.`id` = `opc`.`id_c`))) left join `accounts_opportunities` `acop` on((`opp`.`id` = `acop`.`opportunity_id`))) left join `accounts` `oppacc` on((`acop`.`account_id` = `oppacc`.`id`))) left join `users` `usr1` on((`opp`.`assigned_user_id` = `usr1`.`id`))) left join `email_addresses` `ea_usr` on((`usr1`.`id` = `ea_usr`.`id`))) left join `mst_tanto_s_sosiki` `ts1` on(((`usr1`.`id` = `ts1`.`tanto_cd`) and (`ts1`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es1` on(((`ts1`.`eigyo_sosiki_cd` = `es1`.`eigyo_sosiki_cd`) and (`es1`.`del_flg` = '0')))) left join `email_addresses` `ea_mod` on((`opp`.`modified_user_id` = `ea_mod`.`id`))) left join `email_addresses` `ea_cre` on((`opp`.`created_by` = `ea_cre`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vd_calls_listviewer`
--

/*!50001 DROP TABLE IF EXISTS `vd_calls_listviewer`*/;
/*!50001 DROP VIEW IF EXISTS `vd_calls_listviewer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vd_calls_listviewer` AS select `cal`.`id` AS `id`,`cal`.`name` AS `name`,`cal`.`date_entered` AS `date_entered`,`cal`.`date_modified` AS `date_modified`,`cal`.`modified_user_id` AS `modified_user_id`,`cal`.`created_by` AS `created_by`,`cal`.`description` AS `description`,`cal`.`deleted` AS `deleted`,`cal`.`assigned_user_id` AS `assigned_user_id`,`es1`.`div1_cd` AS `mast_organizationunit_id`,`es1`.`div2_cd` AS `mast_organizationunit2_id`,`es1`.`div3_cd` AS `mast_organizationunit3_id`,`es1`.`div4_cd` AS `mast_organizationunit4_id`,`usr1`.`user_name` AS `sales_staff_code`,concat(`usr1`.`last_name`,' ',`usr1`.`first_name`) AS `sales_staff_name`,`es1`.`eigyo_sosiki_disp_cd` AS `sales_group_code`,`es1`.`eigyo_sosiki_nm` AS `sales_group_name`,date_format(addtime(`cal`.`date_start`,'09:00:00'),'%Y-%m-%d') AS `date_start_fmt`,date_format(addtime(`cal`.`date_start`,'09:00:00'),'%Y-%m-%d %H:%i') AS `date_start_fmt2`,`cal`.`name` AS `call_name`,(case `cal`.`parent_type` when 'Accounts' then '企業' when 'Opportunities' then '案件' else NULL end) AS `parent_type_desc`,(case `cal`.`parent_type` when 'Accounts' then `acc2`.`id` when 'Opportunities' then `acc`.`id` else NULL end) AS `account_id`,(case `cal`.`parent_type` when 'Accounts' then `acc2`.`name` when 'Opportunities' then `acc`.`name` else NULL end) AS `account_name`,`opp`.`id` AS `opportunity_id`,(case `cal`.`parent_type` when 'Opportunities' then `opc`.`anken_id_c` else NULL end) AS `anken_id_c`,(case `cal`.`parent_type` when 'Opportunities' then `opp`.`name` else NULL end) AS `opportunity_name`,`cal`.`status` AS `status`,`ccm`.`com_free_c` AS `com_free_c`,left(`ccm`.`com_free_c`,20) AS `com_free_c20`,date_format(addtime(`cal`.`date_entered`,'09:00:00'),'%Y-%m-%d') AS `date_entered_fmt`,date_format(addtime(`cal`.`date_entered`,'09:00:00'),'%Y-%m-%d %H:%i') AS `date_entered_fmt2`,`usr3`.`user_name` AS `enter_staff_code`,concat(`usr3`.`last_name`,' ',`usr3`.`first_name`) AS `enter_staff_name`,date_format(addtime(`cal`.`date_modified`,'09:00:00'),'%Y-%m-%d') AS `date_modified_fmt`,date_format(addtime(`cal`.`date_modified`,'09:00:00'),'%Y-%m-%d %H:%i') AS `date_modified_fmt2`,`usr2`.`user_name` AS `update_staff_code`,concat(`usr2`.`last_name`,' ',`usr2`.`first_name`) AS `update_staff_name`,`es2`.`eigyo_sosiki_disp_cd` AS `update_group_code`,`es2`.`eigyo_sosiki_nm` AS `update_group_name`,`ccm`.`pam_name_c` AS `pam_name_c`,`ccm`.`pam_sv_num_c` AS `pam_sv_num_c`,`ccm`.`companion_c` AS `companion_c`,date_format(`ccm`.`time_end_c`,'%H:%i') AS `time_end_c`,`ccm`.`division_c` AS `division_c`,`ccm`.`contact_id_c` AS `contact_id_c`,`contacts`.`first_name` AS `contact_first_name`,`contacts`.`last_name` AS `contact_last_name`,concat(`contacts`.`last_name`,`contacts`.`first_name`) AS `contact_name`,`ccm`.`contact_title_c` AS `contact_title_c`,`ccm`.`contact_department_c` AS `contact_department_c`,`cal`.`outlook_id` AS `outlook_id` from ((((((((((((((`calls` `cal` join `calls_cstm` `ccm` on((`cal`.`id` = `ccm`.`id_c`))) left join `users` `usr1` on((`cal`.`assigned_user_id` = `usr1`.`id`))) left join `opportunities` `opp` on((`cal`.`parent_id` = `opp`.`id`))) left join `opportunities_cstm` `opc` on(((`opp`.`id` = `opc`.`id_c`) and (`opp`.`deleted` = 0)))) left join `accounts_opportunities` `aco` on(((`opp`.`id` = `aco`.`opportunity_id`) and (`aco`.`deleted` = 0)))) left join `accounts` `acc` on((`aco`.`account_id` = `acc`.`id`))) left join `accounts` `acc2` on((`cal`.`parent_id` = `acc2`.`id`))) left join `users` `usr2` on((`cal`.`modified_user_id` = `usr2`.`id`))) left join `users` `usr3` on((`cal`.`created_by` = `usr3`.`id`))) left join `mst_tanto_s_sosiki` `ts1` on(((`usr1`.`id` = `ts1`.`sbtm_tanto_cd`) and (`ts1`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es1` on(((`ts1`.`eigyo_sosiki_cd` = `es1`.`eigyo_sosiki_cd`) and (`es1`.`del_flg` = '0')))) left join `mst_tanto_s_sosiki` `ts2` on(((`usr2`.`id` = `ts2`.`sbtm_tanto_cd`) and (`ts2`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es2` on(((`ts2`.`eigyo_sosiki_cd` = `es2`.`eigyo_sosiki_cd`) and (`es2`.`del_flg` = '0')))) left join `contacts` on((`ccm`.`contact_id_c` = `contacts`.`id`))) where ((`cal`.`deleted` = 0) and (date_format(addtime(`cal`.`date_start`,'09:00:00'),'%Y-%m-%d') >= date_format((sysdate() - interval 1 year),'%Y-%m-01'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vd_calls_listviewer_del`
--

/*!50001 DROP TABLE IF EXISTS `vd_calls_listviewer_del`*/;
/*!50001 DROP VIEW IF EXISTS `vd_calls_listviewer_del`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vd_calls_listviewer_del` AS select `cal`.`id` AS `id`,`cal`.`name` AS `name`,`cal`.`date_entered` AS `date_entered`,`cal`.`date_modified` AS `date_modified`,`cal`.`modified_user_id` AS `modified_user_id`,`cal`.`created_by` AS `created_by`,`cal`.`description` AS `description`,`cal`.`deleted` AS `deleted`,`cal`.`assigned_user_id` AS `assigned_user_id`,`es1`.`div1_cd` AS `mast_organizationunit_id`,`es1`.`div2_cd` AS `mast_organizationunit2_id`,`es1`.`div3_cd` AS `mast_organizationunit3_id`,`es1`.`div4_cd` AS `mast_organizationunit4_id`,`usr1`.`user_name` AS `sales_staff_code`,concat(`usr1`.`last_name`,' ',`usr1`.`first_name`) AS `sales_staff_name`,`es1`.`eigyo_sosiki_disp_cd` AS `sales_group_code`,`es1`.`eigyo_sosiki_nm` AS `sales_group_name`,date_format((`cal`.`date_start` + interval 9 hour),'%Y-%m-%d') AS `date_start_fmt`,date_format((`cal`.`date_start` + interval 9 hour),'%Y-%m-%d %H:%i') AS `date_start_fmt2`,`cal`.`name` AS `call_name`,(case `cal`.`parent_type` when 'Accounts' then '企業' when 'Opportunities' then '案件' else NULL end) AS `parent_type_desc`,(case `cal`.`parent_type` when 'Accounts' then `acc2`.`id` when 'Opportunities' then `acc`.`id` else NULL end) AS `account_id`,(case `cal`.`parent_type` when 'Accounts' then `acc2`.`name` when 'Opportunities' then `acc`.`name` else NULL end) AS `account_name`,`opp`.`id` AS `opportunity_id`,(case `cal`.`parent_type` when 'Opportunities' then `opc`.`anken_id_c` else NULL end) AS `anken_id_c`,(case `cal`.`parent_type` when 'Opportunities' then `opp`.`name` else NULL end) AS `opportunity_name`,`cal`.`status` AS `status`,substr(`ccm`.`com_free_c`,1,256) AS `com_free_c`,left(`ccm`.`com_free_c`,20) AS `com_free_c20`,date_format((`cal`.`date_entered` + interval 9 hour),'%Y-%m-%d') AS `date_entered_fmt`,date_format((`cal`.`date_entered` + interval 9 hour),'%Y-%m-%d %H:%i') AS `date_entered_fmt2`,`usr3`.`user_name` AS `enter_staff_code`,concat(`usr3`.`last_name`,' ',`usr3`.`first_name`) AS `enter_staff_name`,date_format((`cal`.`date_modified` + interval 9 hour),'%Y-%m-%d') AS `date_modified_fmt`,date_format((`cal`.`date_modified` + interval 9 hour),'%Y-%m-%d %H:%i') AS `date_modified_fmt2`,`usr2`.`user_name` AS `update_staff_code`,concat(`usr2`.`last_name`,' ',`usr2`.`first_name`) AS `update_staff_name`,`es2`.`eigyo_sosiki_disp_cd` AS `update_group_code`,`es2`.`eigyo_sosiki_nm` AS `update_group_name`,`ccm`.`pam_name_c` AS `pam_name_c`,`ccm`.`pam_sv_num_c` AS `pam_sv_num_c`,`ccm`.`companion_c` AS `companion_c`,date_format(`ccm`.`time_end_c`,'%H:%i') AS `time_end_c`,`ccm`.`division_c` AS `division_c`,`ccm`.`contact_id_c` AS `contact_id_c`,`contacts`.`first_name` AS `contact_first_name`,`contacts`.`last_name` AS `contact_last_name`,concat(`contacts`.`last_name`,`contacts`.`first_name`) AS `contact_name`,`ccm`.`contact_title_c` AS `contact_title_c`,`ccm`.`contact_department_c` AS `contact_department_c`,`cal`.`outlook_id` AS `outlook_id` from ((((((((((((((`calls` `cal` join `calls_cstm` `ccm` on((`cal`.`id` = `ccm`.`id_c`))) left join `users` `usr1` on((`cal`.`assigned_user_id` = `usr1`.`id`))) left join `opportunities` `opp` on((`cal`.`parent_id` = `opp`.`id`))) left join `opportunities_cstm` `opc` on(((`opp`.`id` = `opc`.`id_c`) and (`opp`.`deleted` = 0)))) left join `accounts_opportunities` `aco` on(((`opp`.`id` = `aco`.`opportunity_id`) and (`aco`.`deleted` = 0)))) left join `accounts` `acc` on((`aco`.`account_id` = `acc`.`id`))) left join `accounts` `acc2` on((`cal`.`parent_id` = `acc2`.`id`))) left join `users` `usr2` on((`cal`.`modified_user_id` = `usr2`.`id`))) left join `users` `usr3` on((`cal`.`created_by` = `usr3`.`id`))) left join `mst_tanto_s_sosiki` `ts1` on(((`usr1`.`id` = `ts1`.`sbtm_tanto_cd`) and (`ts1`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es1` on(((`ts1`.`eigyo_sosiki_cd` = `es1`.`eigyo_sosiki_cd`) and (`es1`.`del_flg` = '0')))) left join `mst_tanto_s_sosiki` `ts2` on(((`usr2`.`id` = `ts2`.`sbtm_tanto_cd`) and (`ts2`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es2` on(((`ts2`.`eigyo_sosiki_cd` = `es2`.`eigyo_sosiki_cd`) and (`es2`.`del_flg` = '0')))) left join `contacts` on((`ccm`.`contact_id_c` = `contacts`.`id`))) where (date_format((`cal`.`date_start` + interval 9 hour),'%Y-%m-%d') >= date_format((now() - interval 1 year),'%Y-%m-01')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp2_fy10`
--

/*!50001 DROP TABLE IF EXISTS `view_opp2_fy10`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp2_fy10` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`opc`.`DELETED` = 0),`csv`.`DELETED`,`opc`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`csv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`csv`.`YYYY`,'/',`csv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`csv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`csv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) when '6' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.3) / 12) / 4000),2) when '7' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.35) / 12) / 4000),2) when '8' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.125) / 12) / 4000),2) when '9' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.2) / 12) / 4000),2) when '10' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.4) / 12) / 4000),2) when '11' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 1.0) / 12) / 4000),2) when '12' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.2) / 4000),2) when '13' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 12) / 4000),2) when '14' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 4000),2) else 0 end) AS `SV_NUM`,`csv`.`MONTH_NUM` AS `MONTH_NUM`,`csv`.`TEMP_NUM` AS `TEMP_NUM`,`csv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'解約' AS `SV_TYPE`,`csv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,left(`opp`.`OP_DEPEND_C`,40) AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`csv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`csv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`csv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`csv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`csv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`csv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`csv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`csv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO`,date_format(`csv`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `SV_DATE_MODIFIED_FMT`,`es`.`div1_nm` AS `MAST_ORGANIZATIONUNIT1_NM`,`es`.`div2_nm` AS `MAST_ORGANIZATIONUNIT2_NM`,`es`.`div3_nm` AS `MAST_ORGANIZATIONUNIT3_NM`,`es`.`div4_nm` AS `MAST_ORGANIZATIONUNIT4_NM`,`es`.`div5_nm` AS `MAST_ORGANIZATIONUNIT5_NM`,`es`.`div6_nm` AS `MAST_ORGANIZATIONUNIT6_NM`,left(`opp`.`OP_CONT_PLANDATE_C`,7) AS `OP_CONT_PLANMONTH`,`csv`.`MONTH_NUM_LOCAL_CURRENCY` AS `MONTH_NUM_LOCAL_CURRENCY`,`csv`.`MONTH_NUM_UNIT` AS `MONTH_NUM_UNIT`,`csv`.`TEMP_NUM_LOCAL_CURRENCY` AS `TEMP_NUM_LOCAL_CURRENCY`,`csv`.`TEMP_NUM_UNIT` AS `TEMP_NUM_UNIT`,(case when (`opp`.`OP_STEP_C` = '50') then '契約・申込' when (`opp`.`OP_STEP_C` = '60') then '失注' when (`opp`.`OP_STEP_C` = '70') then '案件消滅' when (`opp`.`OP_COMMIT_C` = '001') then 'ベース' when (`opp`.`OP_COMMIT_C` = '002') then 'アグレッシブ' when (`opp`.`OP_COMMIT_C` = '003') then 'チャレンジ' else '' end) AS `OP_MANAGE_FORECAST`,(case `csv`.`CATE` when '1106' then `csv`.`LINE_NUM` when '2464' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI`,(case `csv`.`CATE` when '1107' then `csv`.`LINE_NUM` when '2463' then `csv`.`LINE_NUM` when '1108' then `csv`.`LINE_NUM` when '2462' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_BRI`,(case `csv`.`CATE` when '1106' then `csv`.`LINE_NUM` when '2464' then `csv`.`LINE_NUM` when '1107' then `csv`.`LINE_NUM` when '2463' then `csv`.`LINE_NUM` when '1108' then `csv`.`LINE_NUM` when '2462' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI1`,(case `csv`.`CATE` when '1106' then (`csv`.`LINE_NUM` * 20) when '2464' then (`csv`.`LINE_NUM` * 30) when '1107' then `csv`.`LINE_NUM` when '2463' then (`csv`.`LINE_NUM` * 1.5) when '1108' then `csv`.`LINE_NUM` when '2462' then (`csv`.`LINE_NUM` * 1.5) else 0 end) AS `SPECIAL_PRI20`,(case `csv`.`CATE` when '2444' then `csv`.`LINE_NUM` when '2263' then `csv`.`LINE_NUM` when '2200' then `csv`.`LINE_NUM` when '2201' then `csv`.`LINE_NUM` when '2202' then `csv`.`LINE_NUM` when '2445' then `csv`.`LINE_NUM` when '2203' then `csv`.`LINE_NUM` when '2204' then `csv`.`LINE_NUM` when '2205' then `csv`.`LINE_NUM` when '2206' then `csv`.`LINE_NUM` when '2446' then `csv`.`LINE_NUM` when '2207' then `csv`.`LINE_NUM` when '2208' then `csv`.`LINE_NUM` when '2209' then `csv`.`LINE_NUM` when '2210' then `csv`.`LINE_NUM` when '2211' then `csv`.`LINE_NUM` when '2212' then `csv`.`LINE_NUM` when '2213' then `csv`.`LINE_NUM` when '2214' then `csv`.`LINE_NUM` when '2264' then `csv`.`LINE_NUM` when '2215' then `csv`.`LINE_NUM` when '2447' then `csv`.`LINE_NUM` when '2217' then `csv`.`LINE_NUM` when '2218' then `csv`.`LINE_NUM` when '2219' then `csv`.`LINE_NUM` when '2220' then `csv`.`LINE_NUM` when '2221' then `csv`.`LINE_NUM` when '2222' then `csv`.`LINE_NUM` when '2223' then `csv`.`LINE_NUM` when '2224' then `csv`.`LINE_NUM` when '2225' then `csv`.`LINE_NUM` when '2448' then `csv`.`LINE_NUM` when '2226' then `csv`.`LINE_NUM` when '2227' then `csv`.`LINE_NUM` when '2228' then `csv`.`LINE_NUM` when '2229' then `csv`.`LINE_NUM` when '2230' then `csv`.`LINE_NUM` when '2231' then `csv`.`LINE_NUM` when '2232' then `csv`.`LINE_NUM` when '2233' then `csv`.`LINE_NUM` when '2234' then `csv`.`LINE_NUM` when '2262' then `csv`.`LINE_NUM` when '2381' then `csv`.`LINE_NUM` when '2382' then `csv`.`LINE_NUM` when '2258' then `csv`.`LINE_NUM` when '2320' then `csv`.`LINE_NUM` when '2305' then `csv`.`LINE_NUM` when '2395' then `csv`.`LINE_NUM` when '2396' then `csv`.`LINE_NUM` when '2397' then `csv`.`LINE_NUM` when '2468' then `csv`.`LINE_NUM` when '2512' then `csv`.`LINE_NUM` when '2513' then `csv`.`LINE_NUM` when '2514' then `csv`.`LINE_NUM` when '2515' then `csv`.`LINE_NUM` when '2516' then `csv`.`LINE_NUM` when '2517' then `csv`.`LINE_NUM` else 0 end) AS `DATA_PHYSICS`,(case `csv`.`CATE` when '2444' then (`csv`.`LINE_NUM` * 130) when '2263' then (`csv`.`LINE_NUM` * 70) when '2200' then (`csv`.`LINE_NUM` * 40) when '2201' then (`csv`.`LINE_NUM` * 20) when '2202' then (`csv`.`LINE_NUM` * 10) when '2445' then (`csv`.`LINE_NUM` * 200) when '2203' then (`csv`.`LINE_NUM` * 150) when '2204' then (`csv`.`LINE_NUM` * 50) when '2205' then (`csv`.`LINE_NUM` * 20) when '2206' then (`csv`.`LINE_NUM` * 10) when '2446' then (`csv`.`LINE_NUM` * 200) when '2207' then (`csv`.`LINE_NUM` * 150) when '2208' then (`csv`.`LINE_NUM` * 70) when '2209' then (`csv`.`LINE_NUM` * 60) when '2210' then (`csv`.`LINE_NUM` * 20) when '2211' then (`csv`.`LINE_NUM` * 10) when '2212' then (`csv`.`LINE_NUM` * 20) when '2213' then (`csv`.`LINE_NUM` * 200) when '2214' then (`csv`.`LINE_NUM` * 30) when '2264' then (`csv`.`LINE_NUM` * 20) when '2215' then (`csv`.`LINE_NUM` * 10) when '2447' then (`csv`.`LINE_NUM` * 200) when '2217' then (`csv`.`LINE_NUM` * 150) when '2218' then (`csv`.`LINE_NUM` * 80) when '2219' then (`csv`.`LINE_NUM` * 10) when '2220' then (`csv`.`LINE_NUM` * 5) when '2221' then (`csv`.`LINE_NUM` * 5) when '2222' then (`csv`.`LINE_NUM` * 100) when '2223' then (`csv`.`LINE_NUM` * 10) when '2224' then (`csv`.`LINE_NUM` * 5) when '2225' then (`csv`.`LINE_NUM` * 5) when '2448' then (`csv`.`LINE_NUM` * 200) when '2226' then (`csv`.`LINE_NUM` * 150) when '2227' then (`csv`.`LINE_NUM` * 10) when '2228' then (`csv`.`LINE_NUM` * 5) when '2229' then (`csv`.`LINE_NUM` * 1) when '2230' then (`csv`.`LINE_NUM` * 10) when '2231' then (`csv`.`LINE_NUM` * 5) when '2232' then (`csv`.`LINE_NUM` * 1) when '2233' then (`csv`.`LINE_NUM` * 1) when '2234' then (`csv`.`LINE_NUM` * 5) when '2262' then (`csv`.`LINE_NUM` * 1) when '2381' then (`csv`.`LINE_NUM` * 1) when '2382' then (`csv`.`LINE_NUM` * 0.1) when '2258' then (`csv`.`MONTH_NUM` / 10000) when '2320' then (`csv`.`MONTH_NUM` / 10000) when '2305' then (`csv`.`MONTH_NUM` / 10000) when '2395' then (`csv`.`MONTH_NUM` / 10000) when '2396' then (`csv`.`MONTH_NUM` / 10000) when '2397' then (`csv`.`MONTH_NUM` / 10000) when '2468' then (`csv`.`LINE_NUM` * 5) when '2512' then (`csv`.`LINE_NUM` * 35) when '2513' then (`csv`.`LINE_NUM` * 9) when '2514' then (`csv`.`LINE_NUM` * 2) when '2515' then (`csv`.`LINE_NUM` * 8) when '2516' then (`csv`.`LINE_NUM` * 3) when '2517' then (`csv`.`LINE_NUM` * 1) else 0 end) AS `DATA_CHANGE`,(case `csv`.`CATE` when '2444' then `csv`.`LINE_NUM` when '2263' then `csv`.`LINE_NUM` when '2200' then `csv`.`LINE_NUM` when '2201' then `csv`.`LINE_NUM` when '2202' then `csv`.`LINE_NUM` when '2445' then `csv`.`LINE_NUM` when '2203' then `csv`.`LINE_NUM` when '2204' then `csv`.`LINE_NUM` when '2205' then `csv`.`LINE_NUM` when '2206' then `csv`.`LINE_NUM` when '2446' then `csv`.`LINE_NUM` when '2207' then `csv`.`LINE_NUM` when '2208' then `csv`.`LINE_NUM` when '2209' then `csv`.`LINE_NUM` when '2210' then `csv`.`LINE_NUM` when '2211' then `csv`.`LINE_NUM` when '2212' then `csv`.`LINE_NUM` when '2213' then `csv`.`LINE_NUM` when '2214' then `csv`.`LINE_NUM` when '2264' then `csv`.`LINE_NUM` when '2215' then `csv`.`LINE_NUM` else 0 end) AS `DATA_GET_PHYSICS`,(case `csv`.`CATE` when '2444' then (`csv`.`LINE_NUM` * 130) when '2263' then (`csv`.`LINE_NUM` * 70) when '2200' then (`csv`.`LINE_NUM` * 40) when '2201' then (`csv`.`LINE_NUM` * 20) when '2202' then (`csv`.`LINE_NUM` * 10) when '2445' then (`csv`.`LINE_NUM` * 200) when '2203' then (`csv`.`LINE_NUM` * 150) when '2204' then (`csv`.`LINE_NUM` * 50) when '2205' then (`csv`.`LINE_NUM` * 20) when '2206' then (`csv`.`LINE_NUM` * 10) when '2446' then (`csv`.`LINE_NUM` * 200) when '2207' then (`csv`.`LINE_NUM` * 150) when '2208' then (`csv`.`LINE_NUM` * 70) when '2209' then (`csv`.`LINE_NUM` * 60) when '2210' then (`csv`.`LINE_NUM` * 20) when '2211' then (`csv`.`LINE_NUM` * 10) when '2212' then (`csv`.`LINE_NUM` * 20) when '2213' then (`csv`.`LINE_NUM` * 200) when '2214' then (`csv`.`LINE_NUM` * 30) when '2264' then (`csv`.`LINE_NUM` * 20) when '2215' then (`csv`.`LINE_NUM` * 10) else 0 end) AS `DATA_GET_CHANGE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` when '2143' then `csv`.`LINE_NUM` when '2183' then `csv`.`LINE_NUM` when '2184' then `csv`.`LINE_NUM` when '2240' then `csv`.`LINE_NUM` when '2241' then `csv`.`LINE_NUM` when '2242' then `csv`.`LINE_NUM` when '2243' then `csv`.`LINE_NUM` when '2244' then `csv`.`LINE_NUM` when '2278' then `csv`.`LINE_NUM` when '2279' then `csv`.`LINE_NUM` when '2280' then `csv`.`LINE_NUM` when '2438' then `csv`.`LINE_NUM` when '2439' then `csv`.`LINE_NUM` when '2440' then `csv`.`LINE_NUM` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `MOBILE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` else 0 end) AS `HS_SALE_END`,(case `csv`.`CATE` when '2143' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_SALE_END`,(case `csv`.`CATE` when '2241' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_SALE_END`,(case `csv`.`CATE` when '2240' then `csv`.`LINE_NUM` else 0 end) AS `HS_ALLOTMENT`,(case `csv`.`CATE` when '2244' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_ALLOTMENT`,(case `csv`.`CATE` when '2242' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_ALLOTMENT`,(case `csv`.`CATE` when '2183' then `csv`.`LINE_NUM` else 0 end) AS `HS_RENTAL`,(case `csv`.`CATE` when '2184' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_RENTAL`,(case `csv`.`CATE` when '2243' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_RENTAL`,(case `csv`.`CATE` when '2265' then `csv`.`LINE_NUM` when '2268' then `csv`.`LINE_NUM` when '2269' then `csv`.`LINE_NUM` when '2287' then `csv`.`LINE_NUM` when '2350' then `csv`.`LINE_NUM` when '2351' then `csv`.`LINE_NUM` when '2352' then `csv`.`LINE_NUM` when '2353' then `csv`.`LINE_NUM` when '2354' then `csv`.`LINE_NUM` when '2355' then `csv`.`LINE_NUM` when '2356' then `csv`.`LINE_NUM` when '2357' then `csv`.`LINE_NUM` when '2358' then `csv`.`LINE_NUM` when '2361' then `csv`.`LINE_NUM` when '2362' then `csv`.`LINE_NUM` when '2363' then `csv`.`LINE_NUM` when '2364' then `csv`.`LINE_NUM` when '2365' then `csv`.`LINE_NUM` when '2366' then `csv`.`LINE_NUM` when '2367' then `csv`.`LINE_NUM` else 0 end) AS `PHS`,(case `csv`.`CATE` when '2191' then `csv`.`LINE_NUM` else 0 end) AS `WHITE_OFFERS`,(case `csv`.`CATE` when '2193' then `csv`.`LINE_NUM` else 0 end) AS `MODEL_CHANGE`,(case `csv`.`CATE` when '1115' then `csv`.`LINE_NUM` else 0 end) AS `ML`,(case `csv`.`CATE` when '2267' then `csv`.`LINE_NUM` when '2413' then `csv`.`LINE_NUM` when '2302' then `csv`.`LINE_NUM` when '2303' then `csv`.`LINE_NUM` when '2304' then `csv`.`LINE_NUM` when '2309' then `csv`.`LINE_NUM` when '2398' then `csv`.`LINE_NUM` when '2310' then `csv`.`LINE_NUM` when '2311' then `csv`.`LINE_NUM` when '2312' then `csv`.`LINE_NUM` when '2313' then `csv`.`LINE_NUM` when '2314' then `csv`.`LINE_NUM` when '2456' then `csv`.`LINE_NUM` when '2457' then `csv`.`LINE_NUM` when '2458' then `csv`.`LINE_NUM` when '2459' then `csv`.`LINE_NUM` when '2460' then `csv`.`LINE_NUM` when '2461' then `csv`.`LINE_NUM` when '2404' then `csv`.`LINE_NUM` when '2401' then `csv`.`LINE_NUM` when '2402' then `csv`.`LINE_NUM` when '2403' then `csv`.`LINE_NUM` when '2285' then `csv`.`LINE_NUM` when '2198' then `csv`.`LINE_NUM` when '2296' then `csv`.`LINE_NUM` when '2297' then `csv`.`LINE_NUM` when '2298' then `csv`.`LINE_NUM` when '2299' then `csv`.`LINE_NUM` when '2300' then `csv`.`LINE_NUM` when '2301' then `csv`.`LINE_NUM` when '2271' then `csv`.`LINE_NUM` when '2282' then `csv`.`LINE_NUM` when '2199' then `csv`.`LINE_NUM` when '2246' then `csv`.`LINE_NUM` when '2380' then `csv`.`LINE_NUM` when '2276' then `csv`.`LINE_NUM` when '2288' then `csv`.`LINE_NUM` when '2289' then `csv`.`LINE_NUM` when '2405' then `csv`.`LINE_NUM` when '2406' then `csv`.`LINE_NUM` when '2407' then `csv`.`LINE_NUM` when '2408' then `csv`.`LINE_NUM` when '2409' then `csv`.`LINE_NUM` when '2293' then `csv`.`LINE_NUM` when '2391' then `csv`.`LINE_NUM` when '2359' then `csv`.`LINE_NUM` when '2360' then `csv`.`LINE_NUM` when '2400' then `csv`.`LINE_NUM` when '2295' then `csv`.`LINE_NUM` when '2368' then `csv`.`LINE_NUM` when '2369' then `csv`.`LINE_NUM` when '2518' then `csv`.`LINE_NUM` when '2247' then `csv`.`LINE_NUM` when '2248' then `csv`.`LINE_NUM` when '2249' then `csv`.`LINE_NUM` when '2250' then `csv`.`LINE_NUM` when '2251' then `csv`.`LINE_NUM` when '2252' then `csv`.`LINE_NUM` when '2253' then `csv`.`LINE_NUM` when '2383' then `csv`.`LINE_NUM` when '2384' then `csv`.`LINE_NUM` when '2385' then `csv`.`LINE_NUM` when '2386' then `csv`.`LINE_NUM` when '2387' then `csv`.`LINE_NUM` when '2388' then `csv`.`LINE_NUM` when '2389' then `csv`.`LINE_NUM` when '2390' then `csv`.`LINE_NUM` when '2286' then `csv`.`LINE_NUM` when '2492' then `csv`.`LINE_NUM` when '2493' then `csv`.`LINE_NUM` when '2494' then `csv`.`LINE_NUM` when '2495' then `csv`.`LINE_NUM` when '2496' then `csv`.`LINE_NUM` when '2497' then `csv`.`LINE_NUM` when '2255' then `csv`.`LINE_NUM` when '2260' then `csv`.`LINE_NUM` when '2261' then `csv`.`LINE_NUM` when '2315' then `csv`.`LINE_NUM` when '2316' then `csv`.`LINE_NUM` when '2317' then `csv`.`LINE_NUM` when '2318' then `csv`.`LINE_NUM` when '2319' then `csv`.`LINE_NUM` when '2256' then `csv`.`LINE_NUM` when '2277' then `csv`.`LINE_NUM` when '2510' then `csv`.`LINE_NUM` when '2511' then `csv`.`LINE_NUM` when '2502' then `csv`.`LINE_NUM` when '2283' then `csv`.`LINE_NUM` when '2284' then `csv`.`LINE_NUM` when '2273' then `csv`.`LINE_NUM` when '2415' then `csv`.`LINE_NUM` when '2416' then `csv`.`LINE_NUM` when '2417' then `csv`.`LINE_NUM` when '2414' then `csv`.`LINE_NUM` when '2467' then `csv`.`LINE_NUM` when '2484' then `csv`.`LINE_NUM` when '2321' then `csv`.`LINE_NUM` when '2322' then `csv`.`LINE_NUM` when '2323' then `csv`.`LINE_NUM` when '2324' then `csv`.`LINE_NUM` when '2325' then `csv`.`LINE_NUM` when '2326' then `csv`.`LINE_NUM` when '2327' then `csv`.`LINE_NUM` when '2328' then `csv`.`LINE_NUM` when '2329' then `csv`.`LINE_NUM` when '2330' then `csv`.`LINE_NUM` when '2331' then `csv`.`LINE_NUM` when '2332' then `csv`.`LINE_NUM` when '2333' then `csv`.`LINE_NUM` when '2334' then `csv`.`LINE_NUM` when '2335' then `csv`.`LINE_NUM` when '2336' then `csv`.`LINE_NUM` when '2337' then `csv`.`LINE_NUM` when '2338' then `csv`.`LINE_NUM` when '2339' then `csv`.`LINE_NUM` when '2340' then `csv`.`LINE_NUM` when '2341' then `csv`.`LINE_NUM` when '2370' then `csv`.`LINE_NUM` when '2371' then `csv`.`LINE_NUM` when '2372' then `csv`.`LINE_NUM` when '2373' then `csv`.`LINE_NUM` when '2374' then `csv`.`LINE_NUM` when '2375' then `csv`.`LINE_NUM` when '2376' then `csv`.`LINE_NUM` when '2377' then `csv`.`LINE_NUM` when '2378' then `csv`.`LINE_NUM` when '2379' then `csv`.`LINE_NUM` when '2392' then `csv`.`LINE_NUM` when '2393' then `csv`.`LINE_NUM` when '2394' then `csv`.`LINE_NUM` when '2410' then `csv`.`LINE_NUM` when '2411' then `csv`.`LINE_NUM` when '2412' then `csv`.`LINE_NUM` when '2469' then `csv`.`LINE_NUM` when '2470' then `csv`.`LINE_NUM` when '2471' then `csv`.`LINE_NUM` when '2472' then `csv`.`LINE_NUM` when '2473' then `csv`.`LINE_NUM` when '2474' then `csv`.`LINE_NUM` when '2475' then `csv`.`LINE_NUM` when '2476' then `csv`.`LINE_NUM` when '2486' then `csv`.`LINE_NUM` when '2499' then `csv`.`LINE_NUM` when '2522' then `csv`.`LINE_NUM` when '2523' then `csv`.`LINE_NUM` when '2524' then `csv`.`LINE_NUM` when '2525' then `csv`.`LINE_NUM` when '2526' then `csv`.`LINE_NUM` when '2527' then `csv`.`LINE_NUM` when '2528' then `csv`.`LINE_NUM` when '2529' then `csv`.`LINE_NUM` when '2530' then `csv`.`LINE_NUM` when '2531' then `csv`.`LINE_NUM` when '2532' then `csv`.`LINE_NUM` when '2418' then `csv`.`LINE_NUM` when '2419' then `csv`.`LINE_NUM` when '2420' then `csv`.`LINE_NUM` when '2421' then `csv`.`LINE_NUM` when '2422' then `csv`.`LINE_NUM` when '2423' then `csv`.`LINE_NUM` when '2477' then `csv`.`LINE_NUM` when '2478' then `csv`.`LINE_NUM` when '2479' then `csv`.`LINE_NUM` when '2480' then `csv`.`LINE_NUM` when '2481' then `csv`.`LINE_NUM` when '2482' then `csv`.`LINE_NUM` when '2483' then `csv`.`LINE_NUM` when '2533' then `csv`.`LINE_NUM` when '2534' then `csv`.`LINE_NUM` when '2535' then `csv`.`LINE_NUM` when '2536' then `csv`.`LINE_NUM` when '2551' then `csv`.`LINE_NUM` when '2552' then `csv`.`LINE_NUM` when '2553' then `csv`.`LINE_NUM` when '2554' then `csv`.`LINE_NUM` when '2555' then `csv`.`LINE_NUM` when '2556' then `csv`.`LINE_NUM` when '2557' then `csv`.`LINE_NUM` when '2558' then `csv`.`LINE_NUM` when '2559' then `csv`.`LINE_NUM` when '2560' then `csv`.`LINE_NUM` when '2561' then `csv`.`LINE_NUM` when '2570' then `csv`.`LINE_NUM` when '2571' then `csv`.`LINE_NUM` when '2572' then `csv`.`LINE_NUM` when '2573' then `csv`.`LINE_NUM` when '2574' then `csv`.`LINE_NUM` when '2575' then `csv`.`LINE_NUM` when '2576' then `csv`.`LINE_NUM` when '2577' then `csv`.`LINE_NUM` when '2578' then `csv`.`LINE_NUM` when '2579' then `csv`.`LINE_NUM` when '2580' then `csv`.`LINE_NUM` when '2581' then `csv`.`LINE_NUM` when '2582' then `csv`.`LINE_NUM` when '2583' then `csv`.`LINE_NUM` when '2584' then `csv`.`LINE_NUM` when '2585' then `csv`.`LINE_NUM` when '2586' then `csv`.`LINE_NUM` when '2587' then `csv`.`LINE_NUM` else 0 end) AS `CLOUD_PHYSICS`,(case `csv`.`CATE` when '2267' then (`csv`.`LINE_NUM` * 0.1) when '2413' then (`csv`.`LINE_NUM` * 0.5) when '2302' then (`csv`.`LINE_NUM` * 3) when '2303' then (`csv`.`LINE_NUM` * 5) when '2304' then (`csv`.`LINE_NUM` * 7) when '2309' then (`csv`.`LINE_NUM` * 14) when '2398' then (`csv`.`LINE_NUM` * 8) when '2310' then (`csv`.`LINE_NUM` * 16) when '2311' then (`csv`.`LINE_NUM` * 38) when '2312' then (`csv`.`LINE_NUM` * 105) when '2313' then (`csv`.`LINE_NUM` * 2) when '2314' then (`csv`.`LINE_NUM` * 10) when '2456' then (`csv`.`LINE_NUM` * 14) when '2457' then (`csv`.`LINE_NUM` * 8) when '2458' then (`csv`.`LINE_NUM` * 16) when '2459' then (`csv`.`LINE_NUM` * 38) when '2460' then (`csv`.`LINE_NUM` * 105) when '2461' then (`csv`.`LINE_NUM` * 2) when '2404' then (`csv`.`LINE_NUM` * 1) when '2401' then (`csv`.`LINE_NUM` * 2) when '2402' then (`csv`.`LINE_NUM` * 3) when '2403' then (`csv`.`LINE_NUM` * 4) when '2285' then (`csv`.`LINE_NUM` * 7.5) when '2198' then (`csv`.`LINE_NUM` * 1) when '2296' then (`csv`.`LINE_NUM` * 3) when '2297' then (`csv`.`LINE_NUM` * 8) when '2298' then (`csv`.`LINE_NUM` * 15) when '2299' then (`csv`.`LINE_NUM` * 30) when '2300' then (`csv`.`LINE_NUM` * 85) when '2301' then (`csv`.`LINE_NUM` * 100) when '2271' then (`csv`.`LINE_NUM` * 12) when '2282' then (`csv`.`LINE_NUM` * 2) when '2199' then (`csv`.`LINE_NUM` * 0.02) when '2246' then (`csv`.`LINE_NUM` * 20) when '2380' then (`csv`.`LINE_NUM` * 1.8) when '2276' then (`csv`.`LINE_NUM` * 0.2) when '2288' then (`csv`.`LINE_NUM` * 0.5) when '2289' then (`csv`.`LINE_NUM` * 0.8) when '2405' then (`csv`.`LINE_NUM` * 4) when '2406' then (`csv`.`LINE_NUM` * 9) when '2407' then (`csv`.`LINE_NUM` * 12) when '2408' then (`csv`.`LINE_NUM` * 26) when '2409' then (`csv`.`LINE_NUM` * 34) when '2293' then (`csv`.`LINE_NUM` * 5) when '2391' then (`csv`.`LINE_NUM` * 0.6) when '2359' then (`csv`.`LINE_NUM` * 0.3) when '2360' then (`csv`.`LINE_NUM` * 0.7) when '2400' then (`csv`.`LINE_NUM` * 0.1) when '2295' then (`csv`.`LINE_NUM` * 8) when '2368' then (`csv`.`LINE_NUM` * 0.02) when '2369' then (`csv`.`LINE_NUM` * 0.06) when '2518' then (`csv`.`LINE_NUM` * 126) when '2247' then (`csv`.`LINE_NUM` * 6) when '2248' then (`csv`.`LINE_NUM` * 20) when '2249' then (`csv`.`LINE_NUM` * 25) when '2250' then (`csv`.`LINE_NUM` * 20) when '2251' then (`csv`.`LINE_NUM` * 25) when '2252' then (`csv`.`LINE_NUM` * 10) when '2253' then (`csv`.`LINE_NUM` * 20) when '2383' then (`csv`.`LINE_NUM` * 6) when '2384' then (`csv`.`LINE_NUM` * 20) when '2385' then (`csv`.`LINE_NUM` * 61) when '2386' then (`csv`.`LINE_NUM` * 125) when '2387' then (`csv`.`LINE_NUM` * 150) when '2388' then (`csv`.`LINE_NUM` * 47) when '2389' then (`csv`.`LINE_NUM` * 128) when '2390' then (`csv`.`LINE_NUM` * 150) when '2286' then (`csv`.`LINE_NUM` * 0.06) when '2492' then (`csv`.`LINE_NUM` * 10) when '2493' then (`csv`.`LINE_NUM` * 30) when '2494' then (`csv`.`LINE_NUM` * 50) when '2495' then (`csv`.`LINE_NUM` * 40) when '2496' then (`csv`.`LINE_NUM` * 60) when '2497' then (`csv`.`LINE_NUM` * 80) when '2255' then (`csv`.`LINE_NUM` * 35) when '2260' then (`csv`.`LINE_NUM` * 17.5) when '2261' then (`csv`.`LINE_NUM` * 8.75) when '2315' then (`csv`.`LINE_NUM` * 7) when '2316' then (`csv`.`LINE_NUM` * 20) when '2317' then (`csv`.`LINE_NUM` * 30) when '2318' then (`csv`.`LINE_NUM` * 70) when '2319' then (`csv`.`LINE_NUM` * 150) when '2256' then (`csv`.`LINE_NUM` * 6) when '2277' then (`csv`.`LINE_NUM` * 0.1) when '2510' then (`csv`.`LINE_NUM` * 0.1) when '2511' then (`csv`.`LINE_NUM` * 0.1) when '2502' then (`csv`.`LINE_NUM` * 0.02) when '2283' then (`csv`.`LINE_NUM` * 3) when '2284' then (`csv`.`LINE_NUM` * 2) when '2273' then (`csv`.`LINE_NUM` * 1) when '2415' then (`csv`.`LINE_NUM` * 1) when '2416' then (`csv`.`LINE_NUM` * 6) when '2417' then (`csv`.`LINE_NUM` * 10) when '2414' then (`csv`.`LINE_NUM` * 0.2) when '2467' then (`csv`.`LINE_NUM` * 0.1) when '2484' then (`csv`.`LINE_NUM` * 14) when '2321' then (`csv`.`LINE_NUM` * 3) when '2322' then (`csv`.`LINE_NUM` * 10) when '2323' then (`csv`.`LINE_NUM` * 10) when '2324' then (`csv`.`LINE_NUM` * 3) when '2325' then (`csv`.`LINE_NUM` * 3) when '2326' then (`csv`.`LINE_NUM` * 3) when '2327' then (`csv`.`LINE_NUM` * 3) when '2328' then (`csv`.`LINE_NUM` * 3) when '2329' then (`csv`.`LINE_NUM` * 3) when '2330' then (`csv`.`LINE_NUM` * 3) when '2331' then (`csv`.`LINE_NUM` * 3) when '2332' then (`csv`.`LINE_NUM` * 3) when '2333' then (`csv`.`LINE_NUM` * 3) when '2334' then (`csv`.`LINE_NUM` * 3) when '2335' then (`csv`.`LINE_NUM` * 1) when '2336' then (`csv`.`LINE_NUM` * 1) when '2337' then (`csv`.`LINE_NUM` * 1) when '2338' then (`csv`.`LINE_NUM` * 1) when '2339' then (`csv`.`LINE_NUM` * 1) when '2340' then (`csv`.`LINE_NUM` * 1) when '2341' then (`csv`.`LINE_NUM` * 1) when '2370' then (`csv`.`LINE_NUM` * 10) when '2371' then (`csv`.`LINE_NUM` * 10) when '2372' then (`csv`.`LINE_NUM` * 3) when '2373' then (`csv`.`LINE_NUM` * 3) when '2374' then (`csv`.`LINE_NUM` * 3) when '2375' then (`csv`.`LINE_NUM` * 3) when '2376' then (`csv`.`LINE_NUM` * 3) when '2377' then (`csv`.`LINE_NUM` * 3) when '2378' then (`csv`.`LINE_NUM` * 3) when '2379' then (`csv`.`LINE_NUM` * 1) when '2392' then (`csv`.`LINE_NUM` * 3) when '2393' then (`csv`.`LINE_NUM` * 3) when '2394' then (`csv`.`LINE_NUM` * 1) when '2410' then (`csv`.`LINE_NUM` * 3) when '2411' then (`csv`.`LINE_NUM` * 3) when '2412' then (`csv`.`LINE_NUM` * 1) when '2469' then (`csv`.`LINE_NUM` * 10) when '2470' then (`csv`.`LINE_NUM` * 3) when '2471' then (`csv`.`LINE_NUM` * 1) when '2472' then (`csv`.`LINE_NUM` * 3) when '2473' then (`csv`.`LINE_NUM` * 3) when '2474' then (`csv`.`LINE_NUM` * 1) when '2475' then (`csv`.`LINE_NUM` * 1) when '2476' then (`csv`.`LINE_NUM` * 1) when '2486' then (`csv`.`LINE_NUM` * 3) when '2499' then (`csv`.`LINE_NUM` * 1) when '2522' then (`csv`.`LINE_NUM` * 10) when '2523' then (`csv`.`LINE_NUM` * 3) when '2524' then (`csv`.`LINE_NUM` * 3) when '2525' then (`csv`.`LINE_NUM` * 3) when '2526' then (`csv`.`LINE_NUM` * 3) when '2527' then (`csv`.`LINE_NUM` * 3) when '2528' then (`csv`.`LINE_NUM` * 3) when '2529' then (`csv`.`LINE_NUM` * 1) when '2530' then (`csv`.`LINE_NUM` * 1) when '2531' then (`csv`.`LINE_NUM` * 1) when '2532' then (`csv`.`LINE_NUM` * 1) when '2418' then (`csv`.`LINE_NUM` * 3) when '2419' then (`csv`.`LINE_NUM` * 1) when '2420' then (`csv`.`LINE_NUM` * 1) when '2421' then (`csv`.`LINE_NUM` * 1) when '2422' then (`csv`.`LINE_NUM` * 1) when '2423' then (`csv`.`LINE_NUM` * 1) when '2477' then (`csv`.`LINE_NUM` * 3) when '2478' then (`csv`.`LINE_NUM` * 3) when '2479' then (`csv`.`LINE_NUM` * 3) when '2480' then (`csv`.`LINE_NUM` * 3) when '2481' then (`csv`.`LINE_NUM` * 3) when '2482' then (`csv`.`LINE_NUM` * 1) when '2483' then (`csv`.`LINE_NUM` * 1) when '2533' then (`csv`.`LINE_NUM` * 3) when '2534' then (`csv`.`LINE_NUM` * 8) when '2535' then (`csv`.`LINE_NUM` * 5) when '2536' then (`csv`.`LINE_NUM` * 80) when '2551' then (`csv`.`LINE_NUM` * 1) when '2552' then (`csv`.`LINE_NUM` * 1) when '2553' then (`csv`.`LINE_NUM` * 3) when '2554' then (`csv`.`LINE_NUM` * 5) when '2555' then (`csv`.`LINE_NUM` * 8) when '2556' then (`csv`.`LINE_NUM` * 13) when '2557' then (`csv`.`LINE_NUM` * 19) when '2558' then (`csv`.`LINE_NUM` * 25) when '2559' then (`csv`.`LINE_NUM` * 44) when '2560' then (`csv`.`LINE_NUM` * 6) when '2561' then (`csv`.`LINE_NUM` * 14) when '2570' then (`csv`.`LINE_NUM` * 14) when '2571' then (`csv`.`LINE_NUM` * 17) when '2572' then (`csv`.`LINE_NUM` * 24) when '2573' then (`csv`.`LINE_NUM` * 31) when '2574' then (`csv`.`LINE_NUM` * 38) when '2575' then (`csv`.`LINE_NUM` * 45) when '2576' then (`csv`.`LINE_NUM` * 52) when '2577' then (`csv`.`LINE_NUM` * 59) when '2578' then (`csv`.`LINE_NUM` * 67) when '2579' then (`csv`.`LINE_NUM` * 74) when '2580' then (`csv`.`LINE_NUM` * 81) when '2581' then (`csv`.`LINE_NUM` * 7) when '2582' then (`csv`.`LINE_NUM` * 2) when '2583' then (`csv`.`LINE_NUM` * 4) when '2584' then (`csv`.`LINE_NUM` * 6) when '2585' then (`csv`.`LINE_NUM` * 2) when '2586' then (`csv`.`LINE_NUM` * 4) when '2587' then (`csv`.`LINE_NUM` * 6) else 0 end) AS `CLOUD_CHANGE`,(case `csv`.`CATE` when '2449' then `csv`.`LINE_NUM` when '2450' then `csv`.`LINE_NUM` when '2451' then `csv`.`LINE_NUM` when '2453' then `csv`.`LINE_NUM` when '2435' then `csv`.`LINE_NUM` when '2487' then `csv`.`LINE_NUM` when '2488' then `csv`.`LINE_NUM` when '2489' then `csv`.`LINE_NUM` when '2490' then `csv`.`LINE_NUM` when '2491' then `csv`.`LINE_NUM` when '2436' then `csv`.`LINE_NUM` when '2437' then `csv`.`LINE_NUM` when '2504' then `csv`.`LINE_NUM` when '2505' then `csv`.`LINE_NUM` when '2506' then `csv`.`LINE_NUM` when '2507' then `csv`.`LINE_NUM` when '2508' then `csv`.`LINE_NUM` when '2509' then `csv`.`LINE_NUM` when '2500' then `csv`.`LINE_NUM` when '2501' then `csv`.`LINE_NUM` when '2562' then `csv`.`LINE_NUM` when '2563' then `csv`.`LINE_NUM` when '2564' then `csv`.`LINE_NUM` when '2565' then `csv`.`LINE_NUM` when '2566' then `csv`.`LINE_NUM` when '2567' then `csv`.`LINE_NUM` when '2568' then `csv`.`LINE_NUM` when '2569' then `csv`.`LINE_NUM` else 0 end) AS `YAHOO_PHYSICS`,(case `csv`.`CATE` when '2449' then (`csv`.`LINE_NUM` * 4) when '2450' then (`csv`.`LINE_NUM` * 7) when '2451' then (`csv`.`LINE_NUM` * 9) when '2453' then (`csv`.`MONTH_NUM` * 0.000023) when '2435' then (`csv`.`TEMP_NUM` * 0.000003125) when '2487' then (`csv`.`TEMP_NUM` * 0.000003125) when '2488' then (`csv`.`TEMP_NUM` * 0.000003125) when '2489' then (`csv`.`TEMP_NUM` * 0.000003125) when '2490' then (`csv`.`TEMP_NUM` * 0.000003125) when '2491' then (`csv`.`TEMP_NUM` * 0.000003125) when '2436' then (`csv`.`LINE_NUM` * 0) when '2437' then (`csv`.`LINE_NUM` * 0) when '2504' then (`csv`.`LINE_NUM` * 4) when '2505' then (`csv`.`LINE_NUM` * 7) when '2506' then (`csv`.`LINE_NUM` * 9) when '2508' then (`csv`.`MONTH_NUM` * 0.000023) when '2509' then (`csv`.`LINE_NUM` * 1) when '2500' then (`csv`.`TEMP_NUM` * 0.00001042) when '2501' then (`csv`.`LINE_NUM` * 3) when '2562' then (`csv`.`LINE_NUM` * 4) when '2563' then (`csv`.`LINE_NUM` * 7) when '2564' then (`csv`.`LINE_NUM` * 9) when '2565' then (`csv`.`MONTH_NUM` * 0.000023) when '2566' then (`csv`.`LINE_NUM` * 4) when '2567' then (`csv`.`LINE_NUM` * 7) when '2568' then (`csv`.`LINE_NUM` * 9) when '2569' then (`csv`.`MONTH_NUM` * 0.000023) else 0 end) AS `YAHOO_CHANGE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` when '2240' then `csv`.`LINE_NUM` when '2183' then `csv`.`LINE_NUM` else 0 end) AS `HS`,(case `csv`.`CATE` when '2438' then `csv`.`LINE_NUM` when '2439' then `csv`.`LINE_NUM` when '2440' then `csv`.`LINE_NUM` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE`,(case `csv`.`CATE` when '2143' then `csv`.`LINE_NUM` when '2244' then `csv`.`LINE_NUM` when '2184' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD`,(case `csv`.`CATE` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `IPAD`,(case `csv`.`CATE` when '2267' then `csv`.`LINE_NUM` else 0 end) AS `GOOGLEAPPS`,(case `csv`.`CATE` when '2500' then (((`csv`.`TEMP_NUM` * 0.50) / 4000) / 12) when '2746' then (((`csv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2747' then (((`csv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2748' then (((`csv`.`TEMP_NUM` * 0.20) / 4000) / 12) when '2749' then (((`csv`.`TEMP_NUM` * 0.15) / 4000) / 12) when '2751' then (((`csv`.`TEMP_NUM` * 0.40) / 4000) / 12) else 0 end) AS `O2O`,(case `csv`.`CATE` when '2467' then (`csv`.`LINE_NUM` * 0.01) else 0 end) AS `PAYPAL` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_cancel_opportunities` `opc` on((`opp`.`ID` = `opc`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_cancel` `csv` on(((`opc`.`SV_CANCEL_IDA` = `csv`.`ID`) and (`csv`.`DELETED` = '0')))) join `ifview_eida_mast_serviceline` `msv` on(((`csv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp2_fy10_2`
--

/*!50001 DROP TABLE IF EXISTS `view_opp2_fy10_2`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp2_fy10_2` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`opc`.`DELETED` = 0),`csv`.`DELETED`,`opc`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`csv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`csv`.`YYYY`,'/',`csv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`csv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`csv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) else 0 end) AS `SV_NUM`,`csv`.`MONTH_NUM` AS `MONTH_NUM`,`csv`.`TEMP_NUM` AS `TEMP_NUM`,`csv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'解約' AS `SV_TYPE`,`csv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,`opp`.`OP_DEPEND_C` AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`csv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`csv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`csv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`csv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`csv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`csv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`csv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`csv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_cancel_opportunities` `opc` on((`opp`.`ID` = `opc`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_cancel` `csv` on((`opc`.`SV_CANCEL_IDA` = `csv`.`ID`))) join `ifview_eida_mast_serviceline` `msv` on(((`csv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`sbtm_tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp2_fy10_del`
--

/*!50001 DROP TABLE IF EXISTS `view_opp2_fy10_del`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_del`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp2_fy10_del` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`opc`.`DELETED` = 0),`csv`.`DELETED`,`opc`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`csv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`csv`.`YYYY`,'/',`csv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`csv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`csv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) when '6' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.3) / 12) / 4000),2) when '7' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.35) / 12) / 4000),2) when '8' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.125) / 12) / 4000),2) when '9' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.2) / 12) / 4000),2) when '10' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.4) / 12) / 4000),2) when '11' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 1.0) / 12) / 4000),2) when '12' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.2) / 4000),2) when '13' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 12) / 4000),2) when '14' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 4000),2) else 0 end) AS `SV_NUM`,`csv`.`MONTH_NUM` AS `MONTH_NUM`,`csv`.`TEMP_NUM` AS `TEMP_NUM`,`csv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'解約' AS `SV_TYPE`,`csv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,left(`opp`.`OP_DEPEND_C`,40) AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`csv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`csv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`csv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`csv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`csv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`csv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`csv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`csv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO`,date_format(`csv`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `SV_DATE_MODIFIED_FMT`,`es`.`div1_nm` AS `MAST_ORGANIZATIONUNIT1_NM`,`es`.`div2_nm` AS `MAST_ORGANIZATIONUNIT2_NM`,`es`.`div3_nm` AS `MAST_ORGANIZATIONUNIT3_NM`,`es`.`div4_nm` AS `MAST_ORGANIZATIONUNIT4_NM`,`es`.`div5_nm` AS `MAST_ORGANIZATIONUNIT5_NM`,`es`.`div6_nm` AS `MAST_ORGANIZATIONUNIT6_NM`,left(`opp`.`OP_CONT_PLANDATE_C`,7) AS `OP_CONT_PLANMONTH`,`csv`.`MONTH_NUM_LOCAL_CURRENCY` AS `MONTH_NUM_LOCAL_CURRENCY`,`csv`.`MONTH_NUM_UNIT` AS `MONTH_NUM_UNIT`,`csv`.`TEMP_NUM_LOCAL_CURRENCY` AS `TEMP_NUM_LOCAL_CURRENCY`,`csv`.`TEMP_NUM_UNIT` AS `TEMP_NUM_UNIT`,(case when (`opp`.`OP_STEP_C` = '50') then '契約・申込' when (`opp`.`OP_STEP_C` = '60') then '失注' when (`opp`.`OP_STEP_C` = '70') then '案件消滅' when (`opp`.`OP_COMMIT_C` = '001') then 'ベース' when (`opp`.`OP_COMMIT_C` = '002') then 'アグレッシブ' when (`opp`.`OP_COMMIT_C` = '003') then 'チャレンジ' else '' end) AS `OP_MANAGE_FORECAST`,`csv`.`MONTH_AGE` AS `MONTH_AGE`,(case `csv`.`CATE` when '1106' then `csv`.`LINE_NUM` when '2464' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI`,(case `csv`.`CATE` when '1107' then `csv`.`LINE_NUM` when '2463' then `csv`.`LINE_NUM` when '1108' then `csv`.`LINE_NUM` when '2462' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_BRI`,(case `csv`.`CATE` when '1106' then `csv`.`LINE_NUM` when '2464' then `csv`.`LINE_NUM` when '1107' then `csv`.`LINE_NUM` when '2463' then `csv`.`LINE_NUM` when '1108' then `csv`.`LINE_NUM` when '2462' then `csv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI1`,(case `csv`.`CATE` when '1106' then (`csv`.`LINE_NUM` * 20) when '2464' then (`csv`.`LINE_NUM` * 30) when '1107' then `csv`.`LINE_NUM` when '2463' then (`csv`.`LINE_NUM` * 1.5) when '1108' then `csv`.`LINE_NUM` when '2462' then (`csv`.`LINE_NUM` * 1.5) else 0 end) AS `SPECIAL_PRI20`,(case `csv`.`CATE` when '2444' then `csv`.`LINE_NUM` when '2263' then `csv`.`LINE_NUM` when '2200' then `csv`.`LINE_NUM` when '2201' then `csv`.`LINE_NUM` when '2202' then `csv`.`LINE_NUM` when '2445' then `csv`.`LINE_NUM` when '2203' then `csv`.`LINE_NUM` when '2204' then `csv`.`LINE_NUM` when '2205' then `csv`.`LINE_NUM` when '2206' then `csv`.`LINE_NUM` when '2446' then `csv`.`LINE_NUM` when '2207' then `csv`.`LINE_NUM` when '2208' then `csv`.`LINE_NUM` when '2209' then `csv`.`LINE_NUM` when '2210' then `csv`.`LINE_NUM` when '2211' then `csv`.`LINE_NUM` when '2212' then `csv`.`LINE_NUM` when '2213' then `csv`.`LINE_NUM` when '2214' then `csv`.`LINE_NUM` when '2264' then `csv`.`LINE_NUM` when '2215' then `csv`.`LINE_NUM` when '2447' then `csv`.`LINE_NUM` when '2217' then `csv`.`LINE_NUM` when '2218' then `csv`.`LINE_NUM` when '2219' then `csv`.`LINE_NUM` when '2220' then `csv`.`LINE_NUM` when '2221' then `csv`.`LINE_NUM` when '2222' then `csv`.`LINE_NUM` when '2223' then `csv`.`LINE_NUM` when '2224' then `csv`.`LINE_NUM` when '2225' then `csv`.`LINE_NUM` when '2448' then `csv`.`LINE_NUM` when '2226' then `csv`.`LINE_NUM` when '2227' then `csv`.`LINE_NUM` when '2228' then `csv`.`LINE_NUM` when '2229' then `csv`.`LINE_NUM` when '2230' then `csv`.`LINE_NUM` when '2231' then `csv`.`LINE_NUM` when '2232' then `csv`.`LINE_NUM` when '2233' then `csv`.`LINE_NUM` when '2234' then `csv`.`LINE_NUM` when '2262' then `csv`.`LINE_NUM` when '2381' then `csv`.`LINE_NUM` when '2382' then `csv`.`LINE_NUM` when '2258' then `csv`.`LINE_NUM` when '2320' then `csv`.`LINE_NUM` when '2305' then `csv`.`LINE_NUM` when '2395' then `csv`.`LINE_NUM` when '2396' then `csv`.`LINE_NUM` when '2397' then `csv`.`LINE_NUM` when '2468' then `csv`.`LINE_NUM` when '2512' then `csv`.`LINE_NUM` when '2513' then `csv`.`LINE_NUM` when '2514' then `csv`.`LINE_NUM` when '2515' then `csv`.`LINE_NUM` when '2516' then `csv`.`LINE_NUM` when '2517' then `csv`.`LINE_NUM` else 0 end) AS `DATA_PHYSICS`,(case `csv`.`CATE` when '2444' then (`csv`.`LINE_NUM` * 130) when '2263' then (`csv`.`LINE_NUM` * 70) when '2200' then (`csv`.`LINE_NUM` * 40) when '2201' then (`csv`.`LINE_NUM` * 20) when '2202' then (`csv`.`LINE_NUM` * 10) when '2445' then (`csv`.`LINE_NUM` * 200) when '2203' then (`csv`.`LINE_NUM` * 150) when '2204' then (`csv`.`LINE_NUM` * 50) when '2205' then (`csv`.`LINE_NUM` * 20) when '2206' then (`csv`.`LINE_NUM` * 10) when '2446' then (`csv`.`LINE_NUM` * 200) when '2207' then (`csv`.`LINE_NUM` * 150) when '2208' then (`csv`.`LINE_NUM` * 70) when '2209' then (`csv`.`LINE_NUM` * 60) when '2210' then (`csv`.`LINE_NUM` * 20) when '2211' then (`csv`.`LINE_NUM` * 10) when '2212' then (`csv`.`LINE_NUM` * 20) when '2213' then (`csv`.`LINE_NUM` * 200) when '2214' then (`csv`.`LINE_NUM` * 30) when '2264' then (`csv`.`LINE_NUM` * 20) when '2215' then (`csv`.`LINE_NUM` * 10) when '2447' then (`csv`.`LINE_NUM` * 200) when '2217' then (`csv`.`LINE_NUM` * 150) when '2218' then (`csv`.`LINE_NUM` * 80) when '2219' then (`csv`.`LINE_NUM` * 10) when '2220' then (`csv`.`LINE_NUM` * 5) when '2221' then (`csv`.`LINE_NUM` * 5) when '2222' then (`csv`.`LINE_NUM` * 100) when '2223' then (`csv`.`LINE_NUM` * 10) when '2224' then (`csv`.`LINE_NUM` * 5) when '2225' then (`csv`.`LINE_NUM` * 5) when '2448' then (`csv`.`LINE_NUM` * 200) when '2226' then (`csv`.`LINE_NUM` * 150) when '2227' then (`csv`.`LINE_NUM` * 10) when '2228' then (`csv`.`LINE_NUM` * 5) when '2229' then (`csv`.`LINE_NUM` * 1) when '2230' then (`csv`.`LINE_NUM` * 10) when '2231' then (`csv`.`LINE_NUM` * 5) when '2232' then (`csv`.`LINE_NUM` * 1) when '2233' then (`csv`.`LINE_NUM` * 1) when '2234' then (`csv`.`LINE_NUM` * 5) when '2262' then (`csv`.`LINE_NUM` * 1) when '2381' then (`csv`.`LINE_NUM` * 1) when '2382' then (`csv`.`LINE_NUM` * 0.1) when '2258' then (`csv`.`MONTH_NUM` / 10000) when '2320' then (`csv`.`MONTH_NUM` / 10000) when '2305' then (`csv`.`MONTH_NUM` / 10000) when '2395' then (`csv`.`MONTH_NUM` / 10000) when '2396' then (`csv`.`MONTH_NUM` / 10000) when '2397' then (`csv`.`MONTH_NUM` / 10000) when '2468' then (`csv`.`LINE_NUM` * 5) when '2512' then (`csv`.`LINE_NUM` * 35) when '2513' then (`csv`.`LINE_NUM` * 9) when '2514' then (`csv`.`LINE_NUM` * 2) when '2515' then (`csv`.`LINE_NUM` * 8) when '2516' then (`csv`.`LINE_NUM` * 3) when '2517' then (`csv`.`LINE_NUM` * 1) else 0 end) AS `DATA_CHANGE`,(case `csv`.`CATE` when '2444' then `csv`.`LINE_NUM` when '2263' then `csv`.`LINE_NUM` when '2200' then `csv`.`LINE_NUM` when '2201' then `csv`.`LINE_NUM` when '2202' then `csv`.`LINE_NUM` when '2445' then `csv`.`LINE_NUM` when '2203' then `csv`.`LINE_NUM` when '2204' then `csv`.`LINE_NUM` when '2205' then `csv`.`LINE_NUM` when '2206' then `csv`.`LINE_NUM` when '2446' then `csv`.`LINE_NUM` when '2207' then `csv`.`LINE_NUM` when '2208' then `csv`.`LINE_NUM` when '2209' then `csv`.`LINE_NUM` when '2210' then `csv`.`LINE_NUM` when '2211' then `csv`.`LINE_NUM` when '2212' then `csv`.`LINE_NUM` when '2213' then `csv`.`LINE_NUM` when '2214' then `csv`.`LINE_NUM` when '2264' then `csv`.`LINE_NUM` when '2215' then `csv`.`LINE_NUM` else 0 end) AS `DATA_GET_PHYSICS`,(case `csv`.`CATE` when '2444' then (`csv`.`LINE_NUM` * 130) when '2263' then (`csv`.`LINE_NUM` * 70) when '2200' then (`csv`.`LINE_NUM` * 40) when '2201' then (`csv`.`LINE_NUM` * 20) when '2202' then (`csv`.`LINE_NUM` * 10) when '2445' then (`csv`.`LINE_NUM` * 200) when '2203' then (`csv`.`LINE_NUM` * 150) when '2204' then (`csv`.`LINE_NUM` * 50) when '2205' then (`csv`.`LINE_NUM` * 20) when '2206' then (`csv`.`LINE_NUM` * 10) when '2446' then (`csv`.`LINE_NUM` * 200) when '2207' then (`csv`.`LINE_NUM` * 150) when '2208' then (`csv`.`LINE_NUM` * 70) when '2209' then (`csv`.`LINE_NUM` * 60) when '2210' then (`csv`.`LINE_NUM` * 20) when '2211' then (`csv`.`LINE_NUM` * 10) when '2212' then (`csv`.`LINE_NUM` * 20) when '2213' then (`csv`.`LINE_NUM` * 200) when '2214' then (`csv`.`LINE_NUM` * 30) when '2264' then (`csv`.`LINE_NUM` * 20) when '2215' then (`csv`.`LINE_NUM` * 10) else 0 end) AS `DATA_GET_CHANGE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` when '2143' then `csv`.`LINE_NUM` when '2183' then `csv`.`LINE_NUM` when '2184' then `csv`.`LINE_NUM` when '2240' then `csv`.`LINE_NUM` when '2241' then `csv`.`LINE_NUM` when '2242' then `csv`.`LINE_NUM` when '2243' then `csv`.`LINE_NUM` when '2244' then `csv`.`LINE_NUM` when '2278' then `csv`.`LINE_NUM` when '2279' then `csv`.`LINE_NUM` when '2280' then `csv`.`LINE_NUM` when '2438' then `csv`.`LINE_NUM` when '2439' then `csv`.`LINE_NUM` when '2440' then `csv`.`LINE_NUM` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `MOBILE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` else 0 end) AS `HS_SALE_END`,(case `csv`.`CATE` when '2143' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_SALE_END`,(case `csv`.`CATE` when '2241' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_SALE_END`,(case `csv`.`CATE` when '2240' then `csv`.`LINE_NUM` else 0 end) AS `HS_ALLOTMENT`,(case `csv`.`CATE` when '2244' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_ALLOTMENT`,(case `csv`.`CATE` when '2242' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_ALLOTMENT`,(case `csv`.`CATE` when '2183' then `csv`.`LINE_NUM` else 0 end) AS `HS_RENTAL`,(case `csv`.`CATE` when '2184' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD_RENTAL`,(case `csv`.`CATE` when '2243' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_RENTAL`,(case `csv`.`CATE` when '2265' then `csv`.`LINE_NUM` when '2268' then `csv`.`LINE_NUM` when '2269' then `csv`.`LINE_NUM` when '2287' then `csv`.`LINE_NUM` when '2350' then `csv`.`LINE_NUM` when '2351' then `csv`.`LINE_NUM` when '2352' then `csv`.`LINE_NUM` when '2353' then `csv`.`LINE_NUM` when '2354' then `csv`.`LINE_NUM` when '2355' then `csv`.`LINE_NUM` when '2356' then `csv`.`LINE_NUM` when '2357' then `csv`.`LINE_NUM` when '2358' then `csv`.`LINE_NUM` when '2361' then `csv`.`LINE_NUM` when '2362' then `csv`.`LINE_NUM` when '2363' then `csv`.`LINE_NUM` when '2364' then `csv`.`LINE_NUM` when '2365' then `csv`.`LINE_NUM` when '2366' then `csv`.`LINE_NUM` when '2367' then `csv`.`LINE_NUM` else 0 end) AS `PHS`,(case `csv`.`CATE` when '2191' then `csv`.`LINE_NUM` else 0 end) AS `WHITE_OFFERS`,(case `csv`.`CATE` when '2193' then `csv`.`LINE_NUM` else 0 end) AS `MODEL_CHANGE`,(case `csv`.`CATE` when '1115' then `csv`.`LINE_NUM` else 0 end) AS `ML`,(case `csv`.`CATE` when '2267' then `csv`.`LINE_NUM` when '2413' then `csv`.`LINE_NUM` when '2302' then `csv`.`LINE_NUM` when '2303' then `csv`.`LINE_NUM` when '2304' then `csv`.`LINE_NUM` when '2309' then `csv`.`LINE_NUM` when '2398' then `csv`.`LINE_NUM` when '2310' then `csv`.`LINE_NUM` when '2311' then `csv`.`LINE_NUM` when '2312' then `csv`.`LINE_NUM` when '2313' then `csv`.`LINE_NUM` when '2314' then `csv`.`LINE_NUM` when '2456' then `csv`.`LINE_NUM` when '2457' then `csv`.`LINE_NUM` when '2458' then `csv`.`LINE_NUM` when '2459' then `csv`.`LINE_NUM` when '2460' then `csv`.`LINE_NUM` when '2461' then `csv`.`LINE_NUM` when '2404' then `csv`.`LINE_NUM` when '2401' then `csv`.`LINE_NUM` when '2402' then `csv`.`LINE_NUM` when '2403' then `csv`.`LINE_NUM` when '2285' then `csv`.`LINE_NUM` when '2198' then `csv`.`LINE_NUM` when '2296' then `csv`.`LINE_NUM` when '2297' then `csv`.`LINE_NUM` when '2298' then `csv`.`LINE_NUM` when '2299' then `csv`.`LINE_NUM` when '2300' then `csv`.`LINE_NUM` when '2301' then `csv`.`LINE_NUM` when '2271' then `csv`.`LINE_NUM` when '2282' then `csv`.`LINE_NUM` when '2199' then `csv`.`LINE_NUM` when '2246' then `csv`.`LINE_NUM` when '2380' then `csv`.`LINE_NUM` when '2276' then `csv`.`LINE_NUM` when '2288' then `csv`.`LINE_NUM` when '2289' then `csv`.`LINE_NUM` when '2405' then `csv`.`LINE_NUM` when '2406' then `csv`.`LINE_NUM` when '2407' then `csv`.`LINE_NUM` when '2408' then `csv`.`LINE_NUM` when '2409' then `csv`.`LINE_NUM` when '2293' then `csv`.`LINE_NUM` when '2391' then `csv`.`LINE_NUM` when '2359' then `csv`.`LINE_NUM` when '2360' then `csv`.`LINE_NUM` when '2400' then `csv`.`LINE_NUM` when '2295' then `csv`.`LINE_NUM` when '2368' then `csv`.`LINE_NUM` when '2369' then `csv`.`LINE_NUM` when '2518' then `csv`.`LINE_NUM` when '2247' then `csv`.`LINE_NUM` when '2248' then `csv`.`LINE_NUM` when '2249' then `csv`.`LINE_NUM` when '2250' then `csv`.`LINE_NUM` when '2251' then `csv`.`LINE_NUM` when '2252' then `csv`.`LINE_NUM` when '2253' then `csv`.`LINE_NUM` when '2383' then `csv`.`LINE_NUM` when '2384' then `csv`.`LINE_NUM` when '2385' then `csv`.`LINE_NUM` when '2386' then `csv`.`LINE_NUM` when '2387' then `csv`.`LINE_NUM` when '2388' then `csv`.`LINE_NUM` when '2389' then `csv`.`LINE_NUM` when '2390' then `csv`.`LINE_NUM` when '2286' then `csv`.`LINE_NUM` when '2492' then `csv`.`LINE_NUM` when '2493' then `csv`.`LINE_NUM` when '2494' then `csv`.`LINE_NUM` when '2495' then `csv`.`LINE_NUM` when '2496' then `csv`.`LINE_NUM` when '2497' then `csv`.`LINE_NUM` when '2255' then `csv`.`LINE_NUM` when '2260' then `csv`.`LINE_NUM` when '2261' then `csv`.`LINE_NUM` when '2315' then `csv`.`LINE_NUM` when '2316' then `csv`.`LINE_NUM` when '2317' then `csv`.`LINE_NUM` when '2318' then `csv`.`LINE_NUM` when '2319' then `csv`.`LINE_NUM` when '2256' then `csv`.`LINE_NUM` when '2277' then `csv`.`LINE_NUM` when '2510' then `csv`.`LINE_NUM` when '2511' then `csv`.`LINE_NUM` when '2502' then `csv`.`LINE_NUM` when '2283' then `csv`.`LINE_NUM` when '2284' then `csv`.`LINE_NUM` when '2273' then `csv`.`LINE_NUM` when '2415' then `csv`.`LINE_NUM` when '2416' then `csv`.`LINE_NUM` when '2417' then `csv`.`LINE_NUM` when '2414' then `csv`.`LINE_NUM` when '2467' then `csv`.`LINE_NUM` when '2484' then `csv`.`LINE_NUM` when '2321' then `csv`.`LINE_NUM` when '2322' then `csv`.`LINE_NUM` when '2323' then `csv`.`LINE_NUM` when '2324' then `csv`.`LINE_NUM` when '2325' then `csv`.`LINE_NUM` when '2326' then `csv`.`LINE_NUM` when '2327' then `csv`.`LINE_NUM` when '2328' then `csv`.`LINE_NUM` when '2329' then `csv`.`LINE_NUM` when '2330' then `csv`.`LINE_NUM` when '2331' then `csv`.`LINE_NUM` when '2332' then `csv`.`LINE_NUM` when '2333' then `csv`.`LINE_NUM` when '2334' then `csv`.`LINE_NUM` when '2335' then `csv`.`LINE_NUM` when '2336' then `csv`.`LINE_NUM` when '2337' then `csv`.`LINE_NUM` when '2338' then `csv`.`LINE_NUM` when '2339' then `csv`.`LINE_NUM` when '2340' then `csv`.`LINE_NUM` when '2341' then `csv`.`LINE_NUM` when '2370' then `csv`.`LINE_NUM` when '2371' then `csv`.`LINE_NUM` when '2372' then `csv`.`LINE_NUM` when '2373' then `csv`.`LINE_NUM` when '2374' then `csv`.`LINE_NUM` when '2375' then `csv`.`LINE_NUM` when '2376' then `csv`.`LINE_NUM` when '2377' then `csv`.`LINE_NUM` when '2378' then `csv`.`LINE_NUM` when '2379' then `csv`.`LINE_NUM` when '2392' then `csv`.`LINE_NUM` when '2393' then `csv`.`LINE_NUM` when '2394' then `csv`.`LINE_NUM` when '2410' then `csv`.`LINE_NUM` when '2411' then `csv`.`LINE_NUM` when '2412' then `csv`.`LINE_NUM` when '2469' then `csv`.`LINE_NUM` when '2470' then `csv`.`LINE_NUM` when '2471' then `csv`.`LINE_NUM` when '2472' then `csv`.`LINE_NUM` when '2473' then `csv`.`LINE_NUM` when '2474' then `csv`.`LINE_NUM` when '2475' then `csv`.`LINE_NUM` when '2476' then `csv`.`LINE_NUM` when '2486' then `csv`.`LINE_NUM` when '2499' then `csv`.`LINE_NUM` when '2522' then `csv`.`LINE_NUM` when '2523' then `csv`.`LINE_NUM` when '2524' then `csv`.`LINE_NUM` when '2525' then `csv`.`LINE_NUM` when '2526' then `csv`.`LINE_NUM` when '2527' then `csv`.`LINE_NUM` when '2528' then `csv`.`LINE_NUM` when '2529' then `csv`.`LINE_NUM` when '2530' then `csv`.`LINE_NUM` when '2531' then `csv`.`LINE_NUM` when '2532' then `csv`.`LINE_NUM` when '2418' then `csv`.`LINE_NUM` when '2419' then `csv`.`LINE_NUM` when '2420' then `csv`.`LINE_NUM` when '2421' then `csv`.`LINE_NUM` when '2422' then `csv`.`LINE_NUM` when '2423' then `csv`.`LINE_NUM` when '2477' then `csv`.`LINE_NUM` when '2478' then `csv`.`LINE_NUM` when '2479' then `csv`.`LINE_NUM` when '2480' then `csv`.`LINE_NUM` when '2481' then `csv`.`LINE_NUM` when '2482' then `csv`.`LINE_NUM` when '2483' then `csv`.`LINE_NUM` when '2533' then `csv`.`LINE_NUM` when '2534' then `csv`.`LINE_NUM` when '2535' then `csv`.`LINE_NUM` when '2536' then `csv`.`LINE_NUM` when '2551' then `csv`.`LINE_NUM` when '2552' then `csv`.`LINE_NUM` when '2553' then `csv`.`LINE_NUM` when '2554' then `csv`.`LINE_NUM` when '2555' then `csv`.`LINE_NUM` when '2556' then `csv`.`LINE_NUM` when '2557' then `csv`.`LINE_NUM` when '2558' then `csv`.`LINE_NUM` when '2559' then `csv`.`LINE_NUM` when '2560' then `csv`.`LINE_NUM` when '2561' then `csv`.`LINE_NUM` when '2570' then `csv`.`LINE_NUM` when '2571' then `csv`.`LINE_NUM` when '2572' then `csv`.`LINE_NUM` when '2573' then `csv`.`LINE_NUM` when '2574' then `csv`.`LINE_NUM` when '2575' then `csv`.`LINE_NUM` when '2576' then `csv`.`LINE_NUM` when '2577' then `csv`.`LINE_NUM` when '2578' then `csv`.`LINE_NUM` when '2579' then `csv`.`LINE_NUM` when '2580' then `csv`.`LINE_NUM` when '2581' then `csv`.`LINE_NUM` when '2582' then `csv`.`LINE_NUM` when '2583' then `csv`.`LINE_NUM` when '2584' then `csv`.`LINE_NUM` when '2585' then `csv`.`LINE_NUM` when '2586' then `csv`.`LINE_NUM` when '2587' then `csv`.`LINE_NUM` else 0 end) AS `CLOUD_PHYSICS`,(case `csv`.`CATE` when '2267' then (`csv`.`LINE_NUM` * 0.1) when '2413' then (`csv`.`LINE_NUM` * 0.5) when '2302' then (`csv`.`LINE_NUM` * 3) when '2303' then (`csv`.`LINE_NUM` * 5) when '2304' then (`csv`.`LINE_NUM` * 7) when '2309' then (`csv`.`LINE_NUM` * 14) when '2398' then (`csv`.`LINE_NUM` * 8) when '2310' then (`csv`.`LINE_NUM` * 16) when '2311' then (`csv`.`LINE_NUM` * 38) when '2312' then (`csv`.`LINE_NUM` * 105) when '2313' then (`csv`.`LINE_NUM` * 2) when '2314' then (`csv`.`LINE_NUM` * 10) when '2456' then (`csv`.`LINE_NUM` * 14) when '2457' then (`csv`.`LINE_NUM` * 8) when '2458' then (`csv`.`LINE_NUM` * 16) when '2459' then (`csv`.`LINE_NUM` * 38) when '2460' then (`csv`.`LINE_NUM` * 105) when '2461' then (`csv`.`LINE_NUM` * 2) when '2404' then (`csv`.`LINE_NUM` * 1) when '2401' then (`csv`.`LINE_NUM` * 2) when '2402' then (`csv`.`LINE_NUM` * 3) when '2403' then (`csv`.`LINE_NUM` * 4) when '2285' then (`csv`.`LINE_NUM` * 7.5) when '2198' then (`csv`.`LINE_NUM` * 1) when '2296' then (`csv`.`LINE_NUM` * 3) when '2297' then (`csv`.`LINE_NUM` * 8) when '2298' then (`csv`.`LINE_NUM` * 15) when '2299' then (`csv`.`LINE_NUM` * 30) when '2300' then (`csv`.`LINE_NUM` * 85) when '2301' then (`csv`.`LINE_NUM` * 100) when '2271' then (`csv`.`LINE_NUM` * 12) when '2282' then (`csv`.`LINE_NUM` * 2) when '2199' then (`csv`.`LINE_NUM` * 0.02) when '2246' then (`csv`.`LINE_NUM` * 20) when '2380' then (`csv`.`LINE_NUM` * 1.8) when '2276' then (`csv`.`LINE_NUM` * 0.2) when '2288' then (`csv`.`LINE_NUM` * 0.5) when '2289' then (`csv`.`LINE_NUM` * 0.8) when '2405' then (`csv`.`LINE_NUM` * 4) when '2406' then (`csv`.`LINE_NUM` * 9) when '2407' then (`csv`.`LINE_NUM` * 12) when '2408' then (`csv`.`LINE_NUM` * 26) when '2409' then (`csv`.`LINE_NUM` * 34) when '2293' then (`csv`.`LINE_NUM` * 5) when '2391' then (`csv`.`LINE_NUM` * 0.6) when '2359' then (`csv`.`LINE_NUM` * 0.3) when '2360' then (`csv`.`LINE_NUM` * 0.7) when '2400' then (`csv`.`LINE_NUM` * 0.1) when '2295' then (`csv`.`LINE_NUM` * 8) when '2368' then (`csv`.`LINE_NUM` * 0.02) when '2369' then (`csv`.`LINE_NUM` * 0.06) when '2518' then (`csv`.`LINE_NUM` * 126) when '2247' then (`csv`.`LINE_NUM` * 6) when '2248' then (`csv`.`LINE_NUM` * 20) when '2249' then (`csv`.`LINE_NUM` * 25) when '2250' then (`csv`.`LINE_NUM` * 20) when '2251' then (`csv`.`LINE_NUM` * 25) when '2252' then (`csv`.`LINE_NUM` * 10) when '2253' then (`csv`.`LINE_NUM` * 20) when '2383' then (`csv`.`LINE_NUM` * 6) when '2384' then (`csv`.`LINE_NUM` * 20) when '2385' then (`csv`.`LINE_NUM` * 61) when '2386' then (`csv`.`LINE_NUM` * 125) when '2387' then (`csv`.`LINE_NUM` * 150) when '2388' then (`csv`.`LINE_NUM` * 47) when '2389' then (`csv`.`LINE_NUM` * 128) when '2390' then (`csv`.`LINE_NUM` * 150) when '2286' then (`csv`.`LINE_NUM` * 0.06) when '2492' then (`csv`.`LINE_NUM` * 10) when '2493' then (`csv`.`LINE_NUM` * 30) when '2494' then (`csv`.`LINE_NUM` * 50) when '2495' then (`csv`.`LINE_NUM` * 40) when '2496' then (`csv`.`LINE_NUM` * 60) when '2497' then (`csv`.`LINE_NUM` * 80) when '2255' then (`csv`.`LINE_NUM` * 35) when '2260' then (`csv`.`LINE_NUM` * 17.5) when '2261' then (`csv`.`LINE_NUM` * 8.75) when '2315' then (`csv`.`LINE_NUM` * 7) when '2316' then (`csv`.`LINE_NUM` * 20) when '2317' then (`csv`.`LINE_NUM` * 30) when '2318' then (`csv`.`LINE_NUM` * 70) when '2319' then (`csv`.`LINE_NUM` * 150) when '2256' then (`csv`.`LINE_NUM` * 6) when '2277' then (`csv`.`LINE_NUM` * 0.1) when '2510' then (`csv`.`LINE_NUM` * 0.1) when '2511' then (`csv`.`LINE_NUM` * 0.1) when '2502' then (`csv`.`LINE_NUM` * 0.02) when '2283' then (`csv`.`LINE_NUM` * 3) when '2284' then (`csv`.`LINE_NUM` * 2) when '2273' then (`csv`.`LINE_NUM` * 1) when '2415' then (`csv`.`LINE_NUM` * 1) when '2416' then (`csv`.`LINE_NUM` * 6) when '2417' then (`csv`.`LINE_NUM` * 10) when '2414' then (`csv`.`LINE_NUM` * 0.2) when '2467' then (`csv`.`LINE_NUM` * 0.1) when '2484' then (`csv`.`LINE_NUM` * 14) when '2321' then (`csv`.`LINE_NUM` * 3) when '2322' then (`csv`.`LINE_NUM` * 10) when '2323' then (`csv`.`LINE_NUM` * 10) when '2324' then (`csv`.`LINE_NUM` * 3) when '2325' then (`csv`.`LINE_NUM` * 3) when '2326' then (`csv`.`LINE_NUM` * 3) when '2327' then (`csv`.`LINE_NUM` * 3) when '2328' then (`csv`.`LINE_NUM` * 3) when '2329' then (`csv`.`LINE_NUM` * 3) when '2330' then (`csv`.`LINE_NUM` * 3) when '2331' then (`csv`.`LINE_NUM` * 3) when '2332' then (`csv`.`LINE_NUM` * 3) when '2333' then (`csv`.`LINE_NUM` * 3) when '2334' then (`csv`.`LINE_NUM` * 3) when '2335' then (`csv`.`LINE_NUM` * 1) when '2336' then (`csv`.`LINE_NUM` * 1) when '2337' then (`csv`.`LINE_NUM` * 1) when '2338' then (`csv`.`LINE_NUM` * 1) when '2339' then (`csv`.`LINE_NUM` * 1) when '2340' then (`csv`.`LINE_NUM` * 1) when '2341' then (`csv`.`LINE_NUM` * 1) when '2370' then (`csv`.`LINE_NUM` * 10) when '2371' then (`csv`.`LINE_NUM` * 10) when '2372' then (`csv`.`LINE_NUM` * 3) when '2373' then (`csv`.`LINE_NUM` * 3) when '2374' then (`csv`.`LINE_NUM` * 3) when '2375' then (`csv`.`LINE_NUM` * 3) when '2376' then (`csv`.`LINE_NUM` * 3) when '2377' then (`csv`.`LINE_NUM` * 3) when '2378' then (`csv`.`LINE_NUM` * 3) when '2379' then (`csv`.`LINE_NUM` * 1) when '2392' then (`csv`.`LINE_NUM` * 3) when '2393' then (`csv`.`LINE_NUM` * 3) when '2394' then (`csv`.`LINE_NUM` * 1) when '2410' then (`csv`.`LINE_NUM` * 3) when '2411' then (`csv`.`LINE_NUM` * 3) when '2412' then (`csv`.`LINE_NUM` * 1) when '2469' then (`csv`.`LINE_NUM` * 10) when '2470' then (`csv`.`LINE_NUM` * 3) when '2471' then (`csv`.`LINE_NUM` * 1) when '2472' then (`csv`.`LINE_NUM` * 3) when '2473' then (`csv`.`LINE_NUM` * 3) when '2474' then (`csv`.`LINE_NUM` * 1) when '2475' then (`csv`.`LINE_NUM` * 1) when '2476' then (`csv`.`LINE_NUM` * 1) when '2486' then (`csv`.`LINE_NUM` * 3) when '2499' then (`csv`.`LINE_NUM` * 1) when '2522' then (`csv`.`LINE_NUM` * 10) when '2523' then (`csv`.`LINE_NUM` * 3) when '2524' then (`csv`.`LINE_NUM` * 3) when '2525' then (`csv`.`LINE_NUM` * 3) when '2526' then (`csv`.`LINE_NUM` * 3) when '2527' then (`csv`.`LINE_NUM` * 3) when '2528' then (`csv`.`LINE_NUM` * 3) when '2529' then (`csv`.`LINE_NUM` * 1) when '2530' then (`csv`.`LINE_NUM` * 1) when '2531' then (`csv`.`LINE_NUM` * 1) when '2532' then (`csv`.`LINE_NUM` * 1) when '2418' then (`csv`.`LINE_NUM` * 3) when '2419' then (`csv`.`LINE_NUM` * 1) when '2420' then (`csv`.`LINE_NUM` * 1) when '2421' then (`csv`.`LINE_NUM` * 1) when '2422' then (`csv`.`LINE_NUM` * 1) when '2423' then (`csv`.`LINE_NUM` * 1) when '2477' then (`csv`.`LINE_NUM` * 3) when '2478' then (`csv`.`LINE_NUM` * 3) when '2479' then (`csv`.`LINE_NUM` * 3) when '2480' then (`csv`.`LINE_NUM` * 3) when '2481' then (`csv`.`LINE_NUM` * 3) when '2482' then (`csv`.`LINE_NUM` * 1) when '2483' then (`csv`.`LINE_NUM` * 1) when '2533' then (`csv`.`LINE_NUM` * 3) when '2534' then (`csv`.`LINE_NUM` * 8) when '2535' then (`csv`.`LINE_NUM` * 5) when '2536' then (`csv`.`LINE_NUM` * 80) when '2551' then (`csv`.`LINE_NUM` * 1) when '2552' then (`csv`.`LINE_NUM` * 1) when '2553' then (`csv`.`LINE_NUM` * 3) when '2554' then (`csv`.`LINE_NUM` * 5) when '2555' then (`csv`.`LINE_NUM` * 8) when '2556' then (`csv`.`LINE_NUM` * 13) when '2557' then (`csv`.`LINE_NUM` * 19) when '2558' then (`csv`.`LINE_NUM` * 25) when '2559' then (`csv`.`LINE_NUM` * 44) when '2560' then (`csv`.`LINE_NUM` * 6) when '2561' then (`csv`.`LINE_NUM` * 14) when '2570' then (`csv`.`LINE_NUM` * 14) when '2571' then (`csv`.`LINE_NUM` * 17) when '2572' then (`csv`.`LINE_NUM` * 24) when '2573' then (`csv`.`LINE_NUM` * 31) when '2574' then (`csv`.`LINE_NUM` * 38) when '2575' then (`csv`.`LINE_NUM` * 45) when '2576' then (`csv`.`LINE_NUM` * 52) when '2577' then (`csv`.`LINE_NUM` * 59) when '2578' then (`csv`.`LINE_NUM` * 67) when '2579' then (`csv`.`LINE_NUM` * 74) when '2580' then (`csv`.`LINE_NUM` * 81) when '2581' then (`csv`.`LINE_NUM` * 7) when '2582' then (`csv`.`LINE_NUM` * 2) when '2583' then (`csv`.`LINE_NUM` * 4) when '2584' then (`csv`.`LINE_NUM` * 6) when '2585' then (`csv`.`LINE_NUM` * 2) when '2586' then (`csv`.`LINE_NUM` * 4) when '2587' then (`csv`.`LINE_NUM` * 6) else 0 end) AS `CLOUD_CHANGE`,(case `csv`.`CATE` when '2449' then `csv`.`LINE_NUM` when '2450' then `csv`.`LINE_NUM` when '2451' then `csv`.`LINE_NUM` when '2453' then `csv`.`LINE_NUM` when '2435' then `csv`.`LINE_NUM` when '2487' then `csv`.`LINE_NUM` when '2488' then `csv`.`LINE_NUM` when '2489' then `csv`.`LINE_NUM` when '2490' then `csv`.`LINE_NUM` when '2491' then `csv`.`LINE_NUM` when '2436' then `csv`.`LINE_NUM` when '2437' then `csv`.`LINE_NUM` when '2504' then `csv`.`LINE_NUM` when '2505' then `csv`.`LINE_NUM` when '2506' then `csv`.`LINE_NUM` when '2507' then `csv`.`LINE_NUM` when '2508' then `csv`.`LINE_NUM` when '2509' then `csv`.`LINE_NUM` when '2500' then `csv`.`LINE_NUM` when '2501' then `csv`.`LINE_NUM` when '2562' then `csv`.`LINE_NUM` when '2563' then `csv`.`LINE_NUM` when '2564' then `csv`.`LINE_NUM` when '2565' then `csv`.`LINE_NUM` when '2566' then `csv`.`LINE_NUM` when '2567' then `csv`.`LINE_NUM` when '2568' then `csv`.`LINE_NUM` when '2569' then `csv`.`LINE_NUM` else 0 end) AS `YAHOO_PHYSICS`,(case `csv`.`CATE` when '2449' then (`csv`.`LINE_NUM` * 4) when '2450' then (`csv`.`LINE_NUM` * 7) when '2451' then (`csv`.`LINE_NUM` * 9) when '2453' then (`csv`.`MONTH_NUM` * 0.000023) when '2435' then (`csv`.`TEMP_NUM` * 0.000003125) when '2487' then (`csv`.`TEMP_NUM` * 0.000003125) when '2488' then (`csv`.`TEMP_NUM` * 0.000003125) when '2489' then (`csv`.`TEMP_NUM` * 0.000003125) when '2490' then (`csv`.`TEMP_NUM` * 0.000003125) when '2491' then (`csv`.`TEMP_NUM` * 0.000003125) when '2436' then (`csv`.`LINE_NUM` * 0) when '2437' then (`csv`.`LINE_NUM` * 0) when '2504' then (`csv`.`LINE_NUM` * 4) when '2505' then (`csv`.`LINE_NUM` * 7) when '2506' then (`csv`.`LINE_NUM` * 9) when '2508' then (`csv`.`MONTH_NUM` * 0.000023) when '2509' then (`csv`.`LINE_NUM` * 1) when '2500' then (`csv`.`TEMP_NUM` * 0.00001042) when '2501' then (`csv`.`LINE_NUM` * 3) when '2562' then (`csv`.`LINE_NUM` * 4) when '2563' then (`csv`.`LINE_NUM` * 7) when '2564' then (`csv`.`LINE_NUM` * 9) when '2565' then (`csv`.`MONTH_NUM` * 0.000023) when '2566' then (`csv`.`LINE_NUM` * 4) when '2567' then (`csv`.`LINE_NUM` * 7) when '2568' then (`csv`.`LINE_NUM` * 9) when '2569' then (`csv`.`MONTH_NUM` * 0.000023) else 0 end) AS `YAHOO_CHANGE`,(case `csv`.`CATE` when '2142' then `csv`.`LINE_NUM` when '2240' then `csv`.`LINE_NUM` when '2183' then `csv`.`LINE_NUM` else 0 end) AS `HS`,(case `csv`.`CATE` when '2438' then `csv`.`LINE_NUM` when '2439' then `csv`.`LINE_NUM` when '2440' then `csv`.`LINE_NUM` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `SMART_PHONE`,(case `csv`.`CATE` when '2143' then `csv`.`LINE_NUM` when '2244' then `csv`.`LINE_NUM` when '2184' then `csv`.`LINE_NUM` else 0 end) AS `DATA_CARD`,(case `csv`.`CATE` when '2441' then `csv`.`LINE_NUM` when '2442' then `csv`.`LINE_NUM` when '2443' then `csv`.`LINE_NUM` else 0 end) AS `IPAD`,(case `csv`.`CATE` when '2267' then `csv`.`LINE_NUM` else 0 end) AS `GOOGLEAPPS`,(case `csv`.`CATE` when '2500' then (((`csv`.`TEMP_NUM` * 0.50) / 4000) / 12) when '2746' then (((`csv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2747' then (((`csv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2748' then (((`csv`.`TEMP_NUM` * 0.20) / 4000) / 12) when '2749' then (((`csv`.`TEMP_NUM` * 0.15) / 4000) / 12) when '2751' then (((`csv`.`TEMP_NUM` * 0.40) / 4000) / 12) else 0 end) AS `O2O`,(case `csv`.`CATE` when '2467' then (`csv`.`LINE_NUM` * 0.01) else 0 end) AS `PAYPAL` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_cancel_opportunities` `opc` on((`opp`.`ID` = `opc`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_cancel` `csv` on((`opc`.`SV_CANCEL_IDA` = `csv`.`ID`))) join `ifview_eida_mast_serviceline` `msv` on(((`csv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp2_fy10_del_test`
--

/*!50001 DROP TABLE IF EXISTS `view_opp2_fy10_del_test`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp2_fy10_del_test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp2_fy10_del_test` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`opc`.`DELETED` = 0),`csv`.`DELETED`,`opc`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`csv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`csv`.`YYYY`,'/',`csv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`csv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`csv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`csv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`csv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) else 0 end) AS `SV_NUM`,`csv`.`MONTH_NUM` AS `MONTH_NUM`,`csv`.`TEMP_NUM` AS `TEMP_NUM`,`csv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'解約' AS `SV_TYPE`,`csv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,`opp`.`OP_DEPEND_C` AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`csv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`csv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`csv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`csv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`csv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`csv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`csv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`csv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_cancel_opportunities` `opc` on((`opp`.`ID` = `opc`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_cancel` `csv` on((`opc`.`SV_CANCEL_IDA` = `csv`.`ID`))) join `ifview_eida_mast_serviceline` `msv` on(((`csv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`sbtm_tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp_fy10`
--

/*!50001 DROP TABLE IF EXISTS `view_opp_fy10`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp_fy10` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`ops`.`DELETED` = 0),`ssv`.`DELETED`,`ops`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`ssv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`ssv`.`YYYY`,'/',`ssv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`ssv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) when '6' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.3) / 12) / 4000),2) when '7' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.35) / 12) / 4000),2) when '8' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.125) / 12) / 4000),2) when '9' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.2) / 12) / 4000),2) when '10' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.4) / 12) / 4000),2) when '11' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 1.0) / 12) / 4000),2) when '12' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * 0.2) / 4000),2) when '13' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 12) / 4000),2) when '14' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 4000),2) else 0 end) AS `SV_NUM`,`ssv`.`MONTH_NUM` AS `MONTH_NUM`,`ssv`.`TEMP_NUM` AS `TEMP_NUM`,`ssv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'提案' AS `SV_TYPE`,`ssv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,left(`opp`.`OP_DEPEND_C`,40) AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`ssv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`ssv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`ssv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`ssv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`ssv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`ssv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`ssv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`ssv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO`,date_format(`ssv`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `SV_DATE_MODIFIED_FMT`,`es`.`div1_nm` AS `MAST_ORGANIZATIONUNIT1_NM`,`es`.`div2_nm` AS `MAST_ORGANIZATIONUNIT2_NM`,`es`.`div3_nm` AS `MAST_ORGANIZATIONUNIT3_NM`,`es`.`div4_nm` AS `MAST_ORGANIZATIONUNIT4_NM`,`es`.`div5_nm` AS `MAST_ORGANIZATIONUNIT5_NM`,`es`.`div6_nm` AS `MAST_ORGANIZATIONUNIT6_NM`,left(`opp`.`OP_CONT_PLANDATE_C`,7) AS `OP_CONT_PLANMONTH`,`ssv`.`MONTH_NUM_LOCAL_CURRENCY` AS `MONTH_NUM_LOCAL_CURRENCY`,`ssv`.`MONTH_NUM_UNIT` AS `MONTH_NUM_UNIT`,`ssv`.`TEMP_NUM_LOCAL_CURRENCY` AS `TEMP_NUM_LOCAL_CURRENCY`,`ssv`.`TEMP_NUM_UNIT` AS `TEMP_NUM_UNIT`,(case when (`opp`.`OP_STEP_C` = '50') then '契約・申込' when (`opp`.`OP_STEP_C` = '60') then '失注' when (`opp`.`OP_STEP_C` = '70') then '案件消滅' when (`opp`.`OP_COMMIT_C` = '001') then 'ベース' when (`opp`.`OP_COMMIT_C` = '002') then 'アグレッシブ' when (`opp`.`OP_COMMIT_C` = '003') then 'チャレンジ' else '' end) AS `OP_MANAGE_FORECAST`,(case `ssv`.`CATE` when '1106' then `ssv`.`LINE_NUM` when '2464' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI`,(case `ssv`.`CATE` when '1107' then `ssv`.`LINE_NUM` when '2463' then `ssv`.`LINE_NUM` when '1108' then `ssv`.`LINE_NUM` when '2462' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_BRI`,(case `ssv`.`CATE` when '1106' then `ssv`.`LINE_NUM` when '2464' then `ssv`.`LINE_NUM` when '1107' then `ssv`.`LINE_NUM` when '2463' then `ssv`.`LINE_NUM` when '1108' then `ssv`.`LINE_NUM` when '2462' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI1`,(case `ssv`.`CATE` when '1106' then (`ssv`.`LINE_NUM` * 20) when '2464' then (`ssv`.`LINE_NUM` * 30) when '1107' then `ssv`.`LINE_NUM` when '2463' then (`ssv`.`LINE_NUM` * 1.5) when '1108' then `ssv`.`LINE_NUM` when '2462' then (`ssv`.`LINE_NUM` * 1.5) else 0 end) AS `SPECIAL_PRI20`,(case `ssv`.`CATE` when '2444' then `ssv`.`LINE_NUM` when '2263' then `ssv`.`LINE_NUM` when '2200' then `ssv`.`LINE_NUM` when '2201' then `ssv`.`LINE_NUM` when '2202' then `ssv`.`LINE_NUM` when '2445' then `ssv`.`LINE_NUM` when '2203' then `ssv`.`LINE_NUM` when '2204' then `ssv`.`LINE_NUM` when '2205' then `ssv`.`LINE_NUM` when '2206' then `ssv`.`LINE_NUM` when '2446' then `ssv`.`LINE_NUM` when '2207' then `ssv`.`LINE_NUM` when '2208' then `ssv`.`LINE_NUM` when '2209' then `ssv`.`LINE_NUM` when '2210' then `ssv`.`LINE_NUM` when '2211' then `ssv`.`LINE_NUM` when '2212' then `ssv`.`LINE_NUM` when '2213' then `ssv`.`LINE_NUM` when '2214' then `ssv`.`LINE_NUM` when '2264' then `ssv`.`LINE_NUM` when '2215' then `ssv`.`LINE_NUM` when '2447' then `ssv`.`LINE_NUM` when '2217' then `ssv`.`LINE_NUM` when '2218' then `ssv`.`LINE_NUM` when '2219' then `ssv`.`LINE_NUM` when '2220' then `ssv`.`LINE_NUM` when '2221' then `ssv`.`LINE_NUM` when '2222' then `ssv`.`LINE_NUM` when '2223' then `ssv`.`LINE_NUM` when '2224' then `ssv`.`LINE_NUM` when '2225' then `ssv`.`LINE_NUM` when '2448' then `ssv`.`LINE_NUM` when '2226' then `ssv`.`LINE_NUM` when '2227' then `ssv`.`LINE_NUM` when '2228' then `ssv`.`LINE_NUM` when '2229' then `ssv`.`LINE_NUM` when '2230' then `ssv`.`LINE_NUM` when '2231' then `ssv`.`LINE_NUM` when '2232' then `ssv`.`LINE_NUM` when '2233' then `ssv`.`LINE_NUM` when '2234' then `ssv`.`LINE_NUM` when '2262' then `ssv`.`LINE_NUM` when '2381' then `ssv`.`LINE_NUM` when '2382' then `ssv`.`LINE_NUM` when '2258' then `ssv`.`LINE_NUM` when '2320' then `ssv`.`LINE_NUM` when '2305' then `ssv`.`LINE_NUM` when '2395' then `ssv`.`LINE_NUM` when '2396' then `ssv`.`LINE_NUM` when '2397' then `ssv`.`LINE_NUM` when '2468' then `ssv`.`LINE_NUM` when '2512' then `ssv`.`LINE_NUM` when '2513' then `ssv`.`LINE_NUM` when '2514' then `ssv`.`LINE_NUM` when '2515' then `ssv`.`LINE_NUM` when '2516' then `ssv`.`LINE_NUM` when '2517' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_PHYSICS`,(case `ssv`.`CATE` when '2444' then (`ssv`.`LINE_NUM` * 130) when '2263' then (`ssv`.`LINE_NUM` * 70) when '2200' then (`ssv`.`LINE_NUM` * 40) when '2201' then (`ssv`.`LINE_NUM` * 20) when '2202' then (`ssv`.`LINE_NUM` * 10) when '2445' then (`ssv`.`LINE_NUM` * 200) when '2203' then (`ssv`.`LINE_NUM` * 150) when '2204' then (`ssv`.`LINE_NUM` * 50) when '2205' then (`ssv`.`LINE_NUM` * 20) when '2206' then (`ssv`.`LINE_NUM` * 10) when '2446' then (`ssv`.`LINE_NUM` * 200) when '2207' then (`ssv`.`LINE_NUM` * 150) when '2208' then (`ssv`.`LINE_NUM` * 70) when '2209' then (`ssv`.`LINE_NUM` * 60) when '2210' then (`ssv`.`LINE_NUM` * 20) when '2211' then (`ssv`.`LINE_NUM` * 10) when '2212' then (`ssv`.`LINE_NUM` * 20) when '2213' then (`ssv`.`LINE_NUM` * 200) when '2214' then (`ssv`.`LINE_NUM` * 30) when '2264' then (`ssv`.`LINE_NUM` * 20) when '2215' then (`ssv`.`LINE_NUM` * 10) when '2447' then (`ssv`.`LINE_NUM` * 200) when '2217' then (`ssv`.`LINE_NUM` * 150) when '2218' then (`ssv`.`LINE_NUM` * 80) when '2219' then (`ssv`.`LINE_NUM` * 10) when '2220' then (`ssv`.`LINE_NUM` * 5) when '2221' then (`ssv`.`LINE_NUM` * 5) when '2222' then (`ssv`.`LINE_NUM` * 100) when '2223' then (`ssv`.`LINE_NUM` * 10) when '2224' then (`ssv`.`LINE_NUM` * 5) when '2225' then (`ssv`.`LINE_NUM` * 5) when '2448' then (`ssv`.`LINE_NUM` * 200) when '2226' then (`ssv`.`LINE_NUM` * 150) when '2227' then (`ssv`.`LINE_NUM` * 10) when '2228' then (`ssv`.`LINE_NUM` * 5) when '2229' then (`ssv`.`LINE_NUM` * 1) when '2230' then (`ssv`.`LINE_NUM` * 10) when '2231' then (`ssv`.`LINE_NUM` * 5) when '2232' then (`ssv`.`LINE_NUM` * 1) when '2233' then (`ssv`.`LINE_NUM` * 1) when '2234' then (`ssv`.`LINE_NUM` * 5) when '2262' then (`ssv`.`LINE_NUM` * 1) when '2381' then (`ssv`.`LINE_NUM` * 1) when '2382' then (`ssv`.`LINE_NUM` * 0.1) when '2258' then (`ssv`.`MONTH_NUM` / 10000) when '2320' then (`ssv`.`MONTH_NUM` / 10000) when '2305' then (`ssv`.`MONTH_NUM` / 10000) when '2395' then (`ssv`.`MONTH_NUM` / 10000) when '2396' then (`ssv`.`MONTH_NUM` / 10000) when '2397' then (`ssv`.`MONTH_NUM` / 10000) when '2468' then (`ssv`.`LINE_NUM` * 5) when '2512' then (`ssv`.`LINE_NUM` * 35) when '2513' then (`ssv`.`LINE_NUM` * 9) when '2514' then (`ssv`.`LINE_NUM` * 2) when '2515' then (`ssv`.`LINE_NUM` * 8) when '2516' then (`ssv`.`LINE_NUM` * 3) when '2517' then (`ssv`.`LINE_NUM` * 1) else 0 end) AS `DATA_CHANGE`,(case `ssv`.`CATE` when '2444' then `ssv`.`LINE_NUM` when '2263' then `ssv`.`LINE_NUM` when '2200' then `ssv`.`LINE_NUM` when '2201' then `ssv`.`LINE_NUM` when '2202' then `ssv`.`LINE_NUM` when '2445' then `ssv`.`LINE_NUM` when '2203' then `ssv`.`LINE_NUM` when '2204' then `ssv`.`LINE_NUM` when '2205' then `ssv`.`LINE_NUM` when '2206' then `ssv`.`LINE_NUM` when '2446' then `ssv`.`LINE_NUM` when '2207' then `ssv`.`LINE_NUM` when '2208' then `ssv`.`LINE_NUM` when '2209' then `ssv`.`LINE_NUM` when '2210' then `ssv`.`LINE_NUM` when '2211' then `ssv`.`LINE_NUM` when '2212' then `ssv`.`LINE_NUM` when '2213' then `ssv`.`LINE_NUM` when '2214' then `ssv`.`LINE_NUM` when '2264' then `ssv`.`LINE_NUM` when '2215' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_GET_PHYSICS`,(case `ssv`.`CATE` when '2444' then (`ssv`.`LINE_NUM` * 130) when '2263' then (`ssv`.`LINE_NUM` * 70) when '2200' then (`ssv`.`LINE_NUM` * 40) when '2201' then (`ssv`.`LINE_NUM` * 20) when '2202' then (`ssv`.`LINE_NUM` * 10) when '2445' then (`ssv`.`LINE_NUM` * 200) when '2203' then (`ssv`.`LINE_NUM` * 150) when '2204' then (`ssv`.`LINE_NUM` * 50) when '2205' then (`ssv`.`LINE_NUM` * 20) when '2206' then (`ssv`.`LINE_NUM` * 10) when '2446' then (`ssv`.`LINE_NUM` * 200) when '2207' then (`ssv`.`LINE_NUM` * 150) when '2208' then (`ssv`.`LINE_NUM` * 70) when '2209' then (`ssv`.`LINE_NUM` * 60) when '2210' then (`ssv`.`LINE_NUM` * 20) when '2211' then (`ssv`.`LINE_NUM` * 10) when '2212' then (`ssv`.`LINE_NUM` * 20) when '2213' then (`ssv`.`LINE_NUM` * 200) when '2214' then (`ssv`.`LINE_NUM` * 30) when '2264' then (`ssv`.`LINE_NUM` * 20) when '2215' then (`ssv`.`LINE_NUM` * 10) else 0 end) AS `DATA_GET_CHANGE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` when '2143' then `ssv`.`LINE_NUM` when '2183' then `ssv`.`LINE_NUM` when '2184' then `ssv`.`LINE_NUM` when '2240' then `ssv`.`LINE_NUM` when '2241' then `ssv`.`LINE_NUM` when '2242' then `ssv`.`LINE_NUM` when '2243' then `ssv`.`LINE_NUM` when '2244' then `ssv`.`LINE_NUM` when '2278' then `ssv`.`LINE_NUM` when '2279' then `ssv`.`LINE_NUM` when '2280' then `ssv`.`LINE_NUM` when '2438' then `ssv`.`LINE_NUM` when '2439' then `ssv`.`LINE_NUM` when '2440' then `ssv`.`LINE_NUM` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `MOBILE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` else 0 end) AS `HS_SALE_END`,(case `ssv`.`CATE` when '2143' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_SALE_END`,(case `ssv`.`CATE` when '2241' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_SALE_END`,(case `ssv`.`CATE` when '2240' then `ssv`.`LINE_NUM` else 0 end) AS `HS_ALLOTMENT`,(case `ssv`.`CATE` when '2244' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_ALLOTMENT`,(case `ssv`.`CATE` when '2242' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_ALLOTMENT`,(case `ssv`.`CATE` when '2183' then `ssv`.`LINE_NUM` else 0 end) AS `HS_RENTAL`,(case `ssv`.`CATE` when '2184' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_RENTAL`,(case `ssv`.`CATE` when '2243' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_RENTAL`,(case `ssv`.`CATE` when '2265' then `ssv`.`LINE_NUM` when '2268' then `ssv`.`LINE_NUM` when '2269' then `ssv`.`LINE_NUM` when '2287' then `ssv`.`LINE_NUM` when '2350' then `ssv`.`LINE_NUM` when '2351' then `ssv`.`LINE_NUM` when '2352' then `ssv`.`LINE_NUM` when '2353' then `ssv`.`LINE_NUM` when '2354' then `ssv`.`LINE_NUM` when '2355' then `ssv`.`LINE_NUM` when '2356' then `ssv`.`LINE_NUM` when '2357' then `ssv`.`LINE_NUM` when '2358' then `ssv`.`LINE_NUM` when '2361' then `ssv`.`LINE_NUM` when '2362' then `ssv`.`LINE_NUM` when '2363' then `ssv`.`LINE_NUM` when '2364' then `ssv`.`LINE_NUM` when '2365' then `ssv`.`LINE_NUM` when '2366' then `ssv`.`LINE_NUM` when '2367' then `ssv`.`LINE_NUM` else 0 end) AS `PHS`,(case `ssv`.`CATE` when '2191' then `ssv`.`LINE_NUM` else 0 end) AS `WHITE_OFFERS`,(case `ssv`.`CATE` when '2193' then `ssv`.`LINE_NUM` else 0 end) AS `MODEL_CHANGE`,(case `ssv`.`CATE` when '1115' then `ssv`.`LINE_NUM` else 0 end) AS `ML`,(case `ssv`.`CATE` when '2267' then `ssv`.`LINE_NUM` when '2413' then `ssv`.`LINE_NUM` when '2302' then `ssv`.`LINE_NUM` when '2303' then `ssv`.`LINE_NUM` when '2304' then `ssv`.`LINE_NUM` when '2309' then `ssv`.`LINE_NUM` when '2398' then `ssv`.`LINE_NUM` when '2310' then `ssv`.`LINE_NUM` when '2311' then `ssv`.`LINE_NUM` when '2312' then `ssv`.`LINE_NUM` when '2313' then `ssv`.`LINE_NUM` when '2314' then `ssv`.`LINE_NUM` when '2456' then `ssv`.`LINE_NUM` when '2457' then `ssv`.`LINE_NUM` when '2458' then `ssv`.`LINE_NUM` when '2459' then `ssv`.`LINE_NUM` when '2460' then `ssv`.`LINE_NUM` when '2461' then `ssv`.`LINE_NUM` when '2404' then `ssv`.`LINE_NUM` when '2401' then `ssv`.`LINE_NUM` when '2402' then `ssv`.`LINE_NUM` when '2403' then `ssv`.`LINE_NUM` when '2285' then `ssv`.`LINE_NUM` when '2198' then `ssv`.`LINE_NUM` when '2296' then `ssv`.`LINE_NUM` when '2297' then `ssv`.`LINE_NUM` when '2298' then `ssv`.`LINE_NUM` when '2299' then `ssv`.`LINE_NUM` when '2300' then `ssv`.`LINE_NUM` when '2301' then `ssv`.`LINE_NUM` when '2271' then `ssv`.`LINE_NUM` when '2282' then `ssv`.`LINE_NUM` when '2199' then `ssv`.`LINE_NUM` when '2246' then `ssv`.`LINE_NUM` when '2380' then `ssv`.`LINE_NUM` when '2276' then `ssv`.`LINE_NUM` when '2288' then `ssv`.`LINE_NUM` when '2289' then `ssv`.`LINE_NUM` when '2405' then `ssv`.`LINE_NUM` when '2406' then `ssv`.`LINE_NUM` when '2407' then `ssv`.`LINE_NUM` when '2408' then `ssv`.`LINE_NUM` when '2409' then `ssv`.`LINE_NUM` when '2293' then `ssv`.`LINE_NUM` when '2391' then `ssv`.`LINE_NUM` when '2359' then `ssv`.`LINE_NUM` when '2360' then `ssv`.`LINE_NUM` when '2400' then `ssv`.`LINE_NUM` when '2295' then `ssv`.`LINE_NUM` when '2368' then `ssv`.`LINE_NUM` when '2369' then `ssv`.`LINE_NUM` when '2518' then `ssv`.`LINE_NUM` when '2247' then `ssv`.`LINE_NUM` when '2248' then `ssv`.`LINE_NUM` when '2249' then `ssv`.`LINE_NUM` when '2250' then `ssv`.`LINE_NUM` when '2251' then `ssv`.`LINE_NUM` when '2252' then `ssv`.`LINE_NUM` when '2253' then `ssv`.`LINE_NUM` when '2383' then `ssv`.`LINE_NUM` when '2384' then `ssv`.`LINE_NUM` when '2385' then `ssv`.`LINE_NUM` when '2386' then `ssv`.`LINE_NUM` when '2387' then `ssv`.`LINE_NUM` when '2388' then `ssv`.`LINE_NUM` when '2389' then `ssv`.`LINE_NUM` when '2390' then `ssv`.`LINE_NUM` when '2286' then `ssv`.`LINE_NUM` when '2492' then `ssv`.`LINE_NUM` when '2493' then `ssv`.`LINE_NUM` when '2494' then `ssv`.`LINE_NUM` when '2495' then `ssv`.`LINE_NUM` when '2496' then `ssv`.`LINE_NUM` when '2497' then `ssv`.`LINE_NUM` when '2255' then `ssv`.`LINE_NUM` when '2260' then `ssv`.`LINE_NUM` when '2261' then `ssv`.`LINE_NUM` when '2315' then `ssv`.`LINE_NUM` when '2316' then `ssv`.`LINE_NUM` when '2317' then `ssv`.`LINE_NUM` when '2318' then `ssv`.`LINE_NUM` when '2319' then `ssv`.`LINE_NUM` when '2256' then `ssv`.`LINE_NUM` when '2277' then `ssv`.`LINE_NUM` when '2510' then `ssv`.`LINE_NUM` when '2511' then `ssv`.`LINE_NUM` when '2502' then `ssv`.`LINE_NUM` when '2283' then `ssv`.`LINE_NUM` when '2284' then `ssv`.`LINE_NUM` when '2273' then `ssv`.`LINE_NUM` when '2415' then `ssv`.`LINE_NUM` when '2416' then `ssv`.`LINE_NUM` when '2417' then `ssv`.`LINE_NUM` when '2414' then `ssv`.`LINE_NUM` when '2467' then `ssv`.`LINE_NUM` when '2484' then `ssv`.`LINE_NUM` when '2321' then `ssv`.`LINE_NUM` when '2322' then `ssv`.`LINE_NUM` when '2323' then `ssv`.`LINE_NUM` when '2324' then `ssv`.`LINE_NUM` when '2325' then `ssv`.`LINE_NUM` when '2326' then `ssv`.`LINE_NUM` when '2327' then `ssv`.`LINE_NUM` when '2328' then `ssv`.`LINE_NUM` when '2329' then `ssv`.`LINE_NUM` when '2330' then `ssv`.`LINE_NUM` when '2331' then `ssv`.`LINE_NUM` when '2332' then `ssv`.`LINE_NUM` when '2333' then `ssv`.`LINE_NUM` when '2334' then `ssv`.`LINE_NUM` when '2335' then `ssv`.`LINE_NUM` when '2336' then `ssv`.`LINE_NUM` when '2337' then `ssv`.`LINE_NUM` when '2338' then `ssv`.`LINE_NUM` when '2339' then `ssv`.`LINE_NUM` when '2340' then `ssv`.`LINE_NUM` when '2341' then `ssv`.`LINE_NUM` when '2370' then `ssv`.`LINE_NUM` when '2371' then `ssv`.`LINE_NUM` when '2372' then `ssv`.`LINE_NUM` when '2373' then `ssv`.`LINE_NUM` when '2374' then `ssv`.`LINE_NUM` when '2375' then `ssv`.`LINE_NUM` when '2376' then `ssv`.`LINE_NUM` when '2377' then `ssv`.`LINE_NUM` when '2378' then `ssv`.`LINE_NUM` when '2379' then `ssv`.`LINE_NUM` when '2392' then `ssv`.`LINE_NUM` when '2393' then `ssv`.`LINE_NUM` when '2394' then `ssv`.`LINE_NUM` when '2410' then `ssv`.`LINE_NUM` when '2411' then `ssv`.`LINE_NUM` when '2412' then `ssv`.`LINE_NUM` when '2469' then `ssv`.`LINE_NUM` when '2470' then `ssv`.`LINE_NUM` when '2471' then `ssv`.`LINE_NUM` when '2472' then `ssv`.`LINE_NUM` when '2473' then `ssv`.`LINE_NUM` when '2474' then `ssv`.`LINE_NUM` when '2475' then `ssv`.`LINE_NUM` when '2476' then `ssv`.`LINE_NUM` when '2486' then `ssv`.`LINE_NUM` when '2499' then `ssv`.`LINE_NUM` when '2522' then `ssv`.`LINE_NUM` when '2523' then `ssv`.`LINE_NUM` when '2524' then `ssv`.`LINE_NUM` when '2525' then `ssv`.`LINE_NUM` when '2526' then `ssv`.`LINE_NUM` when '2527' then `ssv`.`LINE_NUM` when '2528' then `ssv`.`LINE_NUM` when '2529' then `ssv`.`LINE_NUM` when '2530' then `ssv`.`LINE_NUM` when '2531' then `ssv`.`LINE_NUM` when '2532' then `ssv`.`LINE_NUM` when '2418' then `ssv`.`LINE_NUM` when '2419' then `ssv`.`LINE_NUM` when '2420' then `ssv`.`LINE_NUM` when '2421' then `ssv`.`LINE_NUM` when '2422' then `ssv`.`LINE_NUM` when '2423' then `ssv`.`LINE_NUM` when '2477' then `ssv`.`LINE_NUM` when '2478' then `ssv`.`LINE_NUM` when '2479' then `ssv`.`LINE_NUM` when '2480' then `ssv`.`LINE_NUM` when '2481' then `ssv`.`LINE_NUM` when '2482' then `ssv`.`LINE_NUM` when '2483' then `ssv`.`LINE_NUM` when '2533' then `ssv`.`LINE_NUM` when '2534' then `ssv`.`LINE_NUM` when '2535' then `ssv`.`LINE_NUM` when '2536' then `ssv`.`LINE_NUM` when '2551' then `ssv`.`LINE_NUM` when '2552' then `ssv`.`LINE_NUM` when '2553' then `ssv`.`LINE_NUM` when '2554' then `ssv`.`LINE_NUM` when '2555' then `ssv`.`LINE_NUM` when '2556' then `ssv`.`LINE_NUM` when '2557' then `ssv`.`LINE_NUM` when '2558' then `ssv`.`LINE_NUM` when '2559' then `ssv`.`LINE_NUM` when '2560' then `ssv`.`LINE_NUM` when '2561' then `ssv`.`LINE_NUM` when '2570' then `ssv`.`LINE_NUM` when '2571' then `ssv`.`LINE_NUM` when '2572' then `ssv`.`LINE_NUM` when '2573' then `ssv`.`LINE_NUM` when '2574' then `ssv`.`LINE_NUM` when '2575' then `ssv`.`LINE_NUM` when '2576' then `ssv`.`LINE_NUM` when '2577' then `ssv`.`LINE_NUM` when '2578' then `ssv`.`LINE_NUM` when '2579' then `ssv`.`LINE_NUM` when '2580' then `ssv`.`LINE_NUM` when '2581' then `ssv`.`LINE_NUM` when '2582' then `ssv`.`LINE_NUM` when '2583' then `ssv`.`LINE_NUM` when '2584' then `ssv`.`LINE_NUM` when '2585' then `ssv`.`LINE_NUM` when '2586' then `ssv`.`LINE_NUM` when '2587' then `ssv`.`LINE_NUM` else 0 end) AS `CLOUD_PHYSICS`,(case `ssv`.`CATE` when '2267' then (`ssv`.`LINE_NUM` * 0.1) when '2413' then (`ssv`.`LINE_NUM` * 0.5) when '2302' then (`ssv`.`LINE_NUM` * 3) when '2303' then (`ssv`.`LINE_NUM` * 5) when '2304' then (`ssv`.`LINE_NUM` * 7) when '2309' then (`ssv`.`LINE_NUM` * 14) when '2398' then (`ssv`.`LINE_NUM` * 8) when '2310' then (`ssv`.`LINE_NUM` * 16) when '2311' then (`ssv`.`LINE_NUM` * 38) when '2312' then (`ssv`.`LINE_NUM` * 105) when '2313' then (`ssv`.`LINE_NUM` * 2) when '2314' then (`ssv`.`LINE_NUM` * 10) when '2456' then (`ssv`.`LINE_NUM` * 14) when '2457' then (`ssv`.`LINE_NUM` * 8) when '2458' then (`ssv`.`LINE_NUM` * 16) when '2459' then (`ssv`.`LINE_NUM` * 38) when '2460' then (`ssv`.`LINE_NUM` * 105) when '2461' then (`ssv`.`LINE_NUM` * 2) when '2404' then (`ssv`.`LINE_NUM` * 1) when '2401' then (`ssv`.`LINE_NUM` * 2) when '2402' then (`ssv`.`LINE_NUM` * 3) when '2403' then (`ssv`.`LINE_NUM` * 4) when '2285' then (`ssv`.`LINE_NUM` * 7.5) when '2198' then (`ssv`.`LINE_NUM` * 1) when '2296' then (`ssv`.`LINE_NUM` * 3) when '2297' then (`ssv`.`LINE_NUM` * 8) when '2298' then (`ssv`.`LINE_NUM` * 15) when '2299' then (`ssv`.`LINE_NUM` * 30) when '2300' then (`ssv`.`LINE_NUM` * 85) when '2301' then (`ssv`.`LINE_NUM` * 100) when '2271' then (`ssv`.`LINE_NUM` * 12) when '2282' then (`ssv`.`LINE_NUM` * 2) when '2199' then (`ssv`.`LINE_NUM` * 0.02) when '2246' then (`ssv`.`LINE_NUM` * 20) when '2380' then (`ssv`.`LINE_NUM` * 1.8) when '2276' then (`ssv`.`LINE_NUM` * 0.2) when '2288' then (`ssv`.`LINE_NUM` * 0.5) when '2289' then (`ssv`.`LINE_NUM` * 0.8) when '2405' then (`ssv`.`LINE_NUM` * 4) when '2406' then (`ssv`.`LINE_NUM` * 9) when '2407' then (`ssv`.`LINE_NUM` * 12) when '2408' then (`ssv`.`LINE_NUM` * 26) when '2409' then (`ssv`.`LINE_NUM` * 34) when '2293' then (`ssv`.`LINE_NUM` * 5) when '2391' then (`ssv`.`LINE_NUM` * 0.6) when '2359' then (`ssv`.`LINE_NUM` * 0.3) when '2360' then (`ssv`.`LINE_NUM` * 0.7) when '2400' then (`ssv`.`LINE_NUM` * 0.1) when '2295' then (`ssv`.`LINE_NUM` * 8) when '2368' then (`ssv`.`LINE_NUM` * 0.02) when '2369' then (`ssv`.`LINE_NUM` * 0.06) when '2518' then (`ssv`.`LINE_NUM` * 126) when '2247' then (`ssv`.`LINE_NUM` * 6) when '2248' then (`ssv`.`LINE_NUM` * 20) when '2249' then (`ssv`.`LINE_NUM` * 25) when '2250' then (`ssv`.`LINE_NUM` * 20) when '2251' then (`ssv`.`LINE_NUM` * 25) when '2252' then (`ssv`.`LINE_NUM` * 10) when '2253' then (`ssv`.`LINE_NUM` * 20) when '2383' then (`ssv`.`LINE_NUM` * 6) when '2384' then (`ssv`.`LINE_NUM` * 20) when '2385' then (`ssv`.`LINE_NUM` * 61) when '2386' then (`ssv`.`LINE_NUM` * 125) when '2387' then (`ssv`.`LINE_NUM` * 150) when '2388' then (`ssv`.`LINE_NUM` * 47) when '2389' then (`ssv`.`LINE_NUM` * 128) when '2390' then (`ssv`.`LINE_NUM` * 150) when '2286' then (`ssv`.`LINE_NUM` * 0.06) when '2492' then (`ssv`.`LINE_NUM` * 10) when '2493' then (`ssv`.`LINE_NUM` * 30) when '2494' then (`ssv`.`LINE_NUM` * 50) when '2495' then (`ssv`.`LINE_NUM` * 40) when '2496' then (`ssv`.`LINE_NUM` * 60) when '2497' then (`ssv`.`LINE_NUM` * 80) when '2255' then (`ssv`.`LINE_NUM` * 35) when '2260' then (`ssv`.`LINE_NUM` * 17.5) when '2261' then (`ssv`.`LINE_NUM` * 8.75) when '2315' then (`ssv`.`LINE_NUM` * 7) when '2316' then (`ssv`.`LINE_NUM` * 20) when '2317' then (`ssv`.`LINE_NUM` * 30) when '2318' then (`ssv`.`LINE_NUM` * 70) when '2319' then (`ssv`.`LINE_NUM` * 150) when '2256' then (`ssv`.`LINE_NUM` * 6) when '2277' then (`ssv`.`LINE_NUM` * 0.1) when '2510' then (`ssv`.`LINE_NUM` * 0.1) when '2511' then (`ssv`.`LINE_NUM` * 0.1) when '2502' then (`ssv`.`LINE_NUM` * 0.02) when '2283' then (`ssv`.`LINE_NUM` * 3) when '2284' then (`ssv`.`LINE_NUM` * 2) when '2273' then (`ssv`.`LINE_NUM` * 1) when '2415' then (`ssv`.`LINE_NUM` * 1) when '2416' then (`ssv`.`LINE_NUM` * 6) when '2417' then (`ssv`.`LINE_NUM` * 10) when '2414' then (`ssv`.`LINE_NUM` * 0.2) when '2467' then (`ssv`.`LINE_NUM` * 0.1) when '2484' then (`ssv`.`LINE_NUM` * 14) when '2321' then (`ssv`.`LINE_NUM` * 3) when '2322' then (`ssv`.`LINE_NUM` * 10) when '2323' then (`ssv`.`LINE_NUM` * 10) when '2324' then (`ssv`.`LINE_NUM` * 3) when '2325' then (`ssv`.`LINE_NUM` * 3) when '2326' then (`ssv`.`LINE_NUM` * 3) when '2327' then (`ssv`.`LINE_NUM` * 3) when '2328' then (`ssv`.`LINE_NUM` * 3) when '2329' then (`ssv`.`LINE_NUM` * 3) when '2330' then (`ssv`.`LINE_NUM` * 3) when '2331' then (`ssv`.`LINE_NUM` * 3) when '2332' then (`ssv`.`LINE_NUM` * 3) when '2333' then (`ssv`.`LINE_NUM` * 3) when '2334' then (`ssv`.`LINE_NUM` * 3) when '2335' then (`ssv`.`LINE_NUM` * 1) when '2336' then (`ssv`.`LINE_NUM` * 1) when '2337' then (`ssv`.`LINE_NUM` * 1) when '2338' then (`ssv`.`LINE_NUM` * 1) when '2339' then (`ssv`.`LINE_NUM` * 1) when '2340' then (`ssv`.`LINE_NUM` * 1) when '2341' then (`ssv`.`LINE_NUM` * 1) when '2370' then (`ssv`.`LINE_NUM` * 10) when '2371' then (`ssv`.`LINE_NUM` * 10) when '2372' then (`ssv`.`LINE_NUM` * 3) when '2373' then (`ssv`.`LINE_NUM` * 3) when '2374' then (`ssv`.`LINE_NUM` * 3) when '2375' then (`ssv`.`LINE_NUM` * 3) when '2376' then (`ssv`.`LINE_NUM` * 3) when '2377' then (`ssv`.`LINE_NUM` * 3) when '2378' then (`ssv`.`LINE_NUM` * 3) when '2379' then (`ssv`.`LINE_NUM` * 1) when '2392' then (`ssv`.`LINE_NUM` * 3) when '2393' then (`ssv`.`LINE_NUM` * 3) when '2394' then (`ssv`.`LINE_NUM` * 1) when '2410' then (`ssv`.`LINE_NUM` * 3) when '2411' then (`ssv`.`LINE_NUM` * 3) when '2412' then (`ssv`.`LINE_NUM` * 1) when '2469' then (`ssv`.`LINE_NUM` * 10) when '2470' then (`ssv`.`LINE_NUM` * 3) when '2471' then (`ssv`.`LINE_NUM` * 1) when '2472' then (`ssv`.`LINE_NUM` * 3) when '2473' then (`ssv`.`LINE_NUM` * 3) when '2474' then (`ssv`.`LINE_NUM` * 1) when '2475' then (`ssv`.`LINE_NUM` * 1) when '2476' then (`ssv`.`LINE_NUM` * 1) when '2486' then (`ssv`.`LINE_NUM` * 3) when '2499' then (`ssv`.`LINE_NUM` * 1) when '2522' then (`ssv`.`LINE_NUM` * 10) when '2523' then (`ssv`.`LINE_NUM` * 3) when '2524' then (`ssv`.`LINE_NUM` * 3) when '2525' then (`ssv`.`LINE_NUM` * 3) when '2526' then (`ssv`.`LINE_NUM` * 3) when '2527' then (`ssv`.`LINE_NUM` * 3) when '2528' then (`ssv`.`LINE_NUM` * 3) when '2529' then (`ssv`.`LINE_NUM` * 1) when '2530' then (`ssv`.`LINE_NUM` * 1) when '2531' then (`ssv`.`LINE_NUM` * 1) when '2532' then (`ssv`.`LINE_NUM` * 1) when '2418' then (`ssv`.`LINE_NUM` * 3) when '2419' then (`ssv`.`LINE_NUM` * 1) when '2420' then (`ssv`.`LINE_NUM` * 1) when '2421' then (`ssv`.`LINE_NUM` * 1) when '2422' then (`ssv`.`LINE_NUM` * 1) when '2423' then (`ssv`.`LINE_NUM` * 1) when '2477' then (`ssv`.`LINE_NUM` * 3) when '2478' then (`ssv`.`LINE_NUM` * 3) when '2479' then (`ssv`.`LINE_NUM` * 3) when '2480' then (`ssv`.`LINE_NUM` * 3) when '2481' then (`ssv`.`LINE_NUM` * 3) when '2482' then (`ssv`.`LINE_NUM` * 1) when '2483' then (`ssv`.`LINE_NUM` * 1) when '2533' then (`ssv`.`LINE_NUM` * 3) when '2534' then (`ssv`.`LINE_NUM` * 8) when '2535' then (`ssv`.`LINE_NUM` * 5) when '2536' then (`ssv`.`LINE_NUM` * 80) when '2551' then (`ssv`.`LINE_NUM` * 1) when '2552' then (`ssv`.`LINE_NUM` * 1) when '2553' then (`ssv`.`LINE_NUM` * 3) when '2554' then (`ssv`.`LINE_NUM` * 5) when '2555' then (`ssv`.`LINE_NUM` * 8) when '2556' then (`ssv`.`LINE_NUM` * 13) when '2557' then (`ssv`.`LINE_NUM` * 19) when '2558' then (`ssv`.`LINE_NUM` * 25) when '2559' then (`ssv`.`LINE_NUM` * 44) when '2560' then (`ssv`.`LINE_NUM` * 6) when '2561' then (`ssv`.`LINE_NUM` * 14) when '2570' then (`ssv`.`LINE_NUM` * 14) when '2571' then (`ssv`.`LINE_NUM` * 17) when '2572' then (`ssv`.`LINE_NUM` * 24) when '2573' then (`ssv`.`LINE_NUM` * 31) when '2574' then (`ssv`.`LINE_NUM` * 38) when '2575' then (`ssv`.`LINE_NUM` * 45) when '2576' then (`ssv`.`LINE_NUM` * 52) when '2577' then (`ssv`.`LINE_NUM` * 59) when '2578' then (`ssv`.`LINE_NUM` * 67) when '2579' then (`ssv`.`LINE_NUM` * 74) when '2580' then (`ssv`.`LINE_NUM` * 81) when '2581' then (`ssv`.`LINE_NUM` * 7) when '2582' then (`ssv`.`LINE_NUM` * 2) when '2583' then (`ssv`.`LINE_NUM` * 4) when '2584' then (`ssv`.`LINE_NUM` * 6) when '2585' then (`ssv`.`LINE_NUM` * 2) when '2586' then (`ssv`.`LINE_NUM` * 4) when '2587' then (`ssv`.`LINE_NUM` * 6) else 0 end) AS `CLOUD_CHANGE`,(case `ssv`.`CATE` when '2449' then `ssv`.`LINE_NUM` when '2450' then `ssv`.`LINE_NUM` when '2451' then `ssv`.`LINE_NUM` when '2453' then `ssv`.`LINE_NUM` when '2435' then `ssv`.`LINE_NUM` when '2487' then `ssv`.`LINE_NUM` when '2488' then `ssv`.`LINE_NUM` when '2489' then `ssv`.`LINE_NUM` when '2490' then `ssv`.`LINE_NUM` when '2491' then `ssv`.`LINE_NUM` when '2436' then `ssv`.`LINE_NUM` when '2437' then `ssv`.`LINE_NUM` when '2504' then `ssv`.`LINE_NUM` when '2505' then `ssv`.`LINE_NUM` when '2506' then `ssv`.`LINE_NUM` when '2507' then `ssv`.`LINE_NUM` when '2508' then `ssv`.`LINE_NUM` when '2509' then `ssv`.`LINE_NUM` when '2500' then `ssv`.`LINE_NUM` when '2501' then `ssv`.`LINE_NUM` when '2562' then `ssv`.`LINE_NUM` when '2563' then `ssv`.`LINE_NUM` when '2564' then `ssv`.`LINE_NUM` when '2565' then `ssv`.`LINE_NUM` when '2566' then `ssv`.`LINE_NUM` when '2567' then `ssv`.`LINE_NUM` when '2568' then `ssv`.`LINE_NUM` when '2569' then `ssv`.`LINE_NUM` else 0 end) AS `YAHOO_PHYSICS`,(case `ssv`.`CATE` when '2449' then (`ssv`.`LINE_NUM` * 4) when '2450' then (`ssv`.`LINE_NUM` * 7) when '2451' then (`ssv`.`LINE_NUM` * 9) when '2453' then (`ssv`.`MONTH_NUM` * 0.000023) when '2435' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2487' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2488' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2489' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2490' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2491' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2436' then (`ssv`.`LINE_NUM` * 0) when '2437' then (`ssv`.`LINE_NUM` * 0) when '2504' then (`ssv`.`LINE_NUM` * 4) when '2505' then (`ssv`.`LINE_NUM` * 7) when '2506' then (`ssv`.`LINE_NUM` * 9) when '2508' then (`ssv`.`MONTH_NUM` * 0.000023) when '2509' then (`ssv`.`LINE_NUM` * 1) when '2500' then (`ssv`.`TEMP_NUM` * 0.00001042) when '2501' then (`ssv`.`LINE_NUM` * 3) when '2562' then (`ssv`.`LINE_NUM` * 4) when '2563' then (`ssv`.`LINE_NUM` * 7) when '2564' then (`ssv`.`LINE_NUM` * 9) when '2565' then (`ssv`.`MONTH_NUM` * 0.000023) when '2566' then (`ssv`.`LINE_NUM` * 4) when '2567' then (`ssv`.`LINE_NUM` * 7) when '2568' then (`ssv`.`LINE_NUM` * 9) when '2569' then (`ssv`.`MONTH_NUM` * 0.000023) else 0 end) AS `YAHOO_CHANGE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` when '2240' then `ssv`.`LINE_NUM` when '2183' then `ssv`.`LINE_NUM` else 0 end) AS `HS`,(case `ssv`.`CATE` when '2438' then `ssv`.`LINE_NUM` when '2439' then `ssv`.`LINE_NUM` when '2440' then `ssv`.`LINE_NUM` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE`,(case `ssv`.`CATE` when '2143' then `ssv`.`LINE_NUM` when '2244' then `ssv`.`LINE_NUM` when '2184' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD`,(case `ssv`.`CATE` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `IPAD`,(case `ssv`.`CATE` when '2267' then `ssv`.`LINE_NUM` else 0 end) AS `GOOGLEAPPS`,(case `ssv`.`CATE` when '2500' then (((`ssv`.`TEMP_NUM` * 0.50) / 4000) / 12) when '2746' then (((`ssv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2747' then (((`ssv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2748' then (((`ssv`.`TEMP_NUM` * 0.20) / 4000) / 12) when '2749' then (((`ssv`.`TEMP_NUM` * 0.15) / 4000) / 12) when '2751' then (((`ssv`.`TEMP_NUM` * 0.40) / 4000) / 12) else 0 end) AS `O2O`,(case `ssv`.`CATE` when '2467' then (`ssv`.`LINE_NUM` * 0.01) else 0 end) AS `PAYPAL` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_suggest_opportunities` `ops` on((`opp`.`ID` = `ops`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_suggest` `ssv` on(((`ops`.`SV_SUGGEST_IDA` = `ssv`.`ID`) and (`ssv`.`DELETED` = '0')))) join `ifview_eida_mast_serviceline` `msv` on(((`ssv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp_fy10_2`
--

/*!50001 DROP TABLE IF EXISTS `view_opp_fy10_2`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp_fy10_2` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`ops`.`DELETED` = 0),`ssv`.`DELETED`,`ops`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`ssv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`ssv`.`YYYY`,'/',`ssv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`ssv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) else 0 end) AS `SV_NUM`,`ssv`.`MONTH_NUM` AS `MONTH_NUM`,`ssv`.`TEMP_NUM` AS `TEMP_NUM`,`ssv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'提案' AS `SV_TYPE`,`ssv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,`opp`.`OP_DEPEND_C` AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`ssv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`ssv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`ssv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`ssv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`ssv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`ssv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`ssv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`ssv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_suggest_opportunities` `ops` on((`opp`.`ID` = `ops`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_suggest` `ssv` on((`ops`.`SV_SUGGEST_IDA` = `ssv`.`ID`))) join `ifview_eida_mast_serviceline` `msv` on(((`ssv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`sbtm_tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp_fy10_cstm`
--

/*!50001 DROP TABLE IF EXISTS `view_opp_fy10_cstm`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_cstm`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = cp932 */;
/*!50001 SET character_set_results     = cp932 */;
/*!50001 SET collation_connection      = cp932_japanese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp_fy10_cstm` AS select `opp`.`ID` AS `ID`,left(`opp`.`DESCRIPTION`,50) AS `DESCRIPTION`,left(`opp`.`OP_DEP_DETAIL_C`,50) AS `OP_DEP_DETAIL_C`,`opp`.`OP_COMPNAME_MEMO_C` AS `OP_COMPNAME_MEMO_C`,`opp`.`SUP_ID` AS `SUP_ID`,`opp`.`LEAD_SOURCE` AS `LEAD_SOURCE` from `ifview_eida_opportunities` `opp` where ((`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) and (`opp`.`DELETED` = '0')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_opp_fy10_del`
--

/*!50001 DROP TABLE IF EXISTS `view_opp_fy10_del`*/;
/*!50001 DROP VIEW IF EXISTS `view_opp_fy10_del`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_opp_fy10_del` AS select `opp`.`ID` AS `ID`,if((`aco`.`DELETED` = 1),1,if((`opp`.`DELETED` = 1),1,if((`ops`.`DELETED` = 0),`ssv`.`DELETED`,`ops`.`DELETED`))) AS `DELETED`,subtime(`opp`.`DATE_ENTERED`,'09:00:00') AS `DATE_ENTERED`,subtime(`opp`.`DATE_MODIFIED`,'09:00:00') AS `DATE_MODIFIED`,`opp`.`CREATED_BY` AS `CREATED_BY`,`opp`.`MODIFIED_USER_ID` AS `MODIFIED_USER_ID`,`opp`.`ASSIGNED_USER_ID` AS `ASSIGNED_USER_ID`,'001' AS `OP_GROUP_TYPE`,`es`.`eigyo_sosiki_disp_cd` AS `EIGYO_SOSIKI_DISP_CD`,`es`.`eigyo_sosiki_nm` AS `EIGYO_SOSIKI_NM`,`es`.`corp_cd` AS `CORP_CD`,`es`.`div1_cd` AS `MAST_ORGANIZATIONUNIT_ID`,`es`.`div2_cd` AS `MAST_ORGANIZATIONUNIT2_ID`,`es`.`div3_cd` AS `MAST_ORGANIZATIONUNIT3_ID`,`es`.`div4_cd` AS `MAST_ORGANIZATIONUNIT4_ID`,`es`.`div5_cd` AS `MAST_ORGANIZATIONUNIT5_ID`,`es`.`div6_cd` AS `MAST_ORGANIZATIONUNIT6_ID`,`msv`.`SLGRCD` AS `SV_CLASS`,`msv`.`SLGRNM` AS `SV_CLASS_DESC`,`ssv`.`CATE` AS `SV_CATE`,`msv`.`NAME` AS `SV_CATE_NAME`,concat(`ssv`.`YYYY`,'/',`ssv`.`MM`) AS `SV_START_END`,NULL AS `SV_UNIT`,(case `msv`.`DVMTFG` when '1' then round((cast(`ssv`.`LINE_NUM` as decimal(10,0)) * `msv`.`EVLNRT`),2) when '2' then round((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) / 10000),2) when '3' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.15) / 4000) / 12),2) when '4' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * 0.092) / 4000),2) when '5' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.5) / 12) / 4000),2) when '6' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.3) / 12) / 4000),2) when '7' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.35) / 12) / 4000),2) when '8' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.125) / 12) / 4000),2) when '9' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.2) / 12) / 4000),2) when '10' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 0.4) / 12) / 4000),2) when '11' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * 1.0) / 12) / 4000),2) when '12' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * 0.2) / 4000),2) when '13' then round((((cast(`ssv`.`TEMP_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 12) / 4000),2) when '14' then round(((cast(`ssv`.`MONTH_NUM` as decimal(10,0)) * `msv`.`SVDTFG`) / 4000),2) else 0 end) AS `SV_NUM`,`ssv`.`MONTH_NUM` AS `MONTH_NUM`,`ssv`.`TEMP_NUM` AS `TEMP_NUM`,`ssv`.`LINE_NUM` AS `LINE_NUM`,`acc`.`ID` AS `ACCOUNT_ID`,`acc`.`NAME` AS `ACCOUNT_NAME`,`opp`.`ID` AS `OPPORTUNITY_ID`,`opp`.`ANKEN_ID_C` AS `ANKEN_ID_C`,`opp`.`NAME` AS `OPPORTUNITY_NAME`,`opp`.`OWNER_ID` AS `OWNER_ID`,concat(`us2`.`LAST_NAME`,' ',`us2`.`FIRST_NAME`) AS `OWNER_NAME`,concat(`us1`.`LAST_NAME`,' ',`us1`.`FIRST_NAME`) AS `MAIN_SALES_NAME`,`opp`.`OP_STEP_C` AS `OP_STEP_C`,(case `opp`.`OP_STEP_C` when '10' then '案件発掘' when '20' then 'ヒアリング' when '30' then '一次提案' when '40' then '最終提案' when '50' then '契約・申込' when '60' then '失注' when '70' then '案件消滅' else '' end) AS `OP_STEP_C_DESC`,`opp`.`OP_CONT_PLANDATE_C` AS `OP_CONT_PLANDATE_C`,`opp`.`OP_ORDER_POSS_C` AS `OP_ORDER_POSS_C`,(case `opp`.`OP_ORDER_POSS_C` when '10' then '低' when '40' then '中' when '90' then '高' else NULL end) AS `OP_ORDER_POSS_C_DESC`,`opp`.`OP_COMMIT_C` AS `OP_COMMIT_C`,(case `opp`.`OP_COMMIT_C` when '001' then 'ベース' when '002' then 'アグレッシブ' when '003' then 'チャレンジ' else '' end) AS `OP_COMMIT_C_DESC`,date_format(`opp`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `DATE_MODIFIED_FMT`,ifnull(`msv`.`KEY_DRIVER_ID`,'') AS `KEY_DRIVER_ID`,ifnull(`msv`.`KEY_DRIVER_NAME`,'') AS `KEY_DRIVER_NAME`,'提案' AS `SV_TYPE`,`ssv`.`ID` AS `SV_ID`,date_format(`opp`.`DATE_ENTERED`,'%Y-%m-%d %H:%i') AS `DATE_ENTERED_FMT`,`opp`.`SALES_STAGE` AS `SALES_STAGE`,`us1`.`SPM_ORGUNIT_CD` AS `SPM_ORGUNIT_CD`,`us1`.`SPM_ORGUNIT_NAME` AS `SPM_ORGUNIT_NAME`,left(`opp`.`OP_DEPEND_C`,40) AS `OP_DEPEND_C`,`opp`.`OPPORTUNITY_TYPE` AS `OPPORTUNITY_TYPE`,subtime(`ssv`.`DATE_MODIFIED`,'09:00:00') AS `SV_DATE_MODIFIED`,`ssv`.`BILLIARD_FORM` AS `BILLIARD_FORM`,`ssv`.`BILLIARD_MODEL_CODE` AS `BILLIARD_MODEL_CODE`,`ssv`.`BILLIARD_MAKER` AS `BILLIARD_MAKER`,`ssv`.`BILLIARD_MODEL` AS `BILLIARD_MODEL`,`ssv`.`BILLIARD_COLOR` AS `BILLIARD_COLOR`,`opp`.`EMPHASIS_STRATEGY1_C` AS `EMPHASIS_STRATEGY1_C`,`opp`.`EMPHASIS_STRATEGY2_C` AS `EMPHASIS_STRATEGY2_C`,`opp`.`MEASURE_DEPT_C` AS `MEASURE_DEPT_C`,`opp`.`MEASURE_COMPANY_C` AS `MEASURE_COMPANY_C`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES030%') = 1) then '○' else '' end) AS `EM_STRATEGY1_030`,(case when ((`opp`.`EMPHASIS_STRATEGY1_C` like '%ES040%') = 1) then '○' else '' end) AS `EM_STRATEGY1_040`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES050%') = 1) then '○' else '' end) AS `EM_STRATEGY2_050`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES060%') = 1) then '○' else '' end) AS `EM_STRATEGY2_060`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES070%') = 1) then '○' else '' end) AS `EM_STRATEGY2_070`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES080%') = 1) then '○' else '' end) AS `EM_STRATEGY2_080`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES090%') = 1) then '○' else '' end) AS `EM_STRATEGY2_090`,(case when ((`opp`.`EMPHASIS_STRATEGY2_C` like '%ES100%') = 1) then '○' else '' end) AS `EM_STRATEGY2_100`,`ssv`.`BILLIARD_CHOICE_DAY` AS `BILLIARD_CHOICE_DAY`,`ssv`.`BILLIARD_MEMO` AS `BILLIARD_MEMO`,date_format(`ssv`.`DATE_MODIFIED`,'%Y-%m-%d %H:%i') AS `SV_DATE_MODIFIED_FMT`,`es`.`div1_nm` AS `MAST_ORGANIZATIONUNIT1_NM`,`es`.`div2_nm` AS `MAST_ORGANIZATIONUNIT2_NM`,`es`.`div3_nm` AS `MAST_ORGANIZATIONUNIT3_NM`,`es`.`div4_nm` AS `MAST_ORGANIZATIONUNIT4_NM`,`es`.`div5_nm` AS `MAST_ORGANIZATIONUNIT5_NM`,`es`.`div6_nm` AS `MAST_ORGANIZATIONUNIT6_NM`,left(`opp`.`OP_CONT_PLANDATE_C`,7) AS `OP_CONT_PLANMONTH`,`ssv`.`MONTH_NUM_LOCAL_CURRENCY` AS `MONTH_NUM_LOCAL_CURRENCY`,`ssv`.`MONTH_NUM_UNIT` AS `MONTH_NUM_UNIT`,`ssv`.`TEMP_NUM_LOCAL_CURRENCY` AS `TEMP_NUM_LOCAL_CURRENCY`,`ssv`.`TEMP_NUM_UNIT` AS `TEMP_NUM_UNIT`,(case when (`opp`.`OP_STEP_C` = '50') then '契約・申込' when (`opp`.`OP_STEP_C` = '60') then '失注' when (`opp`.`OP_STEP_C` = '70') then '案件消滅' when (`opp`.`OP_COMMIT_C` = '001') then 'ベース' when (`opp`.`OP_COMMIT_C` = '002') then 'アグレッシブ' when (`opp`.`OP_COMMIT_C` = '003') then 'チャレンジ' else '' end) AS `OP_MANAGE_FORECAST`,`ssv`.`MONTH_AGE` AS `MONTH_AGE`,(case `ssv`.`CATE` when '1106' then `ssv`.`LINE_NUM` when '2464' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI`,(case `ssv`.`CATE` when '1107' then `ssv`.`LINE_NUM` when '2463' then `ssv`.`LINE_NUM` when '1108' then `ssv`.`LINE_NUM` when '2462' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_BRI`,(case `ssv`.`CATE` when '1106' then `ssv`.`LINE_NUM` when '2464' then `ssv`.`LINE_NUM` when '1107' then `ssv`.`LINE_NUM` when '2463' then `ssv`.`LINE_NUM` when '1108' then `ssv`.`LINE_NUM` when '2462' then `ssv`.`LINE_NUM` else 0 end) AS `SPECIAL_PRI1`,(case `ssv`.`CATE` when '1106' then (`ssv`.`LINE_NUM` * 20) when '2464' then (`ssv`.`LINE_NUM` * 30) when '1107' then `ssv`.`LINE_NUM` when '2463' then (`ssv`.`LINE_NUM` * 1.5) when '1108' then `ssv`.`LINE_NUM` when '2462' then (`ssv`.`LINE_NUM` * 1.5) else 0 end) AS `SPECIAL_PRI20`,(case `ssv`.`CATE` when '2444' then `ssv`.`LINE_NUM` when '2263' then `ssv`.`LINE_NUM` when '2200' then `ssv`.`LINE_NUM` when '2201' then `ssv`.`LINE_NUM` when '2202' then `ssv`.`LINE_NUM` when '2445' then `ssv`.`LINE_NUM` when '2203' then `ssv`.`LINE_NUM` when '2204' then `ssv`.`LINE_NUM` when '2205' then `ssv`.`LINE_NUM` when '2206' then `ssv`.`LINE_NUM` when '2446' then `ssv`.`LINE_NUM` when '2207' then `ssv`.`LINE_NUM` when '2208' then `ssv`.`LINE_NUM` when '2209' then `ssv`.`LINE_NUM` when '2210' then `ssv`.`LINE_NUM` when '2211' then `ssv`.`LINE_NUM` when '2212' then `ssv`.`LINE_NUM` when '2213' then `ssv`.`LINE_NUM` when '2214' then `ssv`.`LINE_NUM` when '2264' then `ssv`.`LINE_NUM` when '2215' then `ssv`.`LINE_NUM` when '2447' then `ssv`.`LINE_NUM` when '2217' then `ssv`.`LINE_NUM` when '2218' then `ssv`.`LINE_NUM` when '2219' then `ssv`.`LINE_NUM` when '2220' then `ssv`.`LINE_NUM` when '2221' then `ssv`.`LINE_NUM` when '2222' then `ssv`.`LINE_NUM` when '2223' then `ssv`.`LINE_NUM` when '2224' then `ssv`.`LINE_NUM` when '2225' then `ssv`.`LINE_NUM` when '2448' then `ssv`.`LINE_NUM` when '2226' then `ssv`.`LINE_NUM` when '2227' then `ssv`.`LINE_NUM` when '2228' then `ssv`.`LINE_NUM` when '2229' then `ssv`.`LINE_NUM` when '2230' then `ssv`.`LINE_NUM` when '2231' then `ssv`.`LINE_NUM` when '2232' then `ssv`.`LINE_NUM` when '2233' then `ssv`.`LINE_NUM` when '2234' then `ssv`.`LINE_NUM` when '2262' then `ssv`.`LINE_NUM` when '2381' then `ssv`.`LINE_NUM` when '2382' then `ssv`.`LINE_NUM` when '2258' then `ssv`.`LINE_NUM` when '2320' then `ssv`.`LINE_NUM` when '2305' then `ssv`.`LINE_NUM` when '2395' then `ssv`.`LINE_NUM` when '2396' then `ssv`.`LINE_NUM` when '2397' then `ssv`.`LINE_NUM` when '2468' then `ssv`.`LINE_NUM` when '2512' then `ssv`.`LINE_NUM` when '2513' then `ssv`.`LINE_NUM` when '2514' then `ssv`.`LINE_NUM` when '2515' then `ssv`.`LINE_NUM` when '2516' then `ssv`.`LINE_NUM` when '2517' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_PHYSICS`,(case `ssv`.`CATE` when '2444' then (`ssv`.`LINE_NUM` * 130) when '2263' then (`ssv`.`LINE_NUM` * 70) when '2200' then (`ssv`.`LINE_NUM` * 40) when '2201' then (`ssv`.`LINE_NUM` * 20) when '2202' then (`ssv`.`LINE_NUM` * 10) when '2445' then (`ssv`.`LINE_NUM` * 200) when '2203' then (`ssv`.`LINE_NUM` * 150) when '2204' then (`ssv`.`LINE_NUM` * 50) when '2205' then (`ssv`.`LINE_NUM` * 20) when '2206' then (`ssv`.`LINE_NUM` * 10) when '2446' then (`ssv`.`LINE_NUM` * 200) when '2207' then (`ssv`.`LINE_NUM` * 150) when '2208' then (`ssv`.`LINE_NUM` * 70) when '2209' then (`ssv`.`LINE_NUM` * 60) when '2210' then (`ssv`.`LINE_NUM` * 20) when '2211' then (`ssv`.`LINE_NUM` * 10) when '2212' then (`ssv`.`LINE_NUM` * 20) when '2213' then (`ssv`.`LINE_NUM` * 200) when '2214' then (`ssv`.`LINE_NUM` * 30) when '2264' then (`ssv`.`LINE_NUM` * 20) when '2215' then (`ssv`.`LINE_NUM` * 10) when '2447' then (`ssv`.`LINE_NUM` * 200) when '2217' then (`ssv`.`LINE_NUM` * 150) when '2218' then (`ssv`.`LINE_NUM` * 80) when '2219' then (`ssv`.`LINE_NUM` * 10) when '2220' then (`ssv`.`LINE_NUM` * 5) when '2221' then (`ssv`.`LINE_NUM` * 5) when '2222' then (`ssv`.`LINE_NUM` * 100) when '2223' then (`ssv`.`LINE_NUM` * 10) when '2224' then (`ssv`.`LINE_NUM` * 5) when '2225' then (`ssv`.`LINE_NUM` * 5) when '2448' then (`ssv`.`LINE_NUM` * 200) when '2226' then (`ssv`.`LINE_NUM` * 150) when '2227' then (`ssv`.`LINE_NUM` * 10) when '2228' then (`ssv`.`LINE_NUM` * 5) when '2229' then (`ssv`.`LINE_NUM` * 1) when '2230' then (`ssv`.`LINE_NUM` * 10) when '2231' then (`ssv`.`LINE_NUM` * 5) when '2232' then (`ssv`.`LINE_NUM` * 1) when '2233' then (`ssv`.`LINE_NUM` * 1) when '2234' then (`ssv`.`LINE_NUM` * 5) when '2262' then (`ssv`.`LINE_NUM` * 1) when '2381' then (`ssv`.`LINE_NUM` * 1) when '2382' then (`ssv`.`LINE_NUM` * 0.1) when '2258' then (`ssv`.`MONTH_NUM` / 10000) when '2320' then (`ssv`.`MONTH_NUM` / 10000) when '2305' then (`ssv`.`MONTH_NUM` / 10000) when '2395' then (`ssv`.`MONTH_NUM` / 10000) when '2396' then (`ssv`.`MONTH_NUM` / 10000) when '2397' then (`ssv`.`MONTH_NUM` / 10000) when '2468' then (`ssv`.`LINE_NUM` * 5) when '2512' then (`ssv`.`LINE_NUM` * 35) when '2513' then (`ssv`.`LINE_NUM` * 9) when '2514' then (`ssv`.`LINE_NUM` * 2) when '2515' then (`ssv`.`LINE_NUM` * 8) when '2516' then (`ssv`.`LINE_NUM` * 3) when '2517' then (`ssv`.`LINE_NUM` * 1) else 0 end) AS `DATA_CHANGE`,(case `ssv`.`CATE` when '2444' then `ssv`.`LINE_NUM` when '2263' then `ssv`.`LINE_NUM` when '2200' then `ssv`.`LINE_NUM` when '2201' then `ssv`.`LINE_NUM` when '2202' then `ssv`.`LINE_NUM` when '2445' then `ssv`.`LINE_NUM` when '2203' then `ssv`.`LINE_NUM` when '2204' then `ssv`.`LINE_NUM` when '2205' then `ssv`.`LINE_NUM` when '2206' then `ssv`.`LINE_NUM` when '2446' then `ssv`.`LINE_NUM` when '2207' then `ssv`.`LINE_NUM` when '2208' then `ssv`.`LINE_NUM` when '2209' then `ssv`.`LINE_NUM` when '2210' then `ssv`.`LINE_NUM` when '2211' then `ssv`.`LINE_NUM` when '2212' then `ssv`.`LINE_NUM` when '2213' then `ssv`.`LINE_NUM` when '2214' then `ssv`.`LINE_NUM` when '2264' then `ssv`.`LINE_NUM` when '2215' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_GET_PHYSICS`,(case `ssv`.`CATE` when '2444' then (`ssv`.`LINE_NUM` * 130) when '2263' then (`ssv`.`LINE_NUM` * 70) when '2200' then (`ssv`.`LINE_NUM` * 40) when '2201' then (`ssv`.`LINE_NUM` * 20) when '2202' then (`ssv`.`LINE_NUM` * 10) when '2445' then (`ssv`.`LINE_NUM` * 200) when '2203' then (`ssv`.`LINE_NUM` * 150) when '2204' then (`ssv`.`LINE_NUM` * 50) when '2205' then (`ssv`.`LINE_NUM` * 20) when '2206' then (`ssv`.`LINE_NUM` * 10) when '2446' then (`ssv`.`LINE_NUM` * 200) when '2207' then (`ssv`.`LINE_NUM` * 150) when '2208' then (`ssv`.`LINE_NUM` * 70) when '2209' then (`ssv`.`LINE_NUM` * 60) when '2210' then (`ssv`.`LINE_NUM` * 20) when '2211' then (`ssv`.`LINE_NUM` * 10) when '2212' then (`ssv`.`LINE_NUM` * 20) when '2213' then (`ssv`.`LINE_NUM` * 200) when '2214' then (`ssv`.`LINE_NUM` * 30) when '2264' then (`ssv`.`LINE_NUM` * 20) when '2215' then (`ssv`.`LINE_NUM` * 10) else 0 end) AS `DATA_GET_CHANGE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` when '2143' then `ssv`.`LINE_NUM` when '2183' then `ssv`.`LINE_NUM` when '2184' then `ssv`.`LINE_NUM` when '2240' then `ssv`.`LINE_NUM` when '2241' then `ssv`.`LINE_NUM` when '2242' then `ssv`.`LINE_NUM` when '2243' then `ssv`.`LINE_NUM` when '2244' then `ssv`.`LINE_NUM` when '2278' then `ssv`.`LINE_NUM` when '2279' then `ssv`.`LINE_NUM` when '2280' then `ssv`.`LINE_NUM` when '2438' then `ssv`.`LINE_NUM` when '2439' then `ssv`.`LINE_NUM` when '2440' then `ssv`.`LINE_NUM` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `MOBILE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` else 0 end) AS `HS_SALE_END`,(case `ssv`.`CATE` when '2143' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_SALE_END`,(case `ssv`.`CATE` when '2241' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_SALE_END`,(case `ssv`.`CATE` when '2240' then `ssv`.`LINE_NUM` else 0 end) AS `HS_ALLOTMENT`,(case `ssv`.`CATE` when '2244' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_ALLOTMENT`,(case `ssv`.`CATE` when '2242' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_ALLOTMENT`,(case `ssv`.`CATE` when '2183' then `ssv`.`LINE_NUM` else 0 end) AS `HS_RENTAL`,(case `ssv`.`CATE` when '2184' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD_RENTAL`,(case `ssv`.`CATE` when '2243' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE_RENTAL`,(case `ssv`.`CATE` when '2265' then `ssv`.`LINE_NUM` when '2268' then `ssv`.`LINE_NUM` when '2269' then `ssv`.`LINE_NUM` when '2287' then `ssv`.`LINE_NUM` when '2350' then `ssv`.`LINE_NUM` when '2351' then `ssv`.`LINE_NUM` when '2352' then `ssv`.`LINE_NUM` when '2353' then `ssv`.`LINE_NUM` when '2354' then `ssv`.`LINE_NUM` when '2355' then `ssv`.`LINE_NUM` when '2356' then `ssv`.`LINE_NUM` when '2357' then `ssv`.`LINE_NUM` when '2358' then `ssv`.`LINE_NUM` when '2361' then `ssv`.`LINE_NUM` when '2362' then `ssv`.`LINE_NUM` when '2363' then `ssv`.`LINE_NUM` when '2364' then `ssv`.`LINE_NUM` when '2365' then `ssv`.`LINE_NUM` when '2366' then `ssv`.`LINE_NUM` when '2367' then `ssv`.`LINE_NUM` else 0 end) AS `PHS`,(case `ssv`.`CATE` when '2191' then `ssv`.`LINE_NUM` else 0 end) AS `WHITE_OFFERS`,(case `ssv`.`CATE` when '2193' then `ssv`.`LINE_NUM` else 0 end) AS `MODEL_CHANGE`,(case `ssv`.`CATE` when '1115' then `ssv`.`LINE_NUM` else 0 end) AS `ML`,(case `ssv`.`CATE` when '2267' then `ssv`.`LINE_NUM` when '2413' then `ssv`.`LINE_NUM` when '2302' then `ssv`.`LINE_NUM` when '2303' then `ssv`.`LINE_NUM` when '2304' then `ssv`.`LINE_NUM` when '2309' then `ssv`.`LINE_NUM` when '2398' then `ssv`.`LINE_NUM` when '2310' then `ssv`.`LINE_NUM` when '2311' then `ssv`.`LINE_NUM` when '2312' then `ssv`.`LINE_NUM` when '2313' then `ssv`.`LINE_NUM` when '2314' then `ssv`.`LINE_NUM` when '2456' then `ssv`.`LINE_NUM` when '2457' then `ssv`.`LINE_NUM` when '2458' then `ssv`.`LINE_NUM` when '2459' then `ssv`.`LINE_NUM` when '2460' then `ssv`.`LINE_NUM` when '2461' then `ssv`.`LINE_NUM` when '2404' then `ssv`.`LINE_NUM` when '2401' then `ssv`.`LINE_NUM` when '2402' then `ssv`.`LINE_NUM` when '2403' then `ssv`.`LINE_NUM` when '2285' then `ssv`.`LINE_NUM` when '2198' then `ssv`.`LINE_NUM` when '2296' then `ssv`.`LINE_NUM` when '2297' then `ssv`.`LINE_NUM` when '2298' then `ssv`.`LINE_NUM` when '2299' then `ssv`.`LINE_NUM` when '2300' then `ssv`.`LINE_NUM` when '2301' then `ssv`.`LINE_NUM` when '2271' then `ssv`.`LINE_NUM` when '2282' then `ssv`.`LINE_NUM` when '2199' then `ssv`.`LINE_NUM` when '2246' then `ssv`.`LINE_NUM` when '2380' then `ssv`.`LINE_NUM` when '2276' then `ssv`.`LINE_NUM` when '2288' then `ssv`.`LINE_NUM` when '2289' then `ssv`.`LINE_NUM` when '2405' then `ssv`.`LINE_NUM` when '2406' then `ssv`.`LINE_NUM` when '2407' then `ssv`.`LINE_NUM` when '2408' then `ssv`.`LINE_NUM` when '2409' then `ssv`.`LINE_NUM` when '2293' then `ssv`.`LINE_NUM` when '2391' then `ssv`.`LINE_NUM` when '2359' then `ssv`.`LINE_NUM` when '2360' then `ssv`.`LINE_NUM` when '2400' then `ssv`.`LINE_NUM` when '2295' then `ssv`.`LINE_NUM` when '2368' then `ssv`.`LINE_NUM` when '2369' then `ssv`.`LINE_NUM` when '2518' then `ssv`.`LINE_NUM` when '2247' then `ssv`.`LINE_NUM` when '2248' then `ssv`.`LINE_NUM` when '2249' then `ssv`.`LINE_NUM` when '2250' then `ssv`.`LINE_NUM` when '2251' then `ssv`.`LINE_NUM` when '2252' then `ssv`.`LINE_NUM` when '2253' then `ssv`.`LINE_NUM` when '2383' then `ssv`.`LINE_NUM` when '2384' then `ssv`.`LINE_NUM` when '2385' then `ssv`.`LINE_NUM` when '2386' then `ssv`.`LINE_NUM` when '2387' then `ssv`.`LINE_NUM` when '2388' then `ssv`.`LINE_NUM` when '2389' then `ssv`.`LINE_NUM` when '2390' then `ssv`.`LINE_NUM` when '2286' then `ssv`.`LINE_NUM` when '2492' then `ssv`.`LINE_NUM` when '2493' then `ssv`.`LINE_NUM` when '2494' then `ssv`.`LINE_NUM` when '2495' then `ssv`.`LINE_NUM` when '2496' then `ssv`.`LINE_NUM` when '2497' then `ssv`.`LINE_NUM` when '2255' then `ssv`.`LINE_NUM` when '2260' then `ssv`.`LINE_NUM` when '2261' then `ssv`.`LINE_NUM` when '2315' then `ssv`.`LINE_NUM` when '2316' then `ssv`.`LINE_NUM` when '2317' then `ssv`.`LINE_NUM` when '2318' then `ssv`.`LINE_NUM` when '2319' then `ssv`.`LINE_NUM` when '2256' then `ssv`.`LINE_NUM` when '2277' then `ssv`.`LINE_NUM` when '2510' then `ssv`.`LINE_NUM` when '2511' then `ssv`.`LINE_NUM` when '2502' then `ssv`.`LINE_NUM` when '2283' then `ssv`.`LINE_NUM` when '2284' then `ssv`.`LINE_NUM` when '2273' then `ssv`.`LINE_NUM` when '2415' then `ssv`.`LINE_NUM` when '2416' then `ssv`.`LINE_NUM` when '2417' then `ssv`.`LINE_NUM` when '2414' then `ssv`.`LINE_NUM` when '2467' then `ssv`.`LINE_NUM` when '2484' then `ssv`.`LINE_NUM` when '2321' then `ssv`.`LINE_NUM` when '2322' then `ssv`.`LINE_NUM` when '2323' then `ssv`.`LINE_NUM` when '2324' then `ssv`.`LINE_NUM` when '2325' then `ssv`.`LINE_NUM` when '2326' then `ssv`.`LINE_NUM` when '2327' then `ssv`.`LINE_NUM` when '2328' then `ssv`.`LINE_NUM` when '2329' then `ssv`.`LINE_NUM` when '2330' then `ssv`.`LINE_NUM` when '2331' then `ssv`.`LINE_NUM` when '2332' then `ssv`.`LINE_NUM` when '2333' then `ssv`.`LINE_NUM` when '2334' then `ssv`.`LINE_NUM` when '2335' then `ssv`.`LINE_NUM` when '2336' then `ssv`.`LINE_NUM` when '2337' then `ssv`.`LINE_NUM` when '2338' then `ssv`.`LINE_NUM` when '2339' then `ssv`.`LINE_NUM` when '2340' then `ssv`.`LINE_NUM` when '2341' then `ssv`.`LINE_NUM` when '2370' then `ssv`.`LINE_NUM` when '2371' then `ssv`.`LINE_NUM` when '2372' then `ssv`.`LINE_NUM` when '2373' then `ssv`.`LINE_NUM` when '2374' then `ssv`.`LINE_NUM` when '2375' then `ssv`.`LINE_NUM` when '2376' then `ssv`.`LINE_NUM` when '2377' then `ssv`.`LINE_NUM` when '2378' then `ssv`.`LINE_NUM` when '2379' then `ssv`.`LINE_NUM` when '2392' then `ssv`.`LINE_NUM` when '2393' then `ssv`.`LINE_NUM` when '2394' then `ssv`.`LINE_NUM` when '2410' then `ssv`.`LINE_NUM` when '2411' then `ssv`.`LINE_NUM` when '2412' then `ssv`.`LINE_NUM` when '2469' then `ssv`.`LINE_NUM` when '2470' then `ssv`.`LINE_NUM` when '2471' then `ssv`.`LINE_NUM` when '2472' then `ssv`.`LINE_NUM` when '2473' then `ssv`.`LINE_NUM` when '2474' then `ssv`.`LINE_NUM` when '2475' then `ssv`.`LINE_NUM` when '2476' then `ssv`.`LINE_NUM` when '2486' then `ssv`.`LINE_NUM` when '2499' then `ssv`.`LINE_NUM` when '2522' then `ssv`.`LINE_NUM` when '2523' then `ssv`.`LINE_NUM` when '2524' then `ssv`.`LINE_NUM` when '2525' then `ssv`.`LINE_NUM` when '2526' then `ssv`.`LINE_NUM` when '2527' then `ssv`.`LINE_NUM` when '2528' then `ssv`.`LINE_NUM` when '2529' then `ssv`.`LINE_NUM` when '2530' then `ssv`.`LINE_NUM` when '2531' then `ssv`.`LINE_NUM` when '2532' then `ssv`.`LINE_NUM` when '2418' then `ssv`.`LINE_NUM` when '2419' then `ssv`.`LINE_NUM` when '2420' then `ssv`.`LINE_NUM` when '2421' then `ssv`.`LINE_NUM` when '2422' then `ssv`.`LINE_NUM` when '2423' then `ssv`.`LINE_NUM` when '2477' then `ssv`.`LINE_NUM` when '2478' then `ssv`.`LINE_NUM` when '2479' then `ssv`.`LINE_NUM` when '2480' then `ssv`.`LINE_NUM` when '2481' then `ssv`.`LINE_NUM` when '2482' then `ssv`.`LINE_NUM` when '2483' then `ssv`.`LINE_NUM` when '2533' then `ssv`.`LINE_NUM` when '2534' then `ssv`.`LINE_NUM` when '2535' then `ssv`.`LINE_NUM` when '2536' then `ssv`.`LINE_NUM` when '2551' then `ssv`.`LINE_NUM` when '2552' then `ssv`.`LINE_NUM` when '2553' then `ssv`.`LINE_NUM` when '2554' then `ssv`.`LINE_NUM` when '2555' then `ssv`.`LINE_NUM` when '2556' then `ssv`.`LINE_NUM` when '2557' then `ssv`.`LINE_NUM` when '2558' then `ssv`.`LINE_NUM` when '2559' then `ssv`.`LINE_NUM` when '2560' then `ssv`.`LINE_NUM` when '2561' then `ssv`.`LINE_NUM` when '2570' then `ssv`.`LINE_NUM` when '2571' then `ssv`.`LINE_NUM` when '2572' then `ssv`.`LINE_NUM` when '2573' then `ssv`.`LINE_NUM` when '2574' then `ssv`.`LINE_NUM` when '2575' then `ssv`.`LINE_NUM` when '2576' then `ssv`.`LINE_NUM` when '2577' then `ssv`.`LINE_NUM` when '2578' then `ssv`.`LINE_NUM` when '2579' then `ssv`.`LINE_NUM` when '2580' then `ssv`.`LINE_NUM` when '2581' then `ssv`.`LINE_NUM` when '2582' then `ssv`.`LINE_NUM` when '2583' then `ssv`.`LINE_NUM` when '2584' then `ssv`.`LINE_NUM` when '2585' then `ssv`.`LINE_NUM` when '2586' then `ssv`.`LINE_NUM` when '2587' then `ssv`.`LINE_NUM` else 0 end) AS `CLOUD_PHYSICS`,(case `ssv`.`CATE` when '2267' then (`ssv`.`LINE_NUM` * 0.1) when '2413' then (`ssv`.`LINE_NUM` * 0.5) when '2302' then (`ssv`.`LINE_NUM` * 3) when '2303' then (`ssv`.`LINE_NUM` * 5) when '2304' then (`ssv`.`LINE_NUM` * 7) when '2309' then (`ssv`.`LINE_NUM` * 14) when '2398' then (`ssv`.`LINE_NUM` * 8) when '2310' then (`ssv`.`LINE_NUM` * 16) when '2311' then (`ssv`.`LINE_NUM` * 38) when '2312' then (`ssv`.`LINE_NUM` * 105) when '2313' then (`ssv`.`LINE_NUM` * 2) when '2314' then (`ssv`.`LINE_NUM` * 10) when '2456' then (`ssv`.`LINE_NUM` * 14) when '2457' then (`ssv`.`LINE_NUM` * 8) when '2458' then (`ssv`.`LINE_NUM` * 16) when '2459' then (`ssv`.`LINE_NUM` * 38) when '2460' then (`ssv`.`LINE_NUM` * 105) when '2461' then (`ssv`.`LINE_NUM` * 2) when '2404' then (`ssv`.`LINE_NUM` * 1) when '2401' then (`ssv`.`LINE_NUM` * 2) when '2402' then (`ssv`.`LINE_NUM` * 3) when '2403' then (`ssv`.`LINE_NUM` * 4) when '2285' then (`ssv`.`LINE_NUM` * 7.5) when '2198' then (`ssv`.`LINE_NUM` * 1) when '2296' then (`ssv`.`LINE_NUM` * 3) when '2297' then (`ssv`.`LINE_NUM` * 8) when '2298' then (`ssv`.`LINE_NUM` * 15) when '2299' then (`ssv`.`LINE_NUM` * 30) when '2300' then (`ssv`.`LINE_NUM` * 85) when '2301' then (`ssv`.`LINE_NUM` * 100) when '2271' then (`ssv`.`LINE_NUM` * 12) when '2282' then (`ssv`.`LINE_NUM` * 2) when '2199' then (`ssv`.`LINE_NUM` * 0.02) when '2246' then (`ssv`.`LINE_NUM` * 20) when '2380' then (`ssv`.`LINE_NUM` * 1.8) when '2276' then (`ssv`.`LINE_NUM` * 0.2) when '2288' then (`ssv`.`LINE_NUM` * 0.5) when '2289' then (`ssv`.`LINE_NUM` * 0.8) when '2405' then (`ssv`.`LINE_NUM` * 4) when '2406' then (`ssv`.`LINE_NUM` * 9) when '2407' then (`ssv`.`LINE_NUM` * 12) when '2408' then (`ssv`.`LINE_NUM` * 26) when '2409' then (`ssv`.`LINE_NUM` * 34) when '2293' then (`ssv`.`LINE_NUM` * 5) when '2391' then (`ssv`.`LINE_NUM` * 0.6) when '2359' then (`ssv`.`LINE_NUM` * 0.3) when '2360' then (`ssv`.`LINE_NUM` * 0.7) when '2400' then (`ssv`.`LINE_NUM` * 0.1) when '2295' then (`ssv`.`LINE_NUM` * 8) when '2368' then (`ssv`.`LINE_NUM` * 0.02) when '2369' then (`ssv`.`LINE_NUM` * 0.06) when '2518' then (`ssv`.`LINE_NUM` * 126) when '2247' then (`ssv`.`LINE_NUM` * 6) when '2248' then (`ssv`.`LINE_NUM` * 20) when '2249' then (`ssv`.`LINE_NUM` * 25) when '2250' then (`ssv`.`LINE_NUM` * 20) when '2251' then (`ssv`.`LINE_NUM` * 25) when '2252' then (`ssv`.`LINE_NUM` * 10) when '2253' then (`ssv`.`LINE_NUM` * 20) when '2383' then (`ssv`.`LINE_NUM` * 6) when '2384' then (`ssv`.`LINE_NUM` * 20) when '2385' then (`ssv`.`LINE_NUM` * 61) when '2386' then (`ssv`.`LINE_NUM` * 125) when '2387' then (`ssv`.`LINE_NUM` * 150) when '2388' then (`ssv`.`LINE_NUM` * 47) when '2389' then (`ssv`.`LINE_NUM` * 128) when '2390' then (`ssv`.`LINE_NUM` * 150) when '2286' then (`ssv`.`LINE_NUM` * 0.06) when '2492' then (`ssv`.`LINE_NUM` * 10) when '2493' then (`ssv`.`LINE_NUM` * 30) when '2494' then (`ssv`.`LINE_NUM` * 50) when '2495' then (`ssv`.`LINE_NUM` * 40) when '2496' then (`ssv`.`LINE_NUM` * 60) when '2497' then (`ssv`.`LINE_NUM` * 80) when '2255' then (`ssv`.`LINE_NUM` * 35) when '2260' then (`ssv`.`LINE_NUM` * 17.5) when '2261' then (`ssv`.`LINE_NUM` * 8.75) when '2315' then (`ssv`.`LINE_NUM` * 7) when '2316' then (`ssv`.`LINE_NUM` * 20) when '2317' then (`ssv`.`LINE_NUM` * 30) when '2318' then (`ssv`.`LINE_NUM` * 70) when '2319' then (`ssv`.`LINE_NUM` * 150) when '2256' then (`ssv`.`LINE_NUM` * 6) when '2277' then (`ssv`.`LINE_NUM` * 0.1) when '2510' then (`ssv`.`LINE_NUM` * 0.1) when '2511' then (`ssv`.`LINE_NUM` * 0.1) when '2502' then (`ssv`.`LINE_NUM` * 0.02) when '2283' then (`ssv`.`LINE_NUM` * 3) when '2284' then (`ssv`.`LINE_NUM` * 2) when '2273' then (`ssv`.`LINE_NUM` * 1) when '2415' then (`ssv`.`LINE_NUM` * 1) when '2416' then (`ssv`.`LINE_NUM` * 6) when '2417' then (`ssv`.`LINE_NUM` * 10) when '2414' then (`ssv`.`LINE_NUM` * 0.2) when '2467' then (`ssv`.`LINE_NUM` * 0.1) when '2484' then (`ssv`.`LINE_NUM` * 14) when '2321' then (`ssv`.`LINE_NUM` * 3) when '2322' then (`ssv`.`LINE_NUM` * 10) when '2323' then (`ssv`.`LINE_NUM` * 10) when '2324' then (`ssv`.`LINE_NUM` * 3) when '2325' then (`ssv`.`LINE_NUM` * 3) when '2326' then (`ssv`.`LINE_NUM` * 3) when '2327' then (`ssv`.`LINE_NUM` * 3) when '2328' then (`ssv`.`LINE_NUM` * 3) when '2329' then (`ssv`.`LINE_NUM` * 3) when '2330' then (`ssv`.`LINE_NUM` * 3) when '2331' then (`ssv`.`LINE_NUM` * 3) when '2332' then (`ssv`.`LINE_NUM` * 3) when '2333' then (`ssv`.`LINE_NUM` * 3) when '2334' then (`ssv`.`LINE_NUM` * 3) when '2335' then (`ssv`.`LINE_NUM` * 1) when '2336' then (`ssv`.`LINE_NUM` * 1) when '2337' then (`ssv`.`LINE_NUM` * 1) when '2338' then (`ssv`.`LINE_NUM` * 1) when '2339' then (`ssv`.`LINE_NUM` * 1) when '2340' then (`ssv`.`LINE_NUM` * 1) when '2341' then (`ssv`.`LINE_NUM` * 1) when '2370' then (`ssv`.`LINE_NUM` * 10) when '2371' then (`ssv`.`LINE_NUM` * 10) when '2372' then (`ssv`.`LINE_NUM` * 3) when '2373' then (`ssv`.`LINE_NUM` * 3) when '2374' then (`ssv`.`LINE_NUM` * 3) when '2375' then (`ssv`.`LINE_NUM` * 3) when '2376' then (`ssv`.`LINE_NUM` * 3) when '2377' then (`ssv`.`LINE_NUM` * 3) when '2378' then (`ssv`.`LINE_NUM` * 3) when '2379' then (`ssv`.`LINE_NUM` * 1) when '2392' then (`ssv`.`LINE_NUM` * 3) when '2393' then (`ssv`.`LINE_NUM` * 3) when '2394' then (`ssv`.`LINE_NUM` * 1) when '2410' then (`ssv`.`LINE_NUM` * 3) when '2411' then (`ssv`.`LINE_NUM` * 3) when '2412' then (`ssv`.`LINE_NUM` * 1) when '2469' then (`ssv`.`LINE_NUM` * 10) when '2470' then (`ssv`.`LINE_NUM` * 3) when '2471' then (`ssv`.`LINE_NUM` * 1) when '2472' then (`ssv`.`LINE_NUM` * 3) when '2473' then (`ssv`.`LINE_NUM` * 3) when '2474' then (`ssv`.`LINE_NUM` * 1) when '2475' then (`ssv`.`LINE_NUM` * 1) when '2476' then (`ssv`.`LINE_NUM` * 1) when '2486' then (`ssv`.`LINE_NUM` * 3) when '2499' then (`ssv`.`LINE_NUM` * 1) when '2522' then (`ssv`.`LINE_NUM` * 10) when '2523' then (`ssv`.`LINE_NUM` * 3) when '2524' then (`ssv`.`LINE_NUM` * 3) when '2525' then (`ssv`.`LINE_NUM` * 3) when '2526' then (`ssv`.`LINE_NUM` * 3) when '2527' then (`ssv`.`LINE_NUM` * 3) when '2528' then (`ssv`.`LINE_NUM` * 3) when '2529' then (`ssv`.`LINE_NUM` * 1) when '2530' then (`ssv`.`LINE_NUM` * 1) when '2531' then (`ssv`.`LINE_NUM` * 1) when '2532' then (`ssv`.`LINE_NUM` * 1) when '2418' then (`ssv`.`LINE_NUM` * 3) when '2419' then (`ssv`.`LINE_NUM` * 1) when '2420' then (`ssv`.`LINE_NUM` * 1) when '2421' then (`ssv`.`LINE_NUM` * 1) when '2422' then (`ssv`.`LINE_NUM` * 1) when '2423' then (`ssv`.`LINE_NUM` * 1) when '2477' then (`ssv`.`LINE_NUM` * 3) when '2478' then (`ssv`.`LINE_NUM` * 3) when '2479' then (`ssv`.`LINE_NUM` * 3) when '2480' then (`ssv`.`LINE_NUM` * 3) when '2481' then (`ssv`.`LINE_NUM` * 3) when '2482' then (`ssv`.`LINE_NUM` * 1) when '2483' then (`ssv`.`LINE_NUM` * 1) when '2533' then (`ssv`.`LINE_NUM` * 3) when '2534' then (`ssv`.`LINE_NUM` * 8) when '2535' then (`ssv`.`LINE_NUM` * 5) when '2536' then (`ssv`.`LINE_NUM` * 80) when '2551' then (`ssv`.`LINE_NUM` * 1) when '2552' then (`ssv`.`LINE_NUM` * 1) when '2553' then (`ssv`.`LINE_NUM` * 3) when '2554' then (`ssv`.`LINE_NUM` * 5) when '2555' then (`ssv`.`LINE_NUM` * 8) when '2556' then (`ssv`.`LINE_NUM` * 13) when '2557' then (`ssv`.`LINE_NUM` * 19) when '2558' then (`ssv`.`LINE_NUM` * 25) when '2559' then (`ssv`.`LINE_NUM` * 44) when '2560' then (`ssv`.`LINE_NUM` * 6) when '2561' then (`ssv`.`LINE_NUM` * 14) when '2570' then (`ssv`.`LINE_NUM` * 14) when '2571' then (`ssv`.`LINE_NUM` * 17) when '2572' then (`ssv`.`LINE_NUM` * 24) when '2573' then (`ssv`.`LINE_NUM` * 31) when '2574' then (`ssv`.`LINE_NUM` * 38) when '2575' then (`ssv`.`LINE_NUM` * 45) when '2576' then (`ssv`.`LINE_NUM` * 52) when '2577' then (`ssv`.`LINE_NUM` * 59) when '2578' then (`ssv`.`LINE_NUM` * 67) when '2579' then (`ssv`.`LINE_NUM` * 74) when '2580' then (`ssv`.`LINE_NUM` * 81) when '2581' then (`ssv`.`LINE_NUM` * 7) when '2582' then (`ssv`.`LINE_NUM` * 2) when '2583' then (`ssv`.`LINE_NUM` * 4) when '2584' then (`ssv`.`LINE_NUM` * 6) when '2585' then (`ssv`.`LINE_NUM` * 2) when '2586' then (`ssv`.`LINE_NUM` * 4) when '2587' then (`ssv`.`LINE_NUM` * 6) else 0 end) AS `CLOUD_CHANGE`,(case `ssv`.`CATE` when '2449' then `ssv`.`LINE_NUM` when '2450' then `ssv`.`LINE_NUM` when '2451' then `ssv`.`LINE_NUM` when '2453' then `ssv`.`LINE_NUM` when '2435' then `ssv`.`LINE_NUM` when '2487' then `ssv`.`LINE_NUM` when '2488' then `ssv`.`LINE_NUM` when '2489' then `ssv`.`LINE_NUM` when '2490' then `ssv`.`LINE_NUM` when '2491' then `ssv`.`LINE_NUM` when '2436' then `ssv`.`LINE_NUM` when '2437' then `ssv`.`LINE_NUM` when '2504' then `ssv`.`LINE_NUM` when '2505' then `ssv`.`LINE_NUM` when '2506' then `ssv`.`LINE_NUM` when '2507' then `ssv`.`LINE_NUM` when '2508' then `ssv`.`LINE_NUM` when '2509' then `ssv`.`LINE_NUM` when '2500' then `ssv`.`LINE_NUM` when '2501' then `ssv`.`LINE_NUM` when '2562' then `ssv`.`LINE_NUM` when '2563' then `ssv`.`LINE_NUM` when '2564' then `ssv`.`LINE_NUM` when '2565' then `ssv`.`LINE_NUM` when '2566' then `ssv`.`LINE_NUM` when '2567' then `ssv`.`LINE_NUM` when '2568' then `ssv`.`LINE_NUM` when '2569' then `ssv`.`LINE_NUM` else 0 end) AS `YAHOO_PHYSICS`,(case `ssv`.`CATE` when '2449' then (`ssv`.`LINE_NUM` * 4) when '2450' then (`ssv`.`LINE_NUM` * 7) when '2451' then (`ssv`.`LINE_NUM` * 9) when '2453' then (`ssv`.`MONTH_NUM` * 0.000023) when '2435' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2487' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2488' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2489' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2490' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2491' then (`ssv`.`TEMP_NUM` * 0.000003125) when '2436' then (`ssv`.`LINE_NUM` * 0) when '2437' then (`ssv`.`LINE_NUM` * 0) when '2504' then (`ssv`.`LINE_NUM` * 4) when '2505' then (`ssv`.`LINE_NUM` * 7) when '2506' then (`ssv`.`LINE_NUM` * 9) when '2508' then (`ssv`.`MONTH_NUM` * 0.000023) when '2509' then (`ssv`.`LINE_NUM` * 1) when '2500' then (`ssv`.`TEMP_NUM` * 0.00001042) when '2501' then (`ssv`.`LINE_NUM` * 3) when '2562' then (`ssv`.`LINE_NUM` * 4) when '2563' then (`ssv`.`LINE_NUM` * 7) when '2564' then (`ssv`.`LINE_NUM` * 9) when '2565' then (`ssv`.`MONTH_NUM` * 0.000023) when '2566' then (`ssv`.`LINE_NUM` * 4) when '2567' then (`ssv`.`LINE_NUM` * 7) when '2568' then (`ssv`.`LINE_NUM` * 9) when '2569' then (`ssv`.`MONTH_NUM` * 0.000023) else 0 end) AS `YAHOO_CHANGE`,(case `ssv`.`CATE` when '2142' then `ssv`.`LINE_NUM` when '2240' then `ssv`.`LINE_NUM` when '2183' then `ssv`.`LINE_NUM` else 0 end) AS `HS`,(case `ssv`.`CATE` when '2438' then `ssv`.`LINE_NUM` when '2439' then `ssv`.`LINE_NUM` when '2440' then `ssv`.`LINE_NUM` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `SMART_PHONE`,(case `ssv`.`CATE` when '2143' then `ssv`.`LINE_NUM` when '2244' then `ssv`.`LINE_NUM` when '2184' then `ssv`.`LINE_NUM` else 0 end) AS `DATA_CARD`,(case `ssv`.`CATE` when '2441' then `ssv`.`LINE_NUM` when '2442' then `ssv`.`LINE_NUM` when '2443' then `ssv`.`LINE_NUM` else 0 end) AS `IPAD`,(case `ssv`.`CATE` when '2267' then `ssv`.`LINE_NUM` else 0 end) AS `GOOGLEAPPS`,(case `ssv`.`CATE` when '2500' then (((`ssv`.`TEMP_NUM` * 0.50) / 4000) / 12) when '2746' then (((`ssv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2747' then (((`ssv`.`TEMP_NUM` * 0.30) / 4000) / 12) when '2748' then (((`ssv`.`TEMP_NUM` * 0.20) / 4000) / 12) when '2749' then (((`ssv`.`TEMP_NUM` * 0.15) / 4000) / 12) when '2751' then (((`ssv`.`TEMP_NUM` * 0.40) / 4000) / 12) else 0 end) AS `O2O`,(case `ssv`.`CATE` when '2467' then (`ssv`.`LINE_NUM` * 0.01) else 0 end) AS `PAYPAL` from (((((((((`ifview_eida_opportunities` `opp` join `ifview_eida_sv_suggest_opportunities` `ops` on((`opp`.`ID` = `ops`.`OPPORTUNITIES_IDB`))) join `ifview_eida_sv_suggest` `ssv` on((`ops`.`SV_SUGGEST_IDA` = `ssv`.`ID`))) join `ifview_eida_mast_serviceline` `msv` on(((`ssv`.`CATE` = `msv`.`ID`) and (`msv`.`DELETED` = '0')))) join `ifview_eida_accounts_opportunities` `aco` on((`opp`.`ID` = `aco`.`OPPORTUNITY_ID`))) join `ifview_eida_accounts` `acc` on(((`aco`.`ACCOUNT_ID` = `acc`.`ID`) and (`acc`.`DELETED` = '0')))) left join `ifview_eida_users` `us1` on((`opp`.`ASSIGNED_USER_ID` = `us1`.`ID`))) left join `ifview_eida_users` `us2` on(((`opp`.`OWNER_ID` = `us2`.`ID`) and (`us2`.`DELETED` = '0')))) left join `mst_tanto_s_sosiki` `ts` on(((`us1`.`ID` = `ts`.`tanto_cd`) and (`ts`.`del_flg` = '0')))) left join `mst_eigyo_sosiki` `es` on(((`ts`.`eigyo_sosiki_cd` = `es`.`eigyo_sosiki_cd`) and (`es`.`del_flg` = '0')))) where (`opp`.`OP_CONT_PLANDATE_C` >= date_format((sysdate() - interval 6 month),'%Y/%m/01')) order by `aco`.`DATE_MODIFIED`,`aco`.`DELETED` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-29 15:28:59
