# 横浜ゴムMV（YMJ版）
本開発では、SFAで入力した訪問データや基幹システムから連携される実績データを閲覧し、状況を確認するための表やグラフを作成する。

## セットアップ

以下のコマンドを実行してください。

    gradle eclipse

またSpockでユニットテストを作成するために、Eclipseの[Groovyプラグイン](https://github.com/groovy/groovy-eclipse/wiki)を導入してください。


***



###アーキテクチャ
既存のMVシステムではすべての処理をJSPに記述されており、保守性が低いため以下の技術を用いて構造化を図る。
Seasar系ではなくSpring系を採用。

- Spring MVC
    - FrontControllerパターンに基づいたSpring独自のMVCフレームワーク。
- Doma
    - DBアクセス用O/Rマッパー
    - Daoクラスを作成してDBアクセスを定義する。
- Thymeleaf
    - Javaのテンプレートエンジンライブラリ
- D3.js
    - グラフをフロントエンドで描画するためのライブラリ

###開発ルール・実装詳細
Wikiを参照のこと