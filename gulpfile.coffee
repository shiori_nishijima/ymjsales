BASE_DIR = 'src/main/webapp/WEB-INF/'
TS_MAIN_SRC = BASE_DIR + 'ts/**/*.ts'

gulp = require 'gulp'
del = require 'del'
runSequence = require 'run-sequence'
plugins = require('gulp-load-plugins')()

#TypeScriptコンパイル
gulp.task 'tsc-main', ->
  gulp.src TS_MAIN_SRC
    .pipe plugins.plumber()
    .pipe plugins.tsc 
      target: 'ES5'
    .pipe gulp.dest BASE_DIR + 'js'

gulp.task 'clean', ->
  del  [BASE_DIR + 'js' + '/**/*', '!' + BASE_DIR + 'js/*.js' ]
  
gulp.task 'build', ->
  runSequence ['clean', 'tsc-main']

#ファイル変更を監視
gulp.task 'watch', -> 
  gulp.watch TS_MAIN_SRC, -> runSequence ['tsc-main']

# run-sequenceの入れ子呼び出しで記述すると、実行順序を保証できないので定数で渡す
BASE_TASKS = ['tsc-main', 'watch']
gulp.task 'default', ->
  runSequence BASE_TASKS
